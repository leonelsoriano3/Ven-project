/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.constans.ConstansPaymentReport;
import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.AddressesViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.ReviewKeysAnalysisViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.analysisKeys.CreatePaymentReportOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.PaymentReport;
import com.venedental.model.Specialists;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServicePaymentReport;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Null;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "reviewKeysAnalysisRequestBean")
@RequestScoped
public class ReviewKeysAnalysisRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(ReviewKeysAnalysisRequestBean.class.getName());

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{reviewKeysAnalysisViewBean}")
    private ReviewKeysAnalysisViewBean reviewKeysAnalysisViewBean;
    @ManagedProperty(value = "#{binnaclesRequestBean}")
    private BinnaclesRequestBean binnaclesRequestBean;
    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;
    @EJB
    private ServicePaymentReport servicePaymentReport;
    @EJB
    private MailSenderLocal mailSenderLocal;
    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;
    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiesPlansTreatmentKey;

    public List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsLists;

    public BinnaclesRequestBean getBinnaclesRequestBean() {
        return binnaclesRequestBean;
    }

    public void setBinnaclesRequestBean(BinnaclesRequestBean binnaclesRequestBean) {
        this.binnaclesRequestBean = binnaclesRequestBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public ReviewKeysAnalysisViewBean getReviewKeysAnalysisViewBean() {
        return reviewKeysAnalysisViewBean;
    }

    public void setReviewKeysAnalysisViewBean(ReviewKeysAnalysisViewBean reviewKeysAnalysisViewBean) {
        this.reviewKeysAnalysisViewBean = reviewKeysAnalysisViewBean;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public List<ConfirmKeyDetailsOutput> getConfirmKeyAndRejectedDetailsLists() {
        return confirmKeyAndRejectedDetailsLists;
    }

    public void setConfirmKeyAndRejectedDetailsLists(List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsLists) {
        this.confirmKeyAndRejectedDetailsLists = confirmKeyAndRejectedDetailsLists;
    }

    /**
     * Method to update the status of treatment and confirmed parts
     *
     * @throws java.sql.SQLException
     * @throws java.text.ParseException
     * @throws java.io.IOException
     */
    public void updateStatusTreatmentsConfirm() throws SQLException, ParseException, IOException {
        log.log(Level.INFO, "updateStatusTreatmentsConfirm()");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        Users user = this.securityViewBean.obtainUser();
        Date current = this.obtainDate();
        String nameUser = this.securityViewBean.obtainNameUser();
        String mes = this.getReviewKeysAnalysisViewBean().getNameMonth();
        this.savePaymentReport(current, nameUser, mes);
        this.getReviewKeysAnalysisViewBean().filterKeys();
        this.getReviewKeysAnalysisViewBean().goReviewKeysAnalysisList();
    }

    /**
     * Method that allows you to save reports paying a specialist whose treatments have been confirmed
     *
     * @param date
     * @param nameUser
     * @param mes
     * @throws IOException
     */
    @SuppressWarnings("empty-statement")
    public void     savePaymentReport(Date date, String nameUser, String mes) throws IOException {
        log.log(Level.INFO, " savePaymentReport()");
        try {
            PaymentReport paymentReport = new PaymentReport();
            Double auditedAmount = 0.0;
            Double rejectAmount = 0.0;
            Double amountTotal = 0.0;
            List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList = new ArrayList<>();
            Integer idSpecialist = this.getReviewKeysAnalysisViewBean().getSelectedSpecialist().getId();
            Integer idMonthExecution = this.getReviewKeysAnalysisViewBean().getIdMonthExecution();
            Date dateReception = this.getReviewKeysAnalysisViewBean().getSelectedConfirmKey().getDateExecution();
            confirmKeyAndRejectedDetailsList.addAll(this.servicePaymentReport.consultPaymentReport(idSpecialist,
                    idMonthExecution, dateReception));

            if (!confirmKeyAndRejectedDetailsList.isEmpty()) {
                for (ConfirmKeyDetailsOutput confirmKeyDetailsOutput : confirmKeyAndRejectedDetailsList) {
                    if (confirmKeyDetailsOutput.getIdStatus().equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                        rejectAmount += confirmKeyDetailsOutput.getAmountPaid();

                    } else if (confirmKeyDetailsOutput.getIdStatus().equals(EstatusPlansTreatmentsKeys.AUDITADO.
                            getValor())) {
                        auditedAmount += confirmKeyDetailsOutput.getAmountPaid();
                    }
                }
            }

            amountTotal = rejectAmount + auditedAmount;
            /*reporte*/
            CreatePaymentReportOutput createPaymentReportOutput = createPaymentReport(auditedAmount, rejectAmount);
            createPaymentReportOutput.setAmount(amountTotal);
            createPaymentReportOutput.setAuditedAmount(rejectAmount);
            this.generatePdfPaymentReport(date, createPaymentReportOutput, confirmKeyAndRejectedDetailsList, mes);
            for (ConfirmKeyDetailsOutput confirmKeyDetails : confirmKeyAndRejectedDetailsList) {
                if (confirmKeyDetails.getIdPropertiesPlansTreatmentsKey() == -1) {
                    this.serviceAnalysisKeys.createPaymentReportTreatments(createPaymentReportOutput.getIdPaymentReport(),
                            confirmKeyDetails.getIdPlansTreatmentsKey());
                    if (confirmKeyDetails.getIdStatus().equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                        this.servicePlansTreatmentKeys.updateStatus(confirmKeyDetails.getIdPlansTreatmentsKey(),
                                EstatusPlansTreatmentsKeys.ELIMINADO.getValor(), this.getSecurityViewBean().obtainUser().
                                getId(), null);
                    } else {
                        this.servicePlansTreatmentKeys.updateStatus(confirmKeyDetails.getIdPlansTreatmentsKey(),
                                EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor(), this.getSecurityViewBean().obtainUser().
                                getId(), null);
                    }
                } else {
                    this.serviceAnalysisKeys.createPaymentReportProperties(createPaymentReportOutput.getIdPaymentReport(),
                            confirmKeyDetails.getIdPropertiesPlansTreatmentsKey());
                    if (confirmKeyDetails.getIdStatus().equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                        this.servicePropertiesPlansTreatmentKey.updateStatus(confirmKeyDetails.
                                getIdPropertiesPlansTreatmentsKey(), EstatusPlansTreatmentsKeys.ELIMINADO.getValor(),
                                this.getSecurityViewBean().obtainUser().getId(), null);
                    } else {
                        this.servicePropertiesPlansTreatmentKey.updateStatus(confirmKeyDetails.
                                getIdPropertiesPlansTreatmentsKey(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor(),
                                this.getSecurityViewBean().obtainUser().getId(), null);
                    }
                }
            }
        } catch (SQLException | IOException e) {
            log.log(Level.INFO, e.toString());
        }
    }

    /**
     * |
     * *
     * Method to create a report attributes receiving payment by parameters
     *
     * @param amount
     * @param auditedAmount
     * @return
     *
     */
    public CreatePaymentReportOutput createPaymentReport(Double amount, Double auditedAmount) {
        CreatePaymentReportOutput createPaymentReportOutput = this.serviceAnalysisKeys.createPaymentReport(amount,
                auditedAmount);
        return createPaymentReportOutput;
    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansPaymentReport.FILENAME_PAYMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansPaymentReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * Method to build the body of the error message for mail that will be sent to the group veneden
     *
     * @return String
     * @throws IOException
     */
    private String buildMessageErrorVendental() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_ERROR_ADMIN_CONTENT));
        return stringBuilder.toString();
    }

    /**
     * Method to send an email with payment specialty was the report
     *
     * @param files
     * @param specialist
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailSpecialist(List<File> files, Specialists specialist, String mes, String amountTotal)
            throws ParseException, IOException {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}", specialist.getCompleteName());
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        if (specialist.getEmail() != null) {
            String title = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_TITLE_SPECIALIST);
            String subjectMessage = title + " - " + specialist.getCompleteName();
            String contentMessageEspecialist = buildMessageSpecialist(specialist, mes, amountTotal);
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files,
                    specialist.getEmail());
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files,
                    ConstansPaymentReport.EMAIL_VENEDEN_RP);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "Operación realizada con "
                    + "éxito"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "CLAVE(S) CONFIRMADA(S) "
                    + "CON ÉXITO. EL CORREO NO FUE ENVIADO AL ESPECIALISTA, YA QUE NO POSEE DIRECCIÓN DE CORREO "
                    + "ELECTRÓNICA REGISTRADA"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");

            log.log(Level.INFO, "El especialista no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

    }

    /**
     * Method to construct the message body for the email that will be sent to specialist
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialist(Specialists specialist, String mes, String amountTotal)
            throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        mes = this.toUpperCase(mes);
        stringBuilder.append("Estimado Doctor(a). ").append(specialist.getCompleteName()).append(",<br><br>");

        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT));
        stringBuilder.append(readFileProperties().getProperty(
                ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT2)).append(amountTotal).append(" ");
        stringBuilder.append(readFileProperties().getProperty(
                ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT3)).append("").append(mes);
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT4));
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT5));
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT6));
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT7));
        return stringBuilder.toString();
    }

    /**
     * Method to send an email to the administrators group veneden
     *
     * @param email
     * @param contentMessage
     * @throws IOException
     */
    public void sendMailVenedental(String email, String contentMessage) throws IOException {
        List<File> files = new ArrayList<File>();
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}");
        if (email != null) {
            String subjectMessage = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SUBJECT_VENEDEN);
            String title = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_TITLE_VENEDEN);
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessage, files, email);
        } else {
            log.log(Level.INFO, "Venedental no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

    }

    public Date obtainDate() throws ParseException {

        Date fecha = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdfDate.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fechaSolicitud = sdfDate.format(fecha);
        Date dateApplication = Transformar.StringToDate(fechaSolicitud);
        return dateApplication;

    }

    /**
     * Method to generate reports payment for specialists whose reports were created payment
     *
     * @param date
     * @param createPaymentReportOutput
     * @param confirmKeyAndRejectedDetailsList
     * @param mes
     * @throws IOException
     */
    public void generatePdfPaymentReport(Date date, CreatePaymentReportOutput createPaymentReportOutput,
                                         List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList, String mes)
            throws IOException {
        try {

            Specialists specialists = this.getReviewKeysAnalysisViewBean().getSelectedSpecialist();
            if (specialists != null) {

                String nameReport = "RP_" + specialists.getFirstName() + specialists.getLastname() + "_"
                        + specialists.getIdentityNumber().toString() + "_" + formatDate(date);

                Map<String, Object> params = new HashMap<>();
                String logoPath = ReviewKeysAnalysisRequestBean.obtainDirectory() + this.readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_LOGO);
                String jasperPath = ReviewKeysAnalysisRequestBean.obtainDirectory() + this.readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_JASPER);
                // Double honorarium = paymentReport.getAmount() + paymentReport.getAuditedAmount();
                DecimalFormat df = new DecimalFormat("#");
                df.setMaximumFractionDigits(8);
                params.put("number", createPaymentReportOutput.getNumber());
                params.put("totalAmount", createPaymentReportOutput.getAmount());
                params.put("auditAmount", createPaymentReportOutput.getAmount() - createPaymentReportOutput.
                        getAuditedAmount());
                params.put("rejectAmount", createPaymentReportOutput.getAuditedAmount());
                params.put("specialistName", specialists.getCompleteName());
                if (specialists.getRif() != null) {
                    params.put("specialistRif", specialists.getRif());
                } else {
                    params.put("specialistRif", "-");
                }
                params.put("date", date);
                params.put("month", mes);
                params.put("heal_logo2", logoPath);

                for (ConfirmKeyDetailsOutput confirmKeyDetailsOutput : confirmKeyAndRejectedDetailsList) {
                    String amountPaid = df.format(confirmKeyDetailsOutput.getAmountPaid());
                    confirmKeyDetailsOutput.setAmountPaidString(amountPaid);
                }
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                String amountTotalConvert = decimalFormat.format(createPaymentReportOutput.getAmount()
                        - createPaymentReportOutput.getAuditedAmount());
                buildReport(nameReport, confirmKeyAndRejectedDetailsList, params, jasperPath);
                sendMailSpecialist(findFile(nameReport), specialists, mes, amountTotalConvert);
            }
        } catch (ParseException | IOException e) {
            String mailAdminVenden = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_ADMIN_VENEDEN);
            sendMailVenedental(mailAdminVenden, buildMessageErrorVendental());
        }
    }

    /**
     * Method to build the report jasper
     *
     * @param nameReport
     * @param confirmKeyAndRejectedDetailsList
     * @param params
     * @param jasperPath
     * @throws IOException
     */
    public void buildReport(String nameReport, List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList,
                            Map<String, Object> params, String jasperPath) throws IOException {
        try {
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
                    confirmKeyAndRejectedDetailsList);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperPath, params, beanCollectionDataSource);
            nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
            String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
            String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + nameReport + reportExtension);
        } catch (JRException ex) {
            Logger.getLogger(ReviewKeysAnalysisRequestBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Method to seek payment report
     *
     * @return List File
     * @throws IOException
     */
    public List<File> findFile(String nameReport) throws IOException {
        List<File> files = new ArrayList<>();
        String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
        String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    /**
     * Method to convert a date format dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public String formatDate(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fecha = sdf.format(date);
        return fecha;
    }

    /**
     * Method to get the current project path
     *
     * @return String
     */
    public static String obtainDirectory() {
        log.log(Level.INFO, "obtainDirectory()");
        URL rutaURL = ReviewKeysAnalysisRequestBean.class.getProtectionDomain().getCodeSource().getLocation();
        String path = rutaURL.getPath();
        log.log(Level.INFO, "obtainDirectory() ->");
        log.log(Level.INFO, path);
        return path.substring(0, path.indexOf("lib"));
    }

    public String toUpperCase(String mes) {
        char primero = mes.charAt(0);
        mes = Character.toUpperCase(primero) + mes.substring(1, mes.length());
        return mes;
    }

}
