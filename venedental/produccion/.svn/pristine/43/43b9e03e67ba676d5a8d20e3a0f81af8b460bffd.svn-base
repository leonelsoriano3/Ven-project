/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.analysisKeys.*;
import com.venedental.dao.baremo.ConfigBaremoGeneralDAO;
import com.venedental.dao.paymentReport.RegisterPaymentReportDAO;
import com.venedental.dao.paymentReportProperties.RegisterPaymentReportPropertiesDAO;
import com.venedental.dao.paymentReportTreatments.RegisterPaymentReportTreatmentDAO;
import com.venedental.dao.unifyReport.UnifyReportDAO;
import com.venedental.dto.analysisKeys.*;
import com.venedental.dto.paymentReport.ConfirmKeyDetailsInvoicesTransferOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.dto.unifyReport.UnifyReportInput;
import com.venedental.model.StatusAnalysisKeys;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;


/**
 * @author akdeskdev90
 */
@Stateless
public class ServiceAnalysisKeys {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite listar los especialista que tienen tratamientos a
     * auditar
     *
     * @param statusId
     * @param dateFrom
     * @param dateUntil
     * @return
     */
    public List<SpecialistToAuditOutput> findSpecialistToAudit(Integer statusId, Date dateFrom, Date dateUntil) {
        SpecialistToAuditDAO registerSpecialistToAuditDAO = new SpecialistToAuditDAO(ds);
        SpecialistToAuditInput input = new SpecialistToAuditInput(statusId, dateFrom, dateUntil);
        registerSpecialistToAuditDAO.execute(input);
        return registerSpecialistToAuditDAO.getResultList();
    }

    /**
     * Servicio que permite consultar los datos del procesar pago
     *
     * @param idStatus
     * @param startDate
     * @param endDate
     * @return
     * @throws SQLException
     */
    public List<ConsultPaymentProcessOutput> consultPaymentProcess(int idStatus, Date startDate, Date endDate)
            throws SQLException {
        ConsultPaymentProcessDAO consultPaymentProcessDAO = new ConsultPaymentProcessDAO(ds);
        ConsultPaymentProcessInput consultPaymentProcessInput
                = new ConsultPaymentProcessInput(idStatus, startDate, endDate);
        consultPaymentProcessDAO.execute(consultPaymentProcessInput);
        return consultPaymentProcessDAO.getResultList();
    }

    /**
     * Servicio que permite consultar los datos del procesar pago
     *
     * @param idStatus1
     * @param idStatus2
     * @param startDate
     * @param endDate
     * @return
     * @throws SQLException
     */
    public List<ConsultPaymentProcessOutput> consultPaymentProcessByTwoStatus(int idStatus1, int idStatus2,
                                                                              Date startDate, Date endDate) throws SQLException {
        ConsultPaymentProcessByTwoStatusDAO consultPaymentProcessByTwoStatusDAO
                = new ConsultPaymentProcessByTwoStatusDAO(ds);
        ConsultPaymentProcessInput consultPaymentProcessInput = new ConsultPaymentProcessInput(idStatus1, idStatus2,
                startDate, endDate);
        consultPaymentProcessByTwoStatusDAO.execute(consultPaymentProcessInput);
        return consultPaymentProcessByTwoStatusDAO.getResultList();
    }

    /**
     * Buscar todos los estados para el analisis
     *
     * @return
     */
    public List<StatusAnalysisKeys> findAllStatusAnalysisKeys() {
        FindStatusAnalysisKeysDAO findStatusAnalysisKeysDAO = new FindStatusAnalysisKeysDAO(ds);
        findStatusAnalysisKeysDAO.execute(0);
        return findStatusAnalysisKeysDAO.getResultList();
    }

    /**
     * Buscar todos los estados para el analisis
     *
     * @param idStatus
     * @return
     */
    public StatusAnalysisKeys findStatusAnalysisKeysyId(Integer idStatus) {
        FindStatusAnalysisKeysDAO findStatusAnalysisKeysDAO = new FindStatusAnalysisKeysDAO(ds);
        findStatusAnalysisKeysDAO.execute(idStatus);
        return findStatusAnalysisKeysDAO.getResult();
    }

    public List<ConfirmKeyOutput> findConfirmKeyOutput(Integer idSpecialist) {
        ConfirmKeyDAO confirmKeyDAO = new ConfirmKeyDAO(ds);
        confirmKeyDAO.execute(idSpecialist);
        return confirmKeyDAO.getResultList();
    }

    /**
     * Service that seeks the confirmKeysDetails selected from the list
     * confirmList
     *
     * @param idSpecialist
     * @param status
     * @param month
     * @param dateReception
     * @return ConfirmKeyDetailsOutput
     */
    public List<ConfirmKeyDetailsOutput> findConfirmKeyDetailsOutput(Integer idSpecialist, Integer status, Integer month,
                                                                     Date dateReception) {
        ConfirmKeyDetailsDAO confirmKeyDetailsDAO = new ConfirmKeyDetailsDAO(ds);
        ConfirmKeyDetailsInput input = new ConfirmKeyDetailsInput();
        input.setIdSpecialist(idSpecialist);
        input.setIdStatus(status);
        input.setIdMonth(month);
        input.setDateReception(dateReception);
        confirmKeyDetailsDAO.execute(input);
        return confirmKeyDetailsDAO.getResultList();
    }

    /**
     * service that lets you create a PaymentReport
     *
     * @param amount
     * @param auditedAmount
     * @return
     */
    public CreatePaymentReportOutput createPaymentReport(Double amount, Double auditedAmount) {
        RegisterPaymentReportDAO registerPaymentReportDAO = new RegisterPaymentReportDAO(ds);
        CreatePaymentReportInput input = new CreatePaymentReportInput(amount, auditedAmount);
        registerPaymentReportDAO.execute(input);
        return registerPaymentReportDAO.getResult();
    }

    /**
     * service that lets you create a PaymentReportTreatments
     *
     * @param paymentReportId
     * @param plansTreatmentsKeysId
     * @throws java.sql.SQLException
     */
    public void createPaymentReportTreatments(Integer paymentReportId, Integer plansTreatmentsKeysId) throws SQLException {
        RegisterPaymentReportTreatmentDAO registerPaymentReportTreatmentDAO = new RegisterPaymentReportTreatmentDAO(ds);
        CreatePaymentReportTreatmentsInput input = new CreatePaymentReportTreatmentsInput(plansTreatmentsKeysId, paymentReportId);
        registerPaymentReportTreatmentDAO.execute(input);

    }

    /**
     * service that lets you create a PaymentReportTreatments
     *
     * @param paymentReportId
     * @param propertiesPlansTreatmentsKeysId
     * @throws java.sql.SQLException
     */
    public void createPaymentReportProperties(Integer paymentReportId, Integer propertiesPlansTreatmentsKeysId)
            throws SQLException {
        RegisterPaymentReportPropertiesDAO registerPaymentReportPropertiesDAO
                = new RegisterPaymentReportPropertiesDAO(ds);
        CreatePaymentReportPropertiesInput input = new CreatePaymentReportPropertiesInput(paymentReportId,
                propertiesPlansTreatmentsKeysId);
        registerPaymentReportPropertiesDAO.execute(input);

    }

    /**
     * Servicio para visualizar el detalle de las claves confirmadas en la
     * bandeja de procesar pago
     *
     * @param idPaymentReport
     * @param dateFrom
     * @param dateUtil
     * @param status
     * @return ConfirmKeyDetailsOutput
     */
    public List<ConfirmKeyDetailsInvoicesTransferOutput> findConfirmKeyDetailsInvoicesTransfer(Integer idPaymentReport,
                                                                                               Integer status, Date dateFrom, Date dateUtil) {
        ConfirmKeyDetailsInvoicesTransferDAO confirmKeyDetailsInvoicesTransferDAO
                = new ConfirmKeyDetailsInvoicesTransferDAO(ds);
        ConfirmKeyDetailsInput input = new ConfirmKeyDetailsInput(idPaymentReport, status, dateFrom, dateUtil);
        confirmKeyDetailsInvoicesTransferDAO.execute(input);
        return confirmKeyDetailsInvoicesTransferDAO.getResultList();
    }

    /**
     * Metodo para txt en estado por abonar
     *
     * @param status
     * @param dateStar
     * @param dateEnd
     * @return }
     */
    public List<GenerateTxtOutput> ConfirmGenerateTxtOutput(Integer status, Date dateStar, Date dateEnd) {
        GenerateTxtDAO generateTxtDAO = new GenerateTxtDAO(ds);
        GenerateTxtInput input = new GenerateTxtInput();
        input.setIdStatus(status);
        input.setDateStart(dateStar);
        input.setDateEnd(dateEnd);
        generateTxtDAO.execute(input);
        return generateTxtDAO.getResultList();
    }



    /**
     * Servicio que permite colocar en generado las claves generadas
     *
     * @param idKey
     * @throws java.sql.SQLException
     */
    public void changeStatusKeyGenerated(Integer idKey,Date reciveDate) throws SQLException {
        ChangeStatusKeyGeneratedDAO changeStatusKeyGeneratedDAO = new ChangeStatusKeyGeneratedDAO(ds);
        ChangeStatusKeyGeneratedInput input = new ChangeStatusKeyGeneratedInput(idKey,reciveDate);
        changeStatusKeyGeneratedDAO.execute(input);
    }


    /**
     * @param idKey id de la clave
     * @return la clave posee tratamientos
     */
    public ConsultKeyHaveTreatmentOutPut findKeyHaveTreatment(Integer idKey) {
        ConsultKeyHaveTreatmentDAO consultKeyHaveTreatmentDAO = new ConsultKeyHaveTreatmentDAO(ds);
        consultKeyHaveTreatmentDAO.execute(idKey);
        return consultKeyHaveTreatmentDAO.getResult();
    }



    /**
     *
     * @param idKey id de la clave
     * @return regresa pojo para el procedimiento SP_T_BUSCAR_CLAVE_PLANTTREAMKEY(in Int,out Int)
     */
    public ConsultKeyInStatusGenetaredOutPut findKeyInStatusGenerate(Integer idKey) {
        ConsultKeyInStatusGenetaredDAO consultKeyInStatusGenetaredDAO = new ConsultKeyInStatusGenetaredDAO(ds);

        consultKeyInStatusGenetaredDAO.execute(idKey);
        return consultKeyInStatusGenetaredDAO.getResult();
    }


    /**
     * Revenvio de correo a especialista/pagos
     *
     * @param specialistId
     * @param numberMonth
     * @param dateReception
     * @return
     */
    public List<ConsultKeyDetailsStatusPaidOutPut> findConfirmKeyDetails(Integer specialistId,
                                                                         Integer numberMonth, java.sql.Date dateReception) {

        ConsultKeyDetailsStatusPaidDAO consultKeyDetailsStatusPaidDAO = new ConsultKeyDetailsStatusPaidDAO(ds);
        ConsultKeyDetailsStatusPaidInput input = new ConsultKeyDetailsStatusPaidInput(specialistId, numberMonth, dateReception);
        consultKeyDetailsStatusPaidDAO.execute(input);

        return consultKeyDetailsStatusPaidDAO.getResultList();
    }


    public List<ReportAuditedAnyStateOutPut> findReportAuditedAnyState(Integer status, Date startDate, Date endDate) {

        ReportAuditedAnyStateDAO reportAuditedAnyStateDAO = new ReportAuditedAnyStateDAO(ds);
        ReportAuditedAnyStateInput input;

        if (startDate == null || endDate == null) {
            input = new ReportAuditedAnyStateInput(status, null, null);
        } else {
            input = new ReportAuditedAnyStateInput(status, new java.sql.Date(startDate.getTime()),
                    new java.sql.Date(endDate.getTime()));
        }

        reportAuditedAnyStateDAO.execute(input);

        return reportAuditedAnyStateDAO.getResultList();
    }


    /**
     * consigue el payment report id y numero
     * @param idSpecialist
     * @param idStatus
     * @param idMonth
     * @param dateRetention
     * @return
     */
    public ConsultPaymentReportOutPut  findPaimentReport(int idSpecialist,int idStatus,int idMonth,Date dateRetention){

        ConsultPaymentReportDao consultPaymentReportDao = new ConsultPaymentReportDao(ds);
        ConsultPaymentReportInput input = new ConsultPaymentReportInput(idSpecialist,idStatus,idMonth,
                new java.sql.Date(dateRetention.getTime()));

        consultPaymentReportDao.execute(input);
        return consultPaymentReportDao.getResult();
    }
    
      /**
     * servicio paraunificar reportes de pago
     *
     * @param idSpecialist
     * @param idStatus
     * @param idMonth
     * @param yearReport
     * @throws java.sql.SQLException
     */

    public void unifyPaymentReport(Integer idSpecialist,Integer idStatus,Integer idMonth, Integer  yearReport)throws SQLException {
        UnifyReportDAO unifyReportDAO= new UnifyReportDAO(ds);
        UnifyReportInput input = new UnifyReportInput(idSpecialist, idStatus, idMonth, yearReport);
        unifyReportDAO.execute(input);

    }

}
