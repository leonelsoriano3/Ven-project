package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dto.NetworkSpecialistsReportOutput;
import com.venedental.enums.Reemplazo;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.store_procedure.NetworkSpecialistsReportFacadeLocal;
import com.venedental.model.Cities;
import com.venedental.model.SpecialistJob;
import com.venedental.model.Specialists;
import com.venedental.model.Speciality;
import com.venedental.model.State;
import com.venedental.model.TypeSpecialist;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;
import services.utils.EncodeString;
import services.utils.Transformar;

@ManagedBean(name = "directorioViewBean")
@ViewScoped
public class DirectorioViewBean extends BaseBean {
    
    @EJB
    private SpecialistsFacadeLocal specialistsFacadeLocal;
    
    @EJB
    private NetworkSpecialistsReportFacadeLocal networkSpecialistFacadeLocal;
    
    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;
    
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    
    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;
    
    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;
    
    private static final long serialVersionUID = -5547104134772667835L;
    
    private static final Logger log = CustomLogger.getGeneralLogger(DirectorioViewBean.class.getName());
    
    private Map<String, Object> params = new HashMap<String, Object>();
    
    private List<NetworkSpecialistsReportOutput> listaNetworkSpecialists = new ArrayList<>();
    
    private JRBeanCollectionDataSource beanCollectionDataSource;
    
    private String logoPath;
    
    private String reportPath;
    
    private JasperPrint jasperPrint;
    
    public DirectorioViewBean() {
        this.filtroDoctor = "";
        this.viewCitie = new Cities();
        this.viewCitie.setName("");
        this.viewSpeciality = new Speciality();
        this.viewSpeciality.setName("");
        this.viewState = new State();
        this.viewState.setNombre("");
        this.visible = false;
    }
    
    @PostConstruct
    public void init() {
        this.setViewState(new State(10, "Zona Metropolitana"));
        this.getCitiesViewBean().filtrarCitiesClinic(viewState);
        List<Specialists> s = this.specialistsFacadeLocal.findByStatus(1);
        this.getListaSpecialists().addAll(s);
        this.getListaSpecialistsFiltrada().addAll(s);
        this.filtrar();
        this.listaNetworkSpecialists = networkSpecialistFacadeLocal.execute(null);
    }
    
    public void load() throws IOException, JRException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listaNetworkSpecialists", new JRBeanCollectionDataSource(listaNetworkSpecialists));
        this.params.put("heal_logo2", logoPath);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(listaNetworkSpecialists);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/networkSpecialists/networkSpecialists.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }
    
    private Specialists selectedSpecialists;
    
    private List<Object> listaSpecialists;
    
    private List<Object> listaSpecialistsFiltrada;
    
    private State viewState;
    
    private Cities viewCitie;
    
    private TypeSpecialist viewTypeSpecialist;
    
    private Speciality viewSpeciality;
    
    private String filtroDoctor;
    
    private boolean visible;
    
    public boolean isVisible() {
        return visible;
    }
    
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }
    
    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }
    
    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }
    
    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }
    
    public String getFiltroDoctor() {
        return filtroDoctor;
    }
    
    public void setFiltroDoctor(String filtroDoctor) {
        this.filtroDoctor = filtroDoctor;
    }
    
    public Speciality getViewSpeciality() {
        return viewSpeciality;
    }
    
    public void setViewSpeciality(Speciality viewSpeciality) {
        this.viewSpeciality = viewSpeciality;
    }
    
    public TypeSpecialist getViewTypeSpecialist() {
        return viewTypeSpecialist;
    }
    
    public void setViewTypeSpecialist(TypeSpecialist viewTypeSpecialist) {
        this.viewTypeSpecialist = viewTypeSpecialist;
    }
    
    public State getViewState() {
        return viewState;
    }
    
    public void setViewState(State viewState) {
        this.viewState = viewState;
    }
    
    public Cities getViewCitie() {
        return viewCitie;
    }
    
    public void setViewCitie(Cities viewCitie) {
        this.viewCitie = viewCitie;
    }
    
    public Specialists getselectedSpecialists() {
        return selectedSpecialists;
    }
    
    public void setselectedSpecialists(Specialists selectedSpecialists) {
        this.selectedSpecialists = selectedSpecialists;
    }
    
    public List<Object> getListaSpecialists() {
        if (this.listaSpecialists == null) {
            this.listaSpecialists = new ArrayList<>();
        }
        return listaSpecialists;
    }
    
    public void setlistaSpecialists(List<Object> listaSpecialists) {
        this.listaSpecialists = listaSpecialists;
    }
    
    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }
    
    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }
    
    public List<Object> getListaSpecialistsFiltrada() {
        if (this.listaSpecialistsFiltrada == null) {
            this.listaSpecialistsFiltrada = new ArrayList<>();
        }
        return listaSpecialistsFiltrada;
    }
    
    public void setListaSpecialistsFiltrada(List<Object> listaSpecialistsFiltrada) {
        this.listaSpecialistsFiltrada = listaSpecialistsFiltrada;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }
    
    public void filtrar() {
        
        if (this.getViewState() == null && this.getViewCitie() == null && this.getViewTypeSpecialist() == null && this.getViewSpeciality() == null && this.getFiltroDoctor().isEmpty()) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe seleccionar un criterio para realizar la busqueda"));
        }
        try {
            listaSpecialistsFiltrada.clear();
            
            //Preparamos los objetos a de filtro que puedan llegar en null
            if (this.getFiltroDoctor() == null) {
                this.setFiltroDoctor("");
            }
            if (this.getViewState() == null) {
                this.setViewState(new State());
                this.getViewState().setNombre("");
            }
            
            if (this.getViewCitie() == null) {
                this.setViewCitie(new Cities());
                this.getViewCitie().setName("");
            }
            
            if (this.getViewSpeciality() == null) {
                this.setViewSpeciality(new Speciality());
                this.getViewSpeciality().setName("");
            }
            
            if (this.getViewTypeSpecialist() == null) {
                this.setViewTypeSpecialist(new TypeSpecialist());
                this.getViewTypeSpecialist().setName("");
            }
            ////////////////////////////////////////////////////////////////////////////
            
            for (Object s : this.listaSpecialists) {
                Specialists mySpecialists = (Specialists) s;
                if (mySpecialists != null) {
                    //Filtramos campos que no son listas
                    String campoEspecialista = EncodeString.reemplazarString(mySpecialists.getCompleteName(), Reemplazo.AAcento, Reemplazo.EAcento, Reemplazo.Apostrofe, Reemplazo.IAcento, Reemplazo.OAcento, Reemplazo.UAcento, Reemplazo.UDieresis);
                    String filtroCDoctor = EncodeString.reemplazarString(this.getFiltroDoctor(), Reemplazo.AAcento, Reemplazo.EAcento, Reemplazo.Apostrofe, Reemplazo.IAcento, Reemplazo.OAcento, Reemplazo.UAcento, Reemplazo.UDieresis);
                    
                    if ((campoEspecialista.contains(filtroCDoctor) || filtroCDoctor.trim().equals(""))
                            && ((mySpecialists.getTypeSpecialistid() != null && mySpecialists.getTypeSpecialistid().getName().toUpperCase().contains(this.getViewTypeSpecialist().getName().toUpperCase())) || this.getViewTypeSpecialist().getName().trim().equals(""))) {
                        //Filtramos por Lista de Estados donde se encuentren Clínicas
                        if (!mySpecialists.getSpecialistJobList().isEmpty()) {
                            FILTROCLINICAS:
                            for (SpecialistJob c : mySpecialists.getSpecialistJobList()) {
                                if (c.getMedicalOfficeId().getClinicId() != null) {
                                    c.getMedicalOfficeId().setName(c.getMedicalOfficeId().getClinicId().getName());
                                    
                                }
                                
                                if (((c.getMedicalOfficeId() != null && c.getMedicalOfficeId().getAddressId() != null && c.getMedicalOfficeId().getAddressId().getCityId().getStateId().getNombre().toUpperCase().contains(this.getViewState().getNombre().toUpperCase())) || this.getViewState().getNombre().trim().equals(""))
                                        && ((c.getMedicalOfficeId() != null && c.getMedicalOfficeId().getAddressId() != null && c.getMedicalOfficeId().getAddressId().getCityId().getName().toUpperCase().contains(this.getViewCitie().getName().toUpperCase())) || this.getViewCitie().getName().trim().equals(""))) {
                                    if (!mySpecialists.getSpecialityList().isEmpty()) {
                                        for (Speciality y : mySpecialists.getSpecialityList()) {
                                            if (y.getName().toUpperCase().contains(this.getViewSpeciality().getName().toUpperCase()) || this.getViewSpeciality().getName().trim().equals("")) {
                                                
                                                listaSpecialistsFiltrada.add(mySpecialists);
                                                break FILTROCLINICAS;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
    }
    
    public void limpiarFiltro() {
        listaSpecialistsFiltrada.clear();
        
        this.setFiltroDoctor("");
        
        this.setViewState(new State());
        this.getViewState().setNombre("");
        
        this.setViewCitie(new Cities());
        this.getViewCitie().setName("");
        
        this.setViewSpeciality(new Speciality());
        this.getViewSpeciality().setName("");
        
        this.setViewTypeSpecialist(new TypeSpecialist());
        this.getViewTypeSpecialist().setName("");
        
        this.getSpecialityViewBean().getListaSpeciality().clear();
        this.getCitiesViewBean().getListCities().clear();
        
        for (Object s : this.listaSpecialists) {
            Specialists mySpecialists = (Specialists) s;
            if (mySpecialists != null) {
                listaSpecialistsFiltrada.add(mySpecialists);
            }
        }
    }
    
    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }
    
    public void networkPrintingSpecialists(){
        this.listaNetworkSpecialists = networkSpecialistFacadeLocal.execute(null);
    }
    
    public void showReportPdf(ActionEvent actionEvent) throws JRException, IOException,Exception{
        if(!listaNetworkSpecialists.isEmpty()){
            this.load();
            this.export.pdf(jasperPrint,"Red de Especialistas",Transformar.getThisDate());
        }
    }
}
