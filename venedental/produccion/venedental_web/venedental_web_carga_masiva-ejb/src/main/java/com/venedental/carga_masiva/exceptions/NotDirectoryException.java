/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import com.venedental.carga_masiva.base.Messages;
import java.io.File;

/**
 *
 * @author usuario
 */
public class NotDirectoryException extends FileException {
    
    public NotDirectoryException(File file) {
        super(Messages.EXCEPTION_NOT_A_DIRECTORY, file);
    }

    public NotDirectoryException(File file, Throwable throwable) {
        super(Messages.EXCEPTION_NOT_A_DIRECTORY, file, throwable);
    }

}
