/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.ejb;

import javax.ejb.Local;

/**
 *
 * @author usuario
 */
@Local
public interface MassiveLoadLocal {

    public void execute(boolean automatic);

}
