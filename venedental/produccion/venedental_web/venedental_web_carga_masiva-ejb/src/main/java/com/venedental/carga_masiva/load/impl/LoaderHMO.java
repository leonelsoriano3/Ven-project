/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.impl;

import com.venedental.carga_masiva.exceptions.MessageException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class LoaderHMO extends LoaderBasic {

    @Override
    public List<Integer> readPlans(String filename, String sheetName, String[] row, String[] field, int index) throws MessageException {
        List<Integer> ids = new ArrayList<>();
        switch (sheetName) {
            case "Odontología":
                ids.add(14);
                break;
            case "Oftalmología":
                ids.add(15);
                break;
        }
        return ids;
    }

}
