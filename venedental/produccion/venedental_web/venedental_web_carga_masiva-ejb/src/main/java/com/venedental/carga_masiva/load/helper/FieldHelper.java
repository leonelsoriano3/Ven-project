/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.helper;

import com.venedental.carga_masiva.base.Constants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author usuario
 */
public class FieldHelper {

    public static Date getDate(String[] templateField, int fieldIndex, String[] row) {
        String value = getContent(templateField, fieldIndex, row);
        if (value.contains(Constants.DATE_SEPARATOR)) {
            value = value.replace(Constants.DATE_SEPARATOR, Constants.DIRECTORY_SEPARATOR);
        }
        SimpleDateFormat format;
        if (value.length() > 8) {
            String[] tokens = value.split(Constants.DIRECTORY_SEPARATOR);
            if (tokens[0].length() == 4) {
                format = new SimpleDateFormat(Constants.FORMAT_DATE_4_YEAR);
            } else {
                format = new SimpleDateFormat(Constants.FORMAT_DATE_4);
            }
        } else {
            format = new SimpleDateFormat(Constants.FORMAT_DATE_2);
        }
        try {
            return format.parse(value);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Double getDouble(String[] templateField, int fieldIndex, String[] row) {
        String value = getContent(templateField, fieldIndex, row);
        return (value == null ? null : Double.parseDouble(value));
    }

    public static Integer getInteger(String[] templateField, int fieldIndex, String[] row) {
        String value = getContent(templateField, fieldIndex, row);
        return (value == null ? null : Integer.parseInt(value));
    }

    public static String getString(String[] templateField, int fieldIndex, String[] row) {
        return getContent(templateField, fieldIndex, row);
    }

    private static String getContent(String[] templateField, int fieldIndex, String[] row) {
        Integer index = getIndex(templateField, fieldIndex);
        if (index == null) {
            return null;
        }
        return row[index];
    }

    private static Integer getIndex(String[] templateField, int fieldIndex) {
        String field = templateField[fieldIndex];
        if (field.equals(Constants.CAMPO_POR_PROCESAR)) {
            return null;
        }
        return Integer.parseInt(field);
    }

}
