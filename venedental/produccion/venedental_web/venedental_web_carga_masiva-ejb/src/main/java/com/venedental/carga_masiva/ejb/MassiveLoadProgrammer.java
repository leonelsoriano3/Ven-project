/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.ejb;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author usuario
 */
@Stateless
public class MassiveLoadProgrammer implements MassiveLoadProgrammerLocal {

    private static final Logger log = CustomLogger.getLogger(MassiveLoadProgrammer.class.getName());

    @EJB
    private MassiveLoadLocal massiveLoadLocal;

    private boolean loading = false;

    //@Schedule(hour = "*", minute = "*", second = "0")
    @Override
    public void schedule() {
        execution(true);
    }

    @Override
    public boolean execute() {
        if (!loading) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    executionManual();
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            return true;
        }
        return false;
    }

    private void executionManual() {
        execution(false);
    }

    private void execution(boolean automatic) {
        if (!loading) {
            loading = true;
            log.log(Level.INFO, "========================================================");
            SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_COMPLETE);
            log.log(Level.INFO, "{0} - INICIANDO PROCESO DE CARGA MASIVA", format.format(new Date()));
            massiveLoadLocal.execute(automatic);
            log.log(Level.INFO, "{0} - FINALIZANDO PROCESO DE CARGA MASIVA", format.format(new Date()));
            log.log(Level.INFO, "========================================================");
            loading = false;
        }
    }

}
