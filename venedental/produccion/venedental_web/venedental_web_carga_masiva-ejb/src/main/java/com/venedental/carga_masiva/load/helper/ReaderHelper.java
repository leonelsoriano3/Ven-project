/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.helper;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.Messages;
import com.venedental.carga_masiva.dto.RowDTO;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.load.AbstractLoader;
import com.venedental.model.Insurances;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import com.venedental.model.Relationship;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author usuario
 */
public class ReaderHelper {

    private final Insurances insurance;
    private final AbstractLoader loader;
    private final List<Plans> plans;
    private final ServiceFacadeDTO serviceFacadeDTO;

    public ReaderHelper(AbstractLoader loader, Insurances insurance, List<Plans> plans, ServiceFacadeDTO serviceFacadeDTO) {
        this.insurance = insurance;
        this.loader = loader;
        this.plans = plans;
        this.serviceFacadeDTO = serviceFacadeDTO;
    }

    public RowDTO readRow(String filename, String sheetName, long numRow, String[] row, String[] fieldTemplate) {
        String fileSheet = null;
        String location = null;
        String resume = null;
        try {
            fileSheet = loadFileSheet(filename, sheetName);
            location = loadLocation(fileSheet, numRow);
            resume = loadResume(location, row, fieldTemplate);
            Patients patient = loadPatient(row, fieldTemplate);
            Relationship relationship = loadRelationship(row, fieldTemplate, 2);
            List<Plans> plansSelected = loadPlans(filename, sheetName, row, fieldTemplate, 6);
            return new RowDTO(insurance, patient, relationship, plansSelected, fileSheet, location, resume);
        } catch (MessageException e) {
            return new RowDTO(e, fileSheet, location, resume);
        }
    }

    private String loadFileSheet(String filename, String sheetName) {
        StringBuilder sb = new StringBuilder(filename);
        sb.append("\t");
        sb.append(sheetName);
        return sb.toString();
    }

    private String loadLocation(String fileSheet, long numRow) {
        StringBuilder sb = new StringBuilder(fileSheet);
        sb.append("\t");
        sb.append(numRow);
        return sb.toString();
    }

    private String loadResume(String location, String[] row, String[] templateField) throws MessageException {
        try {
            StringBuilder sb = new StringBuilder(location);
            List<Integer> listField = getListFields(templateField);
            for (Integer i : listField) {
                String cell = row[i];
                sb.append("\t");
                sb.append(cell == null || cell.isEmpty() ? "-Vacio-" : cell);
            }
            return sb.toString();
        } catch (Exception e) {
            throw new MessageException(Messages.MESSAGE_DATA_INVALID, e);
        }
    }

    private List<Integer> getListFields(String[] templateField) {
        List<Integer> list = new ArrayList<>();
        for (String field : templateField) {
            if (!field.equals(Constants.CAMPO_POR_PROCESAR)) {
                list.add(Integer.parseInt(field));
            }
        }
        return list;
    }

    private Patients loadPatient(String[] row, String[] fieldTemplate) throws MessageException {
        try {
            Double idPatient = FieldHelper.getDouble(fieldTemplate, 0, row);
            Double idPatientHolder = FieldHelper.getDouble(fieldTemplate, 1, row);
            String completeName = FieldHelper.getString(fieldTemplate, 3, row);
            Date birthDate = FieldHelper.getDate(fieldTemplate, 4, row);
            String sex = FieldHelper.getString(fieldTemplate, 5, row);
            List<MessageException> listException = new ArrayList<>();
            Patients patient = createPatient(idPatient, completeName, birthDate, sex, idPatientHolder, listException);
            loader.loadCustomFieldsPatient(patient, row, fieldTemplate, listException);
            if (!listException.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                sb.append(listException.get(0).getError());
                for (int i = 1; i < listException.size(); i++) {
                    MessageException messageException = listException.get(i);
                    sb.append("/");
                    sb.append(messageException.getError());
                }
                throw new MessageException(sb.toString());
            }
            validatePatient(patient);
            return patient;
        } catch (MessageException e) {
            throw e;
        } catch (NumberFormatException e) {
            throw new MessageException("Error al convertir el numero de la columna", e);
        } catch (Exception e) {
            throw new MessageException("Error de lectura", e);
        }
    }

    private Patients createPatient(Double idPatient, String completeName, Date dateBirth, String sex, Double idHolder, List<MessageException> listException) throws MessageException {
        Patients patient = new Patients();
        try {
            int idPatientInt = Integer.parseInt(String.format(Constants.FORMAT_INT_STRING, idPatient));
            patient.setIdentityNumber(idPatientInt);
        } catch (NumberFormatException e) {
            listException.add(new MessageException(Messages.MESSAGE_ID_INVALID));
        }
        try {
            int idHolderInt = Integer.parseInt(String.format(Constants.FORMAT_INT_STRING, idHolder));
            patient.setIdentityNumberHolder(idHolderInt);
        } catch (Exception e) {
            listException.add(new MessageException(Messages.MESSAGE_ID_HOLDER_INVALID));
        }
        try {
            patient.setDateOfBirth(dateBirth);
        } catch (Exception e) {
            listException.add(new MessageException(Messages.MESSAGE_BIRTH_INVALID));
        }
        try {
            if (sex.toUpperCase().equals(Constants.INITIAL_SEX_F) || sex.toUpperCase().equals(Constants.INITIAL_SEX_M)) {
                patient.setSex(sex.toUpperCase());
            } else if (sex.toUpperCase().startsWith(Constants.PREFIJO_SEXO_F)) {
                patient.setSex(Constants.INITIAL_SEX_F);
            } else if (sex.toUpperCase().startsWith(Constants.PREFIJO_SEXO_M)) {
                patient.setSex(Constants.INITIAL_SEX_M);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            listException.add(new MessageException(Messages.MESSAGE_SEX_INVALID));
        }
        completeName = completeName.replace(".", " ").replace(",", " ");
        completeName = completeName.replace("   ", " ").replace("   ", " ");
        completeName = completeName.replace("  ", " ").replace("  ", " ");
        completeName = completeName.trim();
        patient.setCompleteName(completeName);
        patient.setStatus(1);
        return patient;
    }

    private void validatePatient(Patients patient) throws MessageException {
        if (patient.getIdentityNumber() == null) {
            throw new MessageException(Messages.MESSAGE_ID_INVALID);
        }
        if (patient.getIdentityNumberHolder() == null) {
            throw new MessageException(Messages.MESSAGE_ID_HOLDER_INVALID);
        }
        if (patient.getDateOfBirth() == null) {
            throw new MessageException(Messages.MESSAGE_BIRTH_INVALID);
        }
        if (patient.getCompleteName() == null) {
            throw new MessageException(Messages.MESSAGE_COMPLETE_NAME_INVALID);
        }
        if (patient.getSex() == null) {
            throw new MessageException(Messages.MESSAGE_SEX_INVALID);
        }
    }

    private Relationship loadRelationship(String[] row, String[] template, int i) throws MessageException {
        try {
            Integer idRelationship = loader.loadRelationship(row, template, i);
            return serviceFacadeDTO.getRelationshipFacadeLocal().findById(idRelationship).get(0);
        } catch (MessageException e) {
            throw e;
        }
    }

    private List<Plans> loadPlans(String filename, String sheetName, String[] row, String[] template, int index) throws MessageException {
        List<Integer> listIds = loader.readPlans(filename, sheetName, row, template, index);
        List<Plans> plansList = new ArrayList<>();
        for (Integer id : listIds) {
            Plans selectedPlan = null;
            for (Plans plan : plans) {
                if (((int) plan.getId()) == ((int) id)) {
                    selectedPlan = plan;
                    break;
                }
            }
            if (selectedPlan == null) {
                throw new MessageException(Messages.EXCEPTION_PLANS_INVALID);
            } else {
                plansList.add(selectedPlan);
            }
        }
        return plansList;
    }

}
