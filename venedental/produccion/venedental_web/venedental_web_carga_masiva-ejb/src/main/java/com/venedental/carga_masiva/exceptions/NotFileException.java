/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import com.venedental.carga_masiva.base.Messages;
import java.io.File;

/**
 *
 * @author usuario
 */
public class NotFileException extends FileException {

    public NotFileException(File file) {
        super(Messages.EXCEPTION_NOT_A_FILE, file);
    }

    public NotFileException(File file, Throwable throwable) {
        super(Messages.EXCEPTION_NOT_A_FILE, file, throwable);
    }

}
