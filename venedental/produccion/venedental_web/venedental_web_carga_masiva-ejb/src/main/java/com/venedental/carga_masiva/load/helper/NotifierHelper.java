/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.helper;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.properties.PropertiesInsurances;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.mail.base.enums.Email;
import com.venedental.model.Insurances;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class NotifierHelper {

    private static final Logger log = CustomLogger.getLogger(NotifierHelper.class.getName());

    private final ServiceFacadeDTO serviceFacadeDTO;

    public NotifierHelper(ServiceFacadeDTO serviceFacadeDTO) {
        this.serviceFacadeDTO = serviceFacadeDTO;
    }

    public void notifyInsurance(List<File> files, Insurances insurance, String insuranceName) {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por aseguradora: {0}", insuranceName.toUpperCase());
        String insuranceMail = PropertiesInsurances.get(insuranceName + Constants.SUFIJO_MAIL);
        if (insuranceMail != null) {
            String subjectMessage = "Carga Masiva - " + insurance.getName();
            String contentMessageInsurance = buildMessageInsurance(insurance);
            serviceFacadeDTO.getMailSenderLocal().send(Email.MAIL_CARGA_MASIVA, subjectMessage, "Resultados de la Carga Masiva", contentMessageInsurance, files, insuranceMail);
        } else {
            log.log(Level.INFO, "La aseguradora no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por aseguradora");
    }

    public void notifyGeneral(List<File> files, boolean automatic, String mail) {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos general");
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_4 + " " + Constants.FORMAT_HOUR_12);
        String subjectMessage = "Carga Masiva - " + format.format(new Date());
        String contentMessageVenedental = buildMessageVenedental(files, automatic);
        serviceFacadeDTO.getMailSenderLocal().send(Email.MAIL_CARGA_MASIVA, subjectMessage, "Resultados de la Carga Masiva - General", contentMessageVenedental, files, mail);
        log.log(Level.INFO, "Fin - Proceso de envio de correos general");
    }

    private String buildMessageInsurance(Insurances insurance) {
        StringBuilder stringBuilder = new StringBuilder();
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_4 + " " + Constants.FORMAT_HOUR_12);
        stringBuilder.append("<b>").append("Aseguradora: ").append("</b>").append(insurance.getName()).append("\n");
        stringBuilder.append("(").append(format.format(new Date())).append(")\n");
        return stringBuilder.toString();
    }

    private String buildMessageVenedental(List<File> files, boolean automatic) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<b>").append("Tipo de Carga: ").append("</b>");
        if (automatic) {
            stringBuilder.append("Automática").append("<br/>");
        } else {
            stringBuilder.append("Manual").append("<br/>");
        }
        if (!files.isEmpty()) {
            stringBuilder.append("<br/>").append("<b>").append("Resumen de la Carga:").append("</b>").append("<br/>");
            for (File file : files) {
                stringBuilder.append(file.getName().replace(".txt", "").replace("Resumen", "").replace("-", " - ")).append("<br/>");
            }
        } else {
            stringBuilder.append("<br/>").append(" - No se cargaron archivos").append("<br/>");
        }
        return stringBuilder.toString();
    }

}
