/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.impl;

import com.venedental.carga_masiva.base.Messages;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.load.AbstractLoader;
import com.venedental.carga_masiva.load.helper.FieldHelper;
import com.venedental.model.Patients;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class LoaderBasic extends AbstractLoader {

    @Override
    public void loadCustomFieldsPatient(Patients patient, String[] row, String[] fieldTemplate, List<MessageException> exceptions) {

    }

    @Override
    public List<Integer> readPlans(String filename, String sheetName, String[] row, String[] template, int index) throws MessageException {
        List<Integer> ids = new ArrayList<>();
        String value = FieldHelper.getString(template, index, row);
        if (value == null) {
            value = "";
        }
        String[] idPlans = value.split(",");
        for (String id : idPlans) {
            try {
                Integer idInt = Integer.valueOf(id.trim());
                ids.add(idInt);
            } catch (Exception e) {
                throw new MessageException(Messages.EXCEPTION_PLANS_INVALID);
            }
        }
        return ids;
    }

    @Override
    public Integer loadRelationship(String[] row, String[] template, int index) throws MessageException {
        try {
            return FieldHelper.getInteger(template, index, row);
        } catch (Exception e) {
            throw new MessageException(Messages.MESSAGE_RELATIONSHIP_INVALID);
        }
    }

}
