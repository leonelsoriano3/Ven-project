/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.base.enums;

/**
 *
 * @author usuario
 */
public enum EInsurances {

    HORIZONTE(4), HMO(3), UNIVERSAL(2), UNIVERSITAS(1);

    private final Integer id;

    private EInsurances(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
