/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.base;

/**
 *
 * @author usuario
 */
public class Messages {

    public static String ESTATUS_LOGGER_INVALIDO = "El estatus del logger es invalido, por favor revisar properties";

    public static String EXCEPTION_NON_EXISTENT_DIRECTORY = "El directorio actual no existe";
    public static String EXCEPTION_EMPTY_DIRECTORY = "No hay ficheros en el directorio especificado";
    public static String EXCEPTION_NOT_A_DIRECTORY = "No es un directorio";
    public static String EXCEPTION_NOT_A_EXCEL_FILE = "No es un archivo excel valido";
    public static String EXCEPTION_NOT_A_FILE = "No es un archivo";
    public static String EXCEPTION_READING_FILE = "Error al leer el archivo";

    public static String EXCEPTION_DATA_PATIENT_INVALID = "Los datos del paciente son incorrectos";
    public static String EXCEPTION_PLANS_INVALID = "Los planes son incorrectos";

    public static String MESSAGE_MERGED_PLANS = "Planes combinados";
    public static String MESSAGE_REPEATED_PLANS = "Planes repetidos";
    public static String MESSAGE_EMPTY_PLANS = "No tiene planes asociados";

    public static String MESSAGE_ID_INVALID = "Cedula del beneficiario no es valida";
    public static String MESSAGE_ID_HOLDER_INVALID = "Cedula del titular no es valida";
    public static String MESSAGE_BIRTH_INVALID = "Fecha de nacimiento no es valida";
    public static String MESSAGE_SEX_INVALID = "Inicial del sexo no valida";
    public static String MESSAGE_COMPLETE_NAME_INVALID = "Nombre completo no valido";
    public static String MESSAGE_RELATIONSHIP_INVALID = "Parentesco no valido";
    public static String MESSAGE_DATA_INVALID = "Los datos leidos en el registro son invalidos";

}
