/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.impl;

import com.venedental.carga_masiva.base.Messages;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.load.helper.FieldHelper;
import com.venedental.model.Patients;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class LoaderUniversal extends LoaderBasic {

    @Override
    public void loadCustomFieldsPatient(Patients patient, String[] row, String[] fieldTemplate, List<MessageException> exceptions) {
        try {
            if (loadRelationship(row, fieldTemplate, 2) == 10) {
                patient.setIdentityNumberHolder(patient.getIdentityNumber());
                MessageException select = null;
                for (MessageException m : exceptions) {
                    if (m.getError().equals(Messages.MESSAGE_ID_HOLDER_INVALID)) {
                        select = m;
                        break;
                    }
                }
                if (select != null) {
                    exceptions.remove(select);
                }
            }
        } catch (MessageException e) {

        }
    }

    @Override
    public Integer loadRelationship(String[] row, String[] template, int index) throws MessageException {
        try {
            Integer idRelationship = super.loadRelationship(row, template, index);
            return idRelationship;
        } catch (MessageException e) {
            String value = FieldHelper.getString(template, index, row);
            if (value.trim().toUpperCase().equals("TITULAR")) {
                return 10;
            }
            throw e;
        }
    }

    @Override
    public List<Integer> readPlans(String filename, String sheetName, String[] row, String[] field, int index) throws MessageException {
        List<Integer> ids = new ArrayList<>();
        String name = sheetName.replace(" ", "").replace("(", "").replace(")", "").toUpperCase();
        if (name.contains("PLANVENE")) {
            ids.add(9);
        } else if (name.contains("VENEPLUS")) {
            ids.add(10);
        } else if (name.contains("VENEEXTRA")) {
            ids.add(16);
        } else if (name.contains("VENE300")) {
            ids.add(11);
        } else if (name.contains("VENE1000")) {
            ids.add(12);
        } else if (name.contains("VENE2000")) {
            ids.add(13);
        }
        return ids;
    }

}
