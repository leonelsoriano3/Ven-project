/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.helper;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.dto.RowDTO;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.model.Insurances;
import com.venedental.model.InsurancesPatients;
import com.venedental.model.InsurancesPatientsPK;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import com.venedental.model.PlansPatients;
import com.venedental.model.PlansPatientsPK;
import com.venedental.model.Relationship;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class PersisterHelper {

    private static final Logger log = CustomLogger.getLogger(PersisterHelper.class.getName());

    private final Insurances insurance;
    private final Calendar startDate;
    private final Calendar endDate;
    private final ServiceFacadeDTO serviceFacadeDTO;

    public PersisterHelper(Insurances insurance, ServiceFacadeDTO serviceFacadeDTO) {
        this.insurance = insurance;
        this.serviceFacadeDTO = serviceFacadeDTO;
        Calendar currentDate = Calendar.getInstance();
        startDate = Calendar.getInstance();
        startDate.set(currentDate.get(Calendar.YEAR) - 1, Calendar.DECEMBER, 31, 23, 59, 59);
        startDate.set(Calendar.MILLISECOND, 999);
        endDate = Calendar.getInstance();
        endDate.set(currentDate.get(Calendar.YEAR), Calendar.DECEMBER, 31, 23, 59, 59);
        endDate.set(Calendar.MILLISECOND, 999);
    }

    public void persist(RowDTO row) {
        log.log(Level.INFO, "Inicio - Persistir Fila");
        if (!row.isRegisteredPatient()) {
            log.log(Level.INFO, "El paciente no se encuentra registrado: ", row.getLocation());
            savePatient(row.getPatient(), row.getRelationship());
        }
        Patients patient = findPatients(row);
        if (patient != null) {
            updateInsurancePatients(patient);
            updatePlansPatient(patient, row.getPlans());
        } else {
            log.log(Level.WARNING, "Error al guardar el paciente: ({0})", row.getLocation());
        }
        log.log(Level.INFO, "Fin - Persistir Fila");
    }

    private void savePatient(Patients patient, Relationship relationship) {
        log.log(Level.INFO, "Inicio - Registrando paciente");
        patient.setRelationshipId(relationship);
        serviceFacadeDTO.getPatientsFacadeLocal().create(patient);
        log.log(Level.INFO, "Fin - Registrando paciente");
    }

    private void updateInsurancePatients(Patients patient) {
        log.log(Level.INFO, "Inicio - Actualizar Aseguradora ({0}) x Paciente({1})", new Object[]{insurance.getId(), patient.getId()});
        InsurancesPatients insurancePatients = findInsurancePatients(patient);
        if (insurancePatients == null) {
            log.log(Level.INFO, "Inicio - Nuevo");
            insurancePatients = newInsurancePatients(patient);
            serviceFacadeDTO.getInsurancesPatientsFacadeLocal().create(insurancePatients);
            log.log(Level.INFO, "Fin - Nuevo");
        } else if (insurancePatients.getStatus() != 1) {
            log.log(Level.INFO, "Inicio - Editar");
            insurancePatients.setStatus(1);
            serviceFacadeDTO.getInsurancesPatientsFacadeLocal().edit(insurancePatients);
            log.log(Level.INFO, "Fin - Editar");
        }
        log.log(Level.INFO, "Fin - Actualizar Aseguradora x Paciente");
    }

    private InsurancesPatients findInsurancePatients(Patients patient) {
        List<InsurancesPatients> listInsurancesPatients = serviceFacadeDTO.getInsurancesPatientsFacadeLocal().findByInsurancesPatientsId(patient, insurance);
        if (listInsurancesPatients != null && !listInsurancesPatients.isEmpty()) {
            return listInsurancesPatients.get(0);
        }
        return null;
    }

    private InsurancesPatients newInsurancePatients(Patients patient) {
        InsurancesPatientsPK insurancesPatientsPK = new InsurancesPatientsPK();
        insurancesPatientsPK.setInsurancesId(insurance.getId());
        insurancesPatientsPK.setPatientsId(patient.getId());
        InsurancesPatients insurancePatient = new InsurancesPatients(insurancesPatientsPK);
        insurancePatient.setInsurances(insurance);
        insurancePatient.setPatients(patient);
        insurancePatient.setStatus(1);
        return insurancePatient;
    }

    private Patients findPatients(RowDTO row) {
        Patients patient = row.getPatient();
        if (row.isRegisteredPatient()) {
            return patient;
        }
        List<Patients> list = serviceFacadeDTO.getPatientsFacadeLocal().findByDuplicate(patient.getIdentityNumber(), patient.getIdentityNumberHolder(), patient.getDateOfBirth(), patient.getCompleteName());
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private void updatePlansPatient(Patients patient, List<Plans> plans) {
        log.log(Level.INFO, "Inicio - Actualizar Plan x Paciente ({0})", patient.getId());
        for (Plans plan : plans) {
            enablePlansPatient(patient, plan);
        }
        log.log(Level.INFO, "Inicio - Actualizar Plan x Paciente");
    }

    private void enablePlansPatient(Patients patient, Plans plan) {
        PlansPatients selectPlansPatients = null;
        if (patient.getPlansPatientsList() != null) {
            for (PlansPatients plansPatients : patient.getPlansPatientsList()) {
                if (((int) plansPatients.getPlansId().getId()) == ((int) plan.getId())) {
                    selectPlansPatients = plansPatients;
                    break;
                }
            }
        }
        if (selectPlansPatients == null) {
            log.log(Level.INFO, "Inicio - Nuevo: {0}", plan.getId());
            selectPlansPatients = newPlansPatient(plan, patient);
            createPlansPatient(selectPlansPatients);
            log.log(Level.INFO, "Fin - Nuevo");
        } else {
            log.log(Level.INFO, "Inicio - Editar: {0}", plan.getId());
            updatePlansPatient(selectPlansPatients);
            log.log(Level.WARNING, "Fin - Editar");
        }
    }

    private PlansPatients newPlansPatient(Plans plan, Patients patient) {
        PlansPatientsPK plansPatientsPK = new PlansPatientsPK();
        plansPatientsPK.setPlansId(plan.getId());
        plansPatientsPK.setPatientsId(patient.getId());
        PlansPatients plansPatients = new PlansPatients(plansPatientsPK);
        plansPatients.setPlansId(plan);
        plansPatients.setPatientsId(patient);
        plansPatients.setEffectiveDate(startDate.getTime());
        plansPatients.setDueDate(endDate.getTime());
        plansPatients.setStatus(1);
        return plansPatients;
    }

    private void createPlansPatient(PlansPatients selectPlansPatients) {
        serviceFacadeDTO.getPlansPatientsFacadeLocal().create(selectPlansPatients);
    }

    private void updatePlansPatient(PlansPatients planPatients) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_4);
        String sCurrentStartDate = format.format(planPatients.getEffectiveDate());
        String sCurrentEndDate = format.format(planPatients.getDueDate());
        String sStartDate = format.format(startDate.getTime());
        String sEndDate = format.format(endDate.getTime());
        if (planPatients.getStatus() != 1 || !sCurrentStartDate.equals(sStartDate) || !sCurrentEndDate.equals(sEndDate)) {
            planPatients.setEffectiveDate(startDate.getTime());
            planPatients.setDueDate(endDate.getTime());
            planPatients.setStatus(1);
            serviceFacadeDTO.getPlansPatientsFacadeLocal().edit(planPatients);
        }
    }

}
