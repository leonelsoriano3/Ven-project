/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.helper;

import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.Messages;
import com.venedental.carga_masiva.dto.RowDTO;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.model.Patients;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class ValidatorHelper {

    private static final Logger log = CustomLogger.getLogger(ValidatorHelper.class.getName());

    private final ServiceFacadeDTO serviceFacadeDTO;

    public ValidatorHelper(ServiceFacadeDTO serviceFacadeDTO) {
        this.serviceFacadeDTO = serviceFacadeDTO;
    }

    public void validate(RowDTO row) {
        log.log(Level.INFO, "Inicio - Validacion de fila");
        if (!row.hasError()) {
            validatePatient(row);
            if (!row.hasError()) {
                validatePlans(row);
            }
        }
        log.log(Level.INFO, "Fin - Validacion de fila");
    }

    private void validatePatient(RowDTO row) {
        log.log(Level.INFO, "Inicio - Validacion paciente");
        if (row.getPatient() != null) {
            log.log(Level.INFO, "Inicio - Buscar paciente");
            Patients patient = findPatients(row);
            if (patient != null) {
                row.setPatient(patient);
                row.setRegisteredPatient(true);
            }
            log.log(Level.INFO, "Fin - Buscar paciente");
        } else {
            row.setException(new MessageException(Messages.EXCEPTION_DATA_PATIENT_INVALID));
        }
        log.log(Level.INFO, "Fin - Validacion paciente");
    }

    private Patients findPatients(RowDTO row) {
        Patients patient = row.getPatient();
        List<Patients> list = serviceFacadeDTO.getPatientsFacadeLocal().findByDuplicate(patient.getIdentityNumber(), patient.getIdentityNumberHolder(), patient.getDateOfBirth(), patient.getCompleteName());
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private void validatePlans(RowDTO row) {
        log.log(Level.INFO, "Inicio - Validacion planes");
        if (row.getPlans().isEmpty()) {
            row.setException(new MessageException(Messages.MESSAGE_EMPTY_PLANS));
        }
        log.log(Level.INFO, "Fin - Validacion planes");
    }

}
