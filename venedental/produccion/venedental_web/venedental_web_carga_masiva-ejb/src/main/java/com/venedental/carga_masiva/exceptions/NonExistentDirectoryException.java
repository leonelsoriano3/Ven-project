/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import com.venedental.carga_masiva.base.Messages;
import java.io.File;

/**
 *
 * @author usuario
 */
public class NonExistentDirectoryException extends FileException {

    public NonExistentDirectoryException(File file) {
        super(Messages.EXCEPTION_NON_EXISTENT_DIRECTORY, file);
    }

    public NonExistentDirectoryException(File file, Throwable throwable) {
        super(Messages.EXCEPTION_NON_EXISTENT_DIRECTORY, file, throwable);
    }

}
