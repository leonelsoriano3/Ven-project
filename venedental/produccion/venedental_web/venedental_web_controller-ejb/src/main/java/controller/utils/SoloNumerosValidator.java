/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Usuario
 */
@FacesValidator("SoloNumerosValidator")
public class SoloNumerosValidator implements Validator {

    private static final String NUMEROS_PATRON = "^[0123456789]*$";
    private final Pattern pattern;
    private Matcher matcher;

    public SoloNumerosValidator() {
        pattern = Pattern.compile(NUMEROS_PATRON);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null && !value.toString().isEmpty()) {
            matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ((UIInput) component).getValidatorMessage(), "Error");
                FacesContext.getCurrentInstance().addMessage(component.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }
    }
}
