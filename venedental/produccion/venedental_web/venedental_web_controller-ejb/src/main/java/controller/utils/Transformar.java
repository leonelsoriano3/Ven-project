package controller.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Transformar {
    public static Date StringToDate(String string, String format) throws ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(string);
    }
    
    public static Date StringToDate(String string) throws ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss");
        return formatter.parse(string);
    }
    
    public static String getThisDate() {
        Date fecha = new Date();
        SimpleDateFormat sdfClave = new SimpleDateFormat("yyyyMMdd");
        String fechaClave = sdfClave.format(fecha);
        return fechaClave;
    }
    
    public static Date currentDate() throws ParseException{
        Date fecha = new Date();
        SimpleDateFormat sdfClave = new SimpleDateFormat("yyyyMMdd");
        String fechaClave = sdfClave.format(fecha);
        Date fechaActual = sdfClave.parse(fechaClave);
        return fechaActual;
    }
    
    
    public static Date starDate(Date stardate) throws ParseException{
        String fechaInicialStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(stardate);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date fechaInicial = sdf2.parse(fechaInicialStr);
        return fechaInicial;
    }
    
    public static Date endDate(Date endDate) throws ParseException{
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        String fechaStr = formatter.format(endDate);
        Date date = formatter.parse(fechaStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date fechaFinalTransformada = cal.getTime();
        return fechaFinalTransformada;
    }
    
}
