package controller.utils;

import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Encriptar {

    private static final Logger log = CustomLogger.getLogger(Encriptar.class.getName());

    public static String md5(String clave) throws NoSuchAlgorithmException {
        java.security.MessageDigest md = java.security.MessageDigest
                .getInstance("MD5");
        byte[] array = md.digest(clave.getBytes());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(
                    1, 3));
        }
        log.log(Level.INFO, "Clave sin encriptar {0}", clave);
        log.log(Level.INFO, "Clave encriptada {0}", sb.toString());
        return sb.toString();
    }

}
