/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.utils;

import com.venedental.massiveLoading.base.Constants;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class CustomLogger {

    private static Logger logger;

    public static Logger getLogger(String name) {
        logger = Logger.getLogger(name);
        if (PropertiesConfiguration.get(Constants.PROPERTY_STATUS_LOGGER).compareTo(Constants.STATUS_OFF) == 0) {
            logger.setLevel(Level.OFF);
        } else {
            logger.setLevel(Level.ALL);
            if (PropertiesConfiguration.get(Constants.PROPERTY_STATUS_LOGGER).compareTo(Constants.STATUS_ON) != 0) {
                logger.log(Level.WARNING, "ERROR EN EL ESTATUS DEL LOGGER");
            }
        }
        return logger;
    }

}
