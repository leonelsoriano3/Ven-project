/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.utils;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.faces.context.FacesContext;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Usuario
 */
public class EnviarCorreo {

    private static String username, password;
    private static Properties properties;
    private static Session session;
    private static EnviarCorreo correo;
    private static final Logger log = CustomLogger.getGeneralLogger(EnviarCorreo.class.getName());

    private static void setup() throws MessagingException {
        username = PropertiesLocator.getProperty("mail.username");
        password = PropertiesLocator.getProperty("mail.password");

        properties = new Properties();
        properties.put("mail.smtp.auth", PropertiesLocator.getProperty("mail.smtp.auth"));
        properties.put("mail.smtp.starttls.enable", PropertiesLocator.getProperty("mail.smtp.starttls.enable"));
        properties.put("mail.smtp.host", PropertiesLocator.getProperty("mail.smtp.host"));
        properties.put("mail.smtp.port", PropertiesLocator.getProperty("mail.smtp.port"));
        session = crearSesion();
    }

    private static Session crearSesion() {
        Session nuevaSession = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        nuevaSession.setDebug(true);
        return nuevaSession;
    }

    public static boolean enviar(String realPath, String asunto, String texto, boolean isHTML, String... listaDestinatarios) {
        try {
            if (session == null) {
                setup();
            }
            MimeMessage mensaje = new MimeMessage(session);
            for (String destinatario : listaDestinatarios) {
                log.log(Level.INFO, "ENVIANDO CORREO A: {0}", destinatario);
                mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            }
            mensaje.setSubject(asunto);

            BodyPart parteHtmlMensaje = new MimeBodyPart();
            String htmlText = texto;
            parteHtmlMensaje.setContent(htmlText, "text/html");
            StringBuilder builder = new StringBuilder();
            builder.append(realPath);
            builder.append("resources/img/heal_logo2.png");
            File imagen = new File(builder.toString());
            DataSource fuenteImagen = new FileDataSource(imagen);
            BodyPart parteHtmlImagen = new MimeBodyPart();
            parteHtmlImagen.setDataHandler(new DataHandler(fuenteImagen));
            parteHtmlImagen.setHeader("Content-ID", "<image>");

            MimeMultipart multiPartesMensaje = new MimeMultipart("related");
            multiPartesMensaje.addBodyPart(parteHtmlMensaje);
            multiPartesMensaje.addBodyPart(parteHtmlImagen);

            mensaje.setContent(multiPartesMensaje);
            Transport.send(mensaje);
        } catch (MessagingException ex) {
            log.log(Level.SEVERE, "ERROR ENVIANDO EL CORREO");
            log.log(Level.SEVERE, null, ex);
            return false;
        }
        log.log(Level.INFO, "CORREO ENVIADO EXITOSAMENTE");
        return true;
    }

    public static String getUsername() {
        if (session == null) {
            try {
                setup();
            } catch (MessagingException ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
        return username;
    }

}
