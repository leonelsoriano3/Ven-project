/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Usuario
 */
@FacesValidator("SoloLetrasValidator")
public class SoloLetrasValidator implements Validator {

    private static final String LETRAS_PATRON = "^[a-zA-Z \\'ñÑáÁéÉíÍóÓúÚüÜ]+$";
    private final Pattern pattern;
    private Matcher matcher;

    public SoloLetrasValidator() {
        pattern = Pattern.compile(LETRAS_PATRON);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null && !value.toString().isEmpty()) {
            matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ((UIInput) component).getValidatorMessage(), "Error");
                FacesContext.getCurrentInstance().addMessage(component.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }
    }

}
