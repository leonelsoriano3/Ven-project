/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services.utils;

import com.venedental.enums.Reemplazo;

/**
 *
 * @author Usuario
 */
public class EncodeString {

    public static String reemplazarString(String valor, Reemplazo... reemplazos) {
        String nuevo;
        nuevo = "";
        if(valor != null){
         nuevo = valor.toUpperCase();
        for (Reemplazo reemplazo : reemplazos) {
            nuevo = nuevo.replace(reemplazo.getValor(), reemplazo.getValorReemplazo());
        }
        }
        return nuevo;
    }
    
}
