/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import com.venedental.carga_masiva.base.Messages;
import java.io.File;

/**
 *
 * @author usuario
 */
public class NotExcelFileException extends FileException {

    public NotExcelFileException(File file) {
        super(Messages.EXCEPTION_NOT_A_EXCEL_FILE, file);
    }

    public NotExcelFileException(File file, Throwable throwable) {
        super(Messages.EXCEPTION_NOT_A_EXCEL_FILE, file, throwable);
    }

}
