/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.read;

import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.load.AbstractLoader;

/**
 *
 * @author usuario
 */
public class RowReader {

    private final AbstractLoader loader;

    public RowReader(AbstractLoader loader) {
        this.loader = loader;
    }

    public void execute(String filename, String sheetName, long numRow, String[] row, String[] fieldTemplate) throws MessageException {
        loader.processRow(filename, sheetName, numRow, row, fieldTemplate);
    }

}
