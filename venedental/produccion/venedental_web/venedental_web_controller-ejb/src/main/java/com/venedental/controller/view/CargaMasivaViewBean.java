/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.carga_masiva.ejb.MassiveLoadProgrammerLocal;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.controller.BaseBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "cargaMasivaViewBean")
@ViewScoped
public class CargaMasivaViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;
    private static final Logger log = CustomLogger.getGeneralLogger(CargaMasivaViewBean.class.getName());

    private String user;
    private String password;

    @EJB
    private MassiveLoadProgrammerLocal massiveLoadProgrammerLocal;

    public CargaMasivaViewBean() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void cargaManual() throws MessageException {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        if (massiveLoadProgrammerLocal.execute()) {
            contexto.addMessage(null, new FacesMessage("Info", "Proceso de carga masiva en ejecución. Al finalizar se le notificará vía correo electrónico."));
            requestContext.update("form:growMassiveLoad");
        } else {
            contexto.addMessage(null, new FacesMessage("Error", "Ya se encuentra un proceso de carga masiva en ejecución."));
            requestContext.update("form:growMassiveLoad");
        }
    }

}
