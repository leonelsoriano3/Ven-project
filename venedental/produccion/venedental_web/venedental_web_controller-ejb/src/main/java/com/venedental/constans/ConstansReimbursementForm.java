/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.constans;

/**
 *
 * @author Anthony Torres
 */
public class ConstansReimbursementForm {
    
    // PATHS FILES RESOURCES
    public static String PROPERTIES_PATH = "src/main/resources/";
    
    // ENVIRONMENT VARIABLE REIMBURSEMENT
    public static String FILENAME_REIMBURSEMENT_REQUEST = "ReimbursementForm.properties";
    
    // --> DIRECTORY PATH
    public static String PROPERTY_PATH_REPORT = "directory.path";
    
    // --> DIRECTOTY TEMP PATH
    public static String PROPERTY_PATH_TMP = "directorytemp.path";
    
    // --> DIRECTORY PATH JASPER
    public static String PROPERTY_PATH_JASPER = "directory.pathJasper";
    
    // --> DIRECTORY PATH HOLDER'S JASPER
    public static String PROPERTY_PATH_JASPER_HOLDER = "directory.pathJasperHolder";
    
    public static String PROPERTY_PATH_JASPER_2 = "directory.pathJasper2";
    
    // --> LOGO
    public static String PROPERTY_PATH_LOGO = "directory.pathLogo";
    
    // --> REPORT EXTENTION
    public static String PROPERTY_REPORT_EXTENSION = "report.extention";
    
    // --> IMAGE TITLES
    public static String PROPERTY_HOLDER_IMAGE_TITLE = "report.identityNumberHolderImageTitle";
    
    public static String PROPERTY_BENEFICIARY_IMAGE_TITLE = "report.identityNumberBeneficiaryImageTitle";
    
    public static String PROPERTY_MEDICAL_REPORT_IMAGE_TITLE = "report.medicalReportImageTitle";
    
    public static String PROPERTY_OPTICAL_FORMULA_IMAGE_TITLE = "report.opticalFormulaImageTitle";
    
    public static String PROPERTY_INVOICE_REPORT_IMAGE_TITLE = "report.invoiceReportImageTitle";
    
    // --> USERS
    public static String PROPERTY_ADMIN_VENEDEN = "admin.veneden";
    
    // --> CONTENT APPROVE REIMBURSEMENT
    public static String PROPERTY_EMAIL_CONTENT_REQUEST = "email.messageContentByRequest";
    
    // --> CONTENT REJECT REIMBURSEMENT
    public static String PROPERTY_EMAIL_CONTENT_REQUEST2 = "email.messageContentByRequest2";
    
    // --> TITLE MESSAGE
    public static String PROPERTY_EMAIL_TITLE = "email.messageTitle";
    
    // --> NAME USERS
    public static String PROPERTY_NAME_USER = "nameUser";
    
    public static String PROPERTY_ANALIST_MAIL = "analist.mail";
    
    public static String PROPERTY_ANALIST_MAIL_CONTENT = "email.messageAnalistContent";
    
    public static String PROPERTY_ANALIST_MAIL_TITLE = "email.messageAnalistTitle";
    
    public static String PROPERTY_EMAIL_CONTENT_GREETINGS = "email.messageGreetings";
    
    public static String PROPERTY_EMAIL_CONTENT_FIRM = "email.messageFirm";
    
    public static String PROPERTY_EMAIL_CONTENT_SUBTITLE = "emai.messageSubtitle";
    
}
