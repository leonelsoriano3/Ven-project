package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.AddressesViewBean;
import com.venedental.controller.view.CitiesViewBean;
import com.venedental.controller.view.ClinicsViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.PhoneClinicViewBean;
import com.venedental.controller.view.StateViewBean;
import com.venedental.enums.TipoClinica;
import com.venedental.facade.CitiesFacadeLocal;
import com.venedental.facade.ClinicsFacadeLocal;
import com.venedental.model.Cities;
import com.venedental.model.Clinics;
import com.venedental.model.PhoneClinic;
import com.venedental.model.State;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

@ManagedBean(name = "citiesRequestBean")
@RequestScoped
public class CitiesRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(CitiesRequestBean.class.getName());

    @EJB
    private CitiesFacadeLocal citiesFacadeLocal;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    
   
    public void resetClinics(String dialogo) {
        RequestContext.getCurrentInstance().reset("form:" + dialogo);
    }

    

    public void prepareNewClinics() {
        
        this.getStateViewBean().setNewState(new State());
        this.getCitiesViewBean().setNewCities(new Cities());
        this.getCitiesViewBean().getListCities().clear();
        this.getPhoneClinicViewBean().getNewListPhoneClinic().clear();
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }


    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

}
