
package com.venedental.constans;

public class ConstansReimbursement {

    // PATHS FILES RESOURCES
    public static String PROPERTIES_PATH = "src/main/resources/";
    
    // ENVIRONMENT VARIABLE REIMBURSEMENT
    public static String FILENAME_REIMBURSEMENT_REPORT = "Reimbursement.properties";
    
    // --> LOGO
    public static String PROPERTY_PATH_LOGO = "directory.pathLogo";
    
    // --> USERS
    public static String PROPERTY_ADMIN_VENEDEN = "admin.veneden";
    
    // --> CONTENT APPROVE REIMBURSEMENT
    public static String PROPERTY_EMAIL_CONTENT_APPROVE = "email.messageContentByApprove";
    
    // --> CONTENT REJECT REIMBURSEMENT
    public static String PROPERTY_EMAIL_CONTENT_REJECT = "email.messageContentByReject";
    
    // --> TITLE MESSAGE
    public static String PROPERTY_EMAIL_TITLE = "email.messageTitle";
    // --> NAME USERS
    public static String PROPERTY_NAME_USER = "nameUser";
    
    // --> PATH SERVER FTP FILES
    public static String PROPERTY_PATH_FTP = "path.serverFTP";


}

