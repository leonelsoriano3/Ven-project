package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.model.Addresses;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "addressesViewBean")
@ViewScoped
public class AddressesViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    public AddressesViewBean() {
        this.editAddresses = new Addresses();
        this.newAddresses = new Addresses();
        this.newClinicAddresses = new Addresses();
    }

    private Addresses selectedAddresses;

    private List<Addresses> listaAddresses;

    private List<Addresses> listaAddressesFiltrada;

    private Addresses editAddresses;

    private Addresses newAddresses;
    
    private Addresses newClinicAddresses;

    public Addresses getNewAddresses() {
        return newAddresses;
    }

    public void setNewAddresses(Addresses newAddresses) {
        this.newAddresses = newAddresses;
    }

    public Addresses getEditAddresses() {
        return editAddresses;
    }

    public void setEditAddresses(Addresses editAddresses) {
        this.editAddresses = editAddresses;
    }

    public Addresses getSelectedAddresses() {
        return selectedAddresses;
    }

    public Addresses getNewClinicAddresses() {
        return newClinicAddresses;
    }

    public void setNewClinicAddresses(Addresses newClinicAddresses) {
        this.newClinicAddresses = newClinicAddresses;
    }

    public void setSelectedAddresses(Addresses selectedAddresses) {
        this.selectedAddresses = selectedAddresses;
    }

    public List<Addresses> getListaAddressesFiltrada() {
        if (this.listaAddressesFiltrada == null) {
            this.listaAddressesFiltrada = new ArrayList<>();
        }
        return listaAddressesFiltrada;
    }

    public void setListaAddressesFiltrada(List<Addresses> listaAddressesFiltrada) {
        this.listaAddressesFiltrada = listaAddressesFiltrada;
    }

    public List<Addresses> getListaAddresses() {
        if (this.listaAddresses == null) {
            this.listaAddresses = new ArrayList<>();
        }
        return listaAddresses;
    }

    public void setListaAddresses(List<Addresses> listaAddresses) {
        this.listaAddresses = listaAddresses;
    }

    public List<Addresses> getFilteredAddresses() {
        return null;
    }

}
