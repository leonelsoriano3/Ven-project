/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.constans;

/**
 *
 * @author usuario
 */
public class ConstansPaymentRefundReport {
    
    public static String PROPERTIES_PATH = "src/main/resources/";
    public static String FILENAME_PAYMENT_REPORT = "paymentRefundReport.properties";
    public static String PROPERTY_PATH_LOGO = "directory.pathLogo";
    public static String PROPERTY_ADMIN_VENEDEN = "admin.veneden";
    public static String PROPERTY_EMAIL_CONTENT = "email.messageContent";
    public static String PROPERTY_EMAIL_CONTENT_WITH_ANNEX = "email.messageContentAnnex";
    public static String PROPERTY_EMAIL_CONTENT_WITH_ANNEX_TWO = "email.messageContentAnnexTwo";
    public static String PROPERTY_EMAIL_EXITO_ADMIN_CONTENT = "email.adminContentExito";
    public static String PROPERTY_MESSAGE_TITLE = "email.messageTitle";
    public static String PROPERTY_MESSAGE_TITLE2 = "email.messageTitle2";
    public static String PROPERTY_MESSAGE_TITLE3 = "email.messageTitle3";
    public static String PROPERTY_PROCESSING_PAYMENT = "email.processingPayment";
    public static String PROPERTY_DEAR_PATIENS = "email.dearPatiens";
    public static String PROPERTY_BETTER_SERVICE = "email.betterServices";
    public static String PROPERTY_NAME_USER = "nameUser";
    
}
