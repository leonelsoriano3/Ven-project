package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.RolUser;
import com.venedental.facade.MenuFacadeLocal;
import com.venedental.facade.MenuRolFacadeLocal;
import com.venedental.facade.MenuRolOperationFacadeLocal;
import com.venedental.facade.RolUsersFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.model.security.Menu;
import com.venedental.model.security.MenuRol;
import com.venedental.model.security.MenuRolOperation;
import com.venedental.model.security.Rol;
import com.venedental.model.security.RolUsers;
import com.venedental.model.security.Users;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@ManagedBean(name = "securityViewBean")
@ViewScoped
public class SecurityViewBean extends BaseBean {

    @EJB
    private UsersFacadeLocal usersFacadeLocal;

    @EJB
    private RolUsersFacadeLocal rolUsersFacadeLocal;

    @EJB
    private MenuRolFacadeLocal menuRolFacadeLocal;

    @EJB
    private MenuFacadeLocal menuFacadeLocal;

    @EJB
    private MenuRolOperationFacadeLocal menuRolOperationFacadeLocal;

    public static Boolean rolSpecialist;

    

    public UsersFacadeLocal getUsersFacadeLocal() {
        return usersFacadeLocal;
    }

    public void setUsersFacadeLocal(UsersFacadeLocal usersFacadeLocal) {
        this.usersFacadeLocal = usersFacadeLocal;
    }

    public RolUsersFacadeLocal getRolUsersFacadeLocal() {
        return rolUsersFacadeLocal;
    }

    public void setRolUsersFacadeLocal(RolUsersFacadeLocal rolUsersFacadeLocal) {
        this.rolUsersFacadeLocal = rolUsersFacadeLocal;
    }

    public MenuRolFacadeLocal getMenuRolFacadeLocal() {
        return menuRolFacadeLocal;
    }

    public void setMenuRolFacadeLocal(MenuRolFacadeLocal menuRolFacadeLocal) {
        this.menuRolFacadeLocal = menuRolFacadeLocal;
    }

    public MenuFacadeLocal getMenuFacadeLocal() {
        return menuFacadeLocal;
    }

    public void setMenuFacadeLocal(MenuFacadeLocal menuFacadeLocal) {
        this.menuFacadeLocal = menuFacadeLocal;
    }

    public MenuRolOperationFacadeLocal getMenuRolOperationFacadeLocal() {
        return menuRolOperationFacadeLocal;
    }

    public void setMenuRolOperationFacadeLocal(MenuRolOperationFacadeLocal menuRolOperationFacadeLocal) {
        this.menuRolOperationFacadeLocal = menuRolOperationFacadeLocal;
    }

    public static Boolean getRolSpecialist() {
        return rolSpecialist;
    }

    public static void setRolSpecialist(Boolean rolSpecialist) {
        SecurityViewBean.rolSpecialist = rolSpecialist;
    }

    /**
     * Metodo que permite captar el nombre del usuario que se encuentra en
     * sesion, obteniendolo a traves de la autentificacion del mismo
     */
    public Users obtainUser() {

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());

        if (user != null) {
            return user;
        } else {
            return null;
        }
    }

    /**
     * Metodo que permite captar el nombre del usuario que se encuentra en
     * sesion, obteniendolo a traves de la autentificacion del mismo
     */
    public String obtainNameUser() {
        /*#{request.userPrincipal.name}
         Notación para obtener el login del usuario logueado  
         */
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());

        if (user != null) {
            return user.getName();
        } else {
            return null;
        }

    }

    /**
     * Metodo que permite captar los roles del usuario que se encuentra en
     * sesion, obteniendolo a traves de la autentificacion del mismo
     */
    public List<Rol> obtainRolUser() {

        rolSpecialist = false;
        List<Rol> listRol = new ArrayList<>();
        List<RolUsers> listRolUsers = new ArrayList<>();

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());

        if (user != null) {

            listRolUsers = this.rolUsersFacadeLocal.findByUserId(user.getId());

            if (!listRolUsers.isEmpty()) {
                for (int i = 0; i < listRolUsers.size(); i++) {
                    Rol rol = listRolUsers.get(i).getRol();

                    if (rol.getId() == 8) {

                        rolSpecialist = true;

                    }

                    listRol.add(rol);
                }
            }

        }

        return listRol;

    }
    
    
     /**
     * Metodo que permite captar los roles del usuario que se encuentra en
     * sesion, obteniendolo a traves de la autentificacion del mismo retorna true si es especialista 
     * @return rolSpecialist
     */
    public Boolean obtainRolSpecialist() {

        rolSpecialist = false;
        List<Rol> listRol = new ArrayList<>();
        List<RolUsers> listRolUsers = new ArrayList<>();

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());

        if (user != null) {
            RolUsers rolUser = this.rolUsersFacadeLocal.findRolUserByUserRol(user.getId(), RolUser.ROLE_ESPECIALISTA.getValor());
            if (rolUser != null) {

                rolSpecialist = true;
            }
        }

        return rolSpecialist;

    }

    

    public List<MenuRol> obtainMenuByOperations() {

        Boolean menuEncontrado = false;
        List<Rol> listRol = new ArrayList<>();
        List<MenuRol> listMenuRol = new ArrayList<>();
        List<MenuRol> listMenuRolReturn = new ArrayList<>();
        List<Menu> listMenu = new ArrayList<>();

        listRol = obtainRolUser();

        if (!listRol.isEmpty()) {
            for (int i = 0; i < listRol.size(); i++) {
                int rolId = listRol.get(i).getId();

                listMenuRol = this.menuRolFacadeLocal.findByRolId(rolId);

                if (!listMenuRol.isEmpty()) {

                    for (int j = 0; j < listMenuRol.size(); j++) {

                        Menu menu = listMenuRol.get(j).getMenuId();
                        Rol rol = listMenuRol.get(j).getRolId();

                        for (int k = 0; k < listMenuRolReturn.size(); k++) {

                            if (Objects.equals(menu.getId(), listMenuRolReturn.get(k).getMenuId().getId()) && Objects.equals(rol.getId(), listMenuRolReturn.get(k).getRolId().getId())) {
                                menuEncontrado = true;
                            }

                        }

                        if (menuEncontrado == false) {
                            listMenuRolReturn.add(listMenuRol.get(j));
                        }

                    }
                }

            }
        }

        return listMenuRolReturn;

    }

    public List<MenuRolOperation> obtainMenuRolOperations(int menuRolId) {

        List<MenuRolOperation> listMenuRolOperation = new ArrayList<>();
        listMenuRolOperation = this.menuRolOperationFacadeLocal.findByMenuRolId(menuRolId);
        return listMenuRolOperation;

    }

    /**
     * Metodo que permita filtrar el menu de a cuerdo a el o los roles del
     * usuario que se encuentra en sesion, obteniendolo a traves de la
     * autentificacion del mismo
     * @return 
     */
    public List<Menu> obtainMenuByRol() {

        Boolean menuEncontrado = false;
        List<Rol> listRol = new ArrayList<>();
        List<MenuRol> listMenuRol = new ArrayList<>();
        List<Menu> listMenu = new ArrayList<>();

        listRol = obtainRolUser();

        if (!listRol.isEmpty()) {
            for (int i = 0; i < listRol.size(); i++) {
                int rolId = listRol.get(i).getId();

                listMenuRol = this.menuRolFacadeLocal.findByRolId(rolId);

                if (!listMenuRol.isEmpty()) {

                    for (int j = 0; j < listMenuRol.size(); j++) {

                        Menu menu = listMenuRol.get(j).getMenuId();

                        for (int k = 0; k < listMenu.size(); k++) {

                            if (menu.getId() == listMenu.get(k).getId()) {
                                menuEncontrado = true;
                            }

                        }

                        if (menuEncontrado == false) {
                            listMenu.add(menu);
                        }

                    }
                }
            }
        }

        return listMenu;

    }

    public void encriptar() {

        String password1 = "gpe428";
        String password2 = "sto468";
        String password3 = "rhu044";




        //yar744    ysa993        rhu044
      //  String password1 = "rsa321";
//        String password3 = "17168727";
//        String password4 = "AK@dmin518";
        //String password5 = "";
        // String password6 = "";

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword1 = passwordEncoder.encode(password1);
        String hashedPassword2 = passwordEncoder.encode(password2);
//        String hashedPassword3 = passwordEncoder.encode(password3);
//        String hashedPassword4 = passwordEncoder.encode(password4);
        // String hashedPassword5 = passwordEncoder.encode(password5);
        //String hashedPassword6 = passwordEncoder.encode(password6);

//       System.out.println("Contraseña encriptada ndp2008:" + hashedPassword1);
//        System.out.println("Contraseña encriptada ysa993:" + hashedPassword2);
//        System.out.println("Contraseña encriptada rhu044:" + hashedPassword3);
//        System.out.println("Mi contra es" + hashedPassword4);
        // System.out.println("Contraseña encriptada:" + hashedPassword5);
        // System.out.println("Contraseña encriptada:" + hashedPassword6);

    }

}
