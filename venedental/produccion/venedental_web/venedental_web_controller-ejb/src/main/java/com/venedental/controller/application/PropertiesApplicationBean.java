/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.controller.application;


import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.PlansTreatments;
import com.venedental.model.PropertiesTreatments;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "propertiesApplicationBean")
@ApplicationScoped
public class PropertiesApplicationBean  {
    
    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;
    
    private List<PropertiesTreatments> listProperties;
    
    public PropertiesApplicationBean() {
    }
    
    @PostConstruct
    public void init() {
        this.getListProperties().addAll(this.propertiesTreatmentsFacadeLocal.findAll());
        
    }
    
    public List<PropertiesTreatments> getListProperties() {
        if (this.listProperties == null) {
            this.listProperties = new ArrayList<>();
        }
        
        return listProperties;
    }
    
    public void setListProperties(List<PropertiesTreatments> listProperties) {
        this.listProperties = listProperties;
    }
    
    
    
    
}
