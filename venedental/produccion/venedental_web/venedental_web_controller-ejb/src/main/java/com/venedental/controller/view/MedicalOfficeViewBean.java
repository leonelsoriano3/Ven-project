package com.venedental.controller.view;



import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import com.venedental.model.MedicalOffice;
import com.venedental.model.SpecialistJob;
import org.primefaces.context.RequestContext;


@ManagedBean(name = "medicalOfficeViewBean")
@ViewScoped
public class MedicalOfficeViewBean extends BaseBean {


    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;
    
    
    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;

    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;

    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;

   @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    
    
    private static final long serialVersionUID = -5547104134772667835L;

    public MedicalOfficeViewBean() {
        this.newMedicalOffice = new String[7];
        this.editMedicalOffice = new MedicalOffice();
        this.addMedicalOffice = new MedicalOffice();
        this.tipoRifEdit = "V";
        this.mascaraRifEdit = "V999999999";
        this.visible = false;
    }

    @PostConstruct
    public void init()  {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
//          try {
//           
//          
//             this.listaMedicalOffice.addAll((List<MedicalOffice>) this.getProvider().getObjeto(client.getCliente(), Servicios.MEDICALOFFICE, ""));
//            
//           
//        } catch (JsonMappingException e) {
//        } catch (IOException e) {
//        } catch (PersonalHttpException ex) {
//            this.getErrorHttpSessionBean().updateError(ex);
//        }
        
    }


    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    
    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    private MedicalOffice selectedMedicalOffice;

    private List<MedicalOffice> listaMedicalOffice;

    private List<MedicalOffice> listaMedicalOfficeFiltrada;

    private List<SpecialistJob> listaMedicalOfficeSpecialist;

    private MedicalOffice newMedicalOfficeObj;
    
    private String[] newMedicalOffice;

    private MedicalOffice editMedicalOffice;

    private MedicalOffice addMedicalOffice;

    private String tipoRifEdit;

    private String mascaraRifEdit;

    private MedicalOffice beforeEditMedicalOffice;
    
    private boolean visible;
    

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public MedicalOffice getAddMedicalOffice() {
        return addMedicalOffice;
    }

    public void setAddMedicalOffice(MedicalOffice addMedicalOffice) {
        this.addMedicalOffice = addMedicalOffice;
    }

    public MedicalOffice getEditMedicalOffice() {
        return editMedicalOffice;
    }

    public void setEditMedicalOffice(MedicalOffice editMedicalOffice) {
        this.editMedicalOffice = editMedicalOffice;
    }

    public String[] getNewMedicalOffice() {
        return newMedicalOffice;
    }

    public void setNewMedicalOffice(String[]  newMedicalOffice) {
        this.newMedicalOffice = newMedicalOffice;
    }

    public MedicalOffice getSelectedMedicalOffice() {
        return selectedMedicalOffice;
    }

    public void setSelectedMedicalOffice(MedicalOffice selectedMedicalOffice) {
        this.selectedMedicalOffice = selectedMedicalOffice;
        this.setEditMedicalOffice(selectedMedicalOffice);
        this.getAddressesViewBean().setEditAddresses(selectedMedicalOffice.getAddressId());
        this.getCitiesViewBean().setEditCities(selectedMedicalOffice.getAddressId().getCityId());
        this.getStateViewBean().setEditState(selectedMedicalOffice.getAddressId().getCityId().getStateId());
//        this.getPhoneClinicViewBean().setEditListPhoneClinic(selectedMedicalOffice.getPhoneClinicList());
    }

    public List<MedicalOffice> getListaMedicalOffice()  {
        
        if (this.listaMedicalOffice == null) {
            this.listaMedicalOffice = new ArrayList<>();
        }
        //Collections.sort(this.listaMedicalOffice, new Clinics().compareToEstadoYCiudad());
        return this.listaMedicalOffice;
    }

    public void setListaMedicalOffice(List<MedicalOffice> listaMedicalOffice) {
        this.listaMedicalOffice = listaMedicalOffice;
    }

    public List<SpecialistJob> getListaMedicalOfficeSpecialist() {
        if (this.listaMedicalOfficeSpecialist == null) {
            this.listaMedicalOfficeSpecialist = new ArrayList<>();
        }
        return listaMedicalOfficeSpecialist;
    }

    public void setListaMedicalOfficeSpecialist(List<SpecialistJob> listaMedicalOfficeSpecialist) {
        this.listaMedicalOfficeSpecialist = listaMedicalOfficeSpecialist;
    }

    public List<MedicalOffice> getListaMedicalOfficeFiltrada() {
        return listaMedicalOfficeFiltrada;
    }

    public void setListaMedicalOfficeFiltrada(List<MedicalOffice> listaMedicalOfficeFiltrada) {
        this.listaMedicalOfficeFiltrada = listaMedicalOfficeFiltrada;
    }

    public List<MedicalOffice> autoCompleteMedicalOffice(String query) throws IOException {
        List<MedicalOffice> suggestions = new ArrayList<>();

        for (MedicalOffice p : this.getListaMedicalOffice()) {
            if (p.getName().toUpperCase().startsWith(query.toUpperCase())) {
                suggestions.add(p);
            }
        }
        return suggestions;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public String getTipoRifEdit() {
        return tipoRifEdit;
    }

    public void setTipoRifEdit(String tipoRifEdit) {
        this.tipoRifEdit = tipoRifEdit;
    }

    public String getMascaraRifEdit() {
        return mascaraRifEdit;
    }

    public void setMascaraRifEdit(String mascaraRifEdit) {
        this.mascaraRifEdit = mascaraRifEdit;
    }

    public MedicalOffice getBeforeEditMedicalOffice() {
        return beforeEditMedicalOffice;
    }

    public void setBeforeEditMedicalOffice(MedicalOffice beforeEditMedicalOffice) throws CloneNotSupportedException {
        this.beforeEditMedicalOffice = (MedicalOffice) beforeEditMedicalOffice.clone();
    }

   

    public void limpiarObjetosEdit() {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        this.getEditMedicalOffice().setName(this.getBeforeEditMedicalOffice().getName());
        this.getEditMedicalOffice().setRif(this.getBeforeEditMedicalOffice().getRif());
      
    }

    public void limpiarObjetosCreate() {
        this.getComponenteSessionBean().setCamposRequeridosCreate(false);
    }


    public void mostrarComponente(){
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }
    
    public void addMessage() {
        String summary = visible ? "Checked" : "Unchecked";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }
}
