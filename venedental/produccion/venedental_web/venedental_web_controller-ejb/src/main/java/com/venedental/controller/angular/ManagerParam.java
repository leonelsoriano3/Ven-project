package com.venedental.controller.angular;

import com.google.gson.Gson;
import com.venedental.controller.angular.until.WrapperParam;
import com.venedental.dto.User.UserDTO;
import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.services.ServiceCreateUser;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by leonelsoriano3@gmail.com on 27/07/16. servicio encargado de
 * mantener en estados los paramentros entre vistas se conecta a la parte de
 * front mediante js
 */
@ApplicationPath("/resources")
@Path("ManagerParam")
public class ManagerParam {
    
    @EJB
    private ServiceCreateUser serviceCreateUser;
    
//    [{ "key": '1',"value": "12"},{ "key": '01',"value": "777"}]
//    session.setAttribute("sessionToken", userDTO.getToken());

    @POST
    @Path("saveParameter")
    @Produces({MediaType.APPLICATION_JSON})
    public void postParameter(
            @QueryParam("url") String url,
            @QueryParam("parameters") String parameters,
            @Context HttpServletRequest req,
            @Context UriInfo uriInfo
    ) {
        Gson gs = new Gson();
        WrapperParam[] wrapperParams = gs.fromJson(parameters, WrapperParam[].class);
        HttpSession session = req.getSession(true);

        for (WrapperParam item : wrapperParams) {
//            session.removeAttribute(url + "<AKparam/>" + item.getKey());
            session.setAttribute(url + "<AKparam/>" + item.getKey(), item.getValue());
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getParameter")
    public List<WrapperParam> getParameter(
            @QueryParam("url") String url,
            @QueryParam("parameters") String parameters,
            @Context HttpServletRequest req,
            @Context UriInfo uriInfo) {
        HttpSession session = req.getSession(true);
        Gson gs = new Gson();
        String[] params = gs.fromJson(parameters, String[].class);
        List<WrapperParam> wrapperParams = new ArrayList<>();

        for (int i = 0; i < params.length; i++) {

            String atribute = (String) session.getAttribute(url + "<AKparam/>" + params[i]);
            if (atribute != null && atribute.length() > 0) {
                WrapperParam wrapperParam = new WrapperParam();
                wrapperParam.setKey(url + "<AKparam/>" + params[i]);
                wrapperParam.setValue(atribute);
                wrapperParams.add(wrapperParam);
//                session.removeAttribute(url +"<AKparam/>" + params[i]);
            }
              
        }
        
        return wrapperParams;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("putParameter")
    public List<WrapperParam> putParameter(
            @QueryParam("url") String url,
            @QueryParam("parameters") String parameters,
            @Context HttpServletRequest req,
            @Context UriInfo uriInfo) {
        HttpSession session = req.getSession(true);
        Gson gs = new Gson();
        String[] params = gs.fromJson(parameters, String[].class);
        List<WrapperParam> wrapperParams = new ArrayList<>();

        for (int i = 0; i < params.length; i++) {

            String atribute = (String) session.getAttribute(url + "<AKparam/>" + params[i]);
            if (atribute != null && atribute.length() > 0) {
                session.removeAttribute(url + "<AKparam/>" + params[i]);
                WrapperParam wrapperParam = new WrapperParam();
                wrapperParam.setKey(url + "<AKparam/>" + params[i]);
                wrapperParam.setValue(atribute);
                wrapperParams.add(wrapperParam);
                
            }

        }
        return wrapperParams;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getUsername")
    public UserByLoginDTO getUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
        return user;
    }
    
     @POST
    @Path("deleteParameter")
    @Produces({MediaType.APPLICATION_JSON})
    public void deleteParameter(
          
            @QueryParam("parameters") String parameters,
            @Context HttpServletRequest req,
            @Context UriInfo uriInfo
    ) {
        Gson gs = new Gson();
        WrapperParam[] wrapperParams = gs.fromJson(parameters, WrapperParam[].class);
        HttpSession session = req.getSession(true);

        for (WrapperParam item : wrapperParams) {
            session.removeAttribute("<AKparam/>" + item.getKey());
        }

    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getParameterDelete")
    public List<WrapperParam> getParameterDelete(
            @QueryParam("url") String url,
            @QueryParam("parameters") String parameters,
            @Context HttpServletRequest req,
            @Context UriInfo uriInfo) {
        HttpSession session = req.getSession(true);
        Gson gs = new Gson();
        String[] params = gs.fromJson(parameters, String[].class);
        List<WrapperParam> wrapperParams = new ArrayList<>();

        for (int i = 0; i < params.length; i++) {

            String atribute = (String) session.getAttribute(url + "<AKparam/>" + params[i]);
            if (atribute != null && atribute.length() > 0) {
                WrapperParam wrapperParam = new WrapperParam();
                wrapperParam.setKey(url + "<AKparam/>" + params[i]);
                wrapperParam.setValue(atribute);
                wrapperParams.add(wrapperParam);
                session.removeAttribute(url +"<AKparam/>" + params[i]);
            }
              
        }
        
        return wrapperParams;
    }

}
