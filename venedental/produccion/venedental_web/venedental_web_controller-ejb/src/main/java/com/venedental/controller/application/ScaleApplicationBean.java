/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;

import com.venedental.controller.BaseBean;
import com.venedental.facade.ScaleFacadeLocal;
import com.venedental.model.Scale;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "scaleApplicationBean")
@ApplicationScoped
public class ScaleApplicationBean extends BaseBean {

    @EJB
    private ScaleFacadeLocal scaleFacadeLocal;

    public ScaleApplicationBean() {
    }

    @PostConstruct
    public void init() {
        this.getListScale().addAll(this.scaleFacadeLocal.findAll());
    }

    private List<Scale> listScale;

    public List<Scale> getListScale() {
        if (this.listScale == null) {
            this.listScale = new ArrayList<>();
        }
        return listScale;
    }

    public void setListScale(List<Scale> listScale) {
        this.listScale = listScale;
    }


}
