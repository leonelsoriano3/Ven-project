package com.venedental.controller.viewReport;

import com.venedental.controller.BaseBean;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.application.PlansApplicationBean;
import com.venedental.controller.application.PropertiesApplicationBean;
import com.venedental.controller.application.TreatmentsApplicationBean;
import com.venedental.controller.message.MessagesStandar;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.controller.view.*;
import com.venedental.controller.view.InsurancesViewBean;
import com.venedental.dto.AuditReportInput;
import com.venedental.dto.AuditReportOutput;
import com.venedental.dto.ReadInsurancesReportOutput;
import com.venedental.dto.ReadPlansReportInput;
import com.venedental.dto.ReadPlansReportOutput;
import com.venedental.dto.ReadPropertiesReportInput;
import com.venedental.dto.ReadPropertiesReportOutput;
import com.venedental.dto.ReadSpecialtiesReportOutput;
import com.venedental.dto.ReadTreatmentsReportInput;
import com.venedental.dto.ReadTreatmentsReportOutput;
import com.venedental.dto.reportAudited.EntityOutputDTO;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.KeysPatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PropertiesFacadeLocal;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.facade.TreatmentsFacadeLocal;
import com.venedental.facade.TypeSpecialistFacadeLocal;
import com.venedental.facade.store_procedure.ReadInsurancesFacadeLocal;
import com.venedental.facade.store_procedure.ReadPlansFacadeLocal;
import com.venedental.facade.store_procedure.ReadPropertiesFacadeLocal;
import com.venedental.facade.store_procedure.ReadSpecialtiesFacadeLocal;
import com.venedental.facade.store_procedure.ReadTreatmentsFacadeLocal;
import com.venedental.model.Insurances;
import com.venedental.model.KeysPatients;
import com.venedental.model.Plans;
import com.venedental.model.Properties;
import com.venedental.model.TypeSpecialist;
import com.venedental.services.ServicesReportAudited;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.mail.MessagingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import services.utils.Transformar;
import com.venedental.dto.reportAudited.StateOutputDTO;
import java.util.logging.Level;

@ManagedBean(name = "auditViewBeanReport")
@ViewScoped
public class AuditViewBeanReport extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();

    @EJB
    private ServicesReportAudited servicesReportAudited;

    @EJB
    private KeysPatientsFacadeLocal keysFacadeLocal;

    //@EJB
    // private AuditFacadeLocal auditFacadeLocal;
    @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;

    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    @EJB
    private PlansFacadeLocal plansFacadeLocal;

    @EJB
    private InsurancesFacadeLocal insurancesFacadeLocal;

    @EJB
    private TypeSpecialistFacadeLocal typeSpecialistFacadeLocal;

    @EJB
    private TreatmentsFacadeLocal treatmentsFacadeLocal;

    @EJB
    private PropertiesFacadeLocal propertiesFacadeLocal;

    @EJB
    private ReadInsurancesFacadeLocal insuranceFacadeLocal;

    @EJB
    private ReadSpecialtiesFacadeLocal specialtiesFacadeLocal;

    @EJB
    private ReadPlansFacadeLocal readPlansFacadeLocal;

    @EJB
    private ReadTreatmentsFacadeLocal readTreatmentsFacadeLocal;

    @EJB
    private ReadPropertiesFacadeLocal readPropertiesFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    @ManagedProperty(value = "#{keysPatientsViewBean}")
    private KeysPatientsViewBean keysPatientsViewBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;

    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;

    @ManagedProperty(value = "#{plansPatientsViewBean}")
    private PlansPatientsViewBean plansPatientsViewBean;

    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesPlansTreatmentsKeyViewBean}")
    private PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean;

    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;

    @ManagedProperty(value = "#{messagesStandar}")
    private MessagesStandar messagesStandar;

    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;

    @ManagedProperty(value = "#{insurancesViewBean}")
    private InsurancesViewBean insurancesViewBean;

    @ManagedProperty(value = "#{plansViewBean}")
    private PlansViewBean plansViewBean;

    @ManagedProperty(value = "#{treatmentsViewBean}")
    private TreatmentsViewBean treatmentsViewBean;

    @ManagedProperty(value = "#{propertiesViewBean}")
    private PropertiesViewBean propertiesViewBean;

    @ManagedProperty(value = "#{plansApplicationBean}")
    private PlansApplicationBean plansApplicationBean;

    @ManagedProperty(value = "#{treatmentsApplicationBean}")
    private TreatmentsApplicationBean treatmentsApplicationBean;

    @ManagedProperty(value = "#{propertiesApplicationBean}")
    private PropertiesApplicationBean propertiesApplicationBean;

    private String dueDate;

    private List<StateOutputDTO> ListStateOutputDTO;

    private List<EntityOutputDTO> listEntityOutputDTO;

    private StateOutputDTO selectedState;

    private List<String> listPropiedades;

    private EntityOutputDTO selectedEntity;

    private List<KeysPatients> listaKeys;

    private List<KeysPatients> listaKeysFiltrada;

    private List<KeysPatients> listHistoryKeys;
    
    private Integer idTypeResult;

    private boolean visible;

    private KeysPatients keysPatientsNew;

    private KeysPatients keysPatientsEdit;

    private Map<String, Object> params = new HashMap<String, Object>();

    private JRBeanCollectionDataSource beanCollectionDataSource;

    private JasperPrint jasperPrint;

    private List<TypeSpecialist> listTypeSpecialist;

    private TypeSpecialist newtypeSpecialist;

    private Date startDate, endDate, maxDate;

    private Integer typeSpecialistId;

    private String insuranceName;

    private String reportPath;

    private String logoPath;

    private HttpServletResponse httpServletResponse;

    private ServletOutputStream servletOutputStream;

    private JRXlsxExporter docxExporter;

    private boolean healthArea;

    private boolean insurance;

    private boolean plan;

    private boolean disabledButtExport, disabledButtSerach, disabledButtRest;

    private List<ReadPlansReportOutput> listaPlans;

    private List<Plans> listaPlansAux;

    private ReadPlansReportOutput newPlansList;

    private ReadInsurancesReportOutput newInsurances;

    private ReadSpecialtiesReportOutput newTypeSpecialist;

    private ReadTreatmentsReportOutput newTreatments;

    private List<ReadInsurancesReportOutput> listaInsurances;

    private List<ReadSpecialtiesReportOutput> listaTypeSpecialist;

    private Insurances allInsurances = new Insurances(999999, "Todas las Aseguradoras");

    private Properties allProperties = new Properties("999999", "Todos los dientes");

    private TypeSpecialist allTypeSpecialist = new TypeSpecialist(999999, "Todas las Especialidades");

    private List<ReadTreatmentsReportOutput> listaTreatments;

    private Properties selectedProperties;

    private boolean disablePiece, disableTreatments, disableKey,
            disableIndentSpecialist, disableNameSpecialist,
            disableIndentPatient, disableNamePatient, disableEnty, disableDate;

    private List<ReadPropertiesReportOutput> listaProperties;

    private Integer insurancesId;

    private String insurancesName;

    private List<AuditReportOutput> listaAudit = new ArrayList<>();

    private List<String> listOptionResult;

    private String optionResultSelected;

    private AuditReportInput input = new AuditReportInput();

    private ReadPlansReportInput inputPlan = new ReadPlansReportInput();

    private ReadTreatmentsReportInput inputTreatment = new ReadTreatmentsReportInput();

    private ReadPropertiesReportInput inputProperties = new ReadPropertiesReportInput();

    public boolean isDisableDate() {
        return disableDate;
    }

    public void setDisableDate(boolean disableDate) {
        this.disableDate = disableDate;
    }

    public StateOutputDTO getSelectedState() {
        return selectedState;
    }

    public void setSelectedState(StateOutputDTO selectedState) {
        this.selectedState = selectedState;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }

    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }

    public PropertiesPlansTreatmentsKeyViewBean getPropertiesPlansTreatmentsKeyViewBean() {
        return propertiesPlansTreatmentsKeyViewBean;
    }

    public void setPropertiesPlansTreatmentsKeyViewBean(PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean) {
        this.propertiesPlansTreatmentsKeyViewBean = propertiesPlansTreatmentsKeyViewBean;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public List<StateOutputDTO> getListstateOutputDTO() {
        return ListStateOutputDTO;
    }

    public void setListstateOutputDTO(List<StateOutputDTO> ListstateOutputDTO) {
        this.ListStateOutputDTO = ListstateOutputDTO;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public PlansPatientsViewBean getPlansPatientsViewBean() {
        return plansPatientsViewBean;
    }

    public void setPlansPatientsViewBean(PlansPatientsViewBean plansPatientsViewBean) {
        this.plansPatientsViewBean = plansPatientsViewBean;
    }

    public KeysPatientsViewBean getKeysPatientsViewBean() {
        return keysPatientsViewBean;
    }

    public void setKeysPatientsViewBean(KeysPatientsViewBean keysPatientsViewBean) {
        this.keysPatientsViewBean = keysPatientsViewBean;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public KeysPatientsFacadeLocal getKeysFacadeLocal() {
        return keysFacadeLocal;
    }

    public void setKeysFacadeLocal(KeysPatientsFacadeLocal keysFacadeLocal) {
        this.keysFacadeLocal = keysFacadeLocal;
    }

    public List<KeysPatients> getListaKeys() {
        if (this.listaKeys == null) {
            this.listaKeys = new ArrayList<>();
            this.getListaKeys().addAll(this.getListaKeys());
        }
        return listaKeys;

    }

    public void setListaKeys(List<KeysPatients> listaKeys) {
        this.listaKeys = listaKeys;
    }

    public List<KeysPatients> getListaKeysFiltrada() {
        if (this.listaKeysFiltrada == null) {
            this.listaKeysFiltrada = new ArrayList<>();
            this.getListaKeysFiltrada().addAll(this.getListaKeysFiltrada());
        }
        return listaKeysFiltrada;

    }

    public void setListaKeysFiltrada(List<KeysPatients> listaKeysFiltrada) {
        this.listaKeysFiltrada = listaKeysFiltrada;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public String getMyFormattedDateStart(Date fecha) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTypeSpecialistId() {
        return typeSpecialistId;
    }

    public void setTypeSpecialistId(Integer typeSpecialistId) {
        this.typeSpecialistId = typeSpecialistId;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public MessagesStandar getMessagesStandar() {
        return messagesStandar;
    }

    public void setMessagesStandar(MessagesStandar messagesStandar) {
        this.messagesStandar = messagesStandar;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    public boolean isHealthArea() {
        return healthArea;
    }

    public void setHealthArea(boolean healthArea) {
        this.healthArea = healthArea;
    }

    public boolean isInsurance() {
        return insurance;
    }

    public void setInsurance(boolean insurance) {
        this.insurance = insurance;
    }

    public boolean isPlan() {
        return plan;
    }

    public void setPlan(boolean plan) {
        this.plan = plan;
    }

    public InsurancesViewBean getInsurancesViewBean() {
        return insurancesViewBean;
    }

    public void setInsurancesViewBean(InsurancesViewBean insurancesViewBean) {
        this.insurancesViewBean = insurancesViewBean;
    }

    public PlansViewBean getPlansViewBean() {
        return plansViewBean;
    }

    public void setPlansViewBean(PlansViewBean plansViewBean) {
        this.plansViewBean = plansViewBean;
    }

    public TreatmentsViewBean getTreatmentsViewBean() {
        return treatmentsViewBean;
    }

    public void setTreatmentsViewBean(TreatmentsViewBean treatmentsViewBean) {
        this.treatmentsViewBean = treatmentsViewBean;
    }

    public PropertiesViewBean getPropertiesViewBean() {
        return propertiesViewBean;
    }

    public void setPropertiesViewBean(PropertiesViewBean propertiesViewBean) {
        this.propertiesViewBean = propertiesViewBean;
    }

    public List<ReadPlansReportOutput> getListaPlans() {
        if (this.listaPlans == null) {
            this.listaPlans = new ArrayList<>();
            if (this.newInsurances != null) {
                inputPlan.setIdInsurance((this.newInsurances.getIdInsurance() == -1) ? null
                        : this.newInsurances.getIdInsurance());
            }
            if (this.newTypeSpecialist != null) {
                inputPlan.setIdHealthArea((this.newTypeSpecialist.getIdSpecialty() == -1) ? null
                        : this.newTypeSpecialist.getIdSpecialty());
            }
            this.listaPlans = this.readPlansFacadeLocal.execute(inputPlan);

        }

        return listaPlans;
    }

    public PlansFacadeLocal getPlansFacadeLocal() {
        return plansFacadeLocal;
    }

    public void setPlansFacadeLocal(PlansFacadeLocal plansFacadeLocal) {
        this.plansFacadeLocal = plansFacadeLocal;
    }

    public void setListaPlans(List<ReadPlansReportOutput> listaPlans) {
        this.listaPlans = listaPlans;
    }

    public List<Plans> getListaPlansAux() {
        if (this.listaPlansAux == null) {
            this.listaPlansAux = new ArrayList<>();
            this.listaPlansAux = this.plansFacadeLocal.findAll();
        }

        return listaPlansAux;
    }

    public void setListaPlansAux(List<Plans> listaPlansAux) {
        this.listaPlansAux = listaPlansAux;
    }

    public PlansApplicationBean getPlansApplicationBean() {
        return plansApplicationBean;
    }

    public void setPlansApplicationBean(PlansApplicationBean plansApplicationBean) {
        this.plansApplicationBean = plansApplicationBean;
    }

    public ReadPlansReportOutput getNewPlansList() {
        return newPlansList;
    }

    public void setNewPlansList(ReadPlansReportOutput newPlansList) {
        this.newPlansList = newPlansList;
    }

    public ReadInsurancesReportOutput getNewInsurances() {
        return newInsurances;
    }

    public void setNewInsurances(ReadInsurancesReportOutput newInsurances) {
        this.newInsurances = newInsurances;
    }

    public List<ReadInsurancesReportOutput> getListaInsurances() {
        if (this.listaInsurances == null) {
            this.listaInsurances = new ArrayList<>();
            this.listaInsurances = this.insuranceFacadeLocal.execute(null);

        }
        return listaInsurances;
    }

    public InsurancesFacadeLocal getInsurancesFacadeLocal() {
        return insurancesFacadeLocal;
    }

    public void setInsurancesFacadeLocal(InsurancesFacadeLocal insurancesFacadeLocal) {
        this.insurancesFacadeLocal = insurancesFacadeLocal;
    }

    public ReadSpecialtiesReportOutput getNewTypeSpecialist() {
        return newTypeSpecialist;
    }

    public void setNewTypeSpecialist(ReadSpecialtiesReportOutput newTypeSpecialist) {
        this.newTypeSpecialist = newTypeSpecialist;
    }

    public void setListaInsurances(List<ReadInsurancesReportOutput> listaInsurances) {
        this.listaInsurances = listaInsurances;
    }

    public List<ReadSpecialtiesReportOutput> getListaTypeSpecialist() {
        if (this.listaTypeSpecialist == null) {
            this.listaTypeSpecialist = this.specialtiesFacadeLocal.execute(null);
        }
        return listaTypeSpecialist;
    }

    public void setListaTypeSpecialist(List<ReadSpecialtiesReportOutput> listaTypeSpecialist) {
        this.listaTypeSpecialist = listaTypeSpecialist;
    }

    public List<ReadTreatmentsReportOutput> getListaTreatments() {
        if (this.listaTreatments == null) {
            this.listaTreatments = new ArrayList<>();
            if (this.newTreatments != null) {
                inputTreatment.setIdInsurance(this.newInsurances.getIdInsurance());
            }
            if (this.newTypeSpecialist != null) {
                inputTreatment.setIdHealthArea(this.newTypeSpecialist.getIdSpecialty());
            }
            if (this.newPlansList != null) {
                inputTreatment.setIdPlan(this.newPlansList.getIdPlan());
            }
            this.listaTreatments = this.readTreatmentsFacadeLocal.execute(inputTreatment);
        }
        return listaTreatments;
    }

    public void setListaTreatments(List<ReadTreatmentsReportOutput> listaTreatments) {
        this.listaTreatments = listaTreatments;
    }

    public TypeSpecialistFacadeLocal getTypeSpecialistFacadeLocal() {
        return typeSpecialistFacadeLocal;
    }

    public void setTypeSpecialistFacadeLocal(TypeSpecialistFacadeLocal typeSpecialistFacadeLocal) {
        this.typeSpecialistFacadeLocal = typeSpecialistFacadeLocal;
    }

    public TreatmentsFacadeLocal getTreatmentsFacadeLocal() {
        return treatmentsFacadeLocal;
    }

    public void setTreatmentsFacadeLocal(TreatmentsFacadeLocal treatmentsFacadeLocal) {
        this.treatmentsFacadeLocal = treatmentsFacadeLocal;
    }

    public TreatmentsApplicationBean getTreatmentsApplicationBean() {
        return treatmentsApplicationBean;
    }

    public void setTreatmentsApplicationBean(TreatmentsApplicationBean treatmentsApplicationBean) {
        this.treatmentsApplicationBean = treatmentsApplicationBean;
    }

    public TypeSpecialist getAllTypeSpecialist() {
        return allTypeSpecialist;
    }

    public void setAllTypeSpecialist(TypeSpecialist allTypeSpecialist) {
        this.allTypeSpecialist = allTypeSpecialist;
    }

    public ReadTreatmentsReportOutput getNewTreatments() {
        return newTreatments;
    }

    public void setNewTreatments(ReadTreatmentsReportOutput newTreatments) {
        this.newTreatments = newTreatments;
    }

    public Properties getSelectedProperties() {
        return selectedProperties;
    }

    public void setSelectedProperties(Properties selectedProperties) {
        this.selectedProperties = selectedProperties;
    }

    public List<ReadPropertiesReportOutput> getListaProperties() {
        if (this.listaProperties == null) {
            this.listaProperties = new ArrayList<>();
            if (this.newInsurances != null) {
                inputProperties.setIdInsurance(this.newInsurances.getIdInsurance());
            }
            if (this.newTypeSpecialist != null) {
                inputProperties.setIdHealthArea(this.newTypeSpecialist.getIdSpecialty());
            }
            if (this.newPlansList != null) {
                inputProperties.setIdPlan(this.newPlansList.getIdPlan());
            }
            if (this.newTreatments != null) {
                inputProperties.setIdTreatment(this.newTreatments.getIdTreatment());
            }
            this.listaProperties = this.readPropertiesFacadeLocal.execute(inputProperties);
        }

        return listaProperties;
    }

    public void setListaProperties(List<ReadPropertiesReportOutput> listaProperties) {
        this.listaProperties = listaProperties;
    }

    public PropertiesApplicationBean getPropertiesApplicationBean() {
        return propertiesApplicationBean;
    }

    public void setPropertiesApplicationBean(PropertiesApplicationBean propertiesApplicationBean) {
        this.propertiesApplicationBean = propertiesApplicationBean;
    }

    public Properties getAllProperties() {
        return allProperties;
    }

    public void setAllProperties(Properties allProperties) {
        this.allProperties = allProperties;
    }

    public Integer getInsurancesId() {
        return insurancesId;
    }

    public void setInsurancesId(Integer insurancesId) {
        this.insurancesId = insurancesId;
    }

    public String getInsurancesName() {
        return insurancesName;
    }

    public void setInsurancesName(String insurancesName) {
        this.insurancesName = insurancesName;
    }

    public List<AuditReportOutput> getListaAudit() {
        return listaAudit;
    }

    public void setListaAudit(List<AuditReportOutput> listaAudit) {
        this.listaAudit = listaAudit;
    }

    public Insurances getAllInsurances() {
        return allInsurances;
    }

    public void setAllInsurances(Insurances allInsurances) {
        this.allInsurances = allInsurances;
    }

    public void handleDateSelect(SelectEvent event) {
        RequestContext.getCurrentInstance().execute("PF('keysTable').filter()");
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public boolean isDisabledButtSerach() {
        return disabledButtSerach;
    }

    public void setDisabledButtSerach(boolean disabledButtSerach) {
        this.disabledButtSerach = disabledButtSerach;
    }

    public boolean isDisabledButtRest() {
        return disabledButtRest;
    }

    public void setDisabledButtRest(boolean disabledButtRest) {
        this.disabledButtRest = disabledButtRest;
    }

    public boolean isDisabledButtExport() {
        return disabledButtExport;
    }

    public void setDisabledButtExport(boolean disabledButtExport) {
        this.disabledButtExport = disabledButtExport;
    }

    public ReadInsurancesFacadeLocal getInsuranceFacadeLocal() {
        return insuranceFacadeLocal;
    }

    public void setInsuranceFacadeLocal(ReadInsurancesFacadeLocal insuranceFacadeLocal) {
        this.insuranceFacadeLocal = insuranceFacadeLocal;
    }

    public ReadSpecialtiesFacadeLocal getSpecialtiesFacadeLocal() {
        return specialtiesFacadeLocal;
    }

    public void setSpecialtiesFacadeLocal(ReadSpecialtiesFacadeLocal specialtiesFacadeLocal) {
        this.specialtiesFacadeLocal = specialtiesFacadeLocal;
    }

    public ReadPlansFacadeLocal getReadPlansFacadeLocal() {
        return readPlansFacadeLocal;
    }

    public void setReadPlansFacadeLocal(ReadPlansFacadeLocal readPlansFacadeLocal) {
        this.readPlansFacadeLocal = readPlansFacadeLocal;
    }

    public List<String> getListOptionResult() {
        return listOptionResult;
    }

    public void setListOptionResult(List<String> listOptionResult) {
        this.listOptionResult = listOptionResult;
    }

    public String getOptionResultSelected() {
        return optionResultSelected;
    }

    public void setOptionResultSelected(String optionResultSelected) {
        this.optionResultSelected = optionResultSelected;
    }

    /**
     *
     * @throws ParseException
     * @throws IOException
     * @throws MessagingException
     */
    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);

        StateOutputDTO allState = new StateOutputDTO();
        allState.setIdState(-1);
        allState.setNameState("TODOS LOS ESTADOS");
        this.setListstateOutputDTO(this.servicesReportAudited.state(null));
        this.getListStateOutputDTO().add(0, allState);

        this.setListEntityOutputDTO(this.servicesReportAudited.entity(null));

        listOptionResult = new ArrayList();
        this.listOptionResult.add("POR CLAVE");
        this.listOptionResult.add("POR TRATAMIENTO");

        this.setIdTypeResult(0);
        this.input.setIdResult(idTypeResult);


        this.setIdTypeResult(0);
        this.input.setIdResult(idTypeResult);
        

        InitDisablesBool();

        ReadSpecialtiesReportOutput allSpecialities = new ReadSpecialtiesReportOutput();
        allSpecialities.setIdSpecialty(-1);
        allSpecialities.setNameSpecialty("TODAS LAS ESPECIALIDADES");
        this.getListaTypeSpecialist().add(0, allSpecialities);

        ReadPlansReportOutput allPlans = new ReadPlansReportOutput();
        allPlans.setIdPlan(-1);
        allPlans.setNamePlan("TODOS LOS PLANES");
        this.getListaPlans().add(0, allPlans);

        ReadInsurancesReportOutput allInsurance = new ReadInsurancesReportOutput();
        allInsurance.setIdInsurance(-1);
        allInsurance.setName("TODAS LAS ASEGURADORAS");
        this.getListaInsurances().add(0, allInsurance);




    }

    public void InitDisablesBool() {
        this.setDisableDate(true);
        this.setDisableEnty(true);
        this.setDisableIndentPatient(true);
        this.setDisableIndentSpecialist(true);
        this.setDisableKey(true);
        this.setDisableNamePatient(true);
        this.setDisablePiece(false);
        this.setDisableTreatments(false);
        this.setDisabledButtSerach(false);
        this.setDisabledButtRest(true);
        this.setDisabledButtExport(true);
    }

    public void load() throws IOException, JRException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listaAudit", new JRBeanCollectionDataSource(listaAudit));
        this.params.put("heal_logo2", logoPath);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(listaAudit);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/auditKeys/auditKeys.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    public void sendParametersToTheReport() throws ParseException, IOException, JRException, MessagingException {
        this.params.put("startDate", this.startDate);
        this.params.put("endDate", this.endDate);

        if (this.getNewInsurances() != null) {

            this.params.put("insurance", (this.getNewInsurances().getIdInsurance() == -1)
                    ? null : this.getNewInsurances().getName());
        }

        if (getNewPlansList() != null) {
            this.params.put("plan", (this.getNewPlansList().getIdPlan() == -1)
                    ? null : this.getNewPlansList().getNamePlan());
        }

        if (getNewTreatments() != null) {
            this.params.put("treatment", this.getNewTreatments().getNameTreatment());
        }

        if (this.getNewTypeSpecialist() != null) {
            this.params.put("healthArea", (this.getNewTypeSpecialist().getIdSpecialty() == -1)
                    ? null : this.newTypeSpecialist.getNameSpecialty());
        }

        if (this.getSelectedProperties() != null) {
            this.params.put("tooth", this.getSelectedProperties().getId());
        }

        if (this.getSelectedEntity() != null) {
            this.params.put("entity", (getSelectedEntity().getNameEntity().equals("")) ? null : getSelectedEntity().getNameEntity());
        } else {
            this.params.put("entity", null);
        }

        if (getSelectedState() != null) {

            this.params.put("state", (this.getSelectedState().getIdState() == -1)
                    ? null : this.getSelectedState().getNameState());
        }
        if (this.getOptionResultSelected() != null) {
            this.params.put("reporte", (this.getOptionResultSelected().equals("POR CLAVE")
                    ? null : this.getOptionResultSelected()));
        } else {
            this.params.put("reporte", (null));
        }
    }
    public void fillAuditList() throws ParseException, IOException, JRException, MessagingException {

        this.input.setStartDate(null);
        if (this.getStartDate() != null) {
            input.setStartDate(new java.sql.Date(this.startDate.getTime()));
        } else {
            this.input.setStartDate(null);

        }

        this.input.setEndDate(null);
        if (this.getEndDate() != null) {
            input.setEndDate(new java.sql.Date(this.endDate.getTime()));
        } else {
            this.input.setEndDate(null);
        }

        if (this.getNewTreatments() != null) {
            input.setIdTreatment(this.getNewTreatments().getIdTreatment());
        } else {
            input.setIdTreatment(null);
        }

        this.input.setIdStatus(null);
        if (this.newTypeSpecialist != null) {
            this.input.setHealthArea((this.newTypeSpecialist.getIdSpecialty() == -1)
                    ? null : this.newTypeSpecialist.getIdSpecialty());
        } else {
            this.input.setHealthArea(null);
        }

        this.input.setTypeBaremo(null);
        if (this.getNewPlansList() != null) {
            this.input.setIdPlan((this.getNewPlansList().getIdPlan() == -1)
                    ? null : this.getNewPlansList().getIdPlan());
        } else {
            this.input.setIdPlan(null);

            this.input.setIdPlan(null);
            if (this.getListaPlans() != null) {
                this.input.setIdPlan((this.getListaPlans().get(0).getIdPlan() == -1)
                        ? null : this.getNewPlansList().getIdPlan());
            } else {
                this.input.setIdPlan(null);
            }
        }

        this.input.setIdPieza(null);

        if (this.getNewInsurances() != null) {
            this.input.setIdInsurance((this.getNewInsurances().getIdInsurance() == -1)
                    ? null : this.getNewInsurances().getIdInsurance());
        } else {
            this.input.setIdInsurance(null);
        }

        this.input.setIdEntity(null);
        if (this.getSelectedEntity() != null) {
            this.input.setIdEntity(this.getSelectedEntity().getIdEntity());
        } else {
            this.input.setIdEntity(null);
        }

        this.input.setIdState(null);
        if (this.getSelectedState() != null) {
            this.input.setIdState((this.getSelectedState().getIdState() == -1)
                    ? null : this.getSelectedState().getIdState());
        } else {
            this.input.setIdState(null);
        }

        this.listaAudit.clear();
        this.setListaAudit(this.servicesReportAudited.auditReportOutput(
                (this.input.getStartDate() != null) ? new java.sql.Date(this.startDate.getTime())
                        : null, (this.input.getEndDate() != null) ? new java.sql.Date(this.endDate.getTime())
                        : null, input.getIdInsurance(), input.getHealthArea(),
                input.getIdPlan(), input.getIdTreatment(), input.getIdPieza(),

                input.getIdEntity(), input.getIdState(), input.getIdResult()));

                


        if (this.listaAudit.isEmpty()) {
            this.setDisabledButtExport(true);
        } else {
            this.setDisabledButtExport(false);
            this.setDisabledButtRest(false);
            RequestContext.getCurrentInstance().update("form");


        }
        this.setDisabledButtRest(false);
        this.setDisabledButtRest(false);
        this.sendParametersToTheReport();

        RequestContext.getCurrentInstance().update("form:reportAudit");
    }

    public void disabledButtons() {

        if (this.getStartDate() != null || this.getEndDate() != null
                || this.getSelectedState() != null
                || this.getNewInsurances() != null
                || this.getNewTypeSpecialist() != null
                || this.getNewPlansList() != null
                || this.getSelectedEntity() != null) {
            this.setDisabledButtSerach(false);
            this.setDisabledButtRest(false);
            RequestContext.getCurrentInstance().update("form:btnFindKey");
            RequestContext.getCurrentInstance().update("form:btnResetKey");

        } else {
            this.setDisabledButtSerach(true);
            this.setDisabledButtRest(true);
        }
    }

    public void hiddenColumn() {
        if (this.getOptionResultSelected().equals("POR TRATAMIENTO")) {
            this.setDisableDate(true);
            this.setDisableIndentPatient(true);
            this.setDisableIndentSpecialist(true);
            this.setDisableKey(true);
            this.setDisableNamePatient(true);

            this.setDisablePiece(true);

            this.setDisablePiece(true);  

            this.setDisableTreatments(true);
            this.setDisableEnty(true);

            this.setIdTypeResult(1);
            this.input.setIdResult(this.getIdTypeResult());
            try {
                fillAuditList();
            } catch (Exception e) {
            }

            this.setIdTypeResult(1);
             this.input.setIdResult(this.getIdTypeResult());
             try{fillAuditList();}catch(Exception e){} 
         

        } else {
            this.setDisableDate(true);
            this.setDisableIndentPatient(true);
            this.setDisableIndentSpecialist(true);
            this.setDisableKey(true);
            this.setDisableNamePatient(true);
            this.setDisableEnty(true);
            this.setDisablePiece(false);
            this.setDisableTreatments(false);

            this.setIdTypeResult(0);
            this.input.setIdResult(this.getIdTypeResult());
            try {
                fillAuditList();
            } catch (Exception e) {
            }

            this.setIdTypeResult(0);
             this.input.setIdResult(this.getIdTypeResult());
             try{fillAuditList();}catch(Exception e){} 

        }
        RequestContext.getCurrentInstance().update("form");
        try {
            this.sendParametersToTheReport();
        } catch (Exception e) {

        }

    }

    public void refresh() {

        RequestContext.getCurrentInstance().update("from: estadoCreate");
        FacesContext context = FacesContext.getCurrentInstance();
        Application application = context.getApplication();
        ViewHandler viewHandler = application.getViewHandler();
        UIViewRoot viewRoot = viewHandler.createView(context, context
                .getViewRoot().getViewId());
        context.setViewRoot(viewRoot);
        context.renderResponse();

    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }

    public void onHealthAreaStatusChange() {
        if (this.healthArea) {
            this.insurance = false;
            this.plan = false;
        }
    }

    public void onInsuranceStatusChange() {
        if (this.insurance) {
            this.healthArea = false;
            this.plan = false;

        }
    }

    public void onPlanStatusChange() {
        if (this.plan) {
            this.healthArea = false;
            this.insurance = false;
        }
    }

    public void filtrarPlans(ReadInsurancesReportOutput insurance, ReadSpecialtiesReportOutput healthArea) {
        this.getListaPlans().clear();
        if (insurance != null) {
            inputPlan.setIdInsurance((insurance.getIdInsurance() == -1)
                    ? null : insurance.getIdInsurance());
        }
        if (healthArea != null) {
            inputPlan.setIdHealthArea((healthArea.getIdSpecialty() == -1)
                    ? null : healthArea.getIdSpecialty());
        }
        this.listaPlans = this.readPlansFacadeLocal.execute(inputPlan);
        ReadPlansReportOutput allPlans = new ReadPlansReportOutput();
        allPlans.setIdPlan(-1);
        allPlans.setNamePlan("TODOS LOS PLANES");
        this.listaPlans.add(0, allPlans);

    }

    public void filtrarTreatments(ReadInsurancesReportOutput insurance, ReadSpecialtiesReportOutput healthArea, ReadPlansReportOutput plan) {
        this.getListaTreatments().clear();
        if (insurance != null) {
            inputTreatment.setIdInsurance((insurance.getIdInsurance() == -1)
                    ? null : insurance.getIdInsurance());
        }
        if (healthArea != null) {
            inputTreatment.setIdHealthArea((healthArea.getIdSpecialty() == -1)
                    ? null : healthArea.getIdSpecialty());
        }
        if (plan != null) {
            inputTreatment.setIdPlan((plan.getIdPlan() == -1) ? null
                    : plan.getIdPlan());
        }
        this.listaTreatments = this.readTreatmentsFacadeLocal.execute(inputTreatment);
    }

    public void filtrarProperties(ReadInsurancesReportOutput insurance, ReadSpecialtiesReportOutput healthArea,
            ReadPlansReportOutput plan, ReadTreatmentsReportOutput treatment) {
        this.getListaProperties().clear();
        if (insurance != null) {
            inputProperties.setIdInsurance(insurance.getIdInsurance());
        }
        if (healthArea != null) {
            inputProperties.setIdHealthArea(healthArea.getIdSpecialty());
        }
        if (plan != null) {
            inputProperties.setIdPlan(plan.getIdPlan());
        }
        if (treatment != null) {
            inputProperties.setIdTreatment(treatment.getIdTreatment());
        }
        this.listaProperties = this.readPropertiesFacadeLocal.execute(inputProperties);
    }

    public void allFilters(ReadInsurancesReportOutput insurance, ReadSpecialtiesReportOutput healthArea,
            ReadPlansReportOutput plan, ReadTreatmentsReportOutput treatment) {
        this.filtrarPlans(insurance, healthArea);
        this.filtrarTreatments(insurance, healthArea, plan);
        this.filtrarProperties(insurance, healthArea, plan, treatment);
        this.disabledButtons();

    }

    public void showReportXlsx(ActionEvent actionEvent) throws JRException, IOException, Exception {
        String fileName;
        if (this.getNewInsurances() != null) {
            fileName = this.getNewInsurances().getName();
        } else {
            fileName = "Todas las Aseguradoras";
        }
        if (!listaAudit.isEmpty()) {
            this.load();
            this.export.xlsx(jasperPrint, fileName, Transformar.getThisDate());
        }

    }

    public ServicesReportAudited getServicesReportAudited() {
        return servicesReportAudited;
    }

    public void setServicesReportAudited(ServicesReportAudited servicesReportAudited) {
        this.servicesReportAudited = servicesReportAudited;
    }

    public List<EntityOutputDTO> getListEntityOutputDTO() {
        return listEntityOutputDTO;
    }

    public void setListEntityOutputDTO(List<EntityOutputDTO> listEntityOutputDTO) {
        this.listEntityOutputDTO = listEntityOutputDTO;
    }

    public List<EntityOutputDTO> filterEntity(String query) {
        log.log(Level.INFO, "filterEntity()");
        List<EntityOutputDTO> filteredEntitys = this.servicesReportAudited.entity(query);
        return filteredEntitys;
    }

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void onItemSelect(SelectEvent event) throws IOException {
        log.log(Level.INFO, "onItemSelect()");
        this.setSelectedEntity(new EntityOutputDTO());
        this.setSelectedEntity((EntityOutputDTO) event.getObject());
        this.disabledButtons();
    }

    public EntityOutputDTO getSelectedEntity() {

        return selectedEntity;

    }

    public void setSelectedEntity(EntityOutputDTO selectedEntity) {

        this.selectedEntity = selectedEntity;

    }

    public StateOutputDTO getState() {
        return selectedState;
    }

    public void setState(StateOutputDTO state) {
        this.selectedState = state;
    }

    public List<String> getListPropiedades() {
        return listPropiedades;
    }

    public void setListPropiedades(List<String> listPropiedades) {
        this.listPropiedades = listPropiedades;
    }

    public List<StateOutputDTO> getListStateOutputDTO() {
        return ListStateOutputDTO;
    }

    public void setListStateOutputDTO(List<StateOutputDTO> ListStateOutputDTO) {
        this.ListStateOutputDTO = ListStateOutputDTO;
    }

    public boolean isDisablePiece() {
        return disablePiece;
    }

    public void setDisablePiece(boolean disablePiece) {
        this.disablePiece = disablePiece;
    }

    public boolean isDisableTreatments() {

        return disableTreatments;
    }

    public void setDisableTreatments(boolean disableTreatments) {
        this.disableTreatments = disableTreatments;
    }

    public boolean isDisableKey() {
        return disableKey;
    }

    public void setDisableKey(boolean disableKey) {
        this.disableKey = disableKey;
    }

    public boolean isDisableIndentSpecialist() {
        return disableIndentSpecialist;
    }

    public void setDisableIndentSpecialist(boolean disableIndentSpecialist) {
        this.disableIndentSpecialist = disableIndentSpecialist;
    }

    public boolean isDisableNameSpecialist() {
        return disableNameSpecialist;
    }

    public void setDisableNameSpecialist(boolean disabledNameSpecialist) {
        this.disableNameSpecialist = disableNameSpecialist;
    }

    public boolean isDisableIndentPatient() {
        return disableIndentPatient;
    }

    public void setDisableIndentPatient(boolean disableIndentPatient) {
        this.disableIndentPatient = disableIndentPatient;
    }

    public boolean isDisableNamePatient() {
        return disableNamePatient;
    }

    public void setDisableNamePatient(boolean disableNamePatient) {
        this.disableNamePatient = disableNamePatient;
    }

    public boolean isDisableEnty() {
        return disableEnty;
    }

    public void setDisableEnty(boolean disableEnty) {
        this.disableEnty = disableEnty;
    }

    public Integer getIdTypeResult() {
        return idTypeResult;
    }

    public void setIdTypeResult(Integer idTypeResult) {
        this.idTypeResult = idTypeResult;
    }
}
