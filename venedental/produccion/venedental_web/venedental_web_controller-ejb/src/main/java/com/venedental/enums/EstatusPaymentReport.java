/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.enums;

/**
 *
 * @author AKDESKDEV90
 */
public enum EstatusPaymentReport {
    
    POR_FACTURAR(1), POR_PAGAR(2), PAGADO(3);

    Integer valor;

    private EstatusPaymentReport(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
    
}
