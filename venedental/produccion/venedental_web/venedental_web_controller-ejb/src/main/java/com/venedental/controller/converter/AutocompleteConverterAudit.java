/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;


import com.venedental.controller.viewReport.AuditViewBeanReport;
import com.venedental.dto.reportAudited.EntityOutputDTO;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author akdesk10
 */
@FacesConverter("autocompleteConverterAudit")
public class AutocompleteConverterAudit implements Converter {

    @ManagedProperty(value = "#{auditViewBeanReport}")
    private AuditViewBeanReport auditViewBeanReport;

    private Boolean encontrado = false;


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        int posicion = 0;

        if (value != null && value.trim().length() > 0) {
            try {

                encontrado = false;

                AuditViewBeanReport auditViewBeanReport = (AuditViewBeanReport) context.getApplication().getELResolver().getValue(context.getELContext(), null, "auditViewBeanReport");
                int numero = Integer.parseInt(value);
                int j = 0;
                 
                for (int i = 0; i < auditViewBeanReport.getListEntityOutputDTO().size(); i++) {

                    EntityOutputDTO e = auditViewBeanReport.getListEntityOutputDTO().get(i);
                    if (e.getIdEntity() == numero) {
                        posicion = j;
                    }

                    j++;
                }

                return auditViewBeanReport.getListEntityOutputDTO().get(posicion);

            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof EntityOutputDTO) {
            EntityOutputDTO entity = (EntityOutputDTO) value;
            if(entity.getNameEntity()!=null){
                return entity.getIdEntity().toString();
            }else{
                return "";
            }
           
        }
        return "";
    }

    public AuditViewBeanReport getAuditViewBeanReport() {
        return auditViewBeanReport;
    }

    public void setAuditViewBeanReport(AuditViewBeanReport auditViewBeanReport) {
        this.auditViewBeanReport = auditViewBeanReport;
    }

}
