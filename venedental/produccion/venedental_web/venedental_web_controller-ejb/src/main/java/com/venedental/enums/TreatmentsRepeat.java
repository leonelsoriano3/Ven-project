/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.enums;

/**
 *
 * @author AKDESKDEV90
 */
public enum TreatmentsRepeat {

    RADIOGRAFIA_PERIAPICAL(3), RADIOGRAFIA_CORONAL(4);

    Integer valor;

    private TreatmentsRepeat(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}