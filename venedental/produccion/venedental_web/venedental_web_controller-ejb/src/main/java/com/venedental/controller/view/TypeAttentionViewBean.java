package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.facade.TypeAttentionFacadeLocal;
import com.venedental.model.TypeAttention;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "typeAttentionViewBean")
@ViewScoped
public class TypeAttentionViewBean extends BaseBean {

    @EJB
    private TypeAttentionFacadeLocal typeAttentionFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    private static final long serialVersionUID = -5547104134772667835L;
    
    private TypeAttention newType;

    private Boolean cmbTypeAttention;
    
    private List<TypeAttention> listaTypeAttention;

    private List<TypeAttention> listaTypeAttentionFiltrada;

    private boolean visible;
    

    public TypeAttentionViewBean() {

        this.visible = false;
        this.cmbTypeAttention=true;
    }

    public TypeAttention getNewType() {
        
        return newType;
    }
    
    public void clearNewType(){
        this.newType = new TypeAttention();
        setNewType(newType);
    }

    public void setNewType(TypeAttention newType) {
        this.newType = newType;
    }
    
    
    
    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.getListaTypeAttention().addAll(this.typeAttentionFacadeLocal.findAll());
        this.getListaTypeAttentionFiltrada().addAll(this.getListaTypeAttention());
    }

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public TypeAttentionFacadeLocal getTypeAttentionFacadeLocal() {
        return typeAttentionFacadeLocal;
    }

    public void setTypeAttentionFacadeLocal(TypeAttentionFacadeLocal typeAttentionFacadeLocal) {
        this.typeAttentionFacadeLocal = typeAttentionFacadeLocal;
    }

    public Boolean getCmbTypeAttention() {
        return cmbTypeAttention;
    }

    public void setCmbTypeAttention(Boolean cmbTypeAttention) {
        this.cmbTypeAttention = cmbTypeAttention;
    }


    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<TypeAttention> getListaTypeAttention() {
        if (this.listaTypeAttention == null) {
            this.listaTypeAttention = new ArrayList<>();
        }

        return listaTypeAttention;
    }

    public void setListaTypeAttention(List<TypeAttention> listaTypeAttention) {
        this.listaTypeAttention = listaTypeAttention;
    }

    public List<TypeAttention> getListaTypeAttentionFiltrada() {
        if (this.listaTypeAttentionFiltrada == null) {
            this.listaTypeAttentionFiltrada = new ArrayList<>();
        }
        return listaTypeAttentionFiltrada;
    }

    public void setListaTypeAttentionFiltrada(List<TypeAttention> listaTypeAttentionFiltrada) {
        this.listaTypeAttentionFiltrada = listaTypeAttentionFiltrada;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }

}
