package com.venedental.controller.angular;


import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.specialist.NewPassword;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.services.ServiceCreateUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by leonelsoriano3@gmail.com on 18/03/16.
 */


@SessionScoped
@ApplicationPath("/resources")
@Path("updatePassword")
public class UpdatePassword extends Application {

    @EJB
    private ServiceCreateUser serviceCreateUser;


    @EJB
    private MailSenderLocal mailSenderLocal;


    @Path("newPassword")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public NewPassword newPassword(@QueryParam("newpasswordParam") String newpasswordParam,
                                   @QueryParam("previouspasswordParam") String previouspasswordParam) {
        NewPassword newPassword = new NewPassword();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        boolean passwordVerified = false;
        //password anterior no coincide
        //Integer myreturn = 0;
        newPassword.setNumber(0);


        passwordVerified = BCrypt.checkpw(previouspasswordParam, user.getPassword());

        if(passwordVerified){

            if(user.getEmail().length() == 0){
                //no tiene correo
                // myreturn  = 1;
                newPassword.setNumber(1);
            }else{
                this.sendEmail(user.getEmail(),user.getLogin(),newpasswordParam);
                serviceCreateUser.updatePassword(user.getId(),newpasswordParam);
                //myreturn = 2; //exitoso
                newPassword.setNumber(2);
            }
        }
        // return myreturn;
        return newPassword;
    }


    private String buildBodyEmail(String user,String password){
        StringBuilder content = new StringBuilder();
        content.append("<br/>" +
                "Estimado Usuario.<br/><br/>Se ha efectuado exitosamente el cambio de su contraseña.<br/><br/>" +
                "Su información de acceso es: <br/><br/>Usuario: ");
        content.append(user);
        content.append("<br/> Clave: ");
        content.append(password);
        content.append("<br/><br/>Siempre dispuestos a prestarle un mejor servicio.<br/><br/>" +
                "<b>Grupo Veneden</b>");
        return content.toString();
    }

    private void sendEmail(String email,String user,String password){

        String subject = "Grupo Veneden - Información cambio de contraseña";
        String title = "Información del cambio de contraseña";

        mailSenderLocal.send(Email.MAIL_CREDENCIALES,subject, title, buildBodyEmail(user,password),
                new ArrayList<File>(),email);
    }

}
