/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.application.TreatmentsApplicationBean;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.TreatmentsFacadeLocal;
import com.venedental.model.Plans;
import com.venedental.model.PlansTreatments;
import com.venedental.model.Treatments;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author CarlosDaniel
 */
@ManagedBean(name = "treatmentsViewBean")
@ViewScoped
public class TreatmentsViewBean extends BaseBean {
    
    @EJB
    private TreatmentsFacadeLocal treatmentsFacadeLocal;
    
    @ManagedProperty(value = "#{plansViewBean}")
    private PlansViewBean plansViewBean;
    
    @ManagedProperty(value = "#{treatmentsApplicationBean}")
    private TreatmentsApplicationBean treatmentsApplicationBean;
  
    private Treatments newTreatments;

    private String[] newTreatmentsString;

    private Treatments editITreatments;
    
    private List<Treatments> listaTreatments;
    
     private List<Treatments> listaTreatmentsSpecialist;
    
    private Treatments treatments;
    
    public TreatmentsViewBean() {
        this.newTreatments = new Treatments();
        this.newTreatmentsString = new String[3];
    }
    
    public Treatments getNewTreatments() {
        return newTreatments;
    }
    
    public void setNewTreatments(Treatments newTreatments) {
        this.newTreatments= newTreatments;
    }

    public String[] getNewTreatmentsString() {
        return newTreatmentsString;
    }

    public void setNewTreatmentsString(String[] newTreatmentsString) {
        this.newTreatmentsString = newTreatmentsString;
    }
    
    public List<Treatments> getListaTreatments() {
        if (this.listaTreatments == null) {
            this.listaTreatments = new ArrayList<>();
            this.listaTreatments = this.treatmentsFacadeLocal.findAll();
        }
        return listaTreatments;
    }
    
    public void setListaTreatments(List<Treatments> listaTreatments) {
        this.listaTreatments = listaTreatments;
    }
    
    public Treatments getEditTreatments() {
        return editITreatments;
    }
    
    public void setEditTreatments(Treatments editTreatments) {
        this.editITreatments = editTreatments;
    }

    public PlansViewBean getPlansViewBean() {
        return plansViewBean;
    }

    public void setPlansViewBean(PlansViewBean plansViewBean) {
        this.plansViewBean = plansViewBean;
    }

    public TreatmentsApplicationBean getTreatmentsApplicationBean() {
        return treatmentsApplicationBean;
    }

    public void setTreatmentsApplicationBean(TreatmentsApplicationBean treatmentsApplicationBean) {
        this.treatmentsApplicationBean = treatmentsApplicationBean;
    }

    public Treatments getTreatments() {
        return treatments;
    }

    public void setTreatments(Treatments treatments) {
        this.treatments = treatments;
    }   

    public List<Treatments> getListaTreatmentsSpecialist() {
        if (this.listaTreatmentsSpecialist == null) {
            this.listaTreatmentsSpecialist = new ArrayList<>();
        }

        return listaTreatmentsSpecialist;
    }

    public void setListaTreatmentsSpecialist(List<Treatments> listaTreatmentsSpecialist) {
        this.listaTreatmentsSpecialist = listaTreatmentsSpecialist;
    }
    
    
    
    
    
}
