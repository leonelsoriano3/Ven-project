package com.venedental.controller.angular;

import com.venedental.dto.User.UserDTO;
import com.venedental.dto.createUser.FindUserByCardIdOutput;
import com.venedental.dto.createUser.UpdateUserByTokenOutput;
import com.venedental.dto.emplooyes.EmployeeDTO;
import com.venedental.dto.specialist.SpecialistDTO;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceEmployee;
import com.venedental.services.ServiceSpecialists;
import com.venedental.services.ServiceUser;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.File;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


@ApplicationPath("/resources")
@Path("forgetPassword")
public class forgetPassword extends Application {

    @EJB
    private MailSenderLocal mailSenderLocal;

    @EJB
    private ServiceCreateUser serviceCreateUser;

    @EJB
    private ServiceUser serviceUser;


    @EJB
    private ServiceSpecialists serviceSpecialists;

    @EJB
    private ServiceEmployee serviceEmployee;

    private UserDTO userDTO;

    @Path("findUserByIdCard")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public FindUserByCardIdOutput findUserByIdCard(@QueryParam("cardId") Integer cardId) {
        FindUserByCardIdOutput specialist = serviceCreateUser.findSpecialistByCardId(cardId);
        if(specialist == null){
            specialist = new FindUserByCardIdOutput();
            specialist.setCompleteName("");
        }
        else if(specialist.getUsersId() == 0 ){
            specialist.setId(-1);
        }
        return specialist;
    }





    @Path("findUserByToken")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO findUserByToken(@QueryParam("token") String token,@Context HttpServletRequest req) {

        HttpSession session= req.getSession(true);

        UserDTO userDTO = serviceUser.findUserByToken(token);

        if(userDTO  == null){
            userDTO = new UserDTO();
            userDTO.setIdCard("");
            return userDTO;
        }



        Date today = new Date();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(userDTO.getExpiredDate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return new UserDTO();
        }
        Date tokenDate = c.getTime();

        SpecialistDTO specialist = serviceSpecialists.findSpecialistByUserid(userDTO.getId());


        if(specialist.getId() == null ){

            EmployeeDTO employeesDTO = serviceEmployee.findEmployeeByUserid(userDTO.getId());

            if(employeesDTO != null){
                userDTO.setCompleteNameSpecialist(employeesDTO.getCompleteName());
                userDTO.setIdCard(employeesDTO.getIdentityNumber().toString());
                session.setAttribute("sessionToken", userDTO.getToken());
            }
            else{
                return new UserDTO();
            }
        }
        else{
            userDTO.setCompleteNameSpecialist(specialist.getCompleteName());
            userDTO.setIdCard(specialist.getIdentityNumber().toString());
            session.setAttribute("sessionToken", userDTO.getToken());
        }


        return userDTO;
    }


    @Path("consumeToken")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean consumeToken(@QueryParam("idUs")Integer idUs,@QueryParam("newPassword")String newPassword,
                                @Context HttpServletRequest req){
        boolean makeDecision = true;

        UserDTO userDTO = serviceUser.findUserById(idUs);
        if(userDTO != null ){

            if(userDTO.getEmail() !=  null){
                this.sendEmailNewPassword(userDTO.getEmail(),userDTO.getLogin(),newPassword);
                serviceCreateUser.updatePassword(userDTO.getId(),newPassword);
            }

        }
        else{
            makeDecision = false;
        }

        return makeDecision;
    }


    @Path("sendData")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean sendData(@QueryParam("cardId") Integer cardId,@Context UriInfo uriInfo){


        FindUserByCardIdOutput specialist = serviceCreateUser.findSpecialistByCardId(cardId);
        if(specialist == null){
            return false;
        }
        UserDTO userDTO = serviceUser.findUserById(specialist.getUsersId());
        String token = this.generateToken();
        String link = this.generateLink(uriInfo,token);
        String expiredDate = generateDateExpired();

        try {
            serviceUser.updateToken(userDTO.getId(),token,expiredDate);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        sendEmail(userDTO.getEmail(),link);
        return true;
    }



    @Path("consumeTokenWhenClicking")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UpdateUserByTokenOutput consumeTokenWhenClicking(@QueryParam("token")String token){

        UserDTO userDTO = serviceUser.findUserByToken(token);
        UpdateUserByTokenOutput updateUserByTokenOutput = new UpdateUserByTokenOutput();

        try {

            updateUserByTokenOutput =  serviceUser.consumeTokenClicking(token);

        } catch (SQLException e) {
            System.out.println(e);
        }
        return updateUserByTokenOutput ;
    }



    private String generateDateExpired(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        return dateFormat.format(dt);
    }

    private String generateToken(){

        char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
        long unixTime = System.currentTimeMillis() / 1000L;
        StringBuilder token = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 22; i++) {
            char c = chars[random.nextInt(chars.length)];
            token.append(c);
        }
        token.append(unixTime);
        return  token.toString();
    }

    private String generateLink(UriInfo uriInfo,String token){
        String link = "http://"+ uriInfo.getAbsolutePath().getHost() +
                ":" + uriInfo.getAbsolutePath().getPort() +"/web/requestRecuperate.html?token=" + token;
        return link;
    }

    private String buildBodyEmail(String link){
        StringBuilder content = new StringBuilder();
        content.append("<br/>" +
                "Estimado Usuario.<br/><br/> Para cambiar su contraseña, haga clic en el siguiente enlace:" +
                "  ");
        content.append(link);
        content.append("<br/><br/>Siempre dispuestos a prestarle un mejor servicio <br/><br/>  <b> GrupoVeneden</b>");
        return content.toString();
    }

    private void sendEmail(String email,String link){

        String subject = "Grupo Veneden - Información para restablecer contraseña";
        String title = "Información para restablecer contraseña";

        mailSenderLocal.send(Email.MAIL_CREDENCIALES,subject, title, buildBodyEmail(link),
                new ArrayList<File>(),email);
    }

    private String buildBodyEmailNewPassword(String user,String password){

        StringBuilder text = new StringBuilder();
        text.append("Estimado usuario.<br/>");
        text.append("Se ha efectuado exitosamente el cambio de  su contraseña<br/><br/>");
        text.append("Usuario: ");
        text.append(user);
        text.append("<br/> Clave: ");
        text.append(password);
        text.append("<br/><br/>Siempre dispuestos a prestarle un mejor servicio.<br/><br/>");
        text.append("<b>Grupo VENEDEN</b>");
        return  text.toString();
    }


    private void sendEmailNewPassword(String email,String user,String password){
        String subject = "Grupo Veneden - Información del cambio de nueva contraseña";
        String title = "Información del cambio de nueva contraseña";
        mailSenderLocal.send(Email.MAIL_CREDENCIALES,subject, title, this.buildBodyEmailNewPassword(user,password),
                new ArrayList<File>(),email);

    }

}
