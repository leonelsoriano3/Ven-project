/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.constans.ConstansReportRetention;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dto.analysisKeys.ConsultPaymentProcessOutput;
import com.venedental.dto.paymentReport.ConsultRetentionDetailsOutput;
import com.venedental.dto.paymentReport.ConsultTotalBodyRetentionOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.services.ServicePaymentReport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.model.DefaultStreamedContent;
import services.utils.Transformar;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "ReportProofRetentionViewBean")
@RequestScoped
public class ReportProofRetentionViewBean {

    /* Payment Report */
    private PaymentReportOutput paymentReportOutput;

    private Map<String, Object> params = new HashMap<String, Object>();

    private String logoPath;

    private JRBeanCollectionDataSource beanCollectionDataSource;

    private String reportPath;

    private Date startDate;


    /* ManagedBeans Property */
    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;

    /* Services */
    @EJB
    private ServicePaymentReport servicePaymentReport;

    List<ConsultTotalBodyRetentionOutput> consultTotalBodyRetentionOutput;
    List<ConsultRetentionDetailsOutput> consultRetentionDetailsOutput;

    JasperPrint jasperPrint;

    private ConsultPaymentProcessOutput dataSelected;

    private DefaultStreamedContent download;

    /* Getter y Setter */
    public List<ConsultRetentionDetailsOutput> getConsultRetentionDetailsOutput() {
        return consultRetentionDetailsOutput;
    }

    public void setConsultRetentionDetailsOutput(List<ConsultRetentionDetailsOutput> consultRetentionDetailsOutput) {
        this.consultRetentionDetailsOutput = consultRetentionDetailsOutput;
    }

    public ConsultPaymentProcessOutput getDataSelected() {
        return dataSelected;
    }

    public void setDataSelected(ConsultPaymentProcessOutput dataSelected) {
        this.dataSelected = dataSelected;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public DefaultStreamedContent getDownload() {
        return download;
    }

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public PaymentReportOutput getPaymentReportOutput() {
        return paymentReportOutput;
    }

    public void setPaymentReportOutput(PaymentReportOutput paymentReportOutput) {
        this.paymentReportOutput = paymentReportOutput;
    }

    public List<ConsultTotalBodyRetentionOutput> getConsultTotalBodyRetentionOutput() {
        return consultTotalBodyRetentionOutput;
    }

    public void setConsultTotalBodyRetentionOutput(List<ConsultTotalBodyRetentionOutput> consultTotalBodyRetentionOutput) {
        this.consultTotalBodyRetentionOutput = consultTotalBodyRetentionOutput;
    }

    /*-------------------------------------------------------
     *
     *                      Funcion para 
     *              Registrar Comprobante de retencion
     *   
     *   -----------------------------------------------------
     */
    public void registerWithholding() {
        //contenido
    }

    /*-------------------------------------------------------
     *
     *               Funcion para contruir el
     *                       documento PDF
     *
     *-------------------------------------------------------
     */

    public void loadDataToBuildPDF(ConsultPaymentProcessOutput obj) throws IOException, JRException, SQLException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        System.out.println("Valores "+obj.getIdReport());

        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        setStartDate(new Date());

        this.setPaymentReportOutput(this.servicePaymentReport.consultPaymentReportById(obj.getIdReport()));
        String dateStarExport = sdfSolicitud.format(this.getStartDate());
        this.setConsultTotalBodyRetentionOutput(servicePaymentReport.consultTotalBodyRetention(this.getPaymentReportOutput().getIdSpecialist(), this.getPaymentReportOutput().getNumberRetentionISLR()));
        this.setConsultRetentionDetailsOutput(servicePaymentReport.consultRetentionDetails(this.getPaymentReportOutput().getIdSpecialist(), this.getPaymentReportOutput().getNumberRetentionISLR()));
        
        //Parametros para enviar al pdf
        this.params.put("listaPago", new JRBeanCollectionDataSource(this.getConsultRetentionDetailsOutput()));
        Integer sizeDetail=this.getConsultRetentionDetailsOutput().size();
        
        this.params.put("sizeDetail", sizeDetail);
        this.assignData(getConsultTotalBodyRetentionOutput().get(0));
        this.params.put("heal_logo2", this.logoPath);
        this.params.put("date", dateStarExport);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getConsultTotalBodyRetentionOutput());
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/keysReportProofRetention/ReportProofRetention.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    /*-------------------------------------------------------
     *
     *               Funcion para contruir el
     *                   Descargar PDF
     *
     *-------------------------------------------------------
     */
    public void assignData(ConsultTotalBodyRetentionOutput data) throws IOException {
        
        
        this.params.put("companyName", data.getCompanyName());
        this.params.put("companyDir", data.getCompanyAddress());
        this.params.put("companyRif", (data.getCompanyRif()==null)?("-"):(data.getCompanyRif()));
        this.params.put("specialistName", data.getSpecialistName());
        this.params.put("specialistDir", data.getSpecialistAddress());
        this.params.put("specialistRif", (data.getSpecialistRif()==null)?("-"):(data.getSpecialistRif()));
        StringBuilder stringBuilder = new StringBuilder();
        String typePerson = null;
        if (data.getSpecialistRif() != null) {
            Character typeRif = data.getSpecialistRif().charAt(0);
              if (typeRif.equals('V')) {

                stringBuilder.append(readFileProperties().getProperty(ConstansReportRetention.PROPERTY_TYPE_PERSON_NATURAL));
                typePerson = stringBuilder.toString();
            }
              else if (typeRif.equals('E')) {
                stringBuilder.append(readFileProperties().getProperty(ConstansReportRetention.PROPERTY_TYPE_PERSON_NATURAL));
                typePerson = stringBuilder.toString();
            } 
            else if (typeRif.equals('J')) {
                stringBuilder.append(readFileProperties().getProperty(ConstansReportRetention.PROPERTY_TYPE_PERSON_JURIDICAL));
                typePerson = stringBuilder.toString();
            } 
            else if (typeRif.equals('G')) {

                stringBuilder.append(readFileProperties().getProperty(ConstansReportRetention.PROPERTY_TYPE_PERSON_JURIDICAL));
                typePerson = stringBuilder.toString();
            }
           
        } 
        this.params.put("professionalFess",typePerson);
        this.params.put("retentionConcept", data.getRetentionConcept());
        this.params.put("dateISLR", data.getDateISLR());
        this.params.put("numberRetention", data.getNumberRetentionISLR());
        this.params.put("amountTotal", data.getTotalAmountTotal());
        this.params.put("baseRetention", data.getTotalBaseRetention());
        System.out.println("Valor: "+data.getTotalBaseRetention());
        System.out.println("Monto total retencion: "+data.getTotalAmountRetentionISLR());
        this.params.put("amountTotalRetention", data.getTotalAmountRetentionISLR());
        this.params.put("payment", data.getNetPay());
        this.params.put("totalMontoSustraendo", data.getTotalMontoSustraendo());
        this.params.put("TotalNetoRetention", data.getTotalNetoRetention());

    }
    public void downloadPDF(ConsultPaymentProcessOutput obj) throws JRException, IOException, Exception {
        String fileName;
        fileName = "Reporte";
        this.loadDataToBuildPDF(obj);
        this.export.pdf(jasperPrint, fileName, Transformar.getThisDate());
    }
    
     /**
     * Method to read the properties file constants retention reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansReportRetention.FILENAME_RETENTION_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansReportRetention.FILENAME_RETENTION_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

}
