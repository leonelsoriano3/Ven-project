/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.facade.BinnacleKeysFacadeLocal;

import com.venedental.facade.BinnaclePlansTreatmentsKeysFacadeLocal;

import com.venedental.model.BinnacleKeys;

import com.venedental.model.BinnaclePlansTreatmentsKeys;
import com.venedental.model.KeysPatients;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.StatusAnalysisKeys;

import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "binnaclesRequestBean")
@RequestScoped
public class BinnaclesRequestBean extends BaseBean implements Serializable {

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @EJB
    private BinnaclePlansTreatmentsKeysFacadeLocal binnaclePlansTreatmentsKeysFacadeLocal;

    @EJB
    private BinnacleKeysFacadeLocal binnacleKeysFacadeLocal;



    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public void updateBinnaclePlansTratmentKeys(StatusAnalysisKeys status, PlansTreatmentsKeys plansTreatmentsKeys) {
        Date dateNow = new Date();
        /*Creating the BinnaclePlansTreatmentsKeys*/
        BinnaclePlansTreatmentsKeys binnaclePlansTreatmentKeys = new BinnaclePlansTreatmentsKeys();
        binnaclePlansTreatmentKeys.setIdUsers(this.securityViewBean.obtainUser());
        binnaclePlansTreatmentKeys.setIdStatusPlans(status);
        binnaclePlansTreatmentKeys.setDateBinnacleInitiation(dateNow);
        binnaclePlansTreatmentKeys.setDateBinnacleEnd(dateNow);
        binnaclePlansTreatmentKeys.setIdPlansTreatmentsKeys(plansTreatmentsKeys);
        this.binnaclePlansTreatmentsKeysFacadeLocal.create(binnaclePlansTreatmentKeys);

    }

    public void updateBinnacleKeys(StatusAnalysisKeys status, KeysPatients keysPatients) {       
        Date dateNow = new Date();
        /*Creating the BinnaclePlansTreatmentsKeys*/
        BinnacleKeys binnacleKeys = new BinnacleKeys();
        binnacleKeys.setIdUsersKeys(this.securityViewBean.obtainUser());
        binnacleKeys.setIdStatusKeys(status);
        binnacleKeys.setDateBinnacleInitiation(dateNow);
        binnacleKeys.setDateBinnacleEnd(dateNow);
        binnacleKeys.setIdKeys(keysPatients);
        this.binnacleKeysFacadeLocal.create(binnacleKeys);

    }

}
