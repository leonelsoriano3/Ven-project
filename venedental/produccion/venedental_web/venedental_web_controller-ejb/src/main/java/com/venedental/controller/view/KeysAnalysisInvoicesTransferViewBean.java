package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dto.analysisKeys.ConsultPaymentProcessOutput;
import com.venedental.dto.analysisKeys.GenerateTxtOutput;
import com.venedental.dto.analysisKeys.ReportAuditedAnyStateOutPut;
import com.venedental.dto.paymentReport.ConfirmKeyDetailsInvoicesTransferOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.Reimbursement;
import com.venedental.model.StatusAnalysisKeys;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServicePaymentReport;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "keysInvoicesTransferViewBean")
@ViewScoped
public class KeysAnalysisInvoicesTransferViewBean extends BaseBean implements Serializable {

    /* Attributes */
    private List<StatusAnalysisKeys> statusAnalysisKeysList;

    private List<ConsultPaymentProcessOutput> invoicesTransferKeysList;
    private List<ConsultPaymentProcessOutput> invoicesTransferKeysListFiltered;
    private ConsultPaymentProcessOutput consultPaymentProcessOutputSelected;
    private List<GenerateTxtOutput> generateTxtOutputList;

    /* Payment Report */
    private PaymentReportOutput paymentReportOutput;

    /* Confirm Dialog Keys data */
    private List<ConfirmKeyDetailsInvoicesTransferOutput> ConfirmKeyDetailsInvoicesTransferOutputList;
    private ConfirmKeyDetailsInvoicesTransferOutput confirmKeyDetailsInvoicesTransferOutputSelected;

    /* Buttons Visibility */
    private boolean columnVisibilityStatusKeys;
    /* Buttons Visibility */
    private boolean btnVisibilityDataInvoicesConfirm;
    private boolean btnVisibilityDataInvoicesDownload;

    /* Visiility row numer control in dialog invoce transfer */
    private boolean visibilityRow;

    /* Filter Visibility */
    private boolean columnFilterActive;

    /* Enabled and Disabled Input Text Number Control */
    private boolean inputEnabledDisabledNumberControl, btnActiveDownloadRetentions;

    /* Habilitar si el campo numero de control es obligatorio o no*/
    private boolean enableNumberControl;
    /* Manejo del boton Generar Txt*/
    private boolean btnGenerateTxt;

    /* Data Register Transfer */
    private Date dateInvoices;
    private String[] invoicesRegister;

    /* Data Transfer */
    private Date dateTransfer;
    private String numberTransfer;

    /* Others */
    private Integer statusKeysid,numero=12;
    private String currentDate, minDate;
    private String maskNumberControl;
    private StreamedContent fileRequirement;

    private DefaultStreamedContent txt;

    private Boolean isEmptyTable;

    /* ManagedBean Properties */
    /* Services */
    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;

    @EJB
    private ServicePaymentReport servicePaymentReport;

    /* Constructs */
    public KeysAnalysisInvoicesTransferViewBean() {

        this.invoicesRegister = new String[3];
    }

    @PostConstruct
    public void init() {

        this.isEmptyTable = true;
        // --> Find All Keys Invoices 
        this.findKeysInvoicesTransferByTwoStatus();
        // this.findAllKeysAnalysisInvoicesTransfer(EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor());
        // --> Find Status Analysis Keys
        this.findStatusAnalysisKeys();

        this.setEnableNumberControl(false);
        this.maskNumberControl = "00-";
        this.enableNumberControl = true;
        this.inputEnabledDisabledNumberControl = false;

        this.visibilityRow = false;

        // Range date 
        dateLimit();
        
        this.setBtnGenerateTxt(true);
    }

    public String getMaskNumberControl() {
        return maskNumberControl;
    }

    public void setMaskNumberControl(String maskNumberControl) {
        this.maskNumberControl = maskNumberControl;
    }

    public boolean isEnableNumberControl() {
        return enableNumberControl;
    }

    public void setEnableNumberControl(boolean enableNumberControl) {
        this.enableNumberControl = enableNumberControl;
    }

    public boolean getVisibilityRow() {
        return visibilityRow;
    }

    /* Getter and Setter */
    public void setVisibilityRow(boolean visibilityRow) {
        this.visibilityRow = visibilityRow;
    }

    public List<StatusAnalysisKeys> getStatusAnalysisKeysList() {
        return statusAnalysisKeysList;
    }

    public void setStatusAnalysisKeysList(List<StatusAnalysisKeys> statusAnalysisKeysList) {
        this.statusAnalysisKeysList = statusAnalysisKeysList;
    }

    public List<ConsultPaymentProcessOutput> getInvoicesTransferKeysList() {
        return invoicesTransferKeysList;
    }

    public void setInvoicesTransferKeysList(List<ConsultPaymentProcessOutput> invoicesTransferKeysList) {
        this.invoicesTransferKeysList = invoicesTransferKeysList;
    }

    public boolean isBtnVisibilityDataInvoicesDownload() {
        return btnVisibilityDataInvoicesDownload;
    }

    public void setBtnVisibilityDataInvoicesDownload(boolean btnVisibilityDataInvoicesDownload) {
        this.btnVisibilityDataInvoicesDownload = btnVisibilityDataInvoicesDownload;
    }

    public Integer getStatusKeysid() {
        return statusKeysid;
    }

    public void setStatusKeysid(Integer statusKeysid) {
        this.statusKeysid = statusKeysid;
    }

    public List<ConsultPaymentProcessOutput> getInvoicesTransferKeysListFiltered() {
        return invoicesTransferKeysListFiltered;
    }

    public void setInvoicesTransferKeysListFiltered(List<ConsultPaymentProcessOutput> invoicesTransferKeysListFiltered) {
        this.invoicesTransferKeysListFiltered = invoicesTransferKeysListFiltered;
    }

    public boolean isColumnVisibilityStatusKeys() {
        return columnVisibilityStatusKeys;
    }

    public void setColumnVisibilityStatusKeys(boolean columnVisibilityStatusKeys) {
        this.columnVisibilityStatusKeys = columnVisibilityStatusKeys;
    }

    public ConsultPaymentProcessOutput getConsultPaymentProcessOutputSelected() {
        return consultPaymentProcessOutputSelected;
    }

    public void setConsultPaymentProcessOutputSelected(ConsultPaymentProcessOutput consultPaymentProcessOutputSelected) {
        this.consultPaymentProcessOutputSelected = consultPaymentProcessOutputSelected;
    }

    public boolean isBtnVisibilityDataInvoicesConfirm() {
        return btnVisibilityDataInvoicesConfirm;
    }

    public void setBtnVisibilityDataInvoicesConfirm(boolean btnVisibilityDataInvoicesConfirm) {
        this.btnVisibilityDataInvoicesConfirm = btnVisibilityDataInvoicesConfirm;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public PaymentReportOutput getPaymentReportOutput() {
        return paymentReportOutput;
    }

    public void setPaymentReportOutput(PaymentReportOutput paymentReportOutput) {
        this.paymentReportOutput = paymentReportOutput;
    }

    /* Data Invoices */
    public Date getDateInvoices() {
        return dateInvoices;
    }

    public void setDateInvoices(Date dateInvoices) {
        this.dateInvoices = dateInvoices;
    }

    public String[] getInvoicesRegister() {
        return invoicesRegister;
    }

    public void setInvoicesRegister(String[] invoicesRegister) {
        this.invoicesRegister = invoicesRegister;
    }

    /* Data Transfer */
    public Date getDateTransfer() {
        return dateTransfer;
    }

    public void setDateTransfer(Date dateTransfer) {
        this.dateTransfer = dateTransfer;
    }

    public String getNumberTransfer() {
        return numberTransfer;
    }

    public void setNumberTransfer(String numberTransfer) {
        this.numberTransfer = numberTransfer;
    }

    /* Confirm detail keys dialogs */
    public List<ConfirmKeyDetailsInvoicesTransferOutput> getConfirmKeyDetailsInvoicesTransferOutputList() {
        return ConfirmKeyDetailsInvoicesTransferOutputList;
    }

    public void setConfirmKeyDetailsInvoicesTransferOutputList(List<ConfirmKeyDetailsInvoicesTransferOutput> ConfirmKeyDetailsInvoicesTransferOutputList) {
        this.ConfirmKeyDetailsInvoicesTransferOutputList = ConfirmKeyDetailsInvoicesTransferOutputList;
    }
//ojo
    public ConfirmKeyDetailsInvoicesTransferOutput getConfirmKeyDetailsInvoicesTransferOutputSelected() {
        return confirmKeyDetailsInvoicesTransferOutputSelected;
    }

    public void setConfirmKeyDetailsInvoicesTransferOutputSelected(ConfirmKeyDetailsInvoicesTransferOutput confirmKeyDetailsInvoicesTransferOutputSelected) {
        confirmKeyDetailsInvoicesTransferOutputSelected.setTotalAmountPaid(this.ConfirmKeyDetailsInvoicesTransferOutputList.get(0).getTotalAmountPaid());
        this.confirmKeyDetailsInvoicesTransferOutputSelected = confirmKeyDetailsInvoicesTransferOutputSelected;
    }

    public boolean isInputEnabledDisabledNumberControl() {
        return inputEnabledDisabledNumberControl;
    }

    public void setInputEnabledDisabledNumberControl(boolean inputEnabledDisabledNumberControl) {
        this.inputEnabledDisabledNumberControl = inputEnabledDisabledNumberControl;
    }

    public boolean isBtnActiveDownloadRetentions() {
        return btnActiveDownloadRetentions;
    }

    public void setBtnActiveDownloadRetentions(boolean btnActiveDownloadRetentions) {
        this.btnActiveDownloadRetentions = btnActiveDownloadRetentions;
    }

    public boolean isColumnFilterActive() {
        return columnFilterActive;
    }

    public void setColumnFilterActive(boolean columnFilterActive) {
        this.columnFilterActive = columnFilterActive;
    }
      /* Manejo del boton Generar Txt */
    
    public boolean isBtnGenerateTxt() {
        return btnGenerateTxt;
    }

    public void setBtnGenerateTxt(boolean btnGenerateTxt) {
        this.btnGenerateTxt = btnGenerateTxt;
    }
    
    public DefaultStreamedContent getTxt() {
        return txt;
    }

    public void setTxt(DefaultStreamedContent txt) {
        this.txt = txt;
    }

    public List<GenerateTxtOutput> getGenerateTxtOutputList() {
        return generateTxtOutputList;
    }

    public void setGenerateTxtOutputList(List<GenerateTxtOutput> generateTxtOutputList) {
        this.generateTxtOutputList = generateTxtOutputList;
    }
    
    

    /**
     *  *-------------------------------------------------------------------- * | METHODS IMPLEMENTS IN VIEW BEANS |
     * KEYS ANALYSIS REGISTER | TRANSFER INVOICES * -------------------------------------------------------------------
     * *
     */
    /**
     * Method lists all the status of each key to confirm
     *
     *
     */
    public void findStatusAnalysisKeys() {
        this.setStatusAnalysisKeysList(this.serviceAnalysisKeys.findAllStatusAnalysisKeys());
        List<StatusAnalysisKeys> statusAnalysisKeysAux = new ArrayList<>();
        if (!this.statusAnalysisKeysList.isEmpty()) {
            for (StatusAnalysisKeys analysisKeys : this.statusAnalysisKeysList) {
                if (Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.POR_ABONAR.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.ABONADO.getValor())) {
                    statusAnalysisKeysAux.add(analysisKeys);
                }
            }
        }
        this.setStatusAnalysisKeysList(statusAnalysisKeysAux);
    }

    /**
     *
     * Method lists all the status of each key invoices transfer
     *
     */
    public void findAllKeysAnalysisInvoicesTransfer() {

        try {
            if (this.getStatusKeysid() != null) {
                switch (this.getStatusKeysid()) {

                    case 6:

                        this.resetDataTableKeysInvoicesTransfer();
                        // setter list                        
                        this.setInvoicesTransferKeysList(this.serviceAnalysisKeys.consultPaymentProcess(
                                EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor(), null, null));
                        this.setInvoicesTransferKeysListFiltered(this.getInvoicesTransferKeysList());
                        this.setBtnGenerateTxt(true);
                        RequestContext.getCurrentInstance().update("form:btnGenerateTxt");

                        if (this.getInvoicesTransferKeysList().isEmpty()) {

                            // Disabled Filter
                            this.setColumnFilterActive(true);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(false);
                            this.setColumnVisibilityStatusKeys(false);
                        } else {

                            // Disabled Filter
                            this.setColumnFilterActive(false);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(false);
                            this.setColumnVisibilityStatusKeys(false);
                        }

                        break;
                    case 12:

                        this.resetDataTableKeysInvoicesTransfer();
                        this.setInvoicesTransferKeysListFiltered(new ArrayList<ConsultPaymentProcessOutput>());
                        this.setInvoicesTransferKeysList(this.serviceAnalysisKeys.consultPaymentProcess(
                                EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), null, null));
                        this.setInvoicesTransferKeysListFiltered(this.getInvoicesTransferKeysList());
                        this.setBtnGenerateTxt(false);
                        RequestContext.getCurrentInstance().update("form:btnGenerateTxt");

                        if (this.getInvoicesTransferKeysList().isEmpty()) {
                            this.setBtnGenerateTxt(true);
                        RequestContext.getCurrentInstance().update("form:btnGenerateTxt");
                            // Disabled Filter
                            this.setColumnFilterActive(true);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(false);
                            this.setColumnVisibilityStatusKeys(false);
                        } else {

                            // Disabled Filter
                            this.setColumnFilterActive(false);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(false);
                            this.setColumnVisibilityStatusKeys(false);
                        }

                        break;
                    case 13:

                        this.resetDataTableKeysInvoicesTransfer();
                        // setter list
                        this.setInvoicesTransferKeysList(this.serviceAnalysisKeys.consultPaymentProcess(
                                EstatusPlansTreatmentsKeys.ABONADO.getValor(), null, null));
                        this.setInvoicesTransferKeysListFiltered(this.getInvoicesTransferKeysList());
                        this.setBtnGenerateTxt(true);
                        RequestContext.getCurrentInstance().update("form:btnGenerateTxt");

                        if (this.getInvoicesTransferKeysList().isEmpty()) {

                            // Disabled Filter
                            this.setColumnFilterActive(true);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(true);
                            this.setColumnVisibilityStatusKeys(false);
                        } else {
                            // Disabled Filter
                            this.setColumnFilterActive(false);

                            this.setBtnVisibilityDataInvoicesConfirm(true);
                            this.setBtnVisibilityDataInvoicesDownload(true);
                            this.setColumnVisibilityStatusKeys(false);
                        }

                        break;

                }
            } else {
                findKeysInvoicesTransferByTwoStatus();
            }
            this.isEmptyTable = invoicesTransferKeysList.isEmpty();
            RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");
            RequestContext.getCurrentInstance().update("form");

        } catch (SQLException ex) {
            Logger.getLogger(KeysAnalysisInvoicesTransferViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Method for verify enabled button retentions
     *
     * @param obj
     * @return
     */
    public boolean enableBtnRetention(ConsultPaymentProcessOutput obj) {

        if (obj.numberRetention.isEmpty()) {
            return true;
        } else {
            return false;
        }

    }

    public String returnTitle(ConsultPaymentProcessOutput obj) {

        if (obj.numberRetention.isEmpty()) {
            return "No aplica retención";
        } else {
            return "Comprobante de retención";
        }

    }

    /**
     *
     * Method lists all the keys analysis of each key invoices transfer by status por facturar y abonar.
     *
     */
    public void findKeysInvoicesTransferByTwoStatus() {

        try {
            // Setter and List by Service            

            this.setInvoicesTransferKeysList(this.serviceAnalysisKeys.consultPaymentProcessByTwoStatus(EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor(), EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), null, null));

            if (this.getInvoicesTransferKeysList().isEmpty()) {

                // Disabled Filter
                this.setColumnFilterActive(true);

                this.setColumnVisibilityStatusKeys(true);
                this.setBtnVisibilityDataInvoicesConfirm(true);
                this.setBtnVisibilityDataInvoicesDownload(false);

            } else {

                // Disabled Filter
                this.setColumnFilterActive(false);

                this.setColumnVisibilityStatusKeys(true);
                this.setBtnVisibilityDataInvoicesConfirm(true);
                this.setBtnVisibilityDataInvoicesDownload(false);

            }

        } catch (SQLException ex) {
            Logger.getLogger(KeysAnalysisInvoicesTransferViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * expotar a excel
     * @throws net.sf.jasperreports.engine.JRException
     */
    public void exportStateToXlsx() throws JRException {

        String nameReport = new String();
        JasperPrint jasperPrint;
        ConfigViewBeanReport export = new ConfigViewBeanReport();
        JRBeanCollectionDataSource beanCollectionDataSource;
        String logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        String reportPath = "";

        Map<String, Object> params = new HashMap<>();
        params.put("heal_logo2", logoPath);

        try {
            List<ReportAuditedAnyStateOutPut> reportAuditedAnyState = serviceAnalysisKeys.findReportAuditedAnyState(this.getStatusKeysid(), null, null);

            reportAuditedAnyState.get(0).getDateMonth();

            for (int i = 0; i < reportAuditedAnyState.size(); i++) {

                double amountSum = reportAuditedAnyState.get(i).getAmountNet();

                double amountSumISLR = reportAuditedAnyState.get(i).getAmountRetentionISLR();

                for (int j = 0; j < reportAuditedAnyState.size(); j++) {

                    if (j == i) {
                        //=> TODO quitar esta condicion
                    } else if ((reportAuditedAnyState.get(i).getIdCardSpecialist().equals(
                            reportAuditedAnyState.get(j).getIdCardSpecialist()
                    ))
                            && (reportAuditedAnyState.get(i).getDateMonth().equals(
                            reportAuditedAnyState.get(j).getDateMonth()))) {

                        amountSum += reportAuditedAnyState.get(j).getAmountNet();

                        amountSumISLR += reportAuditedAnyState.get(j).getAmountRetentionISLR();
                    }

                }
                reportAuditedAnyState.get(i).setSumAmountNet(amountSum);
                reportAuditedAnyState.get(i).setSumAmountRetentionISLR(amountSumISLR);
            }

            beanCollectionDataSource = new JRBeanCollectionDataSource(reportAuditedAnyState);
            // params.put("reportAuditedAnyState", new JRBeanCollectionDataSource( reportAuditedAnyState  ));
            //beanCollectionDataSource = new JRBeanCollectionDataSource( invoicesTransferKeysList  );

            params.put("reportKeysAnalisysList1", new JRBeanCollectionDataSource(reportAuditedAnyState));
            switch (this.getStatusKeysid()) {

                case 6:
                    params.put("reportTitle", "REPORTE DE REGISTROS DE AUDITORÍAS EN ESTADO POR FACTURAR");
                    reportPath = FacesContext.getCurrentInstance().getExternalContext().
                            getRealPath("/reports/keysAnalysisInvoicesTransfer/"
                                    + "ReportKeyAnalysisInvoicesTransferInvoice.jasper");
                    nameReport = "ReporteProcesarPagoPorFactuar";

                    break;
                case 12:
                    params.put("reportTitle", "REPORTE DE REGISTROS DE AUDITORÍAS EN ESTADO POR ABONAR");
                    reportPath = FacesContext.getCurrentInstance().getExternalContext().
                            getRealPath("/reports/keysAnalysisInvoicesTransfer/"
                                    + "ReportKeyAnalysisInvoicesTransferToPay.jasper");
                    nameReport = "ReporteProcesarPagoPorAbonar";
                    break;
                case 13:
                    params.put("reportTitle", "REPORTE DE REGISTROS DE AUDITORÍAS EN ESTADO ABONADO");
                    reportPath = FacesContext.getCurrentInstance().getExternalContext().
                            getRealPath("/reports/keysAnalysisInvoicesTransfer/"
                                    + "ReportKeyAnalysisInvoicesTransferPaid.jasper");

                    nameReport = "ReporteProcesarPagoAbonado";
                    fillHaveRetentionReportAuditedAnyState(reportAuditedAnyState);
                    break;

            }

            jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
            export.xlsx(jasperPrint, nameReport, "");
        } catch (IOException ex) {
            Logger.getLogger(KeysAnalysisInvoicesTransferViewBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(KeysAnalysisInvoicesTransferViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }




    /**
     * llena la variable de AuditedAnyState haveRetention se crea para poder tener estos valores en el reporte en estado
     * abonado status(13)
     * @param val
     */
    public void fillHaveRetentionReportAuditedAnyState(List<ReportAuditedAnyStateOutPut> val) {

        for (int i = 0; i < val.size(); i++) {

            for (int j = 0; j < val.size(); j++) {
                if (j != i) {

                    /*TODO hay que quitar esto esta colocado por un error en la validacion que llegan estos valores null*/
                    if(val.get(i).getDateInvoice() == null || val.get(j).getDateInvoice() == null)
                            continue;
                    
                    //conseguir las fechas en formato yyyy-MM-dd
                    java.util.Date sqlDateActual = new java.util.Date(val.get(i).getDateInvoice().getTime());
                    java.util.Date sqlDateTmp = new java.util.Date(val.get(j).getDateInvoice().getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                    String dateActualStr = df.format(sqlDateActual);
                    String dateTmp = df.format(sqlDateTmp);

                    if ((dateActualStr.equals(dateTmp))
                            && val.get(i).getIdCardSpecialist().equals(val.get(j).getIdCardSpecialist())) {
                        val.get(i).setHaveRetention(true);
                        break;
                    } else {
                        val.get(i).setHaveRetention(false);
                    }

                }

            }
        }

    }

    /**
     *
     * Method and reset Datatables
     *
     */
    public void resetDataTableKeysInvoicesTransfer() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:keysInvoicesTransferTable");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:keysInvoicesTransferTable");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");
        }
    }

    /**
     * Method to set Date limit, range: [Year-1,Year]
     */
    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int minYear = year - 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setCurrentDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
    }

    /**
     * Method at converter dateUtil and dateSql
     *
     * @param date
     * @return
     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }
    
//     /**
//     * Funcion que permite generar y descargar un txt
//     *
//     * @throws java.io.IOException
//     */
    public void generateReportTxt() throws IOException {

        String route = "/home/ftp/reimbursement/txt/documentBack.txt";
        File archivo = new File(route);
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo));
        DecimalFormat decf = new DecimalFormat("##.00");
        
        this.setGenerateTxtOutputList(this.serviceAnalysisKeys.ConfirmGenerateTxtOutput(statusKeysid,null,null));

        for (GenerateTxtOutput generateTxtOutput: this.getGenerateTxtOutputList()) {
            String monto=generateTxtOutput.getMonto_pagar_entero()+generateTxtOutput.getMonto_pagar_decimal();
           
            
            int tmp = 35-generateTxtOutput.getNombre_especialista().length();
            
            for (int i = 0; i < tmp; i++) {
                generateTxtOutput.setNombre_especialista(generateTxtOutput.getNombre_especialista() + " ");
            }
            
            
            
            bw.write(generateTxtOutput.getNumero_cuenta()
                    + " " + generateTxtOutput.getCedula_especialista()
                    + " " +monto 
                    + " " + generateTxtOutput.getReferencia_pago()
                    + " " + generateTxtOutput.getNombre_especialista()
                    + " " +generateTxtOutput.getEmail_especialista()+ "\r\n");
        }
        bw.close();
        InputStream stream = new FileInputStream(route);
        this.setTxt(new DefaultStreamedContent(stream, "application/txt", "TxtParaNetcash.txt"));
    }


    public Boolean getIsEmptyTable() {
        return isEmptyTable;
    }

    public void setIsEmptyTable(Boolean emptyTable) {
        isEmptyTable = emptyTable;
    }
}
