package com.venedental.constans;

/**
 * Created by akdesk10 on 21/10/16.
 */
public class ConstansTreatmentsReport {

    public static String PROPERTY_PATH_LOGO = "venedental_web-web-1.1.1_war/recursos/img/heal_logo2.png";

    public static String PROPERTY_TREATMENT_REPORT ="venedental_web-web/src/main/webapp/reports/specialist/treatmentReport.jasper";

    public static String PROPERTY_PATH_REPORT = "directoryTreatmentReport.path";

    public static String FILENAME_TREATMENT_REPORT = "treatmentsReport.properties";

    public static String PROPERTY_PATH_JASPER = "directoryTreatmentReport.pathJasper";

    public static String PROPERTY_PATH  = "directoryTreatmentReport.path";

    public static String PROPERTY_P = "venedental_web-web-1.1.1_war/reports/specialist/";
    
     public static String PROPERTY_EMAIL_TITLE_REPORT = "emailReport.titleEpecialist";
     
     public static String PROPERTY_REPORT_EXTENSION="reportReport.extension";
             
/*
* #directory.pathJasper=venedental_web-web/src/main/webapp/reports/specialist/treatmentReport.jasper
directoryTreatmentReport.pathJasper=venedental_web-web-1.1.1_war/reports/specialist/treatmentReport.jasper

#directory.pathLogo = venedental_web-web/src/main/webapp/recursos/img/heal_logo2.png
directoryTreatmentReport.pathLogo = venedental_web-web-1.1.1_war/recursos/img/heal_logo2.png


directoryTreatmentReport.path=venedental_web-web-1.1.1_war/reports/specialist/*/
}
