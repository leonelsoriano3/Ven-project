package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.AddressesViewBean;
import com.venedental.controller.view.CitiesViewBean;
import com.venedental.controller.view.ClinicsViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.PhoneClinicViewBean;
import com.venedental.controller.view.StateViewBean;
import com.venedental.facade.ClinicsFacadeLocal;
import com.venedental.model.Addresses;
import com.venedental.model.Cities;
import com.venedental.model.Clinics;
import com.venedental.model.PhoneClinic;
import com.venedental.model.State;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

@ManagedBean(name = "clinicsRequestBean")
@RequestScoped
public class ClinicsRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(ClinicsRequestBean.class.getName());

    @EJB
    private ClinicsFacadeLocal clinicsFacadeLocal;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    @ManagedProperty(value = "#{clinicsViewBean}")
    private ClinicsViewBean clinicsViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    private String valor = "";

    public void editClinics(String dialogo) {
        if(this.getCitiesViewBean().getEditCities() !=null){
        this.getCitiesViewBean().getEditCities().setStateId(this.getStateViewBean().getEditState());
        }
        if(this.getAddressesViewBean().getEditAddresses() !=null){
        this.getAddressesViewBean().getEditAddresses().setCityId(this.getCitiesViewBean().getEditCities());
        }
        
        this.getClinicsViewBean().setTipoRifEdit(String.valueOf(this.getClinicsViewBean().getEditClinics()[2].charAt(0)));
        Clinics clinics = new Clinics(Integer.parseInt(this.getClinicsViewBean().getEditClinics()[0]));
        clinics.setId(Integer.parseInt(this.getClinicsViewBean().getEditClinics()[0]));
        clinics.setName(this.getClinicsViewBean().getEditClinics()[1]);
        clinics.setRif(this.getClinicsViewBean().getEditClinics()[2]);
        clinics.setAddressId(this.getAddressesViewBean().getEditAddresses());
        clinics.setStatus(1);
        clinics.setPhoneClinicList(this.getPhoneClinicViewBean().getEditListPhoneClinic());
        this.clinicsFacadeLocal.edit(clinics);
        
        this.getAccionesRequestBean().cerrarDialogoEdit(dialogo);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Cambios realizados satisfactoriamente"));
        context.update("form:growl");
        this.updateList();  
        this.prepareEditClinics();
    }

    public String placeholderRif(String tipoRif) {

        String placeholder = "";

        switch (tipoRif) {
            case "V":
                placeholder = " Ej: V999999999";
                break;
            case "J":
                placeholder = "Ej: J999999999";
                break;
            case "E":
                placeholder = "Ej: E999999999";
                break;
            case "G":
                placeholder = "Ej: G999999999";
                break;
        }

        return placeholder;

    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void resetClinics(String dialogo) {
        RequestContext.getCurrentInstance().reset("form:" + dialogo);
    }

    public void createClinics(String dialogo, String parentToBeUpdated) {
        RequestContext requestContext = RequestContext.getCurrentInstance();

        if (this.getClinicsViewBean().getNewClinics()[0] != null && this.getClinicsViewBean().getNewClinics()[1] != null && this.getAddressesViewBean().getNewAddresses().getAddress() != null) {

            Clinics clinic = this.clinicsFacadeLocal.findByRif(this.getClinicsViewBean().getNewClinics()[0], this.getClinicsViewBean().getNewClinics()[1], this.getAddressesViewBean().getNewAddresses().getAddress());

            if (clinic != null) {
                setValor("ERROR");
                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La clínica ya se encuentra registrada"));
            } else {

                this.getCitiesViewBean().getNewCities().setStateId(this.getStateViewBean().getNewState());
                this.getAddressesViewBean().getNewAddresses().setCityId(this.getCitiesViewBean().getNewCities());
                Clinics newClinics = new Clinics();
                newClinics.setName(this.getClinicsViewBean().getNewClinics()[0]);
                newClinics.setRif(this.getClinicsViewBean().getNewClinics()[1]);
                newClinics.setStatus(1);
                newClinics.setAddressId(this.getAddressesViewBean().getNewAddresses());
                newClinics.setPhoneClinicList(this.getPhoneClinicViewBean().getNewListPhoneClinic());
                this.clinicsFacadeLocal.create(newClinics);
                this.getClinicsViewBean().setTipoRifEdit("V");
                this.getAccionesRequestBean().cerrarDialogoCreate(dialogo);
                FacesContext contexto = FacesContext.getCurrentInstance();
                contexto.addMessage(null, new FacesMessage("Éxito", "Se ha creado la clínica Satisfactoriamente"));
                requestContext.update("form:growl");
                this.updateList();
                this.prepareNewClinics();

            }

        }

    }

    public void prepareNewClinics() {
        this.getClinicsViewBean().setNewClinics(new String[2]);
        this.getStateViewBean().setNewState(new State());
        this.getCitiesViewBean().setNewCities(new Cities());
        this.getCitiesViewBean().getListCities().clear();
        this.getAddressesViewBean().setNewAddresses(new Addresses());
        this.getClinicsViewBean().setTipoRifEdit("V");
        this.getClinicsViewBean().setMascaraRifEdit("V999999999");
        this.getPhoneClinicViewBean().getNewListPhoneClinic().clear();
        this.getPhoneClinicViewBean().getNewPhoneClinic()[0] = "";
        context.update("form:panelClinicsRegister");
    }

    public void prepareEditClinics() {
        this.getStateViewBean().setNewState(new State());
        this.getCitiesViewBean().setNewCities(new Cities());
        this.getCitiesViewBean().getListCities().clear();
        this.getAddressesViewBean().setNewAddresses(new Addresses());
        this.getPhoneClinicViewBean().getEditPhoneClinic()[0] = "";
        context.update("form:panelClinicsEdit");

    }

    public void deleteCliniscToSpecialist(int item) {
        this.getClinicsViewBean().getListaClinicsSpecialist().remove(item);
        if (this.getClinicsViewBean().getListaClinicsSpecialist().isEmpty()) {
            this.getComponenteViewBean().setUltimoPaso(false);
        }
    }

    public List<Clinics> autoCompleteClinics(String query) throws IOException {
        List<Clinics> suggestions = new ArrayList<>();

        for (Clinics p : this.getClinicsViewBean().getListaClinics()) {
            if (p.getName().toUpperCase().startsWith(query.toUpperCase())) {
                suggestions.add(p);
            }
        }
        return suggestions;
    }

    public void addPhoneClinic(String phone, boolean nuevo) {
        if (phone == null || phone.equals("")) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe ingresar un número Telefonico"));
        } else if (telefonoDuplicado(phone, nuevo)) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "El número Telefonico ya existe"));
            if (nuevo) {
                phoneClinicViewBean.setNewPhoneClinic(new String[1]);
            } else {
                phoneClinicViewBean.setEditPhoneClinic(new String[1]);
            }
        } else {
            if (nuevo) {
                this.getPhoneClinicViewBean().getNewListPhoneClinic().add(new PhoneClinic(phone));
                phoneClinicViewBean.setNewPhoneClinic(new String[1]);
            } else {
                this.getPhoneClinicViewBean().getEditListPhoneClinic().add(new PhoneClinic(phone));
                phoneClinicViewBean.setEditPhoneClinic(new String[1]);
            }
        }
    }

    public void removePhoneClinic(PhoneClinic phoneClinic, boolean nuevo) {
        if (nuevo) {
            this.getPhoneClinicViewBean().getNewListPhoneClinic().remove(phoneClinic);
        } else {
            this.getPhoneClinicViewBean().getEditListPhoneClinic().remove(phoneClinic);
        }
    }

    public void validarRif(String rif, String id) {      
        for (Clinics c : this.getClinicsViewBean().getListaClinics()) {
            if (c.getRif() != null && c.getRif().equals(rif)) {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La Clínica ya se encuentra registrada"));
                RequestContext.getCurrentInstance().update(findComponent(id).getClientId());
                break;
            }
        }
    }

    public boolean telefonoDuplicado(String telefono, boolean nuevo) {
        List<PhoneClinic> listaTelefono = null;
        if (nuevo) {
            listaTelefono = phoneClinicViewBean.getNewListPhoneClinic();
        } else {
            listaTelefono = phoneClinicViewBean.getEditListPhoneClinic();
        }

        for (PhoneClinic p : listaTelefono) {
            if (p.getPhone().equals(telefono)) {
                return true;
            }
        }
        return false;
    }

    public void mascaraRifClinic(String tipoRif) {
        this.clinicsViewBean.setMascaraRifEdit("V999999999");
        switch (tipoRif) {
            case "V":
                this.clinicsViewBean.setMascaraRifEdit("V999999999");
                break;
            case "J":
                this.clinicsViewBean.setMascaraRifEdit("J999999999");
                break;
            case "E":
                this.clinicsViewBean.setMascaraRifEdit("E999999999");
                break;
            case "G":
                this.clinicsViewBean.setMascaraRifEdit("G999999999");
                break;
        }
    }
    
    /*REVISAR METODO*/
    public void removeClinics() {
        RequestContext requestContext = RequestContext.getCurrentInstance();

        if (this.clinicsViewBean.getClinicsDelete().getMedicalOfficeList().isEmpty()) {

            for (int i = 0; i < this.clinicsViewBean.getClinicsDelete().getPhoneClinicList().size(); i++) {
                this.clinicsViewBean.getClinicsDelete().getPhoneClinicList().remove(i);
            }

            this.clinicsFacadeLocal.updateStatus(this.clinicsViewBean.getClinicsDelete().getId());
            this.clinicsViewBean.getListaClinics().get(this.clinicsViewBean.getIndiceClinics()).setStatus(0);
            this.clinicsViewBean.getListaClinics().remove(this.clinicsViewBean.getIndiceClinics());
            this.clinicsViewBean.getListaClinics().clear();
            this.clinicsViewBean.getListaClinicsFiltrada().clear();

            setValor("INFORMACIÓN");
            context.update("form:campoValor");
            FacesContext contexto = FacesContext.getCurrentInstance();
            contexto.addMessage(null, new FacesMessage("Éxito", "Se ha eliminado la clínica satisfactoriamente"));
            requestContext.update("form:growl");
            this.updateList();

        } else {

            setValor("ERROR");
            requestContext.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede eliminar la clínica seleccionada ya que posee especialistas asociados"));
            requestContext.update("form:growl");

        }

    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public ClinicsViewBean getClinicsViewBean() {
        return clinicsViewBean;
    }

    public void setClinicsViewBean(ClinicsViewBean clinicsViewBean) {
        this.clinicsViewBean = clinicsViewBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Éxito", summary);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void updateList() {
        this.getClinicsViewBean().resetDataTableClinic();
        this.getClinicsViewBean().findClinics();
    }

}
