/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.MassiveLoadingViewBean;
import com.venedental.model.Insurances;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import controller.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "insurancesMassiveConverter", forClass = Insurances.class)
public class InsurancesMassiveConverter implements Converter {

    private static final Logger log = CustomLogger.getLogger(InsurancesMassiveConverter.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        MassiveLoadingViewBean massiveLoadingViewBean = (MassiveLoadingViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "massiveLoadingViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Insurances insurances : massiveLoadingViewBean.getInsurances()) {
                    if (insurances.getId() == numero) {
                        return insurances;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Aseguradora Invalida"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Insurances) value).getId());
        }

    }

}
