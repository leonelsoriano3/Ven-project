/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.model.PlansPatients;
import com.venedental.model.PlansTreatments;
import com.venedental.model.TypeAttention;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "plansPatientsViewBean")
@ViewScoped
public class PlansPatientsViewBean {

    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;

    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;

    private List<PlansPatients> listPlansPatients;
    private List<PlansPatients> newListPlansPatients;
    private List<PlansPatients> listFilterPlansPatients;
    private List<PlansTreatments> newListPlansTreatments;
    private List<PlansPatients> editListPlansPatients;
    private List<PlansPatients> beforeListPlansPatients;
    private PlansPatients editPlansPatients;
    private PlansPatients selectedPlansPatients;
    private PlansPatients newPlansPatientsObj;
    private String[] newPlansPatients;
    private String typeAttention;
    private boolean activeTratament;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    
    @ManagedProperty(value = "#{plansViewBean}")
    private PlansViewBean plansViewBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    
    
    
        @PostConstruct
    public void init() {
        
        activeTratament = true;
    }

    public List<PlansPatients> getListFilterPlansPatients() {
        if (this.listFilterPlansPatients == null) {
            this.listFilterPlansPatients = new ArrayList<>();
        }
        return listFilterPlansPatients;
    }

    public void setListFilterPlansPatients(List<PlansPatients> listFilterPlansPatients) {
        if (this.listFilterPlansPatients == null) {
            this.listFilterPlansPatients = new ArrayList<>();
        }

        this.listFilterPlansPatients = listFilterPlansPatients;
    }

    public List<PlansTreatments> getNewListPlansTreatments() {
        return newListPlansTreatments;
    }

    public void setNewListPlansTreatments(List<PlansTreatments> newListPlansTreatments) {
        if (this.newListPlansTreatments == null) {
            this.newListPlansTreatments = new ArrayList<>();
        }
        this.newListPlansTreatments = newListPlansTreatments;
    }

    public List<PlansPatients> getBeforeListPlansPatients() {
        return this.beforeListPlansPatients;
    }

    public void setBeforeListPlansPatients(List<PlansPatients> beforeListPlansPatients) {
        if (this.beforeListPlansPatients == null) {
            this.beforeListPlansPatients = new ArrayList<>();
        }
        this.beforeListPlansPatients = beforeListPlansPatients;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public boolean isActiveTratament() {
        return activeTratament;
    }

    public void setActiveTratament(boolean activeTratament) {
        this.activeTratament = activeTratament;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }

    public List<PlansPatients> getEditListPlansPatients() {
        return editListPlansPatients;
    }

    public void setEditListPlansPatients(List<PlansPatients> editListPlansPatients) {
        if (this.editListPlansPatients == null) {
            this.editListPlansPatients = new ArrayList<>();
        }
        this.editListPlansPatients = editListPlansPatients;
    }

    public List<PlansPatients> getNewListPlansPatients() {
        if (this.newListPlansPatients == null) {
            this.newListPlansPatients = new ArrayList<>();
        }
        return newListPlansPatients;
    }

    public void setNewListPlansPatients(List<PlansPatients> newListPlansPatients) {

        this.newListPlansPatients = newListPlansPatients;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansPatientsViewBean() {
        this.editPlansPatients = new PlansPatients();
        this.selectedPlansPatients = new PlansPatients();
        this.newPlansPatients = new String[4];
        this.beforeListPlansPatients = new ArrayList<>();

    }

    public List<PlansPatients> getListPlansPatients() {
        if (this.listPlansPatients == null) {
            this.listPlansPatients = new ArrayList<>();
        }
        return listPlansPatients;
    }

    public void setListPlansPatients(List<PlansPatients> listPlansPatients) {
        this.listPlansPatients = listPlansPatients;
    }

    public PlansPatients getEditPlansPatients() {
        return editPlansPatients;
    }

    public void setEditPlansPatients(PlansPatients editPlansPatients) {
        this.editPlansPatients = editPlansPatients;
    }

    public PlansPatients getSelectedPlansPatients() {
        return selectedPlansPatients;
    }

    public void setSelectedPlansPatients(PlansPatients selectedPlansPatients) {
        this.selectedPlansPatients = selectedPlansPatients;
    }

    public String[] getNewPlansPatients() {
        return newPlansPatients;
    }

    public void setNewPlansPatients(String[] newPlansPatients) {
        this.newPlansPatients = newPlansPatients;
    }

    public PlansPatients getNewPlansPatientsObj() {
        return newPlansPatientsObj;
    }

    public void setNewPlansPatientsObj(PlansPatients newPlansPatientsObj) {
        this.newPlansPatientsObj = newPlansPatientsObj;
    }

    public PlansViewBean getPlansViewBean() {
        return plansViewBean;
    }

    public void setPlansViewBean(PlansViewBean plansViewBean) {
        this.plansViewBean = plansViewBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public String getTypeAttention() {
        return typeAttention;
    }

    public void setTypeAttention(String typeAttention) {
        this.typeAttention = typeAttention;
    }
}
