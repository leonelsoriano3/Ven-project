package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum TypeScale {

    General(1), Null(0),Especifico(2);

    Integer valor;

    private TypeScale(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
