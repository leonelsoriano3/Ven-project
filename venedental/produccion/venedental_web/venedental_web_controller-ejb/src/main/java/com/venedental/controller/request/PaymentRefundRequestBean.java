/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.PaymentRefundViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Reimbursement;
import com.venedental.model.StatusReimbursement;
import com.venedental.model.security.Users;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;

/**
 *
 * @author usuario
 */
@ManagedBean(name = "paymentRefundRequestBean")
@RequestScoped
public class PaymentRefundRequestBean extends BaseBean implements Serializable {

    @ManagedProperty(value = "#{paymentRefundViewBean}")
    private PaymentRefundViewBean paymentRefundViewBean;

    //    declaracion de los EJB
    @EJB
    private ReimbursementFacadeLocal reimbursementFacadeLocal;

    @EJB
    private StatusReimbursementFacadeLocal statusReimbursementFacadeLocal;

    @EJB
    private BinnacleReimbursementFacadeLocal binnacleLocal;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    //declaracion de los setter y los getter
    public PaymentRefundViewBean getPaymentRefundViewBean() {
        return paymentRefundViewBean;
    }

    public void setPaymentRefundViewBean(PaymentRefundViewBean paymentRefundViewBean) {
        this.paymentRefundViewBean = paymentRefundViewBean;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    //funcion que actualiza los datos del reembolsos y pasa los datos de aprobado a por abonar
    public void updateReimbursement() throws IOException, ParseException {

        FacesContext contexto = FacesContext.getCurrentInstance();

        boolean verifToPay = verifyReimbursementToPay(this.getPaymentRefundViewBean().getSelectedFinancial());

        if (this.getPaymentRefundViewBean().getSelectedFinancial().getDate_transfer() != null && this.getPaymentRefundViewBean().getSelectedFinancial().getNumber_transfer() != null) {

            if (verifToPay) {
                int valor = StatusReimbursementEnum.ABONADO.getValor();
                StatusReimbursement statusReimbursement = this.statusReimbursementFacadeLocal.findById(valor);
                this.getPaymentRefundViewBean().getSelectedFinancial().setStatusReimbursmentId(statusReimbursement);
                this.reimbursementFacadeLocal.edit(this.getPaymentRefundViewBean().getSelectedFinancial());

                //envio del correo
                this.getPaymentRefundViewBean().sendMailPatient(this.getPaymentRefundViewBean().getSelectedFinancial());

                // tranformamos la fecha
                Date fecha = new Date();
                SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                String fechaSolicitud = sdfSolicitud.format(fecha);
                Date dateBinnacle = Transformar.StringToDate(fechaSolicitud);

                // Obtenemos el usuario
                Users user = this.securityViewBean.obtainUser();

                BinnacleReimbursement binnacleReimbursement = new BinnacleReimbursement();

                StatusReimbursement statusAbonar = this.statusReimbursementFacadeLocal.find(valor);
                //setear objeto bonacle
                binnacleReimbursement.setReimbursementId(this.getPaymentRefundViewBean().getSelectedFinancial());
                binnacleReimbursement.setDate(dateBinnacle);
                binnacleReimbursement.setUserId(user.getId());
                binnacleReimbursement.setStatusReimbursementId(statusAbonar);

//                // añadimos registro a la tabla binacle
                this.binnacleLocal.create(binnacleReimbursement);
                this.getPaymentRefundViewBean().findReimbursementByStatus(StatusReimbursementEnum.ABONADO.getValor());
                this.paymentRefundViewBean.reimbursementRequestsChange();

                //actualizacion de componentes de la tabla
                contexto.addMessage(null, new FacesMessage("Éxito", "Operación realizada exitosamente"));
                RequestContext.getCurrentInstance().update("form:statusReimbursement");
                RequestContext.getCurrentInstance().execute("PF('tableRefund').filter()");
                RequestContext.getCurrentInstance().update("form:tableRefund");
                RequestContext.getCurrentInstance().update("form:tablepayment");
                RequestContext.getCurrentInstance().update("form:statusReimbursement");
                RequestContext.getCurrentInstance().execute("PF('dialogDataFinancial').hide()");
                RequestContext.getCurrentInstance().update("form:dialogDataFinancial");
                RequestContext.getCurrentInstance().update("form:messageRefund");

            } else {
                
                this.getPaymentRefundViewBean().findReimbursementByStatus(StatusReimbursementEnum.ABONADO.getValor());
                this.paymentRefundViewBean.reimbursementRequestsChange();

                //actualizacion de componentes de la tabla
                contexto.addMessage(null, new FacesMessage("Éxito", "Operación realizada exitosamente"));
                RequestContext.getCurrentInstance().update("form:statusReimbursement");
                RequestContext.getCurrentInstance().execute("PF('tableRefund').filter()");
                RequestContext.getCurrentInstance().update("form:tableRefund");
                RequestContext.getCurrentInstance().update("form:tablepayment");
                RequestContext.getCurrentInstance().update("form:statusReimbursement");
                RequestContext.getCurrentInstance().execute("PF('dialogDataFinancial').hide()");
                RequestContext.getCurrentInstance().update("form:dialogDataFinancial");
                RequestContext.getCurrentInstance().update("form:messageRefund");
                
            }

        }

    }

    //Validar Estado Abonado
    public boolean verifyReimbursementToPay(Reimbursement reimbuserment) {

        Reimbursement reimbVerify = this.reimbursementFacadeLocal.find(reimbuserment.getId());
        if (Objects.equals(reimbVerify.getStatusReimbursmentId().getId(), StatusReimbursementEnum.POR_ABONAR.getValor())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifyReimbursementApprove(Reimbursement reimbuserment) {

        Reimbursement reimbVerify = this.reimbursementFacadeLocal.find(reimbuserment.getId());

        if (Objects.equals(reimbVerify.getStatusReimbursmentId().getId(), StatusReimbursementEnum.APROBADO.getValor())) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Función que tiene como objetivo cambiar los estados aprobados por 
     * abonar 
     * @throws ParseException 
     */

    /**
     * Función que tiene como objetivo cambiar los estados aprobados por abonar
     *
     * @throws ParseException
     */
    public void paymentProcessing() throws ParseException {

        //recorremos los objetos de las listas de las filas seleccionadas
        for (Reimbursement reimbursement : this.paymentRefundViewBean.getListSelectedReimbursement()) {

            boolean verifApprove = this.verifyReimbursementApprove(reimbursement);

            //validamos si es aprobado, cambiamos de aprobado por abonar
            if (verifApprove) {
                int valor = StatusReimbursementEnum.POR_ABONAR.getValor();
                StatusReimbursement statusReimbursement = this.statusReimbursementFacadeLocal.findById(valor);
                reimbursement.setDate_transfer(null);
                reimbursement.setNumber_transfer(null);
                reimbursement.setStatusReimbursmentId(statusReimbursement);
                this.reimbursementFacadeLocal.edit(reimbursement);

                // tranformamos la fecha
                Date fecha = new Date();
                SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                String fechaSolicitud = sdfSolicitud.format(fecha);
                Date dateBinnacle = Transformar.StringToDate(fechaSolicitud);

                // Obtenemos el usuario
                Users user = this.securityViewBean.obtainUser();

                BinnacleReimbursement binnacleReimbursement = new BinnacleReimbursement();

                StatusReimbursement statusAbonar = this.statusReimbursementFacadeLocal.find(valor);
                //setear objeto bonacle
                binnacleReimbursement.setReimbursementId(reimbursement);
                binnacleReimbursement.setDate(dateBinnacle);
                binnacleReimbursement.setUserId(user.getId());
                binnacleReimbursement.setStatusReimbursementId(statusAbonar);

                // añadimos registro a la tabla binacle
                this.binnacleLocal.create(binnacleReimbursement);
                this.getPaymentRefundViewBean().findReimbursementByStatus(StatusReimbursementEnum.POR_ABONAR.getValor());
                this.paymentRefundViewBean.reimbursementRequestsChange();

                this.getPaymentRefundViewBean().findReimbursementByStatus(StatusReimbursementEnum.APROBADO.getValor());

                this.getPaymentRefundViewBean().setTotalPrice("0,00");

            }
            this.getPaymentRefundViewBean().setListSelectedReimbursement(new ArrayList<Reimbursement>());
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ÉXITO", "OPERACIÓN REALIZADA CON ÉXITO"));
            //actualizacion de las vistas y seteo de los check seleccionados
            RequestContext.getCurrentInstance().update("form:statusReimbursement");
            RequestContext.getCurrentInstance().execute("PF('tableRefund').filter()");
            RequestContext.getCurrentInstance().update("form:tableRefund");
            RequestContext.getCurrentInstance().update("form:btnApprove");
            RequestContext.getCurrentInstance().update("form:dialogDataFinancial");
            RequestContext.getCurrentInstance().update("form:messageRefund");
        RequestContext.getCurrentInstance().update("form:paymentTotal");
        RequestContext.getCurrentInstance().update("form:amountTotal");

        }

    }
