/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.KeysPatientsRegisterViewBean;
import com.venedental.model.MedicalOffice;
import com.venedental.model.Plans;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "medicalOfficeConverter", forClass = Plans.class)
public class MedicalOfficeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        KeysPatientsRegisterViewBean keysPatientsRegisterViewBean = (KeysPatientsRegisterViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "keysPatientsRegisterViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                
                for (MedicalOffice medicalOffice : keysPatientsRegisterViewBean.getMedicalOfficeList()) {
                    if (medicalOffice.getId() == numero) {
                        return medicalOffice;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Medical Office Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((MedicalOffice) value).getId());
        }

    }

}