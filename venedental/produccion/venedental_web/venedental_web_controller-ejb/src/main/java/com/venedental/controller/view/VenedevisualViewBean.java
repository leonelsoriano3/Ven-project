/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.MessageSessionBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.ListaLinks;
import org.primefaces.context.RequestContext;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "venedevisualViewBean")
@ViewScoped
public class VenedevisualViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    private List<ListaLinks> listaLinks;
    private List<ListaLinks> listaConsulta;
    private List<ListaLinks> listaOptica;
    private List<ListaLinks> listaRequisitos;

    @ManagedProperty(value = "#{messageSessionBean}")
    private MessageSessionBean messageSessionBean;

    public VenedevisualViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaLinks(new ArrayList<ListaLinks>());
        this.listaLinks.add(new ListaLinks("Servicios", "venedevisual.xhtml", "/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Plan Venedevisual", "planVenedevisual.xhtml", "/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Reembolso", "reembolso.xhtml", "/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Centro de Atención", "directorio.xhtml", "/img/list-bullets2.png"));

        this.setListaConsulta(new ArrayList<ListaLinks>());
        this.listaConsulta.add(new ListaLinks("Estudios diagnósticos", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Agudeza Visual con y sin corrección", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Refracción pre y post Cicloplejía", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Balance muscular", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Visión de colores (cartillas de Ishihara)", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Amsler. Evaluación macular", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Biomicroscopía: Con Lámpara de Hendidura", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Diagnóstico e indicaciones médicas Óptica", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Oftalmoscopia indirecta para evaluación del fondo de ojo", "", "/img/category_icon.png"));
        this.listaConsulta.add(new ListaLinks("Gonioscopia", "", "/img/category_icon.png"));

        this.setListaOptica(new ArrayList<ListaLinks>());
        this.listaOptica.add(new ListaLinks("Monturas y Cristales (por reembolso de acuerdo al plan de cobertura)", "", "/img/category_icon.png"));

        this.setListaRequisitos(new ArrayList<ListaLinks>());
        this.listaRequisitos.add(new ListaLinks("Copia de la Cédula de Identidad del Titular y del Beneficiario (incluidos Menores de Edad que tengan cédula)", "", "/img/category_icon.png"));
        this.listaRequisitos.add(new ListaLinks("Informe Médico emitido por el Oftalmólogo, sellado y firmado por el mismo", "", "/img/category_icon.png"));
        this.listaRequisitos.add(new ListaLinks("Indicación médica, sellada y firmada por el Oftalmólogo", "", "/img/category_icon.png"));
        this.listaRequisitos.add(new ListaLinks("Facturas contables originales, de acuerdo a las estipulaciones del SENIAT,  las cuales deben ser emitidas a nombre del titular y/o beneficiario según sea el caso", "", "/img/category_icon.png"));
        this.listaRequisitos.add(new ListaLinks("Cuando se trate de menores de edad, dicha factura debe ser emitida a nombre del Titular", "", "/img/category_icon.png"));
        this.listaRequisitos.add(new ListaLinks("Número de Cuenta y Banco del Titular (no del Beneficiario), con soporte de Libreta Bancaria y/o Cheque Anulado para realizar la transferencia", "", "/img/category_icon.png"));
    }

    public void mostrarMensaje() {
        if (getMessageSessionBean().getMensaje() != null) {
            FacesContext.getCurrentInstance().addMessage("form:messages1", new FacesMessage("Éxito", new String(getMessageSessionBean().getMensaje())));
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("form:messages1");
            getMessageSessionBean().setMensaje(null);
        }
    }

    public List<ListaLinks> getListaLinks() {
        return listaLinks;
    }

    public void setListaLinks(List<ListaLinks> listaLinks) {
        this.listaLinks = listaLinks;
    }

    public List<ListaLinks> getListaConsulta() {
        return listaConsulta;
    }

    public void setListaConsulta(List<ListaLinks> listaConsulta) {
        this.listaConsulta = listaConsulta;
    }

    public List<ListaLinks> getListaOptica() {
        return listaOptica;
    }

    public void setListaOptica(List<ListaLinks> listaOptica) {
        this.listaOptica = listaOptica;
    }

    public List<ListaLinks> getListaRequisitos() {
        return listaRequisitos;
    }

    public void setListaRequisitos(List<ListaLinks> listaRequisitos) {
        this.listaRequisitos = listaRequisitos;
    }

    public MessageSessionBean getMessageSessionBean() {
        return messageSessionBean;
    }

    public void setMessageSessionBean(MessageSessionBean messageSessionBean) {
        this.messageSessionBean = messageSessionBean;
    }

}
