/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.dto;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.model.Insurances;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import com.venedental.model.PlansPatients;
import com.venedental.model.Relationship;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author usuario
 */
public class RowDTO implements Comparable<RowDTO> {

    private Patients patient;
    private List<Plans> plans;
    private Insurances insurance;
    private Relationship relationship;
    private String reference;
    private final String fileSheet;
    private final String location;
    private final String resume;
    private MessageException exception;
    private boolean registeredPatient;

    public RowDTO(Insurances insurance, Patients patient, Relationship relationship, List<Plans> plans, String fileSheet, String location, String resume) {
        this.insurance = insurance;
        this.patient = patient;
        this.relationship = relationship;
        this.plans = plans;
        this.fileSheet = fileSheet;
        this.location = location;
        this.resume = resume;
        this.registeredPatient = false;
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_4);
        reference = String.format("%d_%d_%s_%s", patient.getIdentityNumber(), patient.getIdentityNumberHolder(), patient.getCompleteName().toUpperCase().replace(" ", "-"), format.format(patient.getDateOfBirth()));
    }

    public RowDTO(MessageException exception, String fileSheet, String location, String resume) {
        this.exception = exception;
        this.fileSheet = fileSheet;
        this.location = location;
        this.resume = resume;
    }

    public Patients getPatient() {
        return patient;
    }

    public List<Plans> getPlans() {
        return plans;
    }

    public Insurances getInsurance() {
        return insurance;
    }

    public String getResume() {
        return resume;
    }

    public MessageException getException() {
        return exception;
    }

    public boolean hasError() {
        return exception != null;
    }

    public void setPatient(Patients patient) {
        this.patient = patient;
    }

    public void setPlans(List<Plans> plans) {
        this.plans = plans;
    }

    public void setException(MessageException exception) {
        this.exception = exception;
    }

    public String getLocation() {
        return location;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }

    public String getFileSheet() {
        return fileSheet;
    }

    public boolean isRegisteredPatient() {
        return registeredPatient;
    }

    public void setRegisteredPatient(boolean registeredPatient) {
        this.registeredPatient = registeredPatient;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        RowDTO rowDto = (RowDTO) obj;
        return rowDto.reference.equals(reference);
    }

    @Override
    public String toString() {
        if (exception != null) {
            return rowInvalid();
        } else {
            return rowValid();
        }
    }

    private String rowValid() {
        StringBuilder sb = new StringBuilder();
        sb.append(resume);
        return sb.toString();
    }

    private String rowInvalid() {
        StringBuilder sb = new StringBuilder();
        if (resume != null) {
            sb.append(resume);
        }
        sb.append("\t").append(getException().getError());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.patient.getIdentityNumberHolder());
        return hash;
    }

    @Override
    public int compareTo(RowDTO o) {
        return reference.compareTo(o.reference);
    }

}
