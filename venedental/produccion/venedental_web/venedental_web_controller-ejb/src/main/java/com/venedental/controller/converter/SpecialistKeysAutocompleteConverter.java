package com.venedental.controller.converter;

import com.venedental.controller.view.KeysPatientsRegisterViewBean;
import com.venedental.model.Specialists;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("specialistKeysAutocompleteConverter")
public class SpecialistKeysAutocompleteConverter implements Converter {

    @ManagedProperty(value = "#{keysPatientsRegisterViewBean}")
    private KeysPatientsRegisterViewBean keysPatientsRegisterViewBean;

    public KeysPatientsRegisterViewBean getKeysPatientstRegisterViewBean() {
        return keysPatientsRegisterViewBean;
    }

    public void setKeysPatientstRegisterViewBean(KeysPatientsRegisterViewBean keysPatientsRegisterViewBean) {
        this.keysPatientsRegisterViewBean = keysPatientsRegisterViewBean;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
       
        KeysPatientsRegisterViewBean keysPatientsRegisterViewBean = (KeysPatientsRegisterViewBean) context.getApplication().getELResolver().getValue(context.getELContext(), null, "keysPatientsRegisterViewBean");

        if (value != null && value.trim().length() > 0) {
            try {
                int numero = Integer.parseInt(value);

                for (Specialists s : keysPatientsRegisterViewBean.getListSpecialists()) {
                    if (s.getId() == numero) {
                        return s;
                    }
                }

                return null;
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid object"));
            }
        } else {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Specialists) {
            Specialists specialist = (Specialists) value;
            return specialist.getId().toString();
        }
        return "";
    }

}
