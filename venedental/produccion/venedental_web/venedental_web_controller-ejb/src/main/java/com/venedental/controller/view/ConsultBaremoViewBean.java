/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dto.baremo.ConsultBaremoGeneralOutput;
import com.venedental.enums.TypeScale;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.facade.TypeSpecialistFacadeLocal;
import com.venedental.model.Insurances;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.TypeSpecialist;
import com.venedental.services.ServiceBaremos;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "consultBaremoViewBean")
@ViewScoped
public class ConsultBaremoViewBean extends BaseBean {

    //    @EJB
    private InsurancesFacadeLocal insurancesFacadeLocal;

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;

    @EJB
    private TypeSpecialistFacadeLocal typeSpecialistFacadeLocal;

    /* ManagedBeans Property */
    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    private final RequestContext context = RequestContext.getCurrentInstance();

    private List<ScaleTreatments> scaleTreatmentsList;

    private ScaleTreatments scaleSelected;

    private List<ScaleTreatments> scaleTreatmentsListFiltered;

    private List<TypeSpecialist> typeSpecialityList;

    private TypeSpecialist selectedTypeSpeciality;

    private List<ConsultBaremoGeneralOutput> specialityDermatologiaAuxList = new ArrayList<>();
    private List<ConsultBaremoGeneralOutput> specialityOdontologiaAuxList = new ArrayList<>();
    private List<ConsultBaremoGeneralOutput> specialityPsicologiaAuxList = new ArrayList<>();
    private List<ConsultBaremoGeneralOutput> specialityOftalmologiaAuxList = new ArrayList<>();
    private List<ConsultBaremoGeneralOutput> specialityFisioterapiaAuxList = new ArrayList<>();

    /* Attributes */
    // --> Jaspert Report
    private Map<String, Object> params = new HashMap<String, Object>();
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private JasperPrint jasperPrint;
    private String reportPath;
    private String logoPath;
    private HttpServletResponse httpServletResponse;
    private ServletOutputStream servletOutputStream;
    private JRXlsxExporter docxExporter;

    private String nameSpeciality, minDate, minDateHasta, nameDermatologia, nameFisioterapia,
            nameOdontologia, nameOftalmologia, namePsicologia,minDateFilter,maxDateString;
    private Integer insurancesSelected;
    private String[] filtersData;
    private Date startDate, maxDate, vigencia, endDate, fecha, dateSqlFilter;
    private boolean btnActive, imputBaremo, end, btnRest, btnBuscar, btnSave, columDateBaremo,
            columNameTratment, columBaremo, columnSpeciality, btnExportar, dateTable;

    /* Services */
    @EJB
    private ServiceBaremos serviceBaremos;

    /* Attributes */
    private List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutputList;

    @PostConstruct
    public void init() {
//        this.getTypeSpecialityList();
//        this.setScaleTreatmentsList(new ArrayList<ScaleTreatments>());
        this.setImputDateEnd(true);
        this.setBtnBuscar(true);
        this.setBtnRest(true);
        this.setBtnExportar(true);
        searchDataByCriterial();
        this.setBtnRest(true);
//        this.setScaleTreatmentsList(scaleTreatmentsFacadeLocal.findByTypeSpeciality(TypeScale.General.getValor(), 0));

        this.setColumnSpeciality(true);
        dateLimit();

    }

    public List<ConsultBaremoGeneralOutput> getConsultBaremoGeneralOutputList() {
        return consultBaremoGeneralOutputList;
    }

    public void setConsultBaremoGeneralOutputList(List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutput) {
        this.consultBaremoGeneralOutputList = consultBaremoGeneralOutput;
    }

    /*----------------------------------------------------------------------------------------*
     Comienzo de Getter y Setter
     *----------------------------------------------------------------------------------------*
     */
    public Boolean getColumnSpeciality() {
        return columnSpeciality;
    }

    public void setColumnSpeciality(Boolean columnSpeciality) {
        this.columnSpeciality = columnSpeciality;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<ScaleTreatments> getScaleTreatmentsListFiltered() {
        return scaleTreatmentsListFiltered;
    }

    public void setScaleTreatmentsListFiltered(List<ScaleTreatments> scaleTreatmentsListFiltered) {
        this.scaleTreatmentsListFiltered = scaleTreatmentsListFiltered;
    }

    public ScaleTreatmentsFacadeLocal getScaleTreatmentsFacadeLocal() {
        return scaleTreatmentsFacadeLocal;
    }

    public List<ScaleTreatments> getScaleTreatmentsList() {
        return scaleTreatmentsList;
    }

    public void setScaleTreatmentsList(List<ScaleTreatments> scaleTreatmentsList) {
        this.scaleTreatmentsList = scaleTreatmentsList;
    }

    public void setTypeSpecialityList(List<TypeSpecialist> typeSpecialityList) {
        this.typeSpecialityList = typeSpecialityList;
    }

    public TypeSpecialistFacadeLocal getTypeSpecialistFacadeLocal() {
        return typeSpecialistFacadeLocal;
    }

    public void setTypeSpecialistFacadeLocal(TypeSpecialistFacadeLocal typeSpecialistFacadeLocal) {
        this.typeSpecialistFacadeLocal = typeSpecialistFacadeLocal;
    }

    public TypeSpecialist getSelectedTypeSpeciality() {
        return selectedTypeSpeciality;
    }

    public void setSelectedTypeSpeciality(TypeSpecialist selectedTypeSpeciality) {
        this.selectedTypeSpeciality = selectedTypeSpeciality;
    }

    public List<TypeSpecialist> getTypeSpecialityList() {
        if (this.typeSpecialityList == null) {
            this.typeSpecialityList = this.typeSpecialistFacadeLocal.findAll();
        }

        Collections.sort(typeSpecialityList, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                TypeSpecialist tp1 = (TypeSpecialist) o1;
                TypeSpecialist tp2 = (TypeSpecialist) o2;
                return tp1.getName().compareTo(tp2.getName());

            }
        });
        return typeSpecialityList;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public ScaleTreatments getScaleSelected() {
        return scaleSelected;
    }

    public void setScaleSelected(ScaleTreatments scaleSelected) {
        this.scaleSelected = scaleSelected;
    }

    public String[] getFiltersData() {
        return filtersData;
    }

    /* Getter and Setter */
    public void setFiltersData(String[] filtersData) {
        this.filtersData = filtersData;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isImputDateEnd() {
        return end;
    }

    public void setImputDateEnd(boolean end) {
        this.end = end;
    }

    public boolean isVisibilitycolumDateBaremo() {
        return columDateBaremo;
    }

    public void setVisibilitycolumDateBaremo(boolean columDateBaremo) {
        this.columDateBaremo = columDateBaremo;
    }

    public boolean isVisibilitycolumNameTratment() {
        return columNameTratment;
    }

    public void setVisibilitycolumNameTratment(boolean columNameTratment) {
        this.columNameTratment = columNameTratment;
    }

    public boolean isVisibilitycolumBaremo() {
        return columBaremo;
    }

    public void setVisibilitycolumBaremo(boolean columBaremo) {
        this.columBaremo = columBaremo;
    }

    public boolean isBtnExportar() {
        return btnExportar;
    }

    public void setBtnExportar(boolean btnExportar) {
        this.btnExportar = btnExportar;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMinDateHasta() {
        return minDateHasta;
    }

    public void setMinDateHasta(String minDateHasta) {
        this.minDateHasta = minDateHasta;
    }

    public boolean isBtnRest() {
        return btnRest;
    }

    public void setBtnRest(boolean btnRest) {
        this.btnRest = btnRest;
    }

    public boolean isBtnBuscar() {
        return btnBuscar;
    }

    public void setBtnBuscar(boolean btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public Integer getInsurancesSelected() {
        return insurancesSelected;
    }

    public void setInsurancesSelected(Integer insurancesSelected) {
        this.insurancesSelected = insurancesSelected;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    public boolean isDateTable() {
        return dateTable;
    }

    public void setDateTable(boolean dateTable) {
        this.dateTable = dateTable;
    }

    public void selectDateConsultBaremo() {
        this.setImputDateEnd(false);
        this.setBtnRest(true);
        this.setBtnBuscar(true);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:configBaremo2");

    }

    public String getNameSpeciality() {
        return nameSpeciality;
    }

    public void setNameSpeciality(String nameSpeciality) {
        this.nameSpeciality = nameSpeciality;
    }

    public List<ConsultBaremoGeneralOutput> getSpecialityDermatologiaAuxList() {
        return specialityDermatologiaAuxList;
    }

    public void setSpecialityDermatologiaAuxList(List<ConsultBaremoGeneralOutput> specialityDermatologiaAuxList) {
        this.specialityDermatologiaAuxList = specialityDermatologiaAuxList;
    }

    public List<ConsultBaremoGeneralOutput> getSpecialityOdontologiaAuxList() {
        return specialityOdontologiaAuxList;
    }

    public void setSpecialityOdontologiaAuxList(List<ConsultBaremoGeneralOutput> specialityOdontologiaAuxList) {
        this.specialityOdontologiaAuxList = specialityOdontologiaAuxList;
    }

    public List<ConsultBaremoGeneralOutput> getSpecialityPsicologiaAuxList() {
        return specialityPsicologiaAuxList;
    }

    public void setSpecialityPsicologiaAuxList(List<ConsultBaremoGeneralOutput> specialityPsicologiaAuxList) {
        this.specialityPsicologiaAuxList = specialityPsicologiaAuxList;
    }

    public List<ConsultBaremoGeneralOutput> getSpecialityOftalmologiaAuxList() {
        return specialityOftalmologiaAuxList;
    }

    public void setSpecialityOftalmologiaAuxList(List<ConsultBaremoGeneralOutput> specialityOftalmologiaAuxList) {
        this.specialityOftalmologiaAuxList = specialityOftalmologiaAuxList;
    }

    public List<ConsultBaremoGeneralOutput> getSpecialityFisioterapiaAuxList() {
        return specialityFisioterapiaAuxList;
    }

    public void setSpecialityFisioterapiaAuxList(List<ConsultBaremoGeneralOutput> specialityFisioterapiaAuxList) {
        this.specialityFisioterapiaAuxList = specialityFisioterapiaAuxList;
    }

    public String getNameDermatologia() {
        return nameDermatologia;
    }

    public void setNameDermatologia(String nameDermatologia) {
        this.nameDermatologia = nameDermatologia;
    }

    public String getNameFisioterapia() {
        return nameFisioterapia;
    }

    public void setNameFisioterapia(String nameFisioterapia) {
        this.nameFisioterapia = nameFisioterapia;
    }

    public String getNameOdontologia() {
        return nameOdontologia;
    }

    public void setNameOdontologia(String nameOdontologia) {
        this.nameOdontologia = nameOdontologia;
    }

    public String getNameOftalmologia() {
        return nameOftalmologia;
    }

    public void setNameOftalmologia(String nameOftalmologia) {
        this.nameOftalmologia = nameOftalmologia;
    }

    public String getNamePsicologia() {
        return namePsicologia;
    }

    public void setNamePsicologia(String namePsicologia) {
        this.namePsicologia = namePsicologia;
    }

    public Date getDateSqlFilter() {
        return dateSqlFilter;
    }

    public void setDateSqlFilter(Date dateSqlFilter) {
        this.dateSqlFilter = dateSqlFilter;
    }

    public String getMinDateFilter() {
        return minDateFilter;
    }

    public void setMinDateFilter(String minDateFilter) {
        this.minDateFilter = minDateFilter;
    }

    public String getMaxDateString() {
        return maxDateString;
    }

    public void setMaxDateString(String maxDateString) {
        this.maxDateString = maxDateString;
    }
    
    
    
    

    /**
     * metodo para verificar si la tabla contenida para el reporte esta
     * totalmente vacia.
     *
     * @return
     */
    public boolean verifyDataTable() {

        if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {
            return true;
        } else {

            return false;
        }

    }

    public void selectDateEndConsultBaremo() {
        this.setBtnRest(false);
        this.setBtnBuscar(false);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:configBaremo2");

    }

    public void selectDateStartConsultBaremo() {
        this.setBtnRest(false);
        this.setBtnBuscar(false);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");

    }

    public void activecmbInsurances() {

        if (this.getStartDate() != null && this.getEndDate() != null) {
            this.setBtnRest(false);

        } else {
            this.setBtnRest(true);

        }

        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");

    }

    //      * Method at converter dateUtil and dateSql
//     *
//     * @param date
//     * @return
//     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    /**
     * metodo para listar los datos segun el criterio de busqueda.
     *
     *
     */
    public void searchDataByCriterial() {
        java.sql.Date dateStartSql = null;
        java.sql.Date dateEndSql = null;
        RequestContext.getCurrentInstance().update("form:configBaremo2");
        this.getSelectedTypeSpeciality();
        RequestContext.getCurrentInstance().update("form:dateBaremo");
        RequestContext.getCurrentInstance().update("form:nameTratment");
        RequestContext.getCurrentInstance().update("form:baremo");
        this.getFiltersData();

        try {

            this.getSelectedTypeSpeciality();
            RequestContext.getCurrentInstance().update("form:configBaremo2");
            RequestContext.getCurrentInstance().update("form:dateBaremo");
            RequestContext.getCurrentInstance().update("form:nameTratment");
            RequestContext.getCurrentInstance().update("form:baremo");
            if (this.getSelectedTypeSpeciality() == null) {
                if (this.getStartDate() == null && this.getEndDate() == null) {
                    dateStartSql = null;
                    dateEndSql = null;

                } else {
                    dateStartSql = this.convertJavaDateToSqlDate(
                            this.getStartDate());
                    dateEndSql = this.convertJavaDateToSqlDate(
                            this.getEndDate());
                }

                this.setConsultBaremoGeneralOutputList(
                        this.serviceBaremos.findConsultBaremoGeneralOutput(
                                dateStartSql,
                                dateEndSql, 0));

                // Visibility
                this.getConsultBaremoGeneralOutputList();

                if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {
                    this.setBtnRest(false);
                    this.setBtnExportar(false);
                    this.setVisibilitycolumDateBaremo(false);
                    this.setVisibilitycolumNameTratment(false);
                    this.setVisibilitycolumBaremo(true);
                    RequestContext.getCurrentInstance().update("form:btnResetKey");
                    RequestContext.getCurrentInstance().update("form:configBaremo2");
                    RequestContext.getCurrentInstance().update("form:dateBaremo");
                    RequestContext.getCurrentInstance().update("form:nameTratment");
                    RequestContext.getCurrentInstance().update("form:baremo");
                    RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
                } else {
                    this.setVisibilitycolumDateBaremo(true);
                    this.setVisibilitycolumNameTratment(true);
                    this.setVisibilitycolumBaremo(true);
                    this.setBtnExportar(true);
                    RequestContext.getCurrentInstance().update("form:configBaremo2");
                    RequestContext.getCurrentInstance().update("form:dateBaremo");
                    RequestContext.getCurrentInstance().update("form:nameTratment");
                    RequestContext.getCurrentInstance().update("form:baremo");
                    RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
                }

            } else if (this.getSelectedTypeSpeciality() != null) {

                this.getSelectedTypeSpeciality();
                RequestContext.getCurrentInstance().update("form:configBaremo2");
                RequestContext.getCurrentInstance().update("form:dateBaremo");
                RequestContext.getCurrentInstance().update("form:nameTratment");
                RequestContext.getCurrentInstance().update("form:baremo");
                if (this.getStartDate() == null && this.getEndDate() == null) {
                    dateStartSql = null;
                    dateEndSql = null;

                } else {
                    dateStartSql = this.convertJavaDateToSqlDate(
                            this.getStartDate());
                    dateEndSql = this.convertJavaDateToSqlDate(
                            this.getEndDate());
                }

                this.setConsultBaremoGeneralOutputList(
                        this.serviceBaremos.findConsultBaremoGeneralOutput(
                                dateStartSql,
                                dateEndSql,
                                this.getSelectedTypeSpeciality().getId()
                        ));

                // Visibility
                this.getConsultBaremoGeneralOutputList();

                if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {
                    this.setBtnExportar(false);
                    this.setBtnRest(false);
                    this.setVisibilitycolumDateBaremo(false);
                    this.setVisibilitycolumNameTratment(false);
                    this.setVisibilitycolumBaremo(false);
                    RequestContext.getCurrentInstance().update("form:btnResetKey");
                    RequestContext.getCurrentInstance().update("form:configBaremo2");
                    RequestContext.getCurrentInstance().update("form:dateBaremo");
                    RequestContext.getCurrentInstance().update("form:nameTratment");
                    RequestContext.getCurrentInstance().update("form:baremo");
                    RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
                } else {
                    this.setVisibilitycolumDateBaremo(true);
                    this.setVisibilitycolumNameTratment(true);
                    this.setVisibilitycolumBaremo(true);
                    this.setBtnExportar(true);
                    RequestContext.getCurrentInstance().update("form:configBaremo2");
                    RequestContext.getCurrentInstance().update("form:dateBaremo");
                    RequestContext.getCurrentInstance().update("form:nameTratment");
                    RequestContext.getCurrentInstance().update("form:baremo");
                    RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
                }
//

                RequestContext.getCurrentInstance().update("form:dateBaremo");
                RequestContext.getCurrentInstance().update("form:nameTratment");
                RequestContext.getCurrentInstance().update("form:baremo");
                RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
            }

        } catch (Exception e) {
            System.out.println("com.venedental.controller.view.ConsultBaremoViewBean.searchDataByCriterial()" + e.getMessage());
//

            RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");

        }

    }

    public void specialtyVisiblecolumn() {
        this.setBtnBuscar(false);
        RequestContext.getCurrentInstance().update("form:configBaremo2");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        if (this.getSelectedTypeSpeciality() == null) {
            this.setColumnSpeciality(true);

            RequestContext.getCurrentInstance().update("form:nameEspeciality");
        } else if (this.getSelectedTypeSpeciality().getId() == 1
                || this.getSelectedTypeSpeciality().getId() == 2
                || this.getSelectedTypeSpeciality().getId() == 3
                || this.getSelectedTypeSpeciality().getId() == 4
                || this.getSelectedTypeSpeciality().getId() == 5) {
            this.setColumnSpeciality(false);

            RequestContext.getCurrentInstance().update("form:nameEspeciality");
        }

        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:configBaremo2");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:configBaremo2");
//

        }

    }

    public void cleanAndPaginatior() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("growReportReimbursement:configBaremo2");
        dataTable.reset();
    }

    public void cleanAndDate() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:configBaremo2");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:configBaremo2");
//

        }
        this.setConsultBaremoGeneralOutputList(null);
        this.setVisibilitycolumDateBaremo(false);
        this.setVisibilitycolumNameTratment(false);
        this.setVisibilitycolumBaremo(false);
        this.setBtnExportar(false);
        RequestContext.getCurrentInstance().update("form:configBaremo2");
        RequestContext.getCurrentInstance().update("form:dateBaremo");
        RequestContext.getCurrentInstance().update("form:nameTratment");
        RequestContext.getCurrentInstance().update("form:baremo");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        this.setStartDate(null);
        this.setEndDate(null);
        this.setMaxDate(null);
        this.setSelectedTypeSpeciality(null);

        // Buttons
//        this.setBtnBuscar(true);
        this.setBtnExportar(true);
        this.setBtnRest(true);

//
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:startDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:typeSpecialistCreate");

    }

    public void selectBtnRestablecer() {
//        this.setBtnBuscar(false);
//        RequestContext.getCurrentInstance().update("form:btnFindKey");
    }
//
    // --> Send Report

    public void sendReportXLSSubscriber(ActionEvent actionEvent) throws JRException, IOException, Exception {

        try {
            if (this.getSelectedTypeSpeciality() == null) {
                if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    String auxStarDate = sdf.format(new Date());
                    String replaceDateReport = auxStarDate.replaceAll("/", "");
                    String replaceDate = auxStarDate.replaceAll("/", "-");
//
                    String name = "ReporteTodasEspecialidades";
                    loadReportsGeneral();
                    this.export.xlsxBaremo(jasperPrint, name, replaceDateReport);
                } else {

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Problemas al generar el Reporte!"));
                }
            } else if (this.getSelectedTypeSpeciality() != null) {
                String nameForReport = "ReportePorEspecialidad";
                if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {
                    SimpleDateFormat sdfa = new SimpleDateFormat("yyyy/MM/dd");
                    String reportStarDate = sdfa.format(new Date());
                    String replaceDateReport = reportStarDate.replaceAll("/", "");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String auxStarDate = sdf.format(new Date());
                    String replaceDate = auxStarDate.replaceAll("/", "-");
                    loadReports();

                    this.export.xlsxBaremo(jasperPrint, nameForReport, replaceDateReport);
                } else {

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Problemas al generar el Reporte!"));
                }
            }

//
        } catch (Exception e) {
            System.out.println(e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Error!", e.getMessage()));
//
        }

    }

    public void loadReportsGeneral() throws IOException, JRException {
        groupList();
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("name", this.getNameDermatologia());
        this.params.put("name_1", this.getNameFisioterapia());
        this.params.put("name_2", this.getNameOdontologia());
        this.params.put("name_3", this.getNameOftalmologia());
        this.params.put("name_4", this.getNamePsicologia());
        this.params.put("consultBaremoGeneralOutputList", new JRBeanCollectionDataSource(this.getConsultBaremoGeneralOutputList()));
        this.params.put("specialityDermatologiaAuxList", new JRBeanCollectionDataSource(this.getSpecialityDermatologiaAuxList()));
        this.params.put("specialityFisioterapiaAuxList", new JRBeanCollectionDataSource(this.getSpecialityFisioterapiaAuxList()));
        this.params.put("specialityOdontologiaAuxList", new JRBeanCollectionDataSource(this.getSpecialityOdontologiaAuxList()));
        this.params.put("specialityOftalmologiaAuxList", new JRBeanCollectionDataSource(this.getSpecialityOftalmologiaAuxList()));
        this.params.put("specialityPsicologiaAuxList", new JRBeanCollectionDataSource(this.getSpecialityPsicologiaAuxList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("startDate", this.getStartDate());
        this.params.put("endDate", this.getEndDate());
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getConsultBaremoGeneralOutputList());
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/baremo/reportGeneral.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    public void loadReports() throws IOException, JRException {

        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("consultBaremoGeneralOutputList", new JRBeanCollectionDataSource(this.getConsultBaremoGeneralOutputList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("startDate", this.getStartDate());
        this.params.put("endDate", this.getEndDate());
        this.params.put("especialidad", this.getSelectedTypeSpeciality().getName());
        this.params.put("heal_logo2", logoPath);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getConsultBaremoGeneralOutputList());
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/baremo/reportBaremo.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    public void groupList() {

        for (ConsultBaremoGeneralOutput consultBaremoGeneralOutput : this.consultBaremoGeneralOutputList) {

            Integer idSpeciality = consultBaremoGeneralOutput.getIdEspecialidad();
            switch (idSpeciality) {

                case 1:
                    specialityOdontologiaAuxList.add(consultBaremoGeneralOutput);

                    break;
                case 2:
                    specialityOftalmologiaAuxList.add(consultBaremoGeneralOutput);
                    break;
                case 3:
                    specialityDermatologiaAuxList.add(consultBaremoGeneralOutput);
                    break;
                case 4:
                    specialityPsicologiaAuxList.add(consultBaremoGeneralOutput);
                    break;
                case 5:
                    specialityFisioterapiaAuxList.add(consultBaremoGeneralOutput);
                    break;

            }
            this.setSpecialityDermatologiaAuxList(specialityDermatologiaAuxList);
            this.setSpecialityOdontologiaAuxList(specialityOdontologiaAuxList);
            this.setSpecialityFisioterapiaAuxList(specialityFisioterapiaAuxList);
            this.setSpecialityOftalmologiaAuxList(specialityOftalmologiaAuxList);
            this.setSpecialityPsicologiaAuxList(specialityPsicologiaAuxList);

//
        }
        if (this.getSpecialityDermatologiaAuxList().isEmpty()) {
            this.setNameDermatologia(null);
        } else {
            this.setNameDermatologia("DERMATOLOGÍA");

        }
        if (this.getSpecialityFisioterapiaAuxList().isEmpty()) {
            this.setNameFisioterapia(null);
        } else {
            this.setNameFisioterapia("FISIOTERAPIA");

        }
        if (this.getSpecialityOdontologiaAuxList().isEmpty()) {
            this.setNameOdontologia(null);
        } else {
            this.setNameOdontologia("ODONTOLOGÍA");

        }
        if (this.getSpecialityOftalmologiaAuxList().isEmpty()) {
            this.setNameOftalmologia(null);
        } else {
            this.setNameOftalmologia("OFTALMOLOGÍA");

        }
        if (this.getSpecialityPsicologiaAuxList().isEmpty()) {
            this.setNamePsicologia(null);
        } else {
            this.setNamePsicologia("PSICOLOGÍA");

        }
    }

    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int limYear = year + 1;
        int minYear = year - 1;
        int hasta = c.get(Calendar.DATE) + 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setMaxDateString("31/12/" + String.valueOf(limYear));
//        this.setMinDateHasta(String.valueOf(hasta)+"/"+String.valueOf(month)+"/"+String.valueOf(year));

        this.dateSqlFilter = this.serviceBaremos.findDateMinFilter(0);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.minDateFilter = sdf.format(this.getDateSqlFilter());

    }

    public void searchPrueba() {
        java.sql.Date dateStartSql = null;
        java.sql.Date dateEndSql = null;
        if (this.getSelectedTypeSpeciality() == null) {
            if (this.getStartDate() == null && this.getEndDate() == null) {
                dateStartSql = null;
                dateEndSql = null;

            } else {
                dateStartSql = this.convertJavaDateToSqlDate(
                        this.getStartDate());
                dateEndSql = this.convertJavaDateToSqlDate(
                        this.getEndDate());
            }

            this.setConsultBaremoGeneralOutputList(
                    this.serviceBaremos.findConsultBaremoGeneralOutput(
                            null,
                            null, 0));

            // Visibility
            this.getConsultBaremoGeneralOutputList();

            if (!this.getConsultBaremoGeneralOutputList().isEmpty()) {
                this.setBtnRest(false);
                this.setBtnExportar(false);
                this.setVisibilitycolumDateBaremo(false);
                this.setVisibilitycolumNameTratment(false);
                this.setVisibilitycolumBaremo(true);
                RequestContext.getCurrentInstance().update("form:btnResetKey");
                RequestContext.getCurrentInstance().update("form:growReportReimbursement");
                RequestContext.getCurrentInstance().update("form:dateBaremo");
                RequestContext.getCurrentInstance().update("form:nameTratment");
                RequestContext.getCurrentInstance().update("form:baremo");
                RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
            } else {
                this.setVisibilitycolumDateBaremo(true);
                this.setVisibilitycolumNameTratment(true);
                this.setVisibilitycolumBaremo(true);
                this.setBtnExportar(true);
                RequestContext.getCurrentInstance().update("form:growReportReimbursement");
                RequestContext.getCurrentInstance().update("form:dateBaremo");
                RequestContext.getCurrentInstance().update("form:nameTratment");
                RequestContext.getCurrentInstance().update("form:baremo");
                RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
            }
        }
    }
}
