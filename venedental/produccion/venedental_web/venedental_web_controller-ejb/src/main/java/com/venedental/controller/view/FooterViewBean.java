/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.ListaIconos;
import model.ListaLinks;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "footerViewBean")
@ViewScoped
public class FooterViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    List<ListaLinks> listaLinks;
    List<ListaIconos> listaSociales;

    public FooterViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaLinks(new ArrayList<ListaLinks>());
        this.listaLinks.add(new ListaLinks("", "/views/web/venedental.xhtml", "/img/venedentalFooter.png"));
        this.listaLinks.add(new ListaLinks("", "/views/web/venedermo.xhtml", "/img/venedermoFooter.png"));
        this.listaLinks.add(new ListaLinks("", "/views/web/venedevisual.xhtml", "/img/venedevisualFooter.png"));
        this.listaLinks.add(new ListaLinks("", "/views/web/veneterapias.xhtml", "/img/veneterapiasFooter.png"));
        this.listaLinks.add(new ListaLinks("", "/views/web/venepsico.xhtml", "/img/venepsicoFooter.png"));

        this.setListaSociales(new ArrayList<ListaIconos>());
        this.listaSociales.add(new ListaIconos("", "#", "/img/social_icon3.png", -28, 0));
        this.listaSociales.add(new ListaIconos("", "#", "/img/social_icon3.png", -2, 0));
        this.listaSociales.add(new ListaIconos("", "#", "/img/social_icon3.png", -107, 0));
        this.listaSociales.add(new ListaIconos("", "#", "/img/social_icon3.png", -81, 0));
        this.listaSociales.add(new ListaIconos("", "#", "/img/social_icon3.png", -55, 0));

    }

    public List<ListaLinks> getListaLinks() {
        return listaLinks;
    }

    public void setListaLinks(List<ListaLinks> listaLinks) {
        this.listaLinks = listaLinks;
    }

    public List<ListaIconos> getListaSociales() {
        return listaSociales;
    }

    public void setListaSociales(List<ListaIconos> listaSociales) {
        this.listaSociales = listaSociales;
    }

}
