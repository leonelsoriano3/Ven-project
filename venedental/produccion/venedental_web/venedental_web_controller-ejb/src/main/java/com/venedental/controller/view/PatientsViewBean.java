package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.model.Patients;
import com.venedental.model.PlansTreatments;
import com.venedental.model.SpecialistJob;
import com.venedental.model.Specialists;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "patientsViewBean")
@ViewScoped
public class PatientsViewBean extends BaseBean {

    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;
    
    
    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;

    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;

    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;

    private static final long serialVersionUID = -5547104134772667835L;
    
    
    private Patients selectedPatients;

    private Patients patientsAutocomplete;

    private List<Patients> listaPatients;

    private List<Patients> listaPatientsFiltrada;

    private List<SpecialistJob> listaPatientsSpecialist;

    private String[] newPatients;

    private Patients newPatientsObj;

    private String[] editPatients;

    private Patients addPatients;

    private Patients beforeEditPatients;

    private boolean visible;

    public PatientsViewBean() {
        this.newPatients = new String[10];
        this.editPatients = new String[10];
        this.addPatients = new Patients();
        this.visible = false;
    }

    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
    }

    

    public PatientsFacadeLocal getPatientsFacadeLocal() {
        return patientsFacadeLocal;
    }

    public void setPatientsFacadeLocal(PatientsFacadeLocal patientsFacadeLocal) {
        this.patientsFacadeLocal = patientsFacadeLocal;
    }
    
    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

  
    
    
    
   
    

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Patients getAddPatients() {
        return addPatients;
    }

    public void setAddPatients(Patients addPatients) {
        this.addPatients = addPatients;
    }

    public String[] getEditPatients() {
        return editPatients;
    }

    public void setEditPatients(String[] editPatients) {
        this.editPatients = editPatients;
    }

    public String[] getNewPatients() {
        return newPatients;
    }

    public Patients getPatientsAutocomplete() {
            
        return patientsAutocomplete;
    }

    public void setPatientsAutocomplete(Patients patientsAutocomplete) {
        this.patientsAutocomplete = patientsAutocomplete;
    }

    public void setNewPatients(String[] newPatients) {
        this.newPatients = newPatients;
    }

    public Patients getNewPatientsObj() {
        return newPatientsObj;
    }

    public void setNewPatientsObj(Patients newPatientsObj) {
        this.newPatientsObj = newPatientsObj;
    }

    public Patients getSelectedPatients() {
        return selectedPatients;
    }

    public void setSelectedPatients(Patients selectedPatients) {
        this.selectedPatients = selectedPatients;
        this.setEditPatients(new String[]{selectedPatients.getId().toString(), selectedPatients.getCompleteName()});

    }

    public List<Patients> completeIdentityNumber(String query) {
        Integer identityNumber = Integer.valueOf(query);
        this.listaPatients = this.patientsFacadeLocal.findByIdentityNumber(identityNumber);
        return this.listaPatients;
    }


    public List<Patients> getListaPatients() {
        if (this.listaPatients == null) {
            this.listaPatients = new ArrayList<>();
        }
        return listaPatients;
    }

    public void setListaPatients(List<Patients> listaPatients) {
        this.listaPatients = listaPatients;
    }

    public List<SpecialistJob> getListaPatientsSpecialist() {
        if (this.listaPatientsSpecialist == null) {
            this.listaPatientsSpecialist = new ArrayList<>();
        }
        return listaPatientsSpecialist;
    }

    public void setListaPatientsSpecialist(List<SpecialistJob> listaPatientsSpecialist) {
        this.listaPatientsSpecialist = listaPatientsSpecialist;
    }

    public List<Patients> getListaPatientsFiltrada() {
        if (this.listaPatientsFiltrada == null) {
            this.listaPatientsFiltrada = new ArrayList<>();
        }
        return listaPatientsFiltrada;
    }

    public void setListaPatientsFiltrada(List<Patients> listaPatientsFiltrada) {
        this.listaPatientsFiltrada = listaPatientsFiltrada;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public Patients getBeforeEditPatients() {
        return beforeEditPatients;
    }

    public void limpiarObjetosEdit() {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        this.setEditPatients(new String[]{this.getBeforeEditPatients().getId().toString()});
    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }
    
//     public List<Patients> filterPatients(String query) {   
//
//        this.setListaPatients(this.getPatientsFacadeLocal().findByIdentityNumberOrNamePatients(query));
//        this.validate();
//        return this.listaPatients;
//    }

}
