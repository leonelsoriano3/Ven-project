package com.venedental.filter;

import com.venedental.controller.session.UsersSessionBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import services.utils.PropertiesLocator;

public class SessionFilter implements Filter {


    
    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        UsersSessionBean usersSessionBean = (session != null) ? (UsersSessionBean) session.getAttribute("usersSessionBean") : null;

        if (usersSessionBean != null) {
            if (usersSessionBean.getAutenticado() != null) {                
                
                chain.doFilter(request, response);
            } else {
                
                StringBuilder ruta = new StringBuilder();
                ruta.append("http://").append(PropertiesLocator.getProperty("serverAddress")).append(":").append(PropertiesLocator.getProperty("serverPort")).append("/index.xhtml");
                httpResponse.sendRedirect(ruta.toString());
                
                chain.doFilter(request, response);
            }

        } else {
            
            StringBuilder ruta = new StringBuilder();
            ruta.append("http://").append(PropertiesLocator.getProperty("serverAddress")).append(":").append(PropertiesLocator.getProperty("serverPort")).append("/index.xhtml");
            httpResponse.sendRedirect(ruta.toString());
            
        }

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // TODO Auto-generated method stub
    }
}
