package com.venedental.enums;

/**
 *
 * @author Edgar Mosquera
 */
public enum RolUser {

    ROLE_ADMINISTRADOR(1), ROLE_COORDINADOR(2), ROLE_ANALISTA(3),
    ROLE_ANALISTA_AVANZADO(4),ROLE_EVALUADOR(5),ROLE_OPERADOR(6),
    ROLE_OPERADOR_AVANZADO(7),ROLE_ESPECIALISTA(8),ROLE_ANALISTA_DE_PAGO(9),
    ROLE_DESARROLLO_PRUEBAS(10),ROLE_ANALISTA_REEMBOLSO(11),ROLE_GERENCIA_OPERACIONES(12),
    ROLE_EVALUADOR_SUPERIOR(13),ROLE_ANALISTA_DE_PAGO_REEMBOLSO(14);

    Integer valor;

    private RolUser(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
