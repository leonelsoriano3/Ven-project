/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.TypeAttentionViewBean;
import com.venedental.model.Cities;
import com.venedental.model.TypeAttention;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "typeAttentionConverter", forClass = TypeAttention.class)
public class TypeAttentionConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        TypeAttentionViewBean typeAttentionViewBean = (TypeAttentionViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "typeAttentionViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (TypeAttention type: typeAttentionViewBean.getListaTypeAttention()) {
                    if (type.getId() == numero) {
                        return type;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tipo de Atención Invalida"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((TypeAttention) value).getId());
        }

    }

}
