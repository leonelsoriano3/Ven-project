package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.ReimbursementViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.facade.BanksPatientsFacadeLocal;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.RequirementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.model.BanksPatients;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Reimbursement;
import com.venedental.model.Requirement;
import com.venedental.model.StatusReimbursement;
import com.venedental.model.security.Users;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;

@ManagedBean(name = "reimbursementRequestBean")
@RequestScoped
public class ReimbursementRequestBean extends BaseBean implements Serializable {

    /* EJBs */
    @EJB
    private ReimbursementFacadeLocal reembolsoLocal;

    @EJB
    private RequirementFacadeLocal requerimientoLocal;

    @EJB
    private BinnacleReimbursementFacadeLocal binnacleLocal;

    @EJB
    private PatientsFacadeLocal patientsLocal;

    @EJB
    private StatusReimbursementFacadeLocal statusReimbursementLocal;

    @EJB
    private BanksPatientsFacadeLocal banksPatientsFacadeLocal;

    /* Attributes */
    private Reimbursement reimbursementSelected;
    private Requirement requirementSelected;

    /* ManagedProperty */
    @ManagedProperty(value = "#{ReimbursementViewBean}")
    private ReimbursementViewBean reimbursementViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;

    /* Constructs */
    public ReimbursementRequestBean() {
    }

    /* Getter and Setter */
    public Reimbursement getReimbursementSelected() {
        return reimbursementSelected;
    }

    public void setReimbursementSelected(Reimbursement reimbursementSelected) {
        this.reimbursementSelected = reimbursementSelected;
    }

    public Requirement getRequirementSelected() {
        return requirementSelected;
    }
    public void setRequirementSelected(Requirement requirementSelected) {
        this.requirementSelected = requirementSelected;
    }

    public ReimbursementViewBean getReimbursementViewBean() {
        return reimbursementViewBean;
    }

    public void setReimbursementViewBean(ReimbursementViewBean reimbursementViewBean) {
        this.reimbursementViewBean = reimbursementViewBean;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    /**
     *
     * method for verifying data refund if they were already updated.
     *
     * @param reimbuserment
     * @return
     */
    public boolean verifyReimbursementApprove(Reimbursement reimbuserment) {

        Reimbursement reimbVerify = this.reembolsoLocal.find(reimbuserment.getId());

        if (Objects.equals(reimbVerify.getStatusReimbursmentId().getId(), StatusReimbursementEnum.APROBADO.getValor())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifyReimbursementReject(Reimbursement reimbuserment) {
        Reimbursement reimbVerify = this.reembolsoLocal.find(reimbuserment.getId());

        if (Objects.equals(reimbVerify.getStatusReimbursmentId().getId(), StatusReimbursementEnum.RECHAZADO.getValor())) {

            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * Method to approve applications for reimbursement
     */
    public void approveReimbursment() {

        try {

            int valorStatus = StatusReimbursementEnum.APROBADO.getValor();
            this.getReimbursementViewBean().getStatusReimbursmentId();
            Users user = this.securityViewBean.obtainUser();

            int contAprobadas = 0;
            int contRechazadas = 0;

            for (Reimbursement reimbursement : this.getReimbursementViewBean().getReimbursementSeleccionadoList()) {

                boolean verifApprove = verifyReimbursementApprove(reimbursement);

                if (verifApprove == true) {

                    contAprobadas++;
                } else {
                    boolean verifyReject = verifyReimbursementReject(reimbursement);

                    if (verifyReject == true) {
                        contRechazadas++;
                    } else {

                        StatusReimbursement statusObjApprove = statusReimbursementLocal.find(valorStatus);

                        // Setter and Request Order
                        reimbursement.setStatusReimbursmentId(statusObjApprove);

                        this.reembolsoLocal.edit(reimbursement);

                        // SEND MAIL
                        this.getReimbursementViewBean().sendMailPatient(reimbursement);

                        // TRANSFORM DATE
                        Date fecha = new Date();
                        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                        String fechaSolicitud = sdfSolicitud.format(fecha);
                        Date dateBinnacle = Transformar.StringToDate(fechaSolicitud);

                        // SETTER Binnacle
                        BinnacleReimbursement binnacleReimbursement = new BinnacleReimbursement();

                        binnacleReimbursement.setReimbursementId(reimbursement);
                        binnacleReimbursement.setDate(dateBinnacle);
                        binnacleReimbursement.setUserId(user.getId());
                        binnacleReimbursement.setStatusReimbursementId(statusObjApprove);

                        // ADD
                        this.binnacleLocal.create(binnacleReimbursement);
                    }

                }

            }

            if (contAprobadas > 0) {
                System.out.println("Condicion Aprobar: Ya han sido aprobada(s) la cantidad de: " + contAprobadas + "");

            }

            if (contRechazadas > 0) {
                System.out.println("Condicion Aprobar: Ya han sido rechazadas(s) la cantidad de: " + contRechazadas + "");
            }

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));

            this.getReimbursementViewBean().setReimbursementList(new ArrayList<Reimbursement>());
            this.getReimbursementViewBean().reimbursementRequestsRejected();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Al Parecer algo Salio Mal", e.getMessage()));
        }

        // Faces on Context - Request
        RequestContext.getCurrentInstance().update("form:messageReimbursement");
    }

    /**
     *
     * Method to reject applications for reimbursement
     */
    public void rejectReimbursment() {

        try {

            this.getReimbursementViewBean().getStatusReimbursmentId();

            int valorStatus = StatusReimbursementEnum.RECHAZADO.getValor();

            if (this.getReimbursementViewBean().getNewObservations()[1] != null
                    && this.getReimbursementViewBean().getNewObservations()[1] != "") {

                int contAprobadas = 0;
                int contRechazadas = 0;

                for (Reimbursement reimbursement : this.getReimbursementViewBean().getReimbursementSeleccionadoList()) {

                    boolean verifApprove = verifyReimbursementApprove(reimbursement);



                    if (verifApprove == true) {
                        contAprobadas++;
                    } else {
                        boolean verifyReject = verifyReimbursementReject(reimbursement);

                        if (verifyReject == true) {
                            contRechazadas++;
                        } else {

                            StatusReimbursement statusObj = statusReimbursementLocal.find(valorStatus);

                            // Setter and Request Order
                            reimbursement.setConcept(this.getReimbursementViewBean().getNewObservations()[1]);
                            reimbursement.setStatusReimbursmentId(statusObj);

                            this.reembolsoLocal.edit(reimbursement);

                            // SEND MAIL
                            this.getReimbursementViewBean().sendMailPatient(reimbursement);

                            // TRANSFORM DATE
                            Date fecha = new Date();
                            SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                            String fechaSolicitud = sdfSolicitud.format(fecha);
                            Date dateBinnacle = Transformar.StringToDate(fechaSolicitud);

                            // SETTER Binnacle
                            Users user = this.securityViewBean.obtainUser();

                            BinnacleReimbursement binnacleReimbursement = new BinnacleReimbursement();

                            binnacleReimbursement.setReimbursementId(reimbursement);
                            binnacleReimbursement.setDate(dateBinnacle);
                            binnacleReimbursement.setUserId(user.getId());
                            binnacleReimbursement.setStatusReimbursementId(statusObj);

                            // ADD
                            this.binnacleLocal.create(binnacleReimbursement);
                        }

                    }


                }

                if (contAprobadas > 0) {
                    System.out.println("Condicion Rechazar: Ya han sido aprobada(s) la cantidad de: " + contAprobadas + "");

                }

                if (contRechazadas > 0) {
                    System.out.println("Condicion Rechazar: Ya han sido rechazadas(s) la cantidad de: " + contRechazadas + "");
                }

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));

                this.getReimbursementViewBean().setReimbursementList(new ArrayList<Reimbursement>());
                this.getReimbursementViewBean().setNewObservations(new String[2]);

                this.accionesRequestBean.cerrarDialogoView("reimbursementRejectD");

                this.getReimbursementViewBean().reimbursementRequestsRejected();

            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Falta ingresar el motivo de rechazo"));

            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_FATAL, "Al Parecer algo Salio Mal", e.getMessage()));
        }

        // Faces on Context - Request
        RequestContext.getCurrentInstance().update("form:messageReimbursement");

    }

    /**
     * Method that updates financial data
     *
     *
     */
    public void reimbursementUpdateDataFinnancial() {

        RequestContext.getCurrentInstance().update("form:panelFinnancialData");

        try {
            if (this.getReimbursementViewBean().getReimbursementSelected().getInvoice_number() != null
                    && this.getReimbursementViewBean().getReimbursementSelected().getInvoice_date() != null
                    && this.getReimbursementViewBean().getReimbursementSelected().getAmount() != 0
                    && this.getReimbursementViewBean().getReimbursementSelected().getAccountNumberSubTring() != null) {

                if (this.getReimbursementViewBean().getReimbursementSelected().getAccountNumberSubTring().length() < 16
                        || this.getReimbursementViewBean().getReimbursementSelected().getAccountNumberSubTring().equals("")
                        || this.getReimbursementViewBean().getReimbursementSelected().getAccountNumberSubTring() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "El número de cuenta debe contener 16 dígitos"));
                } else {

                    // SETTER
                    BanksPatients banksPatients = this.banksPatientsFacadeLocal.find(this.getReimbursementViewBean().getReimbursementSelected().getBanksPatientsId().getId());

                    String code = this.getReimbursementViewBean().getSelectedBanks().getCode();
                    String accountPatients = this.getReimbursementViewBean().getReimbursementSelected().getAccountNumberSubTring();

                    StringBuilder AccountNumberFinality = new StringBuilder();

                    AccountNumberFinality.append(code).append(accountPatients);

                    banksPatients.setAccountNumber(AccountNumberFinality.toString());
                    banksPatients.setBankId(this.getReimbursementViewBean().getSelectedBanks());

                    this.banksPatientsFacadeLocal.edit(banksPatients);
                    this.reembolsoLocal.edit(this.getReimbursementViewBean().getReimbursementSelected());

                    RequestContext.getCurrentInstance().execute("PF('financieroDialog').hide()");
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));

                    this.getReimbursementViewBean().reimbursementRequestsRejected();
                }
                RequestContext.getCurrentInstance().update("form:tableReimbursement");
                RequestContext.getCurrentInstance().update("form:financieroDialog");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "No deben haber campos vacios e incompletos"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_FATAL, "Al Parecer algo Salio Mal", e.getMessage()));
        }
        RequestContext.getCurrentInstance().update("form:messageReimbursement");

    }

    public void viewDialogDataFinnancial(String dialogo, Reimbursement reimbursement) {

        // Setter Transient
        this.getReimbursementViewBean().selectedReimbursementViewDialog(reimbursement);

        // DIALOG
        RequestContext.getCurrentInstance().execute("PF('" + dialogo + "').show()");
        RequestContext.getCurrentInstance().update("form:" + dialogo);
    }

    /**
     * Method for dialog exit data reject
     *
     * @param dialogo
     */
    public void exitDialogRejectData(String dialogo) {

        this.accionesRequestBean.cerrarDialogoView(dialogo);
        this.getReimbursementViewBean().setNewObservations(new String[2]);
    }

    /**
     * Method for verify exit dialog
     *
     *
     */
    public void exitVerifyConfirmDialog() {
        if (!this.getReimbursementViewBean().getReimbursementSeleccionadoList().isEmpty()) {
            exitDialogRejectData("reimbursementRejectD");
            this.accionesRequestBean.cerrarDialogoView("financieroDialog");
        }

    }

}
