package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialOuput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.Specialists;
import com.venedental.services.ServicePaymentReport;
import com.venedental.services.ServiceSpecialists;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import services.utils.Transformar;

@ManagedBean(name="keysAnalisysReportViewBean")
@ViewScoped
public class KeysAnalisysReportViewBean extends BaseBean implements Serializable {

    /* Services */
    
    @EJB
    private ServiceSpecialists serviceSpecialist;
    
    @EJB
    private ServicePaymentReport servicePaymentReport;
    
    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;
    
    /* Attributes */
    
    private Specialists specialist;
    private Specialists selectedSpecialist; 
    
    private List<PaymentReportkeysAnalisysByCriterialOuput> paymentReportkeysAnalisysByCriterialOuputList;
    
    /* Others */
    private boolean activeEndDate,activeBtnSearch,activeBtnReset,activeBtnExportData;
    private boolean activeCedRIF,activeNumberRequest,activeStartDate;
    private boolean visibilitySpecialistName,visibilityCEDRIF,visibilityInvoice;

    private Date startDate, endDate, maxDate;
    private String minDate;
    private String[] filtersData;
    
    /* Attributes Jaspert Reports */
    private Map<String, Object> params = new HashMap<String, Object>();
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private JasperPrint jasperPrint;
    private String reportPath;
    private String logoPath;
    private HttpServletResponse httpServletResponse;
    private ServletOutputStream servletOutputStream;
    private JRXlsxExporter docxExporter;
    
    /* Constructs */
    public KeysAnalisysReportViewBean() {
        
    }
    
    @PostConstruct
    public void init(){
                
        // Reset Buttons
        this.setActiveBtnSearch(true);
        this.setActiveBtnReset(true);
        this.setActiveBtnExportData(true);

        this.setActiveNumberRequest(false);
        this.setActiveStartDate(false);
        this.setActiveEndDate(true);
        this.setActiveCedRIF(false);

        this.setFiltersData(new String[2]);
        this.getFiltersData()[0]="";
        this.getFiltersData()[1]="";
       
        this.setSpecialist(new Specialists());
        this.getSpecialist();

        // reset filters      
        this.setStartDate(null);
        this.setEndDate(null);
        
        this.getStartDate();
        this.getEndDate();
        

        

        // reset column visibility criterial
        this.setVisibilityCEDRIF(true);
        this.setVisibilityInvoice(true);
        this.setVisibilitySpecialistName(true);

        

        // reset table
        this.setPaymentReportkeysAnalisysByCriterialOuputList(new ArrayList<PaymentReportkeysAnalisysByCriterialOuput>());
        this.getPaymentReportkeysAnalisysByCriterialOuputList();
        
        
        RequestContext.getCurrentInstance().update("form:startDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:txtIdentity");
        RequestContext.getCurrentInstance().update("form:txtNumberInvoice");
        
        RequestContext.getCurrentInstance().update("form:tableReportKeysAnalisys");
        
        RequestContext.getCurrentInstance().update("form:columnVisibilityDoctorClinic");
        RequestContext.getCurrentInstance().update("form:columnVisibilityRIFCED");
        RequestContext.getCurrentInstance().update("form:columnVisibilityInvoicesNumber");

        RequestContext.getCurrentInstance().update("form:btnSearchData");
        RequestContext.getCurrentInstance().update("form:btnResetData");
        RequestContext.getCurrentInstance().update("form:btnExportData");
        
        dateLimit();
        
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }
        
    public String[] getFiltersData() {
        return filtersData;
    }

    /* Getter and Setter */
    public void setFiltersData(String[] filtersData) {    
        this.filtersData = filtersData;
    }

    public boolean isVisibilitySpecialistName() {
        return visibilitySpecialistName;
    }

    public void setVisibilitySpecialistName(boolean visibilitySpecialistName) {
        this.visibilitySpecialistName = visibilitySpecialistName;
    }

    public boolean isVisibilityCEDRIF() {
        return visibilityCEDRIF;
    }

    public void setVisibilityCEDRIF(boolean visibilityCEDRIF) {
        this.visibilityCEDRIF = visibilityCEDRIF;
    }

    public boolean isVisibilityInvoice() {
        return visibilityInvoice;
    }

    public void setVisibilityInvoice(boolean visibilityInvoice) {
        this.visibilityInvoice = visibilityInvoice;
    }

    public boolean isActiveCedRIF() {
        return activeCedRIF;
    }

    public void setActiveCedRIF(boolean activeCedRIF) {
        this.activeCedRIF = activeCedRIF;
    }

    public boolean isActiveNumberRequest() {
        return activeNumberRequest;
    }

    public void setActiveNumberRequest(boolean activeNumberRequest) {
        this.activeNumberRequest = activeNumberRequest;
    }

    public boolean isActiveStartDate() {
        return activeStartDate;
    }

    public void setActiveStartDate(boolean activeStartDate) {
        this.activeStartDate = activeStartDate;
    }
        
    public boolean isActiveEndDate() {
        return activeEndDate;
    }

    public void setActiveEndDate(boolean activeEndDate) {
        this.activeEndDate = activeEndDate;
    }
    
    

    /**
     *  obtener los datos del managed bean report.
     * 
     * @return 
     */
    public ConfigViewBeanReport getExport() {
        return export;
    }
    
    /**
     * settear los valor de las propiedades del managed bean report.
     * 
     * @param export 
     */
    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    /**
     *  obtener el valor para los parametros asignados en el reporte.
     * 
     * @return 
     */
    public Map<String, Object> getParams() {
        return params;
    }
    
    /**
     * almacenar el valor para los parametros a usar en el reporte.
     * 
     * @param params 
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
    
    /**
     * datasource general para el reporte.
     * 
     * @return 
     */
    public JRBeanCollectionDataSource getBeanCollectionDataSource() {
        return beanCollectionDataSource;
    }
    
    /**
     * setter general para el datasource del reporte.
     * 
     * @param beanCollectionDataSource 
     */
    public void setBeanCollectionDataSource(JRBeanCollectionDataSource beanCollectionDataSource) {
        this.beanCollectionDataSource = beanCollectionDataSource;
    }
    
    /**
     * obtener las propiedades del jasper print
     * 
     * @return 
     */
    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }
    
    /**
     * almacenar las propiedades del jasper print
     * 
     * @param jasperPrint 
     */
    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }
    
    /**
     * obtener la ubicacion del ejecutable del reporte
     * 
     * @return 
     */
    public String getReportPath() {
        return reportPath;
    }
    
    /**
     * almacenar la ubicacion del ejectutable del reporte.
     * 
     * @param reportPath 
     */
    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }
    
    /**
     * obtener la ruta del logo
     * 
     * @return 
     */
    public String getLogoPath() {
        return logoPath;
    }
    
    /**
     * almacenar la ruta del logo
     * 
     * @param logoPath 
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }
    
    /**
     * obtener la peticion via get / post mediante el protocolo HTTP
     * 
     * @return 
     */
    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    /**
     * envio de peticion via http 
     * 
     * @param httpServletResponse 
     */
    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }
    
    /**
     * Obtener las propiedades para generar reportes via excel
     * 
     * @return 
     */
    public JRXlsxExporter getDocxExporter() {
        return docxExporter;
    }

    /**
     * almacenar las propiedades para generar reportes via excel
     * 
     * @param docxExporter 
     */
    public void setDocxExporter(JRXlsxExporter docxExporter) {
        this.docxExporter = docxExporter;
    }
    
    /**
     * obtener el listado general de la data para proveniente de los servicios 
     * de busqueda.
     * 
     * @return 
     */
    public List<PaymentReportkeysAnalisysByCriterialOuput> getPaymentReportkeysAnalisysByCriterialOuputList() {
        return paymentReportkeysAnalisysByCriterialOuputList;
    }
    
    /**
     * almacenar la lista general con los datos proveniente del servicio de 
     * busqueda asociado.
     * 
     * @param paymentReportkeysAnalisysByCriterialOuputList 
     */
    public void setPaymentReportkeysAnalisysByCriterialOuputList(List<PaymentReportkeysAnalisysByCriterialOuput> paymentReportkeysAnalisysByCriterialOuputList) {
        this.paymentReportkeysAnalisysByCriterialOuputList = paymentReportkeysAnalisysByCriterialOuputList;
    }
    
    /**
     * obtener el valor del especialista
     * 
     * @return 
     */
    public Specialists getSpecialist() {
        return specialist;
    }
    
    /**
     * almacenar el valor del especialista
     * 
     * @param specialist 
     */
    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    /**
     * obtener el valor del especialista seleccionado
     * 
     * @return 
     */
    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    /**
     * almacenar el valor del especialista seleccionado
     * 
     * @param selectedSpecialist 
     */
    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }
    
    /**
     * obtener el valor nativo de la fecha desde.
     * 
     * @return 
     */
    public Date getStartDate() {
        return startDate;
    }
    
    /**
     * almacenar el valor de la fecha desde.
     * 
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    /**
     * obtener el valor de la fecha hasta
     * 
     * @return 
     */
    public Date getEndDate() {
        return endDate;
    }
    
    /**
     * almacenar el valor de la fecha hasta
     * 
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    /**
     * obtener el valor maximo de la fecha para el rango en el filtro.
     * 
     * @return 
     */
    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        
        return maxDate;
    }
    
    /**
     *  almacenar el valor maximo de la fecha para el rango en el filtro.
     * 
     * @param maxDate 
     */
    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }
    
    /**
     * obtener el valor para activar y desactivar el boton buscar datos
     * 
     * @return 
     */
    public boolean isActiveBtnSearch() {
        return activeBtnSearch;
    }
    
    /**
     * almacenar el valor para activar y desactivar el boton buscar
     * 
     * @param activeBtnSearch 
     */
    public void setActiveBtnSearch(boolean activeBtnSearch) {
        this.activeBtnSearch = activeBtnSearch;
    }
    
    /**
     * obtener el valor para activar y desactivar el boton restablecer
     * 
     * @return 
     */
    public boolean isActiveBtnReset() {
        return activeBtnReset;
    }
    
    /**
     * almacenar el valor para activar y desactivar el boton restablecer
     * 
     * @param activeBtnReset 
     */
    public void setActiveBtnReset(boolean activeBtnReset) {
        this.activeBtnReset = activeBtnReset;
    }
    
    /**
     * obtener el valor para activar y desactivar el boton exportar datos
     * 
     * @return 
     */
    public boolean isActiveBtnExportData() {
        return activeBtnExportData;
    }
    
    /**
     * almacenar el valor para activar y desactivar el boton exportar datos.
     * 
     * @param activeBtnExportData 
     */
    public void setActiveBtnExportData(boolean activeBtnExportData) {
        this.activeBtnExportData = activeBtnExportData;
    }
    
 
    
    /**
     *  -----------------------------------------------------------------------
     *  |                METODOS A UTILIZAR EN EL CONTROLADOR 
     *  |                    DE ALCANCE VISTA (VIEWS)
     *  -----------------------------------------------------------------------
     */
    
    /**
     * metodo para verificar si la tabla contenida para el reporte esta 
     * totalmente vacia.
     * 
     * @return 
     */
    public boolean verifyDataTable(){
        
        if(!this.getPaymentReportkeysAnalisysByCriterialOuputList().isEmpty()){
            return true;
        }else{
            
            return false;
        }
      
    }
    
    /**
     * Metodo para activar el campo fecha desde y ced / rif.
     * 
     * 
     */
    public void activeEndDateSelect(){
        
        try {
            
            if(this.getStartDate()!=null && !this.getStartDate().equals("") ){
                
                this.setActiveBtnReset(false);
                this.setActiveEndDate(false);
                this.setActiveNumberRequest(true);
            }else{
                
                this.setActiveBtnReset(true);
                this.setActiveEndDate(true);
                this.setActiveNumberRequest(false);
                this.setEndDate(new Date());
                this.setEndDate(null);
                this.setActiveBtnSearch(true);
                
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:txtNumberInvoice");
        RequestContext.getCurrentInstance().update("form:btnResetData");
        RequestContext.getCurrentInstance().update("form:btnSearchData");
        
        
    }
    
    /**
     * Metodo para activar el boton segun el criterio:
     * - solo por rango de fecha (desde y hasta)
     * - solo por rango de fecha y especialista.
     * 
     */
    public void activeSearchByRangeDateAndSpecialist(){
       
        try {
            
          if(this.getStartDate()!=null && this.getEndDate()!=null
                  && this.getSpecialist()!=null){
              
              this.setActiveBtnSearch(false);
              this.setActiveBtnReset(false);
              this.setActiveNumberRequest(true);
              
          }else if(this.getStartDate()!=null && this.getEndDate()!=null
                  && this.getSpecialist()==null){
              
              this.setActiveBtnSearch(false);
              this.setActiveBtnReset(false);
              this.setActiveNumberRequest(true);
          }else{
              this.setActiveBtnSearch(true);
              this.setActiveBtnReset(true);
              this.setActiveNumberRequest(true);
          }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        RequestContext.getCurrentInstance().update("form:txtNumberInvoice");
        RequestContext.getCurrentInstance().update("form:btnSearchData");
        RequestContext.getCurrentInstance().update("form:btnResetData");
        RequestContext.getCurrentInstance().update("form:btnSearchData");
    }
    
    /**
     * Metodo para validar el area de filtro para el criterio de busqueda
     * por solamente nro de factura.
     * 
     * 
     */
    public void activeNumberInvoicesSearch(){
        
        try {
            
          if(this.getFiltersData()[0]!=null){
                
                // Se desactivan los filtros de rango de fechas y especialista
                this.setActiveStartDate(true);
                this.setActiveEndDate(true);
                this.setActiveCedRIF(true);
                
                // se activan los button
                this.setActiveBtnSearch(false);
                this.setActiveBtnReset(false);
                this.setActiveBtnExportData(true);
                
            }else{
                FacesContext.getCurrentInstance().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_WARN, 
                        "Falta seleccionar el nro. de factura", 
                        "Falta seleccionar el nro. de factura"
                        )
                    );
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        RequestContext.getCurrentInstance().update("form:growInvoicesReportMessage");
        RequestContext.getCurrentInstance().update("form:startDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:txtIdentity");
        
        RequestContext.getCurrentInstance().update("form:btnSearchData");
        RequestContext.getCurrentInstance().update("form:btnResetData");
        RequestContext.getCurrentInstance().update("form:btnExportData");
    }
    /**
     * Metodo para cargar el reporte
     * 
     * @throws IOException
     * @throws JRException 
     */
    public void loadReports() throws IOException, JRException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("heal_logo2", logoPath);
        
        if (this.getStartDate()!=null && this.getEndDate()!=null
            && this.getSpecialist()==null 
            && this.getFiltersData()[0].isEmpty()) {
            
            this.params.put("reportKeysAnalisysList1", 
               new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList)
            );
            
            this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/keysAnalisysInvoicesReports/paymentReportByCriterial_I.jasper");
            this.beanCollectionDataSource = new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList);
            this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
            
            
        }else if(this.getStartDate()!=null && this.getEndDate()!=null
            && this.getSpecialist()!=null && this.getFiltersData()[0].isEmpty()){
            this.params.put("reportKeysAnalisysList2", 
               new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList)
            );
            
            this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/keysAnalisysInvoicesReports/paymentReportByCriterial_II.jasper");
            this.beanCollectionDataSource = new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList);
            this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
            
            
        }else if(this.getStartDate()==null && this.getEndDate()==null
            && this.getSpecialist().getId()==null && !this.getFiltersData()[0].isEmpty()){
            
            this.params.put("reportKeysAnalisysList3", 
               new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList)
            );
            
            this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/keysAnalisysInvoicesReports/paymentReportByCriterial_III.jasper");
            this.beanCollectionDataSource = new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList);
            this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
            
        }else if(this.getStartDate()==null && this.getEndDate()==null
            && this.getSpecialist().getId()!=null && this.getFiltersData()[0].isEmpty()){
            
            this.params.put("reportKeysAnalisysList4", 
               new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList)
            );
            
            this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/keysAnalisysInvoicesReports/paymentReportByCriterial_IV.jasper");
            this.beanCollectionDataSource = new JRBeanCollectionDataSource(paymentReportkeysAnalisysByCriterialOuputList);
            this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
            
        }
        else {
            System.out.println("Error al generar el reporte, por falla de los criterios de busqueda");
        }

          
        
        
    }
    
    /**
     *  metodo para generar el reporte .xls con las claves abonadas segun el 
     *  criterio de busqueda.
     * 
     * @param actionEvent
     * @throws JRException
     * @throws IOException
     * @throws Exception 
     */
    public void sendReportXLS(ActionEvent actionEvent) 
            throws JRException, IOException, Exception {
      
        try {
            
         if(!this.getPaymentReportkeysAnalisysByCriterialOuputList().isEmpty()){
           
           // generar reporte por rango de fechas y especialista.
           if(this.getStartDate()!=null && this.getEndDate()!=null
                    && this.getSpecialist()!=null
                    && this.getFiltersData()[0].isEmpty()){
               
              
               this.params.put("startDate", this.getStartDate());
               this.params.put("endDate", this.getEndDate());
               this.params.put("specialistName", this.getSpecialist().getCompleteName().toUpperCase());
               this.params.put("specialistCEDRIF", 
                 this.getPaymentReportkeysAnalisysByCriterialOuputList().get(0).getIdentityNumberRIFSpecialistsClinic()   
                );
                  
               // Date
               SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
               sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

               String dateEndExport = sdfSolicitud.format(new Date());    
                  
               loadReports();
                  
               this.export.xlsx(
                      jasperPrint,
                      "ReporteClavesAbonadas-"+this.getSpecialist().getCompleteName().toUpperCase().replace(" ","-"),
                      dateEndExport
                );
            // generar reporte por solo rango de fechas      
           }
           
           if(this.getStartDate()!=null && this.getEndDate()!=null
                 && this.getSpecialist()==null
                 && this.getFiltersData()[0].isEmpty()){
                   
                this.params.put("startDate", this.getStartDate());
                this.params.put("endDate", this.getEndDate());
                   
                // Date
                SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
                sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                
                String dateStart = sdfSolicitud.format(this.getStartDate()).replaceAll("/","-");
                String dateEnd = sdfSolicitud.format(this.getEndDate()).replaceAll("/","-");
                
                loadReports();
                                
                this.export.xlsx(
                      jasperPrint,
                      "ReporteClavesAbonadasDesde"+dateStart+"Hasta"+dateEnd,
                      ""
                );
                
           }
           
           if(this.getStartDate()==null 
                   && this.getEndDate()==null
                   && this.getSpecialist().getId()==null
                   && !this.getFiltersData()[0].isEmpty()){
                   
                this.params.put("numberInvoices", this.getFiltersData()[0]);
                this.params.put("specialistName", 
                this.getPaymentReportkeysAnalisysByCriterialOuputList().get(0).getCompleteNameSpecialists()
                        );
                this.params.put("specialistCEDRIF", 
                this.getPaymentReportkeysAnalisysByCriterialOuputList().get(0).getIdentityNumberRIFSpecialistsClinic()   
                   );
                   
                loadReports();
                                
                this.export.xlsx(
                      jasperPrint,
                      "ReporteClavesPorFacturaNro"+this.getFiltersData()[0],
                      ""
                );
           }
           
           if(this.getStartDate()==null && this.getEndDate()==null
                       && this.getSpecialist()!=null
                       && this.getFiltersData()[0].isEmpty()){
                   
                this.params.put("specialistName", 
                this.getPaymentReportkeysAnalisysByCriterialOuputList().get(0).getCompleteNameSpecialists()
                        );
                this.params.put("specialistCEDRIF", 
                this.getPaymentReportkeysAnalisysByCriterialOuputList().get(0).getIdentityNumberRIFSpecialistsClinic()   
                   );
                   
                loadReports();
                                
                this.export.xlsx(
                      jasperPrint,
                      "ReporteClavesPorEspecialista"+this.getSpecialist().getCompleteName().toUpperCase().replace(" ","-"),
                      ""
                );
           }else{
              FacesContext.getCurrentInstance().addMessage(null, 
                 new FacesMessage(FacesMessage.SEVERITY_WARN, 
                 "Falta seleccionar un criterio de busqueda", 
                 "Falta seleccionar un criterio de busqueda"
                 )
              );
                   
           }
         }else{
             FacesContext.getCurrentInstance().addMessage(null, 
                new FacesMessage(FacesMessage.SEVERITY_WARN, 
                "Error al generar el reporte", 
                "Error al generar el reporte"
                 )
             );
         }
        } catch (Exception e) {
            System.out.println(e);
        }
        RequestContext.getCurrentInstance().update("form:growInvoicesReportMessage");
    }
    
   
    
    
    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        
        if(this.getSpecialist()!=null){ this.getSpecialist().setCompleteName("");}
        RequestContext.getCurrentInstance().update("form:txtIdentity");
        List<Specialists> filteredSpecialists = this.serviceSpecialist.findByIdentityOrName(query);
        return filteredSpecialists;
    }
    
    /**
     * Method called when I select a specialist from the list
     *
     * @param event *
     */
    public void onItemSelect(SelectEvent event) {
        this.setSpecialist(new Specialists());
        this.setSpecialist((Specialists) event.getObject());  
       
        if(this.getSpecialist()!=null){
            verifyKeysSpecialists();
        }
      
    }
    
    public void verifyKeysSpecialists(){
        
        if(this.getStartDate()!=null && this.getEndDate()!=null
               && this.getSpecialist()!=null){
           this.setActiveBtnSearch(false);
           this.setActiveBtnReset(false);
           this.setActiveNumberRequest(true);
        }else{
           
            if(this.getStartDate()==null && this.getEndDate()==null
                && this.getSpecialist()==null){
                
                this.setActiveBtnSearch(true);
                this.setActiveBtnReset(true);
                this.setActiveNumberRequest(true);
            }
            
            if(this.getStartDate()==null && this.getEndDate()==null
                && this.getSpecialist()!=null){
                
                
                this.setActiveBtnSearch(false);
                this.setActiveBtnReset(false);
                this.setActiveNumberRequest(true);
                
            }
        }
        RequestContext.getCurrentInstance().update("form:txtNumberInvoice");
        RequestContext.getCurrentInstance().update("form:btnSearchData");
        RequestContext.getCurrentInstance().update("form:btnResetData");
    }
    /**
     *  Methods active buttons
     * 
     * 
     */
    public void activeBtnButtonsFindDataCriterial1(){
        
        if(this.getStartDate()!=null 
                && this.getEndDate()!=null
                && this.getSpecialist()!=null){
            
            this.setActiveBtnSearch(false);
            this.setActiveBtnReset(false);
            this.setActiveBtnExportData(true);
        }else{
            this.setActiveBtnSearch(true);
            this.setActiveBtnReset(true);
            this.setActiveBtnExportData(true);
        }
        
        RequestContext.getCurrentInstance().update("form:btnSearchData");
        RequestContext.getCurrentInstance().update("form:btnResetData");
        RequestContext.getCurrentInstance().update("form:btnExportData");
    }
    
    /**
     * Method at converter dateUtil and dateSql
     * 
     * @param date
     * @return 
     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        if(date!=null){
            return new java.sql.Date(date.getTime());
        }else{
            return null;
        }        
    }
    
     /**
     * Method to set Date limit, range: [Year-1,Year]
     */
    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int minYear = year;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        // this.setCurrentDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
    }
    
 
}
