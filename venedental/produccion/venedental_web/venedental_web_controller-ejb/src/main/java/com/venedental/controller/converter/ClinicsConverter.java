/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.ClinicsViewBean;
import com.venedental.model.Clinics;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import services.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "clinicsConverter", forClass = Clinics.class)
public class ClinicsConverter implements Converter {

    private static final Logger log = CustomLogger.getGeneralLogger(ClinicsConverter.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        ClinicsViewBean clinicsViewBean = (ClinicsViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "clinicsViewBean");
        
        if (value.trim().equals("") || value.equals("null")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Clinics s : clinicsViewBean.getListaClinics()) { 
                    if (s.getId() == numero) {
                        return s;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Clínica invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Clinics) value).getId());
        }

    }

}
