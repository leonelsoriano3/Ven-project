/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.ejb;

import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.model.Insurances;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author usuario
 */
@Local
public interface MassiveLoadingLocal {

    public boolean execute(InsuranceLoad insuranceLoadingConfiguration, 
            List<String> emails,Boolean disassociate,Insurances insurance,
            Boolean include, Boolean exclude,String modeLoad,List<FindPlanByIdOutput> planList);

}
