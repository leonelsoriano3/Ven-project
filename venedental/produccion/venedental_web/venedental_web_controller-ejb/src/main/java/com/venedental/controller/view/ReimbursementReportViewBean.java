package com.venedental.controller.view;

import com.venedental.constans.ConstansPaymentRefundReport;
import com.venedental.controller.BaseBean;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.Insurances;
import com.venedental.model.Reimbursement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;

@ManagedBean
@ViewScoped
public class ReimbursementReportViewBean extends BaseBean {

    /* EJBs */
    @EJB
    private ReimbursementFacadeLocal reimbursementaFacadeLocal;

    @EJB
    private InsurancesFacadeLocal insurancesFacadeLocal;

    @EJB
    private MailSenderLocal mailSenderLocal;

    /* ManagedBeans Property */
    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;

    /* Attributes */
    // --> Jaspert Report
    private Map<String, Object> params = new HashMap<String, Object>();
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private JasperPrint jasperPrint;
    private String reportPath;
    private String logoPath;
    private HttpServletResponse httpServletResponse;
    private ServletOutputStream servletOutputStream;
    private JRXlsxExporter docxExporter;

    // --> Reimbursement
    private List<Reimbursement> reimbursermentReportList;

    // --> Insurances
    private List<Insurances> InsurancesList;
    private Integer insurancesSelected;

    // --> Date
    private Date startDate, endDate, maxDate;

    // --> OTHERS
    private Boolean btnActiveEndDate, btnActiveInsurances, showTable, btnSearchData, btnExportData, btnSendMail;

    private Boolean btnRestore;

    /* Constructs */
    public ReimbursementReportViewBean() {

    }

    @PostConstruct
    public void init() {
        this.cleanAndDate();
        this.setBtnRestore(true);
    }

    /* Getter and Setters */
    public Boolean getBtnSendMail() {
        return btnSendMail;
    }

    public void setBtnSendMail(Boolean btnSendMail) {
        this.btnSendMail = btnSendMail;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public List<Insurances> getInsurancesList() {
        return InsurancesList;
    }

    public void setInsurancesList(List<Insurances> InsurancesList) {
        this.InsurancesList = InsurancesList;
    }

    public Integer getInsurancesSelected() {
        return insurancesSelected;
    }

    public void setInsurancesSelected(Integer insurancesSelected) {
        this.insurancesSelected = insurancesSelected;
    }

    public Boolean getBtnActiveEndDate() {
        return btnActiveEndDate;
    }

    public void setBtnActiveEndDate(Boolean btnActiveEndDate) {
        this.btnActiveEndDate = btnActiveEndDate;
    }

    public Boolean getBtnActiveInsurances() {
        return btnActiveInsurances;
    }

    public void setBtnActiveInsurances(Boolean btnActiveInsurances) {
        this.btnActiveInsurances = btnActiveInsurances;
    }

    public Boolean getBtnSearchData() {
        return btnSearchData;
    }

    public void setBtnSearchData(Boolean btnSearchData) {
        this.btnSearchData = btnSearchData;
    }

    public Boolean getBtnExportData() {
        return btnExportData;
    }

    public void setBtnExportData(Boolean btnExportData) {
        this.btnExportData = btnExportData;
    }

    public List<Reimbursement> getReimbursermentReportList() {
        return reimbursermentReportList;
    }

    public void setReimbursermentReportList(List<Reimbursement> reimbursermentReportList) {
        this.reimbursermentReportList = reimbursermentReportList;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public JRBeanCollectionDataSource getBeanCollectionDataSource() {
        return beanCollectionDataSource;
    }

    public void setBeanCollectionDataSource(JRBeanCollectionDataSource beanCollectionDataSource) {
        this.beanCollectionDataSource = beanCollectionDataSource;
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    public void setHttpServletResponse(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    public ServletOutputStream getServletOutputStream() {
        return servletOutputStream;
    }

    public void setServletOutputStream(ServletOutputStream servletOutputStream) {
        this.servletOutputStream = servletOutputStream;
    }

    public JRXlsxExporter getDocxExporter() {
        return docxExporter;
    }

    public void setDocxExporter(JRXlsxExporter docxExporter) {
        this.docxExporter = docxExporter;
    }

    public Boolean getShowTable() {
        return showTable;
    }

    public void setShowTable(Boolean showTable) {
        this.showTable = showTable;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    /* Methods */
    /**
     * Methods fields list to Insurances
     *
     */
    public void findAllInsurances() {

        this.setInsurancesList(this.insurancesFacadeLocal.findAllOrderByName());

    }

    /**
     * Methods list all to reimbursement by reject
     *
     */
    public void searchReimbursementReportsbystatusReject() {

        try {

            if (this.getInsurancesSelected() != null && this.getInsurancesSelected() != 0) {

                this.setReimbursermentReportList(this.reimbursementaFacadeLocal.searchReimbursementReportsbyStatus(this.getInsurancesSelected(), StatusReimbursementEnum.RECHAZADO.getValor(), this.getStartDate(), this.getEndDate()));

                if (!this.getReimbursermentReportList().isEmpty()) {
                    this.setBtnExportData(false);
                } else {

                    this.setBtnExportData(true);
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe seleccionar una aseguradora"));

                this.setBtnExportData(true);
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Error!", e.getMessage()));
        }

        RequestContext.getCurrentInstance().update("form:growReportReimbursement");
        RequestContext.getCurrentInstance().update("form:tableReimbursementReport");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnResetKey");

    }

    /**
     * Methods list all to reimbursement by reject
     *
     */
    public void searchReimbursementReportsbystatusSubcriber() {

        try {

            if (this.getInsurancesSelected() != null && this.getInsurancesSelected() != 0) {

                this.setReimbursermentReportList(this.reimbursementaFacadeLocal.searchReimbursementReportsbyStatus(this.getInsurancesSelected(), StatusReimbursementEnum.ABONADO.getValor(), this.getStartDate(), this.getEndDate()));
                if (!this.getReimbursermentReportList().isEmpty()) {
                    this.setBtnExportData(false);
                    this.setBtnSendMail(false);
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe seleccionar una aseguradora"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Debe seleccionar una aseguradora"));

            this.setBtnExportData(true);
            this.setBtnSendMail(true);
        }

        RequestContext.getCurrentInstance().update("form:growReportReimbursement");
        RequestContext.getCurrentInstance().update("form:tableReimbursementReport");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:btnSendMail");

    }

    public void onChangeInsurance(String typeReimbursement) {

        Integer statusReimbursement = 0;

        this.setBtnExportData(true);
        this.setBtnSendMail(true);
        this.setBtnSearchData(false);
        this.setReimbursermentReportList(new ArrayList<Reimbursement>());

        //validacion de la pagina origen del reembolso
        switch (typeReimbursement) {
            case "ReimbursementByStatusReject":
                statusReimbursement = StatusReimbursementEnum.RECHAZADO.getValor();
                break;

            case "ReimbursementByStatusSubscriber":
                statusReimbursement = StatusReimbursementEnum.ABONADO.getValor();
                break;
        }

        try {
            if (this.getInsurancesSelected() != null && this.getInsurancesSelected() != 0) {

                if (!this.reimbursementaFacadeLocal.searchReimbursementReportsbyStatus(this.getInsurancesSelected(), statusReimbursement, this.getStartDate(), this.getEndDate()).isEmpty()) {
                    this.btnSearchData = false;
                } else {
                    this.btnSearchData = true;
                }
            } else {
                this.btnSearchData = true;
            }
        } catch (Exception e) {
            this.btnSearchData = true;
        }

        RequestContext.getCurrentInstance().update("form:btnSendMail");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:tableReimbursementReport");

    }

    /**
     * Method to active component Select cmbInsurances
     *
     */
    public void activecmbInsurances() {

        if (this.getStartDate() != null && this.getEndDate() != null) {

            this.setBtnActiveInsurances(false);
            this.setBtnSearchData(true);
            this.setBtnExportData(true);

            findAllInsurances();
        } else {
            this.setBtnActiveInsurances(true);
            this.setBtnSearchData(true);
            this.setBtnExportData(true);
        }

        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:cmbInsurance");
    }

    /**
     * Method to active component DatePicker EndDate
     *
     */
    public void activetxtDateEnd() {

        if (this.getStartDate() != null) {
            this.setBtnActiveEndDate(false);
            this.setBtnRestore(false);
        } else {
            this.setBtnActiveEndDate(true);
            this.setBtnRestore(true);
        }

        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:endDate");
    }

    public void cleanAndDate() {

        // Clear CMB Insurances
        this.setInsurancesList(new ArrayList<Insurances>());

        // Clear Table
        this.setReimbursermentReportList(new ArrayList<Reimbursement>());

        // Clear Value Insurances
        this.setInsurancesSelected(0);

        // Setter NUll and Date
        this.setStartDate(null);
        this.setEndDate(null);
        this.setMaxDate(null);

        // Buttons
        this.setBtnActiveEndDate(true);
        this.setBtnActiveInsurances(true);
        this.setBtnSearchData(true);
        this.setBtnExportData(true);
        this.setBtnSendMail(true);
        this.setBtnRestore(true);

        // Request Context
        RequestContext.getCurrentInstance().update("form:cmbInsurance");
        RequestContext.getCurrentInstance().update("form:startDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:tableReimbursementReport");
        RequestContext.getCurrentInstance().update("form:btnResetKey");
    }

    /**
     * --------------------------------------------------------------------- SEND REPORT BY REIMBURSEMENT AND STATUS
     * REJECT ---------------------------------------------------------------------
     */
    // --> Component Load Reports
    public void loadReports() throws IOException, JRException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("reimbursementRejectList", new JRBeanCollectionDataSource(reimbursermentReportList));
        this.params.put("heal_logo2", logoPath);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(reimbursermentReportList);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportReimbursementByStatusReject.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    // --> Send Report
    public void sendReportXLS(ActionEvent actionEvent) throws JRException, IOException, Exception {

        try {
            if (!this.getReimbursermentReportList().isEmpty()) {

                if (this.getInsurancesSelected() != null
                        && this.getInsurancesSelected() != 0) {

                    Insurances searchInsurance = this.insurancesFacadeLocal.find(
                            this.getInsurancesSelected()
                    );

                    // Date
                    SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
                    sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

                    String dateEndExport = sdfSolicitud.format(this.getEndDate());

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String auxStarDate = sdf.format(new Date());
                    String replaceDate = auxStarDate.replaceAll("/", "-");

                    this.params.put("startDate", this.getStartDate());
                    this.params.put("endDate", this.getEndDate());
                    // Parameters Insurances
                    this.params.put("insurance", searchInsurance.getName().toUpperCase());

                    loadReports();
                    this.export.xlsxReimbursement(jasperPrint, searchInsurance.getName().toUpperCase(), replaceDate.trim());
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Problemas al generar el Reporte!"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Error!", e.getMessage()));
        }
        RequestContext.getCurrentInstance().update("form:growReportReimbursement");

    }

    // --> Component Load Reports
    public void loadReportsSubscriber() throws IOException, JRException {
        Insurances searchInsurance = this.insurancesFacadeLocal.find(this.getInsurancesSelected());
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("reimbursermentReportList", new JRBeanCollectionDataSource(getReimbursermentReportList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("startDate", this.getStartDate());
        this.params.put("endDate", this.getEndDate());
        this.params.put("heal_logo2", logoPath);
        this.params.put("insurance", searchInsurance.getName());
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(getReimbursermentReportList());
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportReimbursementByStatusSubscriber.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    // --> Send Report
    public void sendReportXLSSubscriber(ActionEvent actionEvent) throws JRException, IOException, Exception {

        try {
            if (!this.getReimbursermentReportList().isEmpty()) {

                Insurances searchInsurance = this.insurancesFacadeLocal.find(
                        this.getInsurancesSelected()
                );
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String auxStarDate = sdf.format(new Date());
                String replaceDate = auxStarDate.replaceAll("/", "-");
                loadReportsSubscriber();

                this.export.xlsxReimbursementSuscriber(jasperPrint, searchInsurance.getName().toUpperCase(), replaceDate);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Problemas al generar el Reporte!"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Error!", e.getMessage()));
        }
        RequestContext.getCurrentInstance().update("form:btnExportAuditKeys");
    }

    // --> Send Report with file excel
    public void sendReportFileXLSSubscriber(ActionEvent actionEvent) throws JRException, IOException, Exception {

        try {
            if (!this.getReimbursermentReportList().isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String auxStarDate = sdf.format(new Date());
                System.out.println("Fecha " + auxStarDate);
                String replaceDate = auxStarDate.replaceAll("/", "-");
                System.out.println("Fecha " + replaceDate);
                String numberReport = replaceDate + "ReporteReembolsoAbonado";
                loadReportsSubscriber();

                this.export.xlsx(jasperPrint, numberReport, Transformar.getThisDate());
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Problemas al generar el Reporte!"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal Error!", e.getMessage()));
        }
    }

    /**
     * Method to read the properties file constants payment Refund Report
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansPaymentRefundReport.FILENAME_PAYMENT_REPORT);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansPaymentRefundReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * Method to send an email with change status of reimbursement
     *
     * @param files
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailInsurance(List<File> files, Insurances searchInsurance) throws ParseException, IOException {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos a aseguradora: {0}");

        // Date
        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

        String dateStarExport = sdfSolicitud.format(this.getStartDate());
        String dateEndExport = sdfSolicitud.format(this.getEndDate());

        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

        String title = readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_MESSAGE_TITLE2);
        String subjectMessage = "Relación de solicitud de reembolsos abonadas desde " + dateStarExport + " al " + dateEndExport;
        String contentMessage = buildMessageInsurance(searchInsurance);
        this.mailSenderLocal.send(Email.MAIL_REEMBOLSO, subjectMessage, title, contentMessage, files, searchInsurance.getEmail());

        log.log(Level.INFO, "Fin - Proceso de envio de correos Aseguradora");

    }

    /**
     * Method to construct the message body for the email that will be sent to patient
     *
     * @param patient
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageInsurance(Insurances searchInsurance) throws ParseException, IOException {

        // Date
        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));

        String dateStarExport = sdfSolicitud.format(this.getStartDate());
        dateStarExport = dateStarExport.replaceAll(",", "");
        String dateEndExport = sdfSolicitud.format(this.getEndDate());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Buenas tardes Sr(es). ").append(searchInsurance.getName()).append(",").append("<br><br>");
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_EMAIL_CONTENT));
        stringBuilder.append(dateStarExport).append(" al ").append(dateEndExport).append(".")
                .append(readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_EMAIL_CONTENT_WITH_ANNEX_TWO));
        stringBuilder.append("<br><br>").append("Departamento de Reembolsos").append("<br>");
        stringBuilder.append("<strong>Grupo Veneden</strong>");
        return stringBuilder.toString();
    }

    public void generatePaymentReport() throws IOException {

        try {
            Insurances searchInsurance = this.insurancesFacadeLocal.find(this.getInsurancesSelected());

            if (searchInsurance.getEmail() == null || searchInsurance.getEmail().length() <= 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Ocurrió un error en el envío de correo. La aseguradora no posee correo electrónico registrado. Por favor, verifique"));
                RequestContext.getCurrentInstance().update("form:growReportReimbursement");
            } else {
                this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String auxStarDate = sdf.format(new Date());

                String replaceInsurances = searchInsurance.getName().replaceAll(" ", "-");
                String replaceDate = auxStarDate.replaceAll("/", "-");

                String nameReport = replaceDate + "ReporteReembolsoAbonados" + replaceInsurances.toUpperCase() + ".pdf";
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("reimbursermentReportList", new JRBeanCollectionDataSource(getReimbursermentReportList()));
                params.put("heal_logo2", logoPath);
                params.put("startDate", getStartDate());
                params.put("endDate", getEndDate());
                params.put("insurance", searchInsurance.getName());
                this.beanCollectionDataSource = new JRBeanCollectionDataSource(getReimbursermentReportList());
                this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportReimbursementByStatusSubscriberPDF.jasper");
                buildReport(nameReport, getReimbursermentReportList(), params, reportPath);

                sendMailInsurance(findFile(nameReport), searchInsurance);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));
                RequestContext.getCurrentInstance().update("form:growReportReimbursement");
            }

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * Method to build the report jasper
     *
     * @param nameReport
     * @param reimbursement
     * @param params
     * @param jasperPath
     * @throws IOException
     */
    public void buildReport(String nameReport, List<Reimbursement> reimbursement, Map<String, Object> params, String jasperPath) throws IOException {
        try {
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(reimbursement);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperPath, params, beanCollectionDataSource);
            String reportPath = "/home/ftp/reimbursement/";//readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + nameReport);

        } catch (JRException ex) {
            Logger.getLogger(ReimbursementRequeriment.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Method to seek payment report
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFile(String nameReport) throws IOException {

        try {

            List<File> files = new ArrayList<File>();
            String reportPath = "/home/ftp/reimbursement/";//readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
            String path = reportPath + nameReport;
            File file = new File(path);
            files.add(file);
            return files;
        } catch (Exception e) {
            System.out.println("Error a retorno del documento");
        }
        return null;

    }

    public Boolean getBtnRestore() {
        return btnRestore;
    }

    public void setBtnRestore(Boolean btnRestore) {
        this.btnRestore = btnRestore;
    }

}
