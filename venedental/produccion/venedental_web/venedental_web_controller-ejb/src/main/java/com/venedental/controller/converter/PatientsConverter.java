/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.ReimbursementRequeriment;
import com.venedental.model.Patients;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Anthony Torres
 */
@FacesConverter(value = "patientsConverter", forClass = Patients.class)
public class PatientsConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        
        ReimbursementRequeriment reimbursementRequeriment = (ReimbursementRequeriment) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "reimbursementRequeriment");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Patients s : reimbursementRequeriment.getPatientsBeneficiary()) {
                    if (s.getId() == numero) {
                        return s;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Paciente no válido"));
            } 
        }
        return null;
        
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Patients) value).getId());
        }

    }
    
}
