/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.ejb;

import com.venedental.massiveLoading.base.Constants;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.LoaderInsurance;
import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.Insurances;
import com.venedental.services.ServiceMassiveLoading;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author usuario
 */
@Stateless
public class MassiveLoading implements MassiveLoadingLocal {

    private static final Logger log = CustomLogger.getLogger(MassiveLoading.class.getName());

    @EJB
    private ServiceMassiveLoading serviceMassiveLoading;
    
    @EJB
    private MailSenderLocal mailSenderLocal;
    
    private boolean loading = false;

    @Override
    public boolean execute(final InsuranceLoad insuranceLoadingConfiguration, final List<String> emails,
            final Boolean disassociate, final Insurances insurance, 
            final Boolean include, final Boolean exclude, final String modeLoad,final List<FindPlanByIdOutput> planList) {
        if (!loading) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    execution(insuranceLoadingConfiguration, emails,disassociate, insurance, include, exclude,modeLoad, planList);
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            return true;
        }
        return false;
    }
    
    private void execution(InsuranceLoad insuranceLoadingConfiguration, List<String> emails,
            Boolean  disassociate, Insurances insurance,Boolean include, Boolean exclude, String modeLoad,List<FindPlanByIdOutput> planList) {
        if (!loading) {
            loading = true;
            log.log(Level.INFO, "========================================================");
            SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_COMPLETE);
            log.log(Level.INFO, "{0} - INICIANDO PROCESO DE CARGA MASIVA", format.format(new Date()));
            log.log(Level.INFO, "INICIO CARGA MASIVA - ASEGURADORA <{0}>", insuranceLoadingConfiguration.getInsurance().getName());
            LoaderInsurance ail = new LoaderInsurance(serviceMassiveLoading, mailSenderLocal);
            ail.load(insuranceLoadingConfiguration, emails,disassociate,insurance, include, exclude,modeLoad,planList);
            log.log(Level.INFO, "FIN CARGA MASIVA - ASEGURADORA <{0}>", insuranceLoadingConfiguration.getInsurance().getName());
            log.log(Level.INFO, "{0} - FINALIZANDO PROCESO DE CARGA MASIVA", format.format(new Date()));
            log.log(Level.INFO, "========================================================");
            loading = false;
        }
    }

}
