/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.massiveLoading.ejb.MassiveLoadingLocal;
import com.venedental.controller.BaseBean;
import com.venedental.dto.massiveLoading.FileLoad;
import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.dto.parameters.FindParametersOutput;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.enums.CargaMasivaParameters;
import com.venedental.massiveLoading.base.Constants;
import com.venedental.massiveLoading.base.enums.EFields;
import com.venedental.massiveLoading.base.enums.OptionLoad;
import com.venedental.massiveLoading.dto.ExcelFieldDTO;
import com.venedental.model.Insurances;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceInsurance;
import com.venedental.services.ServiceParameters;
import com.venedental.services.ServicePlans;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import controller.utils.CustomLogger;
import controller.utils.PropertiesConfiguration;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import org.primefaces.util.ComponentUtils;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "massiveLoadingViewBean")
@ViewScoped
public class MassiveLoadingViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;
    private static final Logger log = CustomLogger.getLogger(MassiveLoadingViewBean.class.getName());

    @EJB
    private ServiceInsurance serviceInsurance;
    
    @EJB
    private ServicePlans servicePlans;

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;
    
    @EJB
    private ServiceParameters serviceParameters;

    @EJB
    private MassiveLoadingLocal massiveLoading;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    private List<Insurances> insurances;
    private Insurances selectedInsurance;

    private List<String> files;
    private String selectedFile;

    private List<ExcelFieldDTO> excelFields;
    private List<String> letters;
    public List<ExcelFieldDTO> listExcelConfigured;

    private InsuranceLoad insuranceLoad;

    private boolean disableFilesFields;
    private boolean disableFilesFieldsColumns;
    private boolean disableFilesButton;
    private boolean disableProcessButton;
    private boolean disableCmbInsurance;
    private boolean disableSelectedCmbInsurance;

    private FileLoad fileLoad;

    private Boolean selectBoolean;
    private  Boolean  selectBooleanCompletLoad;
    private  Boolean selectBooleanInclude;
    private  Boolean  selectBooleanExclude;
    private String  selectString;
    private String  selectStringCompletLoad;
    private String  selectStringInclude;
    private String  selectStringExclude;
    private String messageConfirm;
    

    public MassiveLoadingViewBean() {
        disableFilesFields = true;
        disableFilesFieldsColumns = true;
        disableProcessButton = true;
        this.disableCmbInsurance = false;
        this.disableFilesButton=true;     
       
        
    }

    @PostConstruct
    public void init() {
        insurances = filterInsurances();
        letters = Arrays.asList("A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".split(","));
        excelFields = loadSelectOneMenuLetters();
        this.modeLoadDefault();
        
         
        
    }

    public Boolean getSelectBooleanCompletLoad() {
        return selectBooleanCompletLoad;
    }

    public void setSelectBooleanCompletLoad(Boolean selectBooleanCompletLoad) {
        this.selectBooleanCompletLoad = selectBooleanCompletLoad;
    }

    public Boolean getSelectBooleanInclude() {
        return selectBooleanInclude;
    }

    public void setSelectBooleanInclude(Boolean selectBooleanInclude) {
        this.selectBooleanInclude = selectBooleanInclude;
    }

    public Boolean getSelectBooleanExclude() {
        return selectBooleanExclude;
    }

    public void setSelectBooleanExclude(Boolean selectBooleanExclude) {
        this.selectBooleanExclude = selectBooleanExclude;
    }
    
    public String getSelectString() {
        return selectString;
    }

    public void setSelectString(String selectString) {
        this.selectString = selectString;
    }


    public boolean isDisableFilesButton() {
        return disableFilesButton;
    }

    public void setDisableFilesButton(boolean disableFilesButton) {
        this.disableFilesButton = disableFilesButton;
    }

    public boolean isDisableSelectedCmbInsurance() {
        return disableSelectedCmbInsurance;
    }

    public void setDisableSelectedCmbInsurance(boolean disableSelectedCmbInsurance) {
        this.disableSelectedCmbInsurance = disableSelectedCmbInsurance;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public boolean isDisableCmbInsurance() {
        return disableCmbInsurance;
    }

    public void setDisableCmbInsurance(boolean disableCmbInsurance) {
        this.disableCmbInsurance = disableCmbInsurance;
    }

    public FileLoad getFileLoad() {
        return fileLoad;
    }

    public void setFileLoad(FileLoad fileLoad) {
        this.fileLoad = fileLoad;

        this.listExcelField(fileLoad);
    }

    public boolean getSelectBoolean() {
        return selectBoolean;
    }

    public void setSelectBoolean(boolean selectBoolean) {
        this.selectBoolean = selectBoolean;
    }

    public String getSelectStringCompletLoad() {
        return selectStringCompletLoad;
    }

    public void setSelectStringCompletLoad(String selectStringCompletLoad) {
        this.selectStringCompletLoad = selectStringCompletLoad;
    }

    public String getSelectStringInclude() {
        return selectStringInclude;
    }

    public void setSelectStringInclude(String selectStringInclude) {
        this.selectStringInclude = selectStringInclude;
    }

    public String getSelectStringExclude() {
        return selectStringExclude;
    }

    public void setSelectStringExclude(String selectStringExclude) {
        this.selectStringExclude = selectStringExclude;
    }
    
    
    public void modeLoadDefault(){
    
        this.messageConfirm="¿Desea continuar con esta operación?";
        this.selectBoolean = false;
        this.setSelectString(OptionLoad.Inclusiones.getName());
        this.setSelectStringCompletLoad(OptionLoad.CargaCompleta.getName());
        this.setSelectStringInclude(OptionLoad.Inclusiones.getName());
        this.setSelectStringExclude(OptionLoad.Exclusiones.getName());
        this.setSelectBooleanCompletLoad(false);
        this.setSelectBooleanInclude(false);
        this.setSelectBooleanExclude(false);
    
    }
    

    public List<Insurances> filterInsurances() {
        List<Insurances> filteredList = new ArrayList<>();
        String pathUpload = PropertiesConfiguration.get(Constants.PROPERTY_PATH_FTP);
        List<Insurances> list = new ArrayList<Insurances>();
        list = this.serviceInsurance.findInsurances();

        // ELIMIBAR HASTA AQUI
        for (Insurances i : list) {
            boolean show = false;
            File fileFolder = new File(pathUpload + i.getFolder());
            if (fileFolder.exists() && fileFolder.isDirectory() && (fileFolder.list().length > 1 || fileFolder.list().length == 1)) {
                for (String file : fileFolder.list()) {
                    if (!file.equals(Constants.PROCESSED) && isExcel(file)) {
                        show = true;
                        break;
                    }
                }
            }
            if (show) {
                filteredList.add(i);
            }
        }
        if(filteredList.isEmpty()){
            
            this.setDisableSelectedCmbInsurance(true);
            this.setDisableCmbInsurance(true);
            this.setDisableFilesFields(true);
            RequestContext requestContext = RequestContext.getCurrentInstance();
            requestContext.update("form:pListInsurances");
            requestContext.update("form:listInsurances");
            requestContext.update("form:cmbInsurance");
            requestContext.update("form");
        
        }
        
        return filteredList;
    }

    private List<ExcelFieldDTO> loadSelectOneMenuLetters() {
        List<ExcelFieldDTO> list = new ArrayList<>();
        EFields[] efields = EFields.values();
        for (int i = 0; i < efields.length; i++) {
            list.add(new ExcelFieldDTO(efields[i], letters.get(i)));
        }
        return list;
    }

    public void cleanFields() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        disableFilesFields = true;
        disableFilesFieldsColumns = true;
        this.disableFilesButton=true;
        disableProcessButton = true;
        this.setDisableSelectedCmbInsurance(false);
        this.setDisableCmbInsurance(false);
        selectedInsurance = null;
        selectedFile = null;
        excelFields = loadSelectOneMenuLetters();
        this.setFiles(new ArrayList<String>());
        insuranceLoad = null;
        this.setSelectBoolean(false);
        this.filterInsurances();
        
        requestContext.update("form:pListInsurances");
        requestContext.update("form:fileFields");
        requestContext.update("form:addButton");
        requestContext.update("form:cmbInsurance");
        requestContext.update("cleanButton");
        requestContext.update("form:processButton");
        requestContext.update("form:detailLoading");
        requestContext.update("detailLoading");
        requestContext.update("form:tableExcel");
        requestContext.update("form:tableExcel:columnExcel");
        requestContext.update("form:columnExcel");
        requestContext.update("form:listInsurances");
        requestContext.update("form:growMassiveLoading");  
        requestContext.update("form");
    }

    public void selectInsurance() {
        System.out.println("selectInsurance");
        if (selectedInsurance != null) {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            disableFilesFields = false;
            this.disableCmbInsurance = true;
            excelFields = loadSelectOneMenuLetters();
            this.setFiles(new ArrayList<String>());
            insuranceLoad = new InsuranceLoad(selectedInsurance, PropertiesConfiguration.get(Constants.PROPERTY_PATH_FTP));
            files = findAvailableFiles();
            requestContext.update("form");
        }
    }

    public void selectFile() {
        System.out.println("selectFile");

        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedFile != null) {
             excelFields = loadSelectOneMenuLetters();
            disableFilesFields = false;
             disableFilesFieldsColumns = false;
            this.setDisableFilesButton(false);
            requestContext.update("form:addButton");
            requestContext.update("form:tableExcel");
            requestContext.update("form:tableExcel:columnExcel");
            requestContext.update("form");
            

        }
    }

    public void addFile() {
        System.out.println("selectFile");

        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext context = FacesContext.getCurrentInstance();
        disableSelectedCmbInsurance=true;
        this.setDisableFilesButton(true);
        requestContext.update("form:addButton");
        requestContext.update("form:tableExcel");
        requestContext.update("form:cmbInsurance");
        requestContext.update("form:pListInsurances");

    }

    private List<String> findAvailableFiles() {
        List<String> list = new ArrayList<>();
        for (String filename : insuranceLoad.getFolder().list()) {
            if (!filename.equals(Constants.PROCESSED) && isExcel(filename) && !fileAdded(filename)) {
                list.add(filename);
            }
        }
        return list;
    }

    private boolean isExcel(String name) {
        return name.endsWith(Constants.EXTENSION_XLS) || name.endsWith(Constants.EXTENSION_XLSX);
    }

    private boolean fileAdded(String filename) {
        for (FileLoad fileLoad : insuranceLoad.getFiles()) {
            if (fileLoad.getFileName().equals(filename)) {
                return true;
            }
        }
        return false;
    }

    public void addFileConfiguration() {
        System.out.println("addFileConfiguration");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedFile == null) {
            context.addMessage(null, new FacesMessage("Error", "Debe seleccionar un archivo de la lista"));
            requestContext.update("form:growMassiveLoading");
        } else if (!validateExcelColumns()) {
            
            context.addMessage(null, new FacesMessage("Error", "Verificar que cada valor de la columna Excel sea diferente por cada nombre de campo obligatorio"));
            requestContext.update("form:growMassiveLoading");
        } else {
            if (fileAdded(selectedFile)) {
                
                context.addMessage(null, new FacesMessage("Error", "El archivo seleccionado ya ha sido cargado"));
                requestContext.update("form:growMassiveLoading");
            } else {
                disableProcessButton = false;
                this.setDisableSelectedCmbInsurance(true);
                this.setDisableCmbInsurance(true);
                FileLoad newFileLoad = new FileLoad(selectedFile, calculateColumns());
                insuranceLoad.addFileLoad(newFileLoad);
               
                files = findAvailableFiles();
                this.addFile();    
            }
             excelFields = loadSelectOneMenuLetters();
             disableFilesFieldsColumns = true;
             this.filterInsurances() ;
             requestContext.update("form:tableExcel");
            requestContext.update("form:tableExcel:columnExcel");
             
             requestContext.update("form:growMassiveLoading");
        }
        
    }

    private boolean validateExcelColumns() {
        for (int i = 0; i < excelFields.size() - 1; i++) {
            for (int j = i + 1; j < excelFields.size(); j++) {
                if (excelFields.get(i).getLetter().equals(excelFields.get(j).getLetter())) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<Integer> calculateColumns() {
        List<Integer> list = new ArrayList<>();
        for (ExcelFieldDTO dto : excelFields) {
            list.add(letters.indexOf(dto.getLetter()));
        }
        return list;
    }
    
    public void messageConfirm() {

         RequestContext requestContext = RequestContext.getCurrentInstance();
        
        // String pconfirm = ComponentUtils.findComponentClientId("messageConfirm");
        if (this.getSelectString().equals(OptionLoad.CargaCompleta.getName())) {
            this.setMessageConfirm("¿Desea continuar con esta operación?");
        } else if (this.getSelectString().equals(OptionLoad.Inclusiones.getName())) {
            this.setMessageConfirm("¿Desea continuar con la operación de sólo inclusiones de pacientes?");
        } else if (this.getSelectString().equals(OptionLoad.Exclusiones.getName())) {

            this.setMessageConfirm("¿Desea continuar con la operación de sólo exclusiones de pacientes?");
        }
         requestContext.update("form:messageConfirmYes");
 
    }
    
    public void modeLoad() {

        if (this.getSelectString().equals(OptionLoad.CargaCompleta.getName())) {
            this.setSelectBooleanCompletLoad(true);
        } else if (this.getSelectString().equals(OptionLoad.Inclusiones.getName())) {
            this.setSelectBooleanInclude(true);
        } else if (this.getSelectString().equals(OptionLoad.Exclusiones.getName())) {
            this.setSelectBooleanExclude(true);

        }
    }

    public void process() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext context = FacesContext.getCurrentInstance();
        List<String> emails = emailsUser();
        
        this.modeLoad();
        
        List<FindPlanByIdOutput> planList = new ArrayList<FindPlanByIdOutput>();
        planList = this.servicePlans.findPlans(this.getSelectedInsurance().getId());
        if (massiveLoading.execute(insuranceLoad, emails, this.getSelectBoolean(), this.getSelectedInsurance(),
                this.getSelectBooleanInclude(), this.getSelectBooleanExclude(),this.getSelectString(),planList )) {
            context.addMessage(null, new FacesMessage("Éxito", "“Carga masiva registrada con éxito”."));
            this.cleanFields();
            this.modeLoadDefault();
            requestContext.update("form:growMassiveLoading");
        } else {
            context.addMessage(null, new FacesMessage("Error", "Ya se encuentra un proceso de carga masiva en ejecución."));
            requestContext.update("form:growMassiveLoading");
        }
    }

    private List<String> emailsUser() {
        List<String> list = new ArrayList<>();

        List<FindParametersOutput> findParametersOutputList = this.serviceParameters.
                FindParameters(CargaMasivaParameters.CorreoCM.name());
        for (FindParametersOutput findParametersOutput : findParametersOutputList) {

            String email = findParametersOutput.getValueString();
            list.add(email);

        }

        return list;
    }

    public void listExcelField(FileLoad fileLoad) {
        List<ExcelFieldDTO> list = new ArrayList<>();
        this.listExcelConfigured = list;
        EFields[] efields = EFields.values();
        for (int i = 0; i < efields.length; i++) {
            list.add(new ExcelFieldDTO(efields[i], letters.get(fileLoad.getFields().get(i))));
        }
        this.listExcelConfigured = list;
    }

    public List<Insurances> getInsurances() {
        if (this.insurances == null) {
            this.insurances = new ArrayList<>();
        }
        return insurances;
    }

    public void setInsurances(List<Insurances> insurances) {
        this.insurances = insurances;
    }

    public List<String> getFiles() {
        if (this.files == null) {
            this.files = new ArrayList<>();
        }
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public Insurances getSelectedInsurance() {
        return selectedInsurance;
    }

    public void setSelectedInsurance(Insurances selectedInsurance) {
        this.selectedInsurance = selectedInsurance;
    }

    public void setSelectedFile(String selectedFile) {
        this.selectedFile = selectedFile;
    }

    public String getSelectedFile() {
        return selectedFile;
    }

    public List<String> getLetters() {
        if (this.letters == null) {
            this.letters = new ArrayList<>();
        }
        return letters;
    }

    public void setLetters(List<String> letters) {
        this.letters = letters;
    }

    public List<ExcelFieldDTO> getExcelFields() {
        if (this.excelFields == null) {
            this.excelFields = new ArrayList<>();
        }
        return excelFields;
    }

    public void setExcelFields(List<ExcelFieldDTO> excelFields) {
        this.excelFields = excelFields;
    }

    public InsuranceLoad getInsuranceLoad() {
        return insuranceLoad;
    }

    public boolean isDisableFilesFields() {
        return disableFilesFields;
    }

    public void setDisableFilesFields(boolean disableFilesFields) {
        this.disableFilesFields = disableFilesFields;
    }

    public boolean isDisableFilesFieldsColumns() {
        return disableFilesFieldsColumns;
    }

    public void setDisableFilesFieldsColumns(boolean disableFilesFieldsColumns) {
        this.disableFilesFieldsColumns = disableFilesFieldsColumns;
    }

    
    
    
    
    public boolean isDisableProcessButton() {
        return disableProcessButton;
    }

    public void setDisableProcessButton(boolean disableProcessButton) {
        this.disableProcessButton = disableProcessButton;
    }

    public List<ExcelFieldDTO> getListExcelConfigured() {
        return listExcelConfigured;
    }

    public void setListExcelConfigured(List<ExcelFieldDTO> listExcelConfigured) {
        this.listExcelConfigured = listExcelConfigured;
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }
    
    
    
    

}
