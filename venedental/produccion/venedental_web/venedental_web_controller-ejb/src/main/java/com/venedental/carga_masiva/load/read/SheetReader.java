/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.read;

import com.opencsv.CSVReader;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.Messages;
import com.venedental.carga_masiva.exceptions.MessageException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class SheetReader {

    private static final Logger log = CustomLogger.getLogger(SheetReader.class.getName());
    private final RowReader rowReader;

    public SheetReader(RowReader rowReader) {
        this.rowReader = rowReader;
    }

    public void execute(String filename, String sheetName, String pathFile, File file, String[] fieldTemplate) throws MessageException {
        log.log(Level.INFO, "Inicio - Lectura de hoja: {0}", sheetName);
        try {
            CSVReader csvReader = new CSVReader(new java.io.FileReader(file));
            Iterator<String[]> iterator = csvReader.iterator();
            long numRow = 1;
            while (iterator.hasNext()) {
                String[] row = iterator.next();
                rowReader.execute(filename, sheetName, numRow, row, fieldTemplate);
                numRow++;
            }
            file.delete();
        } catch (FileNotFoundException e) {
            throw new MessageException(Messages.EXCEPTION_READING_FILE, e);
        } catch (MessageException e) {
            throw e;
        }
        log.log(Level.INFO, "Fin - Lectura de hoja");
    }

}
