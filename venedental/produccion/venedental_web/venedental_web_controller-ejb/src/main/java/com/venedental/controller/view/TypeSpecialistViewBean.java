/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.facade.TypeSpecialistFacadeLocal;
import com.venedental.model.Insurances;
import com.venedental.model.TypeSpecialist;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author CarlosDaniel
 */
@ManagedBean(name = "typeSpecialistViewBean")
@ViewScoped
public class TypeSpecialistViewBean extends BaseBean {

    @EJB
    private TypeSpecialistFacadeLocal typeSpecialistFacadeLocal;

    private TypeSpecialist newTypeSpecialist;

    private TypeSpecialist editTypeSpecialist;

    private List<TypeSpecialist> listaTypeSpecialist;
    
    private TypeSpecialist allTypeSpecialist = new  TypeSpecialist(999999,"Todas las Especialidades");
    
    public TypeSpecialistViewBean() {
        this.newTypeSpecialist = new TypeSpecialist();
    }

    public TypeSpecialist getNewTypeSpecialist() {
        return newTypeSpecialist;
    }

    public void setNewTypeSpecialist(TypeSpecialist newTypeSpecialist) {
        this.newTypeSpecialist = newTypeSpecialist;
    }

    public List<TypeSpecialist> getListaTypeSpecialist() {
        if (this.listaTypeSpecialist == null) {
            this.listaTypeSpecialist = this.typeSpecialistFacadeLocal.findAll();
            this.listaTypeSpecialist.add(this.allTypeSpecialist);
        }
        return listaTypeSpecialist;
    }

    public void setListaTypeSpecialist(List<TypeSpecialist> listaTypeSpecialist) {
        this.listaTypeSpecialist = listaTypeSpecialist;
    }

    public TypeSpecialist getEditTypeSpecialist() {
        return editTypeSpecialist;
    }

    public void setEditTypeSpecialist(TypeSpecialist editTypeSpecialist) {
        this.editTypeSpecialist = editTypeSpecialist;
    }

}
