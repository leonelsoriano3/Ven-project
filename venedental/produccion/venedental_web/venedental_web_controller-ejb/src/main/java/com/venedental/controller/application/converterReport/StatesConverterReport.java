/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application.converterReport;

import com.venedental.controller.viewReport.AuditViewBeanReport;
import com.venedental.dto.ReadPropertiesReportOutput;
import com.venedental.dto.reportAudited.StateOutputDTO;
import com.venedental.model.Properties;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import services.utils.CustomLogger;

/**
 *
 * @author akdesk10
 */
@FacesConverter(value = "stateConverterReport", forClass = StateOutputDTO.class)
public class StatesConverterReport implements Converter{

    private static final Logger log = CustomLogger.getGeneralLogger(StatesConverterReport.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        
        AuditViewBeanReport stateViewBean = (AuditViewBeanReport) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "auditViewBeanReport");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                if(value.equals("null")){
                    return -1;
                }
                
                Integer numero = Integer.parseInt(value);
                for (StateOutputDTO states : stateViewBean.getListstateOutputDTO()) {
                    if(states!= null){
                        if (states.getIdState()== numero) {
                            return states;
                        }
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Invalida"));
            }
        }
        return null;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((StateOutputDTO) value).getIdState());
        }
        
    }
}
