/*<<<<<<< .mine
=======*/
package com.venedental.controller.angular.generatePaymentReport;

import com.venedental.constans.ConstansGlobalReport;
import com.venedental.constans.ConstansPaymentReport;
import com.venedental.constans.ConstansTreatmentsReport;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalInfoOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalTreatmentsOutput;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import com.venedental.dto.reportDentalTreatments.TreatmentOutput;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServiceManagementSpecialist;
import com.venedental.services.ServiceReportDentalTreatments;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import services.utils.CustomLogger;
import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import net.sf.jasperreports.engine.JRDataSource;

/**
 * Created by luis.carrasco@arkiteck.biz on 29/06/16.
 */


@ApplicationPath("/resources")
@Path("/reportDentalTreatmentsBean")
public class ReportDentalTreatmentsBean extends Application {

    private static String PATH_REPORTE =  "";
    private static String NOMBRE =  "";
    private static Integer NUM_REPORTE = -1;
    private static Date DATEFOREPORT;
    private static String DATEREPORT;
    private static String NAMESPECIALIST="";
    private static List<TreatmentOutput> LISTTREATMENTS = new ArrayList<>();
    private static String PATHDELETEREPORT = "";
    private static Integer IDUSER = 0;
    private static boolean sendEmail;
    private static String emailReportTreatment="reportetratamiento@yahoo.com";

    @EJB
    private ServiceReportDentalTreatments serviceReportDentalTreatments;

    @EJB
    private ServiceCreateUser serviceCreateUser;


    private static final Logger log = CustomLogger.getGeneralLogger(ReportDentalTreatmentsBean.class.getName());

    private JRBeanCollectionDataSource beanCollectionDataSource;

    private JRDataSource listaDataSource;

    // private List<ReportDentalTreatmentsBean> listTreatments;

    @EJB
    private ServiceKeysPatients serviceKeysPatients;

    @EJB
    private MailSenderLocal mailSenderLocal;

    private JasperPrint jasperPrint;



    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getListDentalTreatments")
    public List<ReportDentalTreatmentsOutput> getListDentalTreatments() throws ParseException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
        Integer idUser = user.getId();

        List<ReportDentalTreatmentsOutput> result = serviceReportDentalTreatments.getReportDentalTreatements(idUser);
        for(int i=0; i<result.size();i++){

            //fecha maxima y minima para el filtro de detalle
            String dateKey = new SimpleDateFormat("dd/MM/yyyy").format(result.get(i).getDateKey());
            result.get(i).setDateKeyS(dateKey);
            String startDate = new SimpleDateFormat("dd/MM/yyyy").format(result.get(i).getStartDate());
            result.get(i).setStartDateS(startDate);
            String endDate = new SimpleDateFormat("dd/MM/yyyy").format(result.get(i).getEndDate());
            result.get(i).setEndDateS(endDate);
        }


        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getSpecialistToReport")
    public SpecialistOutput getSpecialistToReport(@QueryParam("idUser") Integer idUser) {

        SpecialistOutput specialistOutput;
        specialistOutput = serviceReportDentalTreatments.getSpecialistToReport(idUser);

        if (specialistOutput != null) {
            return specialistOutput;
        } else {
            return specialistOutput = new SpecialistOutput();
        }

    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("getCorrelativeNumber")
    public Integer getCorrelativeNumber() {
        ReportDentalInfoOutput reportDentalInfoOutput;

        Integer numRep = -1;
        reportDentalInfoOutput = serviceReportDentalTreatments.getCorrelativeNumber(this.getIdUserLogged());
        this.NUM_REPORTE = reportDentalInfoOutput.getNumCorrelativo();
        this.DATEFOREPORT=reportDentalInfoOutput.getDateForReport();
        numRep= reportDentalInfoOutput.getNumCorrelativo();
        return numRep;

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findInfoKeyDetails")
    public List<ConsultKeysPatientsDetailTreatmentOutput> getInfoKeysDetails(@QueryParam("idKey") Integer idKey){

        List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutputs = null;
        consultKeysPatientsDetailTreatmentOutputs = serviceKeysPatients.detailsConsultKeysPatientsTreatments(idKey);
        for(ConsultKeysPatientsDetailTreatmentOutput filterReportDetail:consultKeysPatientsDetailTreatmentOutputs){

            String dateForTratament = new SimpleDateFormat("dd/MM/yyyy").format(filterReportDetail.getDateForTreatment());
            filterReportDetail.setDateFilterTreatment(dateForTratament);
            filterReportDetail.setBtnDownload0(0);
            filterReportDetail.setBtnDownload1(0);
            filterReportDetail.setBtnDownload2(0);
            filterReportDetail.setBtnDownload3(0);
            filterReportDetail.setBtnDownload4(0);

            try {
                filterReportDetail.setLConsultNameFileOutputList(
                        serviceReportDentalTreatments.ConsultnameFile(Integer.valueOf(filterReportDetail.getIdPieceTreatmentKey())));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return consultKeysPatientsDetailTreatmentOutputs;
    }



    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("findInfoKey")
    public KeysDetailsOutput findInfoKey(@QueryParam("idKey") Integer idKey) {

        return serviceKeysPatients.findDetails(idKey);

    }

    public Integer getIdUserLogged(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
        IDUSER = user.getId();
        return user.getId();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("generatePDF")
    public  Response generatePDF() throws IOException, ParseException {

        Response resp = null;
        try {
            SpecialistOutput specialistOutput  = serviceReportDentalTreatments.getSpecialistToReport(this.getIdUserLogged());

            if(specialistOutput != null){
                String nameReport = "RT_" + specialistOutput.getName() + "_" + specialistOutput.getCadrId();
                Map<String, Object> params = new HashMap<>();
                String logoPath = obtainDirectory() + ConstansTreatmentsReport.PROPERTY_PATH_LOGO;
                String jasperPath = obtainDirectory() + this.readFileProperties().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_JASPER);
                ReportDentalTreatmentsBean.PATH_REPORTE =  jasperPath;
                ReportDentalTreatmentsBean.NOMBRE = nameReport;
                this.NAMESPECIALIST=specialistOutput.getName();

                // Información del Especialista para el reporte
                params.put("name",specialistOutput.getName());
                params.put("cardId",specialistOutput.getCadrId());
                params.put("phone",specialistOutput.getPhone());
                params.put("address",specialistOutput.getAddress());
                params.put("especialidad",specialistOutput.getSpecialityName());
                params.put("logoPath", logoPath);
                listaDataSource = new ReportDentalTreatmentsDataSource(LISTTREATMENTS);
                String fecha = new SimpleDateFormat("dd/MM/yyyy").format(DATEFOREPORT);
                Date  fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);

                java.util.Date dateForReport = new java.util.Date(fechaTransformada.getTime());
                this.DATEREPORT=fecha;
                params.put("numCorrelative",NUM_REPORTE);
                params.put("dateForReport",dateForReport);
                String rPath = "/home/ftp/temp/";
                String reportExtension = ".pdf";

                try {
                    nameReport = StringUtils.replace(StringUtils.stripAccents(NOMBRE), " ", "_");
                    JasperPrint jp = JasperFillManager.fillReport(jasperPath, params, listaDataSource);
                    String reportPath ="/home/ftp/temp/";
                    List<File> files = new ArrayList<File>();
                    File file = new File(rPath+nameReport+ reportExtension);
                    files.add(file);

                    JasperExportManager.exportReportToPdfFile(jp, rPath  +nameReport+ reportExtension);
                    sendMailReportTreatment(files);
                    LISTTREATMENTS.removeAll(LISTTREATMENTS);
                } catch (JRException e) {

                    e.printStackTrace();

                }
                PATHDELETEREPORT = rPath + nameReport + reportExtension;
                resp =  this.downloadFile(rPath + nameReport + reportExtension,nameReport + reportExtension);
//                this.deleteFile(sendEmail);
            }
        } catch (IOException e){
//            System.out.println("Error al generar pdf");
        }

//
        return resp;
//        Integer var = 5;
//        return var;
    }

//     /**
//     * Method to seek  report
//     *
//     * @return List File
//     * @throws IOException
//     */
//    public List<File> findFile(String nameReport) throws IOException {
//        List<File> files = new ArrayList<>();
//        String reportPath ="venedental_web-web-1.1.1_war/reports/specialist/";
//        String reportExtension = ".pdf";
//        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
//        String path = reportPath + nameReport + reportExtension;
//        File file = new File(path);
//        files.add(file);
//        return files;
//    }

    private String buildMessageReportTreatment() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Usuario").append(".<br/>").append("<br/>");
        stringBuilder.append("Se anexa reporte de tratamiento nro. ").append(" ").append(this.NUM_REPORTE).append(" del especialista ").append(this.NAMESPECIALIST.toUpperCase());
        stringBuilder.append(" con fecha de emisi&#243;n ").append(this.DATEREPORT).append(".").append("<br><br>");
        stringBuilder.append("Siempre dispuestos a prestarle un mejor servicio.").append("<br><br>");
        stringBuilder.append("<b>").append("Grupo VENEDEN").append("</b>").append("<br/>");
        return stringBuilder.toString();
    }

    public void sendMailReportTreatment(List<File> files) throws IOException{
        sendEmail=false;
        String title = "Reporte de tratamiento nro. "+this.NUM_REPORTE;
        String subjectMessage = title + " especialista "+this.NAMESPECIALIST;
        String contentMessage = buildMessageReportTreatment();
        this.sendEmail=this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessage, files,
                emailReportTreatment);


    }

    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/updateKeysStatus")
    public Integer updateKeysStatus(@QueryParam("idKey") String idKey,
                                    @QueryParam("idKeyTreatment") Integer idKeyTreatment,
                                    @QueryParam("idToothTreatment") Integer idToothTreatment,
                                    @QueryParam("numReport") Integer numReport){
        Boolean updated;
        Integer response = 0;
//        Integer idUser = this.getIdUserLogged();
        Long idK = Long.valueOf(idKey);

        updated = serviceReportDentalTreatments.updateKeysStatus(idK,idKeyTreatment,idToothTreatment,numReport,IDUSER);
        if(updated){
            response = 1;
        }else{
            response = 0;
        }
        return response;
    }


    public static String obtainDirectory() {
        log.log(Level.INFO, "obtainDirectory()");
        URL rutaURL = ReportDentalTreatmentsBean.class.getProtectionDomain().getCodeSource().getLocation();
        String path = rutaURL.getPath();

        return path.substring(0, path.indexOf("lib"));
    }

    @PUT
    @Path("getInfoArrayTreat")
    public void getInfoArrayTreat(@QueryParam("dateKey") String dateKey,@QueryParam("namePatient") String namePatient,
                                  @QueryParam("numberKey") String numberKey,@QueryParam("numTooth") String numTooth,
                                  @QueryParam("nameTreatemnent") String nameTreatemnent){

        TreatmentOutput treatmentOutput = new TreatmentOutput();
        treatmentOutput.setNamePatient(namePatient);
        treatmentOutput.setDateKey(dateKey);
        treatmentOutput.setNumberKey(numberKey);
        treatmentOutput.setNumTooth(numTooth);
        treatmentOutput.setNameTreatment(nameTreatemnent);

        LISTTREATMENTS.add(treatmentOutput);

    }
    /**
     * params :
     *  filePath = Path del archivo a descargar + nombre del archivo + extensión
     *  nameReport = Nombre del archivo + extensión
     *
     * */
    public Response downloadFile(String filePath,String nameReport) throws IOException {

        File file = new File(filePath);
        ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename="+nameReport+" ");
        return responseBuilder.build();

    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("deleteFile")
    public Boolean deleteFile(boolean email) throws IOException {
        Boolean resp = false;
        File file = new File(this.PATHDELETEREPORT);
        if (file.exists()) {
            if(email==true){
                file.delete();
                resp = true;
            }else{
                deleteFile(this.sendEmail);
            }

        }
        return resp;
    }



}
