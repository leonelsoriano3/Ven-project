/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.ReimbursementRequeriment;
import com.venedental.model.Patients;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author AKDESK12
 */
@FacesConverter("autoCompleteReimbursementFormConverter")
public class AutoCompleteReimbursementFormConverter implements Converter{
    @ManagedProperty(value = "#{reimbursementRequeriment}")
    private ReimbursementRequeriment reimbursementRequeriment;
    
    private Boolean encontrado = false;

    public ReimbursementRequeriment getReimbursementRequeriment() {
        return reimbursementRequeriment;
    }

    public void setReimbursementRequeriment(ReimbursementRequeriment reimbursementRequeriment) {
        this.reimbursementRequeriment = reimbursementRequeriment;
    }
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        int posicion = 0;
        
        if (value != null && value.trim().length() > 0) {
            try{
                
                encontrado = false;
                
                ReimbursementRequeriment reimbursementsFormViewBean = (ReimbursementRequeriment) context.getApplication().getELResolver().getValue(context.getELContext(), null, "reimbursementRequeriment");
                int numero = Integer.parseInt(value);
                int j = 0;
                
                for (int i = 0; i < reimbursementsFormViewBean.getPatientsList().size(); i++) {
                    Patients s = reimbursementsFormViewBean.getPatientsList().get(i);
                    if (s.getId() == numero) {
                        posicion = j;
                    }
                    j++;
                }
                if (reimbursementsFormViewBean.getPatientsList().size() > 0) {
                    return reimbursementsFormViewBean.getPatientsList().get(posicion);
                }
                
                return null;
            }catch(NumberFormatException e){
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
            
        }else{
            return null;
        }
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value){
        if (value instanceof Patients) {
            Patients patient = (Patients) value;
            return patient.getId().toString();
        }
        return "";
    }
    
}