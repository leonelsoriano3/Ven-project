/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.PlansTreatmentsViewBean;
import com.venedental.controller.view.RegisterKeysAnalysisViewBean;
import com.venedental.model.PlansTreatments;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "plansTreatmentsConverter", forClass = PlansTreatments.class)
public class PlansTreatmentsConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        PlansTreatmentsViewBean plansTreatmentsViewBean = (PlansTreatmentsViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "plansTreatmentsViewBean");
        RegisterKeysAnalysisViewBean registerKeysAnalysisViewBean = (RegisterKeysAnalysisViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "registerKeysAnalysisViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                
                for (PlansTreatments plansTreatments : plansTreatmentsViewBean.getListPlansTreatments()) {
                    if (plansTreatments.getId() == numero) {
                        return plansTreatments;
                    }
                }
                 for (PlansTreatments plansTreatments : plansTreatmentsViewBean.getNewListPlansTreatments()) {
                    if (plansTreatments.getId() == numero) {
                        return plansTreatments;
                    }
                }
                 for (PlansTreatments plansTreatments : plansTreatmentsViewBean.getListPlansTreatmentsType()) {
                    if (plansTreatments.getId() == numero) {
                        return plansTreatments;
                    }
                }
                
                 for (PlansTreatments plansTreatments : registerKeysAnalysisViewBean.getPlansTreatments()) {
                    if (plansTreatments.getId() == numero) {
                        return plansTreatments;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tratamientos por plan Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((PlansTreatments) value).getId());
        }

    }

}
