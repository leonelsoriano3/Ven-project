/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view.AnalisisHistorico;

import com.venedental.controller.BaseBean;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.analysisKeys.ConfirmKeyOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.PlansPatients;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.Specialists;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceSpecialists;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 * Where class all about the third stage of analysis of key performs
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "reviewKeysAnalysisHistoricalViewBean")
@ViewScoped
public class ReviewKeysAnalysisHistoricalViewBean extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(ReviewKeysAnalysisHistoricalViewBean.class.getName());
    private List<PlansTreatmentsKeys> plansTreatmentsKeys;
    private List<PlansTreatmentsKeys> plansTreatmentsKeysEvaluated;
    private List<PlansTreatmentsKeys> selectedPlansTreatmentsKeys;
    private Specialists specialist;
    private List<PlansPatients> plansPatients;
    private List<ConfirmKeyOutput> confirmList;
    private List<ConfirmKeyOutput> confirmListFiltered;
    private List<ConfirmKeyDetailsOutput> confirmKeyDetailsList;
    private List<ConfirmKeyDetailsOutput> confirmKeyDetailsListFiltered;
    private Specialists selectedSpecialist;
    private Integer selectedConfirmKeysStatus;
    private Integer idMonthExecution;
    private boolean seeColumnCheck = false;
    private Date dateFilter;
    private boolean reviewKeysAnalysisHistoricalDetails;
    private boolean reviewKeysAnalysisList;
    private boolean btnConfirmReviewKeysAnalysisHistoricalDetails;
    private boolean btnLeaveReviewKeysAnalysisHistoricalDetails;
    private ConfirmKeyOutput selectedConfirmKey;
    private String nameMonth;
    private Double totalAmounthPaid;
    private Boolean inputTable = true;
    private static Date maxDate;

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;
    @EJB
    private ServiceSpecialists serviceSpecialist;

    public ReviewKeysAnalysisHistoricalViewBean() {
        log.log(Level.INFO, "ReviewKeysAnalysisHistoricalViewBean()");
        this.reviewKeysAnalysisHistoricalDetails = false;
        this.reviewKeysAnalysisList = true;
        this.btnConfirmReviewKeysAnalysisHistoricalDetails = false;
        this.btnLeaveReviewKeysAnalysisHistoricalDetails = false;
    }

    public Boolean getInputTable() {
        return inputTable;
    }

    public void setInputTable(Boolean inputTable) {
        this.inputTable = inputTable;
    }

    public Integer getSelectedConfirmKeysStatus() {
        return selectedConfirmKeysStatus;
    }

    public void setSelectedConfirmKeysStatus(Integer selectedConfirmKeysStatus) {
        this.selectedConfirmKeysStatus = selectedConfirmKeysStatus;
    }

    public Integer getIdMonthExecution() {
        return idMonthExecution;
    }

    public void setIdMonthExecution(Integer idMonthExecution) {
        this.idMonthExecution = idMonthExecution;
    }

    public boolean getBtnConfirmReviewKeysAnalysisDetails() {
        return btnConfirmReviewKeysAnalysisHistoricalDetails;
    }

    public void setBtnConfirmReviewKeysAnalysisDetails(boolean btnConfirmReviewKeysAnalysisHistoricalDetails) {
        this.btnConfirmReviewKeysAnalysisHistoricalDetails = btnConfirmReviewKeysAnalysisHistoricalDetails;
    }

    public boolean getBtnLeaveReviewKeysAnalysisDetails() {
        return btnLeaveReviewKeysAnalysisHistoricalDetails;
    }

    public void setBtnLeaveReviewKeysAnalysisDetails(boolean btnLeaveReviewKeysAnalysisHistoricalDetails) {
        this.btnLeaveReviewKeysAnalysisHistoricalDetails = btnLeaveReviewKeysAnalysisHistoricalDetails;
    }

    public boolean isSeeColumnCheck() {
        return seeColumnCheck;
    }

    public void setSeeColumnCheck(boolean seeColumnCheck) {
        this.seeColumnCheck = seeColumnCheck;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public List<PlansPatients> getPlansPatients() {
        if (this.plansPatients == null) {
            this.plansPatients = new ArrayList<>();
        }
        return plansPatients;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysEvaluated() {
        if (this.plansTreatmentsKeysEvaluated == null) {
            this.plansTreatmentsKeysEvaluated = new ArrayList<>();
        }
        return plansTreatmentsKeysEvaluated;
    }

    public void setPlansTreatmentsKeysEvaluated(List<PlansTreatmentsKeys> plansTreatmentsKeysEvaluated) {
        this.plansTreatmentsKeysEvaluated = plansTreatmentsKeysEvaluated;
    }

    public void setPlansPatients(List<PlansPatients> plansPatients) {
        this.plansPatients = plansPatients;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeys() {
        if (this.plansTreatmentsKeys == null) {
            this.plansTreatmentsKeys = new ArrayList<>();
        }
        return plansTreatmentsKeys;
    }

    public void setPlansTreatmentsKeys(List<PlansTreatmentsKeys> plansTreatmentsKeys) {
        this.plansTreatmentsKeys = plansTreatmentsKeys;
    }

    public List<PlansTreatmentsKeys> getSelectedPlansTreatmentsKeys() {
        if (this.selectedPlansTreatmentsKeys == null) {
            this.selectedPlansTreatmentsKeys = new ArrayList<>();
        }
        return selectedPlansTreatmentsKeys;
    }

    public void setSelectedPlansTreatmentsKeys(List<PlansTreatmentsKeys> selectedPlansTreatmentsKeys) {
        this.selectedPlansTreatmentsKeys = selectedPlansTreatmentsKeys;
    }

    public List<ConfirmKeyOutput> getConfirmList() {
        if (this.confirmList == null) {
            this.confirmList = new ArrayList<>();
        }

        return confirmList;
    }

    public void setConfirmList(List<ConfirmKeyOutput> confirmList) {
        this.confirmList = confirmList;
    }

    public List<ConfirmKeyOutput> getConfirmListFiltered() {
        return confirmListFiltered;
    }

    public void setConfirmListFiltered(List<ConfirmKeyOutput> confirmListFiltered) {
        this.confirmListFiltered = confirmListFiltered;
    }

    public boolean getReviewKeysAnalysisHistoricalDetails() {
        return reviewKeysAnalysisHistoricalDetails;
    }

    public void setReviewKeysAnalysisHistoricalDetails(boolean reviewKeysAnalysisHistoricalDetails) {
        this.reviewKeysAnalysisHistoricalDetails = reviewKeysAnalysisHistoricalDetails;
    }

    public boolean isReviewKeysAnalysisList() {
        return reviewKeysAnalysisList;
    }

    public void setReviewKeysAnalysisList(boolean reviewKeysAnalysisList) {
        this.reviewKeysAnalysisList = reviewKeysAnalysisList;
    }

    public ConfirmKeyOutput getSelectedConfirmKey() {
        return selectedConfirmKey;
    }

    public List<ConfirmKeyDetailsOutput> getConfirmKeyDetailsList() {
        if (this.confirmKeyDetailsList == null) {
            this.confirmKeyDetailsList = new ArrayList<>();
        }
        return confirmKeyDetailsList;
    }

    public void setConfirmKeyDetailsList(List<ConfirmKeyDetailsOutput> confirmKeyDetailsList) {
        this.confirmKeyDetailsList = confirmKeyDetailsList;
    }

    public List<ConfirmKeyDetailsOutput> getConfirmKeyDetailsListFiltered() {
        return confirmKeyDetailsListFiltered;
    }

    public void setConfirmKeyDetailsListFiltered(List<ConfirmKeyDetailsOutput> confirmKeyDetailsListFiltered) {
        this.confirmKeyDetailsListFiltered = confirmKeyDetailsListFiltered;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public Double getTotalAmounthPaid() {
        return totalAmounthPaid;
    }

    public void setTotalAmounthPaid(Double totalAmounthPaid) {
        this.totalAmounthPaid = totalAmounthPaid;
    }

    /**
     * method that load the objectConfirmKey where is selected of the listConfirmKeysList
     *
     * @param selectedConfirmKey
     */
    public void setSelectedConfirmKey(ConfirmKeyOutput selectedConfirmKey) {
        log.log(Level.INFO, "setSelectedConfirmKey(selectedConfirmKey)");
        int status = 0;
        if (selectedConfirmKey.getNameStatus().equals("PENDIENTE")) {
            status = EstatusPlansTreatmentsKeys.AUDITADO.getValor();
        } else {
            status = EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor();
        }
        this.setIdMonthExecution(selectedConfirmKey.getIdMonthExecution());
        this.setSelectedConfirmKeysStatus(selectedConfirmKey.getStatus());
        this.setConfirmKeyDetailsList(this.serviceAnalysisKeys.findConfirmKeyDetailsOutput(this.
                getSelectedSpecialist().getId(), status, selectedConfirmKey.
                getIdMonthExecution(), selectedConfirmKey.getDateExecution()));
        this.setConfirmKeyDetailsListFiltered(this.getConfirmKeyDetailsList());
        this.setNameMonth(selectedConfirmKey.getNameMonthExecution());
        if (!getConfirmKeyDetailsList().isEmpty()) {
            this.setTotalAmounthPaid(this.getConfirmKeyDetailsList().get(0).getTotalAmountPaid());
        }
        if (Objects.equals(selectedConfirmKey.getStatus(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor())) {

            this.setBtnConfirmReviewKeysAnalysisDetails(false);
            this.setBtnLeaveReviewKeysAnalysisDetails(true);
        } else {

            this.setBtnConfirmReviewKeysAnalysisDetails(true);
            this.setBtnLeaveReviewKeysAnalysisDetails(true);
        }
        this.selectedConfirmKey = selectedConfirmKey;
    }

    /**
     * *
     * Running method is barely up dialogue
     *
     *
     * @param selectedKey
     */
    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        log.log(Level.INFO, "filterSpecialist()");
        List<Specialists> filteredSpecialists = this.serviceSpecialist.findByIdentityOrName(query);
        return filteredSpecialists;
    }

    /**
     * Method called when I select a specialist from the list
     *
     * @param event *
     */
    public void onItemSelect(SelectEvent event) {
        log.log(Level.INFO, "onItemSelect()");
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        filterKeys();
    }

    /**
     * Method that allows you to find the keys generated or state analysis and also treatments that have registered,
     * refuse or for review , is executed when selected a specialist *
     */
    public void filterKeys() {
        log.log(Level.INFO, "filterKeys()");
        this.setInputTable(true);
        resetDataTable();
        this.setConfirmList(this.serviceAnalysisKeys.findConfirmKeyOutput(this.getSelectedSpecialist().getId()));
        this.setConfirmListFiltered(this.getConfirmList());
        if (!this.getConfirmList().isEmpty()) {
            this.setInputTable(false);
        }
        RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        RequestContext.getCurrentInstance().update("form:btnEditKeyAnlysisReview");
    }

    public void resetDataTable() {
        log.log(Level.INFO, "resetDataTable()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reviewAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:reviewAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        }
    }

    /**
     * Method that allows you to clean all the lists used in this controlled
     */
    public void clear() {
        log.log(Level.INFO, "clear()");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        this.setPlansPatients(new ArrayList<PlansPatients>());
        this.setSelectedPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansTreatmentsKeysEvaluated(new ArrayList<PlansTreatmentsKeys>());
    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelect(SelectEvent event) {
        log.log(Level.INFO, "handleDateSelect()");
        if (!this.getConfirmListFiltered().isEmpty()) {
            RequestContext.getCurrentInstance().execute("PF('reviewAnalysisTable').filter()");
        }
    }

    /**
     * DateTrasnformar method that allows a date to the form dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public Date formatDate(Date date) throws ParseException {
        log.log(Level.INFO, "formatDate()");
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;

    }

    /**
     * Method showing the detail of receiving when selected track and hides the main list
     */
    public void goReviewKeysAnalysisDetails() {
        log.log(Level.INFO, "goReviewKeysAnalysisDetails()");
        this.setReviewKeysAnalysisHistoricalDetails(true);
        this.setReviewKeysAnalysisList(false);
        RequestContext.getCurrentInstance().update("form");
    }

    /**
     * Method showing the list of keysConfirm and hide the details
     */
    public void goReviewKeysAnalysisList() {
        log.log(Level.INFO, "goReviewKeysAnalysisList()");
        this.setReviewKeysAnalysisHistoricalDetails(false);
        this.setReviewKeysAnalysisList(true);
        this.setBtnConfirmReviewKeysAnalysisDetails(false);
        this.setBtnLeaveReviewKeysAnalysisDetails(false);
        this.resetDataTableKeys();
        RequestContext.getCurrentInstance().execute("PF('reviewAnalysisTable').filter()");
        RequestContext.getCurrentInstance().update("form");
        RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");

    }

    public void resetDataTableKeys() {
        log.log(Level.INFO, "resetDataTableKeys()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reviewAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:reviewAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        }
    }

    public Date getMaxDate() throws ParseException {
        log.log(Level.INFO, "getMaxDate()");
        maxDate = Transformar.currentDate();
        return maxDate;
    }

}
