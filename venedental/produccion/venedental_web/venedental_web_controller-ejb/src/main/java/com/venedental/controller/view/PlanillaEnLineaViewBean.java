/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.MessageSessionBean;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import model.ListaLinks;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import services.utils.CustomLogger;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "planillaEnLineaViewBean")
@ViewScoped
public class PlanillaEnLineaViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;
    private static final Logger log = CustomLogger.getGeneralLogger(PlanillaEnLineaViewBean.class.getName());

    List<ListaLinks> listaLinks;
    private String nombreCompletoTitular;
    private String cedulaTitular;
    private String nombreCompletoPaciente;
    private String cedulaPaciente;
    private String companyiaSeguro;
    private String empresa;
    private String telefonoContacto;
    private String celular;
    private String correoElectronico;
    private String correoConfirmar;
    private String banco;
    private String numeroCuenta;
    private String numeroFactura;
    private Double montoFactura;
    private String fechaFactura;
    
    private UploadedFile file;
    private UploadedFile file1;
    private UploadedFile file2;
    private UploadedFile file3;
    private UploadedFile file4;

   

    private String valorCorreo;
    private String valorCorreoConfirmar;

    @ManagedProperty(value = "#{messageSessionBean}")
    private MessageSessionBean messageSessionBean;

    @EJB
    private MailSenderLocal mailSenderLocal;
    
    public PlanillaEnLineaViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaLinks(new ArrayList<ListaLinks>());
        this.getListaLinks().add(new ListaLinks("Servicios", "venedental.xhtml", "/recursos/img/list-bullets2.png"));
        this.getListaLinks().add(new ListaLinks("Plan Venedevisual", "planVenedevisual.xhtml", "/recursos/img/list-bullets2.png"));
        this.getListaLinks().add(new ListaLinks("Reembolso", "reembolso.xhtml", "/recursos/img/list-bullets2.png"));
        this.getListaLinks().add(new ListaLinks("Centro de Atención", "directorio.xhtml", "/recursos/img/list-bullets2.png"));
    }

    public List<ListaLinks> getListaLinks() {
        return listaLinks;
    }

    public void setListaLinks(List<ListaLinks> listaLinks) {
        this.listaLinks = listaLinks;
    }

    public String getNombreCompletoTitular() {
        return nombreCompletoTitular;
    }

    public void setNombreCompletoTitular(String nombreCompletoTitular) {
        this.nombreCompletoTitular = nombreCompletoTitular;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public String getCedulaTitular() {
        return cedulaTitular;
    }

    public void setCedulaTitular(String cedulaTitular) {
        this.cedulaTitular = cedulaTitular;
    }

    public String getNombreCompletoPaciente() {
        return nombreCompletoPaciente;
    }

    public void setNombreCompletoPaciente(String nombreCompletoPaciente) {
        this.nombreCompletoPaciente = nombreCompletoPaciente;
    }

    public String getCedulaPaciente() {
        return cedulaPaciente;
    }

    public void setCedulaPaciente(String cedulaPaciente) {
        this.cedulaPaciente = cedulaPaciente;
    }

    public String getCompanyiaSeguro() {
        return companyiaSeguro;
    }

    public void setCompanyiaSeguro(String companyiaSeguro) {
        this.companyiaSeguro = companyiaSeguro;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public MailSenderLocal getMailSenderLocal() {
        return mailSenderLocal;
    }

    public void setMailSenderLocal(MailSenderLocal mailSenderLocal) {
        this.mailSenderLocal = mailSenderLocal;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCorreoConfirmar() {
        return correoConfirmar;
    }

    public void setCorreoConfirmar(String correoConfirmar) {
        this.correoConfirmar = correoConfirmar;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
 
    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Double getMontoFactura() {
        return montoFactura;
    }

    public void setMontoFactura(Double montoFactura) {
        this.montoFactura = montoFactura;
    }

    public MessageSessionBean getMessageSessionBean() {
        return messageSessionBean;
    }

    public void setMessageSessionBean(MessageSessionBean messageSessionBean) {
        this.messageSessionBean = messageSessionBean;
    }

    public String getValorCorreo() {
        return valorCorreo;
    }

    public void setValorCorreo(String valorCorreo) {
        this.valorCorreo = valorCorreo;
    }

    public String getValorCorreoConfirmar() {
        return valorCorreoConfirmar;
    }

    public void setValorCorreoConfirmar(String valorCorreoConfirmar) {
        this.valorCorreoConfirmar = valorCorreoConfirmar;
    }
    
    public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
     public UploadedFile getFile1() {
        return file1;
    }

    public void setFile1(UploadedFile file1) {
        this.file1 = file1;
    }

    public UploadedFile getFile2() {
        return file2;
    }

    public void setFile2(UploadedFile file2) {
        this.file2 = file2;
    }

    public UploadedFile getFile3() {
        return file3;
    }

    public void setFile3(UploadedFile file3) {
        this.file3 = file3;
    }

    public UploadedFile getFile4() {
        return file4;
    }

    public void setFile4(UploadedFile file4) {
        this.file4 = file4;
    }
    
    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage message = new FacesMessage("EXITO", event.getFile().getFileName() + " Archivos Cargados de manera correcta");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    
    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Exitoso", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    public void upload1() {
        if(file1 != null) {
            FacesMessage message = new FacesMessage("Exitoso", file1.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
     public void botonEnviar(ActionEvent actionEvent) {
        addMessage("Solicitud de Reembolso procesada");
        //<br/>Por favor verifique <br/>En la bandeja den entrada <br/>de su correo}
    }
     
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void upload2() {
        if(file2 != null) {
            FacesMessage message = new FacesMessage("Exitoso", file2.getFileName() + " fue cargado.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    public void upload3() {
        if(file3 != null) {
            FacesMessage message = new FacesMessage("Exitoso", file3.getFileName() + " fue cargado.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    public void upload4() {
        if(file4 != null) {
            FacesMessage message = new FacesMessage("Exitoso", file4.getFileName() + " fue cargado.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    public void validarCedula(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String valor = comp.getSubmittedValue().toString();
        if (!valor.isEmpty() && valor.length() < 6) {
            FacesMessage msg = new FacesMessage("La cédula debe contener al menos 6 caracteres");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        } else {
            comp.setValid(true);
        }
    }

    public void validarEmail(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String valor = comp.getSubmittedValue().toString();
        setValorCorreo(valor);
        if (revisar(comp, valor)) {
            if (!valor.equals(getValorCorreoConfirmar())) {
                UIInput compConfirmar = (UIInput) findComponent("confirmarCorreo");
                FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                compConfirmar.setValid(false);
                FacesContext.getCurrentInstance().addMessage(compConfirmar.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }
    }

    public void validarConfirmarEmail(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String valor = comp.getSubmittedValue().toString();
        setValorCorreoConfirmar(valor);
        if (revisar(comp, valor)) {
            if (!valor.isEmpty()) {
                if (!valor.equalsIgnoreCase(getValorCorreo())) {
                    FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    comp.setValid(false);
                    FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                    FacesContext.getCurrentInstance().renderResponse();
                }
            }
        }
    }
    
    public boolean revisar(UIInput comp, String valor) {
        if (!valor.isEmpty() && !Pattern.matches("^([\\dA-Za-z_\\.-]+)@([\\dA-Za-z\\.-]+)\\.([A-Za-z\\.]{2,6})$", valor)) {
            FacesMessage msg = new FacesMessage("No es una dirección de correo válida");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
            return false;
        }
        return true;
    }

    public void validarCorreo(ComponentSystemEvent event) {
        UIInput compCorreo = (UIInput) findComponent("correo");
        UIInput compConfirmar = (UIInput) findComponent("confirmarCorreo");
        String valueCorreo = compCorreo.getLocalValue() == null ? "" : compCorreo.getLocalValue().toString();
        String valueConfirmar = compConfirmar.getLocalValue() == null ? "" : compConfirmar.getLocalValue().toString();
        if (!valueCorreo.equalsIgnoreCase(valueConfirmar)) {
            FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            compConfirmar.setValid(false);
            FacesContext.getCurrentInstance().addMessage(compConfirmar.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public void enviarCorreo() {
        System.out.println("Entro metodo");
        /*String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        String correoAnalista = crearCorreo();
        String correoUsuario = crearCorreoRespuesta();
        String direccionUsuario = getCorreoElectronico();
        mailSenderLocal.send(Email.MAIL_REEMBOLSO, "Solicitud de reembolso", correoAnalista, null, mailSenderLocal.getUsername(Email.MAIL_REEMBOLSO));
        mailSenderLocal.send(Email.MAIL_REEMBOLSO, "Solicitud de reembolso", correoUsuario, null, direccionUsuario);*/
        limpiarFormulario("panelPlanilla");
        //messageSessionBean.setMensaje("Su solicitud fue enviada correctamente. Recibirá en pocos minutos un correo electrónico de confirmación de su solicitud con la información de documentos los cuales debe consignar para procesar su solicitud.");
        try {
             System.out.println("Entro try");
            redireccionar("venedevisual.xhtml");
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

    private void limpiarFormulario(String idDialog) {
        System.out.println("Entro limpiar");
        setBanco("");
        setCedulaPaciente("");
        setCedulaTitular("");
        setCelular("");
        setCompanyiaSeguro("");
        setCorreoConfirmar("");
        setCorreoElectronico("");
        setEmpresa("");
        setMontoFactura(0.0);
        setNombreCompletoPaciente("");
        setNombreCompletoPaciente("");
        setNombreCompletoTitular("");
        setNumeroCuenta("");
        setNumeroFactura("");
        setTelefonoContacto("");
        this.init();
        RequestContext contextC = RequestContext.getCurrentInstance();
        contextC.update("form1:form2:" + idDialog);
        
    }

    private String crearCorreoRespuesta() {
        StringBuilder correo = new StringBuilder();
        correo.append(" <div>\n <p> Su solicitud se ha recibido satisfactoriamente.\n");
        correo.append(" Para procesar su reembolso debe enviar al siguiente correo <a href=\"mailto:reembolso@venedental.com\" target=\"_blank\">reembolso@venedental.com</a> los siguientes\n");
        correo.append(" requisitos:</p>\n <ul>\n <li>\n Copia de la Cédula de Identidad del Titular y del Beneficiario \n");
        correo.append(" (incluidos Menores de Edad que tengan cédulas).\n </li>\n <li>\n");
        correo.append(" Informe Médico emitido por el Oftalmólogo, sellado y firmado por el mismo.\n");
        correo.append(" </li>\n <li>\n Formula médica, sellada y firmada por el Oftalmólogo.\n");
        correo.append(" </li>\n <li>\n Número de Cuenta y Banco del Titular (no del Beneficiario), con soporte \n");
        correo.append(" de Libreta Bancaria y/o Cheque Anulado para realizar la transferencia.\n </li>\n\n </ul>\n");
        correo.append(" <p><b>\n La Factura contable original, de acuerdo a las estipulaciones del SENIAT, \n");
        correo.append(" debe ser emitida a nombre del titular y/o beneficiario según sea el caso,\n");
        correo.append(" y enviarla a través de nuestro casillero ZOOM N°: CCS 2581 a nombre de VENEDENTAL, C.A.\n");
        correo.append(" </b> \n Cuando se trate de menores de edad, dicha factura debe ser emitida a nombre del Titular.</p> \n");
        correo.append(" <p>Los datos recibidos:</p>\n </div>\n <div>\n <table cellspacing=\"1\" cellpadding=\"3\" border=\"0\"> \n <tbody><tr> \n");
        correo.append(" <th colspan=\"2\"> \n <b>Datos Personales</b> \n </th> \n </tr> \n <tr> \n <td> \n Nombres y Apellidos del Titular: \n </td>\n <td> \n");
        correo.append(getNombreCompletoTitular());
        correo.append("\n </td> \n </tr> \n <tr> \n <td> \n ");
        correo.append("Cédula de Identidad: \n </td>\n <td> \n");
        correo.append(getCedulaTitular());
        correo.append("\n </td> \n </tr> \n <tr> \n <td> \n ");
        correo.append("Nombres y Apellidos del Paciente:\n </td>\n <td> \n");
        correo.append(getNombreCompletoPaciente());
        correo.append("\n </td> \n </tr> \n <tr> \n <td> \n ");
        correo.append("Cédula de Identidad:\n </td>\n <td> \n");
        correo.append(getCedulaPaciente());
        correo.append("\n </td> \n </tr> \n <tr> \n <td> \n ");
        correo.append("Compañía de Seguro:\n </td>\n <td> \n");
        correo.append(getCompanyiaSeguro());
        correo.append("\n </td> \n </tr>\n <tr> \n <td> \n ");
        correo.append("Empresa: \n </td>\n <td> \n");
        correo.append(getEmpresa());
        correo.append("\n </td> \n </tr>\n <tr> \n <td> \n ");
        correo.append("Teléfono de Contacto: \n </td>\n <td> \n");
        correo.append(" <a href=\"tel:");
        correo.append(getTelefonoContacto());
        correo.append("\" value=\"");
        correo.append(getTelefonoContacto());
        correo.append("\" target=\"_blank\">");
        correo.append(getTelefonoContacto());
        correo.append("</a>\n </td> \n </tr> \n <tr> \n <td> \n");
        correo.append("Teléfono de Contacto: \n </td>\n <td> \n");
        correo.append(" <a href=\"tel:");
        correo.append(getCelular());
        correo.append("\" value=\"");
        correo.append(getCelular());
        correo.append("\" target=\"_blank\">");
        correo.append(getCelular());
        correo.append("</a>\n </td> \n </tr> \n <tr> \n <td> \n");
        correo.append("Email: \n </td>\n <td> \n");
        correo.append(" <a href=\"mailto:");
        correo.append(getCorreoElectronico());
        correo.append("\" target=\"_blank\">");
        correo.append(getCorreoElectronico());
        correo.append("</a>\n </td> \n </tr>\n\n <tr> \n <th colspan=\"2\"> \n <b>");
        correo.append("Datos para el Reembolso </b> \n </th> \n </tr>\n <tr>\n <td> \n");
        correo.append(" Banco:\n </td>\n <td> \n");
        correo.append(getBanco());
        correo.append("\n </td> \n </tr> \n <tr>\n <td> \n ");
        correo.append("N° Cuenta Bancaria del Titular:\n </td>\n <td> \n");
        correo.append(getNumeroCuenta());
        correo.append("\n </td> \n </tr> \n <tr> \n <th colspan=\"2\"> \n <b>");
        correo.append("Datos de la Factura </b> \n </th> \n </tr>\n <tr> \n <td> \n");
        correo.append(" N° de la Factura:\n </td>\n <td> \n");
        correo.append(getNumeroFactura());
        correo.append("\n </td> \n </tr> \n <tr> \n <td> \n");
        correo.append("Monto de la Factura:\n </td>\n <td> \n");
        correo.append(String.format("%.2f", getMontoFactura()));
        correo.append(" BsF\n </td> \n </tr>\n\n </tbody></table>\n </div>");
        return correo.toString();
    }

    private String crearCorreo() {
        StringBuilder correo = new StringBuilder();
        correo.append("<div style=\"margin-bottom:22.5pt\"><p class=\"MsoNormal\" style=\"background:white\"><span lang=\"ES\" style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">Se ha generado una nueva solicitud de reembolso. <u></u><u></u></span></p>\n");
        correo.append("</div><div><table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"600\" style=\"width:450.0pt;background:#eeeeee\"><tbody><tr><td colspan=\"2\" style=\"padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\">\n");
        correo.append("<b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#1878be\">Datos Personales <u></u><u></u></span></b></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n");
        correo.append("<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Nombres y Apellidos del Titular: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n <p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getNombreCompletoTitular());
        correo.append("<u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n <p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Cédula de Identidad: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n <p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getCedulaTitular());
        correo.append("<u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Nombres y Apellidos del Paciente: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n <p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getNombreCompletoPaciente());
        correo.append("<u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Cédula de Identidad: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getCedulaPaciente());
        correo.append(" <u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Compañía de Seguro: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getCompanyiaSeguro());
        correo.append(" <u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\">\n" + "<p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Empresa: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getEmpresa());
        correo.append(" <u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Teléfono de Contacto: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getTelefonoContacto());
        correo.append(" <u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Teléfono de Contacto: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getCelular());
        correo.append(" <u></u><u></u></span></p></td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\">\n" + "<span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Email: <u></u><u></u></span></p></td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\"><a href=\"mailto:");
        correo.append(getCorreoElectronico());
        correo.append("\" target=\"_blank\">");
        correo.append(getCorreoElectronico());
        correo.append("</a> <u></u><u></u></span></p>\n" + "</td></tr><tr><td colspan=\"2\" style=\"padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#1878be\">");
        correo.append("Datos para el Reembolso <u></u><u></u></span></b></p>\n" + "</td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Banco: <u></u><u></u></span></p></td>\n" + "<td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getBanco());
        correo.append(" <u></u><u></u></span></p>\n" + "</td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("N° Cuenta Bancaria del Titular: <u></u><u></u></span></p>\n" + "</td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getNumeroCuenta());
        correo.append(" <u></u><u></u></span></p>\n" + "</td></tr><tr><td colspan=\"2\" style=\"padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#1878be\">Datos de la Factura <u></u><u></u></span></b></p>\n" + "</td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("N° de la Factura: <u></u><u></u></span></p>\n" + "</td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(getNumeroFactura());
        correo.append(" <u></u><u></u></span></p>\n" + "</td></tr><tr><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;\">");
        correo.append("Monto de la Factura: <u></u><u></u></span></p>\n" + "</td><td style=\"background:white;padding:2.25pt 2.25pt 2.25pt 2.25pt\"><p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#888888\">");
        correo.append(String.format("%.2f", getMontoFactura()));
        correo.append(" BsF <u></u><u></u></span></p>\n" + "</td></tr></tbody></table><p class=\"MsoNormal\"><u></u>&nbsp;<u></u></p></div>");
        return correo.toString();
    }
    
    public void destroyWorld() {
        System.out.println("Entro DestroyWorld");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();

        contexto.addMessage(null, new FacesMessage("Éxito", "Solicitud de Reembolso procesada exitosamente"));
        requestContext.update("growl");
        requestContext.update("form:growl");
        requestContext.update("form1:growl");
        requestContext.update("form1:form2:growl");
        requestContext.update("form1:form2:form3:growl");
        limpiarFormulario("panelPlanilla");
        
    }
}
