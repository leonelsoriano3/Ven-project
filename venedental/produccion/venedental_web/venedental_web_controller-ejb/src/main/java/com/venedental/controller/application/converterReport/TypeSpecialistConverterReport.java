/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.controller.application.converterReport;

import com.venedental.controller.converter.*;
import com.venedental.controller.view.TypeSpecialistViewBean;
import com.venedental.controller.viewReport.AuditViewBeanReport;
import com.venedental.dto.ReadSpecialtiesReportOutput;
import com.venedental.model.TypeSpecialist;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import services.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "typeSpecialistConverterReport", forClass = TypeSpecialist.class)
public class TypeSpecialistConverterReport implements Converter {
    
    private static final Logger log = CustomLogger.getGeneralLogger(TypeSpecialistConverterReport.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        
        AuditViewBeanReport typeSpecialistViewBean = (AuditViewBeanReport) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "auditViewBeanReport");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (ReadSpecialtiesReportOutput typeSpecialist : typeSpecialistViewBean.getListaTypeSpecialist()) {
                    if(typeSpecialist!= null){
                        if (typeSpecialist.getIdSpecialty() == numero) {
                            return typeSpecialist;
                        }
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Ciudad Invalida"));
            }
        }
        return null;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((ReadSpecialtiesReportOutput) value).getIdSpecialty());
        }
        
    }
    
}
