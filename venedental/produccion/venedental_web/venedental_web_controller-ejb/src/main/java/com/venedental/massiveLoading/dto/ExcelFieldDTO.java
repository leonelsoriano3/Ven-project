/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.dto;

import com.venedental.massiveLoading.base.enums.EFields;

/**
 *
 * @author akdesk01
 */
public class ExcelFieldDTO {

    private EFields field;
    private String letter;

    public ExcelFieldDTO(EFields field, String letter) {
        this.field = field;
        this.letter = letter;
    }

    public EFields getField() {
        return field;
    }

    public void setField(EFields field) {
        this.field = field;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

}
