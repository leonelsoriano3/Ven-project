/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.AddressesViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.ConfigBaremoViewBean;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.model.ScaleTreatments;
import com.venedental.services.ServiceScaleTreatments;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Desarrollo
 */
@ManagedBean(name = "scaleTreatmentsRequestBean")
@RequestScoped
public class ScaleTreatmentsRequestBean extends BaseBean implements Serializable {

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;
    
    @EJB
    private ServiceScaleTreatments serviceScaleTreatments;

    private final RequestContext context = RequestContext.getCurrentInstance();

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{configBaremoViewBean}")
    private ConfigBaremoViewBean configBaremoViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }    

    public ScaleTreatmentsFacadeLocal getScaleTreatmentsFacadeLocal() {
        return scaleTreatmentsFacadeLocal;
    }

    public void setScaleTreatmentsFacadeLocal(ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal) {
        this.scaleTreatmentsFacadeLocal = scaleTreatmentsFacadeLocal;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public ConfigBaremoViewBean getConfigBaremoViewBean() {
        return configBaremoViewBean;
    }

    public void setConfigBaremoViewBean(ConfigBaremoViewBean configBaremoViewBean) {
        this.configBaremoViewBean = configBaremoViewBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public void onCellEdit(CellEditEvent event) throws SQLException {
        if (!event.getOldValue().equals(event.getNewValue())) {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            FacesContext contexto = FacesContext.getCurrentInstance();
            DataTable table = (DataTable) event.getSource();
            ScaleTreatments scaleTreatment = (ScaleTreatments) table.getRowData();  
            if (scaleTreatment.getPrice() == null) {
                ScaleTreatments scaleTreatmentOld = this.scaleTreatmentsFacadeLocal.find(scaleTreatment.getId());
                scaleTreatment.setPrice(scaleTreatmentOld.getPrice());
                contexto.addMessage(null, new FacesMessage("Error", "Debe ingresar el precio"));
                requestContext.update("form:growBaremo");
                requestContext.update("form:configBaremo");
                                 
            } else {
                    ScaleTreatments scaleTreatmentsNew = new ScaleTreatments();
                    scaleTreatmentsNew.setIdTreatments(scaleTreatment.getIdTreatments());
                    scaleTreatmentsNew.setIdTypeScale(scaleTreatment.getIdTypeScale());
                    scaleTreatmentsNew.setPrice(scaleTreatment.getPrice());
                    Date dateScale = new Date();
                    scaleTreatmentsNew.setDateScale(dateScale);
                    ScaleTreatments scaleTreatCreated = this.scaleTreatmentsFacadeLocal.create(scaleTreatmentsNew);
                    this.serviceScaleTreatments.registerScaleProTre(scaleTreatCreated.getId());
                    contexto.addMessage(null, new FacesMessage("Éxito", "Cambios realizados satisfactoriamente"));
                    this.getConfigBaremoViewBean().filterListTreatments();
                    requestContext.update("form:growBaremo");
                    requestContext.update("form:configBaremo");                   

            }

        }
    }  
   
}
