///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.venedental.controller.angular;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import java.util.List;
//import java.util.Map;
//import javax.ws.rs.ApplicationPath;
//import javax.ws.rs.GET;
//import javax.ws.rs.core.Application;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.Response.ResponseBuilder;
//import org.jboss.resteasy.plugins.providers.multipart.InputPart;
//import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
//
///**
// *
// * @author akdesk01
// */
//@Path("/UploadFile")
//public class UploadFile extends Application {
//    
//   
//    
//    
//    
//    @Path("ggg")
//    @POST
//    @Consumes("multipart/form-data")
//    public String postWithPhoto(MultipartFormDataInput multipartFormDataInput) throws IOException {
//         // local variables
//        MultivaluedMap<String, String> multivaluedMap = null;
//        String fileName = null;
//        InputStream inputStream = null;
//        String uploadFilePath = null;
// 
//        try {
//            Map<String, List<InputPart>> map = multipartFormDataInput.getFormDataMap();
//            List<InputPart> lstInputPart = map.get("file");
// 
//            for(InputPart inputPart : lstInputPart){
// 
//                // get filename to be uploaded
//                multivaluedMap = inputPart.getHeaders();
//                fileName = getFileName(multivaluedMap);
// 
//                if(null != fileName && !"".equalsIgnoreCase(fileName)){
// 
//                    // write & upload file to UPLOAD_FILE_SERVER
//                    inputStream = inputPart.getBody(InputStream.class,null);
//                    uploadFilePath = writeToFileServer(inputStream, fileName);
//                    
//                    
// 
//                    // close the stream
//                    inputStream.close();
//                }
//            }
//        }
//        catch(IOException ioe){
//            ioe.printStackTrace();
//        }
//        finally{
//            // release resources, if any
//        }
//    
//        
//        return fileName;
//    }
//    
//    /**
//     *
//     * @param multivaluedMap
//     * @return
//     */
//    private String getFileName(MultivaluedMap<String, String> multivaluedMap) {
// 
//        
//        
//        String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");
// 
//        for (String filename : contentDisposition) {
// 
//            if ((filename.trim().startsWith("filename"))) {
//                String[] name = filename.split("=");
//                String exactFileName = name[1].trim().replaceAll("\"", "");
//                return exactFileName;
//            }
//        }
//        return "UnknownFile";
//    }
//    
//    
//    /**
//     *
//     * @param inputStream
//     * @param fileName
//     * @throws IOException
//     */
//    private String writeToFileServer(InputStream inputStream, String fileName) throws IOException {
// 
//        String UPLOAD_FILE_SERVER = "/home/ftp/recaudos/";
//        OutputStream outputStream = null;
//        String qualifiedUploadFilePath = UPLOAD_FILE_SERVER + fileName;
// 
//        try {
//            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
//            int read = 0;
//            byte[] bytes = new byte[1024];
//            while ((read = inputStream.read(bytes)) != -1) {
//                outputStream.write(bytes, 0, read);
//            }
//            outputStream.flush();
//        }
//        catch (IOException ioe) {
//            ioe.printStackTrace();
//        }
//        finally{
//            //release resource, if any
//            outputStream.close();
//        }
//        return qualifiedUploadFilePath;
//    }
//    
//    @Path("dw")
//    @GET
//    @Consumes("multipart/form-data")
//     public Response downloadDocFile() {
//// 
////        // set file (and path) to be download
//        File file = new File("/home/akdesk01/recaudos/hola2.png");
// 
//        ResponseBuilder responseBuilder = Response.ok((Object) file);
//        responseBuilder.header("Content-Disposition", "attachment; filename=\"hola.png\"");
//        return responseBuilder.build();
//    }
//}