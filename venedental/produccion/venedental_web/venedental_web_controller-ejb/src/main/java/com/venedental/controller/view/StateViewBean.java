package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.model.State;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "stateViewBean")
@ViewScoped
public class StateViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    public StateViewBean()  { 
        
    }
        
    private State newState; 
    
    private State newClinicState; 
    
    private State editState;  
    
    private State beforeEditState;

    public State getBeforeEditState() {
        return beforeEditState;
    }

    public void setBeforeEditState(State beforeEditState) throws CloneNotSupportedException {
        this.beforeEditState = (State) beforeEditState.clone();
    }

    public State getNewState() {
        return newState;
    }

    public void setNewState(State newState) {
        this.newState = newState;
    }

    public State getEditState() {
        return editState;
    }

    public void setEditState(State editState) {
        this.editState = editState;
    }

    public State getNewClinicState() {
        return newClinicState;
    }

    public void setNewClinicState(State newClinicState) {
        this.newClinicState = newClinicState;
    }
    
}
