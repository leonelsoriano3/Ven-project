package com.venedental.enums;

/**
 *
 * @author Edgar Mosquera
 */
public enum EstatusEspecialista {

    HABILITADO(1), INACTIVO(0), SUSPENDIDO(2);

    Integer valor;

    private EstatusEspecialista(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
