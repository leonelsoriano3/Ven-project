package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.facade.KeysPatientsFacadeLocal;
import com.venedental.facade.StatusAnalysisKeysFacadeLocal;
import com.venedental.model.KeysPatients;
import com.venedental.model.StatusAnalysisKeys;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;

@ManagedBean(name = "lazyListKeysPatientsViewBean")
@ViewScoped
public class LazyListKeysPatientsViewBean extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private LazyDataModel<KeysPatients> keysPatients;
    private List<KeysPatients> listPatients;
    private static boolean isRangeDate = false;
    private static boolean isSelectDate = false;
    private static Date startDate, endDate, date, maxDate;
    private static String dateValue;
    private StatusAnalysisKeys statusAnalysisKeys;

    @EJB
    private KeysPatientsFacadeLocal keysPatientsFacadeLocal;

    @EJB
    private StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal;

    public static boolean isIsRangeDate() {
        return isRangeDate;
    }

    public static void setIsRangeDate(boolean isRangeDate) {
        LazyListKeysPatientsViewBean.isRangeDate = isRangeDate;
    }

    public static String getDateValue() {
        return dateValue;
    }

    public static void setDateValue(String dateValue) {
        LazyListKeysPatientsViewBean.dateValue = dateValue;
    }

    public StatusAnalysisKeys getStatusAnalysisKeys() {
        return statusAnalysisKeys;
    }

    public void setStatusAnalysisKeys(StatusAnalysisKeys statusAnalysisKeys) {
        this.statusAnalysisKeys = statusAnalysisKeys;
    }
    
    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartDate() {
        return startDate;
    }

    public static boolean isIsSelectDate() {
        return isSelectDate;
    }

    public static void setIsSelectDate(boolean isSelectDate) {
        LazyListKeysPatientsViewBean.isSelectDate = isSelectDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public LazyDataModel<KeysPatients> getKeysPatients() {
        return keysPatients;
    }

    public void setKeysPatients(LazyDataModel<KeysPatients> keysPatients) {
        this.keysPatients = keysPatients;
    }

    public List<KeysPatients> getListPatients() {
        return listPatients;        
    }
    
    public LazyListKeysPatientsViewBean(){
        loadList();
        resetFechas();    
    }

    public void loadList() {
        keysPatients = new LazyDataModel<KeysPatients>() {
            @Override
            public List<KeysPatients> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                if (!filters.isEmpty() && !isRangeDate && !isSelectDate) {
                    isSelectDate = false;
                    boolean bool = true;
                    Iterator it = filters.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry e = (Map.Entry) it.next();
                        if (bool = true) {
                            bool = false;
                            if (e.getKey().toString().equalsIgnoreCase("number")) {
                                listPatients = filterByNumber(e.getValue().toString(), first, pageSize);
                                setRowCount(countByNumber());
                            } else if (e.getKey().toString().equalsIgnoreCase("specialistId.completeName")) {
                                listPatients = filterBySpecialistName(e.getValue().toString(), first, pageSize);
                                setRowCount(countByNumber());
                            } else if (e.getKey().toString().equalsIgnoreCase("specialistId.identityNumber")) {
                                listPatients = filterBySpecialistIdNumber(e.getValue().toString(), first, pageSize);
                                setRowCount(countByNumber());
                            } else if (e.getKey().toString().equalsIgnoreCase("patientsId.completeName")) {
                                listPatients = filterByPatientName(e.getValue().toString(), first, pageSize);
                                setRowCount(countByNumber());
                            } else if (e.getKey().toString().equalsIgnoreCase("patientsId.identityNumber")) {
                                listPatients = filterByPatientIdNumber(e.getValue().toString(), first, pageSize);
                                setRowCount(countByNumber());
                            } else if (e.getKey().toString().equalsIgnoreCase("formatDate(keys.applicationDate)")) {
                                if (e.getValue().toString().length() == 10 && e.getValue().toString().contains("/")) {
                                    listPatients = filterByApplicationDate(e.getValue().toString(), first, pageSize);
                                    setRowCount(countByNumber());
                                }
                            }
                        }
                        break;
                    }
                } else if (isRangeDate && !isSelectDate) {
                    listPatients = filterByRange(first, pageSize);
                    setRowCount(countByNumber());
                } else if (isSelectDate && !isRangeDate) {
                    listPatients = filterByApplicationDate(dateValue, first, pageSize);
                    setRowCount(countByNumber());
                } else {
                    listPatients = findKeysPatients(first, pageSize);
                    setRowCount(countByNumber());
                }
                return listPatients;
            }
        };
    }

    public List<KeysPatients> findKeysPatients(int first, int pageSize) {
        this.setStatusAnalysisKeys(this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.GENERADO.getValor()));          
        return this.keysPatientsFacadeLocal.findByPage(first, pageSize, this.getStatusAnalysisKeys());
    }

    public static boolean isIsDate() {
        return isRangeDate;
    }

    public static void setIsDate(boolean isRangeDate) {
        LazyListKeysPatientsViewBean.isRangeDate = isRangeDate;
    }

    public List<KeysPatients> filterByNumber(String number, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterByNumber(number, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public List<KeysPatients> filterBySpecialistIdNumber(String identityNumber, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterBySpecialistIdNumber(identityNumber, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public List<KeysPatients> filterBySpecialistName(String name, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterBySpecialistName(name, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public List<KeysPatients> filterByPatientIdNumber(String identityNumber, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterByPatientIdNumber(identityNumber, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public List<KeysPatients> filterByPatientName(String name, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterByPatientName(name, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public List<KeysPatients> filterByApplicationDate(String applicationDate, int first, int pageSize) {
        List<KeysPatients> lista = this.keysPatientsFacadeLocal.filterByApplicationDate(applicationDate, first, pageSize, this.getStatusAnalysisKeys());
        return lista;
    }

    public int countByNumber() {
        int count = this.keysPatientsFacadeLocal.reciveCount();
        return count;
    }

    public Date formatDate(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fecha = sdf.format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;
    }

    public List<KeysPatients> filterByRange(int first, int pageSize) {
        try {
            if (endDate == null || startDate == null) {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el rango de fechas"));
                RequestContext.getCurrentInstance().update("form:lazyKeyPattientsGrow");
            } else {
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                String fechaStr = formatter.format(endDate);

                Date date = formatter.parse(fechaStr);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
                Date fechaFinalTransformada = cal.getTime();

                String fechaInicialStr = this.getMyFormattedDateStart(startDate);
                String fechaFinal = this.getMyFormattedDateStart(endDate);

                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date fechaInicial = sdf2.parse(fechaInicialStr);
                if (fechaInicial != null && fechaFinal != null) {
                    listPatients = this.keysPatientsFacadeLocal.filterByApplicationDateRange(fechaInicial, fechaFinalTransformada, first, pageSize, this.getStatusAnalysisKeys());
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(LazyListKeysPatientsViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listPatients;

    }

    public void resetFechas() {
        this.setStartDate(null);
        this.setEndDate(null);
        this.setDate(null);
        isRangeDate = false;
        isSelectDate = false;
        dateValue = null;
        RequestContext.getCurrentInstance().update("form:keysTable");
    }

    public String getMyFormattedDateStart(Date fecha) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha);
    }

    public void filtrarFechas() {
        if (startDate == null || endDate == null) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el rango de fechas"));
            RequestContext.getCurrentInstance().update("form:lazyKeyPattientsGrow");
        } else {
            isRangeDate = true;
            isSelectDate = false;
            this.setDate(null);
            RequestContext.getCurrentInstance().update("form:keysTable");
        }
    }

    public void selectDate() throws ParseException {
        if (date != null) {
            isSelectDate = true;
            this.setStartDate(null);
            this.setEndDate(null);
            isRangeDate = false;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            dateValue = sdf.format(date);
            RequestContext.getCurrentInstance().update("form:keysTable");
        } else {
            isSelectDate = false;
            dateValue = null;
            RequestContext.getCurrentInstance().update("form:keysTable");

        }
    }
}
