package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum EstatusPlansTreatmentsKeys {

   GENERADO(1), EJECUTADO(2), RECIBIDO(3), AUDITADO(4), POR_FACTURAR(6), RECHAZADO(7), ELIMINADO(8), EN_EVALUACION(9), POR_ABONAR(12), ABONADO(13);

    Integer valor;

    private EstatusPlansTreatmentsKeys(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
