package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.EstadoCivil;
import com.venedental.enums.EstatusEspecialista;
import com.venedental.enums.Sexo;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "miscelaneosViewBean")
@SessionScoped
public class MiscelaneosViewBean extends BaseBean {

    private static final long serialVersionUID = -7434469570770537682L;

    private EstadoCivil[] listaEstadoCivil;

    private Sexo[] listaSexo;
    
    private EstatusEspecialista[] listaEstatusEspecialista;
    
    private Date horario1;
    
    private Date horario2;
    
    private Date horario3;
    
    private Date horario4;
    
    private Date horario5;
    
    private Date horario6;

    public Date getHorario1() {
        return horario1;
    }

    public void setHorario1(Date horario1) {
        this.horario1 = horario1;
    }

    public Date getHorario2() {
        return horario2;
    }

    public void setHorario2(Date horario2) {
        this.horario2 = horario2;
    }

    public Date getHorario3() {
        return horario3;
    }

    public void setHorario3(Date horario3) {
        this.horario3 = horario3;
    }

    public Date getHorario4() {
        return horario4;
    }

    public void setHorario4(Date horario4) {
        this.horario4 = horario4;
    }

    public Date getHorario5() {
        return horario5;
    }

    public void setHorario5(Date horario5) {
        this.horario5 = horario5;
    }

    public Date getHorario6() {
        return horario6;
    }

    public void setHorario6(Date horario6) {
        this.horario6 = horario6;
    }

    public EstadoCivil[] getListaEstadoCivil() {
        return EstadoCivil.values();
    }

    public Sexo[] getListaSexo() {
        return Sexo.values();
    }

    public EstatusEspecialista[] getListaEstatusEspecialista() {
        return EstatusEspecialista.values();
    }

   
    

}
