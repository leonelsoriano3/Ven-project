/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;


import com.venedental.controller.view.PropertiesPlansTreatmentsKeyViewBean;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "propertiesPlansTreatmentsKeyConverter", forClass = PropertiesPlansTreatmentsKey.class)
public class PropertiesPlansTreatmentsKeyConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean = (PropertiesPlansTreatmentsKeyViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "propertiesPlansTreatmentsKeyViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                
                for (PropertiesPlansTreatmentsKey plansTreatments : propertiesPlansTreatmentsKeyViewBean.getListPropertiesPlansTreatmentsKey()) {
                    if (plansTreatments.getId() == numero) {
                        return plansTreatments;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "PropertiesPlansTreatmentsKey Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((PropertiesPlansTreatmentsKey) value).getId());
        }

    }

}
