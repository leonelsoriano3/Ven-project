/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum Reemplazo {

    AAcento("Á", "A"), EAcento("É", "E"), IAcento("Í", "I"),
    OAcento("Ó", "O"), UAcento("Ú", "U"), UDieresis("Ü", "U"),
    Apostrofe("'", "");

    String valor;
    String valorReemplazo;

    private Reemplazo(String valor, String valorReemplazo) {
        this.valor = valor;
        this.valorReemplazo = valorReemplazo;
    }

    public String getValor() {
        return valor;
    }

    public String getValorReemplazo() {
        return valorReemplazo;
    }

}
