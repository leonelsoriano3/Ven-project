/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.enums;

/**
 *
 * @author Edgar Mosquera
 */
public enum Sexo {

    FEMENINO("f"), MASCULINO("m");
    
    String valor;
    private Sexo(String v){
        valor = v;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    

}
