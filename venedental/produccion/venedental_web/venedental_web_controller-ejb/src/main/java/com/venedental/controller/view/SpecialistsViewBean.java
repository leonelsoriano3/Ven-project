package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.enums.TypeScale;
import com.venedental.facade.ClinicsFacadeLocal;
import com.venedental.facade.MenuFacadeLocal;
import com.venedental.facade.MenuRolFacadeLocal;
import com.venedental.facade.MenuRolOperationFacadeLocal;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.facade.ScaleTreatmentsSpecialistFacadeLocal;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.TreatmentsFacadeLocal;
import com.venedental.model.Clinics;
import com.venedental.model.PhoneClinic;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.SpecialistJob;
import com.venedental.model.Specialists;
import com.venedental.model.TypeAttention;
import com.venedental.model.security.Menu;
import com.venedental.model.security.MenuRol;
import com.venedental.model.security.MenuRolOperation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import model.lazy.LazySpecialists;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

@ManagedBean(name = "specialistsViewBean")
@ViewScoped
public class SpecialistsViewBean extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();

    @EJB
    private SpecialistsFacadeLocal specialistsFacadeLocal;

    @EJB
    private TreatmentsFacadeLocal treatmentsFacadeLocal;

    @EJB
    private ClinicsFacadeLocal clinicsFacadeLocal;

    @EJB
    private MenuRolFacadeLocal menuRolFacadeLocal;

    @EJB
    private MenuFacadeLocal menuFacadeLocal;

    @EJB
    private MenuRolOperationFacadeLocal menuRolOperationFacadeLocal;

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;
    
    @EJB
    private ScaleTreatmentsSpecialistFacadeLocal scaleTreatmentsSpecialistFacadeLocal;

    private static final long serialVersionUID = -5547104134772667835L;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;

    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;

    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    @ManagedProperty(value = "#{typeAttentionViewBean}")
    private TypeAttentionViewBean typeAttentionViewBean;

    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;

    @ManagedProperty(value = "#{specialistJobViewBean}")
    private SpecialistJobViewBean specialistJobViewBean;

    @ManagedProperty(value = "#{clinicsViewBean}")
    private ClinicsViewBean clinicsViewBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{scaleViewBean}")
    private ScaleViewBean scaleViewBean;

    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;

    @ManagedProperty(value = "#{plansPatientsViewBean}")
    private PlansPatientsViewBean plansPatientsViewBean;

    @ManagedProperty(value = "#{treatmentsViewBean}")
    private TreatmentsViewBean treatmentsViewBean;

    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;
    
    @ManagedProperty(value = "#{scaleTreatmentsSpecialistViewBean}")
    private ScaleTreatmentsSpecialistViewBean scaleTreatmentsSpecialistViewBean;


    private Boolean operationCreate, operationRead, operationUpdate, operationDelete;

    private String nameCompleto = "";

    private Specialists selectedSpecialists;

    private Specialists beforeEditSpecialists;

    private Specialists specialistsAutocomplete;

    private Specialists beforeSpecialists;

    private Specialists editSpecialistsObj;

    private List<Specialists> listaSpecialists;

    private LazyDataModel<Specialists> listaLazySpecialists;

    private List<Specialists> listaSpecialistsFiltrada;

    private List<Specialists> auxListaSpecialists;

    private List<Specialists> listSpecialistFiltered;


    private List<SpecialistJob> listaSpecialistJob;

    private String[] newSpecialists;

    private int indiceSpecialists;

    private String[] editSpecialists;

    private Specialists newSpecialistsCreated;

    private Specialists newSpecialistsObj;

    private Specialists viewSpecialists;

    private String descripcionSexo;

    private String descripcionStatus;

    private String tipoRifEdit;

    private String mascaraRifEdit;

    private String filtroNombre;

    private String filtroEspecialidad;

    private String filtroEstado;

    private String filtroCiudad;

    private String filtroClinica;

    private String filtroEstatus;

    private boolean visible;

    private boolean encontrado;

    private String specialistSelected;

    private Specialists specialistDelete;

    private String nombreCompleto = "";

    private String valor = "";

     private boolean scaleSpecific;


    public SpecialistsViewBean() {
        this.newSpecialists = new String[17];
        this.editSpecialists = new String[18];
        this.newSpecialistsObj = new Specialists();
        this.editSpecialistsObj = new Specialists();
        this.beforeSpecialists = new Specialists();
        this.viewSpecialists = new Specialists();
        this.tipoRifEdit = "V";
        this.mascaraRifEdit = "V999999999";
        this.filtroNombre = "";
        this.filtroEspecialidad = "";
        this.filtroEstado = "";
        this.filtroClinica = "";
        this.filtroCiudad = "";
        this.filtroEstatus = "";
        this.visible = false;
    }

    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.getListaSpecialists().addAll(this.specialistsFacadeLocal.findAll());
        llenarListas2(this.listaSpecialists);
        this.operationSecurity();
    }

    public ScaleViewBean getScaleViewBean() {
        return scaleViewBean;
    }

    public void setScaleViewBean(ScaleViewBean scaleViewBean) {
        this.scaleViewBean = scaleViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public ClinicsViewBean getClinicsViewBean() {
        return clinicsViewBean;
    }

    public void setClinicsViewBean(ClinicsViewBean clinicsViewBean) {
        this.clinicsViewBean = clinicsViewBean;
    }

    public SpecialistJobViewBean getSpecialistJobViewBean() {
        return specialistJobViewBean;
    }

    public void setSpecialistJobViewBean(SpecialistJobViewBean specialistJobViewBean) {
        this.specialistJobViewBean = specialistJobViewBean;
    }

    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }

    public ClinicsFacadeLocal getClinicsFacadeLocal() {
        return clinicsFacadeLocal;
    }

    public void setClinicsFacadeLocal(ClinicsFacadeLocal clinicsFacadeLocal) {
        this.clinicsFacadeLocal = clinicsFacadeLocal;
    }

    public MenuRolFacadeLocal getMenuRolFacadeLocal() {
        return menuRolFacadeLocal;
    }

    public void setMenuRolFacadeLocal(MenuRolFacadeLocal menuRolFacadeLocal) {
        this.menuRolFacadeLocal = menuRolFacadeLocal;
    }

    public MenuFacadeLocal getMenuFacadeLocal() {
        return menuFacadeLocal;
    }

    public void setMenuFacadeLocal(MenuFacadeLocal menuFacadeLocal) {
        this.menuFacadeLocal = menuFacadeLocal;
    }

    public MenuRolOperationFacadeLocal getMenuRolOperationFacadeLocal() {
        return menuRolOperationFacadeLocal;
    }

    public void setMenuRolOperationFacadeLocal(MenuRolOperationFacadeLocal menuRolOperationFacadeLocal) {
        this.menuRolOperationFacadeLocal = menuRolOperationFacadeLocal;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public Boolean getOperationCreate() {
        return operationCreate;
    }

    public void setOperationCreate(Boolean operationCreate) {
        this.operationCreate = operationCreate;
    }

    public Boolean getOperationRead() {
        return operationRead;
    }

    public void setOperationRead(Boolean operationRead) {
        this.operationRead = operationRead;
    }

    public Boolean getOperationUpdate() {
        return operationUpdate;
    }

    public void setOperationUpdate(Boolean operationUpdate) {
        this.operationUpdate = operationUpdate;
    }

    public Boolean getOperationDelete() {
        return operationDelete;
    }

    public void setOperationDelete(Boolean operationDelete) {
        this.operationDelete = operationDelete;
    }
    
    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public TypeAttentionViewBean getTypeAttentionViewBean() {
        return typeAttentionViewBean;
    }

    public void setTypeAttentionViewBean(TypeAttentionViewBean typeAttentionViewBean) {
        this.typeAttentionViewBean = typeAttentionViewBean;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public Specialists getSpecialistsAutocomplete() {
        return specialistsAutocomplete;
    }

    public void setSpecialistsAutocomplete(Specialists specialistsAutocomplete) {
        this.specialistsAutocomplete = specialistsAutocomplete;
    }

    public TreatmentsViewBean getTreatmentsViewBean() {
        return treatmentsViewBean;
    }

    public void setTreatmentsViewBean(TreatmentsViewBean treatmentsViewBean) {
        this.treatmentsViewBean = treatmentsViewBean;
    }
    
    public List<Specialists> getListSpecialistFiltered() {

        if (this.listSpecialistFiltered == null) {
            this.listSpecialistFiltered = new ArrayList<>();
        }
        
        return listSpecialistFiltered;
    }

    public void setListSpecialistFiltered(List<Specialists> listSpecialistFiltered) {
        this.listSpecialistFiltered = listSpecialistFiltered;
    }


    public List<Specialists> completeName(String query) {
        this.getListaSpecialists().clear();
        this.getListaSpecialists().addAll(this.specialistsFacadeLocal.findByStatus(1));
        List<Specialists> allSpecialists = this.getListaSpecialists();
        List<Specialists> filteredSpecialists = new ArrayList<Specialists>();

        for (int i = 0; i < allSpecialists.size(); i++) {
            Specialists specialist = allSpecialists.get(i);
            if (specialist.getIdentityNumber().toString().toLowerCase().startsWith(query)) {
                filteredSpecialists.add(specialist);
            }
        }
        if (filteredSpecialists.isEmpty() || filteredSpecialists == null) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Especialista se encuentra inactivo o suspendido"));

        }

        return filteredSpecialists;
    }
    
     public List<Specialists> filterSpecialist(String query) {        
        List<Specialists> filteredSpecialists = this.getSpecialistsFacadeLocal().findByIdentityNumberOrName(query);
        return filteredSpecialists;
    }

    public void llenarListas2(List<Specialists> lista) {

        List<String> listMedicalOffice = new ArrayList<>();
        List<String> listCities = new ArrayList<>();
        List<String> listStates = new ArrayList<>();

        for (Specialists auxSpecialist : lista) {
            int estatus = 0;
            String descEstatus = "Inactivo";
            if (auxSpecialist.getStatus() != null) {
                estatus = auxSpecialist.getStatus();

                if (estatus == 1) {
                    descEstatus = "Habilitado";
                } else if (estatus == 2) {
                    descEstatus = "Suspendido";
                } else {
                    descEstatus = "Inactivo";
                }
            }
            auxSpecialist.setDescStatus(descEstatus);

            for (SpecialistJob auxSpecialistJob : auxSpecialist.getSpecialistJobList()) {
                if (auxSpecialistJob.getMedicalOfficeId() != null) {
                listMedicalOffice.add(auxSpecialistJob.getMedicalOfficeId().getName());
                    if (auxSpecialistJob.getMedicalOfficeId().getAddressId() != null) {
                        if (auxSpecialistJob.getMedicalOfficeId().getAddressId().getCityId() != null) {
                listCities.add(auxSpecialistJob.getMedicalOfficeId().getAddressId().getCityId().getName());
                listStates.add(auxSpecialistJob.getMedicalOfficeId().getAddressId().getCityId().getStateId().getNombre());
            }
                    }
                }
            }

            auxSpecialist.setNameMedicalOffice(listMedicalOffice);
            auxSpecialist.setCities(listCities);
            auxSpecialist.setStates(listStates);
            listMedicalOffice = new ArrayList<>();
            listCities = new ArrayList<>();
            listStates = new ArrayList<>();

        }

        this.setListaSpecialistsFiltrada(this.getListaSpecialists());
    }

    public Specialists getSpecialistDelete() {
        return specialistDelete;
    }

    public void setSpecialistDelete(Specialists specialistDelete) {
        this.specialistDelete = specialistDelete;
    }

    public int getIndiceSpecialists() {
        return indiceSpecialists;
    }

    public void setIndiceSpecialists(int indiceSpecialists) {
        this.indiceSpecialists = indiceSpecialists;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansPatientsViewBean getPlansPatientsViewBean() {
        return plansPatientsViewBean;
    }

    public void setPlansPatientsViewBean(PlansPatientsViewBean plansPatientsViewBean) {
        this.plansPatientsViewBean = plansPatientsViewBean;
    }

    public List<Specialists> getAuxListaSpecialists() {
        return auxListaSpecialists;
    }

    public void setAuxListaSpecialists(List<Specialists> auxListaSpecialists) {
        this.auxListaSpecialists = auxListaSpecialists;
    }

    public List<SpecialistJob> getListaSpecialistJob() {
        return listaSpecialistJob;
    }

    public void setListaSpecialistJob(List<SpecialistJob> listaSpecialistJob) {
        this.listaSpecialistJob = listaSpecialistJob;
    }

    public Specialists getBeforeSpecialists() {
        return beforeSpecialists;
    }

    public void setBeforeSpecialists(Specialists beforeSpecialists) {
        this.beforeSpecialists = beforeSpecialists;
    }

    public Specialists getEditSpecialistsObj() {
        return editSpecialistsObj;
    }

    public void setEditSpecialistsObj(Specialists editSpecialistsObj) {

        this.setBeforeSpecialists(editSpecialistsObj);
        this.editSpecialists[0] = editSpecialistsObj.getId().toString();
        this.editSpecialists[1] = editSpecialistsObj.getFirstName();
        this.editSpecialists[2] = editSpecialistsObj.getSecondName();
        this.editSpecialists[3] = editSpecialistsObj.getLastname();
        this.editSpecialists[4] = editSpecialistsObj.getSecondLastname();
        this.editSpecialists[5] = editSpecialistsObj.getCompleteName();
        if (editSpecialistsObj.getIdentityNumber() != null) {
            this.editSpecialists[6] = editSpecialistsObj.getIdentityNumber().toString();
        }
        this.editSpecialists[7] = editSpecialistsObj.getRif();
        this.editSpecialists[8] = editSpecialistsObj.getPhone();
        this.editSpecialists[9] = editSpecialistsObj.getCellphone();
        this.editSpecialists[10] = editSpecialistsObj.getMsds();
        this.editSpecialists[11] = editSpecialistsObj.getMedicalAsociation();
        this.editSpecialists[12] = editSpecialistsObj.getEmail();
        this.editSpecialists[13] = editSpecialistsObj.getSex();
        this.editSpecialists[14] = editSpecialistsObj.getCivilStatus();
        this.editSpecialists[16] = editSpecialistsObj.getObservation();
        if (editSpecialistsObj.getStatus() != null) {
            this.editSpecialists[17] = editSpecialistsObj.getStatus().toString();
            this.setDescripcionStatus(editSpecialistsObj.getStatus());
        }
        if (editSpecialistsObj.getAddressId() != null) {
            this.getAddressesViewBean().setEditAddresses(editSpecialistsObj.getAddressId());
            if (editSpecialistsObj.getAddressId().getCityId() != null) {
                this.getCitiesViewBean().setEditCities(editSpecialistsObj.getAddressId().getCityId());
                this.getStateViewBean().setEditState(editSpecialistsObj.getAddressId().getCityId().getStateId());
            }
        }
        this.getClinicsViewBean().setListaClinicsSpecialist(editSpecialistsObj.getSpecialistJobList());
        this.getSpecialistJobViewBean().setEditListSpecialistJob(editSpecialistsObj.getSpecialistJobList());
        this.getTypeSpecialistViewBean().setEditTypeSpecialist(editSpecialistsObj.getTypeSpecialistid());

        this.getSpecialityViewBean().setEditListaSpecialists(editSpecialistsObj.getSpecialityList());
        this.getSpecialityViewBean().filtrarSpeciality();
        this.getScaleViewBean().setEditScale(editSpecialistsObj.getScale());
        this.setScaleSpecific(false);
       
        this.editSpecialistsObj = editSpecialistsObj;
         if (editSpecialistsObj.getScale() != null && editSpecialistsObj.getScale().getId().equals(TypeScale.Especifico.getValor())) {

           this.setScaleSpecific(true);
           this.findScaleTreatmentsSpecialist();
           this.fillTreatmentsSpecialist();
           

    }
    }

    public void findScaleTreatmentsSpecialist(){

    this.getScaleTreatmentsSpecialistViewBean().setListaScaleTreatmentsSpecialist(this.scaleTreatmentsSpecialistFacadeLocal.findBySpecialist(this.getEditSpecialistsObj().getId()));
    RequestContext requestContext = RequestContext.getCurrentInstance();
    requestContext.update("form:configBaremoEspecifico"); 
    
    }
    
    public void updateTypeScale() {
        this.getScaleViewBean().getEditScale();
        if (this.getScaleViewBean().getEditScale() != null && this.getScaleViewBean().getEditScale().getId().equals(TypeScale.General.getValor())) {

            this.setScaleSpecific(false);
        }
        else if (this.getScaleViewBean().getEditScale() != null && this.getScaleViewBean().getEditScale().getId().equals(TypeScale.Especifico.getValor())) {

            this.setScaleSpecific(true);
            fillTreatmentsSpecialist();
        }
         RequestContext requestContext = RequestContext.getCurrentInstance();
         requestContext.update("form:baremoEdit"); 
         requestContext.update("form:configBaremoEspecifico"); 
         requestContext.update("idListConfigBaremoEspecifico"); 
         
         
        
       
    }
    public void fillTreatmentsSpecialist(){
 
        this.getTreatmentsViewBean().setListaTreatmentsSpecialist(this.treatmentsFacadeLocal.findByTypeSpecialistanSpecialialist(this.getEditSpecialistsObj().getId(),this.getEditSpecialistsObj().getTypeSpecialistid().getId())); 
         RequestContext requestContext = RequestContext.getCurrentInstance();
         requestContext.update("form:configBaremoEspecifico"); 
         requestContext.update("form:configScaleSpecialist"); 
         requestContext.update("form:treatmentScaleEdit"); 
         requestContext.update("form:inputPrice"); 

    }

    public Specialists getNewSpecialistsObj() {
        return newSpecialistsObj;
    }

    public void setNewSpecialistsObj(Specialists newSpecialistsObj) {
        this.newSpecialistsObj = newSpecialistsObj;
    }

    public SpecialistsFacadeLocal getSpecialistsFacadeLocal() {
        return specialistsFacadeLocal;
    }

    public void setSpecialistsFacadeLocal(SpecialistsFacadeLocal specialistsFacadeLocal) {
        this.specialistsFacadeLocal = specialistsFacadeLocal;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public LazyDataModel<Specialists> getListaLazySpecialists() {
        if (this.listaSpecialists == null) {
            this.listaSpecialists = this.specialistsFacadeLocal.findAll();
            Collections.sort(this.listaSpecialists);
            this.listaLazySpecialists = new LazySpecialists(this.listaSpecialists);
        }
        return listaLazySpecialists;
    }

    public void setListaLazySpecialists(LazyDataModel<Specialists> listaLazySpecialists) {
        this.listaLazySpecialists = listaLazySpecialists;
    }

    public String getFiltroNombre() {
        return filtroNombre;
    }

    public void setFiltroNombre(String filtroNombre) {
        this.filtroNombre = filtroNombre;
    }

    public String getFiltroEspecialidad() {
        return filtroEspecialidad;
    }

    public void setFiltroEspecialidad(String filtroEspecialidad) {
        this.filtroEspecialidad = filtroEspecialidad;
    }

    public String getFiltroEstado() {
        return filtroEstado;
    }

    public void setFiltroEstado(String filtroEstado) {
        this.filtroEstado = filtroEstado;
    }

    public String getFiltroCiudad() {
        return filtroCiudad;
    }

    public void setFiltroCiudad(String filtroCiudad) {
        this.filtroCiudad = filtroCiudad;
    }

    public String getFiltroClinica() {
        return filtroClinica;
    }

    public void setFiltroClinica(String filtroClinica) {
        this.filtroClinica = filtroClinica;
    }

    public String getFiltroEstatus() {
        return filtroEstatus;
    }

    public void setFiltroEstatus(String filtroEstatus) {
        this.filtroEstatus = filtroEstatus;
    }

    public String getMascaraRifEdit() {
        return mascaraRifEdit;
    }

    public void setMascaraRifEdit(String mascaraRifEdit) {
        this.mascaraRifEdit = mascaraRifEdit;
    }

    public String getTipoRifEdit() {
        return tipoRifEdit;
    }

    public void setTipoRifEdit(String tipoRifEdit) {
        this.tipoRifEdit = tipoRifEdit;
    }

    public Specialists getBeforeEditSpecialists() {
        return beforeEditSpecialists;
    }

    public void setBeforeEditSpecialists(Specialists beforeEditSpecialists) throws CloneNotSupportedException {
        this.beforeEditSpecialists = (Specialists) beforeEditSpecialists.clone();
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public String getDescripcionStatus() {
        return descripcionStatus;
    }

    public String getSpecialistSelected() {
        return specialistSelected;
    }

    public void setSpecialistSelected(String specialistSelected) {
        this.specialistSelected = specialistSelected;
    }

    public void setDescripcionStatus(Integer Status) {
        if (Status == null) {
            return;
        }
        if (Status == 1) {
            this.descripcionStatus = "Habilitado";
        } else if (Status == 2) {
            this.descripcionStatus = "Suspendido";
        } else {
            this.descripcionStatus = "Inactivo";
        }
    }

    public void selectedSpecialist(Specialists specialist, int indice) {

        this.setSpecialistSelected(specialist.getCompleteName());
        this.specialistDelete = specialist;
        this.indiceSpecialists = indice;

    }

    public String getDescripcionSexo() {
        return descripcionSexo;
    }

    public void setDescripcionSexo(String descripcionSexo) {
        switch (descripcionSexo) {
            case "m":
                this.descripcionSexo = "Masculino";
                break;
            case "f":
                this.descripcionSexo = "Femenino";
                break;
            default:
                this.descripcionSexo = "";
                break;
        }

    }

    public String[] getNewSpecialists() {
        return newSpecialists;
    }

    public void setNewSpecialists(String[] newSpecialists) {
        this.newSpecialists = newSpecialists;
    }

    public String[] getEditSpecialists() {
        return editSpecialists;
    }

    public void setEditSpecialists(String[] editSpecialists) {
        this.editSpecialists = editSpecialists;
    }

    public Specialists getNewSpecialistsCreated() {
        return newSpecialistsCreated;
    }

    public void setNewSpecialistsCreated(Specialists newSpecialistsCreated) {
        this.newSpecialistsCreated = newSpecialistsCreated;
    }

    public Specialists getSelectedSpecialists() {
        return selectedSpecialists;
    }

    public Specialists getViewSpecialists() {
        return viewSpecialists;
    }

    public void setViewSpecialists(Specialists viewSpecialists) {
        if (viewSpecialists.getSex() != null) {
            this.setDescripcionSexo(viewSpecialists.getSex());
        }
        if (viewSpecialists.getStatus() != null) {
            this.setDescripcionStatus(viewSpecialists.getStatus());
        }
        this.viewSpecialists = viewSpecialists;
    }

    public void setSelectedSpecialists(Specialists selectedSpecialists) throws CloneNotSupportedException {
        this.selectedSpecialists = selectedSpecialists;

    }

    public List<Specialists> getListaSpecialists() {
        if (this.listaSpecialists == null) {
            this.listaSpecialists = new ArrayList<>();
        }
        return listaSpecialists;
    }

    public void setListaSpecialists(List<Specialists> listaSpecialists) {
        this.listaSpecialists = listaSpecialists;
    }

    public List<Specialists> getListaSpecialistsFiltrada() {
        if (this.listaSpecialistsFiltrada == null) {
            this.listaSpecialistsFiltrada = new ArrayList<>();
        }
        return listaSpecialistsFiltrada;
    }

    public void setListaSpecialistsFiltrada(List<Specialists> listaSpecialistsFiltrada) {
        this.listaSpecialistsFiltrada = listaSpecialistsFiltrada;
    }

    public void deleteEditSpecialistJob(int item) {
        this.getSpecialistJobViewBean().getEditListSpecialistJob().remove(item);
    }

    public void makeCompleteName() {

        System.out.println(this.getNewSpecialists()[1]);

        if (this.getNewSpecialists()[1] != null && this.getNewSpecialists()[1].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + this.getNewSpecialists()[1].trim() + " ";
        }
        if (this.getNewSpecialists()[2] != null && this.getNewSpecialists()[2].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + this.getNewSpecialists()[2].trim() + " ";
        }
        if (this.getNewSpecialists()[3] != null && this.getNewSpecialists()[3].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + this.getNewSpecialists()[3].trim() + " ";
        }
        if (this.getNewSpecialists()[4] != null && this.getNewSpecialists()[4].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + this.getNewSpecialists()[4].trim() + " ";
        }

        this.getNewSpecialists()[5] = nameCompleto;
        nameCompleto = "";

    }

    public void makeCompleteNameEdit(String[] especialista) {

        String nameCompleto = "";
        String spec = this.editSpecialists[1];

        if (this.editSpecialists[1] != null && this.editSpecialists[1].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + especialista[1].trim() + " ";
        }
        if (especialista[2] != null && this.editSpecialists[2].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + especialista[2].trim() + " ";
        }
        if (especialista[3] != null && this.editSpecialists[3].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + especialista[3].trim() + " ";
        }
        if (especialista[4] != null && this.editSpecialists[4].trim().compareTo("") != 0) {

            nameCompleto = nameCompleto + especialista[4].trim() + " ";
        }
        if (nameCompleto != null && nameCompleto.trim().compareTo("") != 0) {
            this.getEditSpecialists()[5] = nameCompleto;
        }

    }

    public void buscarIdentityNumber(String especialista, String id) {
        boolean encontrado = false;
        this.editSpecialists[5] = "";
        this.getSpecialityViewBean().getEditListaSpecialists().clear();
        if (especialista != null && especialista.compareTo("") != 0) {
            int cedula = Integer.parseInt(especialista);
            for (Specialists specialist : getListaSpecialists()) {
                if (specialist.getIdentityNumber() != null && specialist.getIdentityNumber() == cedula) {
                    this.editSpecialists[5] = specialist.getCompleteName();
                    this.editSpecialists[1] = specialist.getCompleteName();
                    this.editSpecialists[0] = specialist.getId().toString();

                    this.setEditSpecialistsObj(specialist);
                    this.getTypeSpecialistViewBean().setEditTypeSpecialist(specialist.getTypeSpecialistid());
                    this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(specialist.getTypeSpecialistid().getName());
                    this.getSpecialityViewBean().setEditListaSpecialists(this.getEditSpecialistsObj().getSpecialityList());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                this.editSpecialists[6] = "";
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado especialistas con ese número de cédula"));
            }
        }

    }

    public void cerrarDialogoCreate(String dialogo) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(false);
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
    }

    
 
    
    
    public void limpiarObjetosEdit() {
        this.getPhoneClinicViewBean().setEditListPhoneClinic(new ArrayList<PhoneClinic>());
        if (this.getClinicsViewBean().getAddClinics() != null) {
            this.getClinicsViewBean().getAddClinics().getAddressId().setAddress(new String());
            
            if (this.getClinicsViewBean().getAddClinics().getAddressId().getCityId() != null) {
                this.getClinicsViewBean().getAddClinics().getAddressId().getCityId().setName(new String());
            }
            if (this.getClinicsViewBean().getAddClinics().getAddressId().getCityId().getStateId() != null) {
                this.getClinicsViewBean().getAddClinics().getAddressId().getCityId().getStateId().setNombre(new String());
            }
            this.getClinicsViewBean().setAddClinics(new Clinics());
        }
    }

    public void limpiarStateEdit() {
        this.getStateViewBean().setEditState(this.getStateViewBean().getBeforeEditState());
    }

    public void limpiarObjetosCreate() {
        this.getComponenteSessionBean().setCamposRequeridosCreate(false);
    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }

    public void buscarSpecialistByIdentityNumber(Specialists especialista, String id) {
        boolean encontrado = false;
        this.editSpecialists[5] = "";
        this.getSpecialityViewBean().getEditListaSpecialists().clear();
        this.getPatientsViewBean().setPatientsAutocomplete(null);
        this.getPatientsViewBean().getEditPatients()[6] = "";
        this.getPlansPatientsViewBean().getListFilterPlansPatients().clear();
        this.getTypeAttentionViewBean().setNewType(new TypeAttention());
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(new ArrayList<PlansTreatmentsKeys>());
        RequestContext.getCurrentInstance().update("form:lstTreatment");
      

        if (especialista != null) {
            int cedula = especialista.getIdentityNumber();
            for (Specialists specialist : getListaSpecialists()) {
                if (specialist.getIdentityNumber() != null && specialist.getIdentityNumber() == cedula) {
                    this.editSpecialists[5] = specialist.getCompleteName();
                    this.editSpecialists[1] = specialist.getCompleteName();
                    this.editSpecialists[0] = specialist.getId().toString();

                    this.setEditSpecialistsObj(specialist);
                    this.getTypeSpecialistViewBean().setEditTypeSpecialist(specialist.getTypeSpecialistid());
                    this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(specialist.getTypeSpecialistid().getName());
                    this.getSpecialityViewBean().setEditListaSpecialists(this.getEditSpecialistsObj().getSpecialityList());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                this.editSpecialists[6] = "";
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado especialistas con ese número de cédula"));
            }
        }

    }

    public void revisarCedula(String id) {

        String revisar = getNewSpecialists()[6];

        int cedula = Integer.parseInt(revisar);
        int idSpecialist = 0;
        if (getNewSpecialists()[0] != null) {
            idSpecialist = Integer.parseInt(getNewSpecialists()[0]);
        }
        for (Specialists e : getListaSpecialists()) {
            if (e.getIdentityNumber() != null && !e.getId().equals(idSpecialist) && e.getIdentityNumber() == cedula) {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El Especialista ya se encuentra registrado"));

                if (idSpecialist != 0) {
                    String identityNumber = this.getBeforeEditSpecialists().getIdentityNumber().toString();

                    getNewSpecialists()[6] = identityNumber;

                    ((UIInput) findComponent(id)).setValue("");
                } else {
                    getNewSpecialists()[10] = null;
                }
                RequestContext.getCurrentInstance().update(findComponent(id).getClientId());
                break;
            }
        }
    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
     public void operationSecurity() {

        this.operationCreate = false;
        this.operationDelete = false;
        this.operationRead = false;
        this.operationUpdate = false;

        List<MenuRol> listMenuRol = new ArrayList<>();
        List<MenuRolOperation> listMenuRolOperation = new ArrayList<>();

        Menu menu = this.menuFacadeLocal.findByName("Especialistas");
        listMenuRol = this.securityViewBean.obtainMenuByOperations();

        for (int i = 0; i < listMenuRol.size(); i++) {

            Menu menuRol = listMenuRol.get(i).getMenuId();

            if (menu != null) {

                if (menu.getId() == menuRol.getId()) {

                    listMenuRolOperation = this.securityViewBean.obtainMenuRolOperations(listMenuRol.get(i).getId());

                    for (int j = 0; j < listMenuRolOperation.size(); j++) {

                        String operation = listMenuRolOperation.get(j).getOperation().getName();

                        if (operation.equals("Crear")) {

                            this.operationCreate = true;

                        } else if (operation.equals("Consultar")) {

                            this.operationRead = true;

                        } else if (operation.equals("Modificar")) {

                            this.operationUpdate = true;

                        } else if (operation.equals("Eliminar")) {

                            this.operationDelete = true;

                        }

                    }

                }
            }

        }

    }
   public boolean getScaleSpecific() {
        return scaleSpecific;
    }

    public void setScaleSpecific(boolean scaleSpecific) {
        this.scaleSpecific = scaleSpecific;
}
    
    public ScaleTreatmentsSpecialistViewBean getScaleTreatmentsSpecialistViewBean() {
        return scaleTreatmentsSpecialistViewBean;
    }

    public void setScaleTreatmentsSpecialistViewBean(ScaleTreatmentsSpecialistViewBean scaleTreatmentsSpecialistViewBean) {
        this.scaleTreatmentsSpecialistViewBean = scaleTreatmentsSpecialistViewBean;
    }
    
    
    
    
    

}
