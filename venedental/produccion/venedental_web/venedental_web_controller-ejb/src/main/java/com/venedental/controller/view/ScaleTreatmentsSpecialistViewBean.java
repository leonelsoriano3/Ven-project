package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.model.ScaleTreatmentsSpecialist;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "scaleTreatmentsSpecialistViewBean")
@ViewScoped
public class ScaleTreatmentsSpecialistViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    public ScaleTreatmentsSpecialistViewBean() {
        this.editScaleTreatmentsSpecialist = new ScaleTreatmentsSpecialist();
        this.newScaleTreatmentsSpecialist = new ScaleTreatmentsSpecialist();
        
    }

    private ScaleTreatmentsSpecialist selectedScaleTreatmentsSpecialist;

    private List<ScaleTreatmentsSpecialist> listaScaleTreatmentsSpecialist;

    private List<ScaleTreatmentsSpecialist> listaScaleTreatmentsSpecialistFiltrada;

    private ScaleTreatmentsSpecialist editScaleTreatmentsSpecialist;

    private ScaleTreatmentsSpecialist newScaleTreatmentsSpecialist;
    

    public ScaleTreatmentsSpecialist getNewScaleTreatmentsSpecialist() {
        return newScaleTreatmentsSpecialist;
    }

    public void setNewScaleTreatmentsSpecialist(ScaleTreatmentsSpecialist newScaleTreatmentsSpecialist) {
        this.newScaleTreatmentsSpecialist = newScaleTreatmentsSpecialist;
    }

    public ScaleTreatmentsSpecialist getEditScaleTreatmentsSpecialist() {
        return editScaleTreatmentsSpecialist;
    }

    public void setEditScaleTreatmentsSpecialist(ScaleTreatmentsSpecialist editScaleTreatmentsSpecialist) {
        this.editScaleTreatmentsSpecialist = editScaleTreatmentsSpecialist;
    }

    public ScaleTreatmentsSpecialist getSelectedScaleTreatmentsSpecialist() {
        return selectedScaleTreatmentsSpecialist;
    }


    public void setSelectedScaleTreatmentsSpecialist(ScaleTreatmentsSpecialist selectedScaleTreatmentsSpecialist) {
        this.selectedScaleTreatmentsSpecialist = selectedScaleTreatmentsSpecialist;
    }

    public List<ScaleTreatmentsSpecialist> getListaScaleTreatmentsSpecialistFiltrada() {
        if (this.listaScaleTreatmentsSpecialistFiltrada == null) {
            this.listaScaleTreatmentsSpecialistFiltrada = new ArrayList<>();
        }
        return listaScaleTreatmentsSpecialistFiltrada;
    }

    public void setListaScaleTreatmentsSpecialistFiltrada(List<ScaleTreatmentsSpecialist> listaScaleTreatmentsSpecialistFiltrada) {
        this.listaScaleTreatmentsSpecialistFiltrada = listaScaleTreatmentsSpecialistFiltrada;
    }

    public List<ScaleTreatmentsSpecialist> getListaScaleTreatmentsSpecialist() {
        if (this.listaScaleTreatmentsSpecialist == null) {
            this.listaScaleTreatmentsSpecialist = new ArrayList<>();
        }
        return listaScaleTreatmentsSpecialist;
    }

    public void setListaScaleTreatmentsSpecialist(List<ScaleTreatmentsSpecialist> listaScaleTreatmentsSpecialist) {
        this.listaScaleTreatmentsSpecialist = listaScaleTreatmentsSpecialist;
    }

    public List<ScaleTreatmentsSpecialist> getFilteredScaleTreatmentsSpecialist() {
        return null;
    }

}
