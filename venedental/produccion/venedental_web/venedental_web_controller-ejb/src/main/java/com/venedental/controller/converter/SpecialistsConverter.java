/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.SpecialistsViewBean;
import com.venedental.model.Specialists;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "specialistsConverter", forClass = Specialists.class)
public class SpecialistsConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        SpecialistsViewBean specialityViewBean = (SpecialistsViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "specialityViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Specialists s : specialityViewBean.getListaSpecialists()) {
                    if (s.getId() == numero) {
                        return s;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Especialista invalido"));
            } 
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Specialists) value).getId());
        }

    }

}
