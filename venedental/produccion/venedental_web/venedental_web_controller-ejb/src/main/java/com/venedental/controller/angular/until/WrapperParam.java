package com.venedental.controller.angular.until;

import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * empaqueta una item para la entrad de el servicio de parametros
 */
@XmlRootElement(name = "wrapperParam")
public class WrapperParam {

    @XmlElement(name = "key")
    private String key;
    @XmlElement(name = "value")
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    @Override
    public String toString(){
        try {
            // takes advantage of toString() implementation to format {"a":"b"}
            return new JSONObject().put("key", key).put("value", value).toString();
        } catch (JSONException e) {
            return null;
        }
    }


}
