package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;

@ManagedBean(name = "componenteViewBean")
@ViewScoped
public class ComponenteViewBean extends BaseBean {

    private static final long serialVersionUID = -7434469570770537682L;

    private boolean skip;

    private boolean ultimoPaso = false;
    
    private String blockerTarget;

    public String getBlockerTarget() {
        return blockerTarget;
    }

    public void setBlockerTarget(String blockerTarget) {
        this.blockerTarget = blockerTarget;
    }

    public boolean isUltimoPaso() {
        return ultimoPaso;
    }

    public void setUltimoPaso(boolean ultimoPaso) {
        this.ultimoPaso = ultimoPaso;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {

        if (skip) {
            skip = false;   //reset in case user goes back  
            return "clinicsTab1";
        } else {
            this.setUltimoPaso(event.getNewStep().compareTo("clinicsTab1") == 0);
            RequestContext.getCurrentInstance().update("form:specialistDialogNew:botonGrabar");     
            return event.getNewStep();

        }
    }
}
