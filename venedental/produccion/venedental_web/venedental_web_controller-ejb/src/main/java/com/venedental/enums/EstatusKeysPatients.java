package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum EstatusKeysPatients {

    GENERADO(1), ELIMINADO(8), RECIBIDO(3), POR_FACTURAR(6);

    Integer valor;

    private EstatusKeysPatients(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
