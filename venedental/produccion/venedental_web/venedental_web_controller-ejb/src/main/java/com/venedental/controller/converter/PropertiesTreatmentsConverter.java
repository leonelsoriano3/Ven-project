/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.PlansTreatmentsKeysViewBean;
import com.venedental.controller.view.PropertiesTreatmentsViewBean;
import com.venedental.controller.view.RegisterKeysAnalysisViewBean;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesTreatments;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "propertiesTreatmentsConverter", forClass = PropertiesTreatments.class)
public class PropertiesTreatmentsConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        PropertiesTreatmentsViewBean propertiesTreatmentsViewBean = (PropertiesTreatmentsViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "propertiesTreatmentsViewBean");
        PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean = (PlansTreatmentsKeysViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "plansTreatmentsKeysViewBean");
        RegisterKeysAnalysisViewBean registerKeysAnalysisViewBean = (RegisterKeysAnalysisViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "registerKeysAnalysisViewBean");
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);

                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatments()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsIII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsIV()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsV()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsVI()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsVII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getEditListPropertiesTreatmentsVIII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatments()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsIII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsIV()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsV()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsVI()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsVII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsViewBean.getNewListPropertiesTreatmentsVIII()) {
                    if (propertiesTreatments.getId() == numero) {
                        return propertiesTreatments;
                    }
                }

                for (PlansTreatmentsKeys plansTreatmentsKeys : plansTreatmentsKeysViewBean.getListPlansTreatmentsKeysSeleccionados()) {

                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilter().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilter()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterII().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterII()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIII().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIII()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIV().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIV()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterV().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterV()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVI().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVI()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVII().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVII()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVIII().size() > 0) {

                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVIII()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }

                }
                if (registerKeysAnalysisViewBean.getPropertiesTreatmentsPermanentI().size() > 0) {
                    for (PropertiesTreatments propertiesTreatments : registerKeysAnalysisViewBean.getPropertiesTreatmentsPermanentI()) {
                        if (propertiesTreatments.getId() == numero) {
                            return propertiesTreatments;
                        }
                    }
                }
                if (registerKeysAnalysisViewBean.getSelectedPropertiesTreatmentsPermanent().size() > 0) {
                    for (PropertiesTreatments propertiesTreatments : registerKeysAnalysisViewBean.getSelectedPropertiesTreatmentsPermanent()) {
                        if (propertiesTreatments.getId() == numero) {
                            return propertiesTreatments;
                        }
                    }
                }
                if (registerKeysAnalysisViewBean.getPropertiesTreatmentsTemporaryI().size() > 0) {
                    for (PropertiesTreatments propertiesTreatments : registerKeysAnalysisViewBean.getPropertiesTreatmentsTemporaryI()) {
                        if (propertiesTreatments.getId() == numero) {
                            return propertiesTreatments;
                        }
                    }
                }
                if (registerKeysAnalysisViewBean.getSelectedPropertiesTreatmentsTemporary().size() > 0) {
                    for (PropertiesTreatments propertiesTreatments : registerKeysAnalysisViewBean.getSelectedPropertiesTreatmentsTemporary()) {
                        if (propertiesTreatments.getId() == numero) {
                            return propertiesTreatments;
                        }
                    }
                }

                for (PlansTreatmentsKeys plansTreatmentsKeys : registerKeysAnalysisViewBean.getPlansTreatmentsKeys()) {
                    if (plansTreatmentsKeys.getPropertiesTemporary().size() > 0) {
                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesTemporary()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getPropertiesPermanent().size() > 0) {
                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getPropertiesPermanent()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getSelectedPropertiesPermanent().size() > 0) {
                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getSelectedPropertiesPermanent()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                    if (plansTreatmentsKeys.getSelectedPropertiesTemporary().size() > 0) {
                        for (PropertiesTreatments propertiesTreatments : plansTreatmentsKeys.getSelectedPropertiesTemporary()) {
                            if (propertiesTreatments.getId() == numero) {
                                return propertiesTreatments;
                            }
                        }
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Properties por tratamiento Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((PropertiesTreatments) value).getId());
        }

    }

}
