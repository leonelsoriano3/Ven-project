/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.helper;

import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.massiveLoading.base.Constants;
import com.venedental.massiveLoading.base.Messages;
import com.venedental.massiveLoading.base.enums.EFields;
import com.venedental.massiveLoading.exceptions.MessageException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author akdesk01
 */
public class ValidatorHelper {

    private static StringBuilder sb;
    
    public static void validateData(List<String> data, List<FindPlanByIdOutput> planList) throws MessageException {
        sb = new StringBuilder();
       
        validateIdentityNumberHolder(data);
        validateIdentityNumber(data);
        validateRelationship(data);
        validateCompleteName(data);
        validateSex(data);
        validatePlans(data, planList);
        validateCompanyCode(data);
        validateBirthday(data);
        if (hasError()) {
            throw new MessageException(sb.toString());
        }
    }

    private static void validateIdentityNumberHolder(List<String> data) {
        EFields field = EFields.IDENTITY_NUMBER_HOLDER;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^([0-9]){1,9}([\\s]+)*$") && !fieldString.replace("0", "").isEmpty() ) {
                fieldString = fieldString.replace(" ", "");
                replaceValue(data, fieldString, field);

            } else {
                throw new MessageException();
            }
            Integer value = Integer.parseInt(fieldString);
        } catch (MessageException | NumberFormatException e) {
            createException(field);
        }

    }

    private static void validateIdentityNumber(List<String> data) {
        EFields field = EFields.IDENTITY_NUMBER;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^([0-9]){1,9}([\\s]+)*$") &&!fieldString.replace("0", "").isEmpty() ) {
                fieldString = fieldString.replace(" ", "");
                replaceValue(data, fieldString, field);

            } else {
                throw new MessageException();
            }
            Integer value = Integer.parseInt(fieldString);
        } catch (MessageException | NumberFormatException e) {
            createException(field);
        }

    }

    private static void validateRelationship(List<String> data) {
        EFields field = EFields.RELATIONSHIP;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (!fieldString.matches("^([0-9])+([\\s]+)*$")&& fieldString.replace(" ", "").isEmpty()) {
                throw new MessageException();
            }
            fieldString = fieldString.replace(" ", "");
            Integer value = Integer.parseInt(fieldString);
            if (value > 10 || value < 1) {
                throw new MessageException();
            }
        } catch (MessageException | NumberFormatException e) {
            createException(field);
        }
        replaceValue(data, fieldString, field);
    }

    private static void validateCompleteName(List<String> data) {
        EFields field = EFields.COMPLETE_NAME;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^(?![\\s'])[a-zA-ZñÑáéíóúÁÉÍÓÚ ']{4,254}$") && !fieldString.replace(" ", "").isEmpty()) {
                
                replaceValue(data, fieldString, field);
            } else {
                throw new MessageException();
            }
        } catch (Exception e) {
            createException(field);
        }

    }

    private static void validateBirthday(List<String> data) {
        EFields field = EFields.BIRTHDAY;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());

            if (fieldString.isEmpty()) {
                throw new MessageException();
            }
            SimpleDateFormat format;
            if (fieldString.length()<8) {
                  throw new MessageException();
            } 
            if (fieldString.matches("^([0-9]){4}\\/[0-9]{2}\\/[0-9]{2}([\\s]+)*$")) {
                fieldString = fieldString.replace(" ", "");
                format = new SimpleDateFormat(Constants.FORMAT_DATE_4_YEAR);
                Date value = format.parse(fieldString);
                DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
                fieldString = fecha.format(value);
                replaceValueIgnoreError(data, fieldString, field);
            } else {
                throw new MessageException();
            }

        } catch (MessageException | ParseException ex) {
            createException(field);
        }

    }

    private static void validateSex(List<String> data) {
        EFields field = EFields.SEX;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^[M|F|m|f]([\\s]+)*$")) {
                fieldString = fieldString.replace(" ", "");
                fieldString = fieldString.toUpperCase();
                replaceValue(data, fieldString, field);
            } else {
                throw new MessageException();
            }
        } catch (Exception ex) {
            createException(field);
        }

    }

    private static void validatePlans(List<String> data, List<FindPlanByIdOutput> planList) {
        EFields field = EFields.PLANS;
        String fieldString = null;
        Boolean invalidate = true;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^([0-9,])+([\\s]+)*$") && !fieldString.replace(" ", "").isEmpty()) {
                fieldString = fieldString.replace(" ", "");
                for (String plan : fieldString.split(",")) {
                    Integer idPlan = Integer.parseInt(plan);
                    FindPlanByIdOutput findPlanByIdOutput = new FindPlanByIdOutput(idPlan);
                    for (FindPlanByIdOutput findPlanByIdOutputL : planList) {
                        if (findPlanByIdOutputL.getIdPlan().equals(findPlanByIdOutput.getIdPlan())) {  
                             invalidate = false;
                            break;
                        }
                    }
                }
            } else {
                throw new MessageException();
            }
            if (!invalidate) {
                replaceValue(data, fieldString, field);
                
            } else{throw new MessageException();}
        } catch (NumberFormatException | MessageException ex) {
            createException(field);
        }

    }

    private static void validateCompanyCode(List<String> data) {
        EFields field = EFields.COMPANY;
        String fieldString = null;
        try {
            fieldString = data.get(field.getOrder());
            if (fieldString.matches("^[^\\s]([a-zA-Z0-9-ñÑáéíóúÁÉÍÓÚ .,]){0,99}([\\s]+)*$")) {
                replaceValue(data, fieldString, field);
            } else {
                throw new MessageException();
            }
        } catch (Exception e) {
            createException(field);
        }

    }

    private static void createException(EFields eField) {
        if (hasError()) {
            sb.append(" / ");
        }
       
        switch (eField) {
            case IDENTITY_NUMBER_HOLDER:
                sb.append(Messages.MESSAGE_ID_HOLDER_INVALID);
                break;
            case IDENTITY_NUMBER:
                sb.append(Messages.MESSAGE_ID_INVALID);
                break;
            case RELATIONSHIP:
                sb.append(Messages.MESSAGE_RELATIONSHIP_INVALID);
                break;
            case COMPLETE_NAME:
                sb.append(Messages.MESSAGE_COMPLETE_NAME_INVALID);
                break;
            case BIRTHDAY:
                sb.append(Messages.MESSAGE_BIRTH_INVALID);
                break;
            case SEX:
                sb.append(Messages.MESSAGE_SEX_INVALID);
                break;

            case PLANS:
                sb.append(Messages.EXCEPTION_PLANS_INVALID);
                break;
            case COMPANY:
                sb.append(Messages.MESSAGE_COMPANY_NAME_INVALID);
                break;

        }
    }

    private static boolean hasError() {
        return !sb.toString().isEmpty();
    }

    private static void replaceValue(List<String> data, String fieldString, EFields field) {
        if (!hasError()) {
            data.set(field.getOrder(), fieldString);
        }
    }
    private static void replaceValueIgnoreError(List<String> data, String fieldString, EFields field) {
            data.set(field.getOrder(), fieldString);
       
    }

}
