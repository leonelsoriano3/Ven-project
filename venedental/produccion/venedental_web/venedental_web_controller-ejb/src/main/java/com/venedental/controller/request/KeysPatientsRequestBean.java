package com.venedental.controller.request;

import com.venedental.constans.ConstansEmailCondicionado;
import com.venedental.constans.ConstansEmailTradicional;
import com.venedental.constans.ConstansPaymentReport;
import com.venedental.controller.BaseBean;
import com.venedental.controller.view.*;
import com.venedental.dto.keyPatients.HeadboardEmailOutput;
import com.venedental.dto.keyPatients.ReadTreatmentsEmailOutput;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.enums.TypeAttention;
import com.venedental.facade.BinnacleKeysFacadeLocal;
import com.venedental.facade.BinnaclePlansTreatmentsKeysFacadeLocal;
import com.venedental.facade.BinnaclePropertiesTreatmentsKeysFacadeLocal;
import com.venedental.facade.KeysPatientsFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PlansTreatmentsKeysFacadeLocal;
import com.venedental.facade.PropertiesPlansTreatmentsKeyFacadeLocal;
import com.venedental.facade.ScalePropertiesTreatmentsFacadeLocal;
import com.venedental.facade.StatusAnalysisKeysFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.*;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceKeysPatients;
import org.apache.commons.lang3.StringUtils;

import java.io.*;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.Properties;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;
import services.utils.Transformar;

@ManagedBean(name = "keysPatientsRequestBean")
@RequestScoped
public class KeysPatientsRequestBean extends BaseBean implements Serializable {
    
    String EMAIL_VENEDEN_CLAVE="backupclaves@yahoo.com";

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(ClinicsRequestBean.class.getName());

    @EJB
    private ServiceKeysPatients serviceKeysPatients;

    @EJB
    private KeysPatientsFacadeLocal keysPatientsFacadeLocal;

    @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;

    @EJB
    private PlansTreatmentsKeysFacadeLocal plansTreatmentsKeysFacadeLocal;

    @EJB
    private PropertiesPlansTreatmentsKeyFacadeLocal propertiesPlansTreatmentsKeyFacadeLocal;

    @EJB
    private StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal;

    @EJB
    private BinnacleKeysFacadeLocal binnacleKeysFacadeLocal;
    @EJB
    private BinnaclePlansTreatmentsKeysFacadeLocal binnaclePlansTreatmentsKeysFacade;
    @EJB
    private BinnaclePropertiesTreatmentsKeysFacadeLocal binnaclePropertiesTreatmentsKeysFacade;

    @ManagedProperty(value = "#{keysPatientsRegisterViewBean}")
    private KeysPatientsRegisterViewBean keysPatientsRegisterViewBean;

    @EJB
    private ScalePropertiesTreatmentsFacadeLocal scalePropertiesTreatmentsFacade;

    @ManagedProperty(value = "#{keysPatientsViewBean}")
    private KeysPatientsViewBean keysPatientsViewBean;
    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;
    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    @ManagedProperty(value = "#{plansPatientsViewBean}")
    private PlansPatientsViewBean plansPatientsViewBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;
    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;
    @ManagedProperty(value = "#{typeAttentionViewBean}")
    private TypeAttentionViewBean typeAttentionViewBean;

    @ManagedProperty(value = "#{propertiesPlansTreatmentsKeyRequestBean}")
    private PropertiesPlansTreatmentsKeyRequestBean propertiesPlansTreatmentsKeyRequestBean;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @EJB
    private MailSenderLocal mailSenderLocal;

    private KeysPatients keysPatientsnew;
    
    private Integer typeEmail;
    
    private Date expirationDate;
    private Date dateOfBirthPatient;

    private List<InsurancesPatients> listaInsurePatients;
    private List<PlansTreatmentsKeys> plansTreatementsKeysList;
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysList;
    private List<String> readTreatmentsEmailOutputEmail;
    private  List<String>  readTreatmentsEmailOutputEmailColum1;
    private  List<String>  readTreatmentsEmailOutputEmailColum2;
    private String lisatNameTreatmentsEmailTradicional;
    private String listaReadTreatmentsColum1;
    private String  listaReadTreatmentsColum2;
    

    private Map<String, Integer> propertiesI;
    private Map<PropertiesTreatments, Integer> propertiesNew;
    private Map<PropertiesTreatments, Integer> propertiesEdit;

    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatementsKeysList() {

        if (this.propertiesPlansTreatementsKeysList == null) {
            this.propertiesPlansTreatementsKeysList = new ArrayList<>();
        }
        return propertiesPlansTreatementsKeysList;
    }

    public void setPropertiesPlansTreatementsKeysList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysList) {
        this.propertiesPlansTreatementsKeysList = propertiesPlansTreatementsKeysList;
    }

    public List<PlansTreatmentsKeys> getPlansTreatementsKeysList() {
        return plansTreatementsKeysList;
    }

    public void setPlansTreatementsKeysList(List<PlansTreatmentsKeys> plansTreatementsKeysList) {
        if (this.plansTreatementsKeysList == null) {
            this.plansTreatementsKeysList = new ArrayList<>();
        }
        this.plansTreatementsKeysList = plansTreatementsKeysList;
    }

    public List<InsurancesPatients> getListaInsurePatients() {
        if (this.listaInsurePatients == null) {
            this.listaInsurePatients = new ArrayList<>();
        }

        return listaInsurePatients;
    }

    public StatusAnalysisKeysFacadeLocal getStatusAnalysisKeysFacadeLocal() {
        return statusAnalysisKeysFacadeLocal;
    }

    public void setStatusAnalysisKeysFacadeLocal(StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal) {
        this.statusAnalysisKeysFacadeLocal = statusAnalysisKeysFacadeLocal;
    }

    public void setInsurePatients(List<InsurancesPatients> listaInsurePatients) {
        this.listaInsurePatients = listaInsurePatients;
    }

    public void prepareNewKeysPatients() {

    }

    public KeysPatients getKeysPatientsnew() {
        return keysPatientsnew;
    }

    public void setKeysPatientsnew(KeysPatients keysPatientsnew) {
        this.keysPatientsnew = keysPatientsnew;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public TypeAttentionViewBean getTypeAttentionViewBean() {
        return typeAttentionViewBean;
    }

    public void setTypeAttentionViewBean(TypeAttentionViewBean typeAttentionViewBean) {
        this.typeAttentionViewBean = typeAttentionViewBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public KeysPatientsFacadeLocal getKeysPatientsFacadeLocal() {
        return keysPatientsFacadeLocal;
    }

    public void setKeysPatientsFacadeLocal(KeysPatientsFacadeLocal keysPatientsFacadeLocal) {
        this.keysPatientsFacadeLocal = keysPatientsFacadeLocal;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public KeysPatientsViewBean getKeysPatientsViewBean() {
        return keysPatientsViewBean;
    }

    public void setKeysPatientsViewBean(KeysPatientsViewBean keysPatientsViewBean) {
        this.keysPatientsViewBean = keysPatientsViewBean;
    }

    public PlansPatientsViewBean getPlansPatientsViewBean() {
        return plansPatientsViewBean;
    }

    public void setPlansPatientsViewBean(PlansPatientsViewBean plansPatientsViewBean) {
        this.plansPatientsViewBean = plansPatientsViewBean;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public Map<String, Integer> getPropertiesI() {
        if (this.propertiesI == null) {
            this.propertiesI = new HashMap<>();
        }
        return propertiesI;
    }

    public void setPropertiesI(Map<String, Integer> propertiesI) {

        this.propertiesI = propertiesI;
    }

    public Map<PropertiesTreatments, Integer> getPropertiesNew() {
        return propertiesNew;
    }

    public void setPropertiesNew(Map<PropertiesTreatments, Integer> propertiesNew) {
        this.propertiesNew = propertiesNew;
    }

    public Map<PropertiesTreatments, Integer> getPropertiesEdit() {
        if (this.propertiesEdit == null) {
            this.propertiesEdit = new HashMap<>();
        }
        return propertiesEdit;
    }

    public void setPropertiesEdit(Map<PropertiesTreatments, Integer> propertiesEdit) {
        this.propertiesEdit = propertiesEdit;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public PropertiesPlansTreatmentsKeyRequestBean getPropertiesPlansTreatmentsKeyRequestBean() {
        return propertiesPlansTreatmentsKeyRequestBean;
    }

    public void setPropertiesPlansTreatmentsKeyRequestBean(
            PropertiesPlansTreatmentsKeyRequestBean propertiesPlansTreatmentsKeyRequestBean) {
        this.propertiesPlansTreatmentsKeyRequestBean = propertiesPlansTreatmentsKeyRequestBean;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public Integer getTypeEmail() {
        return typeEmail;
    }

    public void setTypeEmail(Integer typeEmail) {
        this.typeEmail = typeEmail;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getDateOfBirthPatient() {
        return dateOfBirthPatient;
    }

    public void setDateOfBirthPatient(Date dateOfBirthPatient) {
        this.dateOfBirthPatient = dateOfBirthPatient;
    }

    public List<String> getReadTreatmentsEmailOutputEmail() {
        return readTreatmentsEmailOutputEmail;
    }

    public void setReadTreatmentsEmailOutputEmail(List<String> readTreatmentsEmailOutputEmail) {
        this.readTreatmentsEmailOutputEmail = readTreatmentsEmailOutputEmail;
    }

    public List<String> getReadTreatmentsEmailOutputEmailColum1() {
        return readTreatmentsEmailOutputEmailColum1;
    }

    public void setReadTreatmentsEmailOutputEmailColum1(List<String> readTreatmentsEmailOutputEmailColum1) {
        this.readTreatmentsEmailOutputEmailColum1 = readTreatmentsEmailOutputEmailColum1;
    }

    public List<String> getReadTreatmentsEmailOutputEmailColum2() {
        return readTreatmentsEmailOutputEmailColum2;
    }

    public void setReadTreatmentsEmailOutputEmailColum2(List<String> readTreatmentsEmailOutputEmailColum2) {
        this.readTreatmentsEmailOutputEmailColum2 = readTreatmentsEmailOutputEmailColum2;
    }

    public String getLisatNameTreatmentsEmailTradicional() {
        return lisatNameTreatmentsEmailTradicional;
    }

    public void setLisatNameTreatmentsEmailTradicional(String lisatNameTreatmentsEmailTradicional) {
        this.lisatNameTreatmentsEmailTradicional = lisatNameTreatmentsEmailTradicional;
    }

    public String getListaReadTreatmentsColum1() {
        return listaReadTreatmentsColum1;
    }

    public void setListaReadTreatmentsColum1(String listaReadTreatmentsColum1) {
        this.listaReadTreatmentsColum1 = listaReadTreatmentsColum1;
    }

    public String getListaReadTreatmentsColum2() {
        return listaReadTreatmentsColum2;
    }

    public void setListaReadTreatmentsColum2(String listaReadTreatmentsColum2) {
        this.listaReadTreatmentsColum2 = listaReadTreatmentsColum2;
    }
    
    

   

    
    
    

    public void createKeys() throws ParseException {

        if (!this.getKeysPatientsViewBean().getCreate()) {

            this.getKeysPatientsViewBean().setCreate(true);

            if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {
                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Error", "Debe elegir un tratamiento"));
                this.getKeysPatientsViewBean().setCreate(false);
            } else if (this.getSpecialistsViewBean().getEditSpecialists()[6] == null || this.getSpecialistsViewBean().
                    getEditSpecialists()[6].trim().compareTo("") == 0) {

                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Error", "Debe elegir un especialista"));
                this.getKeysPatientsViewBean().setCreate(false);
            } else if (this.getTypeAttentionViewBean().getNewType() == null) {
                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Error", "Debe elegir un tipo de atención"));
                this.getKeysPatientsViewBean().setCreate(false);
            } else {

                KeysPatients keysPatients = new KeysPatients();

                Date fecha = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdfClave = new SimpleDateFormat("ddMMyyyy");
                SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
                sdfClave.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
                sdf.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
                String fechaClave = sdfClave.format(fecha);
                String fechaActual = sdfSolicitud.format(fecha);
                String fechaSolicitud = sdfSolicitud.format(fecha);
                Date dateApplication = Transformar.StringToDate(fechaSolicitud);

                keysPatients.setApplicationDateString(fechaActual);
                keysPatients.setApplicationDate(dateApplication);
                String fechaNacimiento = sdf.format(this.getPatientsViewBean().getNewPatientsObj().getDateOfBirth());
                String completeName = this.getPatientsViewBean().getNewPatientsObj().getCompleteName();
                int identityNumber = this.getPatientsViewBean().getNewPatientsObj().getIdentityNumber();
                this.getPatientsViewBean().getEditPatients()[2] = fechaNacimiento;
                this.getPatientsViewBean().getEditPatients()[5] = this.getPatientsViewBean().getNewPatientsObj().
                        getCompleteName();
                this.getPatientsViewBean().getEditPatients()[6] = this.getPatientsViewBean().getNewPatientsObj().
                        getIdentityNumber().toString();
                keysPatients.setPatientsId(this.getPatientsViewBean().getNewPatientsObj());

                if (this.keysPatientsViewBean.getSpecialist() != null) {
                    keysPatients.setSpecialistId(this.keysPatientsViewBean.getSpecialist());

                } else {
                    keysPatients.setSpecialistId(this.getSpecialistsViewBean().getEditSpecialistsObj());
                }

                /**
                 * Setearle objeto de status a la clave
                 */
                StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.
                        findById(EstatusKeysPatients.GENERADO.getValor());
                keysPatients.setStatus(status);

                if (this.getTypeAttentionViewBean().getNewType() != null) {
                    if (this.getTypeAttentionViewBean().getNewType().getId().equals(TypeAttention.Diagnostico.getValor())) {
                        keysPatients.setAttentionType(TypeAttention.Diagnostico.name());
                    } else {
                        keysPatients.setAttentionType(TypeAttention.Emergencia.name());
                    }
                }

                Integer countRecords = keysPatientsFacadeLocal.count() + 1;
                String typePlansPatients = "";
                String insurance = "";
                for (PlansPatients plansPatients : this.getPlansPatientsViewBean().getListFilterPlansPatients()) {
                    typePlansPatients = plansPatients.getPlansId().getIdentifier().toString();
                    insurance = plansPatients.getPlansId().getInsuranceId().getId().toString();
                    DateFormat fechaplan = new SimpleDateFormat("dd/MM/yyyy");
                    String convertidofecha = fechaplan.format(plansPatients.getDueDate());
                    this.getKeysPatientsViewBean().setDueDate(convertidofecha);
                }

                String numberKey = insurance + typePlansPatients + fechaClave + countRecords.toString();
                keysPatients.setNumber(numberKey);

                KeysPatients keysPatientsRegistrado = new KeysPatients();
                keysPatientsRegistrado = keysPatientsFacadeLocal.create(keysPatients);
                log.log(Level.INFO, "Clave creada");
                /**
                 * create BinnacleKeys
                 */
                this.createBinnacleKeys(keysPatientsRegistrado, EstatusKeysPatients.GENERADO.getValor());

                this.getKeysPatientsViewBean().setKeysPatientsNew(keysPatientsRegistrado);

                for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().
                        getListPlansTreatmentsKeysSeleccionados()) {

                    plansTreatmentsKeys.setKeys_id(keysPatientsRegistrado.getId());
                    if (!plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().
                            isEmpty()) {
                        plansTreatmentsKeys.setScaleTreatmentsKeys(plansTreatmentsKeys.getPlansTreatmentsId().
                                getTreatmentsId().getScaleTreatmentsListListFilter().get(0));

                    }
                    plansTreatmentsKeys.setIdStatus(status);
                    plansTreatmentsKeys.setDatePlansTreatmentsKeys(dateApplication);
                    PlansTreatmentsKeys plansTreatmentsKeysEditado = plansTreatmentsKeys;

                    List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysListEditado
                            = new ArrayList<PropertiesPlansTreatmentsKey>();
                    propertiesPlansTreatementsKeysListEditado = plansTreatmentsKeys.
                            getPropertiesPlansTreatmentsKeyList();
                    plansTreatmentsKeysEditado.setPropertiesPlansTreatmentsKeyList(
                            new ArrayList<PropertiesPlansTreatmentsKey>());
                    PlansTreatmentsKeys plansTreatmentsKeysRegistrado = plansTreatmentsKeysFacadeLocal.
                            create(plansTreatmentsKeysEditado);

                    /**
                     * create createBinnaclePlansTreatmentsKeys
                     */
                    this.createBinnaclePlansTreatmentsKeys(plansTreatmentsKeysRegistrado, EstatusKeysPatients.GENERADO.getValor());
                    plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyList(propertiesPlansTreatementsKeysListEditado);
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList() != null) {
                        for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey : plansTreatmentsKeys.
                                getPropertiesPlansTreatmentsKeyList()) {
                            propertiesPlansTreatmentsKey.setPlansTreatmentsKeys(plansTreatmentsKeysRegistrado);
                            propertiesPlansTreatmentsKey.setIdStatus(status);
                            propertiesPlansTreatmentsKey.setDate_properties(dateApplication);
                            propertiesPlansTreatmentsKey.setIdScalePropertiesTreatments(
                                    this.scalePropertiesTreatmentsFacade.
                                    findByPropertieTreatment(propertiesPlansTreatmentsKey.
                                            getPropertiesTreatments().getId(),
                                            keysPatientsRegistrado.getSpecialistId().getId()));
                        }
                    }

                }

                this.getKeysPatientsViewBean().setKeysPatientsNew(keysPatientsRegistrado);
                this.getKeysPatientsViewBean().getKeysPatientsNew().
                        setPlansTreatmentsKeysList(this.getPlansTreatmentsKeysViewBean().
                                getListPlansTreatmentsKeysSeleccionados());
                KeysPatients keysPatientsModificado = (KeysPatients) this.keysPatientsFacadeLocal.
                        edit(this.getKeysPatientsViewBean().getKeysPatientsNew());
                /**
                 * create createBinnaclePropertiesList
                 */
                this.createBinnaclePropertiesList(keysPatientsModificado.getId(), EstatusKeysPatients.GENERADO.getValor());
                if (this.keysPatientsViewBean.getSpecialist() != null) {
                    context.update("form:keysSpecialistGrow");
                    this.accionesRequestBean.desplegarDialogoView("keysSpecialistsResult");
                } else {
                    context.update("form:growl");
                    context.update("form:growlKeysEdit");
                    this.accionesRequestBean.desplegarDialogoView("keysResult");
                }

                this.getKeysPatientsViewBean().setCreate(false);
            }
        }
    }

    /**
     * Metodo que permite crear una nueva clave
     *
     * @throws SQLException
     * @throws java.io.IOException
     * @throws java.text.ParseException
     */
    public void registerKey() throws SQLException, IOException, ParseException {
        Integer idCompany = null;
        Integer idKeyCreate = null;
        Integer idStatusKeyCreate = null;
        Integer idPlan = this.keysPatientsRegisterViewBean.getFilterPlanPatientOutputSelected().getIdPlan();
        Integer idUser = this.securityViewBean.obtainUser().getId();
        String numberKey = this.serviceKeysPatients.generateNumber(idPlan);
        Integer idPatient = this.keysPatientsRegisterViewBean.getFilterPlanPatientOutputSelected().getIdPatient();
        Integer idSpecialist = this.keysPatientsRegisterViewBean.getSelectedSpecialist().getId();
        
        Integer idMedicalO = this.keysPatientsRegisterViewBean.getMedicalOfficeSelected().getId();
        if(this.keysPatientsRegisterViewBean.getFilterPlanPatientOutputSelected().getIdCompany()==-1){
          idCompany = 0;  
        }else{
           idCompany = this.keysPatientsRegisterViewBean.getFilterPlanPatientOutputSelected().getIdCompany(); 
        } 
        this.getKeysPatientsRegisterViewBean().setRegisterKeyOutput(this.serviceKeysPatients.registerKey(numberKey, null, idPatient, idSpecialist, idUser, idMedicalO, idCompany, idPlan));
//
        for (RegisterKeyOutput registerKeyOutput : this.getKeysPatientsRegisterViewBean().getRegisterKeyOutput()) {
            idKeyCreate = registerKeyOutput.getIdKey();
            idStatusKeyCreate = registerKeyOutput.getStatusKey();
            Specialists dataSpecialist = this.keysPatientsRegisterViewBean.getSelectedSpecialist();
            String nameReport = "RP_" + dataSpecialist.getFirstName() + dataSpecialist.getLastname() + "_" + dataSpecialist.getIdentityNumber().toString();
            this.getKeysPatientsRegisterViewBean().findDetailsKey(idKeyCreate);
            this.getAccionesRequestBean().desplegarDialogoView("keysResultDialog");
            this.getKeysPatientsRegisterViewBean().setHeadboardEmailOutput(this.serviceKeysPatients.findCabeceraEmail(idKeyCreate));
             for (HeadboardEmailOutput headboardEmailOutput : this.getKeysPatientsRegisterViewBean().getHeadboardEmailOutput()) {
             this.setTypeEmail(headboardEmailOutput.getTypePlan());
             this.setExpirationDate(headboardEmailOutput.getExpirationDate());
             this.setDateOfBirthPatient(headboardEmailOutput.getBirthdate());
             if((headboardEmailOutput.getTypePlan()==0)||(headboardEmailOutput.getTypePlan()==1)){
               sendMailSpecialist(findFileEmailTradicional(nameReport), dataSpecialist, registerKeyOutput,this.getTypeEmail(),headboardEmailOutput);  
             }else if(headboardEmailOutput.getTypePlan()==2){
                 sendMailSpecialist(findFileEmailCondicionado(nameReport), dataSpecialist, registerKeyOutput,this.getTypeEmail(),headboardEmailOutput);
             }
        

//Se llama al metodo que valida el  encabezado del correo
            
         }
             }
    }
    
    public void headboardEmail(Integer idKey){
         
         
    }
    
    /**
     * Method to seek payment report email condicionado
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFileEmailCondicionado(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTIES_PATH);
        String reportExtension = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTIES_PATH);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    
     /**
     * Method to seek payment report email tradicional
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFileEmailTradicional(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTIES_PATH);
        String reportExtension = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTIES_PATH);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    /**
     * Method to seek payment report
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFile(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
        String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    /**
     * Method to send an email specialty
     *
     * @param files
     * @param specialist
     * @param key
     * @param typeEmail
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailSpecialist(List<File> files, Specialists specialist, RegisterKeyOutput key,Integer typeEmail, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
        String emailSpecialist;
        String subjectMessage="";
        String title="";
         String contentMessageEspecialist = null;
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}", specialist.getCompleteName());
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        this.getKeysPatientsRegisterViewBean().findDetailsKey(key.getIdKey());
        String namePatient = this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNamePatient();
        if (this.securityViewBean.obtainRolSpecialist()) {
            emailSpecialist = this.getKeysPatientsRegisterViewBean().getSelectedSpecialist().getEmail();
        } else {
            emailSpecialist = specialist.getEmail();
        }
        if (emailSpecialist != null) {
            
            if((typeEmail==0)||(typeEmail==1)){
                title = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_TITLE_PROPERTY_);
           subjectMessage = title + " " + " - Paciente:" + " " + namePatient;
               contentMessageEspecialist = buildMessageSpecialistTradicional(specialist, key,headboardEmailOutput);
            }else if(typeEmail==2){
                title = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_TITLE_PROPERTY_);
           subjectMessage = title + " " + " - Paciente:" + " " + namePatient;
                 contentMessageEspecialist = buildMessageSpecialistCondicionado(specialist, key,headboardEmailOutput);
            }
            
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files, specialist.getEmail());
            
             this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files, EMAIL_VENEDEN_CLAVE);
            this.getKeysPatientsRegisterViewBean().setLabelMailMessageFailed(false);
            this.getKeysPatientsRegisterViewBean().setLabelSuccessfulMailMessage(true);
            RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");
        } else {
            this.getKeysPatientsRegisterViewBean().setLabelMailMessageFailed(true);
            this.getKeysPatientsRegisterViewBean().setLabelSuccessfulMailMessage(false);
            RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");

            log.log(Level.INFO, "El especialista no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

    }
    
    
    
    /**
     * Method to construct the message body for the email that will be sent to specialist(condicionado, limitado o ilimitado)
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialistCondicionado(Specialists specialist, RegisterKeyOutput key, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
        Integer idPlan=headboardEmailOutput.getIdPlan();
        this.readTreatmentsEmailOutputEmailColum1=new ArrayList<String>();
        this.readTreatmentsEmailOutputEmailColum2=new ArrayList<String>();
         this.listaReadTreatmentsColum1="";
         this.listaReadTreatmentsColum2="";
        
        this.getKeysPatientsRegisterViewBean().setReadTreatmentsEmailOutput(this.serviceKeysPatients.findTreatmentsEmail(idPlan));
       
       for (ReadTreatmentsEmailOutput readTreatmentsEmailOutput : this.getKeysPatientsRegisterViewBean().getReadTreatmentsEmailOutput()) {
           if(readTreatmentsEmailOutput.getIdColumn()==1){
               this.readTreatmentsEmailOutputEmailColum1.add(readTreatmentsEmailOutput.getLineTreatments().toString());
               this.listaReadTreatmentsColum1=this.readTreatmentsEmailOutputEmailColum1.toString();
           }else if(readTreatmentsEmailOutput.getIdColumn()==2){
               this.readTreatmentsEmailOutputEmailColum2.add(readTreatmentsEmailOutput.getLineTreatments().toString());
               this.listaReadTreatmentsColum2=this.readTreatmentsEmailOutputEmailColum2.toString();
                       
           }
           
           
       }
        this.listaReadTreatmentsColum1=this.listaReadTreatmentsColum1.replace(".,",".<br><br>" );
       this.listaReadTreatmentsColum2=this.listaReadTreatmentsColum2.replace(".,",".<br><br>" );
       
        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String expirationDateForKey = sdfSolicitudD.format(headboardEmailOutput.getExpirationDate());
        String dateOfBirth = sdfSolicitudD.format(headboardEmailOutput.getBirthdate());
         String dateOfRequest=sdfSolicitudD.format(headboardEmailOutput.getRequestdate());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Doctor(a). ").append(specialist.getCompleteName()).append(",<br><br>");
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT2));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICONADO_CONTENT3)).append(headboardEmailOutput.getNumberKey());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT4)).append(dateOfRequest);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT5)).append(expirationDateForKey);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT6)).append(headboardEmailOutput.getNamePatient());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT7)).append(headboardEmailOutput.getIdentityNumberPatient());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT8)).append(dateOfBirth);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT9)).append(headboardEmailOutput.getNameRelationShip());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT10)).append(headboardEmailOutput.getNameInsurance());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT11)).append(headboardEmailOutput.getNamePlan());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT12)).append(headboardEmailOutput.getNameEntity());
       stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT13));
        for(String readTreatmentsEmailOutputEmailColum1 : this.readTreatmentsEmailOutputEmailColum1) {
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT14)).append(readTreatmentsEmailOutputEmailColum1).append("<br><br>");
    }
        
       
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT15));
        for(String readTreatmentsEmailOutputEmailColum2 : this.readTreatmentsEmailOutputEmailColum2) {
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT16)).append(readTreatmentsEmailOutputEmailColum2).append("<br><br>");
    }
        
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT17));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT18));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT19));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT20));
        return stringBuilder.toString();
    }

    
    
    /**
     * Method to construct the message body for the email that will be sent to specialist(tradicional)
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialistTradicional(Specialists specialist, RegisterKeyOutput key, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
      this.readTreatmentsEmailOutputEmail=new ArrayList<String>();
         Integer idPlan=headboardEmailOutput.getIdPlan();
        this.getKeysPatientsRegisterViewBean().setReadTreatmentsEmailOutput(this.serviceKeysPatients.findTreatmentsEmail(headboardEmailOutput.getIdPlan()));
       for (ReadTreatmentsEmailOutput readTreatmentsEmailOutput : this.getKeysPatientsRegisterViewBean().getReadTreatmentsEmailOutput()) {
           readTreatmentsEmailOutput.getLineTreatments().toString().replace("[","");
           readTreatmentsEmailOutput.getLineTreatments().toString().replace("[","");      
           this.readTreatmentsEmailOutputEmail.add(readTreatmentsEmailOutput.getLineTreatments().toString());
           this.lisatNameTreatmentsEmailTradicional="";
           this.lisatNameTreatmentsEmailTradicional=this.readTreatmentsEmailOutputEmail.toString();
       }
        this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace(",","");
        this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace("[","");
        this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace("]","");
        
        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String expirationDateForKey = sdfSolicitudD.format(headboardEmailOutput.getExpirationDate());
        String dateOfBirth = sdfSolicitudD.format(headboardEmailOutput.getBirthdate());
         String dateOfRequest=sdfSolicitudD.format(headboardEmailOutput.getRequestdate());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Doctor(a). ").append(specialist.getCompleteName()).append(",<br><br>");
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT));
         stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT2));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT3)).append(headboardEmailOutput.getNumberKey());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT4)).append(dateOfRequest);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT5)).append(expirationDateForKey);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT6)).append(headboardEmailOutput.getNamePatient());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT7)).append(headboardEmailOutput.getIdentityNumberPatient());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT8)).append(dateOfBirth);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT9)).append(headboardEmailOutput.getNameRelationShip());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT10)).append(headboardEmailOutput.getNameInsurance());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT11)).append(headboardEmailOutput.getNamePlan());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT12)).append(headboardEmailOutput.getNameEntity());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT13));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT14)).append(this.lisatNameTreatmentsEmailTradicional).append(".");
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT15));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT16));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT17));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT18));
        return stringBuilder.toString();
    }
//    /**
//     * Method to construct the message body for the email that will be sent to specialist
//     *
//     * @param specialist
//     * @return String
//     * @throws ParseException
//     * @throws IOExceptionConstansEmailforSpecialistKey
//     */
//    private String buildMessageSpecialist(Specialists specialist, RegisterKeyOutput key) throws ParseException, IOException {
//        this.getKeysPatientsRegisterViewBean().findDetailsKey(key.getIdKey());
//        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
//        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
//        String expirationDateForKey = sdfSolicitudD.format(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getExpirationDate());
//        String dateOfBirth = sdfSolicitudD.format(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getDateOfBirthPatient());
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("Estimado Doctor(a). ").append(specialist.getCompleteName()).append(",<br><br>");
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT3)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNumber());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT4)).append(this.getKeysPatientsRegisterViewBean().getDateKeyDetails());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT5)).append(expirationDateForKey);
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT6)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNamePatient());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT7)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getIdentityNumberForPatient());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT8)).append(dateOfBirth);
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT9)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNameRelationShip());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT10)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNameInsurances());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT11)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getNamePlan());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT12)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getEnte());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT13));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT14)).append(this.getKeysPatientsRegisterViewBean().getKeysDetailsOutput().getTreatmentsForPlan()).append(".");
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT15));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT16));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT17));
//        return stringBuilder.toString();
//    }
    
    /**
     * Method to read the properties file constants payment reports email condicionado
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFilePropertiesEmailCondicionado() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailCondicionado.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailCondicionado.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }
    
    /**
     * Method to read the properties file constants payment reports email tradicional
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFilePropertiesEmailTradicional() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailTradicional.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailTradicional.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailCondicionado.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailCondicionado.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    public void editKeysNewOptimizado(String idDialog) throws ParseException {

        if (!this.getKeysPatientsViewBean().getCreate()) {
        }

        this.getKeysPatientsViewBean().setCreate(true);

        boolean piezasvacias = false;

        System.out.println("Tratamiento Seleccionado" + this.plansTreatmentsViewBean.getSelectedPlansTreatments());

        if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {

            this.getPlansTreatmentsKeysViewBean().getBeforeListPlansTreatmentsKeys().
                    addAll(this.plansTreatmentsKeysFacadeLocal.findByKeysId(this.keysPatientsViewBean.
                                    getKeysPatientsEdit().getId()));
            for (PlansTreatmentsKeys plansTreatmentsKeysBefore : this.getPlansTreatmentsKeysViewBean().
                    getBeforeListPlansTreatmentsKeys()) {

                if (plansTreatmentsKeysBefore.getId() != 0 && !this.getPlansTreatmentsKeysViewBean().
                        getListPlansTreatmentsKeysSeleccionados().contains(plansTreatmentsKeysBefore)) {

                    if (!plansTreatmentsKeysBefore.getPropertiesPlansTreatmentsKeyList().isEmpty()) {
                        StatusAnalysisKeys statusE = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
                        this.createBinnaclePropertiesList(plansTreatmentsKeysBefore.getKeysId().getId(), statusE.getId());

                    }

                    /**
                     * create createBinnaclePlansTreatmentsKeys
                     */
                    StatusAnalysisKeys statusE = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
                    plansTreatmentsKeysBefore.setStatusId(statusE.getId());
                    this.createBinnaclePlansTreatmentsKeys(plansTreatmentsKeysBefore, statusE.getId());

                }

            }
            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().
                    getListPlansTreatmentsKeysSeleccionados()) {
                log.log(Level.INFO, "Id PlantreatmentsKey");
                System.out.print(plansTreatmentsKeys.getId());

                this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().clear();
                if (plansTreatmentsKeys.getKeys_id() == 0) {
                    plansTreatmentsKeys.setKeys_id(this.getKeysPatientsViewBean().getKeysPatientsEdit().getId());
                    if (!plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().
                            isEmpty()) {
                        plansTreatmentsKeys.setScaleTreatmentsKeys(plansTreatmentsKeys.getPlansTreatmentsId().
                                getTreatmentsId().getScaleTreatmentsListListFilter().get(0));
                    }
                    StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.GENERADO.
                            getValor());
                    PlansTreatmentsKeys plansTreatmentsKeysRegistrado = new PlansTreatmentsKeys();
                    plansTreatmentsKeys.setIdStatus(status);
                    Date dateActual = this.obtainsDateActual();
                    plansTreatmentsKeys.setDatePlansTreatmentsKeys(dateActual);
                    PlansTreatmentsKeys plansTreatmentsKeysFind = this.plansTreatmentsKeysFacadeLocal.
                            findByKeysAndPlansTreatment(this.getKeysPatientsViewBean().getKeysPatientsEdit().getId(),
                                    plansTreatmentsKeys.getPlansTreatmentsId().getId());

                    if (plansTreatmentsKeysFind == null) {

                        PlansTreatmentsKeys plansTreatmentsKeysEditado = plansTreatmentsKeys;
                        List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysListEditado
                                = new ArrayList<PropertiesPlansTreatmentsKey>();
                        propertiesPlansTreatementsKeysListEditado = plansTreatmentsKeys.
                                getPropertiesPlansTreatmentsKeyList();
                        plansTreatmentsKeysEditado.setPropertiesPlansTreatmentsKeyList(
                                new ArrayList<PropertiesPlansTreatmentsKey>());

                        plansTreatmentsKeysRegistrado = plansTreatmentsKeysFacadeLocal.
                                create(plansTreatmentsKeysEditado);
                        this.createBinnaclePlansTreatmentsKeys(plansTreatmentsKeysRegistrado, EstatusKeysPatients.GENERADO.getValor());
                        plansTreatmentsKeysRegistrado.setPropertiesPlansTreatmentsKeyList(
                                propertiesPlansTreatementsKeysListEditado);
                        this.getPlansTreatmentsKeysViewBean().setEditPlansTreatmentsKeys(plansTreatmentsKeysRegistrado);
                    } else {
                        plansTreatmentsKeys.setId(plansTreatmentsKeysFind.getId());
                        this.createBinnaclePlansTreatmentsKeys(plansTreatmentsKeys, EstatusKeysPatients.GENERADO.getValor());
                        this.getPlansTreatmentsKeysViewBean().setEditPlansTreatmentsKeys(plansTreatmentsKeys);

                    }

                    /**
                     * create createBinnaclePlansTreatmentsKeys
                     */
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList() != null) {
                        for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey : plansTreatmentsKeys.
                                getPropertiesPlansTreatmentsKeyList()) {
                            propertiesPlansTreatmentsKey.setPlansTreatmentsKeys(plansTreatmentsKeysRegistrado);
                            propertiesPlansTreatmentsKey.setDate_properties(dateActual);
                            propertiesPlansTreatmentsKey.setIdScalePropertiesTreatments(
                                    this.scalePropertiesTreatmentsFacade.findByPropertieTreatment(
                                            propertiesPlansTreatmentsKey.getPropertiesTreatments().getId(),
                                            this.keysPatientsViewBean.getKeysPatientsEdit().getSpecialistId().getId()));
                            PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKeys
                                    = this.propertiesPlansTreatmentsKeyFacadeLocal.create(propertiesPlansTreatmentsKey);
                            /**
                             * create createBinnaclePropertiesList
                             */
                            this.createBinnaclePropertiesTreatmentsKeys(propertiesPlansTreatmentsKeys, status.getId());
                        }
                    }
                }
                if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList() != null && plansTreatmentsKeys.
                        getPropertiesPlansTreatmentsKeyList().size() > 0) {
                    this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().clear();
                    //Lista que permite luego validar si se deseleccionaron piezas y eliminarlas de la bd
                    this.getPropertiesPlansTreatmentsKeyRequestBean().
                            setListPropertiesPlansTreatmentsKeySeleccionados(plansTreatmentsKeys.
                                    getPropertiesPlansTreatmentsKeyList());

                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNew().size() > 0) {

                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNew());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewII().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewII());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIII().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIII());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIV().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIV());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewV().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewV());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVI().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVI());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVII().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVII());
                    }
                    if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVIII().size() > 0) {
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().
                                addAll(plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVIII());
                    }

                    if (!this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSelectedNews().isEmpty()) {
                        //Método para evaluar si las piezas fueron deseleccionadas en la edición , 
                        //si no las encuentra entonces las que están en BD las deja en false para elimnarlas 
                        int i = 0;
                        while (i < this.getPropertiesPlansTreatmentsKeyRequestBean().
                                getListPropertiesPlansTreatmentsKeySeleccionados().size()) {
                            PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = this.
                                    getPropertiesPlansTreatmentsKeyRequestBean().
                                    getListPropertiesPlansTreatmentsKeySeleccionados().get(i);
                            Boolean encontrado = false;
                            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().
                                    getListPropertiesTreatmentsSelectedNews()) {
                                if (propertiesPlansTreatmentsKey.getPropertiesTreatments().getPropertiesId().getId().
                                        equals(propertiesTreatments.getPropertiesId().getId())) {
                                    propertiesTreatments.setEncontrado(true);
                                    encontrado = true;
                                }
                            }
                            if (encontrado == false) {
                                this.getPropertiesPlansTreatmentsKeyRequestBean().
                                        getListPropertiesPlansTreatmentsKeySeleccionados().
                                        remove(propertiesPlansTreatmentsKey);
                                //this.propertiesPlansTreatmentsKeyFacadeLocal.remove(propertiesPlansTreatmentsKey);
                                StatusAnalysisKeys statusE = this.statusAnalysisKeysFacadeLocal.
                                        findById(EstatusKeysPatients.ELIMINADO.getValor());
                                this.createBinnaclePropertiesTreatmentsKeys(propertiesPlansTreatmentsKey,
                                        statusE.getId());
                                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList().
                                        remove(propertiesPlansTreatmentsKey);

                                if (i != 0) {
                                    i--;
                                } else {
                                    i = 0;
                                }
                            } else {
                                i++;
                            }

                        }

                        for (PropertiesTreatments propertiesTreatment : this.getPropertiesTreatmentsViewBean().
                                getListPropertiesTreatmentsSelectedNews()) {
                            System.err.println("Id BD propertiesTreatmentsKey " + propertiesTreatment.getId());
                            if (!propertiesTreatment.getEncontrado()) {
                                PropertiesPlansTreatmentsKey propertiesTreatmentsKey = new PropertiesPlansTreatmentsKey();
                                propertiesTreatmentsKey.setPropertiesTreatments(propertiesTreatment);
                                Date dateCurrent = obtainsDateActual();
                                propertiesTreatmentsKey.setDate_properties(dateCurrent);
                                propertiesTreatmentsKey.setPlansTreatmentsKeys(plansTreatmentsKeys);
                                propertiesTreatmentsKey.setIdScalePropertiesTreatments(
                                        this.scalePropertiesTreatmentsFacade.
                                        findByPropertieTreatment(propertiesTreatmentsKey.
                                                getPropertiesTreatments().getId(),
                                                this.keysPatientsViewBean.getKeysPatientsEdit().
                                                getSpecialistId().getId()));

                                propertiesTreatmentsKey = this.propertiesPlansTreatmentsKeyFacadeLocal.
                                        create(propertiesTreatmentsKey);
                                /**
                                 * create createBinnaclePropertiesList
                                 */
                                this.createBinnaclePropertiesList(plansTreatmentsKeys.getKeysId().getId(),
                                        EstatusKeysPatients.GENERADO.getValor());
                                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList().add(propertiesTreatmentsKey);

                            }
                        }

                    } else {

                        context.update("form:campoValor");
                        RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe seleccionar al menos una pieza del tratamiento"
                                + plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getName()));
                        piezasvacias = true;
                        this.getKeysPatientsViewBean().setCreate(false);
                    }

                    //}
                }

                plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyList(this.propertiesPlansTreatmentsKeyFacadeLocal.
                        findByPlansTreatmentsKeys(plansTreatmentsKeys.getId()));

            }

            this.getKeysPatientsViewBean().setKeysPatientsNew(this.keysPatientsViewBean.getKeysPatientsEdit());

            if (!piezasvacias) {

                KeysPatients keysPatientsModificado = (KeysPatients) this.keysPatientsFacadeLocal.edit(this.
                        getKeysPatientsViewBean().getKeysPatientsNew());
                log.log(Level.INFO, "Edité");
                this.accionesRequestBean.cerrarDialogoView(idDialog);

                FacesContext contexto = FacesContext.getCurrentInstance();
                //contexto.addMessage(null, new FacesMessage("Éxito", "¡Cambios realizados satisfactoriamente!"));
                contexto.addMessage(null, new FacesMessage("¡Éxito!", "Cambios Efectuados Satisfactoriamente"));
                this.getKeysPatientsViewBean().setCreate(false);
                if (this.keysPatientsViewBean.getSpecialist() != null) {
                    context.update("form:keysSpecialistGrow");
                    context.update("form:keysSpecialistsList");

                } else {
                    context.update("form:growlKeysEdit");
                    context.update("form:listaKeys");
                }

            }

        } else {
            context.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Error", "Debe elegir un tratamiento"));
            this.getKeysPatientsViewBean().setCreate(false);
        }
        this.getPlansPatientsViewBean().setListFilterPlansPatients(new ArrayList<PlansPatients>());
        this.getPlansTreatmentsViewBean().setListPlansTreatments(new ArrayList<PlansTreatments>());
        this.getPlansTreatmentsViewBean().setListPlansTreatmentsValidate(new ArrayList<PlansTreatments>());

        this.getPropertiesTreatmentsViewBean().limpiarLista();
        this.getKeysPatientsViewBean().limpiarListaProperties();

        if (this.keysPatientsViewBean.getSpecialist() != null) {
            this.limpiar("keysSpecialistEditDialog");

        } else {
            this.limpiar("keysDetailsDialog");
        }
        this.getKeysPatientsViewBean().setCreate(false);
    }

    public void llenarMapasString(List<String> propertiesStringList) {

        for (String propertiesEdit : propertiesStringList) {
            this.getPropertiesI().put(propertiesEdit, 0);
        }

    }

    public void llenarMapasStringNew(List<PropertiesTreatments> propertiesStringList) {

        for (PropertiesTreatments propertiesEditbefore : propertiesStringList) {
            this.getPropertiesNew().put(propertiesEditbefore, 0);
        }
    }

    public void llenarMapasStringEdit(List<PropertiesTreatments> propertiesStringList) {

        for (PropertiesTreatments propertiesEditnew : propertiesStringList) {
            //this.getPropertiesEdit().putAll(propertiesStringList.);
            this.getPropertiesEdit().put(propertiesEditnew, 0);
        }
    }

    public void limpiar(String idDialog) {
        this.getPropertiesTreatmentsViewBean().setSelectedOptionsAllEdit(new ArrayList<PropertiesTreatments>());

        this.getSpecialistsViewBean().getEditSpecialists()[1] = null;
        this.getSpecialistsViewBean().getEditSpecialists()[6] = null;
        this.getTypeSpecialistViewBean().setEditTypeSpecialist(new TypeSpecialist());
        this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(null);
        this.getPatientsViewBean().getEditPatients()[6] = null;
        this.getPatientsViewBean().getEditPatients()[1] = null;
        this.getPatientsViewBean().getEditPatients()[2] = null;
        this.getPropertiesTreatmentsViewBean().setVisibleList(false);
        this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
        this.getPlansPatientsViewBean().setNewListPlansPatients(new ArrayList<PlansPatients>());
        this.getPlansPatientsViewBean().setListFilterPlansPatients(new ArrayList<PlansPatients>());
        this.getListaInsurePatients().clear();
        this.getPlansTreatmentsViewBean().setListPlansTreatments(new ArrayList<PlansTreatments>());
        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(new PlansTreatments());
        this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());

        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
        RequestContext contextC = RequestContext.getCurrentInstance();
        contextC.update("form:" + idDialog);
        contextC.execute("PF('" + idDialog + "').hide()");
        this.getPropertiesTreatmentsViewBean().limpiarLista();
        contextC.update("form:lstTreatmentII");
        this.getKeysPatientsViewBean().init();
        if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {

            this.getKeysPatientsViewBean().limpiarListaProperties();
            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().
                    getListPlansTreatmentsKeysSeleccionados()) {

                plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().setScaleTreatmentsListListFilter(
                        new ArrayList<ScaleTreatments>());

            }

            this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(
                    new ArrayList<PlansTreatmentsKeys>());

            this.getPlansTreatmentsViewBean().setListPlansTreatmentsValidate(new ArrayList<PlansTreatments>());
            this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysFiltrada(this.
                    getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados());
            if (this.getPlansPatientsViewBean().getNewListPlansTreatments() != null) {
                for (PlansTreatments planTreatments : this.getPlansPatientsViewBean().getNewListPlansTreatments()) {
                    for (ScaleTreatments scaleTreatments : planTreatments.getTreatmentsId().getScaleTreatmentsList()) {
                        if (scaleTreatments.getIdTypeScale() != null) {

                            planTreatments.getTreatmentsId().setScaleTreatmentsListListFilter(
                                    new ArrayList<ScaleTreatments>());
                            planTreatments.getTreatmentsId().setPriceString("");
                        }
                    }

                }
            }

        }
        this.cleanerPropertiesTreatments();
    }

    public void cleanerPropertiesTreatments() {

        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatments().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsII().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIII().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIV().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsV().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVI().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVII().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVIII().clear();

    }

    public void actualizarScalePropertiesEditKey(String id, int indice) {
        int cantidad = 0;
        int tamano = 0;
        Double cantidadLong = cantidad * 1.0;

        PlansTreatmentsKeys planstreatmentsKeys = this.getPlansTreatmentsKeysViewBean().
                getListPlansTreatmentsKeysSeleccionados().get(indice);
        if (!planstreatmentsKeys.getPropertiesPlansTreatmentsKeyList().isEmpty()) {
            tamano = planstreatmentsKeys.getPropertiesPlansTreatmentsKeyList().size();
            cantidad = planstreatmentsKeys.getPropertiesPlansTreatmentsKeyListNew().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewII().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewIII().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewIV().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewV().size();
            cantidad += planstreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVI().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewVII().size() + planstreatmentsKeys.
                    getPropertiesPlansTreatmentsKeyListNewVIII().size();
            cantidadLong = cantidad * 1.0;

            Double precioBaremo = planstreatmentsKeys.getPlansTreatmentsId().
                    getTreatmentsId().getPriceNew() * cantidadLong;
            Double ini = 0.00;
            planstreatmentsKeys.getScaleTreatmentsKeys().setPrice(ini);
            DecimalFormat decf = new DecimalFormat("0.00");
            String precio = decf.format(precioBaremo);
            planstreatmentsKeys.getScaleTreatmentsKeys().setPrice(precioBaremo);
            planstreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().clear();
            planstreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().
                    add(planstreatmentsKeys.getScaleTreatmentsKeys());

        } else {
            context.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Error", "Debe elegir al menos una pieza"));
        }

    }

    public void navigationKey() throws IOException {

        if (this.getKeysPatientsViewBean().obtainUserSpecialists()) {
            this.getAccionesRequestBean().navigationApplication("index");
        } else {
            this.getAccionesRequestBean().navigation("index");
        }
    }

    /**
     * method that create the binnacleKeys
     *
     * @param keysPatientsRegistrado
     * @param state
     */
    public void createBinnacleKeys(KeysPatients keysPatientsRegistrado, Integer state) {
        log.log(Level.INFO, "createBinnacleKeys()");
        BinnacleKeys binnacleKeysRegistrado = new BinnacleKeys();
        Users user = this.securityViewBean.obtainUser();
        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(state);
        keysPatientsRegistrado.setStatus(status);
        binnacleKeysRegistrado.setIdKeys(keysPatientsRegistrado);
        binnacleKeysRegistrado.setDateBinnacleInitiation(keysPatientsRegistrado.getApplicationDate());
        binnacleKeysRegistrado.setDateBinnacleEnd(keysPatientsRegistrado.getApplicationDate());
        binnacleKeysRegistrado.setIdStatusKeys(status);
        binnacleKeysRegistrado.setIdUsersKeys(user);
        this.keysPatientsFacadeLocal.edit(keysPatientsRegistrado);
        this.binnacleKeysFacadeLocal.create(binnacleKeysRegistrado);

    }

    /**
     * method that create the binnaclePlansTreatmentsKeys
     *
     * @param plansTreatmentsKeysRegistrado
     * @param state
     * @throws java.text.ParseException
     */
    public void createBinnaclePlansTreatmentsKeys(PlansTreatmentsKeys plansTreatmentsKeysRegistrado, Integer state)
            throws ParseException {
        log.log(Level.INFO, "createBinnaclePlansTreatmentsKeys()");
        BinnaclePlansTreatmentsKeys binnaclePlansTreatmentsKeys = new BinnaclePlansTreatmentsKeys();
        Users user = this.securityViewBean.obtainUser();
        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(state);
        Date dateActual = this.obtainsDateActual();
        plansTreatmentsKeysRegistrado.setIdStatus(status);
        binnaclePlansTreatmentsKeys.setIdPlansTreatmentsKeys(plansTreatmentsKeysRegistrado);
        binnaclePlansTreatmentsKeys.setDateBinnacleInitiation(dateActual);
        binnaclePlansTreatmentsKeys.setDateBinnacleEnd(dateActual);
        binnaclePlansTreatmentsKeys.setIdStatusPlans(status);
        binnaclePlansTreatmentsKeys.setIdUsers(user);
        this.plansTreatmentsKeysFacadeLocal.edit(plansTreatmentsKeysRegistrado);
        this.binnaclePlansTreatmentsKeysFacade.create(binnaclePlansTreatmentsKeys);

    }

    /**
     * method that create the binnaclePlansTreatmentsKeys
     *
     * @param propertiesPlansTreatmentsKeyRegistrado
     * @param state
     * @throws java.text.ParseException
     */
    public void createBinnaclePropertiesTreatmentsKeys(PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKeyRegistrado, Integer state)
            throws ParseException {
        log.log(Level.INFO, "createBinnaclePropertiesTreatmentsKeys()");
        BinnaclePropertiesTreatmentsKeys binnaclePropertiesTreatmentsKeys = new BinnaclePropertiesTreatmentsKeys();
        Users user = this.securityViewBean.obtainUser();
        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(state);
        Date dateActual = this.obtainsDateActual();
        propertiesPlansTreatmentsKeyRegistrado.setDate_properties(dateActual);
        propertiesPlansTreatmentsKeyRegistrado.setIdStatus(status);
        binnaclePropertiesTreatmentsKeys.setIdPropertiesPlansTreatmentsKey(propertiesPlansTreatmentsKeyRegistrado);
        binnaclePropertiesTreatmentsKeys.setDateBinnacleInitiation(dateActual);
        binnaclePropertiesTreatmentsKeys.setDateBinnacleEnd(dateActual);
        binnaclePropertiesTreatmentsKeys.setIdStatusProperties(status);
        binnaclePropertiesTreatmentsKeys.setIdUsers(user);
        this.propertiesPlansTreatmentsKeyFacadeLocal.edit(propertiesPlansTreatmentsKeyRegistrado);
        this.binnaclePropertiesTreatmentsKeysFacade.create(binnaclePropertiesTreatmentsKeys);

    }

    /**
     * method for get the date actual in format dd/MM/yyyy HH:mm:ss
     *
     * @return Date
     * @throws java.text.ParseException
     */
    public Date obtainsDateActual() throws ParseException {

        Date fecha = new Date();
        SimpleDateFormat sdfClave = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fechaClave = sdfClave.format(fecha);
        String fechaSolicitud = sdfSolicitud.format(fecha);
        Date dateApplication = Transformar.StringToDate(fechaSolicitud);
        return dateApplication;
    }

    /**
     * method that create the binnaclePropertiesPlansTreatmentsKeys of a KeysId
     *
     * @param keysId
     * @param status
     * @throws java.text.ParseException
     */
    public void createBinnaclePropertiesList(Integer keysId, Integer status) throws ParseException {
        this.setPropertiesPlansTreatementsKeysList(this.propertiesPlansTreatmentsKeyFacadeLocal.findByKeys(keysId));
        if (!this.getPropertiesPlansTreatementsKeysList().isEmpty()) {
            for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey
                    : this.getPropertiesPlansTreatementsKeysList()) {

                this.createBinnaclePropertiesTreatmentsKeys(propertiesPlansTreatmentsKey, status);

            }
        }
    }

    /**
     * // * Metodo que permite eliminar una clave logicamente // * // * @param ConsultKeysPatientsOutput // * @throws
     * SQLException //
     */
    public void deleteKeyPatient(String dialogo) throws SQLException, ParseException {
        Integer generate = EstatusPlansTreatmentsKeys.GENERADO.getValor();
        Integer delete = EstatusPlansTreatmentsKeys.ELIMINADO.getValor();
        Integer key = this.getKeysPatientsViewBean().getIdKeyDelete();
        Integer idUser = this.securityViewBean.obtainUser().getId();
        String observationDelete = this.getKeysPatientsViewBean().getReasonDelete()[1];
        this.serviceKeysPatients.reasonDeleteKey(key, observationDelete, idUser);
//        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("","Operación realizada con éxito"));
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        this.getAccionesRequestBean().cerrarDialogoView(dialogo);
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        RequestContext.getCurrentInstance().update("form:keysConsultDialogDetails");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:keysPatientsConsults");
        dataTable.reset();
        this.consultKeysPatients();
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        this.getKeysPatientsViewBean().selectSalir();

    }

    public void consultKeysPatients() throws ParseException {
        
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:keysPatientsConsults");
        dataTable.reset();
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");

        Integer idstatusKey;
        if ((this.getKeysPatientsViewBean().getStartDateConsult() == null) && (this.getKeysPatientsViewBean().getEndDateConsult() == null)
                && "".equals(this.getKeysPatientsViewBean().getQueryPatient()) && "".equals(this.getKeysPatientsViewBean().getNumberKey())
                && "".equals(this.getKeysPatientsViewBean().getQuerySpecialist()) && this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect() == null) {
            this.getKeysPatientsViewBean().reset();

        } else {
            String dateS = null;
            String dateE = null;
            if (this.getKeysPatientsViewBean().getStartDateConsult() != null && this.getKeysPatientsViewBean().getEndDateConsult() != null) {
                java.sql.Date dateStartSql = null;
                java.sql.Date dateEndSql = null;
                dateStartSql = this.getKeysPatientsViewBean().convertJavaDateToSqlDate(
                        this.getKeysPatientsViewBean().getStartDateConsult());
                dateEndSql = this.getKeysPatientsViewBean().convertJavaDateToSqlDate(
                        this.getKeysPatientsViewBean().getEndDateConsult());
                SimpleDateFormat sdfClave = new SimpleDateFormat("yyyy-MM-dd");
                dateS = sdfClave.format(dateStartSql);
                dateE = sdfClave.format(dateEndSql);
            } else {
                this.getKeysPatientsViewBean().setStartDateConsult(null);
                this.getKeysPatientsViewBean().setEndDateConsult(null);
            }
            if ("".equals(this.getKeysPatientsViewBean().getQueryPatient())) {
                this.getKeysPatientsViewBean().setQueryPatient(null);
            }
            if ("".equals(this.getKeysPatientsViewBean().getNumberKey())) {
                this.getKeysPatientsViewBean().setNumberKey(null);
            }
            if ("".equals(this.getKeysPatientsViewBean().getQuerySpecialist())) {
                this.getKeysPatientsViewBean().setQuerySpecialist(null);
            }
            if (this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect() == null) {
                idstatusKey = null;
            } else {
                idstatusKey = this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect().getIdstatusKey();
            }

            this.validateSearch();
            this.getKeysPatientsViewBean().setConsultKeysPatientsOutput(this.serviceKeysPatients.consultKeysPatients(dateS, dateE, this.getKeysPatientsViewBean().getNumberKey(),
                    this.getKeysPatientsViewBean().getQueryPatient(), this.getKeysPatientsViewBean().getQuerySpecialist(),
                    idstatusKey));
            Integer idUser = this.securityViewBean.obtainUser().getId();
            this.getKeysPatientsViewBean().setConsultUserRolesOutput(this.serviceKeysPatients.searchUserRoles(idUser));
            this.getKeysPatientsViewBean().setConsultKeysPatientsOutputFilter(this.getKeysPatientsViewBean().getConsultKeysPatientsOutput());
            if(this.getKeysPatientsViewBean().getConsultKeysPatientsOutput().isEmpty()){
                this.getKeysPatientsViewBean().setMessageDataTable(false);
                this.getKeysPatientsViewBean().setDisabledBtnBuscar(true);
                this.getKeysPatientsViewBean().setDisabledBtnRest(false);
                
            }else{
//                this.getKeysPatientsViewBean().setMessageDataTable(false);
            }
            RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
            RequestContext.getCurrentInstance().update("form:dateKeyPatients");
            RequestContext.getCurrentInstance().update("form:keys");
            RequestContext.getCurrentInstance().update("form:patients");
            RequestContext.getCurrentInstance().update("form:identityNumberPatients");
            RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
            RequestContext.getCurrentInstance().update("form:searchkey");
            RequestContext.getCurrentInstance().update("form:state");
            RequestContext.getCurrentInstance().update("form:specialist");
            RequestContext.getCurrentInstance().update("form:identityNumberSpecialist");
            RequestContext.getCurrentInstance().update("form:observation");
             RequestContext.getCurrentInstance().update("form:btnFindKey");
              RequestContext.getCurrentInstance().update("form:btnResetKey");
        }

    }

    public void validateSearch() {
        Integer idState = null;
        String nameState = null;
        if (this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect() == null) {
            idState = null;
            nameState = null;
        } else {
            idState = this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect().getIdstatusKey();
            nameState = this.getKeysPatientsViewBean().getLoadKeyStatesOutputselect().getNameStatusKeys();
        }

        //Condicional cuando solo fecha desde y fecha hasta sea diferente de null      
        if (this.getKeysPatientsViewBean().getStartDateConsult() != null && this.getKeysPatientsViewBean().getEndDateConsult() != null
                && this.getKeysPatientsViewBean().getNumberKey() == null && this.getKeysPatientsViewBean().getQueryPatient() == null
                && this.getKeysPatientsViewBean().getQuerySpecialist() == null && idState == null) {
            this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
            this.getKeysPatientsViewBean().setDisabledColumKey(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
            this.getKeysPatientsViewBean().setDisabledColumObservation(false);
            this.getKeysPatientsViewBean().setDisabledColumPatient(true);
            this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
            this.getKeysPatientsViewBean().setDisabledColumState(false);
        }
        //Condicional cuando solo clave sea diferente de null
        if (this.getKeysPatientsViewBean().getNumberKey() != null && this.getKeysPatientsViewBean().getStartDateConsult() == null
                && this.getKeysPatientsViewBean().getEndDateConsult() == null && this.getKeysPatientsViewBean().getQueryPatient() == null
                && this.getKeysPatientsViewBean().getQuerySpecialist() == null && idState == null) {
            this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
            this.getKeysPatientsViewBean().setDisabledColumKey(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
            this.getKeysPatientsViewBean().setDisabledColumObservation(false);
            this.getKeysPatientsViewBean().setDisabledColumPatient(true);
            this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
            this.getKeysPatientsViewBean().setDisabledColumState(false);

        }
        //Condicional cuando solo paciente sea diferente de null
        if (this.getKeysPatientsViewBean().getNumberKey() == null && this.getKeysPatientsViewBean().getStartDateConsult() == null
                && this.getKeysPatientsViewBean().getEndDateConsult() == null && this.getKeysPatientsViewBean().getQueryPatient() != null
                && this.getKeysPatientsViewBean().getQuerySpecialist() == null && idState == null) {
            this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
            this.getKeysPatientsViewBean().setDisabledColumKey(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
            this.getKeysPatientsViewBean().setDisabledColumObservation(false);
            this.getKeysPatientsViewBean().setDisabledColumPatient(true);
            this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
            this.getKeysPatientsViewBean().setDisabledColumState(false);

        }
        //Condicional cuando solo especialista sea diferente de null
        if (this.getKeysPatientsViewBean().getNumberKey() == null && this.getKeysPatientsViewBean().getStartDateConsult() == null
                && this.getKeysPatientsViewBean().getEndDateConsult() == null && this.getKeysPatientsViewBean().getQueryPatient() == null
                && this.getKeysPatientsViewBean().getQuerySpecialist() != null && idState == null) {
            this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
            this.getKeysPatientsViewBean().setDisabledColumKey(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
            this.getKeysPatientsViewBean().setDisabledColumNumberS(false);
            this.getKeysPatientsViewBean().setDisabledColumObservation(false);
            this.getKeysPatientsViewBean().setDisabledColumPatient(true);
            this.getKeysPatientsViewBean().setDisabledColumSpecialist(false);
            this.getKeysPatientsViewBean().setDisabledColumState(false);

        }
        //Condicional cuando solo selecciona el combo estado

        if (this.getKeysPatientsViewBean().getNumberKey() == null && this.getKeysPatientsViewBean().getStartDateConsult() == null
                && this.getKeysPatientsViewBean().getEndDateConsult() == null && this.getKeysPatientsViewBean().getQueryPatient() == null
                && this.getKeysPatientsViewBean().getQuerySpecialist() == null && idState != null) {
            //Condicional cuando la opcion seleccionada es Todos los estados
            if (nameState.equals("TODOS LOS ESTADOS")) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
                this.getKeysPatientsViewBean().setDisabledColumObservation(false);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
                this.getKeysPatientsViewBean().setDisabledColumState(true);
                //Condicional cuando la opcion seleccionada es diferente a todos los estados y eliminado
            } else if (nameState.equals("GENERADO")
                    || nameState.equals("RECIBIDO")
                    || nameState.equals("POR FACTURAR")
                    || nameState.equals("POR ABONAR")
                    || nameState.equals("ABONADO")) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
                this.getKeysPatientsViewBean().setDisabledColumObservation(false);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
                this.getKeysPatientsViewBean().setDisabledColumState(false);
                //Condicional cuando la opcion seleccionada es eliminado
            } else if (nameState.equals("ELIMINADO")) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
                this.getKeysPatientsViewBean().setDisabledColumObservation(true);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
                this.getKeysPatientsViewBean().setDisabledColumState(false);
            }

            RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
            RequestContext.getCurrentInstance().update("form:dateKeyPatients");
            RequestContext.getCurrentInstance().update("form:keys");
            RequestContext.getCurrentInstance().update("form:patients");
            RequestContext.getCurrentInstance().update("form:identityNumberPatients");
            RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
            RequestContext.getCurrentInstance().update("form:state");
            RequestContext.getCurrentInstance().update("form:specialist");
            RequestContext.getCurrentInstance().update("form:identityNumberSpecialist");
            RequestContext.getCurrentInstance().update("form:observation");
        }

        if (idState != null) {

        //Condicional cuando la opcion seleccionada es todos los estados y se filtra tambien por especialista
            if (nameState.equals("TODOS LOS ESTADOS") && this.getKeysPatientsViewBean().getQuerySpecialist() != null) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(false);
                this.getKeysPatientsViewBean().setDisabledColumObservation(false);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(false);
                this.getKeysPatientsViewBean().setDisabledColumState(true);

            }
            //Condicional cuando la opcion seleccionada es eliminado y se filtra por especialista
            if (nameState.equals("ELIMINADO") && this.getKeysPatientsViewBean().getQuerySpecialist() != null) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(false);
                this.getKeysPatientsViewBean().setDisabledColumObservation(true);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(false);
                this.getKeysPatientsViewBean().setDisabledColumState(false);
            }
            //Condicional cuando no se toma en cuenta al especialista,estado eliminado Y todos los estados
            if (this.getKeysPatientsViewBean().getQuerySpecialist() == null && !"ELIMINADO".equals(nameState)
                    && !"TODOS LOS ESTADOS".equals(nameState)) {
                this.getKeysPatientsViewBean().setDisabledColumDateKey(true);
                this.getKeysPatientsViewBean().setDisabledColumKey(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberP(true);
                this.getKeysPatientsViewBean().setDisabledColumNumberS(true);
                this.getKeysPatientsViewBean().setDisabledColumObservation(false);
                this.getKeysPatientsViewBean().setDisabledColumPatient(true);
                this.getKeysPatientsViewBean().setDisabledColumSpecialist(true);
                this.getKeysPatientsViewBean().setDisabledColumState(false);
            }

        }

        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        RequestContext.getCurrentInstance().update("form:dateKeyPatients");
        RequestContext.getCurrentInstance().update("form:keys");
        RequestContext.getCurrentInstance().update("form:patients");
        RequestContext.getCurrentInstance().update("form:identityNumberPatients");
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        RequestContext.getCurrentInstance().update("form:state");
        RequestContext.getCurrentInstance().update("form:specialist");
        RequestContext.getCurrentInstance().update("form:identityNumberSpecialist");
        RequestContext.getCurrentInstance().update("form:observation");
    }

    public KeysPatientsRegisterViewBean getKeysPatientsRegisterViewBean() {
        return keysPatientsRegisterViewBean;
    }

    public void setKeysPatientsRegisterViewBean(KeysPatientsRegisterViewBean keysPatientsRegisterViewBean) {
        this.keysPatientsRegisterViewBean = keysPatientsRegisterViewBean;
    }
    
    
     public void selectNoreasonDelete(String idDialog){
         if(this.getKeysPatientsViewBean().getReasonDeleteOpen()==1){
             this.accionesRequestBean.cerrarDialogoView(idDialog);
        FacesContext contexto = FacesContext.getCurrentInstance();
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
         }else{
             this.accionesRequestBean.desplegarDialogoView("keysPatientsReasonDelete");
         
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
         }
        
    }
     
     public void reasonDeleteKey(){
         this.accionesRequestBean.desplegarDialogoView("keysPatientsReasonDelete");
         
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        this.getKeysPatientsViewBean().setReasonDeleteOpen(1);
     }
}
