/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.controller.application.converterReport;

import com.venedental.controller.converter.*;
import com.venedental.controller.view.PropertiesViewBean;
import com.venedental.controller.viewReport.AuditViewBeanReport;
import com.venedental.dto.ReadPropertiesReportOutput;
import com.venedental.dto.ReadTreatmentsReportOutput;
import com.venedental.model.Properties;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import services.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "propertiesConverterReport", forClass = Properties.class)
public class PropertiesConverterReport implements Converter {
    
    private static final Logger log = CustomLogger.getGeneralLogger(PropertiesConverterReport.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        
        AuditViewBeanReport propertiesViewBean = (AuditViewBeanReport) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "auditViewBeanReport");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (ReadPropertiesReportOutput properties : propertiesViewBean.getListaProperties()) {
                    if(properties!= null){
                        if (properties.getIdProperties()== numero) {
                            return properties;
                        }
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Pieza Invalida"));
            }
        }
        return null;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((ReadPropertiesReportOutput) value).getIdProperties());
        }
        
    }
    
}
