/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.read;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.exceptions.NotDirectoryException;
import com.venedental.carga_masiva.exceptions.NonExistentDirectoryException;
import com.venedental.carga_masiva.exceptions.EmptyDirectoryException;
import com.venedental.carga_masiva.exceptions.MessageException;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class DirectoryReader {

    private static final Logger log = CustomLogger.getLogger(DirectoryReader.class.getName());
    private final FileReader fileReader;

    public DirectoryReader(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    public void execute(String path, File entrada, String[] fieldTemplate) throws MessageException {
        try {
            validate(path, entrada);
            read(path, entrada, fieldTemplate);
        } catch (MessageException e) {
            log.log(Level.INFO, e.getError());
        }
    }

    protected void validate(String path, File input) throws MessageException {
        log.log(Level.INFO, "Validando directorio: {0}", input.getPath());
        if (input.exists()) {
            if (input.isDirectory()) {
                if (input.list().length == 0) {
                    throw new EmptyDirectoryException(input);
                }
            } else {
                throw new NotDirectoryException(input);
            }
        } else {
            throw new NonExistentDirectoryException(input);
        }
        log.log(Level.INFO, "Directorio valido");
    }

    protected void read(String pathDirectory, File fileDirectory, String[] fieldTemplate) throws MessageException {
        log.log(Level.INFO, "Inicio - Lectura de directorio: {0}", fileDirectory.getPath());
        String[] insurancesFiles = fileDirectory.list();
        for (String fileName : insurancesFiles) {
            if (!fileName.equals(Constants.PROCESSED)) {
                String pathFile = fileDirectory.getAbsolutePath() + Constants.DIRECTORY_SEPARATOR + fileName;
                File fileFile = new File(pathFile);
                try {
                    fileReader.execute(pathDirectory, fileDirectory, pathFile, fileFile, fieldTemplate);
                } catch (MessageException e) {
                    log.log(Level.WARNING, e.getError());
                }
            }
        }
        log.log(Level.INFO, "Fin - Lectura de directorio");
    }

}
