/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.AddressesViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.ReceiveKeyAnalysisViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.KeysPatients;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

/**
 * Class that can perform the basic operations in the database relating to the
 * first stage of analysis of key Phase I
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "receiveKeysAnalysisRequestBean")
@RequestScoped
public class ReceiveKeysAnalysisRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{receiveKeyAnalysisViewBean}")
    private ReceiveKeyAnalysisViewBean receiveKeyAnalysisViewBean;   
    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;    
    
    private static final Logger log = CustomLogger.getGeneralLogger(ReceiveKeysAnalysisRequestBean.class.getName());
       
    @EJB
    private ServiceKeysPatients serviceKeysPatients;
    
    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;
    
    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiePlansTreatmentKey;

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public ReceiveKeyAnalysisViewBean getReceiveKeyAnalysisViewBean() {
        return receiveKeyAnalysisViewBean;
    }

    public void setReceiveKeyAnalysisViewBean(ReceiveKeyAnalysisViewBean receiveKeyAnalysisViewBean) {
        this.receiveKeyAnalysisViewBean = receiveKeyAnalysisViewBean;
    }

    /**
     * method to update the status of the treatments received , as well as the
     * key to analysis and update their binnacle
     */
    public void updateKeys() throws SQLException {
        log.log(Level.INFO, "updateKeys()");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        if (!this.getReceiveKeyAnalysisViewBean().getSelectedKeysPatients().isEmpty()) {
            for (KeysPatients keyPatients : this.getReceiveKeyAnalysisViewBean().getSelectedKeysPatients()) {
                this.serviceKeysPatients.updateStatusKeyPatient(keyPatients.getId(), EstatusKeysPatients.RECIBIDO.getValor(), this.getSecurityViewBean().obtainUser().getId());
                updateStatusPlanTreatmentsKeys(keyPatients.getId());
                updateStatusPropertiesPlanTreatmentsKeys(keyPatients.getId());
            }
            contexto.addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));
            requestContext.update("form:growReceiveAnalysisKeys");
            this.getReceiveKeyAnalysisViewBean().filterKeys();
        } else {
            contexto.addMessage(null, new FacesMessage("Error", "Debe seleccionar una clave"));
            requestContext.update("form:growReceiveAnalysisKeys");
        }
    }

    public void updateStatusPlanTreatmentsKeys(Integer idKey) throws SQLException {
        log.log(Level.INFO, "updateStatusPlanTreatmentsKeys()");
        Integer generate = EstatusPlansTreatmentsKeys.GENERADO.getValor();
        List<PlansTreatmentsKeys> plansTreatmentsKeysList = new ArrayList<PlansTreatmentsKeys>();
        plansTreatmentsKeysList = this.servicePlansTreatmentKeys.findPlanTreKeyByKeyAndStatus(idKey, generate, 0);
        if (!plansTreatmentsKeysList.isEmpty()) {
            for (PlansTreatmentsKeys plansTreatmentsKeysR : plansTreatmentsKeysList) {
                if (Objects.equals(plansTreatmentsKeysR.getStatusId(), EstatusPlansTreatmentsKeys.GENERADO.getValor())) {
                    this.servicePlansTreatmentKeys.updateStatus(plansTreatmentsKeysR.getId(), EstatusPlansTreatmentsKeys.RECIBIDO.getValor(), this.getSecurityViewBean().obtainUser().getId(), new Date());
                }
            }
        }
    }

    public void updateStatusPropertiesPlanTreatmentsKeys(Integer idKey) throws SQLException {
        log.log(Level.INFO, "updateStatusPropertiesPlanTreatmentsKeys()");
        Integer generate = EstatusPlansTreatmentsKeys.GENERADO.getValor();
        List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKey = new ArrayList<PropertiesPlansTreatmentsKey>();
        propertiesPlansTreatmentsKey = this.servicePropertiePlansTreatmentKey.findProPlanstkStatusByKeyAndStatus(idKey, generate, null);
        if (!propertiesPlansTreatmentsKey.isEmpty()) {
            for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKeyR : propertiesPlansTreatmentsKey) {
                if (Objects.equals(propertiesPlansTreatmentsKeyR.getIdStatusProTreatments(), EstatusPlansTreatmentsKeys.GENERADO.getValor())) {
                    this.servicePropertiePlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeyR.getId(), EstatusPlansTreatmentsKeys.RECIBIDO.getValor(), this.getSecurityViewBean().obtainUser().getId(), new Date());
                }
            }
        }
    }

}
