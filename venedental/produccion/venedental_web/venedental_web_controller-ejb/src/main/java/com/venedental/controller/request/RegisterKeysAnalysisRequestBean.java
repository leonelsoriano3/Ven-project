/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.RegisterKeysAnalysisViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.PropertiesTreatments;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import com.venedental.dto.analysisKeys.FileNameForUploadOutput;
import com.venedental.dto.propertiesPlanTreatmentKey.PropertiesPlansTreatmentsKeyOutput;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import services.utils.CustomLogger;

/**
 * Class that can perform the basic operations in the database relating to the second stage of analysis of key Phase I
 *
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "registerKeysAnalysisRequestBean")
@RequestScoped
public class RegisterKeysAnalysisRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(RegisterKeysAnalysisRequestBean.class.getName());

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{registerKeysAnalysisViewBean}")
    private RegisterKeysAnalysisViewBean registerKeysAnalysisViewBean;
    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @EJB
    private ServicePlansTreatmentKeys servicePlanTreatmentKey;
    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiePlansTreatmentKey;
    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public RegisterKeysAnalysisViewBean getRegisterKeysAnalysisViewBean() {
        return registerKeysAnalysisViewBean;
    }

    public RequestContext getContext() {
        return context;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public void setRegisterKeysAnalysisViewBean(RegisterKeysAnalysisViewBean registerKeysAnalysisViewBean) {
        this.registerKeysAnalysisViewBean = registerKeysAnalysisViewBean;
    }

    /**
     * Method to register a new treatment to the key , validating the selection of their properties if has
     *
     * @throws java.sql.SQLException
     */
    public void registerPlansTratmentsKeys() throws SQLException {
        PropertiesPlansTreatmentsKey registerpropertiesPlansTreatmentsKey=new PropertiesPlansTreatmentsKey();
        log.log(Level.INFO, "registerPlansTratmentsKeys()");
        if (this.getRegisterKeysAnalysisViewBean().getSelectedPlansTreatment() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Debe seleccionar un tratamiento"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        } else if (this.getRegisterKeysAnalysisViewBean().getSelectedPlansTreatment().getId() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Debe seleccionar un tratamiento"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        } else if (this.getRegisterKeysAnalysisViewBean().getDateTreatment() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Debe seleccionar una fecha"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        } else if ((this.getRegisterKeysAnalysisViewBean().isVisiblePanelPermanent()
                || this.getRegisterKeysAnalysisViewBean().isVisiblePanelTemporary())
                && this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsPermanent().isEmpty()
                && this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsTemporary().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Debe seleccionar "
                    + "al menos una pieza para el tratamiento"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");

        } else {
            Integer idKey = this.getRegisterKeysAnalysisViewBean().getSelectedKey().getId();
            Integer idTreatment = this.getRegisterKeysAnalysisViewBean().getSelectedPlansTreatment().getId();
            PlansTreatmentsKeys plansTreatmentsKeysAux = this.servicePlanTreatmentKey.findByKeyAndPlaTreat(idKey, idTreatment);

            boolean hasProperties = false;
            List<PropertiesTreatments> propertiesTreatmentsList = new ArrayList<>();
            if (!this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsPermanent().isEmpty()) {
                hasProperties = true;
                propertiesTreatmentsList.addAll(this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsPermanent());

            }
            if (!this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsTemporary().isEmpty()) {
                hasProperties = true;
                propertiesTreatmentsList.addAll(this.getRegisterKeysAnalysisViewBean().getSelectedPropertiesTreatmentsTemporary());
            }
            Integer recived = EstatusPlansTreatmentsKeys.RECIBIDO.getValor();
            Integer delete = EstatusPlansTreatmentsKeys.ELIMINADO.getValor();
            Integer refuse = EstatusPlansTreatmentsKeys.RECHAZADO.getValor();
            Integer idUser = this.securityViewBean.obtainUser().getId();
            Date receptionDate = this.getRegisterKeysAnalysisViewBean().getReceptionDate();
            Date dateTreatment = this.getRegisterKeysAnalysisViewBean().getDateTreatment();
            Integer idScaleTreatment = this.getRegisterKeysAnalysisViewBean().findScaleTreatment(null).getIdScale();

            /*CAMBIAR LOS ENUMS DE ID DE LOS TRATAMIENTOS AL HACER MERGE CONTRA GENERAR CLAVES*/
            if (plansTreatmentsKeysAux != null && plansTreatmentsKeysAux.getIdTreatments() != 3
                    && plansTreatmentsKeysAux.getIdTreatments() != 4) {

                if (Objects.equals(plansTreatmentsKeysAux.getStatusId(), delete)
                        || Objects.equals(plansTreatmentsKeysAux.getStatusId(), refuse)) {

                    this.servicePlanTreatmentKey.updateStatusAndScale(plansTreatmentsKeysAux.getId(), recived, idUser,
                            receptionDate, dateTreatment, idScaleTreatment);
                    this.servicePlanTreatmentKey.updateObservation(plansTreatmentsKeysAux.getId(), null);
                    if (hasProperties) {
                        for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsList) {
                            PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput = this.servicePropertiePlansTreatmentKey.findByPlanTreatKeyStatusAndProTreat(
                                    plansTreatmentsKeysAux.getId(), propertiesTreatments.getId(), delete);
                            if (propertiesPlansTreatmentsKeyOutput == null) {
                                propertiesPlansTreatmentsKeyOutput = this.servicePropertiePlansTreatmentKey.findByPlanTreatKeyStatusAndProTreat(
                                        plansTreatmentsKeysAux.getId(), propertiesTreatments.getId(), refuse);
                            }
                            Integer idScaleTreatPro = this.getRegisterKeysAnalysisViewBean().findScaleTreatment(propertiesTreatments.getId()).getIdScale();
                            if (propertiesPlansTreatmentsKeyOutput != null) {
                                this.servicePropertiePlansTreatmentKey.updateStatusAndScale(propertiesPlansTreatmentsKeyOutput.getId(),
                                        recived, idUser, receptionDate, dateTreatment, idScaleTreatPro);
                                this.servicePropertiePlansTreatmentKey.updateObservation(propertiesPlansTreatmentsKeyOutput.getId(), null);
                            } else {
                               registerpropertiesPlansTreatmentsKey= this.servicePropertiePlansTreatmentKey.registerPropertiesPlanTreatmentKey(plansTreatmentsKeysAux.getId(),
                                        propertiesTreatments.getId(), idScaleTreatPro, this.getRegisterKeysAnalysisViewBean().getDateTreatment(), recived,
                                        null, idUser, receptionDate);
                            this.registerNameFileBd(registerpropertiesPlansTreatmentsKey.getId());
                            }
                        }
                    }

                } else {

                    if (plansTreatmentsKeysAux.getIdTreatments() == 1
                            || plansTreatmentsKeysAux.getIdTreatments() == 11
                            || plansTreatmentsKeysAux.getIdTreatments() == 13
                            || plansTreatmentsKeysAux.getIdTreatments() == 14) {

                        PlansTreatmentsKeys createdPlansTreatmentsKeys = this.servicePlansTreatmentKeys.registrerPlansTreatmentsKeys(idKey,
                                idTreatment, dateTreatment, recived, idScaleTreatment, null, idUser, receptionDate);
                        if (hasProperties) {
                            for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsList) {
                                Integer idScaleTreatPro = this.getRegisterKeysAnalysisViewBean().findScaleTreatment(propertiesTreatments.getId()).getIdScale();
                               registerpropertiesPlansTreatmentsKey= this.servicePropertiePlansTreatmentKey.registerPropertiesPlanTreatmentKey(createdPlansTreatmentsKeys.getId(),
                                        propertiesTreatments.getId(), idScaleTreatPro, dateTreatment, recived, null, idUser, receptionDate);
                            this.registerNameFileBd(registerpropertiesPlansTreatmentsKey.getId());
                            }
                        }

                    } else {

                        this.servicePlanTreatmentKey.updateStatusAndScale(plansTreatmentsKeysAux.getId(), recived, idUser,
                                receptionDate, dateTreatment, idScaleTreatment);
                        this.servicePlanTreatmentKey.updateObservation(plansTreatmentsKeysAux.getId(), null);
                        if (hasProperties) {
                            for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsList) {
                                PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput = this.servicePropertiePlansTreatmentKey.findByPlanTreatKeyStatusAndProTreat(
                                        plansTreatmentsKeysAux.getId(), propertiesTreatments.getId(), delete);
                                if (propertiesPlansTreatmentsKeyOutput == null) {
                                    propertiesPlansTreatmentsKeyOutput = this.servicePropertiePlansTreatmentKey.findByPlanTreatKeyStatusAndProTreat(
                                            plansTreatmentsKeysAux.getId(), propertiesTreatments.getId(), refuse);
                                }
                                Integer idScaleTreatPro = this.getRegisterKeysAnalysisViewBean().findScaleTreatment(propertiesTreatments.getId()).getIdScale();
                                if (propertiesPlansTreatmentsKeyOutput != null) {
                                    this.servicePropertiePlansTreatmentKey.updateStatusAndScale(propertiesPlansTreatmentsKeyOutput.getId(),
                                            recived, idUser, receptionDate, dateTreatment, idScaleTreatPro);
                                    this.servicePropertiePlansTreatmentKey.updateObservation(propertiesPlansTreatmentsKeyOutput.getId(), null);
                                } else {
                                   registerpropertiesPlansTreatmentsKey= this.servicePropertiePlansTreatmentKey.registerPropertiesPlanTreatmentKey(plansTreatmentsKeysAux.getId(),
                                            propertiesTreatments.getId(), idScaleTreatPro, this.getRegisterKeysAnalysisViewBean().getDateTreatment(), recived,
                                            null, idUser, receptionDate);
                                    
                               this.registerNameFileBd(registerpropertiesPlansTreatmentsKey.getId());
                                }
                            }
                        }

                    }

                }

            } else {

                PlansTreatmentsKeys createdPlansTreatmentsKeys = this.servicePlansTreatmentKeys.registrerPlansTreatmentsKeys(idKey,
                        idTreatment, dateTreatment, recived, idScaleTreatment, null, idUser, receptionDate);
                if (hasProperties) {
                    for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsList) {
                        Integer idScaleTreatPro = this.getRegisterKeysAnalysisViewBean().findScaleTreatment(propertiesTreatments.getId()).getIdScale();
                        registerpropertiesPlansTreatmentsKey=this.servicePropertiePlansTreatmentKey.registerPropertiesPlanTreatmentKey(createdPlansTreatmentsKeys.getId(),
                                propertiesTreatments.getId(), idScaleTreatPro, dateTreatment, recived, null, idUser, receptionDate);
                    this.registerNameFileBd(registerpropertiesPlansTreatmentsKey.getId());
                    }
                }

            }
            this.getRegisterKeysAnalysisViewBean().clearPropertiesTreatment();
            this.getRegisterKeysAnalysisViewBean().findDateScale();
            this.getRegisterKeysAnalysisViewBean().findPlansTreatments();
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().enabledAddTratment();
            this.getRegisterKeysAnalysisViewBean().setFileNameForUploadOutput(new ArrayList<FileNameForUploadOutput>());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));
            RequestContext.getCurrentInstance().update("form:registerAddTreatment");
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        }

    }
    
    
     public void registerNameFileBd(Integer idplanPropertiesTreatmentKey) throws SQLException{
        
                for (FileNameForUploadOutput fileNameForUploadOutput : this.getRegisterKeysAnalysisViewBean().getFileNameForUploadOutput()) {


                     this.servicePlansTreatmentKeys.InsertFileSupportRegister(idplanPropertiesTreatmentKey, fileNameForUploadOutput.getNameFileRegister());
                
            }
            
    }

    /**
     * Method which saves an observation for treatment
     *
     * @param event
     */
    public void onCellEdit(CellEditEvent event) throws SQLException {
        log.log(Level.INFO, "onCellEdit()");
        if (this.getRegisterKeysAnalysisViewBean().getOptionsProperties().equals(this.getRegisterKeysAnalysisViewBean().getWhitoutProperties())) {
            if (event.getNewValue() != null) {
                if (event.getNewValue().equals("") || event.getNewValue().toString().trim().compareTo("") == 0) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Observación no puede ser vacía"));
                    RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");

                } else {
                    DataTable table = (DataTable) event.getSource();
                    PlansTreatmentsKeys plansTreatmentsKeys = (PlansTreatmentsKeys) table.getRowData();
                    this.servicePlanTreatmentKey.updateObservation(plansTreatmentsKeys.getId(), plansTreatmentsKeys.getObservation());
                    // this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));
                    RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
                    RequestContext.getCurrentInstance().update("form:plansTreatmentsKeysAnalysisRegister");
                }
            }

        } else if (this.getRegisterKeysAnalysisViewBean().getOptionsProperties().equals(this.getRegisterKeysAnalysisViewBean().getWhitProperties())) {
            if (event.getNewValue() != null) {
                if (event.getNewValue().equals("") || event.getNewValue().toString().trim().compareTo("") == 0) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Observación no puede ser vacía"));
                    RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
                } else {
                    DataTable table = (DataTable) event.getSource();
                    PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput = (PropertiesPlansTreatmentsKeyOutput) table.getRowData();
                    this.servicePropertiePlansTreatmentKey.updateObservation(propertiesPlansTreatmentsKeyOutput.getId(), propertiesPlansTreatmentsKeyOutput.getObservation());
                    //this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito", "Operación realizada con éxito"));
                    RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
                    RequestContext.getCurrentInstance().update("form:propertiesPlansTreatmentsKeysAnalysisRegister");
                }
            }
        }
    }

    public void auditGeneral(String statusParameter) throws SQLException {
        log.log(Level.INFO, "auditGeneral()");
        if (this.getRegisterKeysAnalysisViewBean().getOptionsProperties().equals(this.getRegisterKeysAnalysisViewBean().getWhitProperties())) {
            switch (statusParameter) {
                case "audited":
                    auditPropertie();
                    break;
                case "refuse":
                    updateStatusPropertie(EstatusPlansTreatmentsKeys.RECHAZADO.getValor());
                    break;
                case "evaluation":
                    updateStatusPropertie(EstatusPlansTreatmentsKeys.EN_EVALUACION.getValor());
                    break;
                case "delete":
                    deletePropertie();
                    break;
            }
        } else if (this.getRegisterKeysAnalysisViewBean().getOptionsProperties().equals(this.getRegisterKeysAnalysisViewBean().getWhitoutProperties())) {
            switch (statusParameter) {
                case "audited":
                    auditTreatment();
                    break;
                case "refuse":
                    updateStatusTreatment(EstatusPlansTreatmentsKeys.RECHAZADO.getValor());
                    break;
                case "evaluation":
                    updateStatusTreatment(EstatusPlansTreatmentsKeys.EN_EVALUACION.getValor());
                    break;
                case "delete":
                    deleteTreatment();
                    break;
            }
        }

    }

    public void auditPropertie() throws SQLException {
        log.log(Level.INFO, "auditPropertie()");
        if (!this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected().isEmpty()) {
            for (PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput : this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected()) {
                this.servicePropertiePlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeyOutput.getId(),
                        EstatusPlansTreatmentsKeys.AUDITADO.getValor(), this.securityViewBean.obtainUser().getId(), null);
            }
            String message = "Operación realizada con éxito";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
            this.getRegisterKeysAnalysisViewBean().toggleSelect();
            this.getRegisterKeysAnalysisViewBean().setPropertiesPlansTreatmentsKeySelected(new ArrayList<PropertiesPlansTreatmentsKeyOutput>());
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().toggleSelect();
        }
    }

    public void deletePropertie() throws SQLException {
        log.log(Level.INFO, "deletePropertie()");
        if (!this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected().isEmpty()) {
            List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeyAux = new ArrayList<>();
            List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeyAux2 = new ArrayList<>();
            Integer sizeList = this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected().size();

            for (PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput
                    : this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected()) {
                if (propertiesPlansTreatmentsKeyOutput.isHasGenerate()) {
                    propertiesPlansTreatmentsKeyAux2.add(propertiesPlansTreatmentsKeyOutput);
                } else if (propertiesPlansTreatmentsKeyOutput.getObservation() == null || "".equals(propertiesPlansTreatmentsKeyOutput.getObservation())) {
                    propertiesPlansTreatmentsKeyAux.add(propertiesPlansTreatmentsKeyOutput);
                } else {
                    this.servicePropertiePlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeyOutput.getId(),
                            EstatusPlansTreatmentsKeys.ELIMINADO.getValor(), this.securityViewBean.obtainUser().getId(), null);
                }
            }

            Integer sizeListObservation = propertiesPlansTreatmentsKeyAux.size();
            Integer sizeListDelete = propertiesPlansTreatmentsKeyAux2.size();
            String message = "";
            if (Objects.equals(sizeListDelete, sizeList)) {
                message = "Tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();
            } else if (Objects.equals(sizeListObservation, sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString();
            } else if (sizeListObservation > 0 && sizeListDelete > 0 && (sizeListObservation + sizeListDelete < sizeList)) {
                message = "Tratamiento(s) actualizados exitosamente " + ((sizeList) - (sizeListObservation + sizeListDelete))
                        + ", tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();
            } else if (sizeListObservation == 0 && sizeListDelete == 0) {
                message = "OPERACIÓN REALIZADA CON ÉXITO";
            } else if (sizeListObservation > 0 && sizeListDelete > 0 && (sizeListObservation + sizeListDelete == sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();

            } else if (sizeListObservation > 0 && sizeListDelete == 0 && (sizeListObservation < sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) actualizados exitosamente: " + (sizeList - sizeListObservation);
            } else if (sizeListObservation == 0 && sizeListDelete > 0 && (sizeListDelete < sizeList)) {
                message = "Tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString()
                        + ", tratamiento(s) actualizados exitosamente: " + (sizeList - sizeListDelete);
            }
            this.registerKeysAnalysisViewBean.setPropertiesPlansTreatmentsKeySelected(new ArrayList<PropertiesPlansTreatmentsKeyOutput>());
            if (!propertiesPlansTreatmentsKeyAux.isEmpty() || !propertiesPlansTreatmentsKeyAux2.isEmpty()) {
                this.registerKeysAnalysisViewBean.getPropertiesPlansTreatmentsKeySelected().addAll(propertiesPlansTreatmentsKeyAux);
                this.registerKeysAnalysisViewBean.getPropertiesPlansTreatmentsKeySelected().addAll(propertiesPlansTreatmentsKeyAux2);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().findPlansTreatments();
            this.getRegisterKeysAnalysisViewBean().toggleSelect();

        }
    }

    public void updateStatusPropertie(Integer status) throws SQLException {
        log.log(Level.INFO, "updateStatusPropertie()");
        if (!this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected().isEmpty()) {
            List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeyAux = new ArrayList<>();
            Integer sizeList = this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected().size();
            for (PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput
                    : this.getRegisterKeysAnalysisViewBean().getPropertiesPlansTreatmentsKeySelected()) {
                if (propertiesPlansTreatmentsKeyOutput.getObservation() == null || "".equals(propertiesPlansTreatmentsKeyOutput.getObservation())) {
                    propertiesPlansTreatmentsKeyAux.add(propertiesPlansTreatmentsKeyOutput);
                } else {
                    this.servicePropertiePlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeyOutput.getId(), status,
                            this.securityViewBean.obtainUser().getId(), null);
                }
            }
            Integer sizeList2 = propertiesPlansTreatmentsKeyAux.size();
            String message = "";
            if (sizeList - sizeList2 == 0) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeList2.toString();
            } else if (propertiesPlansTreatmentsKeyAux.isEmpty()) {
                message = "OPERACIÓN REALIZADA CON ÉXITO";
                if (!status.equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                    this.getRegisterKeysAnalysisViewBean().toggleSelect();
                } else {
                    this.getRegisterKeysAnalysisViewBean().btnVisivility(status);
                }
            } else if (!propertiesPlansTreatmentsKeyAux.isEmpty() && (sizeList - sizeList2) > 0) {
                message = "Tratamiento(s) actualizados exitosamente: " + (sizeList - sizeList2)
                        + ", tratamiento(s) sin actualizar porque no poseen observación: " + sizeList2.toString();
            }
            this.registerKeysAnalysisViewBean.setPropertiesPlansTreatmentsKeySelected(new ArrayList<PropertiesPlansTreatmentsKeyOutput>());
            if (!propertiesPlansTreatmentsKeyAux.isEmpty()) {
                this.registerKeysAnalysisViewBean.getPropertiesPlansTreatmentsKeySelected().addAll(propertiesPlansTreatmentsKeyAux);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();

        }
    }

    public void auditTreatment() throws SQLException {
        log.log(Level.INFO, "auditTreatment()");
        if (!this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected().isEmpty()) {
            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected()) {
                this.servicePlansTreatmentKeys.updateStatus(plansTreatmentsKeys.getId(),
                        EstatusPlansTreatmentsKeys.AUDITADO.getValor(), this.securityViewBean.obtainUser().getId(), null);
            }
            String message = "OPERACIÓN REALIZADA CON ÉXITO";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
            this.registerKeysAnalysisViewBean.setPlansTreatmentsKeysSelected(new ArrayList<PlansTreatmentsKeys>());
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().toggleSelect();
        }
    }

    public void updateStatusTreatment(Integer status) throws SQLException {
        log.log(Level.INFO, "updateStatusTreatment()");
        if (!this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected().isEmpty()) {
            List<PlansTreatmentsKeys> plansTreatmentsKeyAux = new ArrayList<>();
            Integer sizeList = this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected().size();
            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected()) {
                if (plansTreatmentsKeys.getObservation() == null || "".equals(plansTreatmentsKeys.getObservation())) {
                    plansTreatmentsKeyAux.add(plansTreatmentsKeys);
                } else {
                    this.servicePlansTreatmentKeys.updateStatus(plansTreatmentsKeys.getId(), status,
                            this.securityViewBean.obtainUser().getId(), null);
                }
            }
            Integer sizeList2 = plansTreatmentsKeyAux.size();
            String message = "";
            if (sizeList - sizeList2 == 0) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeList2.toString();
            } else if (plansTreatmentsKeyAux.isEmpty()) {
                message = "OPERACIÓN REALIZADA CON ÉXITO";
            } else if (!plansTreatmentsKeyAux.isEmpty() && (sizeList - sizeList2) > 0) {
                message = "Tratamiento(s) actualizados exitosamente: " + (sizeList - sizeList2)
                        + ", Tratamiento(s) sin actualizar porque no poseen observación: " + sizeList2.toString();
            }
            this.registerKeysAnalysisViewBean.setPlansTreatmentsKeysSelected(new ArrayList<PlansTreatmentsKeys>());
            if (!plansTreatmentsKeyAux.isEmpty()) {
                this.registerKeysAnalysisViewBean.getPlansTreatmentsKeysSelected().addAll(plansTreatmentsKeyAux);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().findPlansTreatments();
            if (!status.equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                this.getRegisterKeysAnalysisViewBean().toggleSelect();
            } else {
                this.getRegisterKeysAnalysisViewBean().btnVisivility(status);
            }
        }
    }

    public void deleteTreatment() throws SQLException {
        log.log(Level.INFO, "deleteTreatment()");
        if (!this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected().isEmpty()) {
            List<PlansTreatmentsKeys> plansTreatmentsKeyAux = new ArrayList<>();
            List<PlansTreatmentsKeys> plansTreatmentsKeyAux2 = new ArrayList<>();
            Integer sizeList = this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected().size();

            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getRegisterKeysAnalysisViewBean().getPlansTreatmentsKeysSelected()) {
                if (plansTreatmentsKeys.isHasGenerated()) {
                    plansTreatmentsKeyAux.add(plansTreatmentsKeys);
                } else if (plansTreatmentsKeys.getObservation() == null || "".equals(plansTreatmentsKeys.getObservation())) {
                    plansTreatmentsKeyAux2.add(plansTreatmentsKeys);
                } else {
                    this.servicePlansTreatmentKeys.updateStatus(plansTreatmentsKeys.getId(),
                            EstatusPlansTreatmentsKeys.ELIMINADO.getValor(), this.securityViewBean.obtainUser().getId(),
                            null);
                }
            }

            Integer sizeListDelete = plansTreatmentsKeyAux.size();
            Integer sizeListObservation = plansTreatmentsKeyAux2.size();
            String message = "";
            if (Objects.equals(sizeListDelete, sizeList)) {
                message = "Tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();
            } else if (Objects.equals(sizeListObservation, sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString();
            } else if (sizeListObservation > 0 && sizeListDelete > 0 && (sizeListObservation + sizeListDelete < sizeList)) {
                message = "Tratamiento(s) actualizados exitosamente: " + ((sizeList) - (sizeListObservation + sizeListDelete))
                        + ", tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();
            } else if (sizeListObservation == 0 && sizeListDelete == 0) {
                message = "OPERACIÓN REALIZADA CON ÉXITO";
            } else if (sizeListObservation > 0 && sizeListDelete > 0 && (sizeListObservation + sizeListDelete == sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString();
            } else if (sizeListObservation > 0 && sizeListDelete == 0 && (sizeListObservation < sizeList)) {
                message = "Tratamiento(s) sin actualizar porque no poseen observación: " + sizeListObservation.toString()
                        + ", tratamiento(s) actualizados exitosamente: " + (sizeList - sizeListObservation);
            } else if (sizeListObservation == 0 && sizeListDelete > 0 && (sizeListDelete < sizeList)) {
                message = "Tratamiento(s) que no pueden ser eliminados: " + sizeListDelete.toString()
                        + ", tratamiento(s) actualizados exitosamente: " + (sizeList - sizeListDelete);
            }

            this.registerKeysAnalysisViewBean.setPlansTreatmentsKeysSelected(new ArrayList<PlansTreatmentsKeys>());
            if (!plansTreatmentsKeyAux.isEmpty() || !plansTreatmentsKeyAux2.isEmpty()) {
                this.registerKeysAnalysisViewBean.getPlansTreatmentsKeysSelected().addAll(plansTreatmentsKeyAux);
                this.registerKeysAnalysisViewBean.getPlansTreatmentsKeysSelected().addAll(plansTreatmentsKeyAux2);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", message));
            RequestContext.getCurrentInstance().update("form:growReviewAnalysisKeys");
            this.getRegisterKeysAnalysisViewBean().findPlansTratmentsKeys();
            this.getRegisterKeysAnalysisViewBean().findPlansTreatments();
            this.getRegisterKeysAnalysisViewBean().toggleSelect();

        }
    }

}
