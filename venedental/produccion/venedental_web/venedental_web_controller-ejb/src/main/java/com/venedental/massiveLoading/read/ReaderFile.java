/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.read;

import com.venedental.massiveLoading.base.Constants;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.base.Permission;
import com.venedental.massiveLoading.base.Terminal;
import com.venedental.dto.massiveLoading.FileLoad;
import com.venedental.dto.massiveLoading.SheetLoad;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.massiveLoading.LoaderInsurance;
import com.venedental.model.Insurances;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akdesk01
 */
public class ReaderFile {

    private static final Logger log = CustomLogger.getLogger(ReaderFile.class.getName());

    private final FileLoad fileLoad;
    private final LoaderInsurance loader;

    public ReaderFile(FileLoad fileLoad, LoaderInsurance loader) {
        this.fileLoad = fileLoad;
        this.loader = loader;
    }

    public void load(Insurances insurance,List<FindPlanByIdOutput> planList) {
        log.log(Level.INFO, "Inicio - Lectura del Archivo Excel: {0}", fileLoad.getFilepath());
        loader.getWriterHelper().registerCommonLine("ARCHIVO: " + fileLoad.getFileName());
        loader.getWriterHelper().registerCommonLine("");
        
        if (fileLoad.getFileExcel().exists()) {
            convertExcelToCsv(fileLoad.getInsuranceLoad().getFolderPath(), fileLoad.getFilepath());
            String[] sheetFiles = fileLoad.getInsuranceLoad().getFolder().list();
            if (sheetFiles != null) {
                for (String csvName : sheetFiles) {
                    if (csvName.endsWith(Constants.SUFIJO_GEN_CSV)) {
                        String sheetName = csvName.replace(Constants.SUFIJO_GEN_CSV, "");
                        SheetLoad sheetloading = new SheetLoad(csvName, sheetName, fileLoad);
                        ReaderSheet readerSheet = new ReaderSheet(sheetloading, loader);
                        readerSheet.load(insurance,planList);
                    }
                }
            } else {
                log.log(Level.INFO, "El archivo actual \"{0}\" no contiene hojas", fileLoad.getFilepath());
            }
            move(fileLoad.getFileExcel());
        } else {
            log.log(Level.INFO, "El archivo actual \"{0}\" no existe", fileLoad.getFilepath());
        }
        log.log(Level.INFO, "Fin - Lectura del Archivo Excel: {0}", fileLoad.getFilepath());
    }

    private void convertExcelToCsv(String pathDirectory, String pathFile) {
        log.log(Level.INFO, "Inicio - Convertir Excel a CSV");
        String name = "\"" + pathFile + "\"";
        String instructionExcelToCsv = "ssconvert --export-type=Gnumeric_stf:stf_csv -S " + name + " \"" + pathDirectory + Constants.DIRECTORY_SEPARATOR + "%s" + Constants.SUFIJO_GEN_CSV + "\"";
        Terminal.exec(instructionExcelToCsv);
        log.log(Level.INFO, "Fin - Convertir Excel a CSV");
    }

    private void move(File file) {
        log.log(Level.INFO, "Inicio - Mover archivo a carpeta Procesados");
        Permission.permitWrite(fileLoad.getInsuranceLoad().getFolderProcessedPath());
        if (!fileLoad.getInsuranceLoad().getFolderCurrentLoad().exists()) {
            fileLoad.getInsuranceLoad().getFolderCurrentLoad().mkdir();
        }
        String filename = file.getName();
        String newFilename = fileLoad.getInsuranceLoad().getFolderCurrentLoadPath() + Constants.DIRECTORY_SEPARATOR + filename;
        file.renameTo(new File(newFilename));
        Permission.permitReadOnly(fileLoad.getInsuranceLoad().getFolderProcessedPath(), fileLoad.getInsuranceLoad().getFolderName());
        log.log(Level.INFO, "Fin - Mover archivo a carpeta Procesados");
    }

}
