/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;

import com.venedental.controller.BaseBean;
import com.venedental.facade.StateFacadeLocal;
import com.venedental.model.State;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "stateApplicationBean")
@ApplicationScoped
public class StateApplicationBean extends BaseBean {

    @EJB
    private StateFacadeLocal stateFacadeLocal;

    public StateApplicationBean() {

    }

    @PostConstruct
    public void init() {
        this.getListState().addAll(this.stateFacadeLocal.findAll());
    }

    private List<State> listState;

    public List<State> getListState() {
        if (this.listState == null) {
            this.listState = new ArrayList<>();
        }
        return listState;
    }

    public void setListState(List<State> listState) {
        this.listState = listState;
    }


}
