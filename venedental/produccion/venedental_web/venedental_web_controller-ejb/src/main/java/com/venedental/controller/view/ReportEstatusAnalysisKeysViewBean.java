package com.venedental.controller.view;

import com.venedental.constans.ConstansConsultPaymentReport;
import com.venedental.constans.ConstansTreatmentsReport;
import com.venedental.controller.angular.generatePaymentReport.ReportDentalTreatmentsBean;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.dao.reportAuditConsultTrace.DateReportStatusDAO;
import com.venedental.dto.createUser.FindUserByCardIdOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.dto.reportAuditConsultTrace.DateReportStatusOuput;
import com.venedental.dto.reportAuditConsultTrace.PaymentReportDataSource;
import com.venedental.dto.reportAuditConsultTrace.ReportPaymentDetailsOutput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysInput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysOuput;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmentDetailsOutput;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmentsDataSource;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import com.venedental.model.Specialists;
import com.venedental.services.ServiceAuditConsultTrace;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServicePaymentReport;
import com.venedental.services.ServiceReportDentalTreatments;
import com.venedental.services.ServiceSpecialists;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import net.sf.jasperreports.engine.JRDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import javax.ws.rs.core.Response;
import javax.mail.MessagingException;
import org.primefaces.util.ComponentUtils;

@ManagedBean(name = "reportEstatusAnalysisKeysViewBean")
@ViewScoped
public class ReportEstatusAnalysisKeysViewBean {

    private static final Logger log = services.utils.CustomLogger.getGeneralLogger(ReportEstatusAnalysisKeysViewBean.class.getName());

    private List<ReportStatusAnalysisKeysOuput> listByFilter;
   

    private Date startDate;
    private Date endDate;
    private boolean btnBuscar;
    private boolean btnReset;
    private boolean columnFilterActive;
    private boolean columnVisibilityKeys;
    private boolean columnVisibilitySpecialist;
    private String key;
    private String specialist, maxDateString;
    private Date maxDateCalendar;
    private boolean inputDataEnd;
    private String nameSpecislits;
    private Integer numReportTreatmentS;
    private String numPaymentReport;
    private String numKey;
    private String treatmentName;
    private String statusName;
   // private String enableDetail = "-";
 private Date dateMin;

    private static final String EXTENSION = ".pdf";
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private Map<String, Object> params = new HashMap<String, Object>();
    private String logoPath;
    private String reportPath;
    private JasperPrint jasperPrint;
    private JRDataSource listaDataSource;

    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport config;

    @EJB
    private ServiceAuditConsultTrace serviceAuditConsultTrace;
    @EJB
    private ServiceSpecialists serviceSpecialists;
    @EJB
    private ServiceCreateUser serviceCreateUser;
    @EJB
    private ServiceReportDentalTreatments serviceReportDentalTreatments;
    @EJB
    private ServicePaymentReport servicePaymentReport;

    @PostConstruct
    public void init() {
        log.log(Level.INFO, "init()");
        this.setInputDataEnd(true);
        this.setBtnReset(true);
        this.setBtnBuscar(true);
        this.setColumnFilterActive(true);
         this.setColumnVisibilityKeys(true);
         this.setColumnVisibilitySpecialist(true);
         
         //this.setBtnEnableDetail(false);
         
        

    }

    public List<ReportStatusAnalysisKeysOuput> getDatabyFilter() {
            
       
        ReportStatusAnalysisKeysInput input = new ReportStatusAnalysisKeysInput(convertJavaDateToSqlDate(startDate), convertJavaDateToSqlDate(endDate), key, specialist);
        listByFilter = serviceAuditConsultTrace.getAuditConsultTracebyFilter(input);
        if (!input.getIdKey().equals("")) {
            this.setColumnVisibilityKeys(false);
        }
        if (!input.getSpecialist().equals("")) {
            this.setColumnVisibilitySpecialist(false);
        }
       
        if (!listByFilter.isEmpty()) {

            this.setColumnFilterActive(false);
            for (int i = 0; i < listByFilter.size(); i++) {

                listByFilter.get(i).setNumReportTreatmentS(listByFilter.get(i).getNumReportTreatment().toString());
                if (listByFilter.get(i).getNumReportTreatment() == -1 && listByFilter.get(i).getIdPaymentReport() != -1) {

                    listByFilter.get(i).setNumReportTreatmentS("-");
                    listByFilter.get(i).setBtnVisibilityReportTreatment(false);
                    listByFilter.get(i).setBtnVisibilityPaymentReport(true);
                    listByFilter.get(i).setBtnEnableDetail(false);
                    
                }

                
                else if (listByFilter.get(i).getIdPaymentReport() == -1 && listByFilter.get(i).getNumReportTreatment() != -1) {
                    listByFilter.get(i).setBtnVisibilityPaymentReport(false);
                     listByFilter.get(i).setBtnVisibilityReportTreatment(true);
                    listByFilter.get(i).setBtnEnableDetail(false);
                      
                }
                
                else if(listByFilter.get(i).getIdPaymentReport() == -1 && listByFilter.get(i).getNumReportTreatment() == -1){
                listByFilter.get(i).setBtnEnableDetail(true);
                 listByFilter.get(i).setNumReportTreatmentS("-");
                   listByFilter.get(i).setBtnVisibilityPaymentReport(false);
                     listByFilter.get(i).setBtnVisibilityReportTreatment(false);
                }

 
            }
        }
        
RequestContext.getCurrentInstance().update("form:reportEstatusAnalysisKeysTable");
RequestContext.getCurrentInstance().update("form");
DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reportEstatusAnalysisKeysTable");
           dataTable.reset();
           dataTable.getFilters().clear();
        return listByFilter;

    }

    public void cleanDataFilter() {
        this.setBtnBuscar(false);
        this.setEndDate(null);
        this.setStartDate(null);
        this.setKey("");
        this.setSpecialist("");
        listByFilter = new ArrayList<>();
        this.setColumnFilterActive(true);
       
    }

    public String getMaxDateString() {
        return maxDateString;
    }

    //datos para el calendario
    public void setMaxDateString(String maxDateString) {
        this.maxDateString = maxDateString;
    }

    public ServiceSpecialists getServiceSpecialists() {
        return serviceSpecialists;
    }

    public void setServiceSpecialists(ServiceSpecialists serviceSpecialists) {
        this.serviceSpecialists = serviceSpecialists;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getMaxDateCalendar() {
        return maxDateCalendar;
    }

    public void setMaxDateCalendar(Date maxDateCalendar) {
        this.maxDateCalendar = maxDateCalendar;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isInputDataEnd() {
        return inputDataEnd;
    }

    public void setInputDataEnd(boolean inputDataEnd) {
        this.inputDataEnd = inputDataEnd;
    }

    public List<ReportStatusAnalysisKeysOuput> getListByFilter() {
        return listByFilter;
    }

    public void setListByFilter(List<ReportStatusAnalysisKeysOuput> listByFilter) {
        this.listByFilter = listByFilter;
    }

    public boolean isBtnBuscar() {
        return btnBuscar;
    }

    public void setBtnBuscar(boolean btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public boolean isBtnReset() {
        return btnReset;
    }

    public void setBtnReset(boolean btnReset) {
        this.btnReset = btnReset;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public boolean isColumnFilterActive() {
        return columnFilterActive;
    }

    public void setColumnFilterActive(boolean columnFilterActive) {
        this.columnFilterActive = columnFilterActive;
    }

    public ServiceAuditConsultTrace getServiceAuditConsultTrace() {
        return serviceAuditConsultTrace;
    }

    public void setServiceAuditConsultTrace(ServiceAuditConsultTrace serviceAuditConsultTrace) {
        this.serviceAuditConsultTrace = serviceAuditConsultTrace;
    }

    public JRBeanCollectionDataSource getBeanCollectionDataSource() {
        return beanCollectionDataSource;
    }

    public void setBeanCollectionDataSource(JRBeanCollectionDataSource beanCollectionDataSource) {
        this.beanCollectionDataSource = beanCollectionDataSource;
    }

    public boolean isColumnVisibilityKeys() {
        return columnVisibilityKeys;
    }

    public void setColumnVisibilityKeys(boolean columnVisibilityKeys) {
        this.columnVisibilityKeys = columnVisibilityKeys;
    }

    public boolean isColumnVisibilitySpecialist() {
        return columnVisibilitySpecialist;
    }

    public void setColumnVisibilitySpecialist(boolean columnVisibilitySpecialist) {
        this.columnVisibilitySpecialist = columnVisibilitySpecialist;
    }

    public ConfigViewBeanReport getConfig() {
        return config;
    }

    public void setConfig(ConfigViewBeanReport config) {
        this.config = config;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }

    public String getNameSpecislits() {
        return nameSpecislits;
    }

    public void setNameSpecislits(String nameSpecislits) {
        this.nameSpecislits = nameSpecislits;
    }

    public Integer getNumReportTreatmentS() {
        return numReportTreatmentS;
    }

    public void setNumReportTreatmentS(Integer numReportTreatmentS) {
        this.numReportTreatmentS = numReportTreatmentS;
    }

    public String getNumPaymentReport() {
        return numPaymentReport;
    }

    public void setNumPaymentReport(String numPaymentReport) {
        this.numPaymentReport = numPaymentReport;
    }

    

    public String getNumKey() {
        return numKey;
    }

    public void setNumKey(String numKey) {
        this.numKey = numKey;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
    
    
    
    
    
    
    

    public void selectDateEndConsultKeys() {
        this.setBtnBuscar(false);
        this.setBtnReset(false);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:reportEstatusAnalysisKeysTable");

    }

    public void selectDateConsultKeys() {
        if (this.getEndDate() != null) {
            this.setEndDate(null);
            this.maxDateString = maxDateString;
            this.setInputDataEnd(false);
            this.setBtnReset(false);
            this.setBtnBuscar(true);
            RequestContext.getCurrentInstance().update("form:btnResetKey");
            RequestContext.getCurrentInstance().update("form:btnFindKey");
            RequestContext.getCurrentInstance().update("form:endDate");
        } else {
            this.setInputDataEnd(false);
            this.setBtnReset(false);
            this.setBtnBuscar(true);
            RequestContext.getCurrentInstance().update("form:btnResetKey");
            RequestContext.getCurrentInstance().update("form:btnFindKey");
            RequestContext.getCurrentInstance().update("form:endDate");
            RequestContext.getCurrentInstance().update("form:reportEstatusAnalysisKeysTable");
//            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
        }

    }

    public void cleanAndDate() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reportEstatusAnalysisKeysTable");
           dataTable.reset();
           dataTable.getFilters().clear();

        this.setBtnBuscar(true);
        this.setEndDate(null);
        this.setStartDate(null);
        this.setKey("");
        this.setSpecialist("");
        listByFilter = new ArrayList<>();
        this.setColumnFilterActive(true);
        this.setInputDataEnd(true);
        this.setColumnVisibilityKeys(true);
         this.setColumnVisibilitySpecialist(true);
         this.setBtnReset(true);
         this.specialist="";
         this.setNameSpecislits(null);
         this.setNumKey(null);
         this.setNumPaymentReport(null);
         this.setNumReportTreatmentS(null);
         this.setStatusName(null);
         this.setTreatmentName(null);
     
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnReportTreatment"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnReportPaymet"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnResetKey"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnFindKey"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("endDate"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("startDate"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("key"));
        RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("specialist"));
        RequestContext.getCurrentInstance().update("form:prueba");

       RequestContext.getCurrentInstance().update("form:reportEstatusAnalysisKeysTable");
       RequestContext.getCurrentInstance().update("form:reportEstatusAnalysisKeysTable:filter");
       RequestContext.getCurrentInstance().update("form");
    }

    public void exportPDFPaymentReport(ReportStatusAnalysisKeysOuput keysOuput)
            throws JRException, IOException, SQLException, MessagingException {
        Specialists obj = serviceSpecialists.findSpecialistById(keysOuput.getIdSpecialist());
        FindUserByCardIdOutput output = serviceCreateUser.findSpecialistByCardId(obj.getIdentityNumber());
        SpecialistOutput specialistOutput = serviceReportDentalTreatments.getSpecialistToReportById(output.getId());
        PaymentReportOutput paymentReportOutput = servicePaymentReport.consultPaymentReportById(keysOuput.getIdPaymentReport());
        List<ReportPaymentDetailsOutput> LReportPaymentDetailsOutputList = serviceAuditConsultTrace.getInfoTreatmentsForPaymentReport(keysOuput.getIdPaymentReport());
        String nameReport = "RT_" + keysOuput.getSpecialist() + "_" + obj.getIdentityNumber();
        this.loadPaymentReport(new Date(), paymentReportOutput, LReportPaymentDetailsOutputList, specialistOutput);
        this.config.pdf(jasperPrint, nameReport, "");

    }

    public void loadPaymentReport(Date date, PaymentReportOutput paymentReportOutput, List<ReportPaymentDetailsOutput> LReportPaymentDetailsOutputList, SpecialistOutput specialists) throws JRException {

        String dateString = getMonthYearToReport(paymentReportOutput.getReportDate());
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        params.put("number", paymentReportOutput.getNumberPaymentReport());
        params.put("amount", paymentReportOutput.getAmount());
        params.put("month", dateString);
//                params.put("totalAmount", createPaymentReportOutput.getAmount());
        params.put("totalAmount", paymentReportOutput.getAmountTotal());
        params.put("rejectAmount", paymentReportOutput.getAuditedAmount());
        params.put("auditAmount", paymentReportOutput.getAmount());
        params.put("specialistName", specialists.getName());
        if (specialists.getRif() != null) {
            params.put("specialistRif", specialists.getRif());
        } else {
            params.put("-", specialists.getRif());
        }
        params.put("date", date);
        params.put("heal_logo2", logoPath);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        String amountTotalConvert = decimalFormat.format(paymentReportOutput.getAmount()
                - paymentReportOutput.getAuditedAmount());
        params.put("amounTotal", amountTotalConvert);
        this.listaDataSource = new PaymentReportDataSource(LReportPaymentDetailsOutputList);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/specialist/paymentReport.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, listaDataSource);
    }

    public void exportPDFTreatmentReport(ReportStatusAnalysisKeysOuput keysOuput) throws JRException, IOException, MessagingException {
        Specialists obj = serviceSpecialists.findSpecialistById(keysOuput.getIdSpecialist());
        String nameReport = "RT_" + keysOuput.getSpecialist() + "_"+ obj.getIdentityNumber();
        this.loadTreatmentReport(keysOuput, obj);
        this.config.pdf(jasperPrint, nameReport, "");
    }

    public void loadTreatmentReport(ReportStatusAnalysisKeysOuput keysOuput, Specialists obj) throws JRException {
        FindUserByCardIdOutput output = serviceCreateUser.findSpecialistByCardId(obj.getIdentityNumber());
        SpecialistOutput specialistOutput = serviceReportDentalTreatments.getSpecialistToReport(output.getUsersId());
        List<ReportTreatmentDetailsOutput> listTreat = serviceAuditConsultTrace
                .getInfoTreatmentReport(output.getUsersId(),
                        keysOuput.getNumReportTreatment());

        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");

        params.put("name", specialistOutput.getName());
        params.put("cardId", specialistOutput.getCadrId());
        params.put("phone", specialistOutput.getPhone());
        params.put("address", specialistOutput.getAddress());
        params.put("especialidad", specialistOutput.getSpecialityName());
        params.put("logoPath", logoPath);
        params.put("numCorrelative", keysOuput.getNumReportTreatment());
        params.put("dateForReport", new Date());
        this.listaDataSource = new ReportTreatmentsDataSource(listTreat);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/specialist/treatmentReport.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, listaDataSource);
    }

    public Properties readFilePropertiesTreatment() throws IOException {
        Properties mProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '"
                    + ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT
                    + "' not found in the classpath");
        }
        mProperties.load(inputStream);
        return mProperties;
    }

    public static String obtainDirectory() {

        URL rutaURL = ReportDentalTreatmentsBean.class.getProtectionDomain()
                .getCodeSource().getLocation();
        String path = rutaURL.getPath();

        return path.substring(0, path.indexOf("lib"));
    }

    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(ConstansConsultPaymentReport.FILENAME_PAYMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '"
                    + ConstansConsultPaymentReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {

        return date == null ? null : new java.sql.Date(date.getTime());
    }
    
    
    public void validateActivateButtonSearch(AjaxBehaviorEvent event) {
       UIInput comp = (UIInput) event.getComponent();

        try {
              this.setBtnBuscar(false);
                this.setBtnReset(false);
                RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnFindKey"));
                RequestContext.getCurrentInstance().update(ComponentUtils.findComponentClientId("btnResetKey"));
//             
            }

         catch (Exception e) {

        }
    }
 public String getMonthYearToReport(Date date) {
        int month = 0;
        int year = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        return " " + convertMothToString(month) + " - " + year;

    }

    public String convertMothToString(int month) {
        String monthString = "";
        switch (month) {

            case 1:
                return "ENERO";
            case 2:
                return "FEBRERO";
            case 3:
                return "MARZO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAYO";
            case 6:
                return "JUNIO";
            case 7:
                return "JULIO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SEPTIEMBRE";
            case 10:
                return "OCTUBRE";
            case 11:
                return "NOVIEMBRE";
            case 12:
                return "DICIEMBRE";

        }
        log.log(Level.INFO, "obtainDirectory()");
        return "ENERO";
    }

    public Date maxDate(){
    return new Date();
    
    }

    public Date getDateMin() {
        return dateMin;
    }

    public void setDateMin(Date dateMin) {
        this.dateMin = dateMin;
    }

  public Date getMinDateReportStatus(){
   ReportStatusAnalysisKeysInput input = new ReportStatusAnalysisKeysInput(convertJavaDateToSqlDate(startDate), convertJavaDateToSqlDate(endDate));
      DateReportStatusOuput minDate = serviceAuditConsultTrace.getMinDateReportStatus(input);
      

        return minDate.getDateMin();
  }

   public void update() {
  DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reportEstatusAnalysisKeysTable");
           dataTable.reset();
           dataTable.getFilters().clear();
}
    
}
