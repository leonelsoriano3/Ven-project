/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.constans.ConstansPaymentReport;
import com.venedental.controller.BaseBean;
import com.venedental.controller.request.ReviewKeysAnalysisRequestBean;
import com.venedental.dto.analysisKeys.*;
import com.venedental.dto.consultKeys.ConsultDateMinDetalleOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.PaymentReport;
import com.venedental.model.PlansPatients;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.Specialists;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServicePaymentReport;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import com.venedental.services.ServiceSpecialists;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 * Where class all about the third stage of analysis of key performs
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "reviewKeysAnalysisViewBean")
@ViewScoped
public class ReviewKeysAnalysisViewBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(ReviewKeysAnalysisViewBean.class.getName());
    private List<PlansTreatmentsKeys> plansTreatmentsKeys;
    private List<PlansTreatmentsKeys> plansTreatmentsKeysEvaluated;
    private List<PlansTreatmentsKeys> selectedPlansTreatmentsKeys;
    private Specialists specialist;
    private List<PlansPatients> plansPatients;
    private List<ConfirmKeyOutput> confirmList;
    private List<ConfirmKeyOutput> confirmListSelect;
    private List<ConfirmKeyOutput> confirmListFiltered;
    private List<ConfirmKeyDetailsOutput> confirmKeyDetailsList;
    private List<ConfirmKeyDetailsOutput> confirmKeyDetailsListFiltered;
    private Specialists selectedSpecialist;
    private Integer selectedConfirmKeysStatus;
    private Integer idMonthExecution;
    private boolean seeColumnCheck = false;
    private Date dateFilter;
    private boolean reviewKeysAnalysisDetails;
    private boolean reviewKeysAnalysisList;
    private boolean btnConfirmReviewKeysAnalysisDetails;
    private boolean btnLeaveReviewKeysAnalysisDetails;
    private ConfirmKeyOutput selectedConfirmKey;
    private String nameMonth;
    private Double totalAmounthPaid;
    private Boolean inputTable = true;
    private static Date maxDate;
    private boolean renderedColumn = false;
    private boolean diabledBtnUnify = true;
    private boolean isUnify=false;
    private boolean statusReportEvaluate=false;
    private boolean monthReportEvaluate=false;
    private boolean yearReportEvaluate=false;
    private Integer idStatusSelect;
    private Integer idMonthSelect;
    private Integer idYearSelect;
    private Double priceDetail;
    private Date minFilter;
    private Date maxFilter;
    
    

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;


    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;
    @EJB
    private ServiceSpecialists serviceSpecialist;
    @EJB
    private ServicePaymentReport servicePaymentReport;
    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;
    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiesPlansTreatmentKey;
    @EJB
    private MailSenderLocal mailSenderLocal;

    public ReviewKeysAnalysisViewBean() {
        log.log(Level.INFO, "ReviewKeysAnalysisViewBean()");
        this.reviewKeysAnalysisDetails = false;
        this.reviewKeysAnalysisList = true;
        this.btnConfirmReviewKeysAnalysisDetails = false;
        this.btnLeaveReviewKeysAnalysisDetails = false;
    }

    public Boolean getInputTable() {
        return inputTable;
    }

    public void setInputTable(Boolean inputTable) {
        this.inputTable = inputTable;
    }

    public Integer getSelectedConfirmKeysStatus() {
        return selectedConfirmKeysStatus;
    }

    public void setSelectedConfirmKeysStatus(Integer selectedConfirmKeysStatus) {
        this.selectedConfirmKeysStatus = selectedConfirmKeysStatus;
    }

    public Integer getIdMonthExecution() {
        return idMonthExecution;
    }

    public void setIdMonthExecution(Integer idMonthExecution) {
        this.idMonthExecution = idMonthExecution;
    }

    public boolean getBtnConfirmReviewKeysAnalysisDetails() {
        return btnConfirmReviewKeysAnalysisDetails;
    }

    public void setBtnConfirmReviewKeysAnalysisDetails(boolean btnConfirmReviewKeysAnalysisDetails) {
        this.btnConfirmReviewKeysAnalysisDetails = btnConfirmReviewKeysAnalysisDetails;
    }

    public boolean getBtnLeaveReviewKeysAnalysisDetails() {
        return btnLeaveReviewKeysAnalysisDetails;
    }

    public void setBtnLeaveReviewKeysAnalysisDetails(boolean btnLeaveReviewKeysAnalysisDetails) {
        this.btnLeaveReviewKeysAnalysisDetails = btnLeaveReviewKeysAnalysisDetails;
    }

    public boolean isSeeColumnCheck() {
        return seeColumnCheck;
    }

    public void setSeeColumnCheck(boolean seeColumnCheck) {
        this.seeColumnCheck = seeColumnCheck;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public List<PlansPatients> getPlansPatients() {
        if (this.plansPatients == null) {
            this.plansPatients = new ArrayList<>();
        }
        return plansPatients;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysEvaluated() {
        if (this.plansTreatmentsKeysEvaluated == null) {
            this.plansTreatmentsKeysEvaluated = new ArrayList<>();
        }
        return plansTreatmentsKeysEvaluated;
    }

    public void setPlansTreatmentsKeysEvaluated(List<PlansTreatmentsKeys> plansTreatmentsKeysEvaluated) {
        this.plansTreatmentsKeysEvaluated = plansTreatmentsKeysEvaluated;
    }

    public void setPlansPatients(List<PlansPatients> plansPatients) {
        this.plansPatients = plansPatients;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeys() {
        if (this.plansTreatmentsKeys == null) {
            this.plansTreatmentsKeys = new ArrayList<>();
        }
        return plansTreatmentsKeys;
    }

    public void setPlansTreatmentsKeys(List<PlansTreatmentsKeys> plansTreatmentsKeys) {
        this.plansTreatmentsKeys = plansTreatmentsKeys;
    }

    public List<PlansTreatmentsKeys> getSelectedPlansTreatmentsKeys() {
        if (this.selectedPlansTreatmentsKeys == null) {
            this.selectedPlansTreatmentsKeys = new ArrayList<>();
        }
        return selectedPlansTreatmentsKeys;
    }

    public void setSelectedPlansTreatmentsKeys(List<PlansTreatmentsKeys> selectedPlansTreatmentsKeys) {
        this.selectedPlansTreatmentsKeys = selectedPlansTreatmentsKeys;
    }

    public List<ConfirmKeyOutput> getConfirmList() {
        if (this.confirmList == null) {
            this.confirmList = new ArrayList<>();
        }
//String mes = this.getReviewKeysAnalysisViewBean().getNameMonth();

        return this.confirmList;
    }

    public void setConfirmList(List<ConfirmKeyOutput> confirmList) {
        this.confirmList = confirmList;
    }

    public List<ConfirmKeyOutput> getConfirmListFiltered() {
        return confirmListFiltered;
    }

    public void setConfirmListFiltered(List<ConfirmKeyOutput> confirmListFiltered) {
        this.confirmListFiltered = confirmListFiltered;
    }

    public boolean getReviewKeysAnalysisDetails() {
        return reviewKeysAnalysisDetails;
    }

    public void setReviewKeysAnalysisDetails(boolean reviewKeysAnalysisDetails) {
        this.reviewKeysAnalysisDetails = reviewKeysAnalysisDetails;
    }

    public boolean isReviewKeysAnalysisList() {
        return reviewKeysAnalysisList;
    }

    public void setReviewKeysAnalysisList(boolean reviewKeysAnalysisList) {
        this.reviewKeysAnalysisList = reviewKeysAnalysisList;
    }

    public ConfirmKeyOutput getSelectedConfirmKey() {
        return selectedConfirmKey;
    }

    public List<ConfirmKeyDetailsOutput> getConfirmKeyDetailsList() {
        if (this.confirmKeyDetailsList == null) {
            this.confirmKeyDetailsList = new ArrayList<>();
        }
        return confirmKeyDetailsList;
    }

    public void setConfirmKeyDetailsList(List<ConfirmKeyDetailsOutput> confirmKeyDetailsList) {
        this.confirmKeyDetailsList = confirmKeyDetailsList;
    }

    public List<ConfirmKeyDetailsOutput> getConfirmKeyDetailsListFiltered() {
        return confirmKeyDetailsListFiltered;
    }

    public void setConfirmKeyDetailsListFiltered(List<ConfirmKeyDetailsOutput> confirmKeyDetailsListFiltered) {
        this.confirmKeyDetailsListFiltered = confirmKeyDetailsListFiltered;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public Double getTotalAmounthPaid() {
        return totalAmounthPaid;
    }

    public void setTotalAmounthPaid(Double totalAmounthPaid) {
        this.totalAmounthPaid = totalAmounthPaid;
    }

    public boolean isRenderedColumn() {
        return renderedColumn;
    }

    public void setRenderedColumn(boolean renderedColumn) {
        this.renderedColumn = renderedColumn;
    }

    public List<ConfirmKeyOutput> getConfirmListSelect() {
        return confirmListSelect;
    }

    public void setConfirmListSelect(List<ConfirmKeyOutput> confirmListSelect) {
        this.confirmListSelect = confirmListSelect;
    }

    public boolean isDiabledBtnUnify() {
        return diabledBtnUnify;
    }

    public void setDiabledBtnUnify(boolean diabledBtnUnify) {
        this.diabledBtnUnify = diabledBtnUnify;
    }

    public boolean isIsUnify() {
        return isUnify;
    }

    public void setIsUnify(boolean isUnify) {
        this.isUnify = isUnify;
    }

    public boolean isStatusReportEvaluate() {
        return statusReportEvaluate;
    }

    public void setStatusReportEvaluate(boolean statusReportEvaluate) {
        this.statusReportEvaluate = statusReportEvaluate;
    }

    public boolean isMonthReportEvaluate() {
        return monthReportEvaluate;
    }

    public void setMonthReportEvaluate(boolean monthReportEvaluate) {
        this.monthReportEvaluate = monthReportEvaluate;
    }

    public boolean isYearReportEvaluate() {
        return yearReportEvaluate;
    }

    public void setYearReportEvaluate(boolean yearReportEvaluate) {
        this.yearReportEvaluate = yearReportEvaluate;
    }

    public Integer getIdStatusSelect() {
        return idStatusSelect;
    }

    public void setIdStatusSelect(Integer idStatusSelect) {
        this.idStatusSelect = idStatusSelect;
    }

    public Integer getIdMonthSelect() {
        return idMonthSelect;
    }

    public void setIdMonthSelect(Integer idMonthSelect) {
        this.idMonthSelect = idMonthSelect;
    }

    public Integer getIdYearSelect() {
        return idYearSelect;
    }

    public void setIdYearSelect(Integer idYearSelect) {
        this.idYearSelect = idYearSelect;
    }

    public Double getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(Double priceDetail) {
        this.priceDetail = priceDetail;
    }

    public Date getMinFilter() {
        return minFilter;
    }

    public void setMinFilter(Date minFilter) {
        this.minFilter = minFilter;
    }

    public Date getMaxFilter() {
        return maxFilter;
    }

    public void setMaxFilter(Date maxFilter) {
        this.maxFilter = maxFilter;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    

    /**
     * method that load the objectConfirmKey where is selected of the listConfirmKeysList
     *
     * @param selectedConfirmKey
     */
    public void setSelectedConfirmKey(ConfirmKeyOutput selectedConfirmKey) {
        log.log(Level.INFO, "setSelectedConfirmKey(selectedConfirmKey)");
        int status = 0;
        if (selectedConfirmKey.getNameStatus().equals("PENDIENTE")) {
            status = EstatusPlansTreatmentsKeys.AUDITADO.getValor();
        } else {
            status = EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor();
        }
        this.setIdMonthExecution(selectedConfirmKey.getIdMonthExecution());
        this.setSelectedConfirmKeysStatus(selectedConfirmKey.getStatus());
        this.setConfirmKeyDetailsList(this.serviceAnalysisKeys.findConfirmKeyDetailsOutput(this.
                getSelectedSpecialist().getId(), status, selectedConfirmKey.
                getIdMonthExecution(), selectedConfirmKey.getDateExecution()));
        for(ConfirmKeyDetailsOutput ConfirmKeyDetailsOutput: this.confirmKeyDetailsList){
            this.setMinFilter(ConfirmKeyDetailsOutput.getDateMinFilter());
            this.setMaxFilter(ConfirmKeyDetailsOutput.getMaxFilter());
            
        }
        


//        this.modelLazy = new LazyKeyDataInvoicesTransferConfirm(ConfirmKeyDetailsInvoicesTransferOutputList,
//                (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(
//                        ComponentUtils.findComponentClientId("confirmKeyTable"))
//
//        );


        this.setConfirmKeyDetailsListFiltered(this.getConfirmKeyDetailsList());


        this.setNameMonth(selectedConfirmKey.getNameMonthExecution());
        if (!getConfirmKeyDetailsList().isEmpty()) {
            this.setTotalAmounthPaid(this.getConfirmKeyDetailsList().get(0).getTotalAmountPaid());
        }
        if (Objects.equals(selectedConfirmKey.getStatus(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor())) {

            this.setBtnConfirmReviewKeysAnalysisDetails(false);
            this.setBtnLeaveReviewKeysAnalysisDetails(true);
        } else {

            this.setBtnConfirmReviewKeysAnalysisDetails(true);
            this.setBtnLeaveReviewKeysAnalysisDetails(true);
        }
        this.selectedConfirmKey = selectedConfirmKey;
    }

    /**
     * *
     * Running method is barely up dialogue
     *
     *
     * @param selectedKey
     */
    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        log.log(Level.INFO, "filterSpecialist()");
        List<Specialists> filteredSpecialists = this.serviceSpecialist.findByIdentityOrName(query);
        return filteredSpecialists;
    }

    /**
     * Method called when I select a specialist from the list
     *
     * @param event *
     */
    public void onItemSelect(SelectEvent event) {
        log.log(Level.INFO, "onItemSelect()");
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        filterKeys();
    }
    
     public void selectRow() {
        log.log(Level.INFO, "selectRow()");
        Integer cant=this.getConfirmListSelect().size();
        if (cant>=2){
            this.setDiabledBtnUnify(false);
        }else{
            this.setDiabledBtnUnify(true);
        }
        
        RequestContext.getCurrentInstance().update("form:btnUnify");
    }

    public void unSelectRow() {
        log.log(Level.INFO, "unSelectRow()");
        Integer cantCheck=this.getConfirmListSelect().size();
        if (cantCheck<2) {
            this.setDiabledBtnUnify(true);
            RequestContext.getCurrentInstance().update("form:btnUnify");
        }else{
             this.setDiabledBtnUnify(false);
             RequestContext.getCurrentInstance().update("form:btnUnify");
        }
    }

    public void toggleSelect() {
        log.log(Level.INFO, "toggleSelect()");
        Integer cantAct=this.getConfirmListSelect().size();
        if (cantAct<2) {
            this.setDiabledBtnUnify(true);
        } else {
            this.setDiabledBtnUnify(false);
        }
        RequestContext.getCurrentInstance().update("form:btnUnify");
    }
    
    public void clearSelection() {
        log.log(Level.INFO, "clearSelection()");
        this.setConfirmListSelect(new ArrayList<ConfirmKeyOutput>());
        this.setDiabledBtnUnify(true);
        RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        RequestContext.getCurrentInstance().update("form:btnUnify");
    }


    /**
     * Method that allows you to find the keys generated or state analysis and also treatments that have registered,
     * refuse or for review , is executed when selected a specialist *
     */
    public void filterKeys() {
        log.log(Level.INFO, "filterKeys()");
        this.setInputTable(true);
        resetDataTable();
        this.setConfirmList(this.serviceAnalysisKeys.findConfirmKeyOutput(this.getSelectedSpecialist().getId()));
        this.setConfirmListSelect(new ArrayList<ConfirmKeyOutput>());
        this.setConfirmListFiltered(this.getConfirmList());
        if (!this.getConfirmList().isEmpty()) {
            this.setInputTable(false);
            this.setRenderedColumn(true);
        }else{
             this.setRenderedColumn(false);
        }
        RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        RequestContext.getCurrentInstance().update("form:btnEditKeyAnlysisReview");
    }

    public void resetDataTable() {
        log.log(Level.INFO, "resetDataTable()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reviewAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:reviewAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        }
    }

    /**
     * Method that allows you to clean all the lists used in this controlled
     */
    public void clear() {
        log.log(Level.INFO, "clear()");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        this.setPlansPatients(new ArrayList<PlansPatients>());
        this.setSelectedPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansTreatmentsKeysEvaluated(new ArrayList<PlansTreatmentsKeys>());
    }

    /**
     * Method that is executed when a date is selected in the filter key

     */
    public void handleDateSelect() {
        log.log(Level.INFO, "handleDateSelect()");
        //RequestContext.getCurrentInstance().execute("PF('registerSpecialistToAuditWiget').filter()");
        RequestContext.getCurrentInstance().execute(" if(PF('reviewAnalysisTable') != null ){PF('reviewAnalysisTable').filter()};if(PF('registerSpecialistToAuditWiget') != null ){PF('registerSpecialistToAuditWiget').filter();} ");
    }
    
    /**
     * Method that is executed when a date is selected in the filter key

     */
    public void handleDateSelectDetails() {
        log.log(Level.INFO, "handleDateSelect()");
        //RequestContext.getCurrentInstance().execute("PF('registerSpecialistToAuditWiget').filter()");
        RequestContext.getCurrentInstance().execute(" if(PF('confirmKeyDetailsOutputTable') != null ){PF('confirmKeyDetailsOutputTable').filter()}");
    }

    /**
     * DateTrasnformar method that allows a date to the form dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public Date formatDate(Date date) throws ParseException {
        log.log(Level.INFO, "formatDate()");
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;

    }

    /**
     * Method showing the detail of receiving when selected track and hides the main list
     */
    public void goReviewKeysAnalysisDetails() {
        log.log(Level.INFO, "goReviewKeysAnalysisDetails()");
        this.setReviewKeysAnalysisDetails(true);
        this.setReviewKeysAnalysisList(false);
        RequestContext.getCurrentInstance().update("form");
    }

    /**
     * Method showing the list of keysConfirm and hide the details
     */
    public void goReviewKeysAnalysisList() {
        log.log(Level.INFO, "goReviewKeysAnalysisList()");
        this.setReviewKeysAnalysisDetails(false);
        this.setReviewKeysAnalysisList(true);
        this.setBtnConfirmReviewKeysAnalysisDetails(false);
        this.setBtnLeaveReviewKeysAnalysisDetails(false);
        this.resetDataTableKeys();
        RequestContext.getCurrentInstance().execute("PF('reviewAnalysisTable').filter()");
        RequestContext.getCurrentInstance().update("form");
        RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");

    }

    public void resetDataTableKeys() {
        log.log(Level.INFO, "resetDataTableKeys()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:reviewAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:reviewAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:reviewAnalysisKeysList");
        }
    }

    public void resendEmail(ConfirmKeyOutput confirmKeyOutput) throws SQLException, ParseException, IOException {


        if ( this.specialist.getEmail().length() == 0) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El especialista no posee correo asociado"));
            RequestContext.getCurrentInstance().update("form:growReviewAnalysisKeys");
            return;
        }

        Date current = this.obtainDate();

        this.setSelectedConfirmKey(confirmKeyOutput);
        Users user = this.securityViewBean.obtainUser();
        String nameUser = this.securityViewBean.obtainNameUser();

        this.savePaymentReport(current, nameUser, this.getSelectedConfirmKey().getNameMonthExecution(),
                this.getSelectedConfirmKey().getDateExecution(),this.getSelectedConfirmKey().getStatus());

        RequestContext.getCurrentInstance().update("form:growReviewAnalysisKeys");
    }

    /**
     * Method that allows you to save reports paying a specialist whose treatments have been confirmed
     *
     * @param date
     * @param nameUser
     * @param mes
     * @param dateReception fecha que se recive la clave
     * @throws IOException
     */
    @SuppressWarnings("empty-statement")
    public void savePaymentReport(Date date, String nameUser, String mes, Date dateReception,
                                  Integer status) throws IOException {

        log.log(Level.INFO, " savePaymentReport()");

            PaymentReport paymentReport = new PaymentReport();
            Double auditedAmount = 0.0;
            Double rejectAmount = 0.0;
            Double amountTotal = 0.0;

            Double amountPaid = 0.0;

            List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList = new ArrayList<>();
            Integer idSpecialist = this.getSelectedSpecialist().getId();

            //===->TODO: reparar esto
            Integer idMonthExecution = selectedConfirmKey.getIdMonthExecution();
            //amountPaid, rejectAmount


            ConsultPaymentReportOutPut consultPaymentReportOutPut =
                    serviceAnalysisKeys.findPaimentReport(idSpecialist,status,idMonthExecution,dateReception);


            List<ConsultKeyDetailsStatusPaidOutPut> consultKeyDetailsStatusPaidOutPut =
                    serviceAnalysisKeys.findConfirmKeyDetails(idSpecialist, idMonthExecution,
                    new java.sql.Date(dateReception.getTime()));


            if (!consultKeyDetailsStatusPaidOutPut.isEmpty()) {
                for (ConsultKeyDetailsStatusPaidOutPut outPut : consultKeyDetailsStatusPaidOutPut) {
                    ConfirmKeyDetailsOutput transferOutPut = outPut.convertToConfirmKeyDetailsOutput();
                    confirmKeyAndRejectedDetailsList.add(transferOutPut);

                    amountPaid += transferOutPut.getAmountPaid();
                }

            }

            if (!confirmKeyAndRejectedDetailsList.isEmpty()) {
                for (ConfirmKeyDetailsOutput confirmKeyDetailsOutput : confirmKeyAndRejectedDetailsList) {
                    if (confirmKeyDetailsOutput.getIdStatus().equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
                        rejectAmount += confirmKeyDetailsOutput.getAmountPaid();

                    } else if (confirmKeyDetailsOutput.getIdStatus().equals(EstatusPlansTreatmentsKeys.AUDITADO.
                            getValor())) {
                        auditedAmount += confirmKeyDetailsOutput.getAmountPaid();
                    }
                }
            }

            amountTotal = amountPaid - rejectAmount;
            /*reporte*/

            //este pojo esta lleno asi por motivos de comodidad y no modificar cosas antiguas probadas
            CreatePaymentReportOutput createPaymentReportOutput = new CreatePaymentReportOutput();
            createPaymentReportOutput.setAmount(amountTotal);
            createPaymentReportOutput.setAuditedAmount(rejectAmount);
            createPaymentReportOutput.setNumber(consultPaymentReportOutPut.getNumberReport());

            this.generatePdfPaymentReport(date, createPaymentReportOutput, confirmKeyAndRejectedDetailsList, mes);
//
//            for (ConfirmKeyDetailsOutput confirmKeyDetails : confirmKeyAndRejectedDetailsList) {
//                if (confirmKeyDetails.getIdPropertiesPlansTreatmentsKey() == -1) {
////                    this.serviceAnalysisKeys.createPaymentReportTreatments(consultKeyDetailsStatusPaidOutPut.get(0).
////                            getIdPAymentReport() , confirmKeyDetails.getIdPlansTreatmentsKey());
//
//                } else {
////                    this.serviceAnalysisKeys.createPaymentReportProperties(consultKeyDetailsStatusPaidOutPut.get(0).
////                            getIdPAymentReport(),confirmKeyDetails.getIdPropertiesPlansTreatmentsKey());
//                }
//            }

    }

    /**
     * Method to generate reports payment for specialists whose reports were created payment
     *
     * @param date
     * @param createPaymentReportOutput
     * @param confirmKeyAndRejectedDetailsList
     * @throws IOException
     */
    public void generatePdfPaymentReport(Date date, CreatePaymentReportOutput createPaymentReportOutput,
                                         List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList, String mes)
            throws IOException {

        try {

            Specialists specialists = this.getSelectedSpecialist();
            if (specialists != null) {

                String nameReport = "RP_" + specialists.getFirstName() + specialists.getLastname() + "_"
                        + specialists.getIdentityNumber().toString() + "_" + formatDate(date);

                Map<String, Object> params = new HashMap<String, Object>();
                String logoPath = this.obtainDirectory() + this.readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_LOGO);
                String jasperPath = this.obtainDirectory() + this.readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_JASPER_SUBCRIBER);
                // Double honorarium = paymentReport.getAmount() + paymentReport.getAuditedAmount();
                DecimalFormat df = new DecimalFormat("#");
                df.setMaximumFractionDigits(8);
                params.put("number", createPaymentReportOutput.getNumber());
                params.put("totalAmount", createPaymentReportOutput.getAmount());
                params.put("auditAmount", createPaymentReportOutput.getAmount() - createPaymentReportOutput.
                        getAuditedAmount());
                params.put("rejectAmount", createPaymentReportOutput.getAuditedAmount());
                params.put("specialistName", specialists.getCompleteName());
                if (specialists.getRif() != null) {
                    params.put("specialistRif", specialists.getRif());
                } else {
                    params.put("specialistRif", "-");
                }
                params.put("date", date);
                params.put("heal_logo2", logoPath);
                
                params.put("month", mes);

                for (ConfirmKeyDetailsOutput confirmKeyDetailsOutput : confirmKeyAndRejectedDetailsList) {
                    String amountPaid = df.format(confirmKeyDetailsOutput.getAmountPaid());
                    confirmKeyDetailsOutput.setAmountPaidString(amountPaid);
                }
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                String amountTotalConvert = decimalFormat.format(createPaymentReportOutput.getAmount()
                        - createPaymentReportOutput.getAuditedAmount());
                buildReport(nameReport, confirmKeyAndRejectedDetailsList, params, jasperPath);
                sendMailSpecialist(findFile(nameReport), specialists, mes, amountTotalConvert);
            }
        } catch (ParseException | IOException e) {
            String mailAdminVenden = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_ADMIN_VENEDEN);
            sendMailVenedental(mailAdminVenden, buildMessageErrorVendental());
        }
    }

    /**
     * Method to send an email with payment specialty was the report
     *
     * @param files
     * @param specialist
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailSpecialist(List<File> files, Specialists specialist, String mes, String amountTotal)
            throws ParseException, IOException {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}", specialist.getCompleteName());
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();
        if (specialist.getEmail() != null) {
            String title = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_TITLE_SPECIALIST);
            String subjectMessage = title + " - " + specialist.getCompleteName();
            String contentMessageEspecialist = buildMessageSpecialist(specialist, mes, amountTotal);
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files,
                    specialist.getEmail());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "Operación realizada con "
                    + "éxito"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "CLAVE(S) CONFIRMADA(S) "
                    + "CON ÉXITO. EL CORREO NO FUE ENVIADO AL ESPECIALISTA, YA QUE NO POSEE DIRECCIÓN DE CORREO "
                    + "ELECTRÓNICA REGISTRADA"));
            RequestContext.getCurrentInstance().update("form:growRegisterAnalysisKeys");

            log.log(Level.INFO, "El especialista no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

    }

    /**
     * Method to construct the message body for the email that will be sent to specialist
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialist(Specialists specialist, String mes, String amountTotal)
            throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Estimado Doctor(a). ").append(specialist.getCompleteName()).append(",<br><br>");

        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT));
        stringBuilder.append(readFileProperties().getProperty(
                ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT2)).append(amountTotal).append(" ");
        stringBuilder.append(readFileProperties().getProperty(
                ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT3)).append("").append(mes);
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT4));
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT5));
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SPECIALIST_CONTENT6));
        return stringBuilder.toString();
    }

    /**
     * Method to send an email to the administrators group veneden
     *
     * @param email
     * @param contentMessage
     * @throws IOException
     */
    public void sendMailVenedental(String email, String contentMessage) throws IOException {
        List<File> files = new ArrayList<File>();
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}");
        if (email != null) {
            String subjectMessage = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_SUBJECT_VENEDEN);
            String title = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_TITLE_VENEDEN);
            this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessage, files, email);
        } else {
            log.log(Level.INFO, "Venedental no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

    }

    /**
     * Method to build the body of the error message for mail that will be sent to the group veneden
     *
     * @return String
     * @throws IOException
     */
    private String buildMessageErrorVendental() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_EMAIL_ERROR_ADMIN_CONTENT));
        return stringBuilder.toString();
    }

    /**
     * Method to seek payment report
     *
     * @return List File
     * @throws IOException
     */
    public List<File> findFile(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
        String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    public Date obtainDate() throws ParseException {

        Date fecha = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdfDate.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fechaSolicitud = sdfDate.format(fecha);
        Date dateApplication = Transformar.StringToDate(fechaSolicitud);
        return dateApplication;

    }

    /**
     * Method to get the current project path
     *
     * @return String
     */
    public static String obtainDirectory() {
        log.log(Level.INFO, "obtainDirectory()");
        URL rutaURL = ReviewKeysAnalysisRequestBean.class.getProtectionDomain().getCodeSource().getLocation();
        String path = rutaURL.getPath();
        log.log(Level.INFO, "obtainDirectory() ->");
        log.log(Level.INFO, path);
        return path.substring(0, path.indexOf("lib"));
    }

    /**
     * Method to build the report jasper
     *
     * @param nameReport
     * @param confirmKeyAndRejectedDetailsList
     * @param params
     * @param jasperPath
     * @throws IOException
     */
    public void buildReport(String nameReport, List<ConfirmKeyDetailsOutput> confirmKeyAndRejectedDetailsList,
                            Map<String, Object> params, String jasperPath) throws IOException {
        try {
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
                    confirmKeyAndRejectedDetailsList);
            JasperDesign jasperDesign = JRXmlLoader.load(jasperPath);
		JasperReport jasperReport = JasperCompileManager
				.compileReport(jasperDesign);
            System.out.println("entre por aca patilla verde");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanCollectionDataSource);
            nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
            String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
            String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + nameReport + reportExtension);
        } catch (JRException ex) {
            Logger.getLogger(ReviewKeysAnalysisRequestBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansPaymentReport.FILENAME_PAYMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansPaymentReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }
    
    
    public void reportUnify() throws SQLException{
        this.setDiabledBtnUnify(true);
        RequestContext.getCurrentInstance().update("form:btnUnify");
        Specialists specialists = this.getSelectedSpecialist();
        this.evaluateStatusReportSelect();
       this.evaluateMonthReportSelect();
        this.evaluateYearReportSelect();
        if(this.statusReportEvaluate==true&&this.monthReportEvaluate==true&&this.yearReportEvaluate==true){
            this.serviceAnalysisKeys.unifyPaymentReport(specialists.getId(),this.getIdStatusSelect(),this.getIdMonthSelect(),this.getIdYearSelect());
            this.filterKeys();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "OPERACIÓN REALIZADA CON "
                    + "ÉXITO"));
            RequestContext.getCurrentInstance().update("form:growReviewAnalysisKeys");
             
        }else{
           this.clearSelection();
           
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Información", "PARA UNIFICAR REPORTE "
                    + "DEBE SER DE UN MISMO MES,AÑO Y ESTADO"));
             RequestContext.getCurrentInstance().update("form:growReviewAnalysisKeys");
        }
        
    }
    
    public void evaluateStatusReportSelect(){
        Integer idStatusSelect=this.getConfirmListSelect().get(0).getStatus();
        this.setIdStatusSelect(idStatusSelect);
        for (ConfirmKeyOutput confirmKeyOutput : this.confirmListSelect) {
            if(confirmKeyOutput.getStatus()==idStatusSelect){
                this.setStatusReportEvaluate(true);
            }else{
                 this.setStatusReportEvaluate(false);
            }
        }
    }
    public void evaluateMonthReportSelect(){
        Integer idMonthSelect=this.getConfirmListSelect().get(0).getIdMonthExecution();
        this.setIdMonthSelect(idMonthSelect);
        for (ConfirmKeyOutput confirmKeyOutput : this.confirmListSelect) {
            if(confirmKeyOutput.getIdMonthExecution()==idMonthSelect){
                this.setMonthReportEvaluate(true);
            }else{
                 this.setMonthReportEvaluate(false);
            }
        }
    }
    
    public void evaluateYearReportSelect(){
        String dateSelect=this.getConfirmListSelect().get(0).getNameMonthExecution();
         String[] date = dateSelect.split("-");
         String yearSelect=date[1];
         String idYearString=yearSelect.replace(" ", "");
         Integer idYear=Integer.parseInt(idYearString);
         this.setIdYearSelect(idYear);
        for (ConfirmKeyOutput confirmKeyOutput : this.confirmListSelect) {
            String dateList=confirmKeyOutput.getNameMonthExecution();
            String[] dateBD = dateList.split("-");
            String yearBD=dateBD[1];
            if(yearBD.equals(yearSelect)){
                this.setYearReportEvaluate(true);
            }else{
                 this.setYearReportEvaluate(false);
            }
        }
    }
    

    /**
     * |
     * *
     * Method to create a report attributes receiving payment by parameters
     *
     * @param amount
     * @param auditedAmount
     * @return
     *
     */
    public CreatePaymentReportOutput createPaymentReport(Double amount, Double auditedAmount) {
        CreatePaymentReportOutput createPaymentReportOutput = this.serviceAnalysisKeys.createPaymentReport(amount,
                auditedAmount);
        return createPaymentReportOutput;
    }

    public Date getMaxDate() throws ParseException {
        log.log(Level.INFO, "getMaxDate()");
        maxDate = Transformar.currentDate();
        return maxDate;
    }


    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }
    
    public void prueba(){
        System.out.println("com.venedental.controller.view.ReviewKeysAnalysisViewBean.prueba() pruebaaaaaa");
    }
    
    
    public boolean filterAmountPay(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        BigDecimal bigValue = new BigDecimal(value.toString());

        return round(bigValue.doubleValue(), 2).toString().equals(filter.toString());
    }

    public Double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    

}
