/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.constans.ConstansRegisterKeysAnalysis;
import com.venedental.constans.ConstansReimbursementForm;
import com.venedental.controller.BaseBean;
import com.venedental.dto.analysisKeys.SpecialistToAuditOutput;
import com.venedental.dto.analysisKeys.FileNameForUploadOutput;
import com.venedental.dto.keyPatients.HistoricalPatientOutput;
import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;
import com.venedental.dto.propertiesPlanTreatmentKey.PropertiesPlansTreatmentsKeyOutput;
import com.venedental.dto.scaleTreatments.FindScaleRegisterOutput;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.*;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServicePatients;
import com.venedental.services.ServicePlansPatients;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePlansTreatments;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import com.venedental.services.ServicePropertiesTreatments;
import com.venedental.services.ServiceScaleTreatments;
import com.venedental.services.ServiceSpecialists;
import com.venedental.services.ServiceSpeciality;

import java.io.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 * Where class all about the second stage of analysis of key performs
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "registerKeysAnalysisViewBean")
@ViewScoped
public class RegisterKeysAnalysisViewBean extends BaseBean {


    private final RequestContext context = RequestContext.getCurrentInstance();
    private List<KeysPatients> keysPatients;
    private List<KeysPatients> keysPatientsFiltered;
    private List<PlansTreatments> plansTreatments;
    private List<PlansTreatmentsKeys> plansTreatmentsKeys;
    private List<PlansTreatmentsKeys> plansTreatmentsKeysSelected;
    private List<PlansTreatmentsKeys> patientHistorical;
    private List<PropertiesTreatments> propertiesTreatmentsPermanentI;
    private List<PropertiesTreatments> propertiesTreatmentsTemporaryI;
    private List<PropertiesTreatments> selectedPropertiesTreatmentsPermanent;
    private List<PropertiesTreatments> selectedPropertiesTreatmentsTemporary;
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKey;
    private List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeySelected;
    private List<PlansPatients> plansPatients;
    private List<SpecialistToAuditOutput> specialistsToAudit;
    private List<SpecialistToAuditOutput> specialistsToAuditFiltered;
    private List<Speciality> SpecialityOfSpecialist;
    private List<HistoricalPatientOutput> historicalPatientOutput;
    private List<HistoricalPatientOutput> historicalPatientOutputFiltered;
    private List<StatusAnalysisKeys> statusAnalysisKeysList;
    private List<ConsultNameFileOutput> consultNameFileOutput;
    private List<FileNameForUploadOutput> fileNameForUploadOutput;
    private List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeyOutput;
    private Integer numberColunmsPermanent, numberColumnsTemporary = 0;
    private Specialists specialist;
    private Specialists selectedSpecialist;
    private KeysPatients selectedKey;
    private PlansTreatments selectedPlansTreatment;
    private FindScaleRegisterOutput scaleTreatments;
    private Date dateTreatment, maxDate, dateFilter, receptionDate, dateFilterAudit;
    private Date startDate, maxDateCalendar,endDate;
    private BinnaclePlansTreatmentsKeys selectedBinnaclePlansTreatmentsKeys;
    private String[] fileNameRegisterForUpload;
    private String optionsProperties,minDate,maxDateString;
    private String whitProperties;
    private String whitoutProperties;
    private String fileSize;
    private String identityHolderName;
    private String nameFile;
    private String nameFilebd;
    private Integer idStatusSelected;
    private Patients patients;
    private Date minDateEjecution;
    private Date dateFilterHistorical;
    private StreamedContent fileRequirementD1;
    private StreamedContent fileRequirementD2;
    private StreamedContent fileRequirementD3;
    private StreamedContent fileRequirementD4;
    private StreamedContent fileRequirementD5;
    private boolean messageDataTable = false;
    private boolean inputDataEnd = false;
    private boolean btnReset= true;
    private boolean btnBuscar = true;
    private boolean visiblePanelPermanent = false;
    private boolean visiblePanelTemporary = false;
    private boolean renderedColumn = false;
    private boolean seeColunmCheck = false;
    private boolean disabledAddTreatment = true;
    private boolean seeWhitProperties = false;
    private boolean seeWhitoutProperties = false;
    private boolean seeRegisterDetails = false;
    private boolean seeRegisterList = false;
    private boolean seeSpecialistToAudit = true;
    private boolean seeColumnEdit = false;
    private boolean seeHistoricalPatient = true;
    private boolean seePendingTray = false;
    private boolean disabledBtnAudit = true;
    private boolean disabledBtnEvaluation = true;
    private boolean disabledBtnRefuse = true;
    private boolean disabledBtnDelete = true;
    private boolean seeBtnsTransactions = false;
    private boolean seeBtnBandeja = false;
    private boolean disabledBtnHistorical = true;
    private boolean inputAuditDisabled = true;
    private boolean disabledDateEjecution = true;
    private boolean disablesbtnSelectFile=false;
    private boolean disabledbtnRecaudos1=true;
    private boolean disabledbtnRecaudos2=true;
    private boolean disabledbtnRecaudos3=true;
    private boolean disabledbtnRecaudos4=true;
    private boolean disabledbtnRecaudos5=true;
    private boolean disabledfile1=true;
    private boolean disabledLabelRecaudos=true;
    private boolean disabledbtnFile=true;
    

    private double priceValue;



    private String currenNumberKeyTable;

    /**
     * titulos para el boton deshacer
     */
    private final String titleDisabledUndoKey = "No se puede deshacer la recepción de la clave, "
            + "ya que los tratamiento(s) está(n) en auditoría";
    private final String titleEnabledUndoKey = "Deshacer recepción de clave";

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;
    @EJB
    private ServiceSpeciality serviceSpeciality;
    @EJB
    private ServiceSpecialists serviceSpecialist;
    @EJB
    private ServiceKeysPatients serviceKeysPatients;
    @EJB
    private ServicePlansPatients servicePlansPatients;
    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;
    @EJB
    private ServicePlansTreatments servicePlansTreatments;
    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiesPlansTreatmentKey;
    @EJB
    private ServicePropertiesTreatments servicePropertiesTreatments;
    @EJB
    private ServiceScaleTreatments serviceScaleTreatments;
    @EJB
    private ServicePatients servicePatients;
    private static final Logger log = CustomLogger.getGeneralLogger(RegisterKeysAnalysisViewBean.class.getName());

    @PostConstruct
    public void init() {
        SecurityViewBean a = new SecurityViewBean();
        log.log(Level.INFO, "init()");
        this.setInputDataEnd(true);
        this.setBtnReset(true);
        this.setBtnBuscar(true);
        this.dateLimit();
//        goSpecialistToAudit();
        this.whitProperties = "WHITPROPERTIES";
        this.whitoutProperties = "WHITOUTPROPERTIES";
        findStatusAnalysisKeys();
        this.fileNameForUploadOutput = new ArrayList<>();
        this.propertiesPlansTreatmentsKeyOutput= new ArrayList<>();


    }



    private Double amountPay;

    private UIInput amountPayUI;

    public UIInput getAmountPayUI() {
        return amountPayUI;
    }

    public void setAmountPayUI(UIInput amountPayUI) {
        this.amountPayUI = amountPayUI;
    }

    public Double getAmountPay() {
        return amountPay;
    }

    public void setAmountPay(Double amountPay) {
        this.amountPay = amountPay;
    }

    public boolean filterAmountPay(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        BigDecimal bigValue = new BigDecimal(value.toString());

        return round(bigValue.doubleValue(),2).toString().equals(filter.toString());
    }

    public Double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public List<HistoricalPatientOutput> getHistoricalPatientOutputFiltered() {
        return historicalPatientOutputFiltered;
    }

    public void setHistoricalPatientOutputFiltered(List<HistoricalPatientOutput> historicalPatientOutputFiltered) {
        this.historicalPatientOutputFiltered = historicalPatientOutputFiltered;
    }

    public Date getDateFilterHistorical() {
        return dateFilterHistorical;
    }

    public void setDateFilterHistorical(Date dateFilterHistorical) {
        this.dateFilterHistorical = dateFilterHistorical;
    }

    public FindScaleRegisterOutput getScaleTreatments() {
        return scaleTreatments;
    }

    public void setScaleTreatments(FindScaleRegisterOutput scaleTreatments) {
        this.scaleTreatments = scaleTreatments;
    }

    public boolean isDisabledDateEjecution() {
        return disabledDateEjecution;
    }

    public void setDisabledDateEjecution(boolean disabledDateEjecution) {
        this.disabledDateEjecution = disabledDateEjecution;
    }

    public Date getMinDateEjecution() {
        return minDateEjecution;
    }

    public void setMinDateEjecution(Date minDateEjecution) {
        this.minDateEjecution = minDateEjecution;
    }

    public Date getDateFilterAudit() {
        return dateFilterAudit;
    }

    public void setDateFilterAudit(Date dateFilterAudit) {
        this.dateFilterAudit = dateFilterAudit;
    }

    public boolean isInputAuditDisabled() {
        return inputAuditDisabled;
    }

    public void setInputAuditDisabled(boolean inputAuditDisabled) {
        this.inputAuditDisabled = inputAuditDisabled;
    }

    public List<HistoricalPatientOutput> getHistoricalPatientOutput() {
        if (this.historicalPatientOutput == null) {
            this.historicalPatientOutput = new ArrayList<>();
        }
        return historicalPatientOutput;
    }

    public void setHistoricalPatientOutput(List<HistoricalPatientOutput> historicalPatientOutput) {
        this.historicalPatientOutput = historicalPatientOutput;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public boolean isSeeBtnBandeja() {
        return seeBtnBandeja;
    }

    public void setSeeBtnBandeja(boolean seeBtnBandeja) {
        this.seeBtnBandeja = seeBtnBandeja;
    }

    public boolean isSeeBtnsTransactions() {
        return seeBtnsTransactions;
    }

    public void setSeeBtnsTransactions(boolean seeBtnsTransactions) {
        this.seeBtnsTransactions = seeBtnsTransactions;
    }

    public boolean isDisabledBtnAudit() {
        return disabledBtnAudit;
    }

    public void setDisabledBtnAudit(boolean disabledBtnAudit) {
        this.disabledBtnAudit = disabledBtnAudit;
    }

    public boolean isDisabledBtnEvaluation() {
        return disabledBtnEvaluation;
    }

    public void setDisabledBtnEvaluation(boolean disabledBtnEvaluation) {
        this.disabledBtnEvaluation = disabledBtnEvaluation;
    }

    public boolean isDisabledBtnRefuse() {
        return disabledBtnRefuse;
    }

    public void setDisabledBtnRefuse(boolean disabledBtnRefuse) {
        this.disabledBtnRefuse = disabledBtnRefuse;
    }

    public boolean isDisabledBtnDelete() {
        return disabledBtnDelete;
    }

    public void setDisabledBtnDelete(boolean disabledBtnDelete) {
        this.disabledBtnDelete = disabledBtnDelete;
    }

    public boolean isSeePendingTray() {
        return seePendingTray;
    }

    public void setSeePendingTray(boolean seePendingTray) {
        this.seePendingTray = seePendingTray;
    }

    public List<StatusAnalysisKeys> getStatusAnalysisKeysList() {
        if (this.statusAnalysisKeysList == null) {
            this.statusAnalysisKeysList = new ArrayList<>();
        }
        return statusAnalysisKeysList;
    }

    public void setStatusAnalysisKeysList(List<StatusAnalysisKeys> statusAnalysisKeysList) {
        this.statusAnalysisKeysList = statusAnalysisKeysList;
    }

    public Integer getIdStatusSelected() {
        return idStatusSelected;
    }

    public void setIdStatusSelected(Integer idStatusSelected) {
        this.idStatusSelected = idStatusSelected;
    }

    public ServiceAnalysisKeys getServiceAnalysisKeys() {
        return serviceAnalysisKeys;
    }

    public void setServiceAnalysisKeys(ServiceAnalysisKeys serviceAnalysisKeys) {
        this.serviceAnalysisKeys = serviceAnalysisKeys;
    }

    public List<SpecialistToAuditOutput> getSpecialistsToAuditFiltered() {
        return specialistsToAuditFiltered;
    }

    public void setSpecialistsToAuditFiltered(List<SpecialistToAuditOutput> specialistsToAuditFiltered) {
        this.specialistsToAuditFiltered = specialistsToAuditFiltered;
    }

    public List<SpecialistToAuditOutput> getSpecialistsToAudit() {
        if (this.specialistsToAudit == null) {
            this.specialistsToAudit = new ArrayList<>();
        }
        return specialistsToAudit;
    }

    public void setSpecialistsToAudit(List<SpecialistToAuditOutput> specialistsToAudit) {
        this.specialistsToAudit = specialistsToAudit;
    }

    public boolean isSeeWhitProperties() {
        return seeWhitProperties;
    }

    public void setSeeWhitProperties(boolean seeWhitProperties) {
        this.seeWhitProperties = seeWhitProperties;
    }

    public boolean isSeeWhitoutProperties() {
        return seeWhitoutProperties;
    }

    public void setSeeWhitoutProperties(boolean seeWhitoutProperties) {
        this.seeWhitoutProperties = seeWhitoutProperties;
    }

    public String getWhitProperties() {
        return whitProperties;
    }

    public void setWhitProperties(String whitProperties) {
        this.whitProperties = whitProperties;
    }


    public List<PropertiesPlansTreatmentsKeyOutput> getPropertiesPlansTreatmentsKeyOutput() {
        return propertiesPlansTreatmentsKeyOutput;
    }

    public void setPropertiesPlansTreatmentsKeyOutput(List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeyOutput) {
        this.propertiesPlansTreatmentsKeyOutput = propertiesPlansTreatmentsKeyOutput;
    }

    public String getWhitoutProperties() {
        return whitoutProperties;
    }

    public void setWhitoutProperties(String whitoutProperties) {
        this.whitoutProperties = whitoutProperties;
    }

    public String getOptionsProperties() {
        return optionsProperties;
    }

    public void setOptionsProperties(String optionsProperties) {
        this.optionsProperties = optionsProperties;
    }

    public boolean isSeeHistoricalPatient() {
        return seeHistoricalPatient;
    }

    public void setSeeHistoricalPatient(boolean seeHistoricalPatient) {
        this.seeHistoricalPatient = seeHistoricalPatient;
    }

    public boolean isSeeColumnEdit() {
        return seeColumnEdit;
    }

    public void setSeeColumnEdit(boolean seeColumnEdit) {
        this.seeColumnEdit = seeColumnEdit;
    }

    public BinnaclePlansTreatmentsKeys getSelectedBinnaclePlansTreatmentsKeys() {
        return selectedBinnaclePlansTreatmentsKeys;
    }

    public void setSelectedBinnaclePlansTreatmentsKeys(BinnaclePlansTreatmentsKeys selectedBinnaclePlansTreatmentsKeys) {
        this.selectedBinnaclePlansTreatmentsKeys = selectedBinnaclePlansTreatmentsKeys;
    }

    public boolean isSeeSpecialistToAudit() {
        return seeSpecialistToAudit;
    }

    public void setSeeSpecialistToAudit(boolean seeSpecialistToAudit) {
        this.seeSpecialistToAudit = seeSpecialistToAudit;
    }

    public boolean getSeeRegisterDetails() {
        return seeRegisterDetails;
    }

    public void setSeeRegisterDetails(boolean seeRegisterDetails) {
        this.seeRegisterDetails = seeRegisterDetails;
    }

    public boolean getSeeRegisterList() {
        return seeRegisterList;
    }

    public void setSeeRegisterList(boolean seeRegisterList) {
        this.seeRegisterList = seeRegisterList;
    }

    public boolean getSeeColunmCheck() {
        return seeColunmCheck;
    }

    public void setSeeColunmCheck(boolean seeColunmCheck) {
        this.seeColunmCheck = seeColunmCheck;
    }

    public boolean isRenderedColumn() {
        return renderedColumn;
    }

    public void setRenderedColumn(boolean renderedColumn) {
        this.renderedColumn = renderedColumn;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public List<KeysPatients> getKeysPatientsFiltered() {
        return keysPatientsFiltered;
    }

    public void setKeysPatientsFiltered(List<KeysPatients> keysPatientsFiltered) {
        this.keysPatientsFiltered = keysPatientsFiltered;
    }



    public boolean isDisabledAddTreatment() {
        return disabledAddTreatment;
    }

    public void setDisabledAddTreatment(boolean disabledAddTreatment) {
        this.disabledAddTreatment = disabledAddTreatment;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date getDateTreatment() {
        return dateTreatment;
    }

    public void setDateTreatment(Date dateTreatment) {
        this.dateTreatment = dateTreatment;
    }

    public Integer getNumberColunmsPermanent() {
        return numberColunmsPermanent;
    }

    public void setNumberColunmsPermanent(Integer numberColunmsPermanent) {
        this.numberColunmsPermanent = numberColunmsPermanent;
    }

    public Integer getNumberColumnsTemporary() {
        return numberColumnsTemporary;
    }

    public void setNumberColumnsTemporary(Integer numberColumnsTemporary) {
        this.numberColumnsTemporary = numberColumnsTemporary;
    }

    public List<PlansPatients> getPlansPatients() {
        if (this.plansPatients == null) {
            this.plansPatients = new ArrayList<>();
        }
        return plansPatients;
    }

    public void setPlansPatients(List<PlansPatients> plansPatients) {
        this.plansPatients = plansPatients;
    }

    public boolean isVisiblePanelPermanent() {
        return visiblePanelPermanent;
    }

    public void setVisiblePanelPermanent(boolean visiblePanelPermanent) {
        this.visiblePanelPermanent = visiblePanelPermanent;
    }

    public boolean isVisiblePanelTemporary() {
        return visiblePanelTemporary;
    }

    public void setVisiblePanelTemporary(boolean visiblePanelTemporary) {
        this.visiblePanelTemporary = visiblePanelTemporary;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysSelected() {
        return plansTreatmentsKeysSelected;
    }

    public void setPlansTreatmentsKeysSelected(List<PlansTreatmentsKeys> plansTreatmentsKeysSelected) {
        this.plansTreatmentsKeysSelected = plansTreatmentsKeysSelected;
    }

    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKey() {
        if (this.propertiesPlansTreatmentsKey == null) {
            this.propertiesPlansTreatmentsKey = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKey;
    }

    public void setPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKey) {
        this.propertiesPlansTreatmentsKey = propertiesPlansTreatmentsKey;
    }

    public List<PropertiesPlansTreatmentsKeyOutput> getPropertiesPlansTreatmentsKeySelected() {
        return propertiesPlansTreatmentsKeySelected;
    }

    public void setPropertiesPlansTreatmentsKeySelected(List<PropertiesPlansTreatmentsKeyOutput> propertiesPlansTreatmentsKeySelected) {
        this.propertiesPlansTreatmentsKeySelected = propertiesPlansTreatmentsKeySelected;
    }

    

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeys() {
        if (this.plansTreatmentsKeys == null) {
            this.plansTreatmentsKeys = new ArrayList<>();
        }
        return plansTreatmentsKeys;
    }

    public List<PlansTreatmentsKeys> getPatientHistorical() {
        if (this.patientHistorical == null) {
            this.patientHistorical = new ArrayList<>();
        }
        return patientHistorical;
    }

    public void setPatientHistorical(List<PlansTreatmentsKeys> patientHistorical) {
        this.patientHistorical = patientHistorical;
    }

    public void setPlansTreatmentsKeys(List<PlansTreatmentsKeys> plansTreatmentsKeys) {
        this.plansTreatmentsKeys = plansTreatmentsKeys;
    }

    public PlansTreatments getSelectedPlansTreatment() {
        return selectedPlansTreatment;
    }

    public void setSelectedPlansTreatment(PlansTreatments selectedPlansTreatment) {
        this.selectedPlansTreatment = selectedPlansTreatment;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public List<PlansTreatments> getPlansTreatments() {
        if (this.plansTreatments == null) {
            this.plansTreatments = new ArrayList<>();
        }
        return plansTreatments;
    }

    public void setPlansTreatments(List<PlansTreatments> plansTreatments) {
        this.plansTreatments = plansTreatments;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public List<KeysPatients> getKeysPatients() {
        return keysPatients;
    }

    public void setKeysPatients(List<KeysPatients> keysPatients) {
        this.keysPatients = keysPatients;
    }

    public KeysPatients getSelectedKey() {
        return selectedKey;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsPermanentI() {
        if (this.propertiesTreatmentsPermanentI == null) {
            this.propertiesTreatmentsPermanentI = new ArrayList<>();
        }
        return propertiesTreatmentsPermanentI;
    }

    public void setPropertiesTreatmentsPermanentI(List<PropertiesTreatments> propertiesTreatmentsPermanentI) {
        this.propertiesTreatmentsPermanentI = propertiesTreatmentsPermanentI;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsTemporaryI() {
        if (this.propertiesTreatmentsTemporaryI == null) {
            this.propertiesTreatmentsTemporaryI = new ArrayList<>();
        }
        return propertiesTreatmentsTemporaryI;
    }

    public void setPropertiesTreatmentsTemporaryI(List<PropertiesTreatments> propertiesTreatmentsTemporaryI) {
        this.propertiesTreatmentsTemporaryI = propertiesTreatmentsTemporaryI;
    }

    public List<PropertiesTreatments> getSelectedPropertiesTreatmentsPermanent() {
        if (this.selectedPropertiesTreatmentsPermanent == null) {
            this.selectedPropertiesTreatmentsPermanent = new ArrayList<>();
        }
        return selectedPropertiesTreatmentsPermanent;
    }

    public void setSelectedPropertiesTreatmentsPermanent(List<PropertiesTreatments> selectedPropertiesTreatmentsPermanent) {
        this.selectedPropertiesTreatmentsPermanent = selectedPropertiesTreatmentsPermanent;
    }

    public List<PropertiesTreatments> getSelectedPropertiesTreatmentsTemporary() {
        if (this.selectedPropertiesTreatmentsTemporary == null) {
            this.selectedPropertiesTreatmentsTemporary = new ArrayList<>();
        }
        return selectedPropertiesTreatmentsTemporary;
    }

    public void setSelectedPropertiesTreatmentsTemporary(List<PropertiesTreatments> selectedPropertiesTreatmentsTemporary) {
        this.selectedPropertiesTreatmentsTemporary = selectedPropertiesTreatmentsTemporary;
    }

    public Date getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    public List<Speciality> getSpecialityOfSpecialist() {
        return SpecialityOfSpecialist;
    }

    public void setSpecialityOfSpecialist(List<Speciality> SpecialityOfSpecialist) {
        this.SpecialityOfSpecialist = SpecialityOfSpecialist;
    }

    public boolean isDisabledBtnHistorical() {
        return disabledBtnHistorical;
    }

    public void setDisabledBtnHistorical(boolean disabledBtnHistorical) {
        this.disabledBtnHistorical = disabledBtnHistorical;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMaxDateString() {
        return maxDateString;
    }

    public void setMaxDateString(String maxDateString) {
        this.maxDateString = maxDateString;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getMaxDateCalendar() {
        return maxDateCalendar;
    }

    public void setMaxDateCalendar(Date maxDateCalendar) {
        this.maxDateCalendar = maxDateCalendar;
    }

    public boolean isInputDataEnd() {
        return inputDataEnd;
    }

    public void setInputDataEnd(boolean inputDataEnd) {
        this.inputDataEnd = inputDataEnd;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isBtnBuscar() {
        return btnBuscar;
    }

    public void setBtnBuscar(boolean btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public boolean isBtnReset() {
        return btnReset;
    }

    public void setBtnReset(boolean btnReset) {
        this.btnReset = btnReset;
    }

    public Boolean getMessageDataTable() {
        return messageDataTable;
    }

    public void setMessageDataTable(Boolean messageDataTable) {
        this.messageDataTable = messageDataTable;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getIdentityHolderName() {
        return identityHolderName;
    }

    public void setIdentityHolderName(String identityHolderName) {
        this.identityHolderName = identityHolderName;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public List<ConsultNameFileOutput> getConsultNameFileOutput() {
        return consultNameFileOutput;
    }

    public void setConsultNameFileOutput(List<ConsultNameFileOutput> consultNameFileOutput) {
        this.consultNameFileOutput = consultNameFileOutput;
    }

    public String[] getFileNameRegisterForUpload() {
        return fileNameRegisterForUpload;
    }

    public void setFileNameRegisterForUpload(String[] fileNameRegisterForUpload) {
        this.fileNameRegisterForUpload = fileNameRegisterForUpload;
    }

    public List<FileNameForUploadOutput> getFileNameForUploadOutput() {
        return fileNameForUploadOutput;
    }

    public void setFileNameForUploadOutput(List<FileNameForUploadOutput> fileNameForUploadOutput) {
        this.fileNameForUploadOutput = fileNameForUploadOutput;
    }

    public StreamedContent getFileRequirementD1() {
        return fileRequirementD1;
    }

    public void setFileRequirementD1(StreamedContent fileRequirementD1) {
        this.fileRequirementD1 = fileRequirementD1;
    }

    public StreamedContent getFileRequirementD2() {
        return fileRequirementD2;
    }

    public void setFileRequirementD2(StreamedContent fileRequirementD2) {
        this.fileRequirementD2 = fileRequirementD2;
    }

    public StreamedContent getFileRequirementD3() {
        return fileRequirementD3;
    }

    public void setFileRequirementD3(StreamedContent fileRequirementD3) {
        this.fileRequirementD3 = fileRequirementD3;
    }

    public StreamedContent getFileRequirementD4() {
        return fileRequirementD4;
    }

    public void setFileRequirementD4(StreamedContent fileRequirementD4) {
        this.fileRequirementD4 = fileRequirementD4;
    }

    public StreamedContent getFileRequirementD5() {
        return fileRequirementD5;
    }

    public void setFileRequirementD5(StreamedContent fileRequirementD5) {
        this.fileRequirementD5 = fileRequirementD5;
    }

    public boolean isDisablesbtnSelectFile() {
        return disablesbtnSelectFile;
    }

    public void setDisablesbtnSelectFile(boolean disablesbtnSelectFile) {
        this.disablesbtnSelectFile = disablesbtnSelectFile;
    }

    public String getNameFilebd() {
        return nameFilebd;
    }

    public void setNameFilebd(String nameFilebd) {
        this.nameFilebd = nameFilebd;
    }

    public boolean isDisabledbtnRecaudos1() {
        return disabledbtnRecaudos1;
    }

    public void setDisabledbtnRecaudos1(boolean disabledbtnRecaudos1) {
        this.disabledbtnRecaudos1 = disabledbtnRecaudos1;
    }

    public boolean isDisabledbtnRecaudos2() {
        return disabledbtnRecaudos2;
    }

    public void setDisabledbtnRecaudos2(boolean disabledbtnRecaudos2) {
        this.disabledbtnRecaudos2 = disabledbtnRecaudos2;
    }

    public boolean isDisabledbtnRecaudos3() {
        return disabledbtnRecaudos3;
    }

    public void setDisabledbtnRecaudos3(boolean disabledbtnRecaudos3) {
        this.disabledbtnRecaudos3 = disabledbtnRecaudos3;
    }

    public boolean isDisabledbtnRecaudos4() {
        return disabledbtnRecaudos4;
    }

    public void setDisabledbtnRecaudos4(boolean disabledbtnRecaudos4) {
        this.disabledbtnRecaudos4 = disabledbtnRecaudos4;
    }

    public boolean isDisabledbtnRecaudos5() {
        return disabledbtnRecaudos5;
    }

    public void setDisabledbtnRecaudos5(boolean disabledbtnRecaudos5) {
        this.disabledbtnRecaudos5 = disabledbtnRecaudos5;
    }

    public boolean isDisabledfile1() {
        return disabledfile1;
    }

    public void setDisabledfile1(boolean disabledfile1) {
        this.disabledfile1 = disabledfile1;
    }

    public boolean isDisabledLabelRecaudos() {
        return disabledLabelRecaudos;
    }

    public void setDisabledLabelRecaudos(boolean disabledLabelRecaudos) {
        this.disabledLabelRecaudos = disabledLabelRecaudos;
    }

    public boolean isDisabledbtnFile() {
        return disabledbtnFile;
    }

    public void setDisabledbtnFile(boolean disabledbtnFile) {
        this.disabledbtnFile = disabledbtnFile;
    }

    
    
    

    
    
    
    

    /**
     * method for find status for the filters
     */
    public void findStatusAnalysisKeys() {
        log.log(Level.INFO, "findStatusAnalysisKeys()");
        this.setStatusAnalysisKeysList(this.serviceAnalysisKeys.findAllStatusAnalysisKeys());
        List<StatusAnalysisKeys> statusAnalysisKeysAux = new ArrayList<>();
        if (!this.statusAnalysisKeysList.isEmpty()) {
            for (StatusAnalysisKeys analysisKeys : this.statusAnalysisKeysList) {
                if (Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.RECHAZADO.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.EN_EVALUACION.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.AUDITADO.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.RECIBIDO.getValor())
                        || Objects.equals(analysisKeys.getId(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor())) {
                    statusAnalysisKeysAux.add(analysisKeys);
                }
            }
        }
        this.setStatusAnalysisKeysList(statusAnalysisKeysAux);
    }

    /**
     * *
     * Running method is barely up dialogue
     *
     * @param selectedKey
     */
    public void setSelectedKey(KeysPatients selectedKey) throws SQLException {
        log.log(Level.INFO, "setSelectedKey()");
        this.selectedKey = selectedKey;
        this.setSpecialityOfSpecialist(this.serviceSpeciality.findSpecialityOfSpecialist(this.getSelectedSpecialist().
                getId()));
        filterPlansBySpeciality();
        this.setOptionsProperties(this.getWhitProperties());
        int status = EstatusPlansTreatmentsKeys.RECIBIDO.getValor();
        this.setIdStatusSelected(status);
        findPlansTreatments();
        findPlansTratmentsKeys();
        this.setHistoricalPatientOutput(this.serviceKeysPatients.findHistoricalPatient(this.getSelectedKey().
                getIdPatient(), this.getSelectedKey().getId()));

        if (this.getHistoricalPatientOutput().isEmpty()) {
            this.setDisabledBtnHistorical(true);
        } else {
            this.setDisabledBtnHistorical(false);
        }
        this.setPatients(this.servicePatients.findPatientById(this.getSelectedKey().getIdPatient()));
        RequestContext.getCurrentInstance().update("form:dateOfBirthAnalysisRegister");
        RequestContext.getCurrentInstance().update("form:btnHistorical");
        RequestContext.getCurrentInstance().update("form:panelTreatmentsAnalysisRegister");
    }

    public void findHistoricalPatient() {
        this.setHistoricalPatientOutput(new ArrayList<HistoricalPatientOutput>());
        this.setHistoricalPatientOutput(this.serviceKeysPatients.findHistoricalPatient(this.getSelectedKey().
                getIdPatient(), this.getSelectedKey().getId()));


        RequestContext.getCurrentInstance().update("form:registerAnalysispatientHistorical");
    }

    /**
     * Method that allows you to find the keys analysis or state analysis and also treatments that have received, is
     * executed when selected a specialist
     *
     *
     * @param specialistId
     * @param receptionDate
     */
    public void filterKeys(int specialistId, Date receptionDate) {
        log.log(Level.INFO, "filterKeys()");
        resetDataTableKeys();
        this.setReceptionDate(receptionDate);
        this.setSelectedSpecialist(this.serviceSpecialist.findSpecialistById(specialistId));

        int statusKeysRecived = EstatusKeysPatients.RECIBIDO.getValor();

        this.setKeysPatients(this.serviceKeysPatients.findKeysToAuditBySpecialistAndDate(this.getSelectedSpecialist().
                getId(), statusKeysRecived, receptionDate, receptionDate));

        for (int i = 0; i < this.getKeysPatients().size(); i++) {

            this.getKeysPatients().get(i).setUndoReceiveKey(
                    !this.serviceAnalysisKeys.findKeyInStatusGenerate(this.getKeysPatients().get(i).getId()).isKeyStatusGenered()
            );

//            this.serviceAnalysisKeys.findKeyInStatusGenerate(
//                            this.getKeysPatients().get(i).getId()).isKeyStatusGenered()
//             this.serviceAnalysisKeys.findKeyHaveTreatment(
//                            this.getKeysPatients().get(i).getId()).getContainsTreatment()
        }

        this.getKeysPatients();
        if (!this.getKeysPatients().isEmpty()) {
            this.setRenderedColumn(true);
        } else {
            this.setRenderedColumn(false);
        }
        this.goRegisterList();
        RequestContext.getCurrentInstance().update("form:registerAnalysisKeysList");

    }

    public void resetDataTableKeys() {
        log.log(Level.INFO, "resetDataTableKeys()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:registerAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:registerAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:registerAnalysisKeysList");
        }
    }

    /**
     * Method that allows filtering by specialty plans
     */
    public void filterPlansBySpeciality() {
        log.log(Level.INFO, "filterPlansBySpeciality()");
        Integer idTypeSpeciality = this.getSelectedSpecialist().getIdTypeSpeciality();
        this.setPlansPatients(new ArrayList<PlansPatients>());
        List<PlansPatients> plansPatientsP = this.servicePlansPatients.findPlanPatientByIdPatient(this.getSelectedKey().
                getIdPatient());
        if (!plansPatientsP.isEmpty()) {
            for (PlansPatients plansPatient : plansPatientsP) {
                if (plansPatient.getPlanTypeSpecialistId().equals(idTypeSpeciality)) {
                    this.getPlansPatients().add(plansPatient);
                }
            }
        }
    }

    /**
     * search method that allows treatment plans to be shown in the combo for the registration of a new treatment to the
     * key
     */
    public void findPlansTreatments() {
        log.log(Level.INFO, "findPlansTreatments()");
        this.setPlansTreatments(this.servicePlansTreatments.findPlanTreaByKeyAndPlan(null, this.getSelectedKey().getId()));
        RequestContext.getCurrentInstance().update("form:panelTreatmentsAnalysisRegister");
    }

    /**
     * method for searching for treatments that are state received
     */
    public void findPlansTratmentsKeys() throws SQLException {
        log.log(Level.INFO, "findPlansTratmentsKeys()");
        if (this.getOptionsProperties().equals(this.getWhitoutProperties())) {
            this.setPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
            this.setPlansTreatmentsKeys(this.servicePlansTreatmentKeys.findPlanTreatKeySPByKeyAndStatus(
                    this.getSelectedKey().getId(), this.getIdStatusSelected(), this.getReceptionDate()));

            this.setSeeWhitoutProperties(true);
            this.setSeeWhitProperties(false);
        } else if (this.getOptionsProperties().equals(this.getWhitProperties())) {
            this.setPropertiesPlansTreatmentsKeyOutput(new ArrayList<PropertiesPlansTreatmentsKeyOutput>());
            this.setPropertiesPlansTreatmentsKeyOutput(this.servicePropertiesPlansTreatmentKey.findProPlanstkStatusByKeyAndStatusFiles(
                    this.getSelectedKey().getId(), this.getIdStatusSelected(), this.getReceptionDate()));

            if(this.getPropertiesPlansTreatmentsKeyOutput()!=null){
                for (PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput : this.propertiesPlansTreatmentsKeyOutput) {
                    if(propertiesPlansTreatmentsKeyOutput.getPoseeRecaudos()==1){
                        this.setDisabledLabelRecaudos(true);
                        RequestContext.getCurrentInstance().update("form:labelSinRecaudos");
                        this.consultFileNameRegister(propertiesPlansTreatmentsKeyOutput.getId());
                        if(!this.getNameFilebd().equals(null)){
                            String[] nameFiles = this.nameFilebd.split(",");
                            Integer cantidadfiles=nameFiles.length;
                            if(cantidadfiles==1){
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[0]);
                               
                            }

                            else if(cantidadfiles==2){

                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[0]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement2(nameFiles[1]);
                               

                            }else if(cantidadfiles==3){
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[0]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[1]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement3(nameFiles[2]);
                               

                            } else if(cantidadfiles==4){
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[0]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement2(nameFiles[1]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement3(nameFiles[2]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement4(nameFiles[3]);
                               

                            }else if(cantidadfiles==5){
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement1(nameFiles[0]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement2(nameFiles[1]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement3(nameFiles[2]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement4(nameFiles[3]);
                                propertiesPlansTreatmentsKeyOutput.setFileRequirement5(nameFiles[4]);
                               

                            }
                        }else{
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement1("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement2("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement3("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement4("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement5("");
                           
                        }
                        
                    }else{
                         propertiesPlansTreatmentsKeyOutput.setFileRequirement1("");
                        propertiesPlansTreatmentsKeyOutput.setFileRequirement2("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement3("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement4("");
                            propertiesPlansTreatmentsKeyOutput.setFileRequirement5("");
                           
                           
                        
                        
                    }
                            
                    

                }
            }





            this.setSeeWhitProperties(true);
            this.setSeeWhitoutProperties(false);
            System.out.println("com.venedental.controller.view.RegisterKeysAnalysisViewBean.findPlansTratmentsKeys()"+this.propertiesPlansTreatmentsKeyOutput);
        }

        if ((!this.getPlansTreatmentsKeys().isEmpty() || !this.getPropertiesPlansTreatmentsKeyOutput().isEmpty())
                && !this.getIdStatusSelected().equals(EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
            this.setSeeColunmCheck(true);
        } else {
            this.setSeeColunmCheck(false);
        }
        
        RequestContext.getCurrentInstance().update("form:plansTreatmentsKeysAnalysisRegister");
        RequestContext.getCurrentInstance().update("form:propertiesPlansTreatmentsKeysAnalysisRegister");
        RequestContext.getCurrentInstance().update("form");

    }

    /**
     * Method for searching and ordering treatment to be registered is selected in the combo for such parts are shown in
     * the odontodiagrama
     */
    public void findPropertiesTreatments() {
        log.log(Level.INFO, "findPropertiesTreatments()");
        enabledAddTratment();
        findDateScale();
        if (this.getSelectedPlansTreatment() != null) {
             this.setDisabledbtnFile(false);
                RequestContext.getCurrentInstance().update("form:identityHolderForm:identityHolder");
            List<PropertiesTreatments> propertiesTreatmentsList = new ArrayList<>();
            propertiesTreatmentsList = this.servicePropertiesTreatments.findProTreatByKeyAndTreatments(this.
                    getSelectedPlansTreatment().getIdTreatments(), this.getSelectedKey().getId());
            if (!propertiesTreatmentsList.isEmpty()) {
                List<PropertiesTreatments> propertiesPermanentI = new ArrayList<>();
                List<PropertiesTreatments> propertiesPermanentII = new ArrayList<>();
                List<PropertiesTreatments> propertiesPermanentIII = new ArrayList<>();
                List<PropertiesTreatments> propertiesPermanentIV = new ArrayList<>();
                List<PropertiesTreatments> propertiesTemporaryI = new ArrayList<>();
                List<PropertiesTreatments> propertiesTemporaryII = new ArrayList<>();
                List<PropertiesTreatments> propertiesTemporaryIII = new ArrayList<>();
                List<PropertiesTreatments> propertiesTemporaryIV = new ArrayList<>();
                for (PropertiesTreatments propertiesTreatments : propertiesTreatmentsList) {
                    int quadrant = propertiesTreatments.getPropertieQuadrant();
                    if (quadrant == 1) {
                        propertiesPermanentI.add(propertiesTreatments);
                    } else if (quadrant == 2) {
                        propertiesPermanentII.add(propertiesTreatments);
                    } else if (quadrant == 3) {
                        propertiesPermanentIII.add(propertiesTreatments);
                    } else if (quadrant == 4) {
                        propertiesPermanentIV.add(propertiesTreatments);
                    } else if (quadrant == 5) {
                        propertiesTemporaryI.add(propertiesTreatments);
                    } else if (quadrant == 6) {
                        propertiesTemporaryII.add(propertiesTreatments);
                    } else if (quadrant == 7) {
                        propertiesTemporaryIII.add(propertiesTreatments);
                    } else if (quadrant == 8) {
                        propertiesTemporaryIV.add(propertiesTreatments);
                    }
                }
                /*Sorting the list from Permanents by quadrant for display in the view*/
                this.setPropertiesTreatmentsPermanentI(new ArrayList<PropertiesTreatments>());
                this.setPropertiesTreatmentsTemporaryI(new ArrayList<PropertiesTreatments>());
                this.setSelectedPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
                this.setSelectedPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
                if (!propertiesPermanentI.isEmpty()) {
                    Collections.reverse(propertiesPermanentI);
                    this.getPropertiesTreatmentsPermanentI().addAll(propertiesPermanentI);
                }
                if (!propertiesPermanentIII.isEmpty()) {
                    this.getPropertiesTreatmentsPermanentI().addAll(propertiesPermanentIII);
                }
                if (!propertiesPermanentII.isEmpty()) {
                    Collections.reverse(propertiesPermanentII);
                    this.getPropertiesTreatmentsPermanentI().addAll(propertiesPermanentII);
                }
                if (!propertiesPermanentIV.isEmpty()) {
                    this.getPropertiesTreatmentsPermanentI().addAll(propertiesPermanentIV);
                }
                /*Sorting the list from Temporary by quadrant for display in the view*/
                if (!propertiesTemporaryI.isEmpty()) {
                    Collections.reverse(propertiesTemporaryI);
                    this.getPropertiesTreatmentsTemporaryI().addAll(propertiesTemporaryI);
                }
                if (!propertiesTemporaryIII.isEmpty()) {
                    this.getPropertiesTreatmentsTemporaryI().addAll(propertiesTemporaryIII);
                }
                if (!propertiesTemporaryII.isEmpty()) {
                    Collections.reverse(propertiesTemporaryII);
                    this.getPropertiesTreatmentsTemporaryI().addAll(propertiesTemporaryII);
                }
                if (!propertiesTemporaryIV.isEmpty()) {
                    this.getPropertiesTreatmentsTemporaryI().addAll(propertiesTemporaryIV);
                }

                defineColumnsAndVisibility();
                RequestContext.getCurrentInstance().update("form:panelPropertiesTreatments");

            } else {
                this.setVisiblePanelPermanent(false);
                this.setVisiblePanelTemporary(false);
                this.setSelectedPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
                this.setSelectedPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
                this.setDisabledbtnFile(true);
                RequestContext.getCurrentInstance().update("form:identityHolderForm:identityHolder");
                RequestContext.getCurrentInstance().update("form:panelPropertiesTreatments");
            }
        } else {
            this.setVisiblePanelPermanent(false);
            this.setVisiblePanelTemporary(false);
            this.setSelectedPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
            this.setSelectedPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
            this.setDateTreatment(null);
            RequestContext.getCurrentInstance().update("form:treatmentsDateAnalysisRegister");
            RequestContext.getCurrentInstance().update("form:panelPropertiesTreatments");

        }

    }

    public void enabledAddTratment() {
        log.log(Level.INFO, "enabledAddTratment()");
        if (this.getSelectedPlansTreatment() != null && this.getDateTreatment() != null) {
            this.setDisabledAddTreatment(false);
        } else {
            this.setDisabledAddTreatment(true);
        }
        RequestContext.getCurrentInstance().update("form:btnAddTreatmentAlaysisRegister");
                

    }

    /**
     * method that allows Define number columns and visibility for temporary o permanent teeth for the treatment to be
     * recorded
     */
    public void defineColumnsAndVisibility() {
        log.log(Level.INFO, "defineColumnsAndVisibility()");
        if (!this.getPropertiesTreatmentsPermanentI().isEmpty()) {
            this.setVisiblePanelPermanent(true);
            if (this.getPropertiesTreatmentsPermanentI().size() == 1) {
                this.setNumberColunmsPermanent(1);
            } else if ((this.getPropertiesTreatmentsPermanentI().size() % 2) == 0) {
                this.setNumberColunmsPermanent(this.getPropertiesTreatmentsPermanentI().size() / 2);
            } else {
                this.setNumberColunmsPermanent((this.getPropertiesTreatmentsPermanentI().size() / 2) + 1);
            }
        } else {
            this.setVisiblePanelPermanent(false);
        }
        if (!this.getPropertiesTreatmentsTemporaryI().isEmpty()) {
            this.setVisiblePanelTemporary(true);
            if (this.getPropertiesTreatmentsTemporaryI().size() == 1) {
                this.setNumberColumnsTemporary(1);
            } else if ((this.getPropertiesTreatmentsTemporaryI().size() % 2) == 0) {
                this.setNumberColumnsTemporary(this.getPropertiesTreatmentsTemporaryI().size() / 2);
            } else {
                this.setNumberColumnsTemporary((this.getPropertiesTreatmentsTemporaryI().size() / 2) + 1);
            }

        } else {
            this.setVisiblePanelTemporary(false);
        }
    }

    /**
     * method for find scaleTreatment for selected PlansTreatment looking for the nearest scale (below) for the selected
     * date
     *
     * @param idPropertieTreatment
     * @return ScaleTreatments
     */
    public FindScaleRegisterOutput findScaleTreatment(Integer idPropertieTreatment) {
        log.log(Level.INFO, "findScaleTreatment()");
        if (this.getSelectedPlansTreatment() != null && this.getSelectedSpecialist() != null) {
            this.setScaleTreatments(new FindScaleRegisterOutput());
            this.setScaleTreatments(this.serviceScaleTreatments.findScaleTratmentRegister(
                    this.getSelectedSpecialist().getId(), this.getSelectedPlansTreatment().getIdTreatments(),
                    this.getDateTreatment(), idPropertieTreatment));
        }
        return this.getScaleTreatments();
    }

    /**
     * Method that allows you to clean all the lists used in this controlled
     */
    public void clear() {
        log.log(Level.INFO, "clear()");
        this.setPropertiesTreatmentsPermanentI(new ArrayList<PropertiesTreatments>());
        this.setPropertiesTreatmentsTemporaryI(new ArrayList<PropertiesTreatments>());
        this.setSelectedPlansTreatment(new PlansTreatments());
        this.setSelectedPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
        this.setSelectedPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
        this.setPlansTreatmentsKeys(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansTreatmentsKeysSelected(new ArrayList<PlansTreatmentsKeys>());
        this.setPlansPatients(new ArrayList<PlansPatients>());
        this.setScaleTreatments(new FindScaleRegisterOutput());
        this.setVisiblePanelPermanent(false);
        this.setVisiblePanelTemporary(false);
    }

    /*Clean and hide lists added after treatment */
    /**
     * method that allows clear and hide lists added after treatment
     */
    public void clearPropertiesTreatment() {
        log.log(Level.INFO, "clearPropertiesTreatment()");
        this.setSelectedPlansTreatment(null);
        this.setPlansTreatments(new ArrayList<PlansTreatments>());
        this.setDateTreatment(null);
        if (!this.getPropertiesTreatmentsPermanentI().isEmpty()) {
            this.getPropertiesTreatmentsPermanentI().clear();
            this.setSelectedPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());

            this.setVisiblePanelPermanent(false);
        }
        if (!this.getPropertiesTreatmentsTemporaryI().isEmpty()) {
            this.getPropertiesTreatmentsTemporaryI().clear();
            this.setSelectedPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
            this.setVisiblePanelTemporary(false);
        }
        RequestContext.getCurrentInstance().update("form:panelTreatmentsAnalysisRegister");
        RequestContext.getCurrentInstance().update("form:panelPropertiesTreatments");
    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelect(SelectEvent event) {
        log.log(Level.INFO, "handleDateSelect()--->");


        RequestContext.getCurrentInstance().execute("" +
                " if(PF('registerAnalysisTable') != null ){PF('registerAnalysisTable').filter()} " +
                " if(PF('patientHistorical') != null ){PF('patientHistorical').filter()}" +
                " if(PF('registerSpecialistToAuditWiget') != null){PF('registerSpecialistToAuditWiget').filter()}  "
        );

    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelectHistorical(SelectEvent event) {
        log.log(Level.INFO, "handleDateSelect()");

        RequestContext.getCurrentInstance().execute("" +
                " if(PF('registerAnalysisTable') != null){PF('registerAnalysisTable').filter()} " +
                "  if(PF('patientHistorical') != null){PF('patientHistorical').filter()}"
        );
    }

    /**
     * DateTrasnformar method that allows a date to the form dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public Date formatDate(Date date) throws ParseException {
        log.log(Level.INFO, "formatDate()");
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;

    }

    public void goRegisterKeysDetails() {
        log.log(Level.INFO, "goRegisterKeysDetails()");
        clearSelection();
        this.setSeeRegisterDetails(true);
        this.setSeeRegisterList(false);
        this.setSeeSpecialistToAudit(false);
        this.setSeePendingTray(true);
        this.setSeeBtnBandeja(true);
        this.setDateTreatment(null);
        RequestContext.getCurrentInstance().update("form");
    }

    public void goUndoKey(KeysPatients keysPatients) throws SQLException {
        log.log(Level.INFO, "goUndoKey()");
        this.serviceAnalysisKeys.changeStatusKeyGenerated(keysPatients.getId(),this.receptionDate);
        RequestContext.getCurrentInstance().update("form:registerAnalysisKeysList");
        resetDataSpecialistToAudit();
        FacesContext contexto = FacesContext.getCurrentInstance();
        contexto.addMessage(null, new FacesMessage("", "OPERACIÓN REALIZADA CON ÉXITO"));
    }

    public void goRegisterList() {
        log.log(Level.INFO, "goRegisterList()");
        this.setSeeRegisterDetails(false);
        this.setSeeRegisterList(true);
        this.setSeeSpecialistToAudit(false);
        this.setSeeWhitProperties(false);
        this.setSeeWhitoutProperties(false);
        this.setSeePendingTray(true);
        this.setSeeBtnBandeja(true);
        this.clear();
        this.setDisabledDateEjecution(true);
        resetDataTableKeys();
        clearSelection();
        RequestContext.getCurrentInstance().update("form");
    }

    public void cleanAndDate() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:registerSpecialistToAudit");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:registerSpecialistToAudit");
//

        }
        this.setSpecialistsToAudit(null);
        this.setBtnReset(true);
        this.setBtnBuscar(true);
        this.setInputDataEnd(true);
        this.setStartDate(null);
        this.setEndDate(null);
        this.setMaxDate(null);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:startDate");
        RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
//


    }


    //      * Method at converter dateUtil and dateSql
//     *
//     * @param date
//     * @return
//     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }


    public void goSpecialistToAudit() {
        log.log(Level.INFO, "goSpecialistToAudit()");
        java.sql.Date dateStartSql = null;
        java.sql.Date dateEndSql = null;

        clearSelection();
        this.setSeeRegisterDetails(false);
        this.setSeeRegisterList(false);
        this.setSeeSpecialistToAudit(true);
        this.setSeeWhitProperties(false);
        this.setSeeWhitoutProperties(false);
        this.setSeePendingTray(false);
        this.setSeeBtnBandeja(false);
        this.setDisabledDateEjecution(true);
        int idStatus = EstatusKeysPatients.RECIBIDO.getValor();
//        SP_M_ACL_EspecialistaAuditar
        dateStartSql = this.convertJavaDateToSqlDate(
                this.getStartDate());
        dateEndSql = this.convertJavaDateToSqlDate(
                this.getEndDate());
        this.setSpecialistsToAudit(this.serviceAnalysisKeys.findSpecialistToAudit(idStatus, dateStartSql, dateEndSql));
        List<KeysPatients> listKeys = new ArrayList<>();
        for (SpecialistToAuditOutput specialistAudit : this.getSpecialistsToAudit()) {
//            SP_T_KEYSPATIENTS_G_005
            listKeys = this.serviceKeysPatients.findKeysToAuditBySpecialistAndDate(specialistAudit.getIdSpecialist(),
                    EstatusKeysPatients.RECIBIDO.getValor(), specialistAudit.getDateInitiation(),
                    specialistAudit.getDateInitiation());
//            SP_T_KEYSPATIENTS_G_005
            listKeys.addAll(this.serviceKeysPatients.findKeysToAuditBySpecialistAndDate(specialistAudit.getIdSpecialist(),
                    EstatusKeysPatients.POR_FACTURAR.getValor(), specialistAudit.getDateInitiation(),
                    specialistAudit.getDateInitiation()));

            for (KeysPatients key : listKeys) {

                if (key.getStatusName().equals("RECIBIDO")) {
                    specialistAudit.setNameStatusAnalysisKeys("POR AUDITAR");
                    break;
                } else if (specialistAudit.getNameStatusAnalysisKeys() == null
                        || !specialistAudit.getNameStatusAnalysisKeys().equals("POR AUDITAR")) {
                    if (key.getStatusName().equals("EN EVALUACIÓN")) {
                        specialistAudit.setNameStatusAnalysisKeys("POR EVALUAR");
                    } else if (specialistAudit.getNameStatusAnalysisKeys() == null
                            || !specialistAudit.getNameStatusAnalysisKeys().equals("POR EVALUAR")) {
                        specialistAudit.setNameStatusAnalysisKeys("POR CONFIRMAR");
                    }
                }
            }

        }

        if (!this.getSpecialistsToAudit().isEmpty()) {
            this.setSeeColumnEdit(true);
            this.setInputAuditDisabled(false);
            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
            RequestContext.getCurrentInstance().update("form:columnEdit");
        } else {
            this.setSeeColumnEdit(false);
            this.setInputAuditDisabled(true);
            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
            RequestContext.getCurrentInstance().update("form:columnEdit");
        }
        RequestContext.getCurrentInstance().update("form");
    }

    public void resetDataSpecialistToAudit() {
        log.log(Level.INFO, "resetDataSpecialistToAudit()");
        goSpecialistToAudit();
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:registerSpecialistToAudit");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilterAudit(null);
            RequestContext.getCurrentInstance().reset("form:registerSpecialistToAudit");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
            RequestContext.getCurrentInstance().update("form:audit1");
            RequestContext.getCurrentInstance().update("form:audit2");
            RequestContext.getCurrentInstance().update("form:audit3");
        }

    }

    public void handleDateSelectSpecialistToAudit() {
        log.log(Level.INFO, "handleDateSelectSpecialistToAudit()------------>>>>>>>>>>");

        RequestContext.getCurrentInstance().execute("PF('registerSpecialistToAuditWiget').filter()");
    }

    public void selectRow() {
        log.log(Level.INFO, "selectRow()");
        btnVisivility(this.getIdStatusSelected());
        updateBtns();
    }

    public void disabledBtnsAudit() {
        log.log(Level.INFO, "disabledBtnsAudit()");
        this.setDisabledBtnAudit(true);
        this.setDisabledBtnEvaluation(true);
        this.setDisabledBtnRefuse(true);
        this.setDisabledBtnDelete(true);
        updateBtns();
    }

    public void unSelectRow() {
        log.log(Level.INFO, "unSelectRow()");
        if (this.getPlansTreatmentsKeysSelected().isEmpty() && this.getPropertiesPlansTreatmentsKeySelected().isEmpty()) {
            disabledBtnsAudit();
        }
    }

    public void toggleSelect() {
        log.log(Level.INFO, "toggleSelect()");
        if (this.getPlansTreatmentsKeysSelected().isEmpty() && this.getPropertiesPlansTreatmentsKeySelected().isEmpty()) {
            disabledBtnsAudit();
        } else {
            btnVisivility(this.getIdStatusSelected());
        }
        updateBtns();

    }

    public void btnVisivility(Integer idStatus) {
        log.log(Level.INFO, "btnVisivility()");
        if (Objects.equals(idStatus, EstatusPlansTreatmentsKeys.AUDITADO.getValor())) {
            this.setDisabledBtnAudit(true);
            this.setDisabledBtnEvaluation(false);
            this.setDisabledBtnRefuse(false);
            this.setDisabledBtnDelete(false);
        }
        if (Objects.equals(idStatus, EstatusPlansTreatmentsKeys.RECIBIDO.getValor())) {
            this.setDisabledBtnAudit(false);
            this.setDisabledBtnEvaluation(false);
            this.setDisabledBtnRefuse(false);
            this.setDisabledBtnDelete(false);
        }
        if (Objects.equals(idStatus, EstatusPlansTreatmentsKeys.RECHAZADO.getValor())) {
            this.setDisabledBtnAudit(true);
            this.setDisabledBtnEvaluation(true);
            this.setDisabledBtnRefuse(true);
            this.setDisabledBtnDelete(true);
        }
        if (Objects.equals(idStatus, EstatusPlansTreatmentsKeys.EN_EVALUACION.getValor())) {
            this.setDisabledBtnAudit(false);
            this.setDisabledBtnEvaluation(true);
            this.setDisabledBtnRefuse(false);
            this.setDisabledBtnDelete(false);
        }
        updateBtns();

    }

    public void updateBtns() {
        log.log(Level.INFO, "updateBtns()");
        RequestContext.getCurrentInstance().update("form:analysisBtnAudit");
        RequestContext.getCurrentInstance().update("form:analysisBtnEvaluation");
        RequestContext.getCurrentInstance().update("form:analysisBtnRefuse");
        RequestContext.getCurrentInstance().update("form:analysisBtnDelete");
    }

    public void clearSelection() {
        log.log(Level.INFO, "clearSelection()");
        this.setPlansTreatmentsKeysSelected(new ArrayList<PlansTreatmentsKeys>());
        this.setPropertiesPlansTreatmentsKeySelected(new ArrayList<PropertiesPlansTreatmentsKeyOutput>());
        disabledBtnsAudit();
        RequestContext.getCurrentInstance().update("form:plansTreatmentsKeysAnalysisRegister");
        RequestContext.getCurrentInstance().update("form:propertiesPlansTreatmentsKeysAnalysisRegister");
    }

    public void findDateScale() {
        log.log(Level.INFO, "findDateScale()");
        if (this.getSelectedPlansTreatment() != null) {
            Date minDate = this.serviceScaleTreatments.findMinDateScale(this.getSelectedSpecialist().getId(),
                    this.getSelectedPlansTreatment().getIdTreatments());
            this.setMinDateEjecution(this.getSelectedKey().getApplicationDate());
            this.setMinDateEjecution(minDate);
            this.setDisabledDateEjecution(false);
        } else {
            this.setDisabledDateEjecution(true);
        }

        RequestContext.getCurrentInstance().update("form:treatmentsDateAnalysisRegister");
    }

    public void resetHistorical() {
        log.log(Level.INFO, "resetHistorical()()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:registerAnalysispatientHistorical");
        if (dataTable != null) {
            dataTable.reset();
            this.setDateFilterHistorical(null);
            RequestContext.getCurrentInstance().reset("form:registerAnalysispatientHistorical");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:registerAnalysispatientHistorical");
        }

    }


    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int limYear = year + 1;
        int minYear = year - 1;
        int hasta = c.get(Calendar.DATE) + 1;

        this.setMinDate("23/10/2015");
        this.setMaxDateString(String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year));
//        this.setMinDateHasta(String.valueOf(hasta)+"/"+String.valueOf(month)+"/"+String.valueOf(year));






    }


    public void selectDateConsultKeys() {
        if(this.getEndDate()!=null){
            this.setEndDate(null);
            this.setInputDataEnd(false);
            this.setBtnReset(false);
            this.setBtnBuscar(true);
            RequestContext.getCurrentInstance().update("form:btnResetKey");
            RequestContext.getCurrentInstance().update("form:btnFindKey");
            RequestContext.getCurrentInstance().update("form:endDate");
            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
        }else{
            this.setInputDataEnd(false);
            this.setBtnReset(false);
            this.setBtnBuscar(true);
            RequestContext.getCurrentInstance().update("form:btnResetKey");
            RequestContext.getCurrentInstance().update("form:btnFindKey");
            RequestContext.getCurrentInstance().update("form:endDate");
            RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");
        }


    }

    public void selectDateEndConsultKeys() {
        this.setBtnBuscar(false);
        this.setBtnReset(false);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:registerSpecialistToAudit");

    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        log.log(Level.INFO, "readFileProperties()");
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansReimbursementForm.FILENAME_REIMBURSEMENT_REQUEST);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansReimbursementForm.FILENAME_REIMBURSEMENT_REQUEST
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    public String getExtention(String extention) {
        return "." + extention.substring(extention.indexOf("/") + 1, extention.length());
    }
    
    public  String remove1(String input) {
    // Cadena de caracteres original a sustituir.
    String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
    // Cadena de caracteres ASCII que reemplazarán los originales.
    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
    String output = input;
    for (int i=0; i<original.length(); i++) {
        // Reemplazamos los caracteres especiales.
        output = output.replace(original.charAt(i), ascii.charAt(i));
    }//for i
    return output;
}//remove1




    public void handleFileUpload(FileUploadEvent event) throws ParseException {
        log.log(Level.INFO, "handleFileUpload()");
        try {

            this.setFileSize(Long.toString(event.getFile().getSize()));
            String  myFileName = event.getFile().getFileName();
            String nameForServerUpload=remove1(myFileName);
            System.out.println("1. " +remove1(myFileName));
            

            RequestContext requestContext = RequestContext.getCurrentInstance();
            Date date = new Date();
            DateFormat dateF = new SimpleDateFormat("MM-dd-yyyy-HH");
            String partName = dateF.format(date);
            requestContext.update(event.getComponent().getClientId());
            this.setNameFile(partName + nameForServerUpload);

            copyFile(this.getNameFile(), event.getFile().getInputstream());
            Integer idTreatment = this.getSelectedPlansTreatment().getId();
            System.out.println("tratamiento"+idTreatment);

            if(this.fileNameForUploadOutput.size()!=0){
                this.fileNameForUploadOutput.add(new FileNameForUploadOutput(idTreatment,nameFile,0));

            }else{
                this.fileNameForUploadOutput.add(new FileNameForUploadOutput(idTreatment,nameFile,0));
            }
            if(this.fileNameForUploadOutput.size()==5){
                this.setDisablesbtnSelectFile(true);
                RequestContext.getCurrentInstance().update("fileRegister");
//
            }


            System.out.println("com.venedental.controller.view.RegisterKeysAnalysisViewBean.handleFileUpload()"+this.fileNameForUploadOutput);





//            RequestContext.getCurrentInstance().update("form1:form2:sendButton");
        } catch (IOException e) {

            e.printStackTrace();
        }



    }


    public void copyFile(String fileName, InputStream in) {
        log.log(Level.INFO, "copyFile()");
        try {

            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(ConstansRegisterKeysAnalysis.PROPERTY_PATH)+"/"+fileName);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            String ruta = "copyFile() -> " + ConstansRegisterKeysAnalysis.PROPERTY_PATH + fileName;
            log.log(Level.INFO, ruta);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
      /**
     * Method for verify enabled button file
     *
     * 
     * @return
     */
    public boolean enableBtnFile(String path) {
        if(path!=""){
            return true;
        }

    else{
            return false;
        }
    }
    
      /**
     * Method for verify enabled label 
     *
     * 
     * @return
     */
    public boolean enableLabelFile(Integer file) {
        if(file==1){
            return false;
        }

    else{
            return true;
        }
    }


    /**
     * Methods for data requirement register
     *
     *
     */
    public void downloadRequirementByRegister(String filename, Integer idFile) {

        try {


            InputStream inputstream = new FileInputStream(ConstansRegisterKeysAnalysis.PROPERTY_PATH+filename);

            if (inputstream != null) {
                switch (idFile) {
                    case 1:
                        this.setFileRequirementD1(new DefaultStreamedContent(inputstream, "application/pdf",filename));
                        break;
                    case 2:
                        this.setFileRequirementD2(new DefaultStreamedContent(inputstream, "application/pdf",filename));
                        break;
                    case 3:
                        this.setFileRequirementD3(new DefaultStreamedContent(inputstream, "application/pdf",filename));
                        break;
                    case 4:
                        this.setFileRequirementD4(new DefaultStreamedContent(inputstream, "application/pdf",filename));
                        break;
                    case 5:
                        this.setFileRequirementD5(new DefaultStreamedContent(inputstream, "application/pdf",filename));
                        break;
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "Error!", "Ha ocurrido un error en descargar los recaudos"));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Error!", "Ha ocurrido un error en descargar los recaudos"));
        }

//        RequestContext.getCurrentInstance().update("form:messageReimbursement");

    }
    public void InsertFileNameRegister(Integer idTreatmentPiece, String name) throws SQLException {

        this.servicePlansTreatmentKeys.InsertFileSupportRegister(idTreatmentPiece, name);

    }

    public void consultFileNameRegister(Integer idTreatmentPiece) throws SQLException {

        this.setNameFilebd(this.servicePlansTreatmentKeys.ConsultnameFileRegister(idTreatmentPiece));

    }

    public String getTitleDisabledUndoKey() {
        return titleDisabledUndoKey;
    }

    public String getTitleEnabledUndoKey() {
        return titleEnabledUndoKey;
    }

    public String getCurrenNumberKeyTable() {
        return currenNumberKeyTable;
    }

    public void setCurrenNumberKeyTable(String currenNumberKeyTable) {
        this.currenNumberKeyTable = currenNumberKeyTable;
    }


    public double getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(double priceValue) {
        this.priceValue = priceValue;
    }

//    public void InsertFileSupportRegister(Integer idTreatmentKey, String nameFile) throws SQLException {
//        InsertFileSupportDAO insertFileSupportDAO = new InsertFileSupportDAO(ds);
//        InsertFileSupportInput input = new InsertFileSupportInput(idTreatmentKey,nameFile);
//        insertFileSupportDAO.execute(input);
//    }

}
