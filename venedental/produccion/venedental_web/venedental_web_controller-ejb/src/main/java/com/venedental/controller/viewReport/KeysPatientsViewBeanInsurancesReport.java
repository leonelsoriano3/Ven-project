package com.venedental.controller.viewReport;

import com.venedental.controller.BaseBean;
import com.venedental.controller.message.MessagesStandar;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.controller.view.*;
import com.venedental.facade.KeysPatientsFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.KeysPatients;
import com.venedental.model.TypeSpecialist;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.primefaces.context.RequestContext;
import services.utils.Transformar;




@ManagedBean(name = "keysPatientsViewBeanInsurancesReport")
@ViewScoped
public class KeysPatientsViewBeanInsurancesReport extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();

    @EJB
    private KeysPatientsFacadeLocal keysFacadeLocal;

    @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;

    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    @ManagedProperty(value = "#{insurancesViewBean}")
    private InsurancesViewBean insurancesViewBean;

    @ManagedProperty(value = "#{keysPatientsViewBean}")
    private KeysPatientsViewBean keysPatientsViewBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;

    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;

    @ManagedProperty(value = "#{plansPatientsViewBean}")
    private PlansPatientsViewBean plansPatientsViewBean;

    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesPlansTreatmentsKeyViewBean}")
    private PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean;

    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;

    @ManagedProperty(value = "#{messagesStandar}")
    private MessagesStandar messagesStandar;

    @ManagedProperty(value = "#{configViewBeanReport}")
    private ConfigViewBeanReport export;

    private String dueDate;

    private List<KeysPatients> listaKeys;

    private List<KeysPatients> listaKeysFiltrada;

    private List<KeysPatients> listHistoryKeys;

    private boolean visible;

    private KeysPatients keysPatientsNew;

    private KeysPatients keysPatientsEdit;

    private Map<String, Object> params = new HashMap<String, Object>();

    private JRBeanCollectionDataSource beanCollectionDataSource;

    private JasperPrint jasperPrint;

    private List<TypeSpecialist> listTypeSpecialist;

    private TypeSpecialist newtypeSpecialist;

    private Date startDate, endDate;

    private Integer insurancesId;

    private String  insurancesName;

    private String reportPath;

    private  HttpServletResponse httpServletResponse;

    private ServletOutputStream servletOutputStream;

    private JRXlsxExporter docxExporter;

    private String logoPath;

    public KeysPatientsViewBeanInsurancesReport() {
        this.visible = false;
    }
    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }

    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }

    public PropertiesPlansTreatmentsKeyViewBean getPropertiesPlansTreatmentsKeyViewBean() {
        return propertiesPlansTreatmentsKeyViewBean;
    }

    public void setPropertiesPlansTreatmentsKeyViewBean(PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean) {
        this.propertiesPlansTreatmentsKeyViewBean = propertiesPlansTreatmentsKeyViewBean;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public InsurancesViewBean getInsurancesViewBean() {
        return insurancesViewBean;
    }

    public void setInsurancesViewBean(InsurancesViewBean insurancesViewBean) {
        this.insurancesViewBean = insurancesViewBean;
    }

    public PlansPatientsViewBean getPlansPatientsViewBean() {
        return plansPatientsViewBean;
    }

    public void setPlansPatientsViewBean(PlansPatientsViewBean plansPatientsViewBean) {
        this.plansPatientsViewBean = plansPatientsViewBean;
    }

    public KeysPatientsViewBean getKeysPatientsViewBean() {
        return keysPatientsViewBean;
    }

    public void setKeysPatientsViewBean(KeysPatientsViewBean keysPatientsViewBean) {
        this.keysPatientsViewBean = keysPatientsViewBean;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public KeysPatientsFacadeLocal getKeysFacadeLocal() {
        return keysFacadeLocal;
    }

    public void setKeysFacadeLocal(KeysPatientsFacadeLocal keysFacadeLocal) {
        this.keysFacadeLocal = keysFacadeLocal;
    }

    public List<KeysPatients> getListaKeys() {
        if (this.listaKeys == null) {
            this.listaKeys = new ArrayList<>();
            this.getListaKeys().addAll(this.getListaKeys());
        }
        return listaKeys;

    }

    public void setListaKeys(List<KeysPatients> listaKeys) {
        this.listaKeys = listaKeys;
    }

    public List<KeysPatients> getListaKeysFiltrada() {
        if (this.listaKeysFiltrada == null) {
            this.listaKeysFiltrada = new ArrayList<>();
            this.getListaKeysFiltrada().addAll(this.getListaKeysFiltrada());
        }
        return listaKeysFiltrada;

    }

    public void setListaKeysFiltrada(List<KeysPatients> listaKeysFiltrada) {
        this.listaKeysFiltrada = listaKeysFiltrada;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public String getMyFormattedDateStart(Date fecha) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getInsurancesId() {
        return insurancesId;
    }

    public void setInsurancesId(Integer insurancesId) {
        this.insurancesId = insurancesId;
    }

    public String getInsurancesName() {
        return insurancesName;
    }

    public void setInsurancesName(String insurancesName) {
        this.insurancesName = insurancesName;
    }

    public MessagesStandar getMessagesStandar() {
        return messagesStandar;
    }

    public void setMessagesStandar(MessagesStandar messagesStandar) {
        this.messagesStandar = messagesStandar;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    @PostConstruct
    public void init(){
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
    }

    public void load() throws IOException, JRException {
        this.logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listKeys", new JRBeanCollectionDataSource(listaKeysFiltrada));
        this.params.put("heal_logo2", logoPath);
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(listaKeysFiltrada);
        this.reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/insurancesKeys/keys.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
    }

    public void filtrarPorRango() throws ParseException {

        if (startDate == null || endDate == null || insurancesViewBean.getNewInsurances() == null) {
            this.listaKeys = new ArrayList<>();
            this.listaKeysFiltrada = new ArrayList<>();
        }else{
            this.insurancesId= this.insurancesViewBean.getNewInsurances().getId();
            this.insurancesName = this.insurancesViewBean.getNewInsurances().getName();
            this.params.put("insurance", insurancesName);
            this.params.put("startDate", startDate);
            this.params.put("endDate", endDate);

            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            String fechaStr = formatter.format(endDate);
            Date date = formatter.parse(fechaStr);

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            Date fechaFinalTransformada = cal.getTime();

            String fechaInicialStr = this.getMyFormattedDateStart(startDate);
            String fechaFinal = this.getMyFormattedDateStart(endDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date fechaInicial = sdf2.parse(fechaInicialStr);
            if (insurancesName.equals("Todas las Aseguradoras")){
                if (fechaInicial != null && fechaFinal != null) {
                    this.listaKeys = new ArrayList<>();
                    this.listaKeysFiltrada = new ArrayList<>();
                    this.listaKeys.addAll(this.keysFacadeLocal.findByApplicationDate(fechaInicial, fechaFinalTransformada));
                    this.listaKeysFiltrada.addAll(this.listaKeys);
                }
            }else{
                if (fechaInicial != null && fechaFinal != null && insurancesId!= null ) {
                    this.listaKeys = new ArrayList<>();
                    this.listaKeysFiltrada = new ArrayList<>();
                    this.listaKeys.addAll(this.keysFacadeLocal.findByApplicationDateInsurances(fechaInicial, fechaFinalTransformada, insurancesId));
                    this.listaKeysFiltrada.addAll(this.listaKeys);
                }
            }
        }

    }

    public void resetFechas() {
        this.setStartDate(null);
        this.setEndDate(null);
        this.listaKeys = new ArrayList<>();
        this.listaKeysFiltrada = new ArrayList<>();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:listaKeys");
    }


    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }



    public void showReportXlsx(ActionEvent actionEvent) throws JRException, IOException,Exception{
        if(!listaKeysFiltrada.isEmpty()){
            load();
            this.export.xlsx(jasperPrint,insurancesName,Transformar.getThisDate());
        }else{
            messagesStandar.addInfo("Debe elegir los Criterios de busqueda");
        }
    }
}
