package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.facade.SpecialityFacadeLocal;
import com.venedental.model.Speciality;
import com.venedental.model.TypeSpecialist;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "specialityViewBean")
@ViewScoped
public class SpecialityViewBean extends BaseBean {

    @EJB
    private SpecialityFacadeLocal specialityFacadeLocal;

    private static final long serialVersionUID = -5547104134772667835L;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    public SpecialityViewBean() {
        this.newSpeciality = new Speciality();
        this.addSpeciality = new Speciality();
    }

    private Speciality selectedSpeciality;

    private List<Speciality> listaSpeciality;

    private List<Speciality> listaCompletaSpeciality;

    private List<Speciality> editListaSpecialists;

    private List<Speciality> newListaSpecialists;

    private Speciality newSpeciality;

    private Speciality addSpeciality;

    private Speciality editSpeciality;

    public List<Speciality> getListaCompletaSpeciality() {
        if (this.listaCompletaSpeciality == null) {
            this.listaCompletaSpeciality = this.specialityFacadeLocal.findAll();
        }
        return listaCompletaSpeciality;
    }

    public void setListaCompletaSpeciality(List<Speciality> listaCompletaSpeciality) {
        this.listaCompletaSpeciality = listaCompletaSpeciality;
    }

    public Speciality getSelectedSpeciality() {
        return selectedSpeciality;
    }

    public void setSelectedSpeciality(Speciality selectedSpeciality) {
        this.selectedSpeciality = selectedSpeciality;
    }

    public List<Speciality> getListaSpeciality() {
        if (this.listaSpeciality == null) {
            this.listaSpeciality = new ArrayList<>();
        }
        return listaSpeciality;
    }

    public void setListaSpeciality(List<Speciality> listaSpeciality) {
        this.listaSpeciality = listaSpeciality;
    }

    public Speciality getNewSpeciality() {
        return newSpeciality;
    }

    public void setNewSpeciality(Speciality newSpeciality) {
        this.newSpeciality = newSpeciality;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public Speciality getAddSpeciality() {
        return addSpeciality;
    }

    public void setAddSpeciality(Speciality addSpeciality) {
        this.addSpeciality = addSpeciality;
    }

    public Speciality getEditSpeciality() {
        return editSpeciality;
    }

    public void setEditSpeciality(Speciality editSpeciality) {
        this.editSpeciality = editSpeciality;
    }

    public void filtrarSpeciality() {
        this.getListaSpeciality().clear();

        if (this.getTypeSpecialistViewBean().getEditTypeSpecialist() != null) {
            for (Speciality s : this.getListaCompletaSpeciality()) {
                if (Objects.equals(s.getTypeSpecialist().getId(), this.getTypeSpecialistViewBean().getEditTypeSpecialist().getId())) {
                    this.getListaSpeciality().add(s);
                }
            }

        }
    }

    public void filtrarSpeciality(TypeSpecialist tipoEspecialista) {
        this.getListaSpeciality().clear();
        this.getEditListaSpecialists().clear();
        if (tipoEspecialista != null) {
            for (Speciality s : this.getListaCompletaSpeciality()) {
                if (Objects.equals(s.getTypeSpecialist().getId(), tipoEspecialista.getId())) {
                    this.getListaSpeciality().add(s);
                }
            }

        } 
    }

    public List<Speciality> getEditListaSpecialists() {
        if (this.editListaSpecialists == null) {
            this.editListaSpecialists = new ArrayList<>();
        }
        return editListaSpecialists;
    }

    public void setEditListaSpecialists(List<Speciality> editListaSpecialists) {
        this.editListaSpecialists = editListaSpecialists;
    }

    public List<Speciality> getNewListaSpecialists() {
        if (this.newListaSpecialists == null) {
            this.newListaSpecialists = new ArrayList<>();
        }
        return newListaSpecialists;
    }

    public void setNewListaSpecialists(List<Speciality> newListaSpecialists) {
        this.newListaSpecialists = newListaSpecialists;
    }

    public String join(List<Speciality> list) {
        String joinedSpeciality = "";
        if (list != null) {
            for (Speciality item : list) {
                joinedSpeciality = joinedSpeciality + " " + item.getName();
            }
        }
        return joinedSpeciality;
    }

}
