/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;

import com.venedental.facade.CitiesFacadeLocal;
import com.venedental.model.Cities;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "citiesApplicationBean")
@ApplicationScoped
public class CitiesApplicationBean  {

    @EJB
    private CitiesFacadeLocal citiesFacadeLocal;

    public CitiesApplicationBean() {
    }

    @PostConstruct
    public void init() {
        this.getListCities().addAll(this.citiesFacadeLocal.findAll());
        
    }

    private List<Cities> listCities;

    public List<Cities> getListCities() {
        if (this.listCities == null) {
            this.listCities = new ArrayList<>();
        }

        return listCities;
    }

    public void setListCities(List<Cities> listCities) {
        this.listCities = listCities;
    }


}
