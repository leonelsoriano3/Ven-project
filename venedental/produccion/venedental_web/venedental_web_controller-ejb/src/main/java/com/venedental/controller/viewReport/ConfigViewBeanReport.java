/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.controller.viewReport;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.mail.MessagingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ARKDEV16
 */

@ManagedBean(name = "configViewBeanReport")
@ViewScoped
public class ConfigViewBeanReport{

    private  HttpServletResponse httpServletResponse;

    private ServletOutputStream servletOutputStream;

    private JRXlsxExporter docxExporter;

    private String fileName;


    public void pdf(JasperPrint jasperPrint,String nameReport, String date) throws JRException, IOException, MessagingException{
        this.fileName=StringUtils.replace(StringUtils.stripAccents(nameReport)," ", "_");
        this.httpServletResponse=(HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition", "attachment; filename="+fileName+date+".pdf");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        FacesContext.getCurrentInstance().responseComplete();
    }


    public void xlsx(JasperPrint jasperPrint,String nameReport, String date) throws IOException, JRException, MessagingException {
        this.fileName = StringUtils.replace(StringUtils.stripAccents(nameReport)," ", "_");
        this.httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition","attachment; filename="+fileName+date+".xlsx");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        this.docxExporter=new JRXlsxExporter();
        this.docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        this.docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        this.docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void xlsxBaremo(JasperPrint jasperPrint,String nameReport, String date) throws IOException, JRException, MessagingException {
        this.fileName = StringUtils.replace(StringUtils.stripAccents(nameReport)," ", "_");
        this.httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition","attachment; filename="+date+fileName+".xlsx");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        this.docxExporter=new JRXlsxExporter();
        this.docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        this.docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        this.docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void xlsxReimbursement(JasperPrint jasperPrint,String nameReport, String date) throws IOException, JRException, MessagingException {
        this.fileName = StringUtils.replace(StringUtils.stripAccents(nameReport)," ", "");
        this.httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition","attachment; filename="+date+"ReporteReembososRechazados"+fileName+".xlsx");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        this.docxExporter=new JRXlsxExporter();
        this.docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        this.docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        this.docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void xlsxReimbursementSuscriber(JasperPrint jasperPrint,String nameReport, String date) throws IOException, JRException, MessagingException {
        this.fileName = StringUtils.replace(StringUtils.stripAccents(nameReport)," ", "");
        this.httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition","attachment; filename="+date+"ReporteReembososAbonados"+fileName+".xlsx");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        this.docxExporter=new JRXlsxExporter();
        this.docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        this.docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        this.docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void xlsxExportRowsReimbursement(JasperPrint jasperPrint,String nameReport) throws IOException, JRException{
        this.fileName = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", " ");
        this.httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.httpServletResponse.addHeader("Content-disposition","attachment; filename="+fileName+".xlsx");
        this.servletOutputStream=httpServletResponse.getOutputStream();
        this.docxExporter=new JRXlsxExporter();
        this.docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        this.docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        this.docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }
    


}
