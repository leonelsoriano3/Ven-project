/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;


import com.venedental.controller.view.TreatmentsViewBean;
import com.venedental.model.Treatments;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import services.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "treatmentConverter", forClass = Treatments.class)
public class TreatmentConverter implements Converter {

    private static final Logger log = CustomLogger.getGeneralLogger(TreatmentConverter.class.getName());
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        TreatmentsViewBean treatmentsViewBean = (TreatmentsViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "treatmentsViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Treatments treatments : treatmentsViewBean.getListaTreatments()) {
                    if (treatments.getId() == numero) {
                        return treatments;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Tratamiento Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Treatments) value).getId());
        }

    }

}
