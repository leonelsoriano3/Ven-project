/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.ListaLinks;
import model.ListaLinksText;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "indexViewBean")
@ViewScoped
public class IndexViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    private List<ListaLinks> listaImagenes;
    private List<ListaLinks> listaModulos;
    private List<ListaLinksText> listaAliados;
    private MapModel modelMap;
    private String option;

    public IndexViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaImagenes(new ArrayList<ListaLinks>());
        this.listaImagenes.add(new ListaLinks("Grupo Veneden", "Juntos para \"0\" problemas bucodentales",  "./img/slider-main2.jpg"));
        this.listaImagenes.add(new ListaLinks("Contáctenos", "0-501-VENEDEN info@venedental.com.ve",  "./img/slider-main4.jpg"));
        this.listaImagenes.add(new ListaLinks("Calidad para tus clientes", "Ubicados en todo el territorio nacional", "./img/slider-main1.jpg"));
        this.listaImagenes.add(new ListaLinks("Únete a nuestro equipo", "Ingresa aquí para registrarte", "./img/slider-main3.jpg"));

        this.setListaModulos(new ArrayList<ListaLinks>());
        this.listaModulos.add(new ListaLinks("Servicios odontológicos en clínicas asociadas con presencia en todo el territorio nacional, dirigido a la prevención y mantenimiento de la salud bucal", "/views/web/venedental.xhtml", "./img/venedental.png"));
        this.listaModulos.add(new ListaLinks("Servicios oftalmológicos en clínicas asociadas con presencia en todo el territorio nacional, dirigido a la corrección del campo visual", "/views/web/venedevisual.xhtml", "./img/venedevisual.png"));
        this.listaModulos.add(new ListaLinks("Servicios profesionales en todo el territorio nacional especializados en materia dermatológica dirigido a la atención de patología presentes y prevención a futuro", "/views/web/venedermo.xhtml", "./img/venedermo.png"));

        this.setListaAliados(new ArrayList<ListaLinksText>());
        this.listaAliados.add(new ListaLinksText("Universitas", "Compañía de Seguros", "http://www.segurosuniversitas.com/","./img/home_blog_img2.jpg"));
        this.listaAliados.add(new ListaLinksText("Arkiteck", "Soluciones Tecnológicas", "http://www.arkiteck.com/",  "./img/home_blog_img4.jpg"));
        this.listaAliados.add(new ListaLinksText("Consultorios Odontológicos", "Proveedores de Salud", "http://www.venedental.com/directorio.xhtml","./img/home_blog_img3.jpg"));
        this.listaAliados.add(new ListaLinksText("Ópticas", "Proveedores Oftalmológicos", "http://www.venedental.com/directorio.xhtml", "./img/home_blog_img5.jpg"));

        this.setModelMap(new DefaultMapModel());
        

        modelMap.addOverlay(new Marker(new LatLng(10.492259, -66.879068), ""));

    }

    public List<ListaLinks> getListaImagenes() {
        return listaImagenes;
    }

    public void setListaImagenes(List<ListaLinks> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    public List<ListaLinks> getListaModulos() {
        return listaModulos;
    }

    public void setListaModulos(List<ListaLinks> listaModulos) {
        this.listaModulos = listaModulos;
    }

    public List<ListaLinksText> getListaAliados() {
        return listaAliados;
    }

    public void setListaAliados(List<ListaLinksText> listaAliados) {
        this.listaAliados = listaAliados;
    }

    public MapModel getModelMap() {
        return modelMap;
    }

    public void setModelMap(MapModel modelMap) {
        this.modelMap = modelMap;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

}
