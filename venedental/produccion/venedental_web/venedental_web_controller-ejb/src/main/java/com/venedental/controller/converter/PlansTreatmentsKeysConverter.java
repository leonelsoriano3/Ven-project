/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.PlansTreatmentsKeysViewBean;
import com.venedental.controller.view.PlansTreatmentsViewBean;
import com.venedental.controller.view.RegisterKeysAnalysisViewBean;
import com.venedental.model.PlansTreatments;
import com.venedental.model.PlansTreatmentsKeys;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "plansTreatmentsKeysConverter", forClass = PlansTreatmentsKeys.class)
public class PlansTreatmentsKeysConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean = (PlansTreatmentsKeysViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "plansTreatmentsKeysViewBean");
        RegisterKeysAnalysisViewBean registerKeysAnalysisViewBean = (RegisterKeysAnalysisViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "registerKeysAnalysisViewBean");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                
                for (PlansTreatmentsKeys plansTreatmentsKeys : plansTreatmentsKeysViewBean.getListPlansTreatmentsKeysSeleccionados()) {
                    if (plansTreatmentsKeys.getId() == numero) {
                        return plansTreatmentsKeys;
                    }
                }
                
                for (PlansTreatmentsKeys plansTreatmentsKeys : registerKeysAnalysisViewBean.getPlansTreatmentsKeys()) {
                    if (plansTreatmentsKeys.getId() == numero) {
                        return plansTreatmentsKeys;
                    }
                }               
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Planes Tratamientos por clave Invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((PlansTreatmentsKeys) value).getId());
        }

    }

}
