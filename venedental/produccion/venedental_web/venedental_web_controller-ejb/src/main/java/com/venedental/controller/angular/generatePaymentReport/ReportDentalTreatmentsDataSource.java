/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.angular.generatePaymentReport;

import com.venedental.dto.reportDentalTreatments.TreatmentOutput;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author AKDESK09
 */
public class ReportDentalTreatmentsDataSource implements JRDataSource{

    private int index = -1;
    private final List<TreatmentOutput> listRep;

    public ReportDentalTreatmentsDataSource(List<TreatmentOutput> listRep) {
        this.listRep = listRep;
    }
    
    @Override
    public boolean next() throws JRException {
        System.out.println("\t\t" + (index + 1) + "\t");
        return ++index < listRep.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        TreatmentOutput to = listRep.get(index);
        System.out.print("\t\t" +jrf.getName());
        switch (jrf.getName()){
            case "dateKey": 
                return stringValido(to.getDateKey());
            case "namePatient":
                return stringValido(to.getNamePatient());
            case "numberKey":
                return stringValido(to.getNumberKey());
            case "numTooth":
                return stringValido(to.getNumTooth());
            case "nameTreatment":
                return stringValido(to.getNameTreatment());
        }
        return null;
    }
    
    private String stringValido(String str){
        if (str != null){
            return str.trim();
        }
        return "";
    }
}
