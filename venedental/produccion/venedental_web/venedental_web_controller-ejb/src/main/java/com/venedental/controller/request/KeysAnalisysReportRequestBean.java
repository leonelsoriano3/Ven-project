package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.KeysAnalisysReportViewBean;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialOuput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.Specialists;
import com.venedental.services.ServicePaymentReport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.context.RequestContext;

@ManagedBean(name="keysAnalisysReportRequestBean")
@RequestScoped
public class KeysAnalisysReportRequestBean extends BaseBean implements Serializable{

    @EJB
    private ServicePaymentReport servicePaymentReport;
    /* Managed Properties */
    
    @ManagedProperty(value = "#{keysAnalisysReportViewBean}")
    private KeysAnalisysReportViewBean keysAnalisysReportViewBean;
    
    /* Attributes */
    
    /* Cosntructs */
    public KeysAnalisysReportRequestBean() {
    }
    
    @PostConstruct
    public void init(){
        
    }
    
    /* Getter and Setter */
    
    /**
     *  Obtener todo el contenido del managed bean de alcance vista para el 
     *  componente de reportes de pagos analisis de claves.
     * 
     * @return 
     */
    public KeysAnalisysReportViewBean getKeysAnalisysReportViewBean() {
        return keysAnalisysReportViewBean;
    }
    
    /**
     * Almacenamiento de data a traves del managed bean de alcance vista 
     * para el componente de reportes de pagos analisis de claves.
     * 
     * @param keysAnalisysReportViewBean 
     */
    public void setKeysAnalisysReportViewBean(KeysAnalisysReportViewBean keysAnalisysReportViewBean) {
        this.keysAnalisysReportViewBean = keysAnalisysReportViewBean;
    }
    
    /* Methods */
    public void resetCleanData(){
    
        this.getKeysAnalisysReportViewBean().init();
    }
    
    /**
     * metodo para listar los datos segun el criterio de busqueda.
     *  
     * 
     */
    public void searchDataByCriterial(){
       
       this.getKeysAnalisysReportViewBean().getSpecialist();
       this.getKeysAnalisysReportViewBean().getFiltersData();
       
        try {

            // criterial i
            if (this.getKeysAnalisysReportViewBean().getStartDate() != null
                    && this.getKeysAnalisysReportViewBean().getEndDate() != null
                    && this.getKeysAnalisysReportViewBean().getSpecialist() != null
                    && this.getKeysAnalisysReportViewBean().getFiltersData()[0].isEmpty()) {

                this.getKeysAnalisysReportViewBean().setPaymentReportkeysAnalisysByCriterialOuputList(
                   this.servicePaymentReport.consultKeysAnalisysPaymentReportByCriterial(
                      this.getKeysAnalisysReportViewBean().getSpecialist().getId(),
                      this.getKeysAnalisysReportViewBean().getStartDate(),
                      this.getKeysAnalisysReportViewBean().getEndDate(),
                      this.getKeysAnalisysReportViewBean().getFiltersData()[0],
                      EstatusPlansTreatmentsKeys.ABONADO.getValor())
                );

                // Visibility
                this.getKeysAnalisysReportViewBean().setVisibilityCEDRIF(false);
                this.getKeysAnalisysReportViewBean().setVisibilitySpecialistName(false);
                this.getKeysAnalisysReportViewBean().setVisibilityInvoice(true);

                boolean verifyTable = this.getKeysAnalisysReportViewBean().verifyDataTable();

                if (verifyTable) {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(false);
                } else {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(true);
                }
            }
            
            // criterial ii
            if (this.getKeysAnalisysReportViewBean().getStartDate() != null
                    && this.getKeysAnalisysReportViewBean().getEndDate() != null
                    && this.getKeysAnalisysReportViewBean().getSpecialist() == null
                    && this.getKeysAnalisysReportViewBean().getFiltersData()[0].isEmpty()) {

                this.getKeysAnalisysReportViewBean().setPaymentReportkeysAnalisysByCriterialOuputList(
                   this.servicePaymentReport.consultKeysAnalisysPaymentReportByCriterial(
                    0, 
                    this.getKeysAnalisysReportViewBean().getStartDate(),
                    this.getKeysAnalisysReportViewBean().getEndDate(),
                    this.getKeysAnalisysReportViewBean().getFiltersData()[0],
                    EstatusPlansTreatmentsKeys.ABONADO.getValor()
                   )
                );

               
                // Visibility
                this.getKeysAnalisysReportViewBean().setVisibilityCEDRIF(true);
                this.getKeysAnalisysReportViewBean().setVisibilitySpecialistName(true);
                this.getKeysAnalisysReportViewBean().setVisibilityInvoice(true);

                boolean verifyTable = this.getKeysAnalisysReportViewBean().verifyDataTable();
                if (verifyTable) {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(false);
                } else {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(true);
                }
            }
            
            
            // criterial iii
            if (this.getKeysAnalisysReportViewBean().getStartDate()== null
                    && this.getKeysAnalisysReportViewBean().getEndDate()== null
                    && this.getKeysAnalisysReportViewBean().getSpecialist().getId() == null
                    && !this.getKeysAnalisysReportViewBean().getFiltersData()[0].isEmpty()) {
            
                this.getKeysAnalisysReportViewBean().setPaymentReportkeysAnalisysByCriterialOuputList(                    
                      this.servicePaymentReport.consultKeysAnalisysPaymentReportByCriterial(
                      0,
                      null,
                      null,
                      this.getKeysAnalisysReportViewBean().getFiltersData()[0],
                      EstatusPlansTreatmentsKeys.ABONADO.getValor()
                      )
                );

             
                // Visibility
                this.getKeysAnalisysReportViewBean().setVisibilityCEDRIF(true);
                this.getKeysAnalisysReportViewBean().setVisibilitySpecialistName(true);
                this.getKeysAnalisysReportViewBean().setVisibilityInvoice(false);
                
                boolean verifyTable = this.getKeysAnalisysReportViewBean().verifyDataTable();

                if (verifyTable) {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(false);
                } else {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(true);
                }

            } 
            
            // criterial iv.
            if (this.getKeysAnalisysReportViewBean().getStartDate()== null
                    && this.getKeysAnalisysReportViewBean().getEndDate()== null
                    && this.getKeysAnalisysReportViewBean().getSpecialist().getId() != null
                    && this.getKeysAnalisysReportViewBean().getFiltersData()[0].isEmpty()) {
            
                this.getKeysAnalisysReportViewBean().setPaymentReportkeysAnalisysByCriterialOuputList(                    
                      this.servicePaymentReport.consultKeysAnalisysPaymentReportByCriterial(
                      this.getKeysAnalisysReportViewBean().getSpecialist().getId(),
                      null,
                      null,
                      this.getKeysAnalisysReportViewBean().getFiltersData()[0],
                      EstatusPlansTreatmentsKeys.ABONADO.getValor()
                      )
                );

             
                // Visibility
                this.getKeysAnalisysReportViewBean().setVisibilityCEDRIF(false);
                this.getKeysAnalisysReportViewBean().setVisibilitySpecialistName(false);
                this.getKeysAnalisysReportViewBean().setVisibilityInvoice(true);
                
                boolean verifyTable = this.getKeysAnalisysReportViewBean().verifyDataTable();

                if (verifyTable) {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(false);
                } else {
                    this.getKeysAnalisysReportViewBean().setActiveBtnExportData(true);
                }

            } 


            RequestContext.getCurrentInstance().update("form:growInvoicesReportMessage");
            RequestContext.getCurrentInstance().update("form:tableReportKeysAnalisys");
            RequestContext.getCurrentInstance().update("form:columnVisibilityDoctorClinic");
            RequestContext.getCurrentInstance().update("form:columnVisibilityRIFCED");
            RequestContext.getCurrentInstance().update("form:columnVisibilityInvoicesNumber");
            RequestContext.getCurrentInstance().update("form:btnExportData");

        } catch (Exception e) {
            System.out.println(e);

        }

    }

}
