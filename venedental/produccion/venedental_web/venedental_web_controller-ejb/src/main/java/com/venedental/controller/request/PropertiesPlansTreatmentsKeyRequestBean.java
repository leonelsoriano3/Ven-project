package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.PlansTreatmentsKeysViewBean;
import com.venedental.controller.view.PlansTreatmentsViewBean;
import com.venedental.facade.PropertiesPlansTreatmentsKeyFacadeLocal;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.PropertiesTreatments;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

@ManagedBean(name = "propertiesPlansTreatmentsKeyRequestBean")
@RequestScoped
public class PropertiesPlansTreatmentsKeyRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(PropertiesPlansTreatmentsKeyRequestBean.class.getName());

    @EJB
    private PropertiesPlansTreatmentsKeyFacadeLocal propertiesTreatmentsFacadeLocal;

    private List<PropertiesPlansTreatmentsKey> newListPropertiesPlansTreatmentsKey;

    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeySeleccionados;
    
    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyRemovidos;
    
     private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFilter;

    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFiltrada;
    
    private List<PropertiesTreatments> listPropertiesPlansTreatmentsKeyEdit;

    private List<String> listSelectedOptions;

    private String selectedOptions;

    private boolean visibleList;

    private boolean visibleProperties;
    

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
   
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;

    public boolean isVisibleProperties() {
        return visibleProperties;
    }

    public void setVisibleProperties(boolean visibleProperties) {
        this.visibleProperties = visibleProperties;
    }

    public String getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(String selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public List<String> getListSelectedOptions() {

        return listSelectedOptions;
    }

    public void setListSelectedOptions(List<String> listSelectedOptions) {
        this.listSelectedOptions = listSelectedOptions;
    }

    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeyFiltrada() {
        return listPropertiesPlansTreatmentsKeyFiltrada;
    }

    public void setListPropertiesPlansTreatmentsKeyFiltrada(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFiltrada) {
        this.listPropertiesPlansTreatmentsKeyFiltrada = listPropertiesPlansTreatmentsKeyFiltrada;
    }

    public void focusGrowl() {
        RequestContext.getCurrentInstance().scrollTo("growl");
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public List<PropertiesPlansTreatmentsKey> getNewListPropertiesPlansTreatmentsKey() {
        return newListPropertiesPlansTreatmentsKey;
    }

    public void setNewListPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> newListPropertiesPlansTreatmentsKey) {
        this.newListPropertiesPlansTreatmentsKey = newListPropertiesPlansTreatmentsKey;
    }

    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeyFilter() {
        if (this.listPropertiesPlansTreatmentsKeyFilter == null) {
            this.listPropertiesPlansTreatmentsKeyFilter = new ArrayList<>();
        }
        return listPropertiesPlansTreatmentsKeyFilter;
    }

    public void setListPropertiesPlansTreatmentsKeyFilter(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFilter) {
        this.listPropertiesPlansTreatmentsKeyFilter = listPropertiesPlansTreatmentsKeyFilter;
    }

    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeyRemovidos() {
        if (this.listPropertiesPlansTreatmentsKeyRemovidos == null) {
            this.listPropertiesPlansTreatmentsKeyRemovidos = new ArrayList<>();
        }
       
        return listPropertiesPlansTreatmentsKeyRemovidos;
    }

    public void setListPropertiesPlansTreatmentsKeyRemovidos(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyRemovidos) {
        this.listPropertiesPlansTreatmentsKeyRemovidos = listPropertiesPlansTreatmentsKeyRemovidos;
    }


    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeySeleccionados() {
        if (this.listPropertiesPlansTreatmentsKeySeleccionados == null) {
            this.listPropertiesPlansTreatmentsKeySeleccionados = new ArrayList<>();
        }
        return listPropertiesPlansTreatmentsKeySeleccionados;
    }

    public void setListPropertiesPlansTreatmentsKeySeleccionados(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeySeleccionados) {
        this.listPropertiesPlansTreatmentsKeySeleccionados = listPropertiesPlansTreatmentsKeySeleccionados;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public List<PropertiesTreatments> getListPropertiesPlansTreatmentsKeyEdit() {
         if (this.listPropertiesPlansTreatmentsKeyEdit == null) {
            this.listPropertiesPlansTreatmentsKeyEdit = new ArrayList<>();
        }
        return listPropertiesPlansTreatmentsKeyEdit;
    }

    public void setListPropertiesPlansTreatmentsKeyEdit(List<PropertiesTreatments> listPropertiesPlansTreatmentsKeyEdit) {
        this.listPropertiesPlansTreatmentsKeyEdit = listPropertiesPlansTreatmentsKeyEdit;
    }
    
    
    
    
    
    
    
    
    

    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }

   
}
