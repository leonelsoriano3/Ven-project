/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.LevelMenu;
import com.venedental.facade.MenuFacadeLocal;
import com.venedental.facade.MenuRolFacadeLocal;
import com.venedental.facade.RolUsersFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.model.security.Menu;
import com.venedental.model.security.Users;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Edgar Anzola
 */
@ManagedBean(name = "menuModelViewBean")
@ViewScoped
public class MenuModelViewBean extends BaseBean {

    private MenuModel model;

    List<Menu> listMenu;

    List<Menu> listSon;

    List<Menu> listMenuFather;

    List<Menu> listSonSecondLevel;

    @EJB
    private MenuFacadeLocal menuFacadeLocal;

    @EJB
    private UsersFacadeLocal usersFacadeLocal;

    @EJB
    private RolUsersFacadeLocal rolUsersFacadeLocal;

    @EJB
    private MenuRolFacadeLocal menuRolFacadeLocal;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
        //this.obtainMenu();
        this.builderMenu() ;
        this.getModel();
        this.getSecurityViewBean().encriptar();

    }

    public MenuModel getModel() {
        return model;
    }

    public List<Menu> getListSon() {
        return listSon;
    }

    public void setListSon(List<Menu> listSon) {
        this.listSon = listSon;
    }

    public List<Menu> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<Menu> listMenu) {
        this.listMenu = listMenu;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public List<Menu> getListMenuFather() {
        
        return listMenuFather;
    }

    public void setListMenuFather(List<Menu> listMenuFather) {
        this.listMenuFather = listMenuFather;
    }

    public MenuFacadeLocal getMenuFacadeLocal() {
        return menuFacadeLocal;
    }

    public void setMenuFacadeLocal(MenuFacadeLocal menuFacadeLocal) {
        this.menuFacadeLocal = menuFacadeLocal;
    }

    public UsersFacadeLocal getUsersFacadeLocal() {
        return usersFacadeLocal;
    }

    public void setUsersFacadeLocal(UsersFacadeLocal usersFacadeLocal) {
        this.usersFacadeLocal = usersFacadeLocal;
    }

    public RolUsersFacadeLocal getRolUsersFacadeLocal() {
        return rolUsersFacadeLocal;
    }

    public void setRolUsersFacadeLocal(RolUsersFacadeLocal rolUsersFacadeLocal) {
        this.rolUsersFacadeLocal = rolUsersFacadeLocal;
    }

    public MenuRolFacadeLocal getMenuRolFacadeLocal() {
        return menuRolFacadeLocal;
    }

    public void setMenuRolFacadeLocal(MenuRolFacadeLocal menuRolFacadeLocal) {
        this.menuRolFacadeLocal = menuRolFacadeLocal;
    }

    public List<Menu> getListSonSecondLevel() {
        return listSonSecondLevel;
    }

    public void setListSonSecondLevel(List<Menu> listSonSecondLevel) {
        this.listSonSecondLevel = listSonSecondLevel;
    }
    
     /**
     * method that builds the menu
     * 
     */

    public void builderMenu() {

        DefaultMenuItem item;
        DefaultMenuItem itemStyle;
        DefaultSubMenu submenu;
        DefaultSubMenu submenuFather;
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());

        this.setListMenuFather(this.menuFacadeLocal.
                findSonByUserAndFather(user.getId(),LevelMenu.Father.getValor()));
        if (!this.getListMenuFather().isEmpty()) {
            
            for (Menu menuUser : this.getListMenuFather()) {

                if (menuUser.getFather() == 0) {

                    this.setListSon(this.menuFacadeLocal.
                            findSonByUserAndFather(user.getId(), menuUser.
                                    getId()
                                   ));
                    
                    if (!this.getListSon().isEmpty()) {
                        
                         submenu = new DefaultSubMenu(menuUser.getName());
                         itemStyle = new DefaultMenuItem("");
                         itemStyle.setStyleClass("triangulo");
                         submenu.addElement(itemStyle);

                        for (Menu menuSon : this.getListSon()) {

                            this.setListSonSecondLevel(this.menuFacadeLocal.
                                    findSonByUserAndFather(user.getId(), menuSon.
                                            getId()
                                            ));

                            if (this.getListSonSecondLevel().isEmpty()) {
                                item = new DefaultMenuItem(menuSon.getName());
                                /**
                                 * Validar url cuando el usuario loggeado es un
                                 * especilista
                                 */
                                if (menuSon.getId() == 2) {
                                    if (this.getSecurityViewBean().obtainRolSpecialist()
                                            ) {
                                        item.setUrl(menuSon.getUrl());
                                    } else {
                                        item.setUrl(menuSon.getUrl());
                                    }

                                } else {
                                    item.setUrl(menuSon.getUrl());
                                }

                                submenu.addElement(item);

                            } else {
                                
                                submenuFather = new DefaultSubMenu(menuSon.getName());
                                for(Menu menuSecondLevel:this.getListSonSecondLevel()){
                                item = new DefaultMenuItem(menuSecondLevel.getName());
                                item.setUrl(menuSecondLevel.getUrl());
                                submenuFather.addElement(item);
                                
                                }
                                submenu.addElement(submenuFather);
                            }
                         
                        }
                    model.addElement(submenu);
                    }
                    else{
                    
                        item = new DefaultMenuItem(menuUser.getName());
                        item.setUrl(menuUser.getUrl());
                        model.addElement(item);
                    
                    }

                }
            
            }

        }
    }

    public void obtainMenu() {

        int i = 0, j = 0, k = 0, count;
        DefaultMenuItem item;
        DefaultMenuItem itemStyle;
        DefaultSubMenu submenu;
        DefaultSubMenu submenuFather;

        listMenu = new ArrayList<>();
        listMenuFather = new ArrayList<>();
        listMenu = this.getSecurityViewBean().obtainMenuByRol();

        if (!listMenu.isEmpty()) {

            while (i < listMenu.size()) {

                j = 0;
                k = 0;
                Menu menu = listMenu.get(i);
                count = Integer.valueOf(this.menuFacadeLocal.countByFather(menu.getId()));

                if (count == 0 && menu.getFather() == 0) {

                    item = new DefaultMenuItem(menu.getName());
                    item.setUrl(menu.getUrl());
                    model.addElement(item);

                    this.listMenu.remove(menu);

                    if (i != 0) {
                        i--;
                    } else {
                        i = 0;
                    }

                } else {

                    if (count != 0 && menu.getFather() == 0) {

                        submenu = new DefaultSubMenu(menu.getName());
                        itemStyle = new DefaultMenuItem("");
                        itemStyle.setStyleClass("triangulo");
                        submenu.addElement(itemStyle);

                        while (j < listMenu.size()) {

                            Menu menuAux = listMenu.get(j);

                            if (Objects.equals(menu.getId(), menuAux.getFather())) {

                                count = Integer.valueOf(this.menuFacadeLocal.countByFather(menuAux.getId()));

                                //Hijos de los hijos
                                if (count == 0) {

                                    item = new DefaultMenuItem(menuAux.getName());

                                    /**
                                     * Validar url cuando el usuario loggeado es
                                     * un especilista
                                     */
                                    if (menuAux.getId() == 2) {
                                        if (this.getSecurityViewBean().obtainRolSpecialist()) {
                                            item.setUrl("/views/application/keysSpecialist.xhtml");
                                        } else {
                                            item.setUrl(menuAux.getUrl());
                                        }

                                    } else {
                                        item.setUrl(menuAux.getUrl());
                                    }

                                    submenu.addElement(item);
                                    this.listMenu.remove(menuAux);

                                    if (j != 0) {
                                        j--;
                                    } else {
                                        j = 0;
                                    }

                                } else {

                                    submenuFather = new DefaultSubMenu(menuAux.getName());

                                    while (k < listMenu.size()) {

                                        Menu menuAuxFather = listMenu.get(k);

                                        if (Objects.equals(menuAux.getId(), menuAuxFather.getFather())) {

                                            item = new DefaultMenuItem(menuAuxFather.getName());
                                            item.setUrl(menuAuxFather.getUrl());
                                            submenuFather.addElement(item);
                                            this.listMenu.remove(menuAuxFather);

                                            if (k != 0) {
                                                k--;
                                            } else {
                                                k = 0;
                                            }

                                        } else {
                                            k++;
                                        }

                                    }

                                    submenu.addElement(submenuFather);
                                    this.listMenu.remove(menuAux);

                                    if (j != 0) {
                                        j--;
                                    } else {
                                        j = 0;
                                    }

                                }

                            } else {
                                j++;
                            }
                        }

                        model.addElement(submenu);
                        this.listMenu.remove(menu);

                        if (i != 0) {
                            i--;
                        } else {
                            i = 0;
                        }

                    } else {
                        i++;
                    }

                }

            }

        }
    }
    
     /**
     * menu ordering method by id
     * @param menuList
     * @return  List<Menu> 
     */
    public List<Menu> orderMenu(List<Menu> menuList) {
        
        if(!menuList.isEmpty()){
        Collections.sort(menuList, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                Menu menu1 = (Menu) o1;
                Menu menu2 = (Menu) o2;
                return menu1.getId().compareTo(menu2.getId());

            }
         });
        }

        return menuList;
         
        

    }

}
