package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.application.PropertiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.facade.PropertiesFacadeLocal;
import com.venedental.model.Properties;
import com.venedental.model.PropertiesTreatments;
import com.venedental.model.Treatments;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "propertiesViewBean")
@ViewScoped
public class PropertiesViewBean extends BaseBean {
    
    @EJB
    private PropertiesFacadeLocal propertiesFacadeLocal;
    
    private Properties allProperties = new  Properties("999999","Todos los Dientes");
    
    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;
    
    @ManagedProperty(value = "#{treatmentsViewBean}")
    private TreatmentsViewBean treatmentsViewBean;
    
    @ManagedProperty(value = "#{propertiesApplicationBean}")
    private PropertiesApplicationBean propertiesApplicationBean;
    
    
    private static final long serialVersionUID = -5547104134772667835L;
    
    public PropertiesViewBean() {
        this.newProperties = new String[10];
        this.editProperties = new String[10];
        this.addProperties = new Properties();
        this.visible = false;
        this.selectedProperties = new Properties();
    }
    
    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.getListaProperties().addAll(this.propertiesFacadeLocal.findAll());
        this.getListaPropertiesFiltrada().addAll(this.getListaProperties());
    }
    
    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    
    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }
    
    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    
    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }
    
    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }
    
    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }
    
    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }
    
    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }
    
    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }
    
    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }
    
    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }
    
    private Properties selectedProperties;
    
    private List<Properties> listaProperties;
    
    private List<Properties> listaPropertiesFiltrada;
    
    private String[] newProperties;
    
    private Properties newPropertiesObj;
    
    private String[] editProperties;
    
    private Properties addProperties;
    
    private Properties beforeEditProperties;
    
    private boolean visible;
    
    private String color;
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public boolean isVisible() {
        return visible;
    }
    
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    public Properties getAddProperties() {
        return addProperties;
    }
    
    public void setAddProperties(Properties addProperties) {
        this.addProperties = addProperties;
    }
    
    public String[] getEditProperties() {
        return editProperties;
    }
    
    public void setEditProperties(String[] editProperties) {
        this.editProperties = editProperties;
    }
    
    public String[] getNewProperties() {
        return newProperties;
    }
    
    public void setNewProperties(String[] newProperties) {
        this.newProperties = newProperties;
    }
    
    public Properties getNewPropertiesObj() {
        return newPropertiesObj;
    }
    
    public void setNewPropertiesObj(Properties newPropertiesObj) {
        this.newPropertiesObj = newPropertiesObj;
    }
    
    public Properties getSelectedProperties() {
        return selectedProperties;
    }
    
    
    
    public List<Properties> getListaProperties() {
        if (this.listaProperties == null) {
            this.listaProperties = new ArrayList<>();
            this.listaProperties = this.propertiesFacadeLocal.findAll();  
            this.listaProperties.add(this.allProperties);
        }
        return listaProperties;
    }
    
    public void setListaProperties(List<Properties> listaProperties) {
        this.listaProperties = listaProperties;
    }
    
    public List<Properties> getListaPropertiesFiltrada() {
        if (this.listaPropertiesFiltrada == null) {
            this.listaPropertiesFiltrada = new ArrayList<>();
        }
        return listaPropertiesFiltrada;
    }
    
    public void setListaPropertiesFiltrada(List<Properties> listaPropertiesFiltrada) {
        this.listaPropertiesFiltrada = listaPropertiesFiltrada;
    }
    
    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }
    
    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }
    
    
    public Properties getBeforeEditProperties() {
        return beforeEditProperties;
    }
    
    
    public void limpiarObjetosEdit() {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        this.setEditProperties(new String[]{this.getBeforeEditProperties().getId().toString()});
    }
    
    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }
    
    public TreatmentsViewBean getTreatmentsViewBean() {
        return treatmentsViewBean;
    }
    
    public void setTreatmentsViewBean(TreatmentsViewBean treatmentsViewBean) {
        this.treatmentsViewBean = treatmentsViewBean;
    }
    
    public PropertiesApplicationBean getPropertiesApplicationBean() {
        return propertiesApplicationBean;
    }
    
    public void setPropertiesApplicationBean(PropertiesApplicationBean propertiesApplicationBean) {
        this.propertiesApplicationBean = propertiesApplicationBean;
    }
    
    
    public void filtrarProperties(Treatments treatments) {
        this.getTreatmentsViewBean().getEditTreatments();
        this.getListaProperties().clear();
        if (treatments != null) {
            for (PropertiesTreatments p : this.getPropertiesApplicationBean().getListProperties()) {
                if(p.getPropertiesId()!= null){
                    if (Objects.equals(p.getTreatmentsId().getId(),treatments.getId())) {
                        this.getListaProperties().add(p.getPropertiesId());
                    }
                }
            }
        }
    }
    
}
