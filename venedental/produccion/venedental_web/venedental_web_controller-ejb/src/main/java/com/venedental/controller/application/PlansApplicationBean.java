/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;


import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.model.Plans;
import com.venedental.model.PlansTreatments;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "plansApplicationBean")
@ApplicationScoped
public class PlansApplicationBean  {

    @EJB
    private PlansFacadeLocal plansFacadeLocal;
    
    private List<Plans> listPlans;

    public PlansApplicationBean() {
    }

    @PostConstruct
    public void init() {
        this.getListPlans().addAll(this.plansFacadeLocal.findAll());
    }

    public List<Plans> getListPlans() {
        if (this.listPlans == null) {
            this.listPlans = new ArrayList<>();
        }

        return listPlans;
    }

}
