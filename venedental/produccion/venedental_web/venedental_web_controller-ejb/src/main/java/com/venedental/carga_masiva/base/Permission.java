/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.base;

import com.venedental.carga_masiva.base.properties.PropertiesConfiguration;

/**
 *
 * @author usuario
 */
public class Permission {

    public static void permitReadOnly(String path, String user) {
        permit("555", path);
        setOwner(user, path, true);
    }

    public static void permitWrite(String path) {
        permit("777", path);
    }

    public static void permit(String levels, String path) {
        permit(levels, true, path);
    }

    public static void permit(String levels, boolean recursive, String path) {
        StringBuilder builder = new StringBuilder();
        builder.append("chmod ");
        if (recursive) {
            builder.append("-R ");
        }
        builder.append(levels);
        builder.append(" ");
        builder.append(path);
        Terminal.exec(builder.toString());
    }

    
    
    private static void setOwner(String owner, String path, boolean recursive){
        StringBuilder builder = new StringBuilder();
        builder.append("chown ");
        if (recursive) {
            builder.append("-R ");
        }
        builder.append(owner);
        builder.append(" ");
        builder.append(path);
        Terminal.exec(builder.toString());
    }

}
