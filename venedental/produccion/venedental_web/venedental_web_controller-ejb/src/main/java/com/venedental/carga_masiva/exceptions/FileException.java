/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import java.io.File;

/**
 *
 * @author usuario
 */
public class FileException extends MessageException {

    public FileException(String mensaje, File file) {
        super(mensaje);
        setError(file.getAbsolutePath() + " : " + getMessage());
    }

    public FileException(String mensaje, File file, Throwable throwable) {
        super(mensaje, throwable);
        setError(file.getAbsolutePath() + " : " + getMessage());
    }

}
