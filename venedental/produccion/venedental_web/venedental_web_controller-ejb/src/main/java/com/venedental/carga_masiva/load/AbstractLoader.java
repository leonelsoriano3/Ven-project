/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load;

import com.venedental.carga_masiva.load.helper.ValidatorHelper;
import com.venedental.carga_masiva.load.helper.PersisterHelper;
import com.venedental.carga_masiva.load.helper.WriterHelper;
import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.enums.EInsurances;
import com.venedental.carga_masiva.dto.RowDTO;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.base.properties.PropertiesInsurances;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.carga_masiva.load.helper.NotifierHelper;
import com.venedental.carga_masiva.load.helper.ReaderHelper;
import com.venedental.carga_masiva.load.read.DirectoryReader;
import com.venedental.carga_masiva.load.read.FileReader;
import com.venedental.carga_masiva.load.read.RowReader;
import com.venedental.carga_masiva.load.read.SheetReader;
import com.venedental.facade.store_procedure.CleanStatusFacadeLocal;
import com.venedental.model.Insurances;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public abstract class AbstractLoader {

    private static final Logger log = CustomLogger.getLogger(AbstractLoader.class.getName());

    private final DirectoryReader directoryReader;
    private final FileReader fileReader;
    private final SheetReader sheetReader;
    private final RowReader rowReader;
    private ReaderHelper readerHelper;
    private ValidatorHelper validatorHelper;
    private PersisterHelper persisterHelper;
    private WriterHelper writerHelper;
    private NotifierHelper notifierHelper;
    private boolean clean;
    private Insurances insurance;
    private CleanStatusFacadeLocal cleanStatusFacadeLocal;

    public AbstractLoader() {
        this.clean = false;
        this.rowReader = new RowReader(this);
        this.sheetReader = new SheetReader(rowReader);
        this.fileReader = new FileReader(sheetReader);
        this.directoryReader = new DirectoryReader(fileReader);
    }

    public File load(String pathUpload, String insuranceName, ServiceFacadeDTO serviceFacadeDTO, String date) {
        log.log(Level.INFO, "----------------------------------------");
        this.cleanStatusFacadeLocal = serviceFacadeDTO.getCleanStatusFacadeLocal();
        this.fileReader.setDirectory(date);
        try {
            String path = pathUpload + insuranceName;
            File file = new File(path);
            insurance = loadInsurance(insuranceName, serviceFacadeDTO);
            log.log(Level.INFO, "PROCESANDO ASEGURADORA: {0}", insurance.getName());
            List<Plans> plans = loadPlans(insurance, serviceFacadeDTO);
            createHelpers(insuranceName, path, date, insurance, plans, serviceFacadeDTO);
            read(path, file, insuranceName);
            List<File> files = write();
            notify(files, insurance, insuranceName);
            return files.get(0);
        } catch (MessageException ex) {
            CustomLogger.getLogger(AbstractLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Insurances loadInsurance(String insuranceName, ServiceFacadeDTO serviceFacadeDTO) {
        EInsurances eInsurance = EInsurances.valueOf(insuranceName.toUpperCase());
        List<Insurances> listInsurances = serviceFacadeDTO.getInsuranceFacadeLocal().findById(eInsurance.getId());
        if (listInsurances.isEmpty()) {
            return null;
        }
        return listInsurances.get(0);
    }

    private List<Plans> loadPlans(Insurances insurance, ServiceFacadeDTO serviceFacadeDTO) {
        List<Plans> plans = serviceFacadeDTO.getPlansFacadeLocal().findByInsuranceId(insurance);
        return plans;
    }

    private void createHelpers(String user, String pathInsurance, String directoryDate, Insurances insurance, List<Plans> plans, ServiceFacadeDTO serviceFacadeDTO) {
        readerHelper = new ReaderHelper(this, insurance, plans, serviceFacadeDTO);
        validatorHelper = new ValidatorHelper(serviceFacadeDTO);
        persisterHelper = new PersisterHelper(insurance, serviceFacadeDTO);
        writerHelper = new WriterHelper(user, pathInsurance + Constants.DIRECTORY_PROCESSED, directoryDate + Constants.DIRECTORY_SEPARATOR);
        notifierHelper = new NotifierHelper(serviceFacadeDTO);
    }

    private void read(String path, File file, String insuranceName) throws MessageException {
        String stringTemplate = PropertiesInsurances.get(insuranceName + Constants.SUFIJO_FIELDS_TEMPLATE);
        if (stringTemplate == null) {
            stringTemplate = PropertiesInsurances.get(Constants.PROPERTY_BASIC_FIELDS_TEMPLATE);
        }
        String[] fieldsTemplate = stringTemplate.split(Constants.LIST_SEPARATOR);
        directoryReader.execute(path, file, fieldsTemplate);
    }

    private List<File> write() {
        List<File> listFile = new ArrayList<>();
        File resume = writerHelper.writeResume();
        if (resume != null) {
            listFile.add(resume);
        }
        File correct = writerHelper.writeCorrectRow();
        if (correct != null) {
            listFile.add(correct);
        }
        File incorrect = writerHelper.writeIncorrectRow();
        if (incorrect != null) {
            listFile.add(incorrect);
        }
        return listFile;
    }

    private void notify(List<File> files, Insurances insurance, String insuranceName) {
        notifierHelper.notifyInsurance(files, insurance, insuranceName);
    }

    public void processRow(String filename, String sheetName, long numRow, String[] row, String[] fieldTemplate) {
        RowDTO rowSelected = readerHelper.readRow(filename, sheetName, numRow, row, fieldTemplate);
        validatorHelper.validate(rowSelected);
        if (rowSelected.hasError()) {
            writerHelper.registerIncorrectRow(rowSelected);
        } else {
            if(!clean){
                clean = true;
                cleanStatusFacadeLocal.execute(insurance);
            }
            persisterHelper.persist(rowSelected);
            writerHelper.registerCorrectRow(rowSelected);
        }
    }

    public abstract void loadCustomFieldsPatient(Patients patient, String[] row, String[] fieldTemplate, List<MessageException> exceptions);

    public abstract Integer loadRelationship(String[] row, String[] template, int index) throws MessageException;

    public abstract List<Integer> readPlans(String filename, String sheetName, String[] row, String[] field, int index) throws MessageException;

    public ReaderHelper getReaderHelper() {
        return readerHelper;
    }

}
