package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.facade.ClinicsFacadeLocal;
import com.venedental.facade.MenuFacadeLocal;
import com.venedental.facade.MenuRolFacadeLocal;
import com.venedental.facade.MenuRolOperationFacadeLocal;
import com.venedental.model.Clinics;
import com.venedental.model.SpecialistJob;
import com.venedental.model.security.Menu;
import com.venedental.model.security.MenuRol;
import com.venedental.model.security.MenuRolOperation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "clinicsViewBean")
@ViewScoped
public class ClinicsViewBean extends BaseBean {

    @EJB
    private ClinicsFacadeLocal clinicsFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    @EJB
    private MenuRolFacadeLocal menuRolFacadeLocal;

    @EJB
    private MenuFacadeLocal menuFacadeLocal;

    @EJB
    private MenuRolOperationFacadeLocal menuRolOperationFacadeLocal;

    private static final long serialVersionUID = -5547104134772667835L;

    public ClinicsViewBean() {
        this.newClinics = new String[4];
        this.editClinics = new String[4];
        this.addClinics = new Clinics();
        this.tipoRifEdit = "V";
        this.tipoMedicalOffice = "clinica";
        this.mascaraRifEdit = "V999999999";
        this.visible = false;
    }

    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.findClinics();
        this.operationSecurity();
    }

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;

    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;

    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    private Clinics viewClinics;

    public MenuRolFacadeLocal getMenuRolFacadeLocal() {
        return menuRolFacadeLocal;
    }

    public void setMenuRolFacadeLocal(MenuRolFacadeLocal menuRolFacadeLocal) {
        this.menuRolFacadeLocal = menuRolFacadeLocal;
    }

    public MenuFacadeLocal getMenuFacadeLocal() {
        return menuFacadeLocal;
    }

    public void setMenuFacadeLocal(MenuFacadeLocal menuFacadeLocal) {
        this.menuFacadeLocal = menuFacadeLocal;
    }

    public ClinicsFacadeLocal getClinicsFacadeLocal() {
        return clinicsFacadeLocal;
    }

    public void setClinicsFacadeLocal(ClinicsFacadeLocal clinicsFacadeLocal) {
        this.clinicsFacadeLocal = clinicsFacadeLocal;
    }

    public MenuRolOperationFacadeLocal getMenuRolOperationFacadeLocal() {
        return menuRolOperationFacadeLocal;
    }

    public void setMenuRolOperationFacadeLocal(MenuRolOperationFacadeLocal menuRolOperationFacadeLocal) {
        this.menuRolOperationFacadeLocal = menuRolOperationFacadeLocal;
    }

    public Clinics getViewClinics() {
        return viewClinics;
    }

    public void setViewClinics(Clinics viewClinics) {
        this.viewClinics = viewClinics;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    private Boolean operationCreate, operationRead, operationUpdate, operationDelete;

    private Clinics selectedClinics;

    private List<Clinics> listaClinics;

    private List<Clinics> listaClinicsFiltrada;

    private List<SpecialistJob> listaClinicsSpecialist;

    private String[] newClinics;

    private Clinics newClinicsObj;

    private String[] editClinics;

    private String clinicsSelected;

    private Clinics addClinics;

    private String tipoRifEdit;

    private Clinics clinicsDelete;

    private String mascaraRifEdit;

    private Clinics beforeEditClinics;

    private boolean visible;

    private String tipoMedicalOffice;

    private int indiceClinics;

    public Boolean getOperationCreate() {
        return operationCreate;
    }

    public void setOperationCreate(Boolean operationCreate) {
        this.operationCreate = operationCreate;
    }

    public Boolean getOperationRead() {
        return operationRead;
    }

    public void setOperationRead(Boolean operationRead) {
        this.operationRead = operationRead;
    }

    public Boolean getOperationUpdate() {
        return operationUpdate;
    }

    public void setOperationUpdate(Boolean operationUpdate) {
        this.operationUpdate = operationUpdate;
    }

    public Boolean getOperationDelete() {
        return operationDelete;
    }

    public void setOperationDelete(Boolean operationDelete) {
        this.operationDelete = operationDelete;
    }

    public String getTipoMedicalOffice() {
        return tipoMedicalOffice;
    }

    public void setTipoMedicalOffice(String tipoMedicalOffice) {
        this.tipoMedicalOffice = tipoMedicalOffice;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Clinics getAddClinics() {
        return addClinics;
    }

    public void setAddClinics(Clinics addClinics) {
        this.addClinics = addClinics;
    }

    public String[] getEditClinics() {
        return editClinics;
    }

    public Clinics getClinicsDelete() {
        return clinicsDelete;
    }

    public void setClinicsDelete(Clinics clinicsDelete) {
        this.clinicsDelete = clinicsDelete;
    }

    public void setEditClinics(String[] editClinics) {
        this.editClinics = editClinics;
    }

    public String[] getNewClinics() {
        return newClinics;
    }

    public void setNewClinics(String[] newClinics) {
        this.newClinics = newClinics;
    }

    public Clinics getNewClinicsObj() {
        return newClinicsObj;
    }

    public void setNewClinicsObj(Clinics newClinicsObj) {
        this.newClinicsObj = newClinicsObj;
    }

    public Clinics getSelectedClinics() {
        return selectedClinics;
    }

    public String getClinicsSelected() {
        return clinicsSelected;
    }

    public void setClinicsSelected(String clinicsSelected) {
        this.clinicsSelected = clinicsSelected;
    }

    public void selectedClinic(Clinics clinics, int indice) {

        this.setClinicsSelected(clinics.getName());
        this.clinicsDelete = clinics;
        this.indiceClinics = indice;

    }

    public int getIndiceClinics() {
        return indiceClinics;
    }

    public void setIndiceClinics(int indiceClinics) {
        this.indiceClinics = indiceClinics;
    }

    public void setSelectedClinics(Clinics selectedClinics) {
        this.selectedClinics = selectedClinics;
        String status = String.valueOf(selectedClinics.getStatus());
        if (selectedClinics.getRif() != null) {
            this.setTipoRifEdit(String.valueOf(selectedClinics.getRif().charAt(0)));
            mascaraRifClinic(this.getTipoRifEdit());
        } else {
            this.setTipoRifEdit("V");
            this.mascaraRifEdit = "V999999999";
        }
        System.out.println(selectedClinics.getRif());

        if (selectedClinics.getRif() != null) {
            this.setEditClinics(new String[]{selectedClinics.getId().toString(), selectedClinics.getName(), selectedClinics.getRif(), status});
        } else {
            this.setEditClinics(new String[]{selectedClinics.getId().toString(), selectedClinics.getName(), status});
        }
        this.getAddressesViewBean().setEditAddresses(selectedClinics.getAddressId());
        this.getCitiesViewBean().setEditCities(selectedClinics.getAddressId().getCityId());
        this.getStateViewBean().setEditState(selectedClinics.getAddressId().getCityId().getStateId());
        this.getPhoneClinicViewBean().setEditListPhoneClinic(selectedClinics.getPhoneClinicList());

    }

    public void mascaraRifClinic(String tipoRif) {
        this.setMascaraRifEdit("V999999999");
        switch (tipoRif) {
            case "V":
                this.setMascaraRifEdit("V999999999");
                break;
            case "J":
                this.setMascaraRifEdit("J999999999");
                break;
            case "E":
                this.setMascaraRifEdit("E999999999");
                break;
            case "G":
                this.setMascaraRifEdit("G999999999");
                break;
        }
    }

    public List<Clinics> getListaClinics() {
        if (this.listaClinics == null) {
            this.listaClinics = new ArrayList<>();
        }
        Collections.sort(listaClinics, new Clinics().compareToEstadoYCiudad());
        return listaClinics;

    }

    public void setListaClinics(List<Clinics> listaClinics) {
        this.listaClinics = listaClinics;
    }

    public List<SpecialistJob> getListaClinicsSpecialist() {
        if (this.listaClinicsSpecialist == null) {
            this.listaClinicsSpecialist = new ArrayList<>();
        }
        return listaClinicsSpecialist;
    }

    public void setListaClinicsSpecialist(List<SpecialistJob> listaClinicsSpecialist) {
        this.listaClinicsSpecialist = listaClinicsSpecialist;
    }

    public List<Clinics> getListaClinicsFiltrada() {
        return listaClinicsFiltrada;
    }

    public void setListaClinicsFiltrada(List<Clinics> listaClinicsFiltrada) {
        this.listaClinicsFiltrada = listaClinicsFiltrada;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public String getTipoRifEdit() {
        return tipoRifEdit;
    }

    public void setTipoRifEdit(String tipoRifEdit) {
        this.tipoRifEdit = tipoRifEdit;
    }

    public String getMascaraRifEdit() {
        return mascaraRifEdit;
    }

    public void setMascaraRifEdit(String mascaraRifEdit) {
        this.mascaraRifEdit = mascaraRifEdit;
    }

    public Clinics getBeforeEditClinics() {
        return beforeEditClinics;
    }

    public void setBeforeEditClinics(Clinics beforeEditClinics) throws CloneNotSupportedException {
        this.beforeEditClinics = (Clinics) beforeEditClinics.clone();
    }

    public void limpiarObjetosEdit() {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        this.setEditClinics(new String[]{this.getBeforeEditClinics().getId().toString(), this.getBeforeEditClinics().getName(), this.getBeforeEditClinics().getRif()});
    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
        
    }

    public void findClinics() {
        this.setListaClinics(new ArrayList<Clinics>());
        this.setListaClinics(this.clinicsFacadeLocal.findByStatus(1));        
        Collections.sort(listaClinics, new Clinics().compareToEstadoYCiudad()); 
        this.setListaClinicsFiltrada(this.getListaClinics());
        RequestContext.getCurrentInstance().update("form:listaClinics");
    }

    public void resetDataTableClinic() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:listaClinics");
        dataTable.reset();
        RequestContext.getCurrentInstance().reset("form:listaClinics");
        dataTable.getFilters().clear();
        RequestContext.getCurrentInstance().update("form:listaClinics");
    }
    
    public void operationSecurity() {

        this.operationCreate = false;
        this.operationDelete = false;
        this.operationRead = false;
        this.operationUpdate = false;

        List<MenuRol> listMenuRol = new ArrayList<>();
        List<MenuRolOperation> listMenuRolOperation = new ArrayList<>();

        Menu menu = this.menuFacadeLocal.findByName("Clínicas");
        listMenuRol = this.securityViewBean.obtainMenuByOperations();

        for (int i = 0; i < listMenuRol.size(); i++) {

            Menu menuRol = listMenuRol.get(i).getMenuId();

            if (menu != null) {

                if (menu.getId() == menuRol.getId()) {

                    listMenuRolOperation = this.securityViewBean.obtainMenuRolOperations(listMenuRol.get(i).getId());

                    for (int j = 0; j < listMenuRolOperation.size(); j++) {

                        String operation = listMenuRolOperation.get(j).getOperation().getName();

                        if (operation.equals("Crear")) {

                            this.operationCreate = true;

                        } else if (operation.equals("Consultar")) {

                            this.operationRead = true;

                        } else if (operation.equals("Modificar")) {

                            this.operationUpdate = true;

                        } else if (operation.equals("Eliminar")) {

                            this.operationDelete = true;

                        }

                    }

                }
            }

        }

    }
}
