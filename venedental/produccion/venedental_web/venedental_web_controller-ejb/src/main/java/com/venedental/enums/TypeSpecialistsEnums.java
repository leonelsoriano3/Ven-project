package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum TypeSpecialistsEnums {

    ODONTOLOGIA(1), OFTALMOLOGIA(2), DERMATOLOGIA(3), PSICOLOGIA(4), FISIOTERAPIA(5);

    Integer valor;

    private TypeSpecialistsEnums(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
