/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum TipoClinica {
    
    CLINICA(0), CONSULTORIO(1);

    Integer valor;

    private TipoClinica(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
    
    
    
    
}
