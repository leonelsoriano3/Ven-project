/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.session;

import com.venedental.controller.BaseBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "errorHttpSessionBean")
@SessionScoped
public class ErrorHttpSessionBean extends BaseBean {

    private static final Logger log = CustomLogger.getGeneralLogger(ErrorHttpSessionBean.class.getName());

    private SelectItem error;

    private List<SelectItem> itemsError;

    public ErrorHttpSessionBean() {
    }

    @PostConstruct
    public void init() {
        setItemsError(new ArrayList<SelectItem>());
        getItemsError().add(new SelectItem(""));
        getItemsError().add(new SelectItem("404"));
        getItemsError().add(new SelectItem("500"));
    }

    public SelectItem getError() {
        return error;
    }

    public void setError(SelectItem error) {
        this.error = error;
    }

    public List<SelectItem> getItemsError() {
        return itemsError;
    }

    public void setItemsError(List<SelectItem> itemsError) {
        this.itemsError = itemsError;
    }

//    public void updateError(PersonalHttpException error) {
//        if (error != null) {
//            setError(new SelectItem(String.valueOf(error.getError())));
//            actualizarVista();
//        }
//    }

    public void actualizarVista() {
        RequestContext contexto = RequestContext.getCurrentInstance();
        contexto.update("errorHttp");
    }

    public void listenerError(ValueChangeEvent event) {
        try {
            String nroError = (String) ((SelectItem) event.getOldValue()).getValue();
            setError(new SelectItem(""));
            if (nroError.compareTo("500") == 0) {
                redireccionar("/error/error500.html");
            } else if (nroError.compareTo("404") == 0) {
                redireccionar("/error/error404.html");
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

}
