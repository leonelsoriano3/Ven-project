/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.read;

import com.opencsv.CSVReader;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.base.enums.EFields;
import com.venedental.dto.massiveLoading.RowLoad;
import com.venedental.dto.massiveLoading.SheetLoad;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.massiveLoading.LoaderInsurance;
import com.venedental.massiveLoading.base.enums.SubEFields;
import com.venedental.model.Insurances;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akdesk01
 */
public class ReaderSheet {

    private static final Logger log = CustomLogger.getLogger(ReaderSheet.class.getName());

    private final SheetLoad sheetLoad;
    private final LoaderInsurance loader;
    private Boolean incorrect;
    private Boolean correct;

    public ReaderSheet(SheetLoad sheetLoad, LoaderInsurance loader) {
        this.sheetLoad = sheetLoad;
        this.loader = loader;
        this.incorrect=false;
         this.correct=false;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Boolean getIncorrect() {
        return incorrect;
    }

    public void setIncorrect(Boolean incorrect) {
        this.incorrect = incorrect;
    }


    public void load(Insurances insurance,List<FindPlanByIdOutput> planList) {
        log.log(Level.INFO, "Inicio - Lectura de la Hoja: {0}", sheetLoad.getSheetName());
        loader.getWriterHelper().registerCommonLine(" - HOJA: " + sheetLoad.getSheetName());
        try {
            CSVReader csvReader = new CSVReader(new java.io.FileReader(sheetLoad.getSheetFile()));
            Iterator<String[]> iterator = csvReader.iterator();
            
            long numRow = 0;
            loader.getWriterHelper().registerCorrectLine(printHeaderIncorrect());
            loader.getWriterHelper().registerIncorrectLine(printHeaderIncorrect());
            while (iterator.hasNext()) {
                String[] row = iterator.next();
                log.log(Level.INFO, "Fila: {0}", ++numRow);
                if(numRow>1){
                RowLoad rowLoad = createRowLoad(numRow, row);
                ReaderRow readerRow = new ReaderRow(rowLoad, loader);
                readerRow.load(insurance,planList);
                this.incorrect=readerRow.getRowIncorrect();
                this.correct=readerRow.getRowCorrect();
                }
                
            }
            if(!this.correct){
            loader.getWriterHelper().registerCorrectLine("NO EXISTEN REGISTROS VÁLIDOS EN LA HOJA");
            this.correct=false;
            }
            if(!this.incorrect){
            loader.getWriterHelper().registerIncorrectLine("NO EXISTEN REGISTROS INVÁLIDOS EN LA HOJA");
            this.incorrect=false;
            }
            if (numRow == 0) {
                loader.getWriterHelper().registerCorrectLine("    - NO EXISTEN REGISTROS PARA PROCESAR EN LA HOJA");
                loader.getWriterHelper().registerIncorrectLine("    - NO EXISTEN REGISTROS PARA PROCESAR EN LA HOJA");
            }
            sheetLoad.getSheetFile().delete();
        } catch (FileNotFoundException ex) {
            loader.getWriterHelper().registerCorrectLine("");
            loader.getWriterHelper().registerIncorrectLine("    - Error al leer la hoja");
            log.log(Level.INFO, "Error al leer la hoja actual \"{0}\"", sheetLoad.getSheetName());
        }
        log.log(Level.INFO, "Fin - Lectura de la Hoja: {0}", sheetLoad.getSheetName());
    }

    private RowLoad createRowLoad(long numRow, String[] row) {
        List<String> data = new ArrayList<>();
        for(EFields efield : EFields.values()){
            Integer column =findColumn(efield);
            if(column != null && column < row.length ){               
              data.add(row[findColumn(efield)]);
            } 
        }
        return new RowLoad(numRow, data, sheetLoad);
    }

    private Integer findColumn(EFields field){
        if(field.getOrder() < sheetLoad.getFileLoad().getFields().size()){
            return sheetLoad.getFileLoad().getFields().get(field.getOrder());
        }
        
        return null ;
    }
    
    private String printHeader() {
        StringBuilder sb = new StringBuilder("                    | ");
        for(EFields efield : EFields.values()){
            sb.append(prepareField(efield)).append(" | ");
        }
        return sb.toString();
    }
     private String printHeaderIncorrect() {
        StringBuilder sb = new StringBuilder("                    | ");
        for(SubEFields subEfield : SubEFields.values()){
            sb.append(prepareFieldIncorrect(subEfield)).append("|");
        }
        return sb.toString();
    }
    
    private String prepareField(EFields eField) {
        String valueString = eField.getName();
        if (valueString.length() > eField.getWidth()) {
            valueString = valueString.substring(0, eField.getWidth());
        } else if (valueString.length() < eField.getWidth()) {
            char[] chars = new char[eField.getWidth() - valueString.length()];
            Arrays.fill(chars, ' ');
            String spaces = new String(chars);
            valueString = valueString + spaces;
        }
        return valueString;
    }
    private String prepareFieldIncorrect(SubEFields eField) {
        String valueString = eField.getName();
        if (valueString.length() > eField.getWidth()) {
            valueString = valueString.substring(0, eField.getWidth());
        } else if (valueString.length() < eField.getWidth()) {
            char[] chars = new char[eField.getWidth() - valueString.length()];
            Arrays.fill(chars, ' ');
            String spaces = new String(chars);
            valueString = valueString + spaces;
        }
        return valueString;
    }
    
}
