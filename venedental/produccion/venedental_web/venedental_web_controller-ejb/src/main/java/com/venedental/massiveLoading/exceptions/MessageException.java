/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.exceptions;

/**
 *
 * @author usuario
 */
public class MessageException extends Exception {

    private String error;

    public MessageException() {
        super("Empty");
    }
    
    public MessageException(String mensaje) {
        super(mensaje);
        error = mensaje;
    }

    public MessageException(String mensaje, Throwable throwable) {
        super(mensaje, throwable);
        error = mensaje;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
