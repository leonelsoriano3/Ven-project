/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.helper;

import controller.utils.CustomLogger;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.Insurances;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class NotifierHelper {

    private static final Logger log = CustomLogger.getLogger(NotifierHelper.class.getName());

    private final MailSenderLocal mailSenderLocal;

    public NotifierHelper(MailSenderLocal mailSenderLocal) {
        this.mailSenderLocal = mailSenderLocal;
    }

    public void notifyInsurance(List<File> files, Insurances insurance, List<String> emails, String modeLoad) {
        log.log(Level.INFO, "Inicio - Proceso de envio de correos por aseguradora: {0}", insurance.getName());
        if (!emails.isEmpty()) {
            String subjectMessage = "Resumen de Carga Masiva ("+ modeLoad+") Aseguradora " + insurance.getName().toUpperCase();
            String contentMessageInsurance;
            
            if (files.size() == 1) {
                contentMessageInsurance = buildMessageIncorrectColumn(insurance);
            }
            else {
                contentMessageInsurance = buildMessageInsurance(insurance);
            }

            String[] arrayMails = new String[emails.size()];
            for (int i = 0; i < emails.size(); i++) {
                arrayMails[i] = emails.get(i);
            }
            mailSenderLocal.send(Email.MAIL_CARGA_MASIVA, subjectMessage, "Resumen de la Carga Masiva ("+ modeLoad+")", contentMessageInsurance, files, (String[]) arrayMails);
        } else {
            log.log(Level.INFO, "La aseguradora no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos por aseguradora");
    }

    private String buildMessageInsurance(Insurances insurance) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Usuario").append(".<br/>").append("<br/>");
        stringBuilder.append("Se ha realizado la carga masiva para la aseguradora: ").append(insurance.getName().toUpperCase()).append(".<br/>");
        stringBuilder.append("Se anexan los siguientes archivos: ").append(".<br/>");
        stringBuilder.append("  - Anexo 1: Registros válidos").append("<br/>");
        stringBuilder.append("  - Anexo 2: Registros inválidos").append("<br/>");
        stringBuilder.append("  - Anexo 3: Resumen de la aseguradora").append("<br/>").append("<br/>").append("<br/>");
        stringBuilder.append("Siempre dispuestos a prestarle un mejor servicio.").append("<br/>");
        stringBuilder.append("<b>").append("Grupo VENEDEN").append("</b>").append("<br/>");
        return stringBuilder.toString();
    }
    
    private String buildMessageIncorrectColumn(Insurances insurance) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Usuario").append(".<br/>").append("<br/>");
        stringBuilder.append("Se ha realizado la carga masiva para la aseguradora: ").append(insurance.getName().toUpperCase()).append(".<br/>");
        stringBuilder.append("Se anexa el siguientes archivo: ").append(".<br/>");
        stringBuilder.append("  - Anexo: Resumen de la aseguradora").append("<br/>").append("<br/>").append("<br/>");
        stringBuilder.append("Siempre dispuestos a prestarle un mejor servicio.").append("<br/>");
        stringBuilder.append("<b>").append("Grupo VENEDEN").append("</b>").append("<br/>");
        return stringBuilder.toString();
    }

}
