/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;

import com.venedental.controller.BaseBean;
import com.venedental.facade.TypeSpecialistFacadeLocal;
import com.venedental.model.TypeSpecialist;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "typeSpecialistApplicationBean")
@ApplicationScoped
public class TypeSpecialistApplicationBean extends BaseBean {

    @EJB
    private TypeSpecialistFacadeLocal typeSpecialistFacadeLocal;

    public TypeSpecialistApplicationBean() {
    }

    @PostConstruct
    public void init() {
        this.getListTypeSpecialist().addAll(this.typeSpecialistFacadeLocal.findAll());
    }

    private List<TypeSpecialist> listTypeSpecialist;

    public List<TypeSpecialist> getListTypeSpecialist() {
        if (this.listTypeSpecialist == null) {
            this.listTypeSpecialist = new ArrayList<>();
        }
        return listTypeSpecialist;
    }

    public void setListTypeSpecialist(List<TypeSpecialist> listState) {
        this.listTypeSpecialist = listState;
    }


}
