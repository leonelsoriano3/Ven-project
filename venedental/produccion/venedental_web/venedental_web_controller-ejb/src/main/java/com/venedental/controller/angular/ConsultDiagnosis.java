/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.angular;

import com.venedental.dto.managementSpecialist.*;
import com.venedental.services.ServiceManagementSpecialist;




import com.venedental.dto.managementSpecialist.ConsultPropertiesTreatmentsOutput;
import com.venedental.dto.managementSpecialist.ConsultTreatmentSelectOutput;
import com.venedental.dto.managementSpecialist.FindPieceByidKeyOut;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author akdesk01
 */
@ApplicationPath("/resources")
@Path("consultDiagnosis")
public class ConsultDiagnosis {

   private List<ConsultTreatmentSelectOutput> consultTreatmentSelectOutputlist;

   private List<ConsultPropertiesTreatmentsOutput> consultPropertiesTreatmentsOutputList;

     @EJB
    private ServiceManagementSpecialist serviceManagementSpecialist;

    public List<ConsultTreatmentSelectOutput> getConsultTreatmentSelectOutputlist() {
        return consultTreatmentSelectOutputlist;
    }

    public void setConsultTreatmentSelectOutputlist(List<ConsultTreatmentSelectOutput> consultTreatmentSelectOutputlist) {
        this.consultTreatmentSelectOutputlist = consultTreatmentSelectOutputlist;
    }

    public ServiceManagementSpecialist getServiceManagementSpecialist() {
        return serviceManagementSpecialist;
    }

    public void setServiceManagementSpecialist(ServiceManagementSpecialist serviceManagementSpecialist) {
        this.serviceManagementSpecialist = serviceManagementSpecialist;
    }

    public List<ConsultPropertiesTreatmentsOutput> getConsultPropertiesTreatmentsOutputList() {
        return consultPropertiesTreatmentsOutputList;
    }

    public void setConsultPropertiesTreatmentsOutputList(List<ConsultPropertiesTreatmentsOutput> consultPropertiesTreatmentsOutputList) {
        this.consultPropertiesTreatmentsOutputList = consultPropertiesTreatmentsOutputList;
    }







     @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFindPiece")
    public List<FindPieceByidKeyOut> getFindPiece(@QueryParam("key") Integer key){


        List<FindPieceByidKeyOut> list;

        list = this.serviceManagementSpecialist.findPiece(key);

        if(list == null){

            FindPieceByidKeyOut findPieceByidKeyOut = new FindPieceByidKeyOut();
            findPieceByidKeyOut.setIdPiece(-1);
            list.add(findPieceByidKeyOut);
            return list;

        }
        else{
            this.getPieceandTreatment(key);
            for(FindPieceByidKeyOut findPieceByidKeyOut:list){
                for(ConsultPropertiesTreatmentsOutput consultPropertiesTreatmentsOutput:consultPropertiesTreatmentsOutputList){
                    if(findPieceByidKeyOut.getIdPiece()==consultPropertiesTreatmentsOutput.getIdPiece()){
                        findPieceByidKeyOut.setIsSelect(true);
                    }else{
                        findPieceByidKeyOut.setIsSelect(false);
                    }

                }
            }
            return list;

        }

    }



     @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getPieceandTreatment")
    public List<ConsultPropertiesTreatmentsOutput> getPieceandTreatment(@QueryParam("key") Integer key){

        List<ConsultPropertiesTreatmentsOutput> list;
        list = this.serviceManagementSpecialist.FindPieceandTreatment(key);
        this.setConsultPropertiesTreatmentsOutputList(list);
        return list;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getTreatmentSelect")
    public List<ConsultTreatmentSelectOutput> getTreatmentSelect(@QueryParam("key") Integer key){

        List<ConsultTreatmentSelectOutput> list;

        list = this.serviceManagementSpecialist.FindTreatmentSelect(key);
        this.setConsultTreatmentSelectOutputlist(list);
        for(ConsultTreatmentSelectOutput consultTreatmentSelectOutput:list){
            if(consultTreatmentSelectOutput.getTreatmentAssigned()==1){
                consultTreatmentSelectOutput.setIsCheck(true);
            }else if(consultTreatmentSelectOutput.getTreatmentAssigned()==0){
                consultTreatmentSelectOutput.setIsCheck(false);
            }
        }
        return list;

    }

}
