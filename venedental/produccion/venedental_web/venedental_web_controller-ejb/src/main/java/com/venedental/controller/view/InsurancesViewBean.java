/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.model.Insurances;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author CarlosDaniel
 */
@ManagedBean(name = "insurancesViewBean")
@ViewScoped
public class InsurancesViewBean extends BaseBean {
    
    @EJB
    private InsurancesFacadeLocal insurancesFacadeLocal;
    
    private Insurances newInsurances;
    
    private Insurances editInsurances;
    
    private List<Insurances> listaInsurances;
    
    public InsurancesViewBean() {
        this.newInsurances = new Insurances();
    }
    
    public Insurances getNewInsurances() {
        return newInsurances;
    }

    public void setNewInsurances(Insurances newInsurances) {
        this.newInsurances = newInsurances;
    }
    
    public List<Insurances> getListaInsurances() {
        if (this.listaInsurances == null) {
            this.listaInsurances = new ArrayList<>();
            this.listaInsurances = this.insurancesFacadeLocal.findAll();    
        }
        return listaInsurances;
    }

    public void setListaInsurances(List<Insurances> listaInsurances) {
        this.listaInsurances = listaInsurances;
    }
    
    public Insurances getEditInsurances() {
        return editInsurances;
    }
    
    public void setEditTypeSpecialist(Insurances editInsurances) {
        this.editInsurances = editInsurances;
    }
}
