package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.UsersSessionBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.PlansTreatmentsViewBean;
import com.venedental.controller.view.PropertiesTreatmentsViewBean;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.model.security.Users;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;
import services.utils.Encriptar;

@ManagedBean(name = "accionesRequestBean")
@RequestScoped
public class AccionesRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(AccionesRequestBean.class.getName());

    @EJB
    private UsersFacadeLocal usersFacadeLocal;

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{usersSessionBean}")
    private UsersSessionBean usersSessionBean;
    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;

    private String valor = "";

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public UsersSessionBean getUsersSessionBean() {
        return usersSessionBean;
    }

    public void setUsersSessionBean(UsersSessionBean usersSessionBean) {
        this.usersSessionBean = usersSessionBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public void idleListener() throws IOException {
        this.getUsersSessionBean().setAutenticado(null);
        this.redireccionar("/index.xhtml");
    }

    public void activeListener() {
    }

    /*
     public void login() {
     try {
     Users users;
     users = this.usersFacadeLocal.findByLoginAndPassword(this.getLoginViewBean().getLogin(), Encriptar.md5(this.getLoginViewBean().getPassword()));
     this.getUsersSessionBean().setAutenticado(users);
     if (this.getUsersSessionBean().getAutenticado().getId() != (new Users()).getId()) {
     this.redireccionar("/web/index.xhtml");
     log.log(Level.INFO, "Usuario logueado: " + users.getLogin());
     } else {
     FacesContext.getCurrentInstance().addMessage("login", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario o contraseña que ingresaste no son válidos.", null));
     }
     } catch (NoSuchAlgorithmException | IOException ex) {
     Logger.getLogger(AccionesRequestBean.class.getName()).log(Level.SEVERE, null, ex);
     }

     }
     */
    public void desplegarDialogoView(String dialogo) {
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').show()");
    }

    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').show()");
    }

    public void desplegarDialogoEdit(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosEdit(valoresRequeridos);
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').show()");
    }

    public void cerrarDialogoCreate(String dialogo) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(false);
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("éxito", "Operacion realizada correctamente"));
    }

    public void cerrarDialogoView(String dialogo) {
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
    }

    public void cerrarDialogoView2(String dialogo) {
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
        RequestContext.getCurrentInstance().reset("form:" + dialogo);
    }

    public void cerrarDialogoEdit(String dialogo) {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
    }

    /**
     * Method to update an table and close an dialog
     *
     * @param dialog
     * @param dataUpdate
     */
    public void CloseDialogUpdate(String dialog, String dataUpdate) {
        context.update("form:" + dialog);
        context.execute("PF('" + dialog + "').hide()");
        RequestContext.getCurrentInstance().update(dataUpdate);
    }

    public void bloquearPantalla(String target) {
        this.getComponenteViewBean().setBlockerTarget(target);
        context.execute("PF('blockUIWidget').block()");
    }

    public void desbloquearPantalla(String target) {
        this.getComponenteViewBean().setBlockerTarget(target);
        context.execute("PF('blockUIWidget').unblock()");
    }

    public void inicializarCamposRequeridos(Boolean valor) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valor);
        this.getComponenteSessionBean().setCamposRequeridosEdit(valor);
    }

    public void focusGrowl() {
        RequestContext.getCurrentInstance().scrollTo("growl");
    }

    public void navigation(String link) throws IOException {
        this.redireccionar("/web/" + link + ".xhtml");
    }

    public void navigation2(String link) throws IOException {
        this.redireccionar(link);
    }

    public void navigationApplication(String link) throws IOException {
        this.redireccionar("/views/application/" + link + ".xhtml");
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
