/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base;

/**
 *
 * @author usuario
 */
public class Constants {

    public static String PROPERTIES_PATH = "src/main/resources/";

    public static String FILENAME_CONFIGURATION = "config_massive_loading.properties";
    public static String FILENAME_INSURANCES = "insurances.properties";

    public static String PROPERTY_PATH_FTP = "directory.ftp";
    public static String PROPERTY_INSURANCES = "insurances";
    public static String PROPERTY_STATUS_LOGGER = "logger";
    public static String PROPERTY_MAIL_VENEDEN = "mail.veneden";

    public static String PROPERTY_MANUAL_USER = "manual.user";
    public static String PROPERTY_MANUAL_PASSWORD = "manual.password";

    public static String PROPERTY_BASIC_LOADER = "basic.loader";
    public static String PROPERTY_BASIC_FIELDS_TEMPLATE = "basic.dataRow";

    public static String STATUS_ON = "on";
    public static String STATUS_OFF = "off";

    public static String LIST_SEPARATOR = ",";
    public static String EXTENSION_SEPARATOR = ".";
    public static String DATE_SEPARATOR = "-";
    public static String DIRECTORY_SEPARATOR = "/";

    public static String SEPARATOR_SHEET = "_-SHEET-_";
    public static String SEPARATOR_RESULT = "_-RESULT-_";
    public static String SEPARATOR_EXCEPTION = "_-EXCEPTION-_";

    public static String SUFIJO_LOADER = ".loader";
    public static String SUFIJO_FIELDS_TEMPLATE = ".dataRow";
    public static String SUFIJO_MAIL = ".mail";
    public static String SUFIJO_GEN_CSV = "_gen_from_excel.csv";

    public static String EXTENSION_XLS = ".xls";
    public static String EXTENSION_XLSX = ".xlsx";
    public static String EXTENSION_CSV = "csv";

    public static String PACKAGE_LOADER = "com.venedental.carga_masiva.load.impl.";

    public static String CAMPO_POR_PROCESAR = "X";

    public static String FORMAT_INT_STRING = "%.0f";
    public static String FORMAT_DATE_4 = "dd/MM/yyyy";
    public static String FORMAT_DATE_4_YEAR = "yyyy/MM/dd";
    public static String FORMAT_DATE_2 = "dd/MM/yy";
    public static String FORMAT_HOUR = "HH:mm";
    public static String FORMAT_HOUR_12 = "hh:mm:ss aa";
    public static String FORMAT_DATE_DIRECTORY = "yyyyMMdd-HHmm";
    public static String FORMAT_DATE_COMPLETE = "yyyy/MM/dd HH:mm:ss.SSS";

    public static String INITIAL_SEX_M = "M";
    public static String INITIAL_SEX_F = "F";

    public static String PREFIJO_SEXO_M = "MASC";
    public static String PREFIJO_SEXO_F = "FEM";

    public static String DIRECTORY_PROCESSED = "/Procesados/";
    public static String PROCESSED = "Procesados";

    public static String MINOR = "MENOR";
    public static String PASSWORD_SUDO = "password.bash";

    public static String REG_READ = "- Registros Leídos: ";
    public static String REG_VALID = "- Registros Válidos: ";
    public static String REG_INVALID = "- Registros Inválidos: ";

}
