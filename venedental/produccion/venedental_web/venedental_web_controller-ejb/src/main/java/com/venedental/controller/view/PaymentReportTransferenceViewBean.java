/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.facade.PaymentReportFacadeLocal;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.StatusAnalysisKeysFacadeLocal;
import com.venedental.model.PaymentReport;
import com.venedental.model.Specialists;
import com.venedental.model.StatusAnalysisKeys;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "paymentReportTransferenceViewBean")
@ViewScoped
public class PaymentReportTransferenceViewBean {

    private final RequestContext context = RequestContext.getCurrentInstance();
    List<PaymentReport> paymentReport;
    List<PaymentReport> paymentReportFiltered;
    List<PaymentReport> paymentReportSelected;
    Specialists specialist;
    private Specialists selectedSpecialist;

    @EJB
    private SpecialistsFacadeLocal specialistsFacadeLocal;

    @EJB
    private PaymentReportFacadeLocal paymentReportFacadeLocal;

    @EJB
    private StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal;

    public List<PaymentReport> getPaymentReport() {
        if (this.paymentReport == null) {
            this.paymentReport = new ArrayList<>();
        }
        return paymentReport;
    }

    public void setPaymentReport(List<PaymentReport> paymentReport) {
        this.paymentReport = paymentReport;
    }

    public List<PaymentReport> getPaymentReportFiltered() {
        return paymentReportFiltered;
    }

    public void setPaymentReportFiltered(List<PaymentReport> paymentReportFiltered) {
        this.paymentReportFiltered = paymentReportFiltered;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public List<PaymentReport> getPaymentReportSelected() {
        return paymentReportSelected;
    }

    public void setPaymentReportSelected(List<PaymentReport> paymentReportSelected) {
        this.paymentReportSelected = paymentReportSelected;
    }

    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        List<Specialists> filteredSpecialists = this.specialistsFacadeLocal.findByIdentityNumberOrName(query);
        return filteredSpecialists;
    }

    /**
     * Method called when I select a specialist from the list
     *
     * @param event
     * @throws IOException
     */
    public void onItemSelect(SelectEvent event) throws IOException {
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        filterPaymentReport();
    }

    public void filterPaymentReport() {
        resetDataTablePaymentReport();
        StatusAnalysisKeys byInvoice = this.statusAnalysisKeysFacadeLocal.findById(EstatusPlansTreatmentsKeys.POR_ABONAR.getValor());
        this.setPaymentReport(new ArrayList<PaymentReport>());
        this.setPaymentReport(this.paymentReportFacadeLocal.filterBySpecialistReportStatusAndDateRange(this.getSelectedSpecialist().getId(), byInvoice.getId(), null, null));
        RequestContext.getCurrentInstance().update("form:paymentReportTranferenceList");
    }

    public void resetDataTablePaymentReport() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:paymentReportTranferenceList");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:paymentReportTranferenceList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:paymentReportTranferenceList");
        }
    }

}
