/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.angular;

import com.venedental.constans.ConstansEmailCondicionado;
import com.venedental.constans.ConstansEmailTradicional;
import com.venedental.constans.ConstansEmailforSpecialistKey;
import com.venedental.constans.ConstansPaymentReport;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.keyPatients.DateMinHistoricoOutput;
import com.venedental.dto.keyPatients.HeadboardEmailOutput;
import com.venedental.dto.keyPatients.HistoricalPatientOutput;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import com.venedental.dto.keyPatients.ReadTreatmentsEmailOutput;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import com.venedental.dto.managementSpecialist.*;
import com.venedental.dto.medicalOffice.FindMedicalOfficeBySepcialistOutput;
import com.venedental.enums.RolUser;
import com.venedental.facade.RolUsersFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.MedicalOffice;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServiceSpecialists;
import com.venedental.model.Specialists;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceManagementSpecialist;
import com.venedental.services.ServiceMedicalOffice;
import com.venedental.services.ServicePlansPatients;
import com.venedental.services.ServiceReportDentalTreatments;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author akdesk01
 */
@SessionScoped
@ApplicationPath("/resources")
@Path("keysAttentionSpecialist")
public class keysAttentionSpecialist extends Application {
     public static String EMAIL_VENEDEN_CLAVE_ESPECIALISTA="backupclavespecialista@yahoo.com";

    private List<FindRolUserOutput> findRolUserOutput;

    private List<FindDataSpecialistOutput> findDataSpecialistOutput;

    private List<FindDataSpecialistOutput> findDataSpecialistOutputListMedical;

    private List<RegisterKeyOutput> registerKeyOutputList;

    private List<KeysDetailsOutput> keysDetailsOutputList;

    private List<DateMinHistoricoOutput> dateMinHistoricoOutput;

    private List<HistoricalPatientOutput> historicalPatientOutputList;
    
    private List<HeadboardEmailOutput> headboardEmailOutput;
    private List<ReadTreatmentsEmailOutput> readTreatmentsEmailOutput;
    
    
    
    private List<ConsultHistorialOutput> consultHistorialOutput;

    private FindDataSpecialistOutput findDataSpecialist;

    private RegisterKeyOutput registerKeyOutput;

    private KeysDetailsOutput keysDetailsOutput;

    private HistoricalPatientOutput historicalPatientOutput;

    private FindDataSpecialistOutput especialista = null;

    private List<FindMedicalOfficeBySepcialistOutput> findMedicalOfficeBySepcialistOutput;
    private List<MedicalOffice> medicalOfficeList;
    private List<FilterPlanPatientOutput> filterPlanPatientOutputList;
    private List<String> readTreatmentsEmailOutputEmail;
    private  List<String>  readTreatmentsEmailOutputEmailColum1;
    private  List<String>  readTreatmentsEmailOutputEmailColum2;
    private String lisatNameTreatmentsEmailTradicional;
    private String listaReadTreatmentsColum1;
    private String  listaReadTreatmentsColum2;
    
    private Date expirationDate;
    private Date dateOfBirthPatient;

    private boolean rolSpecialista = false;
    private boolean medicalOficceSelection;
    private Integer idSpeciality, idUser, idSpecialist, idPlan, idMedicalO, idPatient, idSpecialistSelect, idPatientSelect, idPlanSelect, idMedicalSelect, idCompanySelect;
    private Integer medicalOfficeVarious;
    private String dateKeyDetails;
    private Date minDateHistorico;
     private Integer typeEmail;

    @EJB
    private ServiceCreateUser serviceCreateUser;

    @EJB
    private ServiceReportDentalTreatments serviceReportDentalTreatments;

    @EJB
    private ServiceKeysPatients serviceKeysPatients;
    
    @EJB
    private ServiceManagementSpecialist serviceManagementSpecialist;

    @EJB
    private ServiceSpecialists serviceSpecialists;

    @EJB
    private ServicePlansPatients servicePlansPatients;

    @EJB
    private UsersFacadeLocal usersFacadeLocal;

    @EJB
    private ServiceMedicalOffice serviceMedicalOffice;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @EJB
    private RolUsersFacadeLocal rolUsersFacadeLocal;

    @EJB
    private MailSenderLocal mailSenderLocal;

    public List<FindRolUserOutput> getFindRolUserOutput() {
        return findRolUserOutput;
    }

    public void setFindRolUserOutput(List<FindRolUserOutput> findRolUserOutput) {
        this.findRolUserOutput = findRolUserOutput;
    }

    public boolean isRolSpecialista() {
        return rolSpecialista;
    }

    public void setRolSpecialista(boolean rolSpecialista) {
        this.rolSpecialista = rolSpecialista;
    }

    public List<FindDataSpecialistOutput> getFindDataSpecialistOutput() {
        return findDataSpecialistOutput;
    }

    public void setFindDataSpecialistOutput(List<FindDataSpecialistOutput> findDataSpecialistOutput) {
        this.findDataSpecialistOutput = findDataSpecialistOutput;
    }

    public Integer getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(Integer idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public FindDataSpecialistOutput getFindDataSpecialist() {
        return findDataSpecialist;
    }

    public void setFindDataSpecialist(FindDataSpecialistOutput findDataSpecialist) {
        this.findDataSpecialist = findDataSpecialist;
    }

    public List<MedicalOffice> getMedicalOfficeList() {
        return medicalOfficeList;
    }

    public void setMedicalOfficeList(List<MedicalOffice> medicalOfficeList) {
        this.medicalOfficeList = medicalOfficeList;
    }

    public boolean isMedicalOficceSelection() {
        return medicalOficceSelection;
    }

    public void setMedicalOficceSelection(boolean medicalOficceSelection) {
        this.medicalOficceSelection = medicalOficceSelection;
    }

    public FindDataSpecialistOutput getEspecialista() {
        return especialista;
    }

    public void setEspecialista(FindDataSpecialistOutput especialista) {
        this.especialista = especialista;
    }

    public RegisterKeyOutput getRegisterKeyOutput() {
        return registerKeyOutput;
    }

    public void setRegisterKeyOutput(RegisterKeyOutput registerKeyOutput) {
        this.registerKeyOutput = registerKeyOutput;
    }

    public List<RegisterKeyOutput> getRegisterKeyOutputList() {
        return registerKeyOutputList;
    }

    public void setRegisterKeyOutputList(List<RegisterKeyOutput> registerKeyOutputList) {
        this.registerKeyOutputList = registerKeyOutputList;
    }

    public KeysDetailsOutput getKeysDetailsOutput() {
        return keysDetailsOutput;
    }

    public void setKeysDetailsOutput(KeysDetailsOutput keysDetailsOutput) {
        this.keysDetailsOutput = keysDetailsOutput;
    }

    public String getDateKeyDetails() {
        return dateKeyDetails;
    }

    public void setDateKeyDetails(String dateKeyDetails) {
        this.dateKeyDetails = dateKeyDetails;
    }

    public List<KeysDetailsOutput> getKeysDetailsOutputList() {
        return keysDetailsOutputList;
    }

    public void setKeysDetailsOutputList(List<KeysDetailsOutput> keysDetailsOutputList) {
        this.keysDetailsOutputList = keysDetailsOutputList;
    }

    public List<DateMinHistoricoOutput> getDateMinHistoricoOutput() {
        return dateMinHistoricoOutput;
    }

    public void setDateMinHistoricoOutput(List<DateMinHistoricoOutput> dateMinHistoricoOutput) {
        this.dateMinHistoricoOutput = dateMinHistoricoOutput;
    }

    public List<HistoricalPatientOutput> getHistoricalPatientOutputList() {
        return historicalPatientOutputList;
    }

    public void setHistoricalPatientOutputList(List<HistoricalPatientOutput> historicalPatientOutputList) {
        this.historicalPatientOutputList = historicalPatientOutputList;
    }

    public List<FindMedicalOfficeBySepcialistOutput> getFindMedicalOfficeBySepcialistOutput() {
        return findMedicalOfficeBySepcialistOutput;
    }

    public void setFindMedicalOfficeBySepcialistOutput(List<FindMedicalOfficeBySepcialistOutput> findMedicalOfficeBySepcialistOutput) {
        this.findMedicalOfficeBySepcialistOutput = findMedicalOfficeBySepcialistOutput;
    }

    public HistoricalPatientOutput getHistoricalPatientOutput() {
        return historicalPatientOutput;
    }

    public void setHistoricalPatientOutput(HistoricalPatientOutput historicalPatientOutput) {
        this.historicalPatientOutput = historicalPatientOutput;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public Integer getIdMedicalO() {
        return idMedicalO;
    }

    public void setIdMedicalO(Integer idMedicalO) {
        this.idMedicalO = idMedicalO;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Date getMinDateHistorico() {
        return minDateHistorico;
    }

    public void setMinDateHistorico(Date minDateHistorico) {
        this.minDateHistorico = minDateHistorico;
    }

    public Integer getIdSpecialistSelect() {
        return idSpecialistSelect;
    }

    public void setIdSpecialistSelect(Integer idSpecialistSelect) {
        this.idSpecialistSelect = idSpecialistSelect;
    }

    public Integer getIdPatientSelect() {
        return idPatientSelect;
    }

    public void setIdPatientSelect(Integer idPatientSelect) {
        this.idPatientSelect = idPatientSelect;
    }

    public Integer getIdPlanSelect() {
        return idPlanSelect;
    }

    public void setIdPlanSelect(Integer idPlanSelect) {
        this.idPlanSelect = idPlanSelect;
    }

    public Integer getIdMedicalSelect() {
        return idMedicalSelect;
    }

    public void setIdMedicalSelect(Integer idMedicalSelect) {
        this.idMedicalSelect = idMedicalSelect;
    }

    public Integer getIdCompanySelect() {
        return idCompanySelect;
    }

    public void setIdCompanySelect(Integer idCompanySelect) {
        this.idCompanySelect = idCompanySelect;
    }

    public Integer getMedicalOfficeVarious() {
        return medicalOfficeVarious;
    }

    public void setMedicalOfficeVarious(Integer medicalOfficeVarious) {
        this.medicalOfficeVarious = medicalOfficeVarious;
    }

    public List<FilterPlanPatientOutput> getFilterPlanPatientOutputList() {
        return filterPlanPatientOutputList;
    }

    public void setFilterPlanPatientOutputList(List<FilterPlanPatientOutput> filterPlanPatientOutputList) {
        this.filterPlanPatientOutputList = filterPlanPatientOutputList;
    }

    public List<ConsultHistorialOutput> getConsultHistorialOutput() {
        return consultHistorialOutput;
    }

    public void setConsultHistorialOutput(List<ConsultHistorialOutput> consultHistorialOutput) {
        this.consultHistorialOutput = consultHistorialOutput;
    }

    public List<HeadboardEmailOutput> getHeadboardEmailOutput() {
        return headboardEmailOutput;
    }

    public void setHeadboardEmailOutput(List<HeadboardEmailOutput> headboardEmailOutput) {
        this.headboardEmailOutput = headboardEmailOutput;
    }

    public List<ReadTreatmentsEmailOutput> getReadTreatmentsEmailOutput() {
        return readTreatmentsEmailOutput;
    }

    public void setReadTreatmentsEmailOutput(List<ReadTreatmentsEmailOutput> readTreatmentsEmailOutput) {
        this.readTreatmentsEmailOutput = readTreatmentsEmailOutput;
    }

    public Integer getTypeEmail() {
        return typeEmail;
    }

    public void setTypeEmail(Integer typeEmail) {
        this.typeEmail = typeEmail;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getDateOfBirthPatient() {
        return dateOfBirthPatient;
    }

    public void setDateOfBirthPatient(Date dateOfBirthPatient) {
        this.dateOfBirthPatient = dateOfBirthPatient;
    }

    public List<String> getReadTreatmentsEmailOutputEmail() {
        return readTreatmentsEmailOutputEmail;
    }

    public void setReadTreatmentsEmailOutputEmail(List<String> readTreatmentsEmailOutputEmail) {
        this.readTreatmentsEmailOutputEmail = readTreatmentsEmailOutputEmail;
    }

    public List<String> getReadTreatmentsEmailOutputEmailColum1() {
        return readTreatmentsEmailOutputEmailColum1;
    }

    public void setReadTreatmentsEmailOutputEmailColum1(List<String> readTreatmentsEmailOutputEmailColum1) {
        this.readTreatmentsEmailOutputEmailColum1 = readTreatmentsEmailOutputEmailColum1;
    }

    public List<String> getReadTreatmentsEmailOutputEmailColum2() {
        return readTreatmentsEmailOutputEmailColum2;
    }

    public void setReadTreatmentsEmailOutputEmailColum2(List<String> readTreatmentsEmailOutputEmailColum2) {
        this.readTreatmentsEmailOutputEmailColum2 = readTreatmentsEmailOutputEmailColum2;
    }

    public String getLisatNameTreatmentsEmailTradicional() {
        return lisatNameTreatmentsEmailTradicional;
    }

    public void setLisatNameTreatmentsEmailTradicional(String lisatNameTreatmentsEmailTradicional) {
        this.lisatNameTreatmentsEmailTradicional = lisatNameTreatmentsEmailTradicional;
    }

    public String getListaReadTreatmentsColum1() {
        return listaReadTreatmentsColum1;
    }

    public void setListaReadTreatmentsColum1(String listaReadTreatmentsColum1) {
        this.listaReadTreatmentsColum1 = listaReadTreatmentsColum1;
    }

    public String getListaReadTreatmentsColum2() {
        return listaReadTreatmentsColum2;
    }

    public void setListaReadTreatmentsColum2(String listaReadTreatmentsColum2) {
        this.listaReadTreatmentsColum2 = listaReadTreatmentsColum2;
    }
    
    
    
    
    
    
    
    
    

    
    
    
    

    @Path("findUserRolSpecialist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<FindRolUserOutput> findUserRolSpecialist(@QueryParam("idUser") Integer idUser) {
        this.setFindRolUserOutput(this.serviceSpecialists.findRolByUser(idUser));
        for (FindRolUserOutput findRolUserOutput : this.findRolUserOutput) {
            if (findRolUserOutput.getIdRol().equals(RolUser.ROLE_ESPECIALISTA.getValor())) {
                this.setRolSpecialista(true);
            }

        }
        return this.findRolUserOutput;
    }

    @Path("findDataSpecialist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<FindDataSpecialistOutput> findDataForSpecialist() {

        this.obtenerUsuario();
        this.findUserRolSpecialist(this.getIdUser());
        Integer idUserSpecialist = this.getIdUser();
//        FindDataSpecialistOutput especialistaInformacion = new FindDataSpecialistOutput();
        if (this.isRolSpecialista() == true) {
            this.setFindDataSpecialistOutput(serviceSpecialists.findDataSpecialist(this.getIdUser()));
            Integer cantidadRegistro = this.findDataSpecialistOutput.size();
            this.getFindDataSpecialistOutput();

            for (FindDataSpecialistOutput findDataSpecialistOutput : this.findDataSpecialistOutput) {
                this.idSpeciality = findDataSpecialistOutput.getIdSpeciality();
                this.setIdSpecialist(findDataSpecialistOutput.getId_Specialist());
                this.setIdSpecialistSelect(findDataSpecialistOutput.getId_Specialist());
                this.setIdUser(idUserSpecialist);

                if (cantidadRegistro > 1) {

                    findDataSpecialistOutput.setMedicalOfficeVarious(1);

                } else {
                    findDataSpecialistOutput.setMedicalOfficeVarious(0);
                }
            }
        }

        return findDataSpecialistOutput;
    }

    @Path("obtenerUsuario")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Integer obtenerUsuario() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());
        this.setIdUser(user.getId());
        return idUser;

    }

    @Path("RegistrarClave")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public KeysDetailsOutput registerKey(@QueryParam("idPatientSelect") Integer idPatient, @QueryParam("idEspecialista") Integer idSpecialist, @QueryParam("idMedicalSelect") Integer idMedicalSelect,
            @QueryParam("idCompanySelect") Integer idCompanySelect, @QueryParam("idPlanSelect") Integer idPlanSelect) throws IOException, ParseException {
        if (idCompanySelect == -1) {
            idCompanySelect = 0;
        } else {
            idCompanySelect = idCompanySelect;
        }
        String numberKey = this.serviceManagementSpecialist.generateNumber(idPlanSelect);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
        Integer idUser = user.getId();

        this.setRegisterKeyOutputList(this.serviceManagementSpecialist.registerKey(numberKey, null, idPatient, idSpecialist, idUser, idMedicalSelect, idCompanySelect, idPlanSelect));
        FindDataSpecialistOutput espe = findDataForSpecialist().get(0);
        for (RegisterKeyOutput registerKeyOutput : this.getRegisterKeyOutputList()) {
            Integer idKey = registerKeyOutput.getIdKey();
            this.setKeysDetailsOutput(this.serviceManagementSpecialist.findDetails(idKey));

//            for (FindDataSpecialistOutput findDataSpecialistOutput : this.findDataSpecialistOutput) {
//                String emailSpecialist = findDataSpecialistOutput.getEmail_especialista();
//
//                if (emailSpecialist.isEmpty()) {
//
//                    keysDetailsOutput.setEmailSpecialist(0);
//
//                } else {
//                    keysDetailsOutput.setEmailSpecialist(1);
//                }
//            }
            String nameReport = "RP__";
            System.out.println("Email");
            System.out.println(keysDetailsOutput);
             this.setHeadboardEmailOutput(this.serviceKeysPatients.findCabeceraEmail(idKey));
             for (HeadboardEmailOutput headboardEmailOutput : this.getHeadboardEmailOutput()) {
             this.setTypeEmail(headboardEmailOutput.getTypePlan());
             this.setExpirationDate(headboardEmailOutput.getExpirationDate());
             this.setDateOfBirthPatient(headboardEmailOutput.getBirthdate());
             if((headboardEmailOutput.getTypePlan()==0)||(headboardEmailOutput.getTypePlan()==1)){
               sendMailSpecialist(findFileEmailTradicional(nameReport), keysDetailsOutput,this.getTypeEmail(),headboardEmailOutput);  
             }else if(headboardEmailOutput.getTypePlan()==2){
                 sendMailSpecialist(findFileEmailCondicionado(nameReport), keysDetailsOutput,this.getTypeEmail(),headboardEmailOutput);
             }
            
             }
//            String baseDate = this.getKeysDetailsOutput().getDate();
//            String day = baseDate.substring(8, 10);
//            String month = baseDate.substring(5, 7);
//            String year = baseDate.substring(0, 4);
//            String hrs = baseDate.substring(11, 19);
        }
        String dateKeyCreate = new SimpleDateFormat("dd/MM/yyyy").format(keysDetailsOutput.getApplicationDate());
        String dateExpirationCreate = new SimpleDateFormat("dd/MM/yyyy").format(keysDetailsOutput.getExpirationDate());
        String dateBrithayCreate = new SimpleDateFormat("dd/MM/yyyy").format(keysDetailsOutput.getDateOfBirthPatient());
        keysDetailsOutput.setDateKeyDetail(dateKeyCreate);
        keysDetailsOutput.setDateExpirationDetail(dateExpirationCreate);
        keysDetailsOutput.setDateBrithayDetail(dateBrithayCreate);
        keysDetailsOutput.setEmailSpecialist(espe.getEmail_especialista().isEmpty()?0:1);

        return keysDetailsOutput;

    }

    @Path("BuscarHistorial")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ConsultHistorialOutput> findHistoricalPatient(@QueryParam("idPatientSelect") Integer idPatientSelect, @QueryParam("idSpecialist") Integer idSpecialist) throws ParseException {
        System.out.println("historial patient" + idPatientSelect);
        System.out.println("historial specialist" + idSpecialist);
//        this.setDateMinHistoricoOutput(this.serviceKeysPatients.findDateMinHistorico(idPatient,idSpecialist));

//        this.setHistoricalPatientOutputList(this.serviceKeysPatients.findHistoricalPatientRegister(idPatientSelect, idSpecialist));
        this.setConsultHistorialOutput(this.serviceManagementSpecialist.findDateHistorialFilter(idPatientSelect, idSpecialist));
            for (ConsultHistorialOutput consultHistorialOutput : this.getConsultHistorialOutput()) {
                String fecha = new SimpleDateFormat("dd/MM/yyyy").format(consultHistorialOutput.getDateApplication());
            java.util.Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            consultHistorialOutput.setDateForHistorial(fecha);
             String fechaMinFilter = new SimpleDateFormat("dd/MM/yyyy").format(consultHistorialOutput.getDateMinFilter());
              String fechaMaxFilter = new SimpleDateFormat("dd/MM/yyyy").format(consultHistorialOutput.getDateMaxFilter());
              consultHistorialOutput.setDateEndFilter(fechaMaxFilter);    
            consultHistorialOutput.setDateStartFilter(fechaMinFilter);
            
           
        }
        return consultHistorialOutput;
    }

    @Path("BuscarConsultorios")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MedicalOffice> findMedicalOffice(@QueryParam("idSpecialist") Integer idSpecialist) {
        this.setMedicalOfficeList(this.serviceMedicalOffice.findByIdSpecialist(idSpecialist));
        if (this.getMedicalOfficeList().size() == 1) {
            this.setMedicalOfficeVarious(1);

        } else {

            this.setMedicalOfficeVarious(0);
        }
        return this.medicalOfficeList;
    }

    /**
     * Method to send an email specialty
     *
     * @param files

     * @param key
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailSpecialist(List<File> files, KeysDetailsOutput key, Integer typeEmail, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
        String emailSpecialist;
         String subjectMessage="";
        String title="";
         String contentMessageEspecialist = null;
        findDataForSpecialist();
        for (FindDataSpecialistOutput findDataSpecialistOutput : this.findDataSpecialistOutput) {
            this.idSpeciality = findDataSpecialistOutput.getIdSpeciality();
            this.setIdSpecialist(findDataSpecialistOutput.getId_Specialist());
            emailSpecialist = findDataSpecialistOutput.getEmail_especialista();

            log.log(Level.INFO, "Inicio - Proceso de envio de correos por especialista: {0}", findDataSpecialistOutput.getNameSpecialist());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            FacesContext contexto = FacesContext.getCurrentInstance();
//        this.getKeysPatientsRegisterViewBean().findDetailsKey(key.getIdKey());
            String namePatient = key.getNamePatient();

            if (emailSpecialist != null) {
                 if((typeEmail==0)||(typeEmail==1)){
                title = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_TITLE_PROPERTY_);
           subjectMessage = title + " " + " - Paciente:" + " " + namePatient;
               contentMessageEspecialist = buildMessageSpecialistTradicional(findDataSpecialistOutput, key,headboardEmailOutput);
            }else if(typeEmail==2){
                title = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_TITLE_PROPERTY_);
           subjectMessage = title + " " + " - Paciente:" + " " + namePatient;
                 contentMessageEspecialist = buildMessageSpecialistCondicionado(findDataSpecialistOutput, key,headboardEmailOutput);
            }
                
              
                this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files, findDataSpecialistOutput.getEmail_especialista());
                this.mailSenderLocal.send(Email.MAIL_REPORTE_PAGO, subjectMessage, title, contentMessageEspecialist, files, EMAIL_VENEDEN_CLAVE_ESPECIALISTA);
//            this.getKeysPatientsRegisterViewBean().setLabelMailMessageFailed(false);
//            this.getKeysPatientsRegisterViewBean().setLabelSuccessfulMailMessage(true);
//            RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");
            } else {
//            this.getKeysPatientsRegisterViewBean().setLabelMailMessageFailed(true);
//            this.getKeysPatientsRegisterViewBean().setLabelSuccessfulMailMessage(false);
//            RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");

                log.log(Level.INFO, "El especialista no tiene un correo registrado");
            }
            log.log(Level.INFO, "Fin - Proceso de envio de correos por especilista");

        }
    }

    /**
     * Method to seek payment report
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFile(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_PATH_REPORT);
        String reportExtension = readFileProperties().getProperty(ConstansPaymentReport.PROPERTY_REPORT_EXTENSION);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }
    
    
    
    /**
     * Method to seek payment report email condicionado
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFileEmailCondicionado(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTIES_PATH);
        String reportExtension = readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTIES_PATH);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    
     /**
     * Method to seek payment report email tradicional
     *
     * @param nameReport
     * @return List File
     * @throws IOException
     */
    public List<File> findFileEmailTradicional(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTIES_PATH);
        String reportExtension = readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTIES_PATH);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailforSpecialistKey.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailforSpecialistKey.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }
    
    
     /**
     * Method to read the properties file constants payment reports email condicionado
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFilePropertiesEmailCondicionado() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailCondicionado.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailCondicionado.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }
    
    /**
     * Method to read the properties file constants payment reports email tradicional
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFilePropertiesEmailTradicional() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansEmailTradicional.FILENAME);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansEmailTradicional.FILENAME
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }
    

//    /**
//     * Method to construct the message body for the email that will be sent to
//     * specialist
//     *
//
//     * @return String
//     * @throws ParseException
//     * @throws IOException
//     */
//    private String buildMessageSpecialist(FindDataSpecialistOutput findDataSpecialistOutput, KeysDetailsOutput key) throws ParseException, IOException {
//        this.setKeysDetailsOutput(this.serviceManagementSpecialist.findDetails(key.getId()));
//        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
//        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
//        String expirationDateForKey = sdfSolicitudD.format(getKeysDetailsOutput().getExpirationDate());
//        String dateOfBirth = sdfSolicitudD.format(getKeysDetailsOutput().getDateOfBirthPatient());
//        String dateClave = sdfSolicitudD.format(getKeysDetailsOutput().getApplicationDate());
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("Estimado Doctor(a). ").append(findDataSpecialistOutput.getNameSpecialist()).append(",<br><br>");
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT3)).append(getKeysDetailsOutput().getNumber());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT4)).append(dateClave);
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT5)).append(expirationDateForKey);
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT6)).append(getKeysDetailsOutput().getNamePatient());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT7)).append(getKeysDetailsOutput().getIdentityNumberForPatient());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT8)).append(dateOfBirth);
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT9)).append(getKeysDetailsOutput().getNameRelationShip());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT10)).append(getKeysDetailsOutput().getNameInsurances());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT11)).append(getKeysDetailsOutput().getNamePlan());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT12)).append(getKeysDetailsOutput().getEnte());
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT13));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT14)).append(getKeysDetailsOutput().getTreatmentsForPlan()).append(".");
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT15));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT16));
//        stringBuilder.append(readFileProperties().getProperty(ConstansEmailforSpecialistKey.PROPERTY_EMAIL_SPECIALIST_CONTENT17));
//        return stringBuilder.toString();
//    }
    
    
    
     /**
     * Method to construct the message body for the email that will be sent to specialist(condicionado, limitado o ilimitado)
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialistCondicionado(FindDataSpecialistOutput findDataSpecialistOutput, KeysDetailsOutput key, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
        Integer idPlan=headboardEmailOutput.getIdPlan();
        this.readTreatmentsEmailOutputEmailColum1=new ArrayList<String>();
        this.readTreatmentsEmailOutputEmailColum2=new ArrayList<String>();
         this.listaReadTreatmentsColum1="";
         this.listaReadTreatmentsColum2="";
        
        this.setReadTreatmentsEmailOutput(this.serviceKeysPatients.findTreatmentsEmail(idPlan));
       
       for (ReadTreatmentsEmailOutput readTreatmentsEmailOutput : this.getReadTreatmentsEmailOutput()) {
           if(readTreatmentsEmailOutput.getIdColumn()==1){
               this.readTreatmentsEmailOutputEmailColum1.add(readTreatmentsEmailOutput.getLineTreatments().toString());
               this.listaReadTreatmentsColum1=this.readTreatmentsEmailOutputEmailColum1.toString();
           }else if(readTreatmentsEmailOutput.getIdColumn()==2){
               this.readTreatmentsEmailOutputEmailColum2.add(readTreatmentsEmailOutput.getLineTreatments().toString());
               this.listaReadTreatmentsColum2=this.readTreatmentsEmailOutputEmailColum2.toString();
                       
           }
           
           
       }
        this.listaReadTreatmentsColum1=this.listaReadTreatmentsColum1.replace(".,",".<br><br>" );
        this.listaReadTreatmentsColum2=this.listaReadTreatmentsColum2.replace(".,",".<br><br>" );
       
        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String expirationDateForKey = sdfSolicitudD.format(headboardEmailOutput.getExpirationDate());
        String dateOfBirth = sdfSolicitudD.format(headboardEmailOutput.getBirthdate());
         String dateOfRequest=sdfSolicitudD.format(headboardEmailOutput.getRequestdate());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Doctor(a). ").append(findDataSpecialistOutput.getNameSpecialist()).append(",<br><br>");
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT2));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICONADO_CONTENT3)).append(headboardEmailOutput.getNumberKey());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT4)).append(dateOfRequest);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT5)).append(expirationDateForKey);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT6)).append(headboardEmailOutput.getNamePatient());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT7)).append(headboardEmailOutput.getIdentityNumberPatient());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT8)).append(dateOfBirth);
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT9)).append(headboardEmailOutput.getNameRelationShip());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT10)).append(headboardEmailOutput.getNameInsurance());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT11)).append(headboardEmailOutput.getNamePlan());
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT12)).append(headboardEmailOutput.getNameEntity());
       stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT13));
        for(String readTreatmentsEmailOutputEmailColum1 : this.readTreatmentsEmailOutputEmailColum1) {
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT14)).append(readTreatmentsEmailOutputEmailColum1).append("<br><br>");
    }
        
       
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT15));
        for(String readTreatmentsEmailOutputEmailColum2 : this.readTreatmentsEmailOutputEmailColum2) {
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT16)).append(readTreatmentsEmailOutputEmailColum2).append("<br><br>");
    }
        
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT17));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT18));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT19));
        stringBuilder.append(readFilePropertiesEmailCondicionado().getProperty(ConstansEmailCondicionado.PROPERTY_CONDICIONADO_CONTENT20));
        return stringBuilder.toString();
    }

    
    
    /**
     * Method to construct the message body for the email that will be sent to specialist(tradicional)
     *
     * @param specialist
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageSpecialistTradicional(FindDataSpecialistOutput findDataSpecialistOutput, KeysDetailsOutput key, HeadboardEmailOutput headboardEmailOutput) throws ParseException, IOException {
      this.readTreatmentsEmailOutputEmail=new ArrayList<String>();
         Integer idPlan=headboardEmailOutput.getIdPlan();
        this.setReadTreatmentsEmailOutput(this.serviceKeysPatients.findTreatmentsEmail(headboardEmailOutput.getIdPlan()));
       for (ReadTreatmentsEmailOutput readTreatmentsEmailOutput : this.getReadTreatmentsEmailOutput()) {
           this.readTreatmentsEmailOutputEmail.add(readTreatmentsEmailOutput.getLineTreatments().toString());
           this.lisatNameTreatmentsEmailTradicional="";
           this.lisatNameTreatmentsEmailTradicional=this.readTreatmentsEmailOutputEmail.toString();
       }
       this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace(",","");
        this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace("[","");
        this.lisatNameTreatmentsEmailTradicional=this.lisatNameTreatmentsEmailTradicional.replace("]","");
        
        SimpleDateFormat sdfSolicitudD = new SimpleDateFormat("dd/MM/yyyy");
        sdfSolicitudD.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String expirationDateForKey = sdfSolicitudD.format(headboardEmailOutput.getExpirationDate());
        String dateOfBirth = sdfSolicitudD.format(headboardEmailOutput.getBirthdate());
         String dateOfRequest=sdfSolicitudD.format(headboardEmailOutput.getRequestdate());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Doctor(a). ").append(findDataSpecialistOutput.getNameSpecialist()).append(",<br><br>");
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT));
         stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT2));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT3)).append(headboardEmailOutput.getNumberKey());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT4)).append(dateOfRequest);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT5)).append(expirationDateForKey);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT6)).append(headboardEmailOutput.getNamePatient());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT7)).append(headboardEmailOutput.getIdentityNumberPatient());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT8)).append(dateOfBirth);
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT9)).append(headboardEmailOutput.getNameRelationShip());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT10)).append(headboardEmailOutput.getNameInsurance());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT11)).append(headboardEmailOutput.getNamePlan());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT12)).append(headboardEmailOutput.getNameEntity());
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT13));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT14)).append(this.lisatNameTreatmentsEmailTradicional).append(".");
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT15));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT16));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT17));
        stringBuilder.append(readFilePropertiesEmailTradicional().getProperty(ConstansEmailTradicional.PROPERTY_TRADICIONAL_CONTENT18));
        return stringBuilder.toString();
    }

    @Path("findPatient")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<FilterPlanPatientOutput> findPatient(@QueryParam("patient") String patient, @QueryParam("idEspecialidad") Integer idEspecialidad, @QueryParam("idSpecialist") Integer idSpecialist) throws ParseException {
//        findDataForSpecialist();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());
        Integer idUser = user.getId();
//        findDataForSpecialist(38);
        this.setFilterPlanPatientOutputList(servicePlansPatients.findByPatient(patient, idEspecialidad, idUser));
        for (FilterPlanPatientOutput filterPlanPatientOutput : this.filterPlanPatientOutputList) {
            String dateBrithaySelect = new SimpleDateFormat("dd/MM/yyyy").format(filterPlanPatientOutput.getBirthDatePatient());
            filterPlanPatientOutput.setBirthDatePatientSelect(dateBrithaySelect);
        }
        return filterPlanPatientOutputList;

    }
}
