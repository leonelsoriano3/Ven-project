package com.venedental.controller.session;

import com.venedental.controller.BaseBean;
import com.venedental.model.security.Users;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "usersSessionBean")
@SessionScoped
public class UsersSessionBean extends BaseBean {

    public UsersSessionBean() {
        this.autenticado = new Users();
    }

    private Users autenticado;

    public Users getAutenticado() {
        return autenticado;
    }

    public void setAutenticado(Users autenticado) {
        this.autenticado = autenticado;
    }
    
    
}
