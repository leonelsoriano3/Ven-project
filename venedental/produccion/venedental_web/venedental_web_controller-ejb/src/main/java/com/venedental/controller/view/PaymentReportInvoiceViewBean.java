/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.facade.PaymentReportFacadeLocal;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.StatusAnalysisKeysFacadeLocal;
import com.venedental.model.PaymentReport;
import com.venedental.model.Specialists;
import com.venedental.model.StatusAnalysisKeys;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "paymentReportInvoiceViewBean")
@ViewScoped
public class PaymentReportInvoiceViewBean {

    private final RequestContext context = RequestContext.getCurrentInstance();
    List<PaymentReport> paymentReport;
    List<PaymentReport> paymentReportFiltered;
    List<PaymentReport> paymentReportSelected;
    Specialists specialist;
    private Specialists selectedSpecialist;

    @EJB
    private SpecialistsFacadeLocal specialistsFacadeLocal;

    @EJB
    private PaymentReportFacadeLocal paymentReportFacadeLocal;

    @EJB
    private StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal;

    public List<PaymentReport> getPaymentReport() {        
        return paymentReport;
    }

    public void setPaymentReport(List<PaymentReport> paymentReport) {
        this.paymentReport = paymentReport;
    }

    public List<PaymentReport> getPaymentReportFiltered() {
        return paymentReportFiltered;
    }

    public void setPaymentReportFiltered(List<PaymentReport> paymentReportFiltered) {
        this.paymentReportFiltered = paymentReportFiltered;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public List<PaymentReport> getPaymentReportSelected() {
        return paymentReportSelected;
    }

    public void setPaymentReportSelected(List<PaymentReport> paymentReportSelected) {
        this.paymentReportSelected = paymentReportSelected;
    }

    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        List<Specialists> filteredSpecialists = this.specialistsFacadeLocal.findByIdentityNumberOrName(query);
        return filteredSpecialists;
    }

    /**
     * Method called when I select a specialist from the list
     *
     * @param event
     * @throws IOException
     */
    public void onItemSelect(SelectEvent event) throws IOException {
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        filterPaymentReport();
    }

    public void filterPaymentReport() {
        resetDataTablePaymentReport();
        StatusAnalysisKeys byInvoice = this.statusAnalysisKeysFacadeLocal.findById(EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor());        
        this.setPaymentReport(new ArrayList<PaymentReport>());        
        this.setPaymentReport(this.paymentReportFacadeLocal.filterBySpecialistReportStatusAndDateRange(2733, byInvoice.getId(), null, null));        
        RequestContext.getCurrentInstance().update("form:invoiceList");
    }
    
    public void filter(){
        this.getPaymentReport().removeAll(this.getPaymentReport());
        this.setPaymentReport(new ArrayList<PaymentReport>());
        StatusAnalysisKeys byInvoice = this.statusAnalysisKeysFacadeLocal.findById(EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor());
        this.setPaymentReport(this.paymentReportFacadeLocal.filterBySpecialistReportStatusAndDateRange(2733, byInvoice.getId(), null, null));        
        RequestContext.getCurrentInstance().update("form:invoiceList");
    
    }
    
    
    public void resetDataTablePaymentReport() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:invoiceList");
        if (dataTable != null) {
            dataTable.reset();
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().reset("form:invoiceList");
            RequestContext.getCurrentInstance().update("form:invoiceList");
        }
    }

}
