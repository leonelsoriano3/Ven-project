///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.venedental.controller.converter;
//
//import com.venedental.controller.application.BaremoApplicationBean;
//import com.venedental.model.Baremo;
//import javax.faces.application.FacesMessage;
//import javax.faces.component.UIComponent;
//import javax.faces.context.FacesContext;
//import javax.faces.convert.Converter;
//import javax.faces.convert.ConverterException;
//import javax.faces.convert.FacesConverter;
//
///**
// *
// * @author Usuario
// */
//@FacesConverter(value = "baremoConverter", forClass = Baremo.class)
//public class BaremoConverter implements Converter {
//
//    @Override
//    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
//
//        BaremoApplicationBean baremoApplicationBean = (BaremoApplicationBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "baremoApplicationBean");
//        
//        if (value.trim().equals("")) {
//            return null;
//        } else {
//            try {
//                int numero = Integer.parseInt(value);
//                for (Baremo b : baremoApplicationBean.getListBaremo()) {
//                    if (b.getId() == numero) {
//                        return b;
//                    }
//                }
//            } catch (NumberFormatException exception) {
//                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Baremo invalido"));
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public String getAsString(FacesContext context, UIComponent component, Object value) {
//
//        if (value == null || value.equals("")) {
//            return "";
//        } else {
//            return String.valueOf(((Baremo) value).getId());
//        }
//
//    }
//
//}
