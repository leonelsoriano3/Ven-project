/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base;

import controller.utils.CustomLogger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.utils.PropertiesConfiguration;

/**
 *
 * @author usuario
 */
public class Terminal {

    private static final Logger log = CustomLogger.getLogger(Terminal.class.getName());

    public static void exec(String instruction) {
        try {
            StringBuilder build = new StringBuilder();
            build.append("echo ");
            build.append(PropertiesConfiguration.get(Constants.PASSWORD_SUDO));
            build.append("| ");
            build.append("sudo -S ");
            build.append(instruction);
            String[] instructions = {"/bin/bash", "-c", build.toString()};
            executeProcess(instructions);
        } catch (IOException | InterruptedException ex) {
            log.log(Level.WARNING, "Bash - Error creando el proceso");
        }
    }

    private static void executeProcess(String[] instructions) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder(instructions);
        Process process = processBuilder.start();
        process.waitFor();
    }

    private static void getInputStream(Process process) {
        try {
            String line;
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Error - PrintInput");
        }
    }

    private static void getErrorStream(Process process) {
        try {
            String line;
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Error - PrintError");
        }
    }

    private static void print(Process process) {
        getInputStream(process);
        getErrorStream(process);
    }

}
