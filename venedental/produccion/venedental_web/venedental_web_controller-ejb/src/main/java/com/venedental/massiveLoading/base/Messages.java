/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base;

/**
 *
 * @author usuario
 */
public class Messages {

    public static String ESTATUS_LOGGER_INVALIDO = "El estatus del logger es inválido, por favor revisar properties";

    public static String EXCEPTION_NON_EXISTENT_DIRECTORY = "El directorio actual no existe";
    public static String EXCEPTION_EMPTY_DIRECTORY = "No hay ficheros en el directorio especificado";
    public static String EXCEPTION_NOT_A_DIRECTORY = "No es un directorio";
    public static String EXCEPTION_NOT_A_EXCEL_FILE = "No es un archivo excel válido";
    public static String EXCEPTION_NOT_A_FILE = "No es un archivo";
    public static String EXCEPTION_READING_FILE = "Error al leer el archivo";
    public static String EXCEPTION_COLUMN_NUMBER = "Error al convertir el número de la columna";
    public static String EXCEPTION_READ = "Error de lectura";
    

    public static String EXCEPTION_DATA_PATIENT_INVALID = "Los datos del paciente son incorrectos";
    public static String EXCEPTION_PLANS_INVALID = "CÓDIGO DEL PLAN NO VÁLIDO";

    public static String MESSAGE_MERGED_PLANS = "Planes combinados";
    public static String MESSAGE_REPEATED_PLANS = "Plans repetidos";
    public static String MESSAGE_EMPTY_PLANS = "No tiene planes asociados";

    public static String MESSAGE_ID_INVALID = "CÉDULA BENEFICIARIO NO VÁLIDA";
    public static String MESSAGE_ID_HOLDER_INVALID = "CÉDULA TITULAR NO VÁLIDA";
    public static String MESSAGE_BIRTH_INVALID = "FECHA DE NACIMIENTO NO VÁLIDA";
    public static String MESSAGE_SEX_INVALID = "INICIAL DE SEXO NO VÁLIDA";
    public static String MESSAGE_COMPLETE_NAME_INVALID = "NOMBRE COMPLETO NO VÁLIDO";
    public static String MESSAGE_RELATIONSHIP_INVALID = "CÓDIGO DE PARENTESCO NO VÁLIDO";
    public static String MESSAGE_COMPANY_NAME_INVALID = "ENTE NO VÁLIDO";
    public static String MESSAGE_DATA_INVALID = "Los datos leídos en el registro son inválidos";

}
