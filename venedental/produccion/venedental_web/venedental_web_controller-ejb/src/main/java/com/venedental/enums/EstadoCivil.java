/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.enums;

/**
 *
 * @author CarlosDaniel
 */
public enum EstadoCivil {
    
    SOLTERO,
    CASADO,
    DIVORCIADO,
    VIUDO;
    
    private EstadoCivil(){
    }
        
}
