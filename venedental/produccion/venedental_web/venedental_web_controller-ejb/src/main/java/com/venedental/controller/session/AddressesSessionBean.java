package com.venedental.controller.session;

import com.venedental.controller.BaseBean;
import com.venedental.facade.AddressesFacadeLocal;
import com.venedental.model.Addresses;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "addressesSessionBean")
@SessionScoped
public class AddressesSessionBean extends BaseBean {

    @EJB
    private AddressesFacadeLocal addressesFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    public AddressesSessionBean() {
        this.getListAddresses().addAll(this.addressesFacadeLocal.findAll());
    }

    private List<Addresses> listAddresses;

    public List<Addresses> getListAddresses() {
        if (this.listAddresses == null) {
            this.listAddresses = new ArrayList<>();
        }
        return listAddresses;
    }

    public void setListAddresses(List<Addresses> listAddresses) {
        this.listAddresses = listAddresses;
    }
    
    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

}
