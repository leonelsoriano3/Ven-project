/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base.enums;

/**
 *
 * @author akdesk01
 */
public enum SubEFields {

    IDENTITY_NUMBER_HOLDER(0, "C.I titular",11),
    IDENTITY_NUMBER(1, "C.I beneficiario", 16),
    COMPLETE_NAME(2, "Nombre completo", 50),
    SEX(3, "Sexo", 4),
    BIRTHDAY(4, "Fecha nac.", 10),
    RELATIONSHIP(5, "Parentesco", 10),
    PLANS(6, "Planes", 14),
    COMPANY(7, "Ente", 50);

    private final Integer order;
    private final String name;
    private final Integer width;

    private SubEFields(Integer order, String name, Integer width) {
        this.order = order;
        this.name = name;
        this.width = width;
    }

    public Integer getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public Integer getWidth() {
        return width;
    }

}
