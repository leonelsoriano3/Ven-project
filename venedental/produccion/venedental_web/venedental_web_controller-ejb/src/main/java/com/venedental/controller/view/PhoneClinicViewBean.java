/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.model.PhoneClinic;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Usuario
 */
@ManagedBean(name = "phoneClinicViewBean")
@ViewScoped
public class PhoneClinicViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    private String[] newPhoneClinic;
    private String[] editPhoneClinic;
    private PhoneClinic selectedPhoneClinic;
    private List<PhoneClinic> listPhoneClinic;
    private List<PhoneClinic> newListPhoneClinic;
    private List<PhoneClinic> editListPhoneClinic;

    public PhoneClinicViewBean() {
        this.newPhoneClinic = new String[1];
        this.editPhoneClinic = new String[1];
    }

    public String[] getNewPhoneClinic() {
        return newPhoneClinic;
    }

    public void setNewPhoneClinic(String[] newPhoneClinic) {
        this.newPhoneClinic = newPhoneClinic;
    }

    public String[] getEditPhoneClinic() {
        return editPhoneClinic;
    }

    public void setEditPhoneClinic(String[] editPhoneClinic) {
        this.editPhoneClinic = editPhoneClinic;
    }

    public PhoneClinic getSelectedPhoneClinic() {
        return selectedPhoneClinic;
    }

    public void setSelectedPhoneClinic(PhoneClinic selectedPhoneClinic) {
        this.selectedPhoneClinic = selectedPhoneClinic;
    }

    public List<PhoneClinic> getListPhoneClinic() {
        if (this.listPhoneClinic == null) {
            this.listPhoneClinic = new ArrayList<>();
        }
        return listPhoneClinic;
    }

    public void setListPhoneClinic(List<PhoneClinic> listPhoneClinic) {
        this.listPhoneClinic = listPhoneClinic;
    }

    public List<PhoneClinic> getNewListPhoneClinic() {
        if (this.newListPhoneClinic == null) {
            this.newListPhoneClinic = new ArrayList<>();
            
        }
        return newListPhoneClinic;
    }

    public void setNewListPhoneClinic(List<PhoneClinic> newListPhoneClinic) {
        this.newListPhoneClinic = newListPhoneClinic;
    }

    public List<PhoneClinic> getEditListPhoneClinic() {
        if (this.editListPhoneClinic == null) {
            this.editListPhoneClinic = new ArrayList<>();
        }
        return editListPhoneClinic;
    }

    public void setEditListPhoneClinic(List<PhoneClinic> editListPhoneClinic) {
        this.editListPhoneClinic = editListPhoneClinic;
    }

}
