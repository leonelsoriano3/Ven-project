package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum TypeAttention {

    Diagnostico(1), Emergencia(2);

    Integer valor;

    private TypeAttention(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
