/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;
import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.facade.PropertiesPlansTreatmentsKeyFacadeLocal;
import com.venedental.model.Properties;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.PropertiesTreatments;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "propertiesPlansTreatmentsKeyViewBean")
@ViewScoped
public class PropertiesPlansTreatmentsKeyViewBean {
    
    @EJB
    private PropertiesPlansTreatmentsKeyFacadeLocal propertiesTreatmentsFacadeLocal;
    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKey;
    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFiltrada;
    private List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeySeleccionados;
    private List<PropertiesPlansTreatmentsKey> newListPropertiesPlansTreatmentsKey;
    private List<PropertiesPlansTreatmentsKey> editListPropertiesPlansTreatmentsKey;
    private List<PropertiesPlansTreatmentsKey> beforeListPropertiesPlansTreatmentsKey;
    private PropertiesPlansTreatmentsKey editPropertiesPlansTreatmentsKey;
    private PropertiesPlansTreatmentsKey selectedPropertiesPlansTreatmentsKey;
    private PropertiesPlansTreatmentsKey newPropertiesPlansTreatmentsKeyObj;
    private String[] newPropertiesPlansTreatmentsKey;
    private String color;
    private boolean visibleList;
    private boolean visiblePanel;
    private List<String> selectedOptions;
    private List<String> selectedOptionsI;
    private List<String> selectedOptionsII;
    private List<String> selectedOptionsIII;
    private List<String> selectedOptionsIV;
    private List<String> selectedOptionsV;
    private List<String> selectedOptionsVI;
    private List<String> selectedOptionsVII;
    private List<String> selectedOptionsVIII;
    
    private List<PropertiesPlansTreatmentsKey> selectedEditOptions;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsI;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsII;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsIII;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsIV;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsV;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsVI;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsVII;
    private List<PropertiesPlansTreatmentsKey> selectedEditOptionsVIII;
    
    

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    @ManagedProperty(value = "#{propetiesViewBean}")
    private PropertiesViewBean propetiesViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;

    
    public PropertiesPlansTreatmentsKeyViewBean() {
        this.editPropertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        //this.selectedPropertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.newPropertiesPlansTreatmentsKey = new String[4];
        this.beforeListPropertiesPlansTreatmentsKey = new ArrayList<>();
        this.visibleList = false;
        
    }
    
    @PostConstruct
    public void init() {
    
        addTreatments();
       // this.fillListGeneric();
    
    }
    
    public boolean isVisiblePanel() {
        return visiblePanel;
    }

    public void setVisiblePanel(boolean visiblePanel) {
        this.visiblePanel = visiblePanel;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptions() {
        if (this.selectedEditOptions == null) {
            this.selectedEditOptions = new ArrayList<>();
        }
        return selectedEditOptions;
    }

    public void setSelectedEditOptions(List<PropertiesPlansTreatmentsKey> selectedEditOptions) {
        this.selectedEditOptions = selectedEditOptions;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsI() {
        if (this.selectedEditOptionsI == null) {
            this.selectedEditOptionsI = new ArrayList<>();
        }
        return selectedEditOptionsI;
    }

    public void setSelectedEditOptionsI(List<PropertiesPlansTreatmentsKey> selectedEditOptionsI) {
        this.selectedEditOptionsI = selectedEditOptionsI;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsII() {
        if (this.selectedEditOptionsII == null) {
            this.selectedEditOptionsII = new ArrayList<>();
        }
        return selectedEditOptionsII;
    }

    public void setSelectedEditOptionsII(List<PropertiesPlansTreatmentsKey> selectedEditOptionsII) {
        this.selectedEditOptionsII = selectedEditOptionsII;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsIII() {
        if (this.selectedEditOptionsIII == null) {
            this.selectedEditOptionsIII = new ArrayList<>();
        }
        return selectedEditOptionsIII;
    }

    public void setSelectedEditOptionsIII(List<PropertiesPlansTreatmentsKey> selectedEditOptionsIII) {
        this.selectedEditOptionsIII = selectedEditOptionsIII;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsIV() {
        if (this.selectedEditOptionsIV == null) {
            this.selectedEditOptionsIV = new ArrayList<>();
        }
        return selectedEditOptionsIV;
    }

    public void setSelectedEditOptionsIV(List<PropertiesPlansTreatmentsKey> selectedEditOptionsIV) {
        this.selectedEditOptionsIV = selectedEditOptionsIV;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsV() {
        if (this.selectedEditOptionsV == null) {
            this.selectedEditOptionsV = new ArrayList<>();
        }
        return selectedEditOptionsV;
    }

    public void setSelectedEditOptionsV(List<PropertiesPlansTreatmentsKey> selectedEditOptionsV) {
        this.selectedEditOptionsV = selectedEditOptionsV;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsVI() {
        if (this.selectedEditOptionsVI == null) {
            this.selectedEditOptionsVI = new ArrayList<>();
        }
        return selectedEditOptionsVI;
    }

    public void setSelectedEditOptionsVI(List<PropertiesPlansTreatmentsKey> selectedEditOptionsVI) {
        this.selectedEditOptionsVI = selectedEditOptionsVI;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsVII() {
        if (this.selectedEditOptionsVII == null) {
            this.selectedEditOptionsVII = new ArrayList<>();
        }
        return selectedEditOptionsVII;
    }

    public void setSelectedEditOptionsVII(List<PropertiesPlansTreatmentsKey> selectedEditOptionsVII) {
        this.selectedEditOptionsVII = selectedEditOptionsVII;
    }

    public List<PropertiesPlansTreatmentsKey> getSelectedEditOptionsVIII() {
        if (this.selectedEditOptionsVIII == null) {
            this.selectedEditOptionsVIII = new ArrayList<>();
        }
        return selectedEditOptionsVIII;
    }

    public void setSelectedEditOptionsVIII(List<PropertiesPlansTreatmentsKey> selectedEditOptionsVIII) {
        this.selectedEditOptionsVIII = selectedEditOptionsVIII;
    }

   
    public List<String> getSelectedOptions() {
        if (this.selectedOptions == null) {
            this.selectedOptions = new ArrayList<>();
        }
        return selectedOptions;
    }

    public void setSelectedOptions(List<String> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public List<String> getSelectedOptionsI() {
         if (this.selectedOptionsI == null) {
            this.selectedOptionsI = new ArrayList<>();
        }
        return selectedOptionsI;
    }

    public void setSelectedOptionsI(List<String> selectedOptionsI) {
        this.selectedOptionsI = selectedOptionsI;
    }

    public List<String> getSelectedOptionsII() {
         if (this.selectedOptionsII == null) {
            this.selectedOptionsII = new ArrayList<>();
        }
        return selectedOptionsII;
    }

    public void setSelectedOptionsII(List<String> selectedOptionsII) {
        this.selectedOptionsII = selectedOptionsII;
    }

    public List<String> getSelectedOptionsIII() {
         if (this.selectedOptionsIII == null) {
            this.selectedOptionsIII = new ArrayList<>();
        }
        return selectedOptionsIII;
    }

    public void setSelectedOptionsIII(List<String> selectedOptionsIII) {
        this.selectedOptionsIII = selectedOptionsIII;
    }

    public List<String> getSelectedOptionsIV() {
         if (this.selectedOptionsIV == null) {
            this.selectedOptionsIV = new ArrayList<>();
        }
        return selectedOptionsIV;
    }

    public void setSelectedOptionsIV(List<String> selectedOptionsIV) {
        this.selectedOptionsIV = selectedOptionsIV;
    }

    public List<String> getSelectedOptionsV() {
        if (this.selectedOptionsV == null) {
            this.selectedOptionsV = new ArrayList<>();
        }
        return selectedOptionsV;
    }

    public void setSelectedOptionsV(List<String> selectedOptionsV) {
        this.selectedOptionsV = selectedOptionsV;
    }

    public List<String> getSelectedOptionsVI() {
        if (this.selectedOptionsVI == null) {
            this.selectedOptionsVI = new ArrayList<>();
        }
        return selectedOptionsVI;
    }

    public void setSelectedOptionsVI(List<String> selectedOptionsVI) {
        this.selectedOptionsVI = selectedOptionsVI;
    }

    public List<String> getSelectedOptionsVII() {
        if (this.selectedOptionsVII == null) {
            this.selectedOptionsVII = new ArrayList<>();
        }
        return selectedOptionsVII;
    }

    public void setSelectedOptionsVII(List<String> selectedOptionsVII) {
        this.selectedOptionsVII = selectedOptionsVII;
    }

    public List<String> getSelectedOptionsVIII() {
        if (this.selectedOptionsVIII == null) {
            this.selectedOptionsVIII = new ArrayList<>();
        }
        return selectedOptionsVIII;
    }

    public void setSelectedOptionsVIII(List<String> selectedOptionsVIII) {
        this.selectedOptionsVIII = selectedOptionsVIII;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }


    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public List<PropertiesPlansTreatmentsKey> getBeforeListPropertiesPlansTreatmentsKey() {
        return this.beforeListPropertiesPlansTreatmentsKey;
    }

    public void setBeforeListPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> beforeListPropertiesPlansTreatmentsKey) {
        if (this.beforeListPropertiesPlansTreatmentsKey == null) {
            this.beforeListPropertiesPlansTreatmentsKey = new ArrayList<>();
        }
        this.beforeListPropertiesPlansTreatmentsKey = beforeListPropertiesPlansTreatmentsKey;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }


    public List<PropertiesPlansTreatmentsKey> getEditListPropertiesPlansTreatmentsKey() {
        if (this.editListPropertiesPlansTreatmentsKey == null) {
            this.editListPropertiesPlansTreatmentsKey = new ArrayList<>();
        }
        return editListPropertiesPlansTreatmentsKey;
    }

    public void setEditListPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> editListPropertiesPlansTreatmentsKey) {
        
        this.editListPropertiesPlansTreatmentsKey = editListPropertiesPlansTreatmentsKey;
    }

    public List<PropertiesPlansTreatmentsKey> getNewListPropertiesPlansTreatmentsKey() {
        return newListPropertiesPlansTreatmentsKey;
    }

    public void setNewListPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> newListPropertiesPlansTreatmentsKey) {
        if (this.newListPropertiesPlansTreatmentsKey == null) {
            this.newListPropertiesPlansTreatmentsKey = new ArrayList<>();
        }
        this.newListPropertiesPlansTreatmentsKey = newListPropertiesPlansTreatmentsKey;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }
    
 

    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeyFiltrada() {
        return listPropertiesPlansTreatmentsKeyFiltrada;
    }

    public void setListPropertiesPlansTreatmentsKeyFiltrada(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeyFiltrada) {
        this.listPropertiesPlansTreatmentsKeyFiltrada = listPropertiesPlansTreatmentsKeyFiltrada;
    }
    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKey() {
        if (this.listPropertiesPlansTreatmentsKey == null) {
            this.listPropertiesPlansTreatmentsKey = new ArrayList<>();
        }
        return listPropertiesPlansTreatmentsKey;
    }

    public void setListPropertiesPlansTreatmentsKey(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKey) {
        this.listPropertiesPlansTreatmentsKey = listPropertiesPlansTreatmentsKey;
    }

    public PropertiesPlansTreatmentsKey getEditPropertiesPlansTreatmentsKey() {
        return editPropertiesPlansTreatmentsKey;
    }

    public void setEditPropertiesPlansTreatmentsKey(PropertiesPlansTreatmentsKey editPropertiesPlansTreatmentsKey) {
        this.editPropertiesPlansTreatmentsKey = editPropertiesPlansTreatmentsKey;
    }

    public PropertiesPlansTreatmentsKey getSelectedPropertiesPlansTreatmentsKey() {
        return selectedPropertiesPlansTreatmentsKey;
    }

    public void setSelectedPropertiesPlansTreatmentsKey(PropertiesPlansTreatmentsKey selectedPropertiesPlansTreatmentsKey) {
        this.selectedPropertiesPlansTreatmentsKey = selectedPropertiesPlansTreatmentsKey;
    }

    public String[] getNewPropertiesPlansTreatmentsKey() {
        return newPropertiesPlansTreatmentsKey;
    }

    public void setNewPropertiesPlansTreatmentsKey(String[] newPropertiesPlansTreatmentsKey) {
        this.newPropertiesPlansTreatmentsKey = newPropertiesPlansTreatmentsKey;
    }

    public PropertiesPlansTreatmentsKey getNewPropertiesPlansTreatmentsKeyObj() {
        return newPropertiesPlansTreatmentsKeyObj;
    }

    public void setNewPropertiesPlansTreatmentsKeyObj(PropertiesPlansTreatmentsKey newPropertiesPlansTreatmentsKeyObj) {
        this.newPropertiesPlansTreatmentsKeyObj = newPropertiesPlansTreatmentsKeyObj;
    }

    public PropertiesViewBean getPropetiesViewBean() {
        return propetiesViewBean;
    }

    public void setPropetiesViewBean(PropertiesViewBean propetiesViewBean) {
        this.propetiesViewBean = propetiesViewBean;
    }
    
    public List<PropertiesPlansTreatmentsKey> getListPropertiesPlansTreatmentsKeySeleccionados() {
         if (this.listPropertiesPlansTreatmentsKeySeleccionados == null) {
            this.listPropertiesPlansTreatmentsKeySeleccionados = new ArrayList<>();
        }
        return listPropertiesPlansTreatmentsKeySeleccionados;
    }

    public void setListPropertiesPlansTreatmentsKeySeleccionados(List<PropertiesPlansTreatmentsKey> listPropertiesPlansTreatmentsKeySeleccionados) {
        this.listPropertiesPlansTreatmentsKeySeleccionados = listPropertiesPlansTreatmentsKeySeleccionados;
    }
    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }
//      public void addTreatments(String id, PlansTreatments treatmentsSelected) {
//
//        this.setNewListPropertiesPlansTreatmentsKey(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(treatmentsSelected.getTreatmentsId().getId()));
//        if (!this.getNewListPropertiesPlansTreatmentsKey().isEmpty()) {
//            //
//            this.desplegarDialogoCreate(id, true);
//            this.visibleList = true;
//        }
//    }
    public void addTreatments() {

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            this.setNewListPropertiesPlansTreatmentsKey(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));
            if (!this.getNewListPropertiesPlansTreatmentsKey().isEmpty()) {
                //
                this.desplegarDialogoCreate("keysOdontodiagram", true);
                this.visibleList = true;
            }
        }
    }
    
    public void fillListGeneric(){
     
        
    Integer llenarI = 18;
    for(int i=0 ; i<8 ; i++ ){
        String value = llenarI.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsI().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsI().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarI--;
    }
    
    Integer llenarII = 21;
    for(int i=0 ; i<8 ; i++ ){
        
        String value = llenarII.toString();
       PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsII().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        
        this.getSelectedEditOptionsII().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarII++;
    }
     Integer llenarIII = 48;
    for(int i=0 ; i<8 ; i++ ){
        
       
        String value = llenarIII.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsIII().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsIII().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarIII--;
    }
    Integer llenarIV = 31;
    for(int i=0 ; i<8 ; i++ ){
        String value = llenarIV.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsIV().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsIV().get(i).setPropertiesTreatments(propertiesTreatments);
       
        llenarIV++;
    }
    Integer llenarV = 55;
    for(int i=0 ; i<5 ; i++ ){

        String value = llenarV.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsV().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsV().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarV--;
    }
    Integer llenarVI = 61;
    for(int i=0 ; i<5 ; i++ ){
        String value = llenarVI.toString();
        
       PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsVI().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsVI().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarVI++;
    }
    
    Integer llenarVII = 85;
    for(int i=0 ; i<5 ; i++ ){
        String value = llenarVII.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsVII().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsVII().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarVII--;
    }
    Integer llenarVIII = 71;
    for(int i=0 ; i<5 ; i++ ){
        
        
        String value = llenarVIII.toString();
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        this.getSelectedEditOptionsVIII().add(propertiesPlansTreatmentsKey);
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        Properties properties = new Properties();
        properties.setStringValue(value);
        propertiesTreatments.setPropertiesId(properties);
        this.getSelectedEditOptionsVIII().get(i).setPropertiesTreatments(propertiesTreatments);
        llenarVIII++;
       }
    }
    
    

}
