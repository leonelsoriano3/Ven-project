package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.KeysAnalysisInvoicesTransferViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.dto.analysisKeys.ConsultPaymentProcessOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServicePaymentReport;
import com.venedental.services.ServicePlansTreatmentKeys;
import com.venedental.services.ServicePropertiesPlansTreatmentKey;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "keysInvoicesTransferRequestBean")
@RequestScoped
public class KeysInvoicesTransferRequestBean extends BaseBean implements Serializable {

    /* Attributes */
    private List<PlansTreatmentsKeys> planTreatmentsKeysbyReportList;
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeybyReportList;

    /* Payment Report */
    private PaymentReportOutput paymentReportOutput;

    /* ManagedBean Properties */
    @ManagedProperty(value = "#{keysInvoicesTransferViewBean}")
    private KeysAnalysisInvoicesTransferViewBean keysInvoicesTransferViewBean;

    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    /* Services */
    @EJB
    private ServicePaymentReport servicePaymentReport;

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;

    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;

    @EJB
    private ServicePropertiesPlansTreatmentKey servicePropertiesPlansTreatmentKey;


    /* Constructs */
    public KeysInvoicesTransferRequestBean() {
    }

    /**
     * -------------------------------------------------------------------- |
     * GETTER AND SETTER ATTRIBUTES | KEYSINVOICES REQUEST
     * --------------------------------------------------------------------
     */
    /**
     * Actions Request Beans
     *
     * @return
     */
    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public KeysAnalysisInvoicesTransferViewBean getKeysInvoicesTransferViewBean() {
        return keysInvoicesTransferViewBean;
    }

    public void setKeysInvoicesTransferViewBean(KeysAnalysisInvoicesTransferViewBean keysInvoicesTransferViewBean) {
        this.keysInvoicesTransferViewBean = keysInvoicesTransferViewBean;
    }

    public PaymentReportOutput getPaymentReportOutput() {
        return paymentReportOutput;
    }

    public void setPaymentReportOutput(PaymentReportOutput paymentReportOutput) {
        this.paymentReportOutput = paymentReportOutput;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public List<PlansTreatmentsKeys> getPlanTreatmentsKeysbyReportList() {
        return planTreatmentsKeysbyReportList;
    }

    public void setPlanTreatmentsKeysbyReportList(List<PlansTreatmentsKeys> planTreatmentsKeysbyReportList) {
        this.planTreatmentsKeysbyReportList = planTreatmentsKeysbyReportList;
    }

    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeybyReportList() {
        return propertiesPlansTreatmentsKeybyReportList;
    }

    public void setPropertiesPlansTreatmentsKeybyReportList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeybyReportList) {
        this.propertiesPlansTreatmentsKeybyReportList = propertiesPlansTreatmentsKeybyReportList;
    }

    /**
     * --------------------------------------------------------------------
     * METHODS A IMPLEMENTS IN THE COMPONENTS | KEYSINVOICES REQUEST
     * --------------------------------------------------------------------
     */
    /**
     * Methods at transfer invoices dialog.
     *
     * @param dialogo
     */
    public void keysRegisterTransferData(String dialogo) {

        try {

            if (this.getKeysInvoicesTransferViewBean().getNumberTransfer() != null && this.getKeysInvoicesTransferViewBean()
                    .getDateTransfer() != null && this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().
                            getBillNumber() != null  &&  this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().
                            getBillNumber().trim().compareTo("") > 0 
                            && this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getBillDate() !=null
                            && this.getKeysInvoicesTransferViewBean()
                            .getPaymentReportOutput().getControlNumber() != null
                            && this.getKeysInvoicesTransferViewBean()
                             .getPaymentReportOutput().getControlNumber().trim().compareTo("") > 0
                           
                           ) {

                this.getKeysInvoicesTransferViewBean().getPaymentReportOutput();
                String nroControl = this.getKeysInvoicesTransferViewBean().getMaskNumberControl();
                 nroControl = nroControl+ 
                         this.getKeysInvoicesTransferViewBean()
                             .getPaymentReportOutput().getControlNumber();
                  this.getKeysInvoicesTransferViewBean()
                             .getPaymentReportOutput().setControlNumber(nroControl);
                 
                if (!this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getBankAccountNumber().isEmpty()) {

                    Users user = this.securityViewBean.obtainUser();
                    java.sql.Date dateTransfer = this.getKeysInvoicesTransferViewBean().convertJavaDateToSqlDate(
                            this.getKeysInvoicesTransferViewBean().getDateTransfer());

                    // Call Service Update PaymentReport 
                    this.servicePaymentReport.updatePaymentReportTransfer(this.getKeysInvoicesTransferViewBean()
                            .getPaymentReportOutput().getIdReport(), dateTransfer, this.getKeysInvoicesTransferViewBean()
                            .getNumberTransfer(), this.getKeysInvoicesTransferViewBean().getPaymentReportOutput()
                            .getIdBankAccount());
                    // Converter Date
                    java.sql.Date dateInvoices = this.getKeysInvoicesTransferViewBean().convertJavaDateToSqlDate(
                            this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getBillDate());
                    
                    // Call Service Update PaymentReport 
                    this.servicePaymentReport.updatePaymentReportInvoices(this.getKeysInvoicesTransferViewBean()
                            .getPaymentReportOutput().getIdReport(),dateInvoices, this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().
                             getBillNumber(), this.getKeysInvoicesTransferViewBean()
                             .getPaymentReportOutput().getControlNumber(), user.getId());

                    // call Service Update P.T.K 
                    this.setPlanTreatmentsKeysbyReportList(this.servicePlansTreatmentKeys
                            .findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_ABONAR
                                    .getValor(), null, null));
                    // Call service Update P.P.T.K                
                    this.setPropertiesPlansTreatmentsKeybyReportList(this.servicePropertiesPlansTreatmentKey.
                            findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_ABONAR
                                    .getValor(), null, null));

                    // Update Status P.T.K 
                    if (!this.getPlanTreatmentsKeysbyReportList().isEmpty()) {

                        for (PlansTreatmentsKeys ptkUpdateRegisterTransfer : this.getPlanTreatmentsKeysbyReportList()) {

                            this.servicePlansTreatmentKeys.updateStatus(ptkUpdateRegisterTransfer.getId(),
                                    EstatusPlansTreatmentsKeys.ABONADO.getValor(), user.getId(), null);
                        }
                    }

                    // Update Status P.P.T.K
                    if (!this.getPropertiesPlansTreatmentsKeybyReportList().isEmpty()) {

                        for (PropertiesPlansTreatmentsKey pptkUpdateRegisterTransfer
                                : this.getPropertiesPlansTreatmentsKeybyReportList()) {

                            this.servicePropertiesPlansTreatmentKey.updateStatus(pptkUpdateRegisterTransfer.getId(),
                                    EstatusPlansTreatmentsKeys.ABONADO.getValor(), user.getId(), null);
                        }
                    }

                    String retencion[] = this.servicePaymentReport.generatePaymentReportRetention(
                            this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getIdSpecialist(),
                            this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getIdReport());
                    int condic = Integer.valueOf(retencion[1]);

                    switch (condic) {

                        case 0:
                            System.out.println("Se pudo generar el comprobante de retencion con exito");
                            break;
                        case 1:
                            System.out.println("El especialista no se le retiene islr");
                            break;

                        case 2:
                            System.out.println("No llega al monto minimo");
                            break;
                    }

                    // Clean List
                    this.getKeysInvoicesTransferViewBean().resetDataTableKeysInvoicesTransfer();
                    this.getKeysInvoicesTransferViewBean().setInvoicesTransferKeysList(
                            new ArrayList<ConsultPaymentProcessOutput>());
                    this.setPlanTreatmentsKeysbyReportList(new ArrayList<PlansTreatmentsKeys>());
                    this.setPropertiesPlansTreatmentsKeybyReportList(new ArrayList<PropertiesPlansTreatmentsKey>());

                    if (this.getKeysInvoicesTransferViewBean().getStatusKeysid() != null) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito ",
                                "Operación realizada con éxito"));

                        this.getKeysInvoicesTransferViewBean().findAllKeysAnalysisInvoicesTransfer();
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Éxito",
                                "Operación realizada con éxito"));
                        this.getKeysInvoicesTransferViewBean().findKeysInvoicesTransferByTwoStatus();
                    }

                    //this.getKeysInvoicesTransferViewBean().findAllKeysAnalysisInvoicesTransfer();
                    RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");
                    // Exit Dialog
                    exitKeysInvoicesTransfer(dialogo);

                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Error",
                            "La factura no posee cuentas asociadas para registrar la transferencia"));
                }

            } else {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Error", "El formulario posee campos vacios"));
            }
            RequestContext.getCurrentInstance().update("form:growInvoicesTransferMessage");
            this.getKeysInvoicesTransferViewBean().findAllKeysAnalysisInvoicesTransfer();
            RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    
    
    /**
     * Method update data keys invoices
     *
     * @param dialogo
     */
    public void keysRegisterInvoicesData(String dialogo) {

        try {
            // GET data
            this.getKeysInvoicesTransferViewBean().getPaymentReportOutput();
            this.getKeysInvoicesTransferViewBean().getDateInvoices();
            this.getKeysInvoicesTransferViewBean().getInvoicesRegister();
            String nroControl = this.getKeysInvoicesTransferViewBean().getMaskNumberControl();

            boolean dataNumberBool = verifyActiveNumberControl(this.getKeysInvoicesTransferViewBean()
                    .getPaymentReportOutput().getIdSpecialist());

            if (dataNumberBool == true && this.keysInvoicesTransferViewBean.isEnableNumberControl()) {
                if (this.getKeysInvoicesTransferViewBean().getDateInvoices() != null
                        && this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[0] != null
                        && this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[1] != null 
                        && this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[1].trim().compareTo("") > 0
                        
                        ) {
                   
                    // Obtain Users
                    Users user = this.securityViewBean.obtainUser();
                    
                    this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[1] = nroControl + this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[1];

                    // Converter Date
                    java.sql.Date dateInvoices = this.getKeysInvoicesTransferViewBean().convertJavaDateToSqlDate(
                            this.getKeysInvoicesTransferViewBean().getDateInvoices());

                    // Call Service Update PaymentReport 
                    this.servicePaymentReport.updatePaymentReportInvoices(this.getKeysInvoicesTransferViewBean()
                            .getPaymentReportOutput().getIdReport(), dateInvoices, this.getKeysInvoicesTransferViewBean()
                            .getInvoicesRegister()[0], this.getKeysInvoicesTransferViewBean()
                            .getInvoicesRegister()[1], user.getId());

                    // call Service Update P.T.K 
                    this.setPlanTreatmentsKeysbyReportList(this.servicePlansTreatmentKeys.
                            findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_FACTURAR
                                    .getValor(), null, null));
                    // Call service Update P.P.T.K                
                    this.setPropertiesPlansTreatmentsKeybyReportList(this.servicePropertiesPlansTreatmentKey
                            .findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_FACTURAR
                                    .getValor(), null, null));

                    // get list
                    this.getPlanTreatmentsKeysbyReportList();
                    this.getPropertiesPlansTreatmentsKeybyReportList();

                    // Update Record Update by P.T.K
                    if (!this.getPlanTreatmentsKeysbyReportList().isEmpty()) {

                        for (PlansTreatmentsKeys planTreatmentsKeysUpdate : this.getPlanTreatmentsKeysbyReportList()) {

                            this.servicePlansTreatmentKeys.updateStatus(planTreatmentsKeysUpdate.getId(),
                                    EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), user.getId(), null);

                        }
                    }

                    // Update Record Update by P.P.T.K
                    if (!this.getPropertiesPlansTreatmentsKeybyReportList().isEmpty()) {

                        for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKeysUpdate
                                : this.getPropertiesPlansTreatmentsKeybyReportList()) {

                            this.servicePropertiesPlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeysUpdate
                                    .getId(), EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), user.getId(), null);
                        }
                    }

                    // Clean List
                    this.getKeysInvoicesTransferViewBean().setInvoicesTransferKeysList(
                            new ArrayList<ConsultPaymentProcessOutput>());
                    this.setPlanTreatmentsKeysbyReportList(new ArrayList<PlansTreatmentsKeys>());
                    this.setPropertiesPlansTreatmentsKeybyReportList(new ArrayList<PropertiesPlansTreatmentsKey>());

                    this.getKeysInvoicesTransferViewBean().findAllKeysAnalysisInvoicesTransfer();
                    RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");

                    exitKeysInvoicesRegister(dialogo);

                } else {

                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN,
                                    "Error",
                                    "El formulario posee campos vacios"
                            )
                    );
                }
            } else {

                if (this.getKeysInvoicesTransferViewBean().getDateInvoices() != null
                        && this.getKeysInvoicesTransferViewBean().getInvoicesRegister()[0] != null) {

                    // Obtain Users
                    Users user = this.securityViewBean.obtainUser();

                    // Converter Date
                    java.sql.Date dateInvoices = this.getKeysInvoicesTransferViewBean().convertJavaDateToSqlDate(
                            this.getKeysInvoicesTransferViewBean().getDateInvoices());

                    // Call Service Update PaymentReport 
                    this.servicePaymentReport.updatePaymentReportInvoices(this.getKeysInvoicesTransferViewBean()
                            .getPaymentReportOutput().getIdReport(), dateInvoices, this.getKeysInvoicesTransferViewBean()
                            .getInvoicesRegister()[0], null, user.getId());

                    // call Service Update P.T.K 
                    this.setPlanTreatmentsKeysbyReportList(this.servicePlansTreatmentKeys
                            .findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_FACTURAR
                                    .getValor(), null, null));
                    // Call service Update P.P.T.K                
                    this.setPropertiesPlansTreatmentsKeybyReportList(this.servicePropertiesPlansTreatmentKey
                            .findProPlaTreKeyByReportInvoicesTransfer(this.getKeysInvoicesTransferViewBean()
                                    .getPaymentReportOutput().getIdReport(), EstatusPlansTreatmentsKeys.POR_FACTURAR
                                    .getValor(), null, null));

                    // get list
                    this.getPlanTreatmentsKeysbyReportList();
                    this.getPropertiesPlansTreatmentsKeybyReportList();

                    // Update Record Update by P.T.K
                    if (!this.getPlanTreatmentsKeysbyReportList().isEmpty()) {

                        for (PlansTreatmentsKeys planTreatmentsKeysUpdate : this.getPlanTreatmentsKeysbyReportList()) {

                            this.servicePlansTreatmentKeys.updateStatus(planTreatmentsKeysUpdate.getId(),
                                    EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), user.getId(), null);

                        }
                    }

                    // Update Record Update by P.P.T.K
                    if (!this.getPropertiesPlansTreatmentsKeybyReportList().isEmpty()) {

                        for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKeysUpdate
                                : this.getPropertiesPlansTreatmentsKeybyReportList()) {

                            this.servicePropertiesPlansTreatmentKey.updateStatus(propertiesPlansTreatmentsKeysUpdate
                                    .getId(), EstatusPlansTreatmentsKeys.POR_ABONAR.getValor(), user.getId(), null);

                        }
                    }

                    // Clean List
                    this.getKeysInvoicesTransferViewBean().setInvoicesTransferKeysList(new ArrayList<ConsultPaymentProcessOutput>());
                    this.setPlanTreatmentsKeysbyReportList(new ArrayList<PlansTreatmentsKeys>());
                    this.setPropertiesPlansTreatmentsKeybyReportList(new ArrayList<PropertiesPlansTreatmentsKey>());

                    this.getKeysInvoicesTransferViewBean().findAllKeysAnalysisInvoicesTransfer();
                    RequestContext.getCurrentInstance().update("form:keysInvoicesTransferTable");

                    exitKeysInvoicesRegister(dialogo);
                } else {

                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN,
                                    "Error",
                                    "El formulario posee campos vacios"
                            )
                    );
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        RequestContext.getCurrentInstance().update("form:growInvoicesTransferMessage");

    }

    /**
     * Methods an the validate number control active by retentions
     *
     * @param idSpecialist
     * @return
     * @throws SQLException
     */
    public boolean verifyActiveNumberControl(Integer idSpecialist) throws SQLException {

        boolean dataNumberControl = this.servicePaymentReport.findValidationNumberControlInvoicesKeys(idSpecialist);

        return dataNumberControl;
    }

    /**
     * Methods an the views invoices Dialog confirm key
     *
     * @param dialogo
     * @param consultPaymentProcessOutput
     */
    public void findKeysDetailConfirmsDialog(String dialogo, ConsultPaymentProcessOutput consultPaymentProcessOutput) {

        try {

            // Setter and object
            this.getKeysInvoicesTransferViewBean().setConfirmKeyDetailsInvoicesTransferOutputList(this.serviceAnalysisKeys.findConfirmKeyDetailsInvoicesTransfer(consultPaymentProcessOutput.getIdReport(), consultPaymentProcessOutput.getIdStatus(), null, null));
            this.getKeysInvoicesTransferViewBean().setConfirmKeyDetailsInvoicesTransferOutputSelected(this.getKeysInvoicesTransferViewBean().getConfirmKeyDetailsInvoicesTransferOutputList().get(0));

            this.getKeysInvoicesTransferViewBean().getConfirmKeyDetailsInvoicesTransferOutputList();
            this.getKeysInvoicesTransferViewBean().getConfirmKeyDetailsInvoicesTransferOutputSelected();

            if (this.getKeysInvoicesTransferViewBean().getConfirmKeyDetailsInvoicesTransferOutputList().isEmpty()) {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se encuentran registros"));
            } else {

                // View Dialog
                this.accionesRequestBean.desplegarDialogoView(dialogo);
            }

        } catch (Exception e) {
            System.err.println(e.getStackTrace());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error de data", e.getMessage()));
        }
        RequestContext.getCurrentInstance().update("form:growInvoicesTransferMessage");
    }

    /**
     * Method to verify the deployment of dialogues
     *
     * @param ConsultPaymentProcessOutputSelect
     */
    public void openViewDiaogInvoicesTransfer(ConsultPaymentProcessOutput ConsultPaymentProcessOutputSelect) {        
        if (Objects.equals(ConsultPaymentProcessOutputSelect.getIdStatus(), EstatusPlansTreatmentsKeys.POR_FACTURAR.getValor())) {
            this.findKeysInvoicesViewDialog("keysInvoicesDialog", ConsultPaymentProcessOutputSelect);

        }
        if (Objects.equals(ConsultPaymentProcessOutputSelect.getIdStatus(), EstatusPlansTreatmentsKeys.POR_ABONAR.getValor())) {
            this.findKeysTransferViewDialog("keysTransferDialog", ConsultPaymentProcessOutputSelect);

        }
        if (Objects.equals(ConsultPaymentProcessOutputSelect.getIdStatus(), EstatusPlansTreatmentsKeys.ABONADO.getValor())) {
            this.findKeysInvoicesTransferViewDialogConsult("keysInvoicesTransferConsult", ConsultPaymentProcessOutputSelect);

        }
    }

    /**
     * Methods an the views invoices Dialogs
     *
     * @param dialogo
     */
    public void findKeysInvoicesViewDialog(String dialogo, ConsultPaymentProcessOutput consultPaymentProcessOutputSelect) {

        try {
            this.setPaymentReportOutput(this.servicePaymentReport.consultPaymentReportById(consultPaymentProcessOutputSelect.getIdReport()));
            this.getKeysInvoicesTransferViewBean().setPaymentReportOutput(this.getPaymentReportOutput());
            boolean dataNumberControl = verifyActiveNumberControl(this.getPaymentReportOutput().getIdSpecialist());
            //this.getKeysInvoicesTransferViewBean().setInputEnabledDisabledNumberControl(dataNumberControl);
            double totalNatural = 12500.00, totalJuridico = 25.00;
            if (consultPaymentProcessOutputSelect.getIsNatural() == 1 && this.getKeysInvoicesTransferViewBean()
                    .getPaymentReportOutput().getAmountTotal() >= totalNatural && dataNumberControl == true) {
                //this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
                
            } else if (consultPaymentProcessOutputSelect.getIsNatural() == 0 && this.getKeysInvoicesTransferViewBean()
                    .getPaymentReportOutput().getAmountTotal() >= totalJuridico && dataNumberControl == true) {
                //this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
                //this.getKeysInvoicesTransferViewBean().setEnableNumberControl(true);
            } else {
                //this.getKeysInvoicesTransferViewBean().setVisibilityRow(false);
               
            }
            this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
            this.getKeysInvoicesTransferViewBean().setEnableNumberControl(true);
            RequestContext.getCurrentInstance().update("form:rowControlNumber");
            RequestContext.getCurrentInstance().update("form:keysInvoicesDialog");
            RequestContext.getCurrentInstance().update("form:panelRegisterInvoicesKeys");
            RequestContext.getCurrentInstance().update("form:rowControlNumber");
            this.accionesRequestBean.desplegarDialogoCreate(dialogo, true);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }
    }

    public void findKeysTransferViewDialog(String dialogo, ConsultPaymentProcessOutput consultPaymentProcessOutputSelect) {

        try {

            this.setPaymentReportOutput(this.servicePaymentReport.consultPaymentReportById(consultPaymentProcessOutputSelect.getIdReport()));
            this.getKeysInvoicesTransferViewBean().setPaymentReportOutput(this.getPaymentReportOutput());
            this.getKeysInvoicesTransferViewBean().getPaymentReportOutput();
            if(this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().
                    getControlNumber().length()>3){ this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().
                    setControlNumber(this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getControlNumber().substring(3));
            }
           
            double totalNatural = 12500.00, totalJuridico = 25.00;
            if (consultPaymentProcessOutputSelect.getIsNatural() == 1 && this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getAmountTotal() >= totalNatural) {
                this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
              } 
                //else if (consultPaymentProcessOutputSelect.getIsNatural() == 0 && this.getKeysInvoicesTransferViewBean().getPaymentReportOutput().getAmountTotal() >= totalJuridico) {
//                this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
//            } else {
//                this.getKeysInvoicesTransferViewBean().setVisibilityRow(false);
//            }
            this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);

            RequestContext.getCurrentInstance().update("form:keysTransferDialog");
            RequestContext.getCurrentInstance().update("form:rowControlNumber2");
            RequestContext.getCurrentInstance().update("form:panelRegisterTransferKeys");
            this.accionesRequestBean.desplegarDialogoEdit(dialogo, true);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Methods an the views invoices and transfer Dialogs consult
     *
     * @param dialogo
     * @param consultPaymentProcessOutputSelect
     */
    public void findKeysInvoicesTransferViewDialogConsult(String dialogo, ConsultPaymentProcessOutput consultPaymentProcessOutputSelect) {

        try {

            // Setter and Object
            this.setPaymentReportOutput(this.servicePaymentReport.consultPaymentReportById(consultPaymentProcessOutputSelect.getIdReport()));

            this.getPaymentReportOutput();
            this.getKeysInvoicesTransferViewBean().setVisibilityRow(true);
            RequestContext.getCurrentInstance().update("form:keysInvoicesTransferConsult");
            RequestContext.getCurrentInstance().update("form:rowControlNumber3");
            RequestContext.getCurrentInstance().update("form:panelRegisterTransferKeysConsult");
            this.getKeysInvoicesTransferViewBean().setPaymentReportOutput(this.getPaymentReportOutput());
            // View Dialog
           this.accionesRequestBean.desplegarDialogoEdit(dialogo, false);
        } catch (Exception e) {

            System.out.println(e.getStackTrace());
        }
    }

    /**
     * Methods a exit dialog with register invoices keys
     *
     * @param dialogo
     */
    public void exitKeysInvoicesRegister(String dialogo) {

        try {

            // Clean data
            cleanDataInvoicesDialog();

            // exit dialog
            this.accionesRequestBean.cerrarDialogoCreate(dialogo);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }
    }

    /**
     * Methods a exit dialog with register Transfer invoices keys
     *
     * @param dialogo
     */
    public void exitKeysInvoicesTransfer(String dialogo) {

        try {
            cleanDataTransferDialog();

            this.accionesRequestBean.cerrarDialogoEdit(dialogo);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Methods at clean data
     *
     * @void
     */
    public void cleanDataInvoicesDialog() {

        this.getKeysInvoicesTransferViewBean().setDateInvoices(null);
        this.getKeysInvoicesTransferViewBean().setInvoicesRegister(new String[2]);
        this.getKeysInvoicesTransferViewBean().setInputEnabledDisabledNumberControl(new Boolean(false));

    }

    /**
     * Methods at clean data transfer dialog
     *
     * @void
     */
    public void cleanDataTransferDialog() {
        this.getKeysInvoicesTransferViewBean().setDateTransfer(null);
        this.getKeysInvoicesTransferViewBean().setNumberTransfer(null);
        RequestContext.getCurrentInstance().update("form:panelRegisterTransferKeys");
        RequestContext.getCurrentInstance().update("form:panelGridRegisterTransferKeys");        
    }
        

}
