/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.ejb;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.properties.PropertiesConfiguration;
import com.venedental.carga_masiva.base.properties.PropertiesInsurances;
import com.venedental.carga_masiva.dto.ServiceFacadeDTO;
import com.venedental.carga_masiva.load.AbstractLoader;
import com.venedental.carga_masiva.load.helper.NotifierHelper;
import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.InsurancesPatientsFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.RelationshipFacadeLocal;
import com.venedental.facade.store_procedure.CleanStatusFacadeLocal;
import com.venedental.mail.ejb.MailSenderLocal;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author usuario
 */
@Stateless
public class MassiveLoad implements MassiveLoadLocal {

    private static final Logger log = CustomLogger.getLogger(MassiveLoad.class.getName());

    @EJB
    private CleanStatusFacadeLocal cleanStatusFacadeLocal;

    @EJB
    private InsurancesFacadeLocal insurancesFacadeLocal;

    @EJB
    private InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal;

    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;

    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    @EJB
    private PlansFacadeLocal plansFacadeLocal;

    @EJB
    private RelationshipFacadeLocal relationshipFacadeLocal;

    @EJB
    private MailSenderLocal mailSenderLocal;

    @Override
    public void execute(boolean automatic) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_DIRECTORY);
        String date = format.format(new Date());
        ServiceFacadeDTO serviceFacadeDto = completeServiceSet();
        String uploadsPath = PropertiesConfiguration.get(Constants.PROPERTY_PATH_FTP);
        log.log(Level.INFO, "Directorio de subidas: {0}", uploadsPath);
        String insurances = PropertiesConfiguration.get(Constants.PROPERTY_INSURANCES);
        String[] listInsurances = insurances.split(Constants.LIST_SEPARATOR);
        List<File> files = new ArrayList<>();
        for (String insurance : listInsurances) {
            File resumeFile = processInsurance(uploadsPath, insurance, serviceFacadeDto, date);
            if (resumeFile != null) {
                files.add(resumeFile);
            }
        }
        String mail = PropertiesConfiguration.get(Constants.PROPERTY_MAIL_VENEDEN);
        log.log(Level.INFO, "MAIL GENERAL: {0}", mail);
        NotifierHelper notifierHelper = new NotifierHelper(serviceFacadeDto);
        notifierHelper.notifyGeneral(files, automatic, mail);
    }

    private ServiceFacadeDTO completeServiceSet() {
        ServiceFacadeDTO serviceFacadeDto = new ServiceFacadeDTO();
        serviceFacadeDto.setCleanStatusFacadeLocal(cleanStatusFacadeLocal);
        serviceFacadeDto.setInsuranceFacadeLocal(insurancesFacadeLocal);
        serviceFacadeDto.setInsurancesPatientsFacadeLocal(insurancesPatientsFacadeLocal);
        serviceFacadeDto.setPatientsFacadeLocal(patientsFacadeLocal);
        serviceFacadeDto.setPlansPatientsFacadeLocal(plansPatientsFacadeLocal);
        serviceFacadeDto.setPlansFacadeLocal(plansFacadeLocal);
        serviceFacadeDto.setRelationshipFacadeLocal(relationshipFacadeLocal);
        serviceFacadeDto.setMailSenderLocal(mailSenderLocal);
        return serviceFacadeDto;
    }

    private File processInsurance(String uploadsPath, String insurance, ServiceFacadeDTO serviceFacadeDto, String date) {
        String loaderName = PropertiesInsurances.get(insurance + Constants.SUFIJO_LOADER);
        if (loaderName == null) {
            loaderName = PropertiesInsurances.get(Constants.PROPERTY_BASIC_LOADER);
        }
        AbstractLoader loader = findClass(loaderName);
        if (loader == null) {
            loaderName = PropertiesInsurances.get(Constants.PROPERTY_BASIC_LOADER);
            loader = findClass(loaderName);
        }
        return loader.load(uploadsPath, insurance, serviceFacadeDto, date);
    }

    private AbstractLoader findClass(String loaderName) {
        try {
            String value = Constants.PACKAGE_LOADER + loaderName;
            Class classLoader = Class.forName(value);
            return (AbstractLoader) classLoader.newInstance();
        } catch (ClassNotFoundException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException ex) {
            CustomLogger.getLogger(MassiveLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
