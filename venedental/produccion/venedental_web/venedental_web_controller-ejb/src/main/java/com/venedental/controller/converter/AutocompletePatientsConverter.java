package com.venedental.controller.converter;

import com.venedental.controller.view.PatientsViewBean;
import com.venedental.controller.view.SpecialistsViewBean;
import com.venedental.model.Patients;
import com.venedental.model.Specialists;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("autocompletePatientsConverter")
public class AutocompletePatientsConverter implements Converter {

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;

    private Boolean encontrado = false;

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        int posicion = 0;

        if (value != null && value.trim().length() > 0) {
            try {

                encontrado = false;

                PatientsViewBean patientViewBean = (PatientsViewBean) context.getApplication().getELResolver().getValue(context.getELContext(), null, "patientsViewBean");
                int numero = Integer.parseInt(value);
                int j = 0;

                for (int i = 0; i < patientViewBean.getListaPatients().size(); i++) {

                    Patients s = patientViewBean.getListaPatients().get(i);
                    if (s.getId() == numero) {
                        posicion = j;
                    }

                    j++;
                }
                if(patientViewBean.getListaPatients().size() > 0){
                   return patientViewBean.getListaPatients().get(posicion);
                }

                return null;

            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Patients) {
            Patients patient = (Patients) value;
            return patient.getId().toString();
        }
        return "";
    }

}