package com.venedental.controller.angular;

import com.venedental.constans.ConstansConsultPaymentReport;
import com.venedental.constans.ConstansTreatmentsReport;
import com.venedental.controller.angular.generatePaymentReport.ReportDentalTreatmentsBean;
import com.venedental.controller.view.ReviewKeysAnalysisViewBean;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;

import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.paymentReport.ConfirmKeyDetailsInvoicesTransferOutput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.dto.reportAuditConsultTrace.*;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import com.venedental.model.utils.CustomLogger;
import com.venedental.services.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by luis.carrasco@arkiteck.biz on 15/07/16.
 */

@ApplicationPath("/resources")
@Path("/auditConsultTraceBean")
public class AuditConsultTraceBean extends Application {


    private static final Logger log = services.utils.CustomLogger.getGeneralLogger(AuditConsultTraceBean.class.getName());
    private static String DATEREPORT="";
    private static Date DATEFORREPORT;

    @EJB
    private ServiceAuditConsultTrace serviceAuditConsultTrace;

    @EJB
    private ServiceCreateUser serviceCreateUser;

    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;

    @EJB
    private ServicePaymentReport servicePaymentReport;

    @EJB
    private ServiceReportDentalTreatments serviceReportDentalTreatments;

    private JRDataSource listaDataSource;

    private static String PATHDELETEREPORT="";

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getListAuditConsultTrace")
    public List<AuditConsultTraceOutput> getListAuditConsultTrace() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        Integer idUser = user.getId();
        List<AuditConsultTraceOutput> result = serviceAuditConsultTrace.getAuditConsultTrace(idUser);

        if(result == null){

            
            result = new ArrayList();
            
        }
        else
        if(result.size() == 0) {

            result = new ArrayList();
        }
        else{



        }


        return result;

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getPaymentReportDetails")
    public PaymentReportOutput getPaymentReportDetails(@QueryParam("idPaymentReport") Integer idPaymentReport){

        PaymentReportOutput paymentReportOutput =  new PaymentReportOutput();

        try {

           return paymentReportOutput  = servicePaymentReport.consultPaymentReportById(idPaymentReport);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return paymentReportOutput;
    }


    // detalle de los tratamientos que van en el reporte de pago
    @GET
    //   @Produces("application/pdf")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("downloadPDF")
    public Response downloadPDF(@QueryParam("idPayReport")Integer idPayReport){
        Response resp = null;

        try {

           List<ConfirmKeyDetailsOutput> confirmKeyDetailsOutput = null;

            PaymentReportOutput paymentReportOutput  = servicePaymentReport.consultPaymentReportById(idPayReport);
            List<ReportPaymentDetailsOutput> LReportPaymentDetailsOutputList = serviceAuditConsultTrace.getInfoTreatmentsForPaymentReport(idPayReport);

            Date date = new Date();
            String mes = String.valueOf(new Date('m'));
            resp  =  this.generatePdfPaymentReport(date,paymentReportOutput,LReportPaymentDetailsOutputList);

        } catch (SQLException e) {
            e.printStackTrace();


        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public Integer getIdUserLogged(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        return user.getId();
    }



    /*
    MUESTRA EL REPORTE DE PAGO PARA LOS ESPECIALISTAS CONSULTA DE TRAZA DE LA AUDITORÍA

     */


    public Response generatePdfPaymentReport(Date date, PaymentReportOutput paymentReportOutput,List<ReportPaymentDetailsOutput> LReportPaymentDetailsOutputs)
            throws IOException {
        double rejectAmount = 0;
        DateFormat dateF = new SimpleDateFormat("MM-dd-yyyy-HH:mm:ss");
        DateFormat dateC = new SimpleDateFormat("MM-dd-yyyy");
        String reportDate = dateF.format(date);
        String dateReport = dateC.format(paymentReportOutput.getReportDate());
        Response resp = null;
        String dateString = getMonthYearToReport(paymentReportOutput.getReportDate());

        try {

            SpecialistOutput specialists =getSpecialistToReport(this.getIdUserLogged()) ;

            if (specialists!= null) {



                String nameReport = "RP_" + specialists.getName() + "_"
                        + specialists.getCadrId().toString() + "_" + reportDate;

                Map<String, Object> params = new HashMap<>();
                String logoPath = obtainDirectory() + ConstansConsultPaymentReport.PROPERTY_PATH_LOGO;
                String jasperPath = obtainDirectory() + ConstansConsultPaymentReport.PROPERTY_PATH_JASPER;
                DecimalFormat df = new DecimalFormat("#");
                df.setMaximumFractionDigits(8);
                params.put("number",paymentReportOutput.getNumberPaymentReport());
                params.put("amount",paymentReportOutput.getAmount());
                params.put("month", dateString);
//                params.put("totalAmount", createPaymentReportOutput.getAmount());
                params.put("totalAmount",paymentReportOutput.getAmountTotal());
                params.put("rejectAmount", paymentReportOutput.getAuditedAmount());
                params.put("auditAmount", paymentReportOutput.getAmount());
                params.put("specialistName", specialists.getName());
                if (specialists.getRif() != null) {
                    params.put("specialistRif", specialists.getRif());
                } else {
                    params.put("-", specialists.getRif());
                }
                params.put("date", date);
                params.put("heal_logo2", logoPath);

                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
                String amountTotalConvert = decimalFormat.format(paymentReportOutput.getAmount()
                        - paymentReportOutput.getAuditedAmount());
                params.put("amounTotal", amountTotalConvert);
                String nameReportPrint = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
                String extention = ".pdf";
                String rPath = this.obtainDirectory() + ConstansConsultPaymentReport.PROPERTY_PATH_REPORT;
                String reportExtension = ".pdf";
                nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
                listaDataSource = new PaymentReportDataSource(LReportPaymentDetailsOutputs);
                JasperPrint jp = JasperFillManager.fillReport(jasperPath, params, listaDataSource);
                String reportPath = readFileProperties().getProperty(ConstansConsultPaymentReport.PROPERTY_PATH_JASPER);
                JasperExportManager.exportReportToPdfFile(jp, rPath  +nameReport+ reportExtension);
                this.PATHDELETEREPORT = rPath + nameReportPrint + extention;
                resp =  this.downloadFile(rPath + nameReportPrint + extention,nameReportPrint + extention);

            }
        } catch (IOException e) {

            System.out.println(e.getStackTrace());
        } catch (JRException e) {
            e.printStackTrace();
        }
        return resp;
    }



    /**
     * params :
     *  filePath = Path del archivo a descargar + nombre del archivo + extensión
     *  nameReport = Nombre del archivo + extensión
     *
     * */
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("deleteFile")
    public Boolean deleteFile() throws IOException {
        Boolean resp = false;
        File file = new File(this.PATHDELETEREPORT);
        if (file.exists()) {
            file.delete();
            resp = true;
        }
    return resp;
    }

    public Response downloadFile(String filePath, String nameReport) throws IOException {

        File file = new File(filePath);
        ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename="+nameReport+" ");
        return responseBuilder.build();

    }

    public SpecialistOutput getSpecialistToReport(Integer idUser) {

        SpecialistOutput specialistOutput;
        specialistOutput = serviceReportDentalTreatments.getSpecialistToReport(idUser);

        if (specialistOutput != null) {
            return specialistOutput;
        } else {
            return specialistOutput = new SpecialistOutput();
        }

    }

    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansConsultPaymentReport.FILENAME_PAYMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansConsultPaymentReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }


    public static String obtainDirectory() {

        URL rutaURL = ReportDentalTreatmentsBean.class.getProtectionDomain().getCodeSource().getLocation();
        String path = rutaURL.getPath();

        return path.substring(0, path.indexOf("lib"));
    }

    public Properties readFilePropertiesTreatment() throws IOException {
        Properties mProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT
                    + "' not found in the classpath");
        }
        mProperties.load(inputStream);
        return mProperties;
    }


    @GET
    @Produces("application/pdf")
    @Path("generateTreatmentReport")
    public Response consultTreatmentReport(@QueryParam("numReportTreat") Integer numReportTreat) throws ParseException{

        List<ReportTreatmentDetailsOutput> listTreat = serviceAuditConsultTrace.getInfoTreatmentReport
                (this.getIdUserLogged(),numReportTreat);
        Response resp = null;

        try {
            SpecialistOutput specialistOutput  = serviceReportDentalTreatments.getSpecialistToReport(this.getIdUserLogged());
            if(specialistOutput != null){
                String nameReport = "RT_" + specialistOutput.getName() + "_" + specialistOutput.getCadrId();
                Map<String, Object> params = new HashMap<>();
                String logoPath = obtainDirectory() + ConstansTreatmentsReport.PROPERTY_PATH_LOGO;
                String jasperPath = obtainDirectory() + readFilePropertiesTreatment().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_JASPER);
                ConstansTreatmentsReport.PROPERTY_PATH_REPORT = nameReport;
                if(listTreat != null){
                     for(ReportTreatmentDetailsOutput reportTreatmentDetailsOutput:listTreat){
                        this.DATEREPORT= new SimpleDateFormat("dd/MM/yyyy").format(reportTreatmentDetailsOutput.getDateReport());
                     }

                }
                
                Date  fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(this.DATEREPORT);
              
                 java.util.Date dateForReport = new java.util.Date(fechaTransformada.getTime());
                   this.DATEFORREPORT=dateForReport;

                // Información del Especialista para el reporte
                params.put("name",specialistOutput.getName());
                params.put("cardId",specialistOutput.getCadrId());
                params.put("phone",specialistOutput.getPhone());
                params.put("address",specialistOutput.getAddress());
                params.put("especialidad",specialistOutput.getSpecialityName());
                params.put("logoPath", logoPath);
                params.put("numCorrelative", numReportTreat);
                params.put("dateForReport",dateForReport);
                listaDataSource = new ReportTreatmentsDataSource(listTreat);
                String rPath = obtainDirectory() + ConstansTreatmentsReport.PROPERTY_P ;
                rPath = StringUtils.replace(StringUtils.stripAccents(rPath), " ", "_");
                String reportExtension = ".pdf";
                try {
                    String reportPath = ConstansTreatmentsReport.PROPERTY_PATH_JASPER;
                    nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
                    JasperPrint jp = JasperFillManager.fillReport(jasperPath, params, listaDataSource);
                    JasperExportManager.exportReportToPdfFile(jp, rPath  +nameReport+ reportExtension);

                } catch (JRException e) {

                    e.printStackTrace();

                }
                 this.PATHDELETEREPORT = rPath + nameReport + reportExtension;
                resp =  this.downloadFile(rPath + nameReport + reportExtension,nameReport + reportExtension);
            }
        } catch (IOException e){

        }
        return resp;


    }

    public String getMonthYearToReport(Date date){
        int month = 0;
        int year = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        return " "+convertMothToString(month)+" - "+year ;

    }
    public String convertMothToString(int month){
        String monthString ="";
        switch (month){

            case 1 :
                return "ENERO";
            case 2 :
                return "FEBRERO";
            case 3 :
                return "MARZO";
            case 4 :
                return "ABRIL";
            case 5 :
                return  "MAYO";
            case 6 :
                return "JUNIO";
            case 7 :
                return  "JULIO";
            case 8 :
                return "AGOSTO";
            case 9 :
                return "SEPTIEMBRE";
            case 10 :
                return "OCTUBRE";
            case 11 :
                return  "NOVIEMBRE";
            case 12 :
                return  "DICIEMBRE";

        }
        log.log(Level.INFO, "obtainDirectory()");
        return  "ENERO";
    }
}

