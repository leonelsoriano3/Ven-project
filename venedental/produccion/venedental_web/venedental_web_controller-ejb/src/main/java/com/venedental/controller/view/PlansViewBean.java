package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.application.PlansApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.model.Insurances;
import com.venedental.model.Plans;
import com.venedental.model.SpecialistJob;
import com.venedental.model.TypeSpecialist;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "plansViewBean")
@ViewScoped
public class PlansViewBean extends BaseBean {
    
    @EJB
    private PlansFacadeLocal plansFacadeLocal;
    
    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;
    
    @ManagedProperty(value = "#{insurancesViewBean}")
    private InsurancesViewBean insurancesViewBean;
    
    
    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;
    
    @ManagedProperty(value = "#{plansApplicationBean}")
    private PlansApplicationBean plansApplicationBean;
    
   
    
    private static final long serialVersionUID = -5547104134772667835L;
    
    private Plans newPlansList;
    
    private Plans editPlan;
    
    public PlansViewBean() {
        this.newPlans = new String[10];
        this.editPlans = new String[10];
        this.addPlans = new Plans();
        this.visible = false;
        this.selectedPlans = new Plans();
    }
    
    @PostConstruct
    public void init() {
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.getListaPlans().addAll(this.plansFacadeLocal.findAll());
        this.getListaPlansFiltrada().addAll(this.getListaPlans());
    }
    
    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    
    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }
    
    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    
    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }
    
    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }
    
    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }
    
    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }
    
    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }
    
    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }
    
    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }
    
    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }
    
    private Plans selectedPlans;
    
    private List<Plans> listaPlans;
    
    private List<Plans> listaPlansFiltrada;
    
    
    
    private String[] newPlans;
    
    private Plans newPlansObj;
    
    private String[] editPlans;
    
    private Plans addPlans;
    
    private Plans beforeEditPlans;
    
    private boolean visible;
    
    
    
    public boolean isVisible() {
        return visible;
    }
    
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    public Plans getAddPlans() {
        return addPlans;
    }
    
    public void setAddPlans(Plans addPlans) {
        this.addPlans = addPlans;
    }
    
    public String[] getEditPlans() {
        return editPlans;
    }
    
    public void setEditPlans(String[] editPlans) {
        this.editPlans = editPlans;
    }
    
    public String[] getNewPlans() {
        return newPlans;
    }
    
    public void setNewPlans(String[] newPlans) {
        this.newPlans = newPlans;
    }
    
    public Plans getNewPlansObj() {
        return newPlansObj;
    }
    
    public void setNewPlansObj(Plans newPlansObj) {
        this.newPlansObj = newPlansObj;
    }
    
    public Plans getSelectedPlans() {
        return selectedPlans;
    }
    
    public Plans getNewPlansList() {
        return newPlansList;
    }
    
    public void setNewPlansList(Plans newPlansList) {
        this.newPlansList = newPlansList;
    }
    
    public Plans getEditPlan() {
        return editPlan;
    }
    
    public void setEditPlan(Plans editPlan) {
        this.editPlan = editPlan;
    }
    
    
    public List<Plans> getListaPlans() {
        if (this.listaPlans == null) {
            this.listaPlans = new ArrayList<>();
            this.listaPlans = this.plansFacadeLocal.findAll();
        }
        return listaPlans;
        
    }
    
    public void setListaPlans(List<Plans> listaPlans) {
        this.listaPlans = listaPlans;
    }
    
    public List<Plans> getListaPlansFiltrada() {
        if (this.listaPlansFiltrada == null) {
            this.listaPlansFiltrada = new ArrayList<>();
        }
        return listaPlansFiltrada;
    }
    
    public void setListaPlansFiltrada(List<Plans> listaPlansFiltrada) {
        this.listaPlansFiltrada = listaPlansFiltrada;
    }
    
    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }
    
    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }
    
    
    public Plans getBeforeEditPlans() {
        return beforeEditPlans;
    }
    
    public void limpiarObjetosEdit() {
        this.getComponenteSessionBean().setCamposRequeridosEdit(false);
        this.setEditPlans(new String[]{this.getBeforeEditPlans().getId().toString()});
    }
    
    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }
    
    public InsurancesViewBean getInsurancesViewBean() {
        return insurancesViewBean;
    }
    
    public void setInsurancesViewBean(InsurancesViewBean insurancesViewBean) {
        this.insurancesViewBean = insurancesViewBean;
    }
    
    public PlansApplicationBean getPlansApplicationBean() {
        return plansApplicationBean;
    }
    
    public void setPlansApplicationBean(PlansApplicationBean plansApplicationBean) {
        this.plansApplicationBean = plansApplicationBean;
    }
    
    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }
    
    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

   
    
    public void filtrarPlansInsurance(Insurances insurance) {
        this.getInsurancesViewBean().getEditInsurances();
        this.getListaPlans().clear();
        if (insurance != null) {
            for (Plans p : this.getPlansApplicationBean().getListPlans()) {
                if(p.getInsuranceId()!= null){
                    if (Objects.equals(p.getInsuranceId().getId(), insurance.getId())) {
                        this.getListaPlans().add(p);
                    }
                }
            }
        }
    }
    
    public void filtrarPlansTypeSpecialist(TypeSpecialist typeSpecialist) {
        this.getTypeSpecialistViewBean().getEditTypeSpecialist();
        this.getListaPlans().clear();
        if (typeSpecialist != null) {
            for (Plans p : this.getPlansApplicationBean().getListPlans()) {
                if(p.getTypeSpecialistId()!= null){
                    if (Objects.equals(p.getTypeSpecialistId().getId(),typeSpecialist.getId())) {
                        this.getListaPlans().add(p);
                    }
                }
            }
        }
    }
}
