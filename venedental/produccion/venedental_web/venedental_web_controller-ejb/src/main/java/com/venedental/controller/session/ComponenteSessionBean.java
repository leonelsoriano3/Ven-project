package com.venedental.controller.session;

import com.venedental.controller.BaseBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.TabChangeEvent;
import services.utils.CustomLogger;

@ManagedBean(name = "componenteSessionBean")
@SessionScoped
public class ComponenteSessionBean extends BaseBean {

    private static final long serialVersionUID = -7434469570770537682L;
    private static final Logger log = CustomLogger.getGeneralLogger(ComponenteSessionBean.class.getName());
    
    private boolean camposRequeridosEdit;

    private boolean camposRequeridosCreate;

    private boolean visibleListaClinics;
    
    private boolean visibleClinic;
    
    private boolean visibleConsult;

    public ComponenteSessionBean() {
        this.camposRequeridosCreate = false;
        this.camposRequeridosEdit = false;
        this.visibleListaClinics = false;
        this.visibleClinic = false;
        this.visibleConsult = false;
        log.log(Level.INFO, "Inició ComponenteSesionBean");
    }

    public boolean isVisibleListaClinics() {
        return visibleListaClinics;
    }

    public void setVisibleListaClinics(boolean visibleListaClinics) {
        this.visibleListaClinics = visibleListaClinics;
    }

    public boolean isCamposRequeridosEdit() {
        return camposRequeridosEdit;
    }

    public void setCamposRequeridosEdit(boolean camposRequeridosEdit) {
        this.camposRequeridosEdit = camposRequeridosEdit;
    }

    public boolean isCamposRequeridosCreate() {
        return camposRequeridosCreate;
    }

    public void setCamposRequeridosCreate(boolean camposRequeridosCreate) {
        this.camposRequeridosCreate = camposRequeridosCreate;
    }

    public boolean isVisibleClinic() {
        return visibleClinic;
    }

    public void setVisibleClinic(boolean visibleClinic) {
        this.visibleClinic = visibleClinic;
    }

    public boolean isVisibleConsult() {
        return visibleConsult;
    }

    public void setVisibleConsult(boolean visibleConsult) {
        this.visibleConsult = visibleConsult;
    }
    
    
    
    
    public void onTabChange(TabChangeEvent event) {
        if(event != null){
            
            
        System.out.println("tab id = " + event.getTab().getId());
        
            switch (event.getTab().getId()) {
                case "clinicTab":
                    this.setVisibleClinic(true);
                    break;
                case "consultab":
                    this.setVisibleConsult(true);
                    break;
            }
        
        
        } 

    }

}
