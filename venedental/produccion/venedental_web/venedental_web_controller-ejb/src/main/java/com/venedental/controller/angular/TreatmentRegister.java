/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.angular;

import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.managementSpecialist.*;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceManagementSpecialist;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.primefaces.model.StreamedContent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import java.text.SimpleDateFormat;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author akdesk01
 */
@SessionScoped
@ApplicationPath("/resources")
@Path("treatmentRegister")
public class TreatmentRegister {

    private List<ConsultPropertiesTreatmentsRTOutput> consultPropertiesTreatmentsRTOutput;

    private List<ConsultPropertiesKeysRTOutput> consultPropertiesKeysRTOutput;

    private List<ConsultTreatmentAssignedOutput> consultTreatmentAssignedOutput;

    private ConsultNameFileSupportOutput consultNameFileSupportOutput;

    private List<ConsultNameFileOutput> nameFile;

    private StreamedContent fileRequirement;

    //    private List<ConsultPropertiesTreatmentsRTOutput> consultPropertiesTreatmentsRTOutput;
    @EJB
    private ServiceCreateUser serviceCreateUser;

    @EJB
    private ServiceManagementSpecialist serviceManagementSpecialist;

    public List<ConsultPropertiesTreatmentsRTOutput> getConsultPropertiesTreatmentsRTOutput() {
        return consultPropertiesTreatmentsRTOutput;
    }

    public void setConsultPropertiesTreatmentsRTOutput(List<ConsultPropertiesTreatmentsRTOutput> consultPropertiesTreatmentsRTOutput) {
        this.consultPropertiesTreatmentsRTOutput = consultPropertiesTreatmentsRTOutput;
    }

    public List<ConsultPropertiesKeysRTOutput> getConsultPropertiesKeysRTOutput() {
        return consultPropertiesKeysRTOutput;
    }

    public void setConsultPropertiesKeysRTOutput(List<ConsultPropertiesKeysRTOutput> consultPropertiesKeysRTOutput) {
        this.consultPropertiesKeysRTOutput = consultPropertiesKeysRTOutput;
    }

    public List<ConsultTreatmentAssignedOutput> getConsultTreatmentAssignedOutput() {
        return consultTreatmentAssignedOutput;
    }

    public void setConsultTreatmentAssignedOutput(List<ConsultTreatmentAssignedOutput> consultTreatmentAssignedOutput) {
        this.consultTreatmentAssignedOutput = consultTreatmentAssignedOutput;
    }

    public ConsultNameFileSupportOutput getConsultNameFileSupportOutput() {
        return consultNameFileSupportOutput;
    }

    public void setConsultNameFileSupportOutput(ConsultNameFileSupportOutput consultNameFileSupportOutput) {
        this.consultNameFileSupportOutput = consultNameFileSupportOutput;
    }



    public StreamedContent getFileRequirement() {
        return fileRequirement;
    }

    public void setFileRequirement(StreamedContent fileRequirement) {
        this.fileRequirement = fileRequirement;
    }

    public List<ConsultNameFileOutput> getNameFile() {
        return nameFile;
    }

    public void setNameFile(List<ConsultNameFileOutput> nameFile) {
        this.nameFile = nameFile;
    }









    @Path("upload")
    @POST
    @Consumes("multipart/form-data")
    public String upload(MultipartFormDataInput multipartFormDataInput) throws IOException {
        // local variables
        MultivaluedMap<String, String> multivaluedMap = null;
        String fileName = null;
        InputStream inputStream = null;
        String uploadFilePath = null;

        try {
            Map<String, List<InputPart>> map = multipartFormDataInput.getFormDataMap();
            List<InputPart> lstInputPart = map.get("file");

            for (InputPart inputPart : lstInputPart) {

                // get filename to be uploaded
                multivaluedMap = inputPart.getHeaders();
                fileName = getFileName(multivaluedMap);

                if (null != fileName && !"".equalsIgnoreCase(fileName)) {

                    // write & upload file to UPLOAD_FILE_SERVER
                    inputStream = inputPart.getBody(InputStream.class, null);

                    uploadFilePath = writeToFileServer(inputStream, fileName);


                    // close the stream
                    inputStream.close();
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            // release resources, if any
        }

        return fileName;
    }

    public  String remove1(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i=0; i<original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }//remove1
//

    /**
     *
     * @param multivaluedMap
     * @return
     */
    private String getFileName(MultivaluedMap<String, String> multivaluedMap) throws UnsupportedEncodingException {

        Date date = new Date();
        DateFormat dateF = new SimpleDateFormat("MM-dd-yyyy-HH");
        String fileDate = dateF.format(date);
        //  Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
        //         new String(ByteStreams.toByteArray(multivaluedMap), ISO_8859_1);
        String datos="";
        String utf8="";

        String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {

            if ((filename.trim().startsWith("filename"))) {
//                // Vemos si el formato entrante es ASCII o UTF8
//CharsetEncoder isoEncoder = Charset.forName("ISO-8859-15").newEncoder();
//                CharsetEncoder utf8Encoder = Charset.forName("UTF-8").newEncoder();
//                Boolean isISO = isoEncoder.canEncode(filename);
//                Boolean isUTF8 = utf8Encoder.canEncode(filename);
////
////
////// Convertir de UTF-8 a ISO-8859-1
//                if (isISO == false) {
//                    Charset utf8charset = Charset.forName("UTF-8");
//                    Charset iso88591charset = Charset.forName("ISO-8859-15");
//
//                    // Decode UTF-8
//                    ByteBuffer bb = ByteBuffer.wrap(filename.getBytes());
//                    CharBuffer data = utf8charset.decode(bb);
//
//                    // Encode ISO-8559-1
//                    ByteBuffer outputBuffer = iso88591charset.encode(data);
//                    byte[] outputData = outputBuffer.array();
//                     utf8 =filename;
//                    utf8 = new String(filename.getBytes("UTF-8"), "ISO-8859-1");
//
//                    datos = new String(outputData);
//                }
//
                String[] name = filename.split("=");
                String MultipartFileName = name[1].trim().replaceAll("\"", "");
                String nameRecaudo=MultipartFileName.replace("\"", "");
                String tempFileName = nameRecaudo.replaceAll(" ", "_");

                String nameforServer=remove1(tempFileName);
                System.out.println("2. " +remove1(nameforServer));
                String exactFileName=fileDate+"_"+nameforServer;


                System.out.println("fecha de archivo" + fileDate);
                System.out.println("nombre de archivo" + exactFileName);
                System.out.println("com.venedental.controller.angular.TreatmentRegister.getFileName()"+utf8);
                System.out.println("com.venedental.controller.angular.TreatmentRegister.getFileName()"+datos);
                return exactFileName;
                // Vemos si el formato entrante es ASCII o UTF8
//CharsetEncoder isoEncoder = Charset.forName("ISO-8859-15").newEncoder();
//                CharsetEncoder utf8Encoder = Charset.forName("UTF-8").newEncoder();
//                Boolean isISO = isoEncoder.canEncode(filename);
//                Boolean isUTF8 = utf8Encoder.canEncode(filename);
//
//
//// Convertir de UTF-8 a ISO-8859-1
//                if (isISO == false) {
//                    Charset utf8charset = Charset.forName("UTF-8");
//                    Charset iso88591charset = Charset.forName("ISO-8859-15");
//
//                    // Decode UTF-8
//                    ByteBuffer bb = ByteBuffer.wrap(filename.getBytes());
//                    CharBuffer data = utf8charset.decode(bb);
//
//                    // Encode ISO-8559-1
//                    ByteBuffer outputBuffer = iso88591charset.encode(data);
//                    byte[] outputData = outputBuffer.array();
//
//                    datos = new String(outputData);
//                }
//                String s = new String(filename.getBytes("UTF-8"), "ISO-8859-1");
//                 String hola= new String (filename.getBytes ("ISO-8859-15"), "UTF-8");
//                 String dos=new String(filename.getBytes(), "ISO-8859-15");

//                System.out.println("com.venedental.controller.angular.TreatmentRegister.getFileName()"+datos);
//     Charset UTF_8 = Charset.forName("UTF-8");
//                byte hola[]=tempFileName.getBytes(ISO_8859_1);
//                String value = new String(hola, UTF_8);

            }

        }

        return "UnknownFile";
    }

    /**
     *
     * @param inputStream
     * @param fileName
     * @throws IOException
     */
    private String writeToFileServer(InputStream inputStream, String fileName) throws IOException {

        String UPLOAD_FILE_SERVER = "/home/ftp/recaudos/";
        OutputStream outputStream = null;
        String qualifiedUploadFilePath = UPLOAD_FILE_SERVER + fileName;

        try {
            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            //release resource, if any
            outputStream.close();
        }
        return qualifiedUploadFilePath;
    }

    //Busca los tratamientos sin pieza
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getListTreatmentWithoutPieceRT")
    public List<FindTreatmentWithoutPieceOut> getListTreatmentWithoutPiece(@QueryParam("key") Integer key) {

        List<FindTreatmentWithoutPieceOut> list;
        list = this.serviceManagementSpecialist.getListTreatmentWithoutPiece(key);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.getListTreatmentWithoutPiece()" + list);

        if (list.size() == 0) {
            FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = null;
            findTreatmentWithoutPieceOut.setIdTreament(-1);
            list.add(findTreatmentWithoutPieceOut);
            return list;

        } else {

            return list;
        }

    }

    //Busca los tratamientos de las piezas para el combo
    @Path("findPropertiesTreatments")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<ConsultPropertiesTreatmentsRTOutput> findPropertiesTreatments(@QueryParam("idKey") Integer idKey, @QueryParam("idPiece") Integer idPiece) {
        this.setConsultPropertiesTreatmentsRTOutput(this.serviceManagementSpecialist.ConsultPropertiesTreatmentsRegisterT(idKey, idPiece));

        return this.consultPropertiesTreatmentsRTOutput;
    }

    //Busca los tratamientos asignados a una pieza
    @Path("findPropertiesForKey")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<ConsultPropertiesKeysRTOutput> findPropertiesForKey(@QueryParam("idKey") Integer idKey,@QueryParam("idPiece") Integer idPiece) throws SQLException, ParseException {
        this.setConsultPropertiesKeysRTOutput(this.serviceManagementSpecialist.ConsultPropertiesKeyRegisterT(idKey,idPiece));

        for (ConsultPropertiesKeysRTOutput consultPropertiesKeysRTOutput : this.consultPropertiesKeysRTOutput) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            String dateInString = formatter.format(consultPropertiesKeysRTOutput.getDateOrigin());
            Date dateTreatment = formatter.parse(dateInString);
            consultPropertiesKeysRTOutput.setDate(dateInString);

            this.consultFileName(consultPropertiesKeysRTOutput.getId_piece_treatment_key());
            if(nameFile==null){
                consultPropertiesKeysRTOutput.setListRecaudos(null);

            }else{


                consultPropertiesKeysRTOutput.setListRecaudos(nameFile);
            }
        }

        return this.consultPropertiesKeysRTOutput;
    }

    //Elimina las piezas que fueron asignadas en registrar diagnostico
    @Path("deletePropertiesKey")
    @GET

    public Integer deletePropertiesKey(@QueryParam("idPieceTreatmentKey") Integer idPieceTreatmentKey) throws SQLException {
        this.serviceManagementSpecialist.DeletePropertiesKey(idPieceTreatmentKey);

        return idPieceTreatmentKey;
    }

    //Elimina los tratamientos que fueron agregados en registrar tratamiento
    @Path("deleteTreatmentsKey")
    @GET

    public Integer deleteTreatmentsKey(@QueryParam("idTreatmentKey") Integer idTreatmentKey) throws SQLException {
        this.serviceManagementSpecialist.DeleteTreatmentsKey(idTreatmentKey);

        return idTreatmentKey;
    }

//    @Path("downloadFileSupport")
//    @GET
//    public List<ConsultNameFileSupportOutput> downloadFileSupport(@QueryParam("idTrearmentKey") Integer idTrearmentKey) {
//        String UPLOAD_FILE_SERVER = "/home/ftp/recaudos/";
//        InputStream inputstream = null;
//
//        try {
//
////Consultar los nombres de los archivos asociados a esa clave, con sus tratamientos.
//            this.setConsultNameFileSupportOutput(this.serviceManagementSpecialist.ConsultnameFile(idTrearmentKey));
//            for (ConsultNameFileSupportOutput consultNameFileSupportOutput : this.consultNameFileSupportOutput) {
//                this.setFileRequirement(new DefaultStreamedContent(inputstream, "application/pdf", consultNameFileSupportOutput.getNameFileSupport()));
//
//            }
//            inputstream = new FileInputStream(UPLOAD_FILE_SERVER);
//            this.setFileRequirement(new DefaultStreamedContent(inputstream, "application/png", UPLOAD_FILE_SERVER));
//        } catch (Exception e) {
//
//        }
//        return consultNameFileSupportOutput;
//    }

    //Busca los tratamientos sin pieza asignados a la clave
    @Path("findTreatmentAssignedKey")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<ConsultTreatmentAssignedOutput> findTreatmentAssignedKey(@QueryParam("idKey") Integer idKey) throws SQLException {
        this.setConsultTreatmentAssignedOutput(this.serviceManagementSpecialist.ConsultTreatmentAssignedforKey(idKey));

        return this.consultTreatmentAssignedOutput;
    }

//


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("insertTreatmentKey")
    public InsertPlantTreatmentKeyOut InsertPlantTreamentKey(@QueryParam("key") Integer key,
                                                             @QueryParam("idPlanTreat") Integer idPlanTreat,
                                                             @QueryParam("idBareTreat") Integer idBareTreat,
                                                             @QueryParam("date") String date) throws SQLException, ParseException {


        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = date;
        Date dateTreatment = formatter.parse(dateInString);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertPlantTreamentKey() fecha tratamiento general"+dateTreatment);
        Integer generado = EstatusPlansTreatmentsKeys.GENERADO.getValor();

        InsertPlantTreatmentKeyOut insertPlantTreatmentKeyOut = null;

        insertPlantTreatmentKeyOut = serviceManagementSpecialist.InsertTreatmentForKey(key,
                idPlanTreat, dateTreatment, generado, idBareTreat, null, this.getIdUserLogged(), null);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertPlantTreamentKey()"+insertPlantTreatmentKeyOut);
//        updateToothTreatment(insertPlantTreatmentKeyOut.getIdTreatmentKey(), idPiece, idBareT, dateTreatment);

        return insertPlantTreatmentKeyOut;

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("insertTreatmentofKey")
    public InsertPlantTreatmentKeyOut InsertTreamentOfKey(@QueryParam("key") Integer key,
                                                          @QueryParam("idPlanTreat") Integer idPlanTreat,
                                                          @QueryParam("idBareTreat") Integer idBareTreat,
                                                          @QueryParam("date") String date,
                                                          @QueryParam("idPieceT") Integer idPieceT,
                                                          @QueryParam("idBareT") Integer idBareT,
                                                          @QueryParam("idBarePiece") Integer idBarePiece) throws SQLException, ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = date;
        Date dateTreatment = formatter.parse(dateInString);
////      SimpleDateFormat format = new SimpleDateFormat("DD/MM/YYYY");
//        Date dateTreatment = format.parse(date);
        System.out.println(date);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertPlantTreamentKey() fecha tratamiento con pieza"+dateTreatment);
        Integer generado = EstatusPlansTreatmentsKeys.GENERADO.getValor();

        InsertPlantTreatmentKeyOut insertPlantTreatmentKeyOut = null;

        insertPlantTreatmentKeyOut = serviceManagementSpecialist.InsertTreatmentForKey(key,
                idPlanTreat, dateTreatment, generado, idBareTreat, null, this.getIdUserLogged(), null);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertPlantTreamentKey()"+insertPlantTreatmentKeyOut);
        PropertiesPlansTreatmentsKey output = null;
        output = serviceManagementSpecialist.InsertPropertiesForKey(insertPlantTreatmentKeyOut.getIdTreatmentKey(),
                idPieceT, idBarePiece, dateTreatment, generado, null, this.getIdUserLogged(), null);
//        this.InsertFileName(output.getId(), "hola");
        insertPlantTreatmentKeyOut.setIdPropertiesPlanTreatmentKey(output.getId());

        return insertPlantTreatmentKeyOut;

    }

    public Integer getIdUserLogged() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        return user.getId();
    }





    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateTooth")
    public PropertiesPlansTreatmentsKey updateToothTreatment(@QueryParam("id") Integer id,
                                                             @QueryParam("idTreatPiece") Integer idPieceTreatment,
                                                             @QueryParam("idBareTreatPiece") Integer idPriceTreatment,
                                                             @QueryParam("date") Date date) throws SQLException {
        PropertiesPlansTreatmentsKey output = null;
        Integer generado = EstatusPlansTreatmentsKeys.GENERADO.getValor();
        output = serviceManagementSpecialist.InsertPropertiesForKey(id,
                idPieceTreatment, idPriceTreatment, date, generado, null, this.getIdUserLogged(), null);
        System.out.println("com.venedental.controller.angular.TreatmentRegister.updateToothTreatment()"+output.getNameStatusProTreatments());
        return output;

    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("saveFileName")
    public String InsertFileName(@QueryParam("idTreatmentPiece") Integer idTreatmentPiece,
                                 @QueryParam("name") String name) throws SQLException {

//        String path="/home/ftp/recaudos/"+name+"";
        // String path="/home/ftp/recaudos/"+name+"";

        //my server windows

        //   String path="C:\\recaudos\\"+name+"";
        Date date = new Date();
        DateFormat dateF = new SimpleDateFormat("MM-dd-yyyy-HH");
        String partName = dateF.format(date);
        String nameFile=partName+"_"+name;
        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertFileName()"+nameFile);


        this.serviceManagementSpecialist.InsertFileSupport(idTreatmentPiece, nameFile);
        return name;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFileName")
    public List<ConsultNameFileOutput> consultFileName(@QueryParam("idTreatmentPiece") Integer idTreatmentPiece) throws SQLException {

        this.setNameFile(this.serviceManagementSpecialist.ConsultnameFile(idTreatmentPiece));



        return nameFile;

    }


    @POST
    @Path("downloadTreatRep")
    @Produces("application/png")
    public Response downloadExcelFile(@QueryParam("nameFile") String nameFile) {
        // set file (and path) to be download
        String path= "/home/ftp/recaudos/";
        File file = new File(path+nameFile);
        String filename=nameFile;

        Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename="+filename+" ");

        return responseBuilder.build();

    }

    @POST
    @Path("downloadFile")
    @Produces("application/png")
    public Response downloadFile(@QueryParam("nameFile") String nameFile) {
        // set file (and path) to be download
        String path= "/home/ftp/recaudos/";
        File file = new File(path+nameFile);
        String filename=nameFile;

        Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
        //    responseBuilder.header("Content-Disposition", "attachment; filename=\"+"+filename+"+\"");
        responseBuilder.header("Content-Disposition", "attachment; filename="+filename+" ");
        return responseBuilder.build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFileNameRecaudos")
    public List <ConsultNameFileOutput> consultFileNameRecaudos(@QueryParam("idTreatmentPiece") Integer idTreatmentPiece) throws SQLException {

        this.setNameFile(this.serviceManagementSpecialist.ConsultNameFileRecaudos(idTreatmentPiece));

        return nameFile;

    }


}
