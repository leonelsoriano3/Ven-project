
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.angular;


import com.venedental.controller.view.SecurityViewBean;
import static com.venedental.controller.view.SecurityViewBean.rolSpecialist;
import com.venedental.dto.consultKeys.ConsultDateDetailTreatmentOutput;
import com.venedental.dto.consultKeys.ConsultDateMinDetalleOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsOutput;
import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import com.venedental.dto.managementSpecialist.ConsultKeysSpecialistOutput;
import com.venedental.dto.managementSpecialist.FindDataSpecialistOutput;
import com.venedental.dto.managementSpecialist.FindInfoSpecialistOutput;
import com.venedental.dto.managementSpecialist.FindPieceByidKeyOut;
import com.venedental.dto.managementSpecialist.FindPropertyByKeyIdOut;
import com.venedental.dto.managementSpecialist.FindRolUserOutput;
import com.venedental.dto.managementSpecialist.FindTreatmentWithoutPieceOut;
import com.venedental.dto.managementSpecialist.InsertPlantTreatmentKeyIn;
import com.venedental.dto.managementSpecialist.InsertPlantTreatmentKeyOut;
import com.venedental.dto.managementSpecialist.InsertProplatreKeyIn;
import com.venedental.dto.managementSpecialist.InsertProplatreKeyOut;
import com.venedental.dto.medicalOffice.FindMedicalOfficeBySepcialistOutput;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.enums.RolUser;
import com.venedental.facade.RolUsersFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.model.MedicalOffice;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServiceSpecialists;
import com.venedental.model.Specialists;
import com.venedental.model.security.RolUsers;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceMedicalOffice;
import com.venedental.services.ServicePlansPatients;
import com.venedental.services.ServiceManagementSpecialist;
import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.List;
import javax.faces.bean.ManagedProperty;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
/**
 *
 * @author akdesk01
 */



@SessionScoped
@ApplicationPath("/resources")
@Path("ConsultKeysSpecialist")
public class ConsultKeysSpecialist extends Application {
    
    private List<FindRolUserOutput> findRolUserOutput;
    
    private ConsultKeysSpecialistOutput consultKeysSpecialistOutput;
    
    private List <ConsultKeysSpecialistOutput> consultKeysSpecialistOutputList;
    
    private List<ConsultKeysPatientsDetailOutput> consultKeysPatientsDetailOutputList;
    
    private ConsultKeysPatientsDetailOutput consultKeysPatientsDetailOutput;
    
    private List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutput;
    
   private List<FindDataSpecialistOutput> findDataSpecialistOutput;
   
    private List <ConsultDateDetailTreatmentOutput> consultDateDetailTreatmentOutput;

    public List<ConsultDateDetailTreatmentOutput> getConsultDateDetailTreatmentOutput() {
        return consultDateDetailTreatmentOutput;
    }

    public void setConsultDateDetailTreatmentOutput(List<ConsultDateDetailTreatmentOutput> consultDateDetailTreatmentOutput) {
        this.consultDateDetailTreatmentOutput = consultDateDetailTreatmentOutput;
    }
   
   private FindDataSpecialistOutput findDataSpecialist;
   
   private FindDataSpecialistOutput especialista = null;
   
    private List<FindMedicalOfficeBySepcialistOutput> findMedicalOfficeBySepcialistOutput;
    private List<MedicalOffice> medicalOfficeList;
    
    
    
    private boolean rolSpecialista = false;
    private boolean medicalOficceSelection;
    private Integer idSpeciality,idUser,idSpecialist;
    
    
    

    
    @EJB
    private ServiceSpecialists serviceSpecialists;
    
     @EJB
    private ServicePlansPatients servicePlansPatients;
     
     @EJB
    private UsersFacadeLocal usersFacadeLocal;
     
     
     @EJB
    private ServiceMedicalOffice serviceMedicalOffice;
     
     @EJB
     private ServiceManagementSpecialist serviceManagementSpecialist;
     @EJB
    private ServiceCreateUser serviceCreateUser;

     
     
     
     
     @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

     
      @EJB
    private RolUsersFacadeLocal rolUsersFacadeLocal;

    public List<FindRolUserOutput> getFindRolUserOutput() {
        return findRolUserOutput;
    }

    public void setFindRolUserOutput(List<FindRolUserOutput> findRolUserOutput) {
        this.findRolUserOutput = findRolUserOutput;
    }

    public boolean isRolSpecialista() {
        return rolSpecialista;
    }

    public void setRolSpecialista(boolean rolSpecialista) {
        this.rolSpecialista = rolSpecialista;
    }

    public List<FindDataSpecialistOutput> getFindDataSpecialistOutput() {
        return findDataSpecialistOutput;
    }

    public void setFindDataSpecialistOutput(List<FindDataSpecialistOutput> findDataSpecialistOutput) {
        this.findDataSpecialistOutput = findDataSpecialistOutput;
    }

    public Integer getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(Integer idSpeciality) {
        this.idSpeciality = idSpeciality;
    }
    
    
     public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public FindDataSpecialistOutput getFindDataSpecialist() {
        return findDataSpecialist;
    }

    public void setFindDataSpecialist(FindDataSpecialistOutput findDataSpecialist) {
        this.findDataSpecialist = findDataSpecialist;
    }
    
     public List<MedicalOffice> getMedicalOfficeList() {
        return medicalOfficeList;
    }

    public void setMedicalOfficeList(List<MedicalOffice> medicalOfficeList) {
        this.medicalOfficeList = medicalOfficeList;
    }

    public boolean isMedicalOficceSelection() {
        return medicalOficceSelection;
    }

    public void setMedicalOficceSelection(boolean medicalOficceSelection) {
        this.medicalOficceSelection = medicalOficceSelection;
    }

    public FindDataSpecialistOutput getEspecialista() {
        return especialista;
    }

    public void setEspecialista(FindDataSpecialistOutput especialista) {
        this.especialista = especialista;
    }

    public ConsultKeysSpecialistOutput getConsultKeysSpecialistOutput() {
        return consultKeysSpecialistOutput;
    }

    public void setConsultKeysSpecialistOutput(ConsultKeysSpecialistOutput consultKeysSpecialistOutput) {
        this.consultKeysSpecialistOutput = consultKeysSpecialistOutput;
    }

    public List<ConsultKeysSpecialistOutput> getConsultKeysSpecialistOutputList() {
        return consultKeysSpecialistOutputList;
    }

    public void setConsultKeysSpecialistOutputList(List<ConsultKeysSpecialistOutput> consultKeysSpecialistOutputList) {
        this.consultKeysSpecialistOutputList = consultKeysSpecialistOutputList;
    }

    

    public List<ConsultKeysPatientsDetailTreatmentOutput> getConsultKeysPatientsDetailTreatmentOutput() {
        return consultKeysPatientsDetailTreatmentOutput;
    }

    public void setConsultKeysPatientsDetailTreatmentOutput(List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutput) {
        this.consultKeysPatientsDetailTreatmentOutput = consultKeysPatientsDetailTreatmentOutput;
    }

    public ConsultKeysPatientsDetailOutput getConsultKeysPatientsDetailOutput() {
        return consultKeysPatientsDetailOutput;
    }

    public void setConsultKeysPatientsDetailOutput(ConsultKeysPatientsDetailOutput consultKeysPatientsDetailOutput) {
        this.consultKeysPatientsDetailOutput = consultKeysPatientsDetailOutput;
    }

    public List<ConsultKeysPatientsDetailOutput> getConsultKeysPatientsDetailOutputList() {
        return consultKeysPatientsDetailOutputList;
    }

    public void setConsultKeysPatientsDetailOutputList(List<ConsultKeysPatientsDetailOutput> consultKeysPatientsDetailOutputList) {
        this.consultKeysPatientsDetailOutputList = consultKeysPatientsDetailOutputList;
    }
    
     
    @Path("findKeys")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    
    public List <ConsultKeysSpecialistOutput> findKeys(@QueryParam("idSpecialistforFind") Integer idSpecialist,
                                                       @QueryParam("startdatekeys") String startdatekeys,
                                                       @QueryParam("enddatekeys") String enddatekeys,
                                                       @QueryParam("patient") String patient,
                                                       @QueryParam("Key") String Key) throws ParseException {
        if(patient.equals("")){
            patient=null;
        }
        if(Key.equals("")){
            Key=null;
        }
        java.util.Date fechaInicio;
        java.util.Date fechaFin;
        java.sql.Date sqlFechaInicio = null;
        java.sql.Date sqlFechaFin = null;
        if(startdatekeys!=null && enddatekeys!=null && !enddatekeys.equals("") && !startdatekeys.equals("")){
            String fechaStartKeys=startdatekeys;
         String fechaFinKeys=enddatekeys;
       fechaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(fechaStartKeys);
        fechaFin= new SimpleDateFormat("dd/MM/yyyy").parse(fechaFinKeys);
         sqlFechaInicio= new java.sql.Date(fechaInicio.getTime());
         sqlFechaFin= new java.sql.Date(fechaFin.getTime());
        }
        
        //lista de fechas consultadas
       this.setConsultKeysSpecialistOutputList(this.serviceManagementSpecialist.findKeysSpecialist(idSpecialist, sqlFechaInicio, sqlFechaFin,Key, patient));
        if(this.consultKeysSpecialistOutputList!=null){
            for(ConsultKeysSpecialistOutput consultKeysSpecialistOutput: this.consultKeysSpecialistOutputList){
             String fecha = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysSpecialistOutput.getDateKey());
             String fechafiltro = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysSpecialistOutput.getDateStart());
             String fechaEndFilter= new SimpleDateFormat("dd/MM/yyyy").format(consultKeysSpecialistOutput.getDateEnd());
        java.util.Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        consultKeysSpecialistOutput.setDateKeyTable(fecha);
        consultKeysSpecialistOutput.setDateMinFilter(fechafiltro);
        consultKeysSpecialistOutput.setDataEndDateFilter(fechaEndFilter);
        }
       
        
        //formatear fecha desde - hasta
//        startdatekeys = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysSpecialistOutput.getDateStart());
//        consultKeysSpecialistOutput.setDateStartS(startdatekeys);
//       enddatekeys = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysSpecialistOutput.getDateEnd());
//        consultKeysSpecialistOutput.setDateEndS(enddatekeys);
        
        
        }
        return this.consultKeysSpecialistOutputList;
    }
    
    
    @Path("obtenerUsuario")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Integer obtenerUsuario() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = this.usersFacadeLocal.findByLogin(auth.getName());
        this.setIdUser(user.getId());
        return idUser;

    }
    
    @Path("findDataSpecialist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List <FindDataSpecialistOutput> findDataForSpecialist() {

        this.obtenerUsuario();
        this.findUserRolSpecialist(this.getIdUser());
//        FindDataSpecialistOutput especialistaInformacion = new FindDataSpecialistOutput();
        if (this.isRolSpecialista() == true) {
          this.setFindDataSpecialistOutput(serviceSpecialists.findDataSpecialist(this.getIdUser()));
          Integer cantidadRegistro=this.findDataSpecialistOutput.size();
          this.getFindDataSpecialistOutput();
          
          for(FindDataSpecialistOutput findDataSpecialistOutput: this.findDataSpecialistOutput){
              this.idSpeciality = findDataSpecialistOutput.getIdSpeciality();
            this.setIdSpecialist(findDataSpecialistOutput.getId_Specialist());
            
          
           if(cantidadRegistro>1){
               
            findDataSpecialistOutput.setMedicalOfficeVarious(1);
            
        }else{
            findDataSpecialistOutput.setMedicalOfficeVarious(0);
        }  
      }   
    }
       
        return findDataSpecialistOutput;
    }
    
    

    @Path("findUserRolSpecialist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public List<FindRolUserOutput> findUserRolSpecialist(@QueryParam("idUser") Integer idUser) {
        this.setFindRolUserOutput(this.serviceSpecialists.findRolByUser(idUser));
        for (FindRolUserOutput findRolUserOutput : this.findRolUserOutput) {
            if (findRolUserOutput.getIdRol().equals(RolUser.ROLE_ESPECIALISTA.getValor())) {
                this.setRolSpecialista(true);
            }

        }
        return this.findRolUserOutput;
    }
    
    /**
     * Funcion designada para cargar los valores del dialogo de detalles
     *
     * @param consultKeysPatientsOutput
     */
    
    /**
     * Funcion designada para cargar los valores del dialogo de detalles
     * @param idKey
     */
    @Path("cargarDetalleClave")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ConsultKeysPatientsDetailOutput chargeDetail(@QueryParam("idKey") Integer idKey) {
        
        
        this.setConsultKeysPatientsDetailOutput(this.serviceSpecialists.detailsConsultKeysSpecialist(idKey));
            String dateKeyDetail = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysPatientsDetailOutput.getDateKey());
            String dateExpirationDetail=new SimpleDateFormat("dd/MM/yyyy").format(consultKeysPatientsDetailOutput.getExpiratioKey());
            String dateBrithayDetail=new SimpleDateFormat("dd/MM/yyyy").format(consultKeysPatientsDetailOutput.getBrithayPatient());
        consultKeysPatientsDetailOutput.setDateKeyForDetail(dateKeyDetail);
        consultKeysPatientsDetailOutput.setExpirationKeyDetail(dateExpirationDetail);
        consultKeysPatientsDetailOutput.setBrithayPatientDetail(dateBrithayDetail);
       
       
        
       
//        this.chargeDetailTreatment(idKey);
        
        
        return this.consultKeysPatientsDetailOutput;
        
    }
    
    @Path("cargarDetalleClaveTratamiento")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ConsultKeysPatientsDetailTreatmentOutput>  chargeDetailTreatment(@QueryParam("idKey") Integer idKey) throws ParseException {
 // String fecha = ("12/12/2016");
//        java.util.Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);

        this.setConsultKeysPatientsDetailTreatmentOutput(this.serviceManagementSpecialist.detailsConsultKeysPatientsTreatments(idKey));
        for(ConsultKeysPatientsDetailTreatmentOutput consultKeysPatientsDetailTreatmentOutput:this.getConsultKeysPatientsDetailTreatmentOutput()){
          String fechaFilterDetail =new SimpleDateFormat("dd/MM/yyyy").format(consultKeysPatientsDetailTreatmentOutput.getDateForTreatment());
//           String fecha = new SimpleDateFormat("dd/MM/yyyy").format(consultKeysPatientsDetailTreatmentOutput.getDateForTreatment());
//           java.util.Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            consultKeysPatientsDetailTreatmentOutput.setDateForTreatment(consultKeysPatientsDetailTreatmentOutput.getDateForTreatment());
            consultKeysPatientsDetailTreatmentOutput.setNumberPiece(consultKeysPatientsDetailTreatmentOutput.getNumberPiece());
            consultKeysPatientsDetailTreatmentOutput.setNameTreatment(consultKeysPatientsDetailTreatmentOutput.getNameTreatment());
            consultKeysPatientsDetailTreatmentOutput.setDateFilterTreatment(fechaFilterDetail);
        }
         return this.consultKeysPatientsDetailTreatmentOutput;
        
    }
    
    
    /*
    * Metodo que permite eliminar una clave logicamente
    */
    @Path("deleteKey")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Integer deleteKey(@QueryParam("idKeyForDelete") Integer idKeyForDelete,@QueryParam("observation") String observation) throws SQLException {
        Integer generate = EstatusPlansTreatmentsKeys.GENERADO.getValor();
        Integer delete = EstatusPlansTreatmentsKeys.ELIMINADO.getValor();
        this.obtenerUsuario();
       this.serviceManagementSpecialist.reasonDeleteKey(idKeyForDelete, observation, idUser);
       Integer deleteKey=1;
        this.setConsultKeysPatientsDetailOutputList(this.serviceManagementSpecialist.detailsConsultKeysPatients(idKeyForDelete));
        
         return deleteKey;
        
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getListTreatmentWithoutPiece")
    public List<FindTreatmentWithoutPieceOut> getListTreatmentWithoutPiece(@QueryParam("key") Integer key){

        List<FindTreatmentWithoutPieceOut> list;
        list = this.serviceManagementSpecialist.getListTreatmentWithoutPiece(key);

        if(list.size() == 0){
            FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = null;
            findTreatmentWithoutPieceOut.setIdTreament(-1);
            list.add(findTreatmentWithoutPieceOut);
            return list;

        }
        else{

            return list;

        }

    }
    
    
     @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("filterSpecialist")
    public String findDataKey() {
       // DataKeyDTO dataKeyDTO = serviceManagementSpecialist.findKeysToAuditBySpecialistAndDate(14671);
//        List<Specialists>  tmp = serviceSpecialists.findByIdentityOrName(specialist);
        return  "hola";
    }
/*


 */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFindKeysToAuditByTooth")
    public  List<FindPropertyByKeyIdOut>   getTreatmentstoTooth(@QueryParam("key") Integer key,
                                                        @QueryParam("tooth") Integer tooth) {

        FindPropertyByKeyIdOut findPropertyByKeyIdOut = new FindPropertyByKeyIdOut();
        List<FindPropertyByKeyIdOut>  findPropertyByKeyIdOuts = null;
        findPropertyByKeyIdOuts = this.serviceManagementSpecialist.getTreatmentToTooth(key,tooth);
        if (findPropertyByKeyIdOut  == null) {

            findPropertyByKeyIdOut  = new FindPropertyByKeyIdOut();
            findPropertyByKeyIdOut.setIdPiece(-1);
            findPropertyByKeyIdOuts.add(findPropertyByKeyIdOut);
            return findPropertyByKeyIdOuts;
        }
        else {
            return findPropertyByKeyIdOuts ;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFindPiece")
    public List<FindPieceByidKeyOut> getFindPiece(@QueryParam("key") Integer key){


        List<FindPieceByidKeyOut> list;

        list = this.serviceManagementSpecialist.findPiece(key);

        if(list == null){

            FindPieceByidKeyOut findPieceByidKeyOut = new FindPieceByidKeyOut();
            findPieceByidKeyOut.setIdPiece(-1);
            list.add(findPieceByidKeyOut);
            return list;

        }
        else{
            return list;

        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFindKeysSpecialist")
    public List<ConsultKeysSpecialistOutput> getFindKeysSpecialist(@QueryParam("idSpecialist") Integer idSpecialist,
                                                                @QueryParam("dateStart")Long dateStart,
                                                                @QueryParam("dateEnd") Long dateEnd,
                                                                @QueryParam("numberForKey") String numberForKey,
                                                                @QueryParam("patient") String patient){

         List<ConsultKeysSpecialistOutput> list;
        java.sql.Date sqlDateS = new java.sql.Date(dateStart);
        java.sql.Date sqlDateE = new java.sql.Date(dateEnd);

        list = this.serviceManagementSpecialist.findKeysSpecialist(idSpecialist,sqlDateS,sqlDateE,numberForKey,patient);

        if(list == null){

            ConsultKeysSpecialistOutput consultKeysSpecialistOutput = new ConsultKeysSpecialistOutput();
            consultKeysSpecialistOutput.setIdKey(-1);
            list.add(consultKeysSpecialistOutput);

            return list;
        }
        else {

            return list;
        }

     }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFindInfoSpecialist")
    public FindInfoSpecialistOutput getFindInfoSpecialist(@QueryParam("key") Integer key) throws ParseException {

        FindInfoSpecialistOutput findInfoSpecialistOutput = null;

        findInfoSpecialistOutput = serviceManagementSpecialist.findInfoSpecialist(key);

        if(findInfoSpecialistOutput == null){

            findInfoSpecialistOutput.setIdKey(-1);
            return  findInfoSpecialistOutput;

        }else
        {
            String fecha = new SimpleDateFormat("dd/MM/yyyy").format(findInfoSpecialistOutput.getExpiredDate());
            java.util.Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            findInfoSpecialistOutput.setExpDate(fecha);
            fecha = new SimpleDateFormat("dd/MM/yyyy").format(findInfoSpecialistOutput.getApplicationDate());
            fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            findInfoSpecialistOutput.setAppDate(fecha);
            fecha = new SimpleDateFormat("dd/MM/yyyy").format(findInfoSpecialistOutput.getBirthDate());
            fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            findInfoSpecialistOutput.setbDate(fecha);

            return findInfoSpecialistOutput;


        }

    }

   


//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("insertPlanTreatmentKey")
//    public InsertPlantTreatmentKeyOut InsertPlantTreamentKey(@QueryParam("key") Integer key,
//                                                             @QueryParam("idPlanTreat") Integer idPlanTreat,
//                                                             @QueryParam("idBareTreat") Integer idBareTreat){
//
//
//
//
//        InsertPlantTreatmentKeyIn insertPlantTreatmentKeyIn = new InsertPlantTreatmentKeyIn(key,
//                             idPlanTreat,null,1,idBareTreat,null,this.getIdUserLogged(),null,1);
//
//
//        InsertPlantTreatmentKeyOut insertPlantTreatmentKeyOut = null;
//
//        insertPlantTreatmentKeyOut = serviceManagementSpecialist.InsertPlantTreamentKey(insertPlantTreatmentKeyIn);
//
//        return insertPlantTreatmentKeyOut;
//
//    }

    public Integer getIdUserLogged(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        return user.getId();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("updateToothTreatment")
    public InsertProplatreKeyOut updateToothTreatment(@QueryParam("idTreatKey") Integer idTreatKey,
                                                     @QueryParam("idTreatPiece") Integer idTreatPiece,
                                                     @QueryParam("idBareTreatPiece") Integer idBareTreatPiece,
                                                      @QueryParam("idKeyTreatDiag") Integer idKeyTreatDiag){

        InsertProplatreKeyOut output = null;
        InsertProplatreKeyIn input = new InsertProplatreKeyIn(idTreatKey,idTreatPiece,null,1,idBareTreatPiece,null,null,this.getIdUserLogged(),idKeyTreatDiag);


        output = serviceManagementSpecialist.InsertProplatreKeyService(input);

        return output;


    }
    
    
      @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getListTreatmentWP")
    public List<FindTreatmentWithoutPieceOut> getListTreatmentWP(@QueryParam("key") Integer key){

        List<FindTreatmentWithoutPieceOut> list;
        list = this.serviceManagementSpecialist.getListTreatmentWithoutPiece(key);

        if(list.size() == 0){
            FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = null;
            findTreatmentWithoutPieceOut.setIdTreament(-1);
            list.add(findTreatmentWithoutPieceOut);
            return list;

        }
        else{
            for(FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut:list){
                if(findTreatmentWithoutPieceOut.getTreatmentAssigned().equals(1)){
                    findTreatmentWithoutPieceOut.setIsTreatmentAssigned(true);
                }else{
                    findTreatmentWithoutPieceOut.setIsTreatmentAssigned(false);
                }
            }

            return list;

        }

    }

// @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("insertTreatmentKey")
//
//    public PlansTreatmentsKeys InsertPlantTreamentKey(@QueryParam("key") Integer key,
//            @QueryParam("idPlanTreat") Integer idPlanTreat,
//            @QueryParam("idBareTreat") Integer idBareTreat,
//            @QueryParam("date") String date) {
//
//        java.util.Date dateTreatment = new java.util.Date(date);
//        Integer generado = EstatusPlansTreatmentsKeys.GENERADO.getValor();
//        PlansTreatmentsKeys createdPlansTreatmentsKeys = serviceManagementSpecialist.InsertTreatmentForKey(key,
//                idPlanTreat, dateTreatment, generado, idBareTreat, null, this.getIdUserLogged(), null);
//        System.out.println("com.venedental.controller.angular.TreatmentRegister.InsertPlantTreamentKey()"+createdPlansTreatmentsKeys.getId());
//        
//        return createdPlansTreatmentsKeys;
//
//    }
    
    
    //para retornar el Min-date
    
    //@POST
    //@Produces(MediaType.APPLICATION_JSON)
   @GET
   @Produces(MediaType.TEXT_PLAIN)
    @Path("getDateMin")
    public Date getDateMin(){
     Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
   
    return serviceManagementSpecialist.getDateMin(user.getId());
     
    }
     
    
      @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("consultDateDetailTreatment")

    public List<ConsultDateDetailTreatmentOutput> ConsultDateDetailTreatmentOutput(@QueryParam("idKey") Integer idKey) throws ParseException {
        consultDateDetailTreatmentOutput = this.serviceManagementSpecialist.findDateMinYMaxDetalle(idKey);

        for (ConsultDateDetailTreatmentOutput consultDateDetailTreatmentOutput : this.getConsultDateDetailTreatmentOutput()) {

            //fecha maxima y minima para el filtro de detalle
            String dateMinDetailTreatment = new SimpleDateFormat("dd/MM/yyyy").format(consultDateDetailTreatmentOutput.getDateMinTreatment());
            consultDateDetailTreatmentOutput.setDateMinTreatmentFilter(dateMinDetailTreatment);
            String dateMaxDetailTreatment = new SimpleDateFormat("dd/MM/yyyy").format(consultDateDetailTreatmentOutput.getDateMiaxTreatment());
            consultDateDetailTreatmentOutput.setDateMiaxTreatmentFilter(dateMaxDetailTreatment);
        }
        return consultDateDetailTreatmentOutput;
   }
    
}
