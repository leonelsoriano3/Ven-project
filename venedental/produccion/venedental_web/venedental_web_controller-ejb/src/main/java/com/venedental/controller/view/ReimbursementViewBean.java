package com.venedental.controller.view;

import static com.sun.faces.facelets.util.Path.context;
import com.venedental.constans.ConstansReimbursement;
import com.venedental.controller.BaseBean;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.enums.Relationship;
import com.venedental.enums.TypeSpecialistsEnums;
import com.venedental.facade.BanksFacadeLocal;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.RequirementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.Banks;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import com.venedental.model.Reimbursement;
import com.venedental.model.Requirement;
import com.venedental.model.StatusReimbursement;
import com.venedental.model.security.Users;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "ReimbursementViewBean")
@ViewScoped
public class ReimbursementViewBean extends BaseBean {

    /* Facades - Services */
    @EJB
    private ReimbursementFacadeLocal reembolsoLocal;
    @EJB
    private RequirementFacadeLocal requerimientoLocal;
    @EJB
    private BinnacleReimbursementFacadeLocal binnacleLocal;
    @EJB
    private PatientsFacadeLocal patientsLocal;
    @EJB
    private UsersFacadeLocal usuarioFacadeLocal;
    @EJB
    private BanksFacadeLocal banksFacadeLocal;
    @EJB
    private StatusReimbursementFacadeLocal statusReimbursementFacadeLocal;
    @EJB
    private MailSenderLocal mailSenderLocal;
    @EJB
    private PlansFacadeLocal plansFacadeLocal;
    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    /* Atributes */
    //---> Atributos necesarios para exportar Jasper
    private JasperPrint jasperPrint;
    private Map<String, Object> params = new HashMap<String, Object>();
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private ConfigViewBeanReport export;

    // --> Reimbursement
    private List<Reimbursement> reimbursementList;
    private List<Reimbursement> reimbursementSeleccionadoList;

    private Reimbursement reimbursementSelected;

    // --> Requirement
    private List<Requirement> requirementList;
    private StreamedContent fileRequirement;

    // --> BinnacleReimbursement
    private List<BinnacleReimbursement> binnacleReimbursementList;
    private BinnacleReimbursement binnacleReimbursementSelected;

    // --> Banks
    private List<Banks> banksList;
    private Banks selectedBanks;
    private String codeBanks;
    private Banks selectedBank2;

    private int statusReimbursmentId;

    private List<Plans> plansList;

    // --> Status
    private List<StatusReimbursement> statusRangeList;
    private StatusReimbursement selectedStatusReimbursement;


    /* OTHERS */
    private String[] newObservations;

    private String currentDate, minDate;

    private boolean btnVisibleAprobar, btnVisibleRechazar, columObservations, columnCheck,
            columnDateTransfer, columnDateRequest, btnVisibilityUpdate, btnExport;
    private boolean btnVisilityDataUpdateFinnacial, btnVisilityDataReadFinnacial;
    /* Attributes Enabled - Disabled Data Finnancial */

    // --> Data Finnacial
    private boolean visibilityNumberInvoice, visibilityAmount, visibilityInvoiceDate, visibilityAccountBank;
    private Plans planSelected;
    // --> Data Transfer
    private boolean visibilityDataTransfer;

    private Date dateTransfilter, dateRequestFilter;

    private double amountPaymentOriginal, amountPaymentOriginal2;
    private boolean fieldChangeAmount;
    private boolean btnDataFinancial;
    private boolean varChangePlan;
    /* Constructs */

    private boolean btnSaveReject;

    /*Aux del dialogo*/
    private String invoice_number;
    private Date invoice_date;
    private double amount;
    private String obser_monto_pagar;
    private String acoumntNumber;

    /**
     * Funcion que se encarga de validar si cumple los requisitos para la habilitacion del campo observacion y ademas de
     * la habilitacion/deshabilitacion del boton del dialogo de datos financieros
     *
     * @param event
     */
    boolean firtCharge = false;

    public ReimbursementViewBean() {

        this.newObservations = new String[2];
    }

    /**
     * Method to initialize components
     */
    @PostConstruct
    public void init() {
        this.findReimbursementByStatus(StatusReimbursementEnum.SOLICITADO.getValor());
        this.export = new ConfigViewBeanReport();
        this.fieldChangeAmount = true;
        this.btnDataFinancial = false;
        this.setBtnVisibleAprobar(true);
        this.setBtnVisibleRechazar(true);
        this.setBtnSaveReject(true);
        this.setStatusReimbursmentId(1);
        this.getPlansList().addAll(this.plansFacadeLocal.findAll());
        dateLimit();
    }

    /*-----------------------------------------------------------*/
    /*                  Comienzo de Getter y Setter              */
    /*-----------------------------------------------------------*/
    public boolean isVarChangePlan() {
        return varChangePlan;
    }

    public void setVarChangePlan(boolean changePlan) {
        this.varChangePlan = changePlan;
    }

    public List<Plans> getPlansList() {
        if (this.plansList == null) {
            this.plansList = new ArrayList<>();
        }
        return plansList;
    }

    public void setPlansList(List<Plans> plansList) {
        this.plansList = plansList;
    }

    public String getAcoumntNumber() {
        return acoumntNumber;
    }

    public void setAcoumntNumber(String acoumntNumber) {
        this.acoumntNumber = acoumntNumber;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public boolean isBtnSaveReject() {
        return btnSaveReject;
    }

    public void setBtnSaveReject(boolean btnSaveReject) {
        this.btnSaveReject = btnSaveReject;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public Date getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(Date invoice_date) {
        this.invoice_date = invoice_date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getObser_monto_pagar() {
        return obser_monto_pagar;
    }

    public void setObser_monto_pagar(String obser_monto_pagar) {
        this.obser_monto_pagar = obser_monto_pagar;
    }

    public boolean isBtnDataFinancial() {
        return btnDataFinancial;
    }

    public void setBtnDataFinancial(boolean btnDataFinancial) {
        this.btnDataFinancial = btnDataFinancial;
    }

    public double getAmountPaymentOriginal() {
        return amountPaymentOriginal;
    }

    public void setAmountPaymentOriginal(double amountPaymentOriginal) {
        this.amountPaymentOriginal = amountPaymentOriginal;
    }

    public void setFieldChangeAmount(boolean fieldChangeAmount) {
        this.fieldChangeAmount = fieldChangeAmount;
    }

    public boolean isFieldChangeAmount() {
        return fieldChangeAmount;
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }

    public boolean isBtnExport() {
        return btnExport;
    }

    public void setBtnExport(boolean btnExport) {
        this.btnExport = btnExport;
    }

    public Banks getSelectedBank2() {
        return selectedBank2;
    }

    public void setSelectedBank2(Banks selectedBank2) {
        this.selectedBank2 = selectedBank2;
    }

    public int getStatusReimbursmentId() {
        return statusReimbursmentId;
    }

    public void setStatusReimbursmentId(int statusReimbursmentId) {
        this.statusReimbursmentId = statusReimbursmentId;
    }

    public List<Reimbursement> getReimbursementList() {

        if (this.reimbursementList == null) {
            this.reimbursementList = new ArrayList<>();
        }
        return reimbursementList;
    }

    public void setReimbursementList(List<Reimbursement> reimbursementList) {
        this.reimbursementList = reimbursementList;
    }

    public Reimbursement getReimbursementSelected() {
        return reimbursementSelected;
    }

    public void setReimbursementSelected(Reimbursement reimbursementSelected) {
        this.reimbursementSelected = reimbursementSelected;
    }

    public List<BinnacleReimbursement> getBinnacleReimbursementList() {
        return binnacleReimbursementList;
    }

    public void setBinnacleReimbursementList(List<BinnacleReimbursement> binnacleReimbursementList) {
        this.binnacleReimbursementList = binnacleReimbursementList;
    }

    public BinnacleReimbursement getBinnacleReimbursementSelected() {
        return binnacleReimbursementSelected;
    }

    public void setBinnacleReimbursementSelected(BinnacleReimbursement binnacleReimbursementSelected) {
        this.binnacleReimbursementSelected = binnacleReimbursementSelected;
    }

    public boolean getBtnVisibleAprobar() {
        return btnVisibleAprobar;
    }

    public void setBtnVisibleAprobar(boolean btnVisibleAprobar) {
        this.btnVisibleAprobar = btnVisibleAprobar;
    }

    public boolean getBtnVisibleRechazar() {
        return btnVisibleRechazar;
    }

    public void setBtnVisibleRechazar(boolean btnVisibleRechazar) {
        this.btnVisibleRechazar = btnVisibleRechazar;
    }

    public boolean isBtnVisibilityUpdate() {
        return btnVisibilityUpdate;
    }

    public void setBtnVisibilityUpdate(boolean btnVisibilityUpdate) {
        this.btnVisibilityUpdate = btnVisibilityUpdate;
    }

    public List<Reimbursement> getReimbursementSeleccionadoList() {
        if (this.reimbursementSeleccionadoList == null) {
            this.reimbursementSeleccionadoList = new ArrayList<>();
        }
        return reimbursementSeleccionadoList;
    }

    public void setReimbursementSeleccionadoList(List<Reimbursement> reimbursementSeleccionadoList) {
        this.reimbursementSeleccionadoList = reimbursementSeleccionadoList;
    }

    public List<Banks> getBanksList() {
        return banksList;
    }

    public void setBanksList(List<Banks> banksList) {
        this.banksList = banksList;
    }

    public boolean isColumObservations() {
        return columObservations;
    }

    public void setColumObservations(boolean columObservations) {
        this.columObservations = columObservations;
    }

    public StatusReimbursement getSelectedStatusReimbursement() {
        return selectedStatusReimbursement;
    }

    public void setSelectedStatusReimbursement(StatusReimbursement selectedStatusReimbursement) {
        this.selectedStatusReimbursement = selectedStatusReimbursement;
    }

    public List<StatusReimbursement> getStatusRangeList() {
        return statusRangeList;
    }

    public void setStatusRangeList(List<StatusReimbursement> statusRangeList) {
        this.statusRangeList = statusRangeList;
    }

    public boolean isColumnCheck() {
        return columnCheck;
    }

    public void setColumnCheck(boolean columnCheck) {
        this.columnCheck = columnCheck;
    }

    public boolean isColumnDateTransfer() {
        return columnDateTransfer;
    }

    public void setColumnDateTransfer(boolean columnDateTransfer) {
        this.columnDateTransfer = columnDateTransfer;
    }

    public boolean isColumnDateRequest() {
        return columnDateRequest;
    }

    public void setColumnDateRequest(boolean columnDateRequest) {
        this.columnDateRequest = columnDateRequest;
    }

    public Date getDateTransfilter() {
        return dateTransfilter;
    }

    public void setDateTransfilter(Date dateTransfilter) {
        this.dateTransfilter = dateTransfilter;
    }

    public Date getDateRequestFilter() {
        return dateRequestFilter;
    }

    public void setDateRequestFilter(Date dateRequestFilter) {
        this.dateRequestFilter = dateRequestFilter;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public StreamedContent getFileRequirement() {
        return fileRequirement;
    }

    public void setFileRequirement(StreamedContent fileRequirement) {
        this.fileRequirement = fileRequirement;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public boolean isVisibilityNumberInvoice() {
        return visibilityNumberInvoice;
    }

    public void setVisibilityNumberInvoice(boolean visibilityNumberInvoice) {
        this.visibilityNumberInvoice = visibilityNumberInvoice;
    }

    public boolean isVisibilityAmount() {
        return visibilityAmount;
    }

    public void setVisibilityAmount(boolean visibilityAmount) {
        this.visibilityAmount = visibilityAmount;
    }

    public boolean isVisibilityInvoiceDate() {
        return visibilityInvoiceDate;
    }

    public void setVisibilityInvoiceDate(boolean visibilityInvoiceDate) {
        this.visibilityInvoiceDate = visibilityInvoiceDate;
    }

    public boolean isVisibilityAccountBank() {
        return visibilityAccountBank;
    }

    public void setVisibilityAccountBank(boolean visibilityAccountBank) {
        this.visibilityAccountBank = visibilityAccountBank;
    }

    public boolean isVisibilityDataTransfer() {
        return visibilityDataTransfer;
    }

    public void setVisibilityDataTransfer(boolean visibilityDataTransfer) {
        this.visibilityDataTransfer = visibilityDataTransfer;
    }

    public String[] getNewObservations() {
        return newObservations;
    }

    public void setNewObservations(String[] newObservations) {
        this.newObservations = newObservations;
    }

    public boolean isBtnVisilityDataUpdateFinnacial() {
        return btnVisilityDataUpdateFinnacial;
    }

    public void setBtnVisilityDataUpdateFinnacial(boolean btnVisilityDataUpdateFinnacial) {
        this.btnVisilityDataUpdateFinnacial = btnVisilityDataUpdateFinnacial;
    }

    public boolean isBtnVisilityDataReadFinnacial() {
        return btnVisilityDataReadFinnacial;
    }

    public void setBtnVisilityDataReadFinnacial(boolean btnVisilityDataReadFinnacial) {
        this.btnVisilityDataReadFinnacial = btnVisilityDataReadFinnacial;
    }

    public String getCodeBanks() {
        return codeBanks;
    }

    public void setCodeBanks(String codeBanks) {
        this.codeBanks = codeBanks;
    }

    public Banks getSelectedBanks() {
        return selectedBanks;
    }

    public void setSelectedBanks(Banks selectedBanks) {
        this.selectedBanks = selectedBanks;
    }

    public PlansFacadeLocal getPlansFacadeLocal() {
        return plansFacadeLocal;
    }

    public void setPlansFacadeLocal(PlansFacadeLocal plansFacadeLocal) {
        this.plansFacadeLocal = plansFacadeLocal;
    }

    public PlansPatientsFacadeLocal getPlansPatientsFacadeLocal() {
        return plansPatientsFacadeLocal;
    }

    public void setPlansPatientsFacadeLocal(PlansPatientsFacadeLocal plansPatientsFacadeLocal) {
        this.plansPatientsFacadeLocal = plansPatientsFacadeLocal;
    }

    public double getAmountPaymentOriginal2() {
        return amountPaymentOriginal2;
    }

    public void setAmountPaymentOriginal2(double amountPaymentOriginal2) {
        this.amountPaymentOriginal2 = amountPaymentOriginal2;
    }
    /*-----------------------------------------------------------*/
    /*                  final de Getter y Setter                 */
    /*-----------------------------------------------------------*/

    public void reimbursementRejectBtn() {
        try {
            if (newObservations[1] == null || newObservations[1].equalsIgnoreCase("")) {
                this.setBtnSaveReject(true);
            } else {
                this.setBtnSaveReject(false);
            }
            RequestContext.getCurrentInstance().update("form:btnRejectData");

        } catch (Exception e) {
            System.out.println("Error funcion reimbursementReject " + e);
        }

    }

    public void validateDataFinancial(AjaxBehaviorEvent event) {
        this.validateAmountPayment();
        this.validateComponentFinnalcialData(event);
        if ((this.getInvoice_number() != null && !this.getInvoice_number().isEmpty()) && this.getInvoice_date()
                != null && this.getAmount() > 0 && this.getAmountPaymentOriginal() > 0
                && this.getReimbursementSelected().getAccountNumberSubTring().length() > 15) {
            if (this.isFieldChangeAmount() == true && (!this.getObser_monto_pagar().equalsIgnoreCase("")
                    && !this.getObser_monto_pagar().isEmpty())) {
                this.setBtnDataFinancial(false);
                RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
            } else if (this.isFieldChangeAmount() == false && (this.getObser_monto_pagar().equalsIgnoreCase("")
                    && this.getObser_monto_pagar().isEmpty())) {
                this.setBtnDataFinancial(false);
                RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
            } else {
                this.setBtnDataFinancial(true);
                RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
            }
        } else {
            this.setBtnDataFinancial(true);
            RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
        }
        RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
        RequestContext.getCurrentInstance().update("form:messagesFinnancial");
        RequestContext.getCurrentInstance().update("messagesFinnancial");

    }

    /**
     * Funcion que se encarga de cargar el monto por pagar original
     *
     * @param reimbursement
     */
    public void chargeAmountPayment(Reimbursement reimbursement) {

        if (reimbursement.getMonto_remb_pagar() == 0.0) {
            this.setAmountPaymentOriginal(reimbursement.getPlansId().getReimbursement_amount());
            this.setAmountPaymentOriginal2(reimbursement.getPlansId().getReimbursement_amount());
        } else {
            this.setAmountPaymentOriginal(reimbursement.getMonto_remb_pagar());
            this.setAmountPaymentOriginal2(reimbursement.getMonto_remb_pagar());
        }
        this.setAmount(reimbursement.getAmount());
        this.setInvoice_date(reimbursement.getInvoice_date());
        this.setInvoice_number(reimbursement.getInvoice_number());
        if (reimbursement.getObser_monto_pagar() == null) {
            this.setObser_monto_pagar("");
        } else {
            this.setObser_monto_pagar(reimbursement.getObser_monto_pagar());
        }

        this.setFieldChangeAmount(false);
        this.setBtnDataFinancial(true);
        RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
        RequestContext.getCurrentInstance().update("form:panelFinnancialData");
        RequestContext.getCurrentInstance().update("form:panelFinancial");

    }

    /**
     * Funcion que se encarga de cargar el monto por pagar original
     *
     * @param reimbursement
     */
    public void changePlanAmountPayment(Reimbursement reimbursement) {

        if (reimbursement.getMonto_remb_pagar() == 0.0) {
            this.setAmountPaymentOriginal(reimbursement.getPlansId().getReimbursement_amount());
            this.setAmountPaymentOriginal2(reimbursement.getPlansId().getReimbursement_amount());
        } else {
            this.setAmountPaymentOriginal(reimbursement.getMonto_remb_pagar());
            this.setAmountPaymentOriginal2(reimbursement.getMonto_remb_pagar());
        }
        //this.setAmount(reimbursement.getAmount());
        this.setInvoice_date(reimbursement.getInvoice_date());
        this.setInvoice_number(reimbursement.getInvoice_number());
        this.setAcoumntNumber(reimbursement.getAccountNumberSubTring());

    }

    /**
     * Metodo que se usa para validar los componentes del diálogo FinnalcialData
     *
     * @param event
     */
    public void validateComponentFinnalcialData(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        if (this.getAmountPaymentOriginal() < 1) {
            FacesMessage msg = new FacesMessage("Monto a pagar es obligatorio. Ingresar número mayor a cero (0)");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        } else if (this.getAmountPaymentOriginal() > 1 && this.getAmountPaymentOriginal2() != this.getAmountPaymentOriginal()) {
            if (!(this.getObser_monto_pagar().length() > 0)) {
                FacesMessage msg = new FacesMessage("Observación es obligatorio");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                comp.setValid(false);
                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }

        if (this.getAmount() < 1) {
            FacesMessage msg = new FacesMessage("El Monto es obligatorio. Ingresar número mayor a cero (0)");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        }

        if (this.getReimbursementSelected().getAccountNumberSubTring().length() < 16) {
            FacesMessage msg = new FacesMessage("Cantidad de caracteres incorrectos, debe poseer un "
                    + "total de 16 dígitos.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        }

        try {
            if (this.getInvoice_number().isEmpty() || this.getInvoice_number() == null
                    || !(Integer.parseInt(this.getInvoice_number()) > 0)) {
                FacesMessage msg = new FacesMessage("Nro. de factura es obligatorio. Ingresar número mayor a cero (0)");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                comp.setValid(false);
                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
                RequestContext.getCurrentInstance().execute("$(\".btnSave\").addClass(\"btn-disabled\")");
                RequestContext.getCurrentInstance().execute("px = true;");
            } else {
                RequestContext.getCurrentInstance().execute("$(\".btnSave\").removeClass(\"btn-disabled\");");
                RequestContext.getCurrentInstance().execute("px = false;");
            }
        } catch (Exception e) {
            if (this.getInvoice_number().isEmpty() || this.getInvoice_number() == null) {
                FacesMessage msg = new FacesMessage("Nro. de factura es obligatorio. Ingresar número mayor a cero (0)");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                comp.setValid(false);
                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
                RequestContext.getCurrentInstance().execute("$(\".btnSave\").addClass(\"btn-disabled\")");
            } else {
                RequestContext.getCurrentInstance().execute("$(\".btnSave\").removeClass(\"btn-disabled\");");
            }
        }
    }

    /**
     * Funcion que se encarga de validar si el monto a pagar de un paciente fue modificado
     *
     */
    public void validateAmountPayment() {
        if (this.isVarChangePlan() == true) {
            if (this.getAmountPaymentOriginal2() == this.getAmountPaymentOriginal()) {
                this.setObser_monto_pagar("");
                this.setFieldChangeAmount(false);
            } else {
                this.setFieldChangeAmount(true);
            }
        } else {
            if (this.getAmountPaymentOriginal2() == this.getAmountPaymentOriginal() && this.getAmountPaymentOriginal()
                    == this.reimbursementSelected.getPlansPatientsId().getPlansId().getReimbursement_amount()) {
                this.setObser_monto_pagar("");
                this.setFieldChangeAmount(false);
            } else {
                this.setFieldChangeAmount(true);
            }
        }
    }

    /**
     * Methods for data requirement
     *
     * @param reimbursement
     */
    public void downloadRequirementByReimbursement(Reimbursement reimbursement) {

        InputStream inputstream = null;
        
        try {

            if (fileRequirement != null) {
                fileRequirement = null;
            }

            List<Requirement> requirementSelected = this.requerimientoLocal.findAllByReimbursement(reimbursement.getId());

            if (new File(requirementSelected.get(0).getFileSystemPath()).isFile()) {

                inputstream = new FileInputStream(requirementSelected.get(0).getFileSystemPath());
                this.setFileRequirement(new DefaultStreamedContent(inputstream, "application/pdf", requirementSelected.get(0).getFileName()));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_FATAL, "Error!", "La solicitud no posee recaudos asociados!"));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_FATAL, "Error!", "La solicitud no posee recaudos asociados!"));
        }

    }

    /**
     * Funcion que permite al usuario descargar un excel con los datos mostrados en la tabla al seleccionar alguno de lo
     * estados
     */
    public void exportRowsReimbursement() {
        try {
            if (!this.getReimbursementList().isEmpty()) {

                if (this.statusReimbursmentId == StatusReimbursementEnum.SOLICITADO.getValor()) {
                    this.loadDataExportReimbursementApplied();
                } else if (this.statusReimbursmentId == StatusReimbursementEnum.APROBADO.getValor()) {
                    this.loadDataExportReimbursementApproved();
                } else if (this.statusReimbursmentId == StatusReimbursementEnum.RECHAZADO.getValor()) {
                    this.loadDataExportReimbursementReject();
                } else if (this.statusReimbursmentId == StatusReimbursementEnum.POR_ABONAR.getValor()) {
                    this.loadDataExportReimbursementPerPay();
                } else {
                    this.loadDataExportReimbursementPayment();
                }

                this.export.xlsxExportRowsReimbursement(this.getJasperPrint(), "ReporteReembolso");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_FATAL, "Error!", "La tabla no tiene datos"));
            }
        } catch (Exception e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * Funcion hija de exportRowsReimbursement, que se encarga de llenar los datos necesarios para el exporte del jasper
     *
     * @throws net.sf.jasperreports.engine.JRException
     */
    public void loadDataExportReimbursementApplied() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getReimbursementList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO SOLICITADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getReimbursementList());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursement.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    public void loadDataExportReimbursementApproved() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getReimbursementList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO APROBADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getReimbursementList());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursement.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);

    }

    public void loadDataExportReimbursementReject() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getReimbursementList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO RECHAZADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getReimbursementList());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursementRechazado.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    public void loadDataExportReimbursementPerPay() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getReimbursementList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO POR ABONAR");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getReimbursementList());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursementPorAbonar.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    public void loadDataExportReimbursementPayment() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getReimbursementList()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO ABONADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getReimbursementList());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursementAbonado.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    /**
     *
     * @return List searchAllStatusByRange()
     */
    public List<StatusReimbursement> findAllStatusReimbursement() {

        return this.statusRangeList = this.statusReimbursementFacadeLocal.findAll();
    }

    /**
     * Metodo que retorna una lista de los planes que tiene un paciente ingresando como parametro un reembolso
     *
     * @param reimbursement
     * @return
     */
    public List<Plans> findPlanPatients(Reimbursement reimbursement) {
        return this.plansFacadeLocal.finPlansPatientsByHealthArea(reimbursement.getPatientsId().getId(), 2);
    }

    /**
     *
     * @param status List Reimbursement by Status Solicitado
     */
    public void findReimbursementByStatus(int status) {

        if (status == StatusReimbursementEnum.ABONADO.getValor()) {
            this.setReimbursementList(this.reembolsoLocal.findByStatusTransfer(status));
        } else {
            this.setReimbursementList(this.reembolsoLocal.findByStatus(status));
            if (this.getReimbursementList().isEmpty()) {
                this.setBtnExport(true);
            } else {
                this.setBtnExport(false);
            }
        }

        // Columns Check
        if (status == StatusReimbursementEnum.SOLICITADO.getValor()) {

            this.setColumnCheck(true);
            this.setColumnDateRequest(true);
            this.setColumnDateTransfer(false);

            // --> Enabled - Disabled Data Finnancial
            this.setVisibilityNumberInvoice(false);
            this.setVisibilityInvoiceDate(false);
            this.setVisibilityAmount(false);
            this.setVisibilityAccountBank(false);

            // --> Buttons Finnancial
            this.setBtnVisilityDataReadFinnacial(false);
            this.setBtnVisilityDataUpdateFinnacial(true);

            // --> Rendered View Data Transfer Finnancial
            this.setVisibilityDataTransfer(false);
        }
        if (status == StatusReimbursementEnum.APROBADO.getValor()) {
            this.setColumnCheck(false);
            this.setColumnDateRequest(true);
            this.setColumnDateTransfer(false);

            // --> Enabled - Disabled Data Finnancial
            this.setVisibilityNumberInvoice(true);
            this.setVisibilityInvoiceDate(true);
            this.setVisibilityAmount(true);
            this.setVisibilityAccountBank(true);

            // --> Buttons Finnancial
            this.setBtnVisilityDataReadFinnacial(true);
            this.setBtnVisilityDataUpdateFinnacial(false);

            // --> Rendered View Data Transfer Finnancial
            this.setVisibilityDataTransfer(false);
        }
        if (status == StatusReimbursementEnum.RECHAZADO.getValor()) {
            this.setColumnCheck(false);
            this.setColumnDateRequest(true);
            this.setColumnDateTransfer(false);

            // --> Enabled - Disabled Data Finnancial
            this.setVisibilityNumberInvoice(true);
            this.setVisibilityInvoiceDate(true);
            this.setVisibilityAmount(true);
            this.setVisibilityAccountBank(true);

            // --> Rendered View Data Transfer Finnancial
            this.setVisibilityDataTransfer(false);

            // --> Buttons Finnancial
            this.setBtnVisilityDataReadFinnacial(true);
            this.setBtnVisilityDataUpdateFinnacial(false);

        }
        if (status == StatusReimbursementEnum.POR_ABONAR.getValor()) {
            this.setColumnCheck(false);
            this.setColumnDateRequest(true);
            this.setColumnDateTransfer(false);

            // --> Enabled - Disabled Data Finnancial
            this.setVisibilityNumberInvoice(true);
            this.setVisibilityInvoiceDate(true);
            this.setVisibilityAmount(true);
            this.setVisibilityAccountBank(true);

            // --> Rendered View Data Transfer Finnancial
            this.setVisibilityDataTransfer(false);

            // --> Buttons Finnancial
            this.setBtnVisilityDataReadFinnacial(true);
            this.setBtnVisilityDataUpdateFinnacial(false);
        }
        if (status == StatusReimbursementEnum.ABONADO.getValor()) {
            this.setColumnCheck(false);
            this.setColumnDateRequest(false);
            this.setColumnDateTransfer(true);

            // --> Enabled - Disabled Data Finnancial
            this.setVisibilityNumberInvoice(true);
            this.setVisibilityInvoiceDate(true);
            this.setVisibilityAmount(true);
            this.setVisibilityAccountBank(true);

            // --> Rendered View Data Transfer Finnancial
            this.setVisibilityDataTransfer(true);

            // --> Buttons Finnancial
            this.setBtnVisilityDataReadFinnacial(true);
            this.setBtnVisilityDataUpdateFinnacial(false);
        }

        List<Patients> patientsTitular;

        for (Reimbursement reimbursement : this.getReimbursementList()) {

            reimbursement.getPlansPatientsId().getPatientsId().getId();

            if (reimbursement.getPlansPatientsId().getPatientsId().getRelationshipId().getId() != Relationship.TITULAR.getValor()) {

                patientsTitular = this.patientsLocal.findByIdentityNumberAndRelationShip(
                        reimbursement.getPlansPatientsId().getPatientsId().getIdentityNumberHolder(), Relationship.TITULAR.getValor()
                );

                if (patientsTitular != null) {

                    reimbursement.setIndentityNumberTitular(patientsTitular.get(0).getIdentityNumber());
                    reimbursement.setCompleteNameTitular(patientsTitular.get(0).getCompleteName());
                    reimbursement.setCompleteNameBeneficiary(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Error:", "El Paciente beneficiario no posee Titular!"));
                }

            } else {
                // --> Asigamos Valores a los transients al Titular tipo 10
                reimbursement.setIndentityNumberTitular(reimbursement.getPlansPatientsId().getPatientsId().getIdentityNumber());
                reimbursement.setCompleteNameTitular(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());
                reimbursement.setCompleteNameBeneficiary("-");
            }
        }

        /*Se setea el el reembolso si dependiendo del valor*/
        this.setBtnExport((this.reimbursementList.isEmpty()) ? (true) : (false));

        RequestContext.getCurrentInstance().update("form:messageReimbursement");
        RequestContext.getCurrentInstance().update("form:btnExport");
        RequestContext.getCurrentInstance().update("form:btnFinnancialDataRead");
        RequestContext.getCurrentInstance().update("form:btnFinnancialData");
        RequestContext.getCurrentInstance().update("form:tableReimbursement");
    }

    public void updateButtonsApproveReject(boolean btnAprovem) {

        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnReject");
    }

    /**
     * Method to display Reimbursement requests Rejected
     *
     */
    public void reimbursementRequestsRejected() {

        if (this.getStatusReimbursmentId() != 0) {

            if (this.getStatusReimbursmentId() == StatusReimbursementEnum.SOLICITADO.getValor()) {

                this.findReimbursementByStatus(StatusReimbursementEnum.SOLICITADO.getValor());

                // Buttons
                this.setBtnVisibleAprobar(true);
                this.setBtnVisibleRechazar(true);
                this.setColumObservations(false);
                this.setBtnVisibilityUpdate(false);

                this.resetDataTableReimbursement();

            }

            if (this.getStatusReimbursmentId() == StatusReimbursementEnum.RECHAZADO.getValor()) {

                this.findReimbursementByStatus(StatusReimbursementEnum.RECHAZADO.getValor());

                // Buttons
                this.setBtnVisibleAprobar(true);
                this.setBtnVisibleRechazar(true);
                this.setColumObservations(true);
                this.setBtnVisibilityUpdate(true);

                this.resetDataTableReimbursement();

            }

            if (this.getStatusReimbursmentId() == StatusReimbursementEnum.APROBADO.getValor()) {

                this.findReimbursementByStatus(StatusReimbursementEnum.APROBADO.getValor());

                // Buttons
                this.setBtnVisibleAprobar(true);
                this.setBtnVisibleRechazar(true);
                this.setColumObservations(false);
                this.setBtnVisibilityUpdate(true);

                this.resetDataTableReimbursement();

            }

            if (this.getStatusReimbursmentId() == StatusReimbursementEnum.POR_ABONAR.getValor()) {

                this.findReimbursementByStatus(StatusReimbursementEnum.POR_ABONAR.getValor());

                // Buttons
                this.setBtnVisibleAprobar(true);
                this.setBtnVisibleRechazar(true);
                this.setColumObservations(false);
                this.setBtnVisibilityUpdate(true);

                this.resetDataTableReimbursement();

            }

            if (this.getStatusReimbursmentId() == StatusReimbursementEnum.ABONADO.getValor()) {

                this.findReimbursementByStatus(StatusReimbursementEnum.ABONADO.getValor());

                // Buttons
                this.setBtnVisibleAprobar(true);
                this.setBtnVisibleRechazar(true);
                this.setColumObservations(false);
                this.setBtnVisibilityUpdate(true);

                this.resetDataTableReimbursement();

            }

        }

        RequestContext.getCurrentInstance().execute("PF('reimbTable2').filter()");
        // FacesContext
        RequestContext.getCurrentInstance().update("form:tableReimbursement");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnReject");
    }

    /**
     * List Banks in Dialog data finnancial
     *
     */
    public void searchBanksByReimbursemnt() {

        this.setBanksList(this.banksFacadeLocal.findAll());
    }

    /**
     * On change in code data finnancial
     *
     */
    public void onChangeCodeBanks() {
        RequestContext.getCurrentInstance().update("form:panelFinnancialData");
    }

    /**
     *
     * Method Refund the records list.
     *
     * @param reimbursement_id
     * @return List<> searchBinnacleReimbursementByReimbursementid
     */
    public List<BinnacleReimbursement> searchBinnacleReimbursementByReimbursementid(int reimbursement_id) {

        List<BinnacleReimbursement> lista = new ArrayList<>();
        lista = this.binnacleLocal.findAllByReimbursement(reimbursement_id);

        List<Reimbursement> patientsReimbursement;
        //List<>

        for (BinnacleReimbursement binnacleReimbursement : lista) {

            if (binnacleReimbursement.getStatusReimbursementId().getId() != StatusReimbursementEnum.SOLICITADO.getValor()) {

                // Users
                Users searchUsers;

                searchUsers = this.usuarioFacadeLocal.find(binnacleReimbursement.getUserId());

                binnacleReimbursement.setIssuerUser(searchUsers.getName());

            } else {
                // Client
                binnacleReimbursement.setIssuerUser(
                        binnacleReimbursement.getReimbursementId().getPlansPatientsId().getPatientsId().getCompleteName()
                );

            }
        }

        return lista;

    }

    /**
     * Method to load the financial data in the dialogue
     *
     * @param reimbursement
     */
    public void selectedReimbursementViewDialog(Reimbursement reimbursement) {

        try {

            if (reimbursement != null) {

                // Setter Banks
                this.setSelectedBanks(reimbursement.getBanksPatientsId().getBankId());
                this.searchBanksByReimbursemnt();

                String numberAccountSub = reimbursement.getBanksPatientsId().getAccountNumber().substring(4, 20);

                reimbursement.setAccountNumberSubTring(numberAccountSub);

                this.setReimbursementSelected(reimbursement);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN, "Error: ", "Registro vacio!"));
            }
        } catch (Exception e) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Error:", "Solicitud no Cargada!"));
        }

        RequestContext.getCurrentInstance().update("form:messageReimbursement");
        RequestContext.getCurrentInstance().update("form:columEntityBank");
        RequestContext.getCurrentInstance().update("form:txtCode");

    }

    /**
     * Metodo que asigna al objeto reembolso seleccionado el plan seleccionado
     *
     * @param plans
     */
    public void changePlan(Plans plans) {
        try {
            //this.changePlanAmountPayment(getReimbursementSelected());

            this.getReimbursementSelected().setPlansId(plans);
            this.setAmountPaymentOriginal(plans.getReimbursement_amount());
            this.setAmountPaymentOriginal2(plans.getReimbursement_amount());
            this.setObser_monto_pagar("");
            this.setFieldChangeAmount(false);
            this.setBtnDataFinancial(false);
            this.setVarChangePlan(true);

            RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");
            RequestContext.getCurrentInstance().update("form:inputAmountToPay");
            RequestContext.getCurrentInstance().update("form:panelFinnancialData");
            RequestContext.getCurrentInstance().update("form:fieldObser");

        } catch (Exception e) {
            System.out.println("Error changeplan: " + e);
        }
    }

    /**
     *
     * Methods for selected rows in datatable
     */
    public void selectRow() {

        if (this.reimbursementSeleccionadoList.isEmpty()) {
            this.setBtnVisibleAprobar(true);
            this.setBtnVisibleRechazar(true);
        } else {
            this.setBtnVisibleAprobar(false);
            this.setBtnVisibleRechazar(false);
        }

        RequestContext.getCurrentInstance().update("form:tableReimbursement");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnReject");

    }

    public Plans getPlanSelected() {
        return planSelected;
    }

    public void setPlanSelected(Plans planSelected) {
        this.planSelected = planSelected;
    }

    /**
     *
     * Methods for Unselected rows in datatable
     */
    public void unSelectRow() {

        if (this.reimbursementSeleccionadoList.isEmpty()) {
            this.setBtnVisibleAprobar(true);
            this.setBtnVisibleRechazar(true);
        } else {
            this.setBtnVisibleAprobar(false);
            this.setBtnVisibleRechazar(false);
        }

        RequestContext.getCurrentInstance().update("form:tableReimbursement");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnReject");
    }

    /**
     *
     * Methods for toggleSelect rows in datatable
     */
    public void toggleSelect() {

        if (this.reimbursementSeleccionadoList.isEmpty()) {
            this.setBtnVisibleAprobar(true);
            this.setBtnVisibleRechazar(true);
        } else {
            this.setBtnVisibleAprobar(false);
            this.setBtnVisibleRechazar(false);
        }

        RequestContext.getCurrentInstance().update("form:tableReimbursement");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnReject");
    }

    /**
     * Reset Datatable in Filter Reimbursement
     *
     */
    public void resetDataTableReimbursement() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tableReimbursement");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:tableReimbursement");
            RequestContext.getCurrentInstance().reset("form");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:tableReimbursement");
            RequestContext.getCurrentInstance().update("form");
        }

        if (isVarChangePlan() == true) {
            RequestContext.getCurrentInstance().execute("window.location.reload();");
        }
    }

    /**
     *  *------------------------------------------------------------------------------ * MAIL SENDER BY USERS AND
     * GROUPS VENEDEN * ----------------------------------------------------------------------------- *
     */
    /**
     * METHOD TO READ FILE A PROPERTIES REIMBURSEMENT
     *
     * @return
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {

        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansReimbursement.FILENAME_REIMBURSEMENT_REPORT);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansReimbursement.FILENAME_REIMBURSEMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * METHOD TO BUILD MESSAGE PATIENTS A REIMBURSEMENT
     *
     * @param reimbursement
     * @return
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessagePatiens(Reimbursement reimbursement) throws ParseException, IOException {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Estimado paciente, ").append("<br><br>");

        if (reimbursement.getStatusReimbursmentId().getId() == StatusReimbursementEnum.APROBADO.getValor()) {
            stringBuilder.append("Su solicitud de reembolso nro.").append(reimbursement.getNumberRequest()).append(", ");
            stringBuilder.append(readFileProperties().getProperty(ConstansReimbursement.PROPERTY_EMAIL_CONTENT_APPROVE)).append("<br><br>");
            stringBuilder.append("Siempre dispuestos a prestarte un mejor servicio.").append("<br><br>").append("<strong>Grupo VENEDEN</strong>");
        }
        if (reimbursement.getStatusReimbursmentId().getId() == StatusReimbursementEnum.RECHAZADO.getValor()) {
            stringBuilder.append("Su solicitud de reembolso nro. ").append(reimbursement.getNumberRequest()).append(", ");
            stringBuilder.append(readFileProperties().getProperty(ConstansReimbursement.PROPERTY_EMAIL_CONTENT_REJECT)).append("<br><br>");
            stringBuilder.append("Siempre dispuestos a prestarte un mejor servicio.").append("<br><br>").append("<strong>Grupo VENEDEN</strong>");
        }

        return stringBuilder.toString();
    }

    /**
     * METHOD TO SEND A MESSAGE NOTIFY REIMBURSEMENT
     *
     * @param reimbursement
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailPatient(Reimbursement reimbursement) throws ParseException, IOException {

        log.log(Level.INFO, "Inicio - Proceso de envio de correos  en la bandeja de reembolso notificando a los pacientes: {0}",
                reimbursement.getCompleteNameTitular());

        if (reimbursement.getResponse_email() != null) {

            String title = readFileProperties().getProperty(ConstansReimbursement.PROPERTY_EMAIL_TITLE);

            String subjectMessage = "Grupo Veneden ©“ Solicitud de Reembolso";

            String contentMessage = buildMessagePatiens(reimbursement);

            this.mailSenderLocal.send(Email.MAIL_REEMBOLSO, subjectMessage, title, contentMessage, new ArrayList<File>(),
                    reimbursement.getResponse_email()
            );
        } else {
            log.log(Level.INFO, "El paciente no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos a pacientes");
    }

    /**
     * Method to set Date limit, range: [Year-1,Year]
     */
    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int minYear = year - 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setCurrentDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
    }

}