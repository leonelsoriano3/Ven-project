/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.controller.application.converterReport;

import com.venedental.controller.converter.*;
import com.venedental.controller.view.PlansViewBean;
import com.venedental.controller.viewReport.AuditViewBeanReport;
import com.venedental.dto.ReadPlansReportOutput;
import com.venedental.model.Plans;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "plansConverterReport", forClass = Plans.class)
public class PlansConverterReport implements Converter {
    
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        
        AuditViewBeanReport plansViewBean = (AuditViewBeanReport) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "auditViewBeanReport");
        
        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                
                for (ReadPlansReportOutput plans : plansViewBean.getListaPlans()) {
                    if(plans!= null){
                        if (plans.getIdPlan() == numero) {
                            return plans;
                        }
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Plan Invalido"));
            }
        }
        return null;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((ReadPlansReportOutput) value).getIdPlan());
        }
        
    }
    
}
