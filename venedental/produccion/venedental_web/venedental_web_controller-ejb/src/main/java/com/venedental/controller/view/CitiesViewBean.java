package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.application.StateApplicationBean;
import com.venedental.model.Cities;
import com.venedental.model.Clinics;
import com.venedental.model.State;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "citiesViewBean")
@ViewScoped
public class CitiesViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    public CitiesViewBean() {
    }
    
    @PostConstruct
    private void init(){
        if (this.getStateViewBean() != null) {
            this.filtrarCities();
        }
        
    }

    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    
     @ManagedProperty(value = "#{stateApplicationBean}")
    private StateApplicationBean stateApplicationBean;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }

    public StateApplicationBean getStateApplicationBean() {
        return stateApplicationBean;
    }

    public void setStateApplicationBean(StateApplicationBean stateApplicationBean) {
        this.stateApplicationBean = stateApplicationBean;
    }
    
    
    

    private Cities editCities;

    private Cities newCities;
    
    private Cities newClinicCities;

    public Cities getEditCities() {
        return editCities;
    }

    public void setEditCities(Cities editCities) {
        this.editCities = editCities;
    }

    public Cities getNewCities() {
        return newCities;
    }

    public void setNewCities(Cities newCities) {
        this.newCities = newCities;
    }

    public Cities getNewClinicCities() {
        return newClinicCities;
    }

    public void setNewClinicCities(Cities newClinicCities) {
        this.newClinicCities = newClinicCities;
    }

    private List<Cities> listCities;

    public List<Cities> getListCities() {
        if (this.listCities == null) {
            this.listCities = new ArrayList<>();
      
        }
     
        return listCities;
    }

    public void setListCities(List<Cities> listCities) {
        this.listCities = listCities;
    }

    public void filtrarCities() {
        this.getListCities().clear();
        
            if (this.getStateViewBean().getEditState() != null) {
                for (Cities c : this.getCitiesApplicationBean().getListCities()) {
                    if (Objects.equals(c.getStateId().getId(), this.getStateViewBean().getEditState().getId())) {
                        this.getListCities().add(c);
                    }
                }
                
                   Collections.sort(this.getListCities(), new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    Cities cities1 = (Cities) o1;
                    Cities cities2 = (Cities) o2;
                    return cities1.getName().compareTo(cities2.getName());

                }

            });

            }
    }
    

    public void filtrarCitiesClinic(State estado) {
        
        this.getStateViewBean().getEditState();
        this.getListCities().clear();
        if (estado != null) {            
            for (Cities c : this.getCitiesApplicationBean().getListCities()) {
                if (Objects.equals(c.getStateId().getId(), estado.getId())) {
                    this.getListCities().add(c);
                }
            }
            
            Collections.sort(this.getListCities(), new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    Cities cities1 = (Cities) o1;
                    Cities cities2 = (Cities) o2;
                    return cities1.getName().compareTo(cities2.getName());

                }

            });

        }
    }
}
