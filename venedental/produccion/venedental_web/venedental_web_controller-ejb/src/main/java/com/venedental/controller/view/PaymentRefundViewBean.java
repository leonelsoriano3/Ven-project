package com.venedental.controller.view;

import com.venedental.constans.ConstansPaymentRefundReport;
import com.venedental.controller.BaseBean;
import static com.venedental.controller.BaseBean.log;
import com.venedental.controller.viewReport.ConfigViewBeanReport;
import com.venedental.enums.Relationship;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.RequirementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Patients;
import com.venedental.model.Reimbursement;
import com.venedental.model.Requirement;
import com.venedental.model.StatusReimbursement;
import com.venedental.model.security.Users;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import com.venedental.services.ServicePaymentRefund;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import services.utils.Transformar;
import com.venedental.dto.PaymentRefund.GenerateTxtInput;
import com.venedental.dto.PaymentRefund.GenerateTxtOutput;
import java.sql.SQLException;



@ManagedBean(name = "paymentRefundViewBean")
@ViewScoped
public class PaymentRefundViewBean extends BaseBean {
    /* Services */
    
    @EJB
    private ServicePaymentRefund servicePaymentRefund;
    
    private List<GenerateTxtOutput> generateTxtOutputList;

    private Double amountPay;

    public Double getAmountPay() {
        return amountPay;
    }

    public void setAmountPay(Double amountPay) {
        this.amountPay = amountPay;
    }

    public List<GenerateTxtOutput> getGenerateTxtOutputList() {
        return generateTxtOutputList;
    }

    public void setGenerateTxtOutputList(List<GenerateTxtOutput> generateTxtOutputList) {
        this.generateTxtOutputList = generateTxtOutputList;
    }
    
    

    /**
     * Filtar monto a pagar
     * @param value datos a evaluar
     * @param filter palabra en el filtro
     * @param locale 
     * @return regresa verdadero si el value cumple con el filtro
     */
    public boolean filterAmountPay(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }
        
        BigDecimal bigValue = new BigDecimal(value.toString());

        return bigValue.toString().matches("(?i).*" + (filter.toString().replaceFirst("\\.0*$", "")) + ".*");
    }

    //Declaracion de las fachadas 
    @EJB
    private ReimbursementFacadeLocal reimbursementFacadeLocal;

    @EJB
    private StatusReimbursementFacadeLocal statusReimbursementFacadeLocal;

    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;

    @EJB
    private MailSenderLocal mailSenderLocal;

    @EJB
    private BinnacleReimbursementFacadeLocal binnacleLocal;

    @EJB
    private UsersFacadeLocal usuarioFacadeLocal;

    @EJB
    private RequirementFacadeLocal requerimientoLocal;

    //---> Atributos necesarios para exportar Jasper
    private JasperPrint jasperPrint;
    private Map<String, Object> params = new HashMap<String, Object>();
    private JRBeanCollectionDataSource beanCollectionDataSource;
    private ConfigViewBeanReport export;

    //lista de reembolsos
    private List<Reimbursement> listReimbursement;

    private List<Reimbursement> listSelectedReimbursement;

    private List<StatusReimbursement> listStatusReimbursement;

    private StreamedContent fileRequirement;

    private DefaultStreamedContent txt;

    private boolean btnExport;

    //objeto usado cuando se selecciona un elemento de la tabla
    private Reimbursement selectedFinancial;
    private Date selectedDate;
    private String currentDate, minDate;

    /*Constantes y boolean que permite ver el boton de generar Txt*/
    private final String IDENTITYNUMBER = "CEDULA";
    private final String AMOUT = "MONTO";
    private final String NAME = "NOMBRE";
    private boolean btnGenerateTxt;

    int statusReimbursementId;

    //dato que permite la visibilidad de los elementos que se usan para los aprobados
    private boolean visibilityButton;
    private boolean visibilityCheck;
    private boolean btnVisibleAprobar;
    private boolean columnPayment;
    private boolean columnState;
    private boolean btnFinancial;
    private boolean statusPaymentColumn;

    private String totalPrice;

    private Date maxDate;

    private boolean seeColumnCheck = false;
    private boolean seeAmountTotal;

    private List<Requirement> requirementList;
    private List<Requirement> requirementSelectedList;
    private Requirement requirementSelected;

    public StreamedContent getFileRequirement() {

        try {
            List<Requirement> requirementSelectedList = this.requerimientoLocal.
                    findAllByReimbursement(this.getSelectedFinancial().getId());
            InputStream inputstream = new FileInputStream(requirementSelectedList.get(0).getFileSystemPath());
            fileRequirement = new DefaultStreamedContent(inputstream, "application/pdf",
                    requirementSelectedList.get(0).getFileName());
            return fileRequirement;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Error!", "La solicitud no posee recaudos asociados!"));
        }

        RequestContext.getCurrentInstance().update("form:messageReimbursement");

        return fileRequirement;

    }

    /*Comienzo de Getter y Setter*/
    public boolean isStatusPaymentColumn() {
        return statusPaymentColumn;
    }

    public void setStatusPaymentColumn(boolean statusPaymentColumn) {
        this.statusPaymentColumn = statusPaymentColumn;
    }

    public boolean isBtnExport() {
        return btnExport;
    }

    public void setBtnExport(boolean btnExport) {
        this.btnExport = btnExport;
    }

    public StatusReimbursementFacadeLocal getStatusReimbursementFacadeLocal() {
        return statusReimbursementFacadeLocal;
    }

    public void setStatusReimbursementFacadeLocal(StatusReimbursementFacadeLocal statusReimbursementFacadeLocal) {
        this.statusReimbursementFacadeLocal = statusReimbursementFacadeLocal;
    }

    public MailSenderLocal getMailSenderLocal() {
        return mailSenderLocal;
    }

    public void setMailSenderLocal(MailSenderLocal mailSenderLocal) {
        this.mailSenderLocal = mailSenderLocal;
    }

    public BinnacleReimbursementFacadeLocal getBinnacleLocal() {
        return binnacleLocal;
    }

    public void setBinnacleLocal(BinnacleReimbursementFacadeLocal binnacleLocal) {
        this.binnacleLocal = binnacleLocal;
    }

    public UsersFacadeLocal getUsuarioFacadeLocal() {
        return usuarioFacadeLocal;
    }

    public void setUsuarioFacadeLocal(UsersFacadeLocal usuarioFacadeLocal) {
        this.usuarioFacadeLocal = usuarioFacadeLocal;
    }

    public RequirementFacadeLocal getRequerimientoLocal() {
        return requerimientoLocal;
    }

    public void setRequerimientoLocal(RequirementFacadeLocal requerimientoLocal) {
        this.requerimientoLocal = requerimientoLocal;
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public ConfigViewBeanReport getExport() {
        return export;
    }

    public void setExport(ConfigViewBeanReport export) {
        this.export = export;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Requirement> getRequirementSelectedList() {
        return requirementSelectedList;
    }

    public void setRequirementSelectedList(List<Requirement> requirementSelectedList) {
        this.requirementSelectedList = requirementSelectedList;
    }

    public void setFileRequirement(StreamedContent fileRequirement) {
        this.fileRequirement = fileRequirement;
    }

    public boolean isColumnPayment() {
        return columnPayment;
    }

    public void setColumnPayment(boolean columnPayment) {
        this.columnPayment = columnPayment;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public PatientsFacadeLocal getPatientsFacadeLocal() {
        return patientsFacadeLocal;
    }

    public void setPatientsFacadeLocal(PatientsFacadeLocal patientsFacadeLocal) {
        this.patientsFacadeLocal = patientsFacadeLocal;
    }

    public ReimbursementFacadeLocal getReimbursementFacadeLocal() {
        return reimbursementFacadeLocal;
    }

    public void setReimbursementFacadeLocal(ReimbursementFacadeLocal reimbursementFacadeLocal) {
        this.reimbursementFacadeLocal = reimbursementFacadeLocal;
    }

    public List<Reimbursement> getListReimbursement() {
        if (this.listReimbursement == null) {
            this.listReimbursement = new ArrayList<>();
        }
        return listReimbursement;
    }

    public List<Reimbursement> getListSelectedReimbursement() {
        if (this.listSelectedReimbursement == null) {
            this.listSelectedReimbursement = new ArrayList<>();
        }
        return listSelectedReimbursement;
    }

    public void setListSelectedReimbursement(List<Reimbursement> listSelectedReimbursement) {
        this.listSelectedReimbursement = listSelectedReimbursement;
    }

    public List<StatusReimbursement> getListStatusReimbursement() {
        if (this.listStatusReimbursement == null) {
            this.listStatusReimbursement = new ArrayList<>();
        }
        return listStatusReimbursement;
    }

    public void setListStatusReimbursement(List<StatusReimbursement> listStatusReimbursement) {
        this.listStatusReimbursement = listStatusReimbursement;
    }

    public boolean isSeeColumnCheck() {
        return seeColumnCheck;
    }

    public void setSeeColumnCheck(boolean seeColumnCheck) {
        this.seeColumnCheck = seeColumnCheck;
    }

    public void setListReimbursement(List<Reimbursement> listReimbursement) {
        this.listReimbursement = listReimbursement;
    }

    public Reimbursement getSelectedFinancial() {
        return selectedFinancial;
    }

    public void setSelectedFinancial(Reimbursement selectedFinancial) {
        this.selectedFinancial = selectedFinancial;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public void selectDate(SelectEvent event) {
        RequestContext.getCurrentInstance().execute("PF('tableRefund').filter()");
    }

    public boolean isVisibilityButton() {
        return visibilityButton;
    }

    public void setVisibilityButton(boolean visibilityButton) {
        this.visibilityButton = visibilityButton;
    }

    public boolean isVisibilityCheck() {
        return visibilityCheck;
    }

    public void setVisibilityCheck(boolean visibilityCheck) {
        this.visibilityCheck = visibilityCheck;
    }

    public int getStatusReimbursementId() {
        return statusReimbursementId;
    }

    public void setStatusReimbursementId(int statusReimbursementId) {
        this.statusReimbursementId = statusReimbursementId;
    }

    public boolean isBtnVisibleAprobar() {
        return btnVisibleAprobar;
    }

    public void setBtnVisibleAprobar(boolean btnVisibleAprobar) {
        this.btnVisibleAprobar = btnVisibleAprobar;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public Requirement getRequirementSelected() {
        return requirementSelected;
    }

    public void setRequirementSelected(Requirement requirementSelected) {
        this.requirementSelected = requirementSelected;
    }

    public boolean isColumnState() {
        return columnState;
    }

    public void setColumnState(boolean columnState) {
        this.columnState = columnState;
    }

    public boolean isBtnFinancial() {
        return btnFinancial;
    }

    public void setBtnFinancial(boolean btnFinancial) {
        this.btnFinancial = btnFinancial;
    }

    public boolean isSeeAmountTotal() {
        return seeAmountTotal;
    }

    public void setSeeAmountTotal(boolean seeAmountTotal) {
        this.seeAmountTotal = seeAmountTotal;
    }

    public DefaultStreamedContent getTxt() {
        return txt;
    }

    public void setTxt(DefaultStreamedContent txt) {
        this.txt = txt;
    }

    public boolean isBtnGenerateTxt() {
        return btnGenerateTxt;
    }

    public void setBtnGenerateTxt(boolean btnGenerateTxt) {
        this.btnGenerateTxt = btnGenerateTxt;
    }

    /*Fin de Getter y Setter*/
    public boolean changeInpuFinancial(int status) {
        if (StatusReimbursementEnum.POR_ABONAR.getValor() == status) {
            return false;
        }
        return true;
    }

    public boolean changeVisibilityByAprove(int status) {
        if (StatusReimbursementEnum.APROBADO.getValor() == status) {
            return false;
        }
        return true;
    }

    public void cerrarDialogoView(String dialogo) {
        RequestContext.getCurrentInstance().update("form:" + dialogo);
        RequestContext.getCurrentInstance().execute("PF('" + dialogo + "').hide()");
        RequestContext.getCurrentInstance().update("form:tableRefund");
    }

    public void CloseDialogUpdate(String dialog, String dataUpdate) {
        RequestContext.getCurrentInstance().update("form:" + dialog);
        RequestContext.getCurrentInstance().execute("PF('" + dialog + "').hide()");
        this.reimbursementRequestsChange();
        RequestContext.getCurrentInstance().update(dataUpdate);
    }

//    Funcion para deshabilitar los botones cuando el estado no es por abonar
    public boolean getVisibilityButton(int id) {
        if (id == StatusReimbursementEnum.POR_ABONAR.getValor()) {
            this.setVisibilityButton(true);
        } else {
            this.setVisibilityButton(false);
        }
        return this.isVisibilityButton();
    }

//    funcion que deshabilita los check cuando el estado no es aprobado
    public boolean disableCheck(int id) {
        if (id == StatusReimbursementEnum.APROBADO.getValor()) {
            this.setVisibilityCheck(false);
        } else {
            this.setVisibilityCheck(true);
        }
        return this.isVisibilityCheck();
    }

    /**
     * Funcion que permite al usuario descargar un excel con los datos mostrados en la tabla al seleccionar alguno de lo
     * estados
     */
    public void exportRowsReimbursement() {
        try {
            if (!this.getListReimbursement().isEmpty()) {

                if (this.getStatusReimbursementId() == StatusReimbursementEnum.APROBADO.getValor()) {
                    this.loadDataExportReimbursementApproved();
                } else if (this.getStatusReimbursementId() == StatusReimbursementEnum.POR_ABONAR.getValor()) {
                    this.loadDataExportReimbursementPerPay();
                } else if (this.getStatusReimbursementId() == StatusReimbursementEnum.ABONADO.getValor()) {
                    this.loadDataExportReimbursementPayment();
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_FATAL, "Error!", "Seleccione algun estado"));
                }

                this.export.xlsxExportRowsReimbursement(this.getJasperPrint(), "ReporteReembolso");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_FATAL, "Error!", "La tabla no tiene datos"));
            }
        } catch (Exception e) {
            System.err.println("Error " + e);
        }
    }

    public void loadDataExportReimbursementApproved() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getListReimbursement()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO APROBADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getListReimbursement());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursement.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    public void loadDataExportReimbursementPerPay() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getListReimbursement()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO POR ABONAR");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getListReimbursement());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursementPorAbonar.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    public void loadDataExportReimbursementPayment() throws JRException {
        /*Declaracion de atriburos internos para el jasper*/
        String logoPath = "", reportPath = "";
        /*Paso de parametros al Jasper*/
        logoPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/recursos/img/heal_logo2.png");
        this.params.put("listReimbursement", new JRBeanCollectionDataSource(this.getListReimbursement()));
        this.params.put("heal_logo2", logoPath);
        this.params.put("reportTitle", "REPORTE DE SOLICITUD(ES) DE REEMBOLSO EN ESTADO ABONADO");
        this.beanCollectionDataSource = new JRBeanCollectionDataSource(this.getListReimbursement());
        reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/reimbursement/reportByStatusReimbursementAbonado.jasper");
        this.jasperPrint = JasperFillManager.fillReport(reportPath, this.params, beanCollectionDataSource);
    }

    /**
     * Methods for data requirement
     *
     * @param reimbursement
     */
    public void downloadRequirementByReimbursement(Reimbursement reimbursement) {

        try {

            List<Requirement> requirementSelected = this.requerimientoLocal.findAllByReimbursement(reimbursement.getId());
            InputStream inputstream = new FileInputStream(requirementSelected.get(0).getFileSystemPath());

            if (inputstream != null) {
                this.setFileRequirement(new DefaultStreamedContent(inputstream, "application/pdf",
                        requirementSelected.get(0).getFileName()));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                        "Error!", "La solicitud no posee recaudos asociados!"));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Error!", "La solicitud no posee recaudos asociados!"));
        }

        RequestContext.getCurrentInstance().update("form:messageReimbursement");

    }
    
    public void generateReportTxtprueba() throws IOException, SQLException {

        String route = "/home/ftp/reimbursement/txt/documentBack.txt";
        File archivo = new File(route);
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo));
        DecimalFormat decf = new DecimalFormat("##.00");
        
        this.setGenerateTxtOutputList(this.servicePaymentRefund.ConfirmGenerateTxtRefund(0,statusReimbursementId, null, null));

        for (GenerateTxtOutput generateTxtOutput: this.getGenerateTxtOutputList()) {
            String monto=generateTxtOutput.getEntireAmount()+generateTxtOutput.getDecimalAmount();
           
            
            int tmp = 35-generateTxtOutput.getPatientName().length();
            
            for (int i = 0; i < tmp; i++) {
                generateTxtOutput.setPatientName(generateTxtOutput.getPatientName() + " ");
            }
            
            
            
            bw.write(generateTxtOutput.getAccountNumber()
                    + " " + generateTxtOutput.getPatient_Cedula()
                    + " " +monto 
                    + " " + generateTxtOutput.getPaymentReference()
                    + " " + generateTxtOutput.getPatientName()
                    + " " +generateTxtOutput.getEmailRecipient()+ "\r\n");
        }
        bw.close();
        InputStream stream = new FileInputStream(route);
        this.setTxt(new DefaultStreamedContent(stream, "application/txt", "TxtParaNetcash.txt"));
    }

    /**
     * Funcion que permite generar y descargar un txt
     *
     * @throws java.io.IOException
     */
//    public void generateReportTxt() throws IOException {
//
//        String route = "/home/ftp/reimbursement/txt/documentBack.txt";
//        File archivo = new File(route);
//        BufferedWriter bw;
//        bw = new BufferedWriter(new FileWriter(archivo));
//        DecimalFormat decf = new DecimalFormat("###.#");
//
//        for (Reimbursement reimbursement : this.getListReimbursement()) {
//            String aux = decf.format(reimbursement.getMonto_remb_pagar());
//            //String aux =reimbursement.getMonto_remb_pagar().toString();
//            log.log(Level.SEVERE, "MONTO A PAGAR REEMBOLSO: {0}", aux);
//            bw.write(reimbursement.getBanksPatientsId().getAccountNumber()
//                    + " " + this.trimText(String.valueOf(reimbursement.getPatientsId().getIdentityNumberHolder()), 10, IDENTITYNUMBER)
//                    + " " + this.trimText(returnWithoutDecimal(aux), 15, AMOUT) + " "
//                    + this.trimText(reimbursement.getCompleteNameTitular(), 67, NAME) + "\r\n");
//        }
//        bw.close();
//        InputStream stream = new FileInputStream(route);
//        this.setTxt(new DefaultStreamedContent(stream, "application/txt", "TxtParaNetcash.txt"));
//    }
//
//    String returnWithoutDecimal(String a) {
//        String aux = "";
//        for (int i = 0; i < a.length(); i++) {
//            if (!(a.charAt(i) == ',')) {
//                aux = aux + a.charAt(i);
//            }
//        }
//        return aux;
//
//    }

    /**
     * Funcion que se encarga de completar el espacio necesario para el txt esta recibe un string y le coloca 0 adelante
     * de ella
     *
     * @param dato
     * @param limite
     * @return
     */
    public String trimText(String dato, int limite, String tipo) {
        if (tipo.equalsIgnoreCase(IDENTITYNUMBER)) {
            if (Double.parseDouble(dato) > 80000000) {
                for (int i = dato.length(); i < limite; i++) {
                    dato = "0" + dato;
                }
                dato = "E" + dato;
            } else {
                for (int i = dato.length(); i < limite; i++) {
                    dato = "0" + dato;
                }
                dato = "V" + dato;
            }
        } else if (tipo.equalsIgnoreCase(AMOUT)) {
            for (int i = dato.length(); i < limite; i++) {
                dato = "0" + dato;
            }
        } else {
            String aux = "";
            try {
                aux = dato.substring(0, limite);
            } catch (Exception e) {
                aux = dato.substring(0, dato.length());
            }
            dato = aux;
        }
        return dato;
    }

    @PostConstruct
    public void init() {
        setBtnVisibleAprobar(true);
        this.setBtnExport(false);
        this.setColumnPayment(false);
        this.setSeeColumnCheck(false);
        this.setColumnState(true);
        this.setBtnFinancial(true);
        this.setSeeAmountTotal(false);
        this.setTotalPrice("0,00");
        this.setBtnGenerateTxt(false);
        this.export = new ConfigViewBeanReport();
        setSelectedFinancial(new Reimbursement());
        this.getDataReimbursements();
        if (!this.getListReimbursement().isEmpty()) {
            this.setSeeColumnCheck(false);
            this.setSeeAmountTotal(false);
        }
        this.getListStatusReimbursement().add(this.statusReimbursementFacadeLocal.findById(StatusReimbursementEnum.APROBADO.getValor()));
        this.getListStatusReimbursement().add(this.statusReimbursementFacadeLocal.findById(StatusReimbursementEnum.POR_ABONAR.getValor()));
        this.getListStatusReimbursement().add(this.statusReimbursementFacadeLocal.findById(StatusReimbursementEnum.ABONADO.getValor()));
        this.setSelectedDate(null);
        dateLimit();
    }

    //Obtener datos de los reembolsos
    public void getDataReimbursements() {
        this.setListReimbursement(this.reimbursementFacadeLocal.findByTwoStatus(StatusReimbursementEnum.APROBADO.getValor(), StatusReimbursementEnum.POR_ABONAR.getValor()));
        for (Reimbursement reimbursement : this.listReimbursement) {
            Patients patient = reimbursement.getPlansPatientsId().getPatientsId();
            if (!Objects.equals(patient.getRelationshipId().getId(), Relationship.TITULAR.getValor())) {
                //beneficiary
                List<Patients> patienrHolder = new ArrayList<Patients>();
                //obtenemos los datos del titular
                patienrHolder = this.patientsFacadeLocal.findByIdentityNumberAndRelationShip(reimbursement.
                        getPlansPatientsId().getPatientsId().getIdentityNumberHolder(), Relationship.TITULAR.getValor());
                if (!patienrHolder.isEmpty()) {
                    reimbursement.setCompleteNameTitular(patienrHolder.get(0).getCompleteName());
                    reimbursement.setIndentityNumberTitular(patienrHolder.get(0).getIdentityNumber());
                } else {
                    reimbursement.setCompleteNameTitular(patient.getCompleteName());
                    reimbursement.setIndentityNumberTitular(patient.getIdentityNumberHolder());
                }
                //obtencion de datos del beneficiario
                reimbursement.setCompleteNameBeneficiary(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());

            } else {
                //holder
                reimbursement.setCompleteNameTitular(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());
                reimbursement.setIndentityNumberTitular(reimbursement.getPlansPatientsId().getPatientsId().getIdentityNumber());
                //obtencion de datos del beneficiario
                reimbursement.setCompleteNameBeneficiary("-");
            }

        }

    }

    //funcion que busca y lista los reembolsos segun el estatus solicitado
    public void findReimbursementByStatus(int status) {

        if (status == StatusReimbursementEnum.ABONADO.getValor()) {
            this.setListReimbursement(this.getReimbursementFacadeLocal().findByStatusTransfer(status));
            this.setColumnPayment(true);
        } else {
            this.setListReimbursement(this.getReimbursementFacadeLocal().findByStatus(status));
            this.setColumnPayment(false);
        }
        // visualizacion de la columna check
        if (status == StatusReimbursementEnum.APROBADO.getValor()) {
            this.setSeeColumnCheck(true);

        } else {
            this.setSeeColumnCheck(false);
        }

        List<Patients> patientsTitular;

        for (Reimbursement reimbursement : this.getListReimbursement()) {

            reimbursement.getPlansPatientsId().getPatientsId().getId();

            if (reimbursement.getPlansPatientsId().getPatientsId().getRelationshipId().getId() != Relationship.TITULAR.getValor()) {

                patientsTitular = this.patientsFacadeLocal.findByIdentityNumberAndRelationShip(
                        reimbursement.getPlansPatientsId().getPatientsId().getIdentityNumberHolder(), Relationship.TITULAR.getValor()
                );


                if (patientsTitular != null) {

                    reimbursement.setIndentityNumberTitular(patientsTitular.get(0).getIdentityNumber());
                    reimbursement.setCompleteNameTitular(patientsTitular.get(0).getCompleteName());
                    reimbursement.setCompleteNameBeneficiary(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Error:", "El Paciente beneficiario no posee Titular!"));
                }

            } else {
                // --> Asigamos Valores a los transients al Titular tipo 10
                reimbursement.setIndentityNumberTitular(reimbursement.getPlansPatientsId().getPatientsId().getIdentityNumber());
                reimbursement.setCompleteNameTitular(reimbursement.getPlansPatientsId().getPatientsId().getCompleteName());
                reimbursement.setCompleteNameBeneficiary("-");
            }
        }

        if (this.getListReimbursement().isEmpty()) {
            this.setBtnExport(false);
        } else {
            this.setBtnExport(true);
        }

        RequestContext.getCurrentInstance().update("form:tableRefund");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnExport");

    }

    public void resetDataTableReimbursement() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tableRefund");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:tableRefund");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:tableRefund");
        }
    }

    //funcion que se ejecuta cuando uno de los radio es pulsado valida el estatus y setea la tabla
    public void reimbursementRequestsChange() {

        this.setColumnState(false);

        if (this.getStatusReimbursementId() != 0) {
            this.resetDataTableReimbursement();

            if (this.getStatusReimbursementId() == StatusReimbursementEnum.APROBADO.getValor()) {
                this.findReimbursementByStatus(StatusReimbursementEnum.APROBADO.getValor());
                this.setBtnFinancial(true);
                this.setSeeColumnCheck(true);
                if (this.getListReimbursement().isEmpty()) {
                    this.setSeeAmountTotal(false);
                } else {
                    this.setSeeAmountTotal(true);
                }
                // botones
                this.setBtnVisibleAprobar(true);
                this.setBtnGenerateTxt(false);
                this.setStatusPaymentColumn(false);
            }

            if (this.getStatusReimbursementId() == StatusReimbursementEnum.POR_ABONAR.getValor()) {
                this.findReimbursementByStatus(StatusReimbursementEnum.POR_ABONAR.getValor());
                // botones
                this.setBtnFinancial(false);
                this.setBtnVisibleAprobar(true);
                this.setSeeColumnCheck(false);
                this.setSeeAmountTotal(false);
                this.setStatusPaymentColumn(false);
                if (this.getListReimbursement().isEmpty()) {
                    this.setBtnGenerateTxt(false);
                } else {
                    this.setBtnGenerateTxt(true);
                }
            }

            if (this.getStatusReimbursementId() == StatusReimbursementEnum.ABONADO.getValor()) {
                this.findReimbursementByStatus(StatusReimbursementEnum.ABONADO.getValor());
                // botones
                this.setBtnFinancial(true);
                this.setBtnGenerateTxt(false);
                this.setBtnVisibleAprobar(true);
                this.setStatusPaymentColumn(true);
                this.setSeeColumnCheck(false);
                this.setSeeAmountTotal(false);
            }
        }
        //actualizacion de la tabla
        RequestContext.getCurrentInstance().execute("PF('tableRefund').filter()");
        RequestContext.getCurrentInstance().update("form:tableRefund");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:dialogDataFinancial");
        RequestContext.getCurrentInstance().update("form:paymentTotal");
        RequestContext.getCurrentInstance().update("form:amountTotal");
        RequestContext.getCurrentInstance().update("form:btnApprove");
        RequestContext.getCurrentInstance().update("form:btnGenerateTxt");
    }

    //funcion que muestra el pdf
    public void findRequirementByReimbursement() {
        this.setRequirementList(this.requerimientoLocal.findAllByReimbursement(this.getSelectedFinancial().getId()));
    }

    //funcion para llamar la bitacora seleccionada
    public List<BinnacleReimbursement> searchBinnacleReimbursementByReimbursementid(int reimbursement_id) {

        List<BinnacleReimbursement> lista = new ArrayList<>();
        lista = this.binnacleLocal.findAllByReimbursement(reimbursement_id);
        List<Reimbursement> patientsReimbursement;
        //List<>
        for (BinnacleReimbursement binnacleReimbursement : lista) {
            if (binnacleReimbursement.getStatusReimbursementId().getId() != StatusReimbursementEnum.SOLICITADO.getValor()) {
                // Users
                Users searchUsers;
                searchUsers = this.usuarioFacadeLocal.find(binnacleReimbursement.getUserId());
                binnacleReimbursement.setIssuerUser(searchUsers.getName());
            } else {
                // Client
                binnacleReimbursement.setIssuerUser(binnacleReimbursement.getReimbursementId().
                        getPlansPatientsId().getPatientsId().getCompleteName());
            }
        }
        return lista;
    }

    /**
     * Funcion encargada de sumar el total de de precios y acumularlos en una variable
     */
    public void sumPrices() {

        double acomulador = 0.00;
        for (Reimbursement iterator : this.listSelectedReimbursement) {
            acomulador += iterator.getMonto_remb_pagar();
        }

        DecimalFormat decf = new DecimalFormat("#,##0.00");

        if (acomulador == 0) {
            acomulador = 0.00;
            this.setTotalPrice(decf.format(acomulador));
        }
        this.setTotalPrice(decf.format(acomulador));

        RequestContext.getCurrentInstance().update("form:paymentTotal");
        RequestContext.getCurrentInstance().update("form:amountTotal");

    }

    /**
     *
     * Methods for selected rows in datatable
     */
    public void selectRow() {

        this.setBtnVisibleAprobar(false);
        RequestContext.getCurrentInstance().update("form:btnApprove");
        this.sumPrices();

    }

    /**
     *
     * Methods for Unselected rows in datatable
     */
    public void unSelectRow() {

        if (this.listSelectedReimbursement.isEmpty()) {
            this.setBtnVisibleAprobar(true);
        } else {
            this.setBtnVisibleAprobar(false);
        }
        this.sumPrices();
        RequestContext.getCurrentInstance().update("form:btnApprove");

    }

    /**
     *
     * Methods for toggleSelect rows in datatable
     */
    public void toggleSelect() {

        if (this.listSelectedReimbursement.isEmpty()) {
            this.setBtnVisibleAprobar(true);
        } else {
            this.setBtnVisibleAprobar(false);

        }

        this.sumPrices();
        RequestContext.getCurrentInstance().update("form:btnApprove");
    }

    /**
     * Method to read the properties file constants payment Refund Report
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().
                getResourceAsStream(ConstansPaymentRefundReport.FILENAME_PAYMENT_REPORT);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansPaymentRefundReport.FILENAME_PAYMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * Method to send an email with change status of reimbursement
     *
     * @param patiens
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailPatient(Reimbursement reimbursement) throws ParseException, IOException {
        log.log(Level.INFO, "Inicio - Proceso de envio de correo a paciente reembolso: {0}", reimbursement.getCompleteNameTitular());
        if (reimbursement.getResponse_email() != null) {
            String title = readFileProperties().getProperty(
                    ConstansPaymentRefundReport.PROPERTY_MESSAGE_TITLE
            );
            String subjectMessage = readFileProperties().getProperty(
                    ConstansPaymentRefundReport.PROPERTY_MESSAGE_TITLE3
            );
            String contentMessage = buildMessagePatiens(reimbursement);
            this.mailSenderLocal.send(Email.MAIL_REEMBOLSO, subjectMessage, title, contentMessage,
                    new ArrayList<File>(), reimbursement.getResponse_email());
        } else {
            log.log(Level.INFO, "El paciente no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos a pacientes");

    }

    /**
     * Method to construct the message body for the email that will be sent to patient
     *
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessagePatiens(Reimbursement reimbursement) throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_DEAR_PATIENS)).append("<br>");
        stringBuilder.append(readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_PROCESSING_PAYMENT)).
                append(" " + reimbursement.getNumberRequest()).append(".");
        stringBuilder.append("<br>" + readFileProperties().getProperty(ConstansPaymentRefundReport.PROPERTY_BETTER_SERVICE));
        return stringBuilder.toString();
    }

    /**
     * Method to set Date limit, range: [Year-1,Year]
     */
    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int minYear = year - 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setCurrentDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
    }

}