/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import java.text.ParseException;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import services.utils.Transformar;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "paymentReportViewBean")
@ViewScoped
public class PaymentReportViewBean extends BaseBean {

    private Date selectedDate, maxDate;
    private boolean btnExecuteManualDisabled = false;

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public boolean isBtnExecuteManualDisabled() {
        return btnExecuteManualDisabled;
    }

    public void setBtnExecuteManualDisabled(boolean btnExecuteManualDisabled) {
        this.btnExecuteManualDisabled = btnExecuteManualDisabled;
    }

}
