/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.ListaLinks;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "venedentalViewBean")
@ViewScoped
public class VenedentalViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    private List<ListaLinks> listaLinks;

    public VenedentalViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaLinks(new ArrayList<ListaLinks>());
        this.listaLinks.add(new ListaLinks("Servicios", "venedental.xhtml", "/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Plan Venedental", "planVenedental.xhtml", "/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Centro de Atención", "directorio.xhtml", "/img/list-bullets2.png"));
    }

    public List<ListaLinks> getListaLinks() {
        return listaLinks;
    }

    public void setListaLinks(List<ListaLinks> listaLinks) {
        this.listaLinks = listaLinks;
    }

}
