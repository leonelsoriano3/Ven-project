/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;
import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesTreatments;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "propertiesTreatmentsViewBean")
@ViewScoped
public class PropertiesTreatmentsViewBean {
    
    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;
    
    private List<PropertiesTreatments> listPropertiesTreatments;
    private List<PropertiesTreatments> listPropertiesTreatmentsAll;
    private List<PropertiesTreatments> listPropertiesTreatmentsFiltrada;
    private List<PropertiesTreatments> listPropertiesTreatmentsSelectedNews;
    private List<PropertiesTreatments> listPropertiesTreatmentsSeleccionados;
    private List<PropertiesTreatments> newListPropertiesTreatments;
    private List<PropertiesTreatments> newListPropertiesTreatmentsII;
    private List<PropertiesTreatments> newListPropertiesTreatmentsIII;
    private List<PropertiesTreatments> newListPropertiesTreatmentsIV;
    private List<PropertiesTreatments> newListPropertiesTreatmentsV;
    private List<PropertiesTreatments> newListPropertiesTreatmentsVI;
    private List<PropertiesTreatments> newListPropertiesTreatmentsVII;
    private List<PropertiesTreatments> newListPropertiesTreatmentsVIII;
    
     private List<PropertiesTreatments> selectedListPropertiesTreatments;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsII;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsIII;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsIV;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsV;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsVI;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsVII;
    private List<PropertiesTreatments> selectedListPropertiesTreatmentsVIII;
    
    private List<PropertiesTreatments> editListPropertiesTreatments;
    private List<PropertiesTreatments> editListPropertiesTreatmentsII;
    private List<PropertiesTreatments> editListPropertiesTreatmentsIII;
    private List<PropertiesTreatments> editListPropertiesTreatmentsIV;
    private List<PropertiesTreatments> editListPropertiesTreatmentsV;
    private List<PropertiesTreatments> editListPropertiesTreatmentsVI;
    private List<PropertiesTreatments> editListPropertiesTreatmentsVII;
    private List<PropertiesTreatments> editListPropertiesTreatmentsVIII;
    private List<PropertiesTreatments> beforeListPropertiesTreatments;
    
    private PropertiesTreatments editPropertiesTreatments;
    private PropertiesTreatments selectedPropertiesTreatments;
    private PropertiesTreatments newPropertiesTreatmentsObj;
    private String[] newPropertiesTreatments;
    private String color;
    private boolean visibleList;
    private boolean visibleListKeysHistory;
    private boolean disableEdit;
    private boolean visiblePanel;
     private boolean encontrado;
    private List<String> selectedOptions;
    private List<PropertiesTreatments> selectedOptionsAll;
     private List<PropertiesTreatments> selectedOptionsAllEdit;
    private List<String> selectedOptionsI;
    private List<String> selectedOptionsII;
    private List<String> selectedOptionsIII;
    private List<String> selectedOptionsIV;
    private List<String> selectedOptionsV;
    private List<String> selectedOptionsVI;
    private List<String> selectedOptionsVII;
    private List<String> selectedOptionsVIII;
    private List<String> selectedEditOptions;
    private List<String> selectedEditOptionsI;
    private List<String> selectedEditOptionsII;
    private List<String> selectedEditOptionsIII;
    private List<String> selectedEditOptionsIV;
    private List<String> selectedEditOptionsV;
    private List<String> selectedEditOptionsVI;
    private List<String> selectedEditOptionsVII;
    private List<String> selectedEditOptionsVIII;
    private boolean visibleProperties;
    

    

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    @ManagedProperty(value = "#{propetiesViewBean}")
    private PropertiesViewBean propetiesViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    
    
    public PropertiesTreatmentsViewBean() {
        this.editPropertiesTreatments = new PropertiesTreatments();
        //this.selectedPropertiesTreatments = new PropertiesTreatments();
        this.newPropertiesTreatments = new String[4];
        this.beforeListPropertiesTreatments = new ArrayList<>();
        this.visibleList = false;
        this.visibleListKeysHistory = false;
        this.disableEdit = true;
        this.encontrado = false;
    }
    
    @PostConstruct
    public void init() {
    
        addTreatments();
        this.getListPropertiesTreatmentsAll().addAll(this.propertiesTreatmentsFacadeLocal.findAll());
    
    }

    public boolean getVisibleListKeysHistory() {
        return visibleListKeysHistory;
    }

    public void setVisibleListKeysHistory(boolean visibleListKeysHistory) {
        this.visibleListKeysHistory = visibleListKeysHistory;
    }
    
    
    
    

    public boolean isEncontrado() {
        return encontrado;
    }

    public void setEncontrado(boolean encontrado) {
        this.encontrado = encontrado;
    }

    public List<PropertiesTreatments> getListPropertiesTreatmentsSelectedNews() {
        if (this.listPropertiesTreatmentsSelectedNews == null) {
            this.listPropertiesTreatmentsSelectedNews = new ArrayList<>();
        }
        return listPropertiesTreatmentsSelectedNews;
    }

    public void setListPropertiesTreatmentsSelectedNews(List<PropertiesTreatments> listPropertiesTreatmentsSelectedNews) {
        this.listPropertiesTreatmentsSelectedNews = listPropertiesTreatmentsSelectedNews;
    }
    public List<PropertiesTreatments> getSelectedOptionsAllEdit() {
         if (this.selectedOptionsAllEdit == null) {
            this.selectedOptionsAllEdit = new ArrayList<>();
        }
        return selectedOptionsAllEdit;
    }

    public void setSelectedOptionsAllEdit(List<PropertiesTreatments> selectedOptionsAllEdit) {
        this.selectedOptionsAllEdit = selectedOptionsAllEdit;
    }
    
    public List<PropertiesTreatments> getSelectedOptionsAll() {
        if (this.selectedOptionsAll == null) {
            this.selectedOptionsAll = new ArrayList<>();
        }
        return selectedOptionsAll;
    }

    public void setSelectedOptionsAll(List<PropertiesTreatments> selectedOptionsAll) {
        this.selectedOptionsAll = selectedOptionsAll;
    }
    
    public List<PropertiesTreatments> getListPropertiesTreatmentsAll() {
         if (this.listPropertiesTreatmentsAll == null) {
            this.listPropertiesTreatmentsAll = new ArrayList<>();
        }
        return listPropertiesTreatmentsAll;
    }

    public void setListPropertiesTreatmentsAll(List<PropertiesTreatments> listPropertiesTreatmentsAll) {
        this.listPropertiesTreatmentsAll = listPropertiesTreatmentsAll;
    }

 

    public boolean isDisableEdit() {
        return disableEdit;
    }

    public void setDisableEdit(boolean disableEdit) {
        this.disableEdit = disableEdit;
    }
    
    public boolean isVisiblePanel() {
        return visiblePanel;
    }

    public void setVisiblePanel(boolean visiblePanel) {
        this.visiblePanel = visiblePanel;
    }

    public List<String> getSelectedEditOptions() {
        if (this.selectedEditOptions == null) {
            this.selectedEditOptions = new ArrayList<>();
        }
        return selectedEditOptions;
    }

    public void setSelectedEditOptions(List<String> selectedEditOptions) {
        this.selectedEditOptions = selectedEditOptions;
    }

    public List<String> getSelectedEditOptionsI() {
        return selectedEditOptionsI;
    }

    public void setSelectedEditOptionsI(List<String> selectedEditOptionsI) {
        this.selectedEditOptionsI = selectedEditOptionsI;
    }

    public List<String> getSelectedEditOptionsII() {
        return selectedEditOptionsII;
    }

    public void setSelectedEditOptionsII(List<String> selectedEditOptionsII) {
        this.selectedEditOptionsII = selectedEditOptionsII;
    }

    public List<String> getSelectedEditOptionsIII() {
        return selectedEditOptionsIII;
    }

    public void setSelectedEditOptionsIII(List<String> selectedEditOptionsIII) {
        this.selectedEditOptionsIII = selectedEditOptionsIII;
    }

    public List<String> getSelectedEditOptionsIV() {
        return selectedEditOptionsIV;
    }

    public void setSelectedEditOptionsIV(List<String> selectedEditOptionsIV) {
        this.selectedEditOptionsIV = selectedEditOptionsIV;
    }

    public List<String> getSelectedEditOptionsV() {
        return selectedEditOptionsV;
    }

    public void setSelectedEditOptionsV(List<String> selectedEditOptionsV) {
        this.selectedEditOptionsV = selectedEditOptionsV;
    }

    public List<String> getSelectedEditOptionsVI() {
        return selectedEditOptionsVI;
    }

    public void setSelectedEditOptionsVI(List<String> selectedEditOptionsVI) {
        this.selectedEditOptionsVI = selectedEditOptionsVI;
    }

    public List<String> getSelectedEditOptionsVII() {
        return selectedEditOptionsVII;
    }

    public void setSelectedEditOptionsVII(List<String> selectedEditOptionsVII) {
        this.selectedEditOptionsVII = selectedEditOptionsVII;
    }

    public List<String> getSelectedEditOptionsVIII() {
        return selectedEditOptionsVIII;
    }

    public void setSelectedEditOptionsVIII(List<String> selectedEditOptionsVIII) {
        this.selectedEditOptionsVIII = selectedEditOptionsVIII;
    }

    public List<String> getSelectedOptions() {
        if (this.selectedOptions == null) {
            this.selectedOptions = new ArrayList<>();
        }
        return selectedOptions;
    }

    public void setSelectedOptions(List<String> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public List<String> getSelectedOptionsI() {
         if (this.selectedOptionsI == null) {
            this.selectedOptionsI = new ArrayList<>();
        }
        return selectedOptionsI;
    }

    public void setSelectedOptionsI(List<String> selectedOptionsI) {
        this.selectedOptionsI = selectedOptionsI;
    }

    public List<String> getSelectedOptionsII() {
         if (this.selectedOptionsII == null) {
            this.selectedOptionsII = new ArrayList<>();
        }
        return selectedOptionsII;
    }

    public void setSelectedOptionsII(List<String> selectedOptionsII) {
        this.selectedOptionsII = selectedOptionsII;
    }

    public List<String> getSelectedOptionsIII() {
         if (this.selectedOptionsIII == null) {
            this.selectedOptionsIII = new ArrayList<>();
        }
        return selectedOptionsIII;
    }

    public void setSelectedOptionsIII(List<String> selectedOptionsIII) {
        this.selectedOptionsIII = selectedOptionsIII;
    }

    public List<String> getSelectedOptionsIV() {
         if (this.selectedOptionsIV == null) {
            this.selectedOptionsIV = new ArrayList<>();
        }
        return selectedOptionsIV;
    }

    public void setSelectedOptionsIV(List<String> selectedOptionsIV) {
        this.selectedOptionsIV = selectedOptionsIV;
    }

    public List<String> getSelectedOptionsV() {
        if (this.selectedOptionsV == null) {
            this.selectedOptionsV = new ArrayList<>();
        }
        return selectedOptionsV;
    }

    public void setSelectedOptionsV(List<String> selectedOptionsV) {
        this.selectedOptionsV = selectedOptionsV;
    }

    public List<String> getSelectedOptionsVI() {
        if (this.selectedOptionsVI == null) {
            this.selectedOptionsVI = new ArrayList<>();
        }
        return selectedOptionsVI;
    }

    public void setSelectedOptionsVI(List<String> selectedOptionsVI) {
        this.selectedOptionsVI = selectedOptionsVI;
    }

    public List<String> getSelectedOptionsVII() {
        if (this.selectedOptionsVII == null) {
            this.selectedOptionsVII = new ArrayList<>();
        }
        return selectedOptionsVII;
    }

    public void setSelectedOptionsVII(List<String> selectedOptionsVII) {
        this.selectedOptionsVII = selectedOptionsVII;
    }

    public List<String> getSelectedOptionsVIII() {
        if (this.selectedOptionsVIII == null) {
            this.selectedOptionsVIII = new ArrayList<>();
        }
        return selectedOptionsVIII;
    }

    public void setSelectedOptionsVIII(List<String> selectedOptionsVIII) {
        this.selectedOptionsVIII = selectedOptionsVIII;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }


    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public List<PropertiesTreatments> getBeforeListPropertiesTreatments() {
        return this.beforeListPropertiesTreatments;
    }

    public void setBeforeListPropertiesTreatments(List<PropertiesTreatments> beforeListPropertiesTreatments) {
        if (this.beforeListPropertiesTreatments == null) {
            this.beforeListPropertiesTreatments = new ArrayList<>();
        }
        this.beforeListPropertiesTreatments = beforeListPropertiesTreatments;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }


    public List<PropertiesTreatments> getEditListPropertiesTreatments() {
         if (this.editListPropertiesTreatments == null) {
            this.editListPropertiesTreatments = new ArrayList<>();
        }
          Collections.sort(this.editListPropertiesTreatments, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatments;
    }

    public void setEditListPropertiesTreatments(List<PropertiesTreatments> editListPropertiesTreatments) {
       
        this.editListPropertiesTreatments = editListPropertiesTreatments;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatments() {
        if (this.newListPropertiesTreatments == null) {
            this.newListPropertiesTreatments = new ArrayList<>();
        }
        Collections.sort(this.newListPropertiesTreatments, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatments;
    }

    public void setNewListPropertiesTreatments(List<PropertiesTreatments> newListPropertiesTreatments) {
        
        this.newListPropertiesTreatments = newListPropertiesTreatments;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsII() {
        if (this.newListPropertiesTreatmentsII == null) {
            this.newListPropertiesTreatmentsII = new ArrayList<>();
        }
         Collections.sort( this.newListPropertiesTreatmentsII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsII;
    }

    public void setNewListPropertiesTreatmentsII(List<PropertiesTreatments> newListPropertiesTreatmentsII) {
        this.newListPropertiesTreatmentsII = newListPropertiesTreatmentsII;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsIII() {
        if (this.newListPropertiesTreatmentsIII == null) {
            this.newListPropertiesTreatmentsIII = new ArrayList<>();
        }
         Collections.sort( this.newListPropertiesTreatmentsIII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsIII;
    }

    public void setNewListPropertiesTreatmentsIII(List<PropertiesTreatments> newListPropertiesTreatmentsIII) {
        this.newListPropertiesTreatmentsIII = newListPropertiesTreatmentsIII;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsIV() {
        if (this.newListPropertiesTreatmentsIV == null) {
            this.newListPropertiesTreatmentsIV = new ArrayList<>();
        }
         Collections.sort( this.newListPropertiesTreatmentsIV, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsIV;
    }

    public void setNewListPropertiesTreatmentsIV(List<PropertiesTreatments> newListPropertiesTreatmentsIV) {
        this.newListPropertiesTreatmentsIV = newListPropertiesTreatmentsIV;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsV() {
        if (this.newListPropertiesTreatmentsV == null) {
            this.newListPropertiesTreatmentsV = new ArrayList<>();
        }
         Collections.sort( this.newListPropertiesTreatmentsV, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        
        return newListPropertiesTreatmentsV;
    }

    public void setNewListPropertiesTreatmentsV(List<PropertiesTreatments> newListPropertiesTreatmentsV) {
        this.newListPropertiesTreatmentsV = newListPropertiesTreatmentsV;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsVI() {
        if (this.newListPropertiesTreatmentsVI == null) {
            this.newListPropertiesTreatmentsVI = new ArrayList<>();
        }
         Collections.sort(this.newListPropertiesTreatmentsVI, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsVI;
    }

    public void setNewListPropertiesTreatmentsVI(List<PropertiesTreatments> newListPropertiesTreatmentsVI) {
        this.newListPropertiesTreatmentsVI = newListPropertiesTreatmentsVI;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsVII() {
         if (this.newListPropertiesTreatmentsVII == null) {
            this.newListPropertiesTreatmentsVII = new ArrayList<>();
        }
          Collections.sort(this.newListPropertiesTreatmentsVII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsVII;
    }

    public void setNewListPropertiesTreatmentsVII(List<PropertiesTreatments> newListPropertiesTreatmentsVII) {
        this.newListPropertiesTreatmentsVII = newListPropertiesTreatmentsVII;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatmentsVIII() {
          if (this.newListPropertiesTreatmentsVIII == null) {
            this.newListPropertiesTreatmentsVIII = new ArrayList<>();
        }
           Collections.sort(this.newListPropertiesTreatmentsVIII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return newListPropertiesTreatmentsVIII;
    }

    public void setNewListPropertiesTreatmentsVIII(List<PropertiesTreatments> newListPropertiesTreatmentsVIII) {
        this.newListPropertiesTreatmentsVIII = newListPropertiesTreatmentsVIII;
    }
    
    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }
    


    public List<PropertiesTreatments> getListPropertiesTreatmentsFiltrada() {
        return listPropertiesTreatmentsFiltrada;
    }

    public void setListPropertiesTreatmentsFiltrada(List<PropertiesTreatments> listPropertiesTreatmentsFiltrada) {
        this.listPropertiesTreatmentsFiltrada = listPropertiesTreatmentsFiltrada;
    }
    public List<PropertiesTreatments> getListPropertiesTreatments() {
        if (this.listPropertiesTreatments == null) {
            this.listPropertiesTreatments = new ArrayList<>();
        }
        return listPropertiesTreatments;
    }

    public void setListPropertiesTreatments(List<PropertiesTreatments> listPropertiesTreatments) {
        this.listPropertiesTreatments = listPropertiesTreatments;
    }

    public PropertiesTreatments getEditPropertiesTreatments() {
        return editPropertiesTreatments;
    }

    public void setEditPropertiesTreatments(PropertiesTreatments editPropertiesTreatments) {
        this.editPropertiesTreatments = editPropertiesTreatments;
    }

    public PropertiesTreatments getSelectedPropertiesTreatments() {
        return selectedPropertiesTreatments;
    }

    public void setSelectedPropertiesTreatments(PropertiesTreatments selectedPropertiesTreatments) {
        this.selectedPropertiesTreatments = selectedPropertiesTreatments;
    }

    public String[] getNewPropertiesTreatments() {
        return newPropertiesTreatments;
    }

    public void setNewPropertiesTreatments(String[] newPropertiesTreatments) {
        this.newPropertiesTreatments = newPropertiesTreatments;
    }

    public PropertiesTreatments getNewPropertiesTreatmentsObj() {
        return newPropertiesTreatmentsObj;
    }

    public void setNewPropertiesTreatmentsObj(PropertiesTreatments newPropertiesTreatmentsObj) {
        this.newPropertiesTreatmentsObj = newPropertiesTreatmentsObj;
    }

    public PropertiesViewBean getPropetiesViewBean() {
        return propetiesViewBean;
    }

    public void setPropetiesViewBean(PropertiesViewBean propetiesViewBean) {
        this.propetiesViewBean = propetiesViewBean;
    }
    
    public List<PropertiesTreatments> getListPropertiesTreatmentsSeleccionados() {
         if (this.listPropertiesTreatmentsSeleccionados == null) {
            this.listPropertiesTreatmentsSeleccionados = new ArrayList<>();
        }
        return listPropertiesTreatmentsSeleccionados;
    }

    public void setListPropertiesTreatmentsSeleccionados(List<PropertiesTreatments> listPropertiesTreatmentsSeleccionados) {
        this.listPropertiesTreatmentsSeleccionados = listPropertiesTreatmentsSeleccionados;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsII() {
        if (this.editListPropertiesTreatmentsII == null) {
            this.editListPropertiesTreatmentsII= new ArrayList<>();
        }
        Collections.sort(this.editListPropertiesTreatmentsII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsII;
    }

    public void setEditListPropertiesTreatmentsII(List<PropertiesTreatments> editListPropertiesTreatmentsII) {
        this.editListPropertiesTreatmentsII = editListPropertiesTreatmentsII;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsIII() {
        if (this.editListPropertiesTreatmentsIII == null) {
            this.editListPropertiesTreatmentsIII = new ArrayList<>();
            
        }
          Collections.sort( this.editListPropertiesTreatmentsIII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsIII;
    }

    public void setEditListPropertiesTreatmentsIII(List<PropertiesTreatments> editListPropertiesTreatmentsIII) {
        this.editListPropertiesTreatmentsIII = editListPropertiesTreatmentsIII;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsIV() {
        if (this.editListPropertiesTreatmentsIV == null) {
            this.editListPropertiesTreatmentsIV = new ArrayList<>();
        }
       Collections.sort(this.editListPropertiesTreatmentsIV, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsIV;
    }

    public void setEditListPropertiesTreatmentsIV(List<PropertiesTreatments> editListPropertiesTreatmentsIV) {
        this.editListPropertiesTreatmentsIV = editListPropertiesTreatmentsIV;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsV() {
        if (this.editListPropertiesTreatmentsV == null) {
            this.editListPropertiesTreatmentsV = new ArrayList<>();
        }
        Collections.sort(this.editListPropertiesTreatmentsV, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsV;
    }

    public void setEditListPropertiesTreatmentsV(List<PropertiesTreatments> editListPropertiesTreatmentsV) {
        this.editListPropertiesTreatmentsV = editListPropertiesTreatmentsV;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsVI() {
        if (this.editListPropertiesTreatmentsVI == null) {
            this.editListPropertiesTreatmentsVI = new ArrayList<>();
        }
        return editListPropertiesTreatmentsVI;
    }

    public void setEditListPropertiesTreatmentsVI(List<PropertiesTreatments> editListPropertiesTreatmentsVI) {
        this.editListPropertiesTreatmentsVI = editListPropertiesTreatmentsVI;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsVII() {
        if (this.editListPropertiesTreatmentsVII == null) {
            this.editListPropertiesTreatmentsVII = new ArrayList<>();
        }
          Collections.sort( this.editListPropertiesTreatmentsVII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsVII;
    }

    public void setEditListPropertiesTreatmentsVII(List<PropertiesTreatments> editListPropertiesTreatmentsVII) {
        this.editListPropertiesTreatmentsVII = editListPropertiesTreatmentsVII;
    }

    public List<PropertiesTreatments> getEditListPropertiesTreatmentsVIII() {
        if (this.editListPropertiesTreatmentsVIII == null) {
            this.editListPropertiesTreatmentsVIII = new ArrayList<>();
        }
        Collections.sort( this.editListPropertiesTreatmentsVIII, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                    PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                    return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

                }
            });
        return editListPropertiesTreatmentsVIII;
    }

    public void setEditListPropertiesTreatmentsVIII(List<PropertiesTreatments> editListPropertiesTreatmentsVIII) {
        this.editListPropertiesTreatmentsVIII = editListPropertiesTreatmentsVIII;
    }
    
    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatments() {
        if (this.selectedListPropertiesTreatments == null) {
            this.selectedListPropertiesTreatments = new ArrayList<>();
        }
        return selectedListPropertiesTreatments;
    }

    public void setSelectedListPropertiesTreatments(List<PropertiesTreatments> selectedListPropertiesTreatments) {
        this.selectedListPropertiesTreatments = selectedListPropertiesTreatments;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsII() {
        if (this.selectedListPropertiesTreatmentsII == null) {
            this.selectedListPropertiesTreatmentsII = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsII;
    }

    public void setSelectedListPropertiesTreatmentsII(List<PropertiesTreatments> selectedListPropertiesTreatmentsII) {
        this.selectedListPropertiesTreatmentsII = selectedListPropertiesTreatmentsII;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsIII() {
        if (this.selectedListPropertiesTreatmentsIII == null) {
            this.selectedListPropertiesTreatmentsIII = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsIII;
    }

    public void setSelectedListPropertiesTreatmentsIII(List<PropertiesTreatments> selectedListPropertiesTreatmentsIII) {
        this.selectedListPropertiesTreatmentsIII = selectedListPropertiesTreatmentsIII;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsIV() {
        if (this.selectedListPropertiesTreatmentsIV == null) {
            this.selectedListPropertiesTreatmentsIV = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsIV;
    }

    public void setSelectedListPropertiesTreatmentsIV(List<PropertiesTreatments> selectedListPropertiesTreatmentsIV) {
        this.selectedListPropertiesTreatmentsIV = selectedListPropertiesTreatmentsIV;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsV() {
        if (this.selectedListPropertiesTreatmentsV == null) {
            this.selectedListPropertiesTreatmentsV = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsV;
    }

    public void setSelectedListPropertiesTreatmentsV(List<PropertiesTreatments> selectedListPropertiesTreatmentsV) {
        this.selectedListPropertiesTreatmentsV = selectedListPropertiesTreatmentsV;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsVI() {
        if (this.selectedListPropertiesTreatmentsVI == null) {
            this.selectedListPropertiesTreatmentsVI = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsVI;
    }

    public void setSelectedListPropertiesTreatmentsVI(List<PropertiesTreatments> selectedListPropertiesTreatmentsVI) {
        this.selectedListPropertiesTreatmentsVI = selectedListPropertiesTreatmentsVI;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsVII() {
        if (this.selectedListPropertiesTreatmentsVII == null) {
            this.selectedListPropertiesTreatmentsVII = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsVII;
    }

    public void setSelectedListPropertiesTreatmentsVII(List<PropertiesTreatments> selectedListPropertiesTreatmentsVII) {
        this.selectedListPropertiesTreatmentsVII = selectedListPropertiesTreatmentsVII;
    }

    public List<PropertiesTreatments> getSelectedListPropertiesTreatmentsVIII() {
        if (this.selectedListPropertiesTreatmentsVIII == null) {
            this.selectedListPropertiesTreatmentsVIII = new ArrayList<>();
        }
        return selectedListPropertiesTreatmentsVIII;
    }

    public void setSelectedListPropertiesTreatmentsVIII(List<PropertiesTreatments> selectedListPropertiesTreatmentsVIII) {
        this.selectedListPropertiesTreatmentsVIII = selectedListPropertiesTreatmentsVIII;
    }

    public void disable(int item) {
        PlansTreatmentsKeys plansTreatmentsKeys = this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().get(item);
        plansTreatmentsKeys.setDisableEdit(!disableEdit);

        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().remove(plansTreatmentsKeys);
        
        if (item == this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().size() && item > 0) {
            item = item - 1;
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().set(item, plansTreatmentsKeys);
        } else {
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(plansTreatmentsKeys);
        }
        
        
    }
    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }

     public void addPropertiesByList(String idDialog){
         
        
        if(!this.getNewListPropertiesTreatments().isEmpty()){
        
       for(PropertiesTreatments propertiesTreatments : this.getNewListPropertiesTreatments()){
          
           String properties = propertiesTreatments.getPropertiesId().getStringValue();
            this.getSelectedOptions().add(properties);
       
           }
        }
     }
    
    
    public void addTreatments() {

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            this.setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));
            if (!this.getNewListPropertiesTreatments().isEmpty()) {
                //
                this.desplegarDialogoCreate("keysOdontodiagram", true);
                this.visibleList = true;
            }
        }
    }
     public void deletePropertiesTreatments(int item) {
        if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {
            if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().size() == item) {
                item = item - 1;
            }
            PlansTreatmentsKeys planstreatmentsKey = this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().get(item);
            if(!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().isEmpty()){
            if(this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(planstreatmentsKey.getPlansTreatmentsId())){
             this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().remove(planstreatmentsKey.getPlansTreatmentsId());}
            }
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().remove(item);
            //this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysFiltrada(this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados());
            if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {
                this.getComponenteViewBean().setUltimoPaso(false);
            }
        }

    }
     
     
     
     
       public void fillListProperties(List<PropertiesTreatments> newListPropertiesTreatments) {
           limpiarLista();
        for (PropertiesTreatments propertiesTreatments : newListPropertiesTreatments) {
            int cuadrante = propertiesTreatments.getPropertiesId().getQuadrant();
            switch(cuadrante) {
                case 1:
                    this.getEditListPropertiesTreatments().add(propertiesTreatments);

                    break;
                case 2:
                    this.getEditListPropertiesTreatmentsII().add(propertiesTreatments);
                    break;
                case 3:
                    this.getEditListPropertiesTreatmentsIII().add(propertiesTreatments);

                    break;

                case 4:
                    this.getEditListPropertiesTreatmentsIV().add(propertiesTreatments);
                    break;

                case 5:
                    this.getEditListPropertiesTreatmentsV().add(propertiesTreatments);
                    break;

                case 6:
                    this.getEditListPropertiesTreatmentsVI().add(propertiesTreatments);
                    break;

                case 7:
                    this.getEditListPropertiesTreatmentsVII().add(propertiesTreatments);
                    break;

                case 8:
                    this.getEditListPropertiesTreatmentsVIII().add(propertiesTreatments);
                    break;
            }

        }

    }
    
    public void limpiarLista(){
    
    this.getEditListPropertiesTreatments().clear();
    this.getEditListPropertiesTreatmentsII().clear();
    this.getEditListPropertiesTreatmentsIII().clear();
    this.getEditListPropertiesTreatmentsIV().clear();
    this.getEditListPropertiesTreatmentsV().clear();
    this.getEditListPropertiesTreatmentsVI().clear();
    this.getEditListPropertiesTreatmentsVII().clear();
    this.getEditListPropertiesTreatmentsVIII().clear();
    }
    public void limpiarListaNew(){
    
    this.setNewListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
    this.setNewListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());
    }
    
    
     public void addTreatmentsNew(String idDialog) {

        this.limpiarListaNew();
         List<PropertiesTreatments>  editlistPropertiesTreatments = new ArrayList<>();

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            if (getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId() != null) {
                this.getPlansTreatmentsViewBean().setNameTreatments(getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getName());
               
                editlistPropertiesTreatments = this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId());  
               // this.setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));
            }
            if (! editlistPropertiesTreatments.isEmpty()) {
                fillListPropertiesEdit( editlistPropertiesTreatments);
                this.visibleProperties = true;

                this.setVisiblePanel(true);
              

            } else {

                this.setVisiblePanel(false);

            }

        }
     

    }
     
     public void fillListPropertiesEdit(List<PropertiesTreatments> newListPropertiesTreatments) {
       
        for (PropertiesTreatments propertiesTreatments : newListPropertiesTreatments) {
            int cuadrante = propertiesTreatments.getPropertiesId().getQuadrant();
            switch (cuadrante) {
                case 1:
                    this.getNewListPropertiesTreatments().add(propertiesTreatments);

                    break;
                case 2:
                    this.getNewListPropertiesTreatmentsII().add(propertiesTreatments);
                    break;
                case 3:
                    this.getNewListPropertiesTreatmentsIII().add(propertiesTreatments);

                    break;

                case 4:
                    this.getNewListPropertiesTreatmentsIV().add(propertiesTreatments);
                    break;

                case 5:
                    this.getNewListPropertiesTreatmentsV().add(propertiesTreatments);
                    break;

                case 6:
                    this.getNewListPropertiesTreatmentsVI().add(propertiesTreatments);
                    break;

                case 7:
                    this.getNewListPropertiesTreatmentsVII().add(propertiesTreatments);
                    break;

                case 8:
                    this.getNewListPropertiesTreatmentsVIII().add(propertiesTreatments);
                    break;
            }

        }

    }

    public boolean getVisibleProperties() {
        return visibleProperties;
    }

    public void setVisibleProperties(boolean visibleProperties) {
        this.visibleProperties = visibleProperties;
    }
  
      
    
    

}
