/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.constans.ConstansReimbursementForm;
import com.venedental.controller.BaseBean;
import com.venedental.enums.Relationship;
import com.venedental.enums.TypeSpecialistsEnums;
import com.venedental.facade.BanksFacadeLocal;
import com.venedental.facade.BanksPatientsFacadeLocal;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.InsurancesPatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.RequirementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.model.Banks;
import com.venedental.model.BanksPatients;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Patients;
import com.venedental.model.Reimbursement;
import com.venedental.model.InsurancesPatients;
import com.venedental.model.Plans;
import com.venedental.model.PlansPatients;
import com.venedental.model.Requirement;
import com.venedental.model.StatusReimbursement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import services.utils.CustomLogger;

/**
 *
 * @author Anthony Torres
 */
@ManagedBean(name = "reimbursementFormViewBean")
@ViewScoped
public class ReimbursementFormViewBean extends BaseBean {

    private static final Logger log = CustomLogger.getGeneralLogger(ReimbursementFormViewBean.class.getName());
    //-----Atributos-----//
    //EJB
    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;
    @EJB
    private BanksFacadeLocal bankFacadeLocal;
    @EJB
    private InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal;
    @EJB
    private PlansFacadeLocal plansFacadeLocal;
    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;
    @EJB
    private ReimbursementFacadeLocal reimbursementFacadeLocal;
    @EJB
    private BanksPatientsFacadeLocal banksPatientsFacadeLocal;
    @EJB
    private RequirementFacadeLocal requirementFacadeLocal;
    @EJB
    private StatusReimbursementFacadeLocal statusReimbursmentFacadeLocal;
    @EJB
    private BinnacleReimbursementFacadeLocal binnacleReimbursementFacadeLocal;
    @EJB
    private MailSenderLocal mailSenderLocal;

    //List
    private List<Patients> patientsList;
    private List<Patients> patientsListed;
    private List<Banks> bankList;
    private List<InsurancesPatients> insurancesPatientsList;
    private List<Patients> patientsHolder;
    private List<Patients> patientsBeneficiary;
    private List<PlansPatients> plansPatientsList;
    private List<Reimbursement> reimbursementList;
    private List<BanksPatients> banksPatientsList;
    private List<Requirement> requirementList;
    private List<StatusReimbursement> statusReimbursementList;
    private List<BinnacleReimbursement> binnacleReimbursementList;
    private List<UploadedFile> fileList;
    private Reimbursement reimbursementSave;
    private List<Plans> plansList;

    //Objets
    private Patients patient;
    private Patients patientsComplete;
    private Banks bank;
    private PlansPatients plansPatients;
    private Patients beneficiarySelected;
    private BanksPatients banksPatients;
    private Plans planSelected;

    private Date invoiceDate;

    private String completeName;
    private String completeNameholder;
    private String email;
    private String emailConfirm;
    private String insuranceName;
    private String company;
    private String phone;
    private String cellular;
    private String accountNumber;
    private String invoiceNumber;
    private String reimbursementHolder;
    private String code;
    private String identityNumberHolder;
    private String identityNumber;
    private String identityNumberHolderSet;
    private String minDate;
    private String actualDate;
    private String numberRequest;
    private String identityHolderImage;

    private String maskBanks;
    private String placeHolderbanks;

    private int insuranceId;
    private int[] ophthalmologyPlans;

    private Double invoiceAmount;

    private byte[] identityNumberHolderImage;

    private boolean visible;
    private boolean visible2;
    private boolean visible3;
    private boolean visible4;
    private boolean disabled;
    private boolean required;
    private boolean required2;
    private boolean planValid;
    private boolean identityBool;
    private boolean beneficiaryBool;
    private boolean medicalReportBool;
    private boolean opticalFormulaBool;
    private boolean invoiceBool;
    private boolean sendButtonBool;
    private boolean requerimentsRendered;
    private boolean isHolder;
    private boolean seeUploadIdentityHolder;
    private boolean disabledInputBank;
    private boolean disabledPlans;

    //-----End Objects-----//
    //-----PostConstruct-----//
    @PostConstruct
    public void init() {

        log.log(Level.INFO, "init()");
        this.setSeeUploadIdentityHolder(false);
        this.setReimbursementHolder("");
        this.setIdentityNumberHolder("");
        this.setIdentityNumberHolderSet("");
        this.setCompleteNameholder("");
        this.setCompleteName("");
        this.setInvoiceAmount(0.0);
        this.setVisible(false);
        this.setVisible2(false);
        this.setVisible3(false);
        this.setVisible4(false);
        this.setDisabled(true);
        this.setRequerimentsRendered(false);
        this.setDisabledInputBank(true);
        this.setIdentityBool(false);
        this.setBeneficiaryBool(true);
        this.setMedicalReportBool(true);
        this.setOpticalFormulaBool(true);
        this.setInvoiceBool(true);
        this.setSendButtonBool(true);
        this.setDisabledPlans(true);
        this.setMaskBanks("");
        reset();
        showBanks();
        dateLimit();

    }
    //-----End PostConstruct-----//

    //-----Metodos de acceso-----//
    public boolean isDisabledPlans() {
        return disabledPlans;
    }

    public void setDisabledPlans(boolean disabledPlans) {
        this.disabledPlans = disabledPlans;
    }

    public boolean isDisabledInputBank() {
        return disabledInputBank;
    }

    public void setDisabledInputBank(boolean disabledInputBank) {
        this.disabledInputBank = disabledInputBank;
    }

    public boolean isSeeUploadIdentityHolder() {
        return seeUploadIdentityHolder;
    }

    public void setSeeUploadIdentityHolder(boolean seeUploadIdentityHolder) {
        this.seeUploadIdentityHolder = seeUploadIdentityHolder;
    }

    public boolean isIsHolder() {
        return isHolder;
    }

    public MailSenderLocal getMailSenderLocal() {
        return mailSenderLocal;
    }

    public void setMailSenderLocal(MailSenderLocal mailSenderLocal) {
        this.mailSenderLocal = mailSenderLocal;
    }

    public boolean isVisible3() {
        return visible3;
    }

    public void setVisible3(boolean visible3) {
        this.visible3 = visible3;
    }

    public void setIsHolder(boolean isHolder) {
        this.isHolder = isHolder;
    }

    public Plans getPlanSelected() {
        return planSelected;
    }

    public boolean isRequired2() {
        return required2;
    }

    public boolean isVisible4() {
        return visible4;
    }

    public void setVisible4(boolean visible4) {
        this.visible4 = visible4;
    }

    public Double getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Double invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public void setRequired2(boolean required2) {
        this.required2 = required2;
    }

    public void setPlanSelected(Plans planSelected) {
        this.planSelected = planSelected;
    }

    public Reimbursement getReimbursementSave() {
        return reimbursementSave;
    }

    public void setReimbursementSave(Reimbursement reimbursementSave) {
        this.reimbursementSave = reimbursementSave;
    }

    public PatientsFacadeLocal getPatientsFacadeLocal() {
        return patientsFacadeLocal;
    }

    public List<Plans> getPlansList() {
        if (this.plansList == null) {
            this.plansList = new ArrayList<>();
        }
        return plansList;
    }

    public void setPlansList(List<Plans> plansList) {
        this.plansList = plansList;
    }

    public List<Patients> getPatientsListed() {
        if (this.patientsListed == null) {
            this.patientsListed = new ArrayList<>();
        }
        return patientsListed;
    }

    public void setPatientsListed(List<Patients> patientsListed) {
        this.patientsListed = patientsListed;
    }

    public boolean isRequerimentsRendered() {
        return requerimentsRendered;
    }

    public void setRequerimentsRendered(boolean requerimentsRendered) {
        this.requerimentsRendered = requerimentsRendered;
    }

    public void setPatientsFacadeLocal(PatientsFacadeLocal patientsFacadeLocal) {
        this.patientsFacadeLocal = patientsFacadeLocal;
    }

    public List<Patients> getPatientsList() {
        if (this.patientsList == null) {
            this.patientsList = new ArrayList<>();
        }
        return patientsList;
    }

    public void setPatientsList(List<Patients> patientsList) {
        this.patientsList = patientsList;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public boolean isBeneficiaryBool() {
        return beneficiaryBool;
    }

    public void setBeneficiaryBool(boolean beneficiaryBool) {
        this.beneficiaryBool = beneficiaryBool;
    }

    public boolean isSendButtonBool() {
        return sendButtonBool;
    }

    public void setSendButtonBool(boolean sendButtonBool) {
        this.sendButtonBool = sendButtonBool;
    }

    public boolean isMedicalReportBool() {
        return medicalReportBool;
    }

    public boolean isIdentityBool() {
        return identityBool;
    }

    public void setIdentityBool(boolean identityBool) {
        this.identityBool = identityBool;
    }

    public void setMedicalReportBool(boolean medicalReportBool) {
        this.medicalReportBool = medicalReportBool;
    }

    public boolean isOpticalFormulaBool() {
        return opticalFormulaBool;
    }

    public void setOpticalFormulaBool(boolean opticalFormulaBool) {
        this.opticalFormulaBool = opticalFormulaBool;
    }

    public boolean isInvoiceBool() {
        return invoiceBool;
    }

    public void setInvoiceBool(boolean invoiceBool) {
        this.invoiceBool = invoiceBool;
    }

    public List<UploadedFile> getFileList() {
        if (this.fileList == null) {
            this.fileList = new ArrayList<>();

        }
        return fileList;
    }

    public void setFileList(List<UploadedFile> fileList) {
        this.fileList = fileList;
    }

    public String getIdentityHolderImage() {
        return identityHolderImage;
    }

    public void setIdentityHolderImage(String identityHolderImage) {
        this.identityHolderImage = identityHolderImage;
    }

    public BinnacleReimbursementFacadeLocal getBinnacleReimbursementFacadeLocal() {
        return binnacleReimbursementFacadeLocal;
    }

    public void setBinnacleReimbursementFacadeLocal(BinnacleReimbursementFacadeLocal binnacleReimbursementFacadeLocal) {
        this.binnacleReimbursementFacadeLocal = binnacleReimbursementFacadeLocal;
    }

    public byte[] getIdentityNumberHolderImage() {
        return identityNumberHolderImage;
    }

    public void setIdentityNumberHolderImage(byte[] identityNumberHolderImage) {
        this.identityNumberHolderImage = identityNumberHolderImage;
    }

    public List<BinnacleReimbursement> getBinnacleReimbursementList() {
        return binnacleReimbursementList;
    }

    public void setBinnacleReimbursementList(List<BinnacleReimbursement> binnacleReimbursementList) {
        this.binnacleReimbursementList = binnacleReimbursementList;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public StatusReimbursementFacadeLocal getStatusReimbursmentFacadeLocal() {
        return statusReimbursmentFacadeLocal;
    }

    public void setStatusReimbursmentFacadeLocal(StatusReimbursementFacadeLocal statusReimbursmentFacadeLocal) {
        this.statusReimbursmentFacadeLocal = statusReimbursmentFacadeLocal;
    }

    public List<StatusReimbursement> getStatusReimbursementList() {
        return statusReimbursementList;
    }

    public void setStatusReimbursementList(List<StatusReimbursement> statusReimbursementList) {
        this.statusReimbursementList = statusReimbursementList;
    }

    public RequirementFacadeLocal getRequirementFacadeLocal() {
        return requirementFacadeLocal;
    }

    public void setRequirementFacadeLocal(RequirementFacadeLocal requirementFacadeLocal) {
        this.requirementFacadeLocal = requirementFacadeLocal;
    }

    public Patients getPatient() {
        return patient;
    }

    public ReimbursementFacadeLocal getReimbursementFacadeLocal() {
        return reimbursementFacadeLocal;
    }

    public String getNumberRequest() {
        return numberRequest;
    }

    public BanksPatientsFacadeLocal getBanksPatientsFacadeLocal() {
        return banksPatientsFacadeLocal;
    }

    public List<BanksPatients> getBanksPatientsList() {
        return banksPatientsList;
    }

    public void setBanksPatientsList(List<BanksPatients> banksPatientsList) {
        this.banksPatientsList = banksPatientsList;
    }

    public void setBanksPatientsFacadeLocal(BanksPatientsFacadeLocal banksPatientsFacadeLocal) {
        this.banksPatientsFacadeLocal = banksPatientsFacadeLocal;
    }

    public void setNumberRequest(String numberRequest) {
        this.numberRequest = numberRequest;
    }

    public BanksPatients getBanksPatients() {
        return banksPatients;
    }

    public void setBanksPatients(BanksPatients banksPatients) {
        this.banksPatients = banksPatients;
    }

    public void setReimbursementFacadeLocal(ReimbursementFacadeLocal reimbursementFacadeLocal) {
        this.reimbursementFacadeLocal = reimbursementFacadeLocal;
    }

    public List<Reimbursement> getReimbursementList() {
        return reimbursementList;
    }

    public void setReimbursementList(List<Reimbursement> reimbursementList) {
        this.reimbursementList = reimbursementList;
    }

    public PlansPatients getPlansPatients() {
        return plansPatients;
    }

    public int[] getOphthalmologyPlans() {
        return ophthalmologyPlans;
    }

    public void setOphthalmologyPlans(int[] ophthalmologyPlans) {
        this.ophthalmologyPlans = ophthalmologyPlans;
    }

    public boolean isPlanValid() {
        return planValid;
    }

    public void setPlanValid(boolean planValid) {
        this.planValid = planValid;
    }

    public void setPlansPatients(PlansPatients plansPatients) {
        this.plansPatients = plansPatients;
    }

    public PlansPatientsFacadeLocal getPlansPatientsFacadeLocal() {
        return plansPatientsFacadeLocal;
    }

    public void setPlansPatientsFacadeLocal(PlansPatientsFacadeLocal plansPatientsFacadeLocal) {
        this.plansPatientsFacadeLocal = plansPatientsFacadeLocal;
    }

    public List<PlansPatients> getPlansPatientsList() {
        return plansPatientsList;
    }

    public void setPlansPatientsList(List<PlansPatients> plansPatientsList) {
        this.plansPatientsList = plansPatientsList;
    }

    public String getActualDate() {
        return actualDate;
    }

    public void setActualDate(String actualDate) {
        this.actualDate = actualDate;
    }

    public void setPatient(Patients patient) {
        this.patient = patient;
    }

    public String getCompleteName() {
        return completeName;
    }

    public String getIdentityNumberHolderSet() {
        return identityNumberHolderSet;
    }

    public void setIdentityNumberHolderSet(String identityNumberHolderSet) {
        this.identityNumberHolderSet = identityNumberHolderSet;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getCompleteNameholder() {
        return completeNameholder;
    }

    public void setCompleteNameholder(String completeNameholder) {
        this.completeNameholder = completeNameholder;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public InsurancesPatientsFacadeLocal getInsurancesPatientsFacadeLocal() {
        return insurancesPatientsFacadeLocal;
    }

    public void setInsurancesPatientsFacadeLocal(InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal) {
        this.insurancesPatientsFacadeLocal = insurancesPatientsFacadeLocal;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellular() {
        return cellular;
    }

    public void setCellular(String cellular) {
        this.cellular = cellular;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getEmailConfirm() {
        return emailConfirm;
    }

    public String getPlaceHolderbanks() {
        return placeHolderbanks;
    }

    public void setPlaceHolderbanks(String placeHolderbanks) {
        this.placeHolderbanks = placeHolderbanks;
    }

    public String getMaskBanks() {
        return maskBanks;
    }

    public void setMaskBanks(String maskBanks) {
        this.maskBanks = maskBanks;
    }

    public void setEmailConfirm(String emailConfirm) {
        this.emailConfirm = emailConfirm;
    }

    public BanksFacadeLocal getBankFacadeLocal() {
        return bankFacadeLocal;
    }

    public void setBankFacadeLocal(BanksFacadeLocal bankFacadeLocal) {
        this.bankFacadeLocal = bankFacadeLocal;
    }

    public List<Banks> getBankList() {
        return bankList;
    }

    //
//    public LazyDataModel<Specialists> getListaLazySpecialists() {
//        if (this.listaSpecialists == null) {
//            this.listaSpecialists = this.specialistsFacadeLocal.findAll();
//            Collections.sort(this.listaSpecialists);
//            this.listaLazySpecialists = new LazySpecialists(this.listaSpecialists);
//        }
//        return listaLazySpecialists;
//    }
    //
    public void setBankList(List<Banks> bankList) {
        this.bankList = bankList;
    }

    public Banks getBank() {
        return bank;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public boolean isVisible2() {
        return visible2;
    }

    public void setVisible2(boolean visible2) {
        this.visible2 = visible2;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public int getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(int insuranceId) {
        this.insuranceId = insuranceId;
    }

    public List<InsurancesPatients> getInsurancesPatientsList() {
        if (this.insurancesPatientsList == null) {
            this.insurancesPatientsList = new ArrayList<>();
        }
        return insurancesPatientsList;
    }

    public void setInsurancesPatientsList(List<InsurancesPatients> insurancesPatientsList) {
        this.insurancesPatientsList = insurancesPatientsList;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getReimbursementHolder() {
        return reimbursementHolder;
    }

    public void setReimbursementHolder(String reimbursementHolder) {
        this.reimbursementHolder = reimbursementHolder;
    }

    public List<Patients> getPatientsBeneficiary() {
        if (this.patientsBeneficiary == null) {
            this.patientsBeneficiary = new ArrayList<>();
        }
        return patientsBeneficiary;
    }

    public void setPatientsBeneficiary(List<Patients> patientsBeneficiary) {
        this.patientsBeneficiary = patientsBeneficiary;
    }

    public Patients getBeneficiarySelected() {
        return beneficiarySelected;
    }

    public void setBeneficiarySelected(Patients beneficiarySelected) {
        this.beneficiarySelected = beneficiarySelected;
    }

    public List<Patients> getPatientsHolder() {
        if (this.patientsHolder == null) {
            this.patientsHolder = new ArrayList<>();
        }
        return patientsHolder;
    }

    public void setPatientsHolder(List<Patients> patientsHolder) {
        this.patientsHolder = patientsHolder;
    }

    public Patients getPatientsComplete() {
        return patientsComplete;
    }

    public void setPatientsComplete(Patients patientsComplete) {
        this.patientsComplete = patientsComplete;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIdentityNumberHolder() {
        return identityNumberHolder;
    }

    public void setIdentityNumberHolder(String identityNumberHolder) {
        this.identityNumberHolder = identityNumberHolder;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    //-----Fin Métodos de acceso-----//
    //-----Métodos-----//
    /**
     * Method to validate Identity Number
     *
     * @param event
     */
    public void validateIdentityNumber(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String value = comp.getSubmittedValue().toString();
        if (value.length() > 5 && value.length() < 6) {
            FacesMessage msg = new FacesMessage("La cédula debe contener al menos 6 caracteres");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
            RequestContext.getCurrentInstance().update("form1:form2:msgCedula");
        }
        if (value.length() == 0) {
            this.setReimbursementHolder(null);
            RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");

        }
    }

    /**
     * validate Account Number
     *
     * @param event
     */
    public void validateAccountNumber(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String value = comp.getSubmittedValue().toString();
        if (value.length() < 16) {
            FacesMessage msg = new FacesMessage("El número de cuenta debe tener 16.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
            RequestContext.getCurrentInstance().update("form1:form2:msgBanca");
        }
    }

    /**
     * Method to validate email coincidence
     *
     * @param event
     */
    public void validarEmail(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String value = comp.getSubmittedValue().toString();
        setEmail(value);
        if (check(comp, value)) {
            if (!value.equals(getEmailConfirm()) && this.getEmailConfirm().length() > 0) {
                UIInput compConfirm = (UIInput) findComponent("confirmEmail");
                FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                compConfirm.setValid(false);
                FacesContext.getCurrentInstance().addMessage(compConfirm.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }
    }

    /**
     * validate Confirm Email
     *
     * @param event
     */
    public void validateConfirmEmail(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String value = comp.getSubmittedValue().toString();
        setEmailConfirm(value);
        if (check(comp, value)) {
            if (!value.isEmpty() && value.length() > 0) {
                if (!value.equalsIgnoreCase(getEmail())) {
                    FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");

                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    comp.setValid(false);
                    FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                    FacesContext.getCurrentInstance().renderResponse();
                }
            }
        }
    }

    /**
     * Method to validate a the correct input of a email ej: email@email.com
     *
     * @param comp
     * @param value
     * @return
     */
    public boolean check(UIInput comp, String value) {

        if (!value.isEmpty() && !Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", value)) {
            //"^([\\dA-Za-z_\\.-]+)@([\\dA-Za-z\\.-]+)\\.([A-Za-z\\.]{2,6})$"
            FacesMessage msg = new FacesMessage("No es una dirección de correo válida");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
            return false;
        }
        return true;
    }

    /**
     * Method to validate email
     *
     * @param event
     */
    public void validateEmail(ComponentSystemEvent event) {
        UIInput compEmail = (UIInput) findComponent("correo");
        UIInput compConfirm = (UIInput) findComponent("confirmEmail");
        String valueEmail = compEmail.getLocalValue() == null ? "" : compEmail.getLocalValue().toString();
        String valueConfirm = compConfirm.getLocalValue() == null ? "" : compConfirm.getLocalValue().toString();
        if (valueConfirm.length() > 0) {
            if (!valueEmail.equalsIgnoreCase(valueConfirm)) {
                FacesMessage msg = new FacesMessage("Las direcciones de correo no coinciden");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                compConfirm.setValid(false);
                FacesContext.getCurrentInstance().addMessage(compConfirm.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
            }
        }
    }

    /**
     * Method to show back list
     */
    public void showBanks() {
        this.setBankList(this.bankFacadeLocal.findAllOrderByName());
    }

    /**
     * Method to clear the reimbursement form fields
     *
     */
    public void clearForm() {
        this.setIdentityNumberHolderSet("");
        this.setReimbursementHolder("");
        this.setIdentityNumberHolder("");
        this.setIdentityNumber("");
        this.setCompleteName("");
        this.setCompleteNameholder("");
        this.setInsuranceName("");
        this.setPatientsBeneficiary(new ArrayList<Patients>());
        this.setBeneficiarySelected(new Patients());
        this.setBank(new Banks());
        this.setBankList(new ArrayList<Banks>());
        this.setAccountNumber("");
        this.setCompany("");
        this.setInsuranceName("");
        this.setPhone("");
        this.setCellular("");
        this.setEmail("");
        this.setEmailConfirm("");
        this.setBank(new Banks());
        this.setBankList(new ArrayList<Banks>());
        this.setInvoiceAmount(null);
        this.setInvoiceDate(null);
        this.setInvoiceNumber("");
        this.setSeeUploadIdentityHolder(false);
        this.setDisabledInputBank(true);
        this.init();
        RequestContext contextC = RequestContext.getCurrentInstance();
        contextC.update("form1:form2:panelOpcion");
        contextC.update("form1:form2:panelPlanilla");
    }

    /**
     * Method to clear the reimbursement type selected
     *
     * @param idDialog
     */
    private void clearFormBySelection(String idDialog) {
        this.setIdentityNumberHolder("");
        this.setIdentityNumber("");
        this.setCompleteName("");
        this.setCompleteNameholder("");
        this.setInsuranceName("");
        this.setPatientsBeneficiary(new ArrayList<Patients>());
        this.setBeneficiarySelected(new Patients());
        this.setPlansList(new ArrayList<Plans>());
        this.setBank(new Banks());
        this.setBankList(new ArrayList<Banks>());
        this.setAccountNumber("");
        this.setCompany("");
        this.setInsuranceName("");
        this.setPhone("");
        this.setCellular("");
        this.setEmail("");
        this.setEmailConfirm("");
        this.setBank(new Banks());
        this.setBankList(new ArrayList<Banks>());
        this.setInvoiceAmount(null);
        this.setInvoiceDate(null);
        this.setInvoiceNumber("");
        this.setSeeUploadIdentityHolder(false);
        this.setDisabledInputBank(true);
        showBanks();
        RequestContext contextC = RequestContext.getCurrentInstance();
        contextC.update("form1:form2:" + idDialog);
    }

    /**
     * Method to set Date limit, range: [Year-1,Year]
     */
    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int minYear = year - 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setActualDate(String.valueOf(day) + "/" + String.valueOf(month) + "/" + String.valueOf(year));
    }

    /**
     * reTyped
     *
     * @param event
     */
    public void reTyped(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        String value = comp.getSubmittedValue().toString();
        if (this.getIdentityNumberHolder().length() > 0 && !this.getIdentityNumberHolder().equalsIgnoreCase("") && !this.getIdentityNumberHolder().equalsIgnoreCase(null) && !value.equalsIgnoreCase(this.getIdentityNumberHolder())) {

            this.setVisible(false);
            this.setVisible2(false);
            this.setVisible3(false);
            this.setVisible4(false);
            this.setRequerimentsRendered(false);
            this.setReimbursementHolder("");

            if (this.getReimbursementHolder().equalsIgnoreCase("") || this.getReimbursementHolder().equalsIgnoreCase(null)) {
                this.setDisabled(true);
                RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
                RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
            } else {
                this.setDisabled(false);
                RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
                RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
            }
        } else if (this.getIdentityNumberHolder().equalsIgnoreCase("") && value.isEmpty() && this.getIdentityNumberHolderSet().equals("")) {
            this.setIdentityNumberHolderSet("");
            this.setVisible(false);
            this.setVisible2(false);
            this.setVisible3(false);
            this.setVisible4(false);
            this.setRequerimentsRendered(false);
            this.setReimbursementHolder("");
            reset();

            this.setDisabled(true);
            RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
            RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
            RequestContext.getCurrentInstance().update("form1:form2:formSendButton");

        } else if (this.getIdentityNumberHolder().equalsIgnoreCase("") && value.isEmpty() && !this.getIdentityNumberHolderSet().equals("")) {
            this.setIdentityNumberHolderSet("");
            this.setVisible(false);
            this.setVisible2(false);
            this.setVisible3(false);
            this.setVisible4(false);
            this.setRequerimentsRendered(false);
            this.setReimbursementHolder("");
            reset();

            this.setDisabled(true);
            RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
            RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
            RequestContext.getCurrentInstance().update("form1:form2:formSendButton");

        }

    }

    public void findPlanPatients() {
        this.setPlansList(this.plansFacadeLocal.findPlansAvailableByPatient(this.getBeneficiarySelected().getId(), TypeSpecialistsEnums.OFTALMOLOGIA.getValor(), 0));
        this.setPlanSelected(null);
        this.setVisible3(false);
        if (!this.getPatientsList().isEmpty()) {
            this.setDisabledPlans(false);
        } else {
            this.setDisabledPlans(true);
        }
        RequestContext.getCurrentInstance().update("form1:form2:insurances");
    }

    /**
     * buttonAction aceppt
     *
     * @param actionEvent
     */
    public void buttonAction(ActionEvent actionEvent) {
        log.log(Level.INFO, "buttonAction()");
        this.setIdentityNumberHolder(this.getIdentityNumberHolderSet());
        this.setPatientsList(this.patientsFacadeLocal.findByIdentityNumberAndRelationShip(Integer.valueOf(this.getIdentityNumberHolder()), Relationship.TITULAR.getValor()));
        if (!this.getPatientsList().isEmpty()) {
            this.setPlansPatientsList(this.plansPatientsFacadeLocal.findByIdentityHolderAndPlan(this.getPatientsList().get(0).getIdentityNumberHolder(), 2));
            if (!this.getPlansPatientsList().isEmpty()) {
                Integer idTypeAtenncion = TypeSpecialistsEnums.OFTALMOLOGIA.getValor();
                List<Patients> beneficiaryAux = new ArrayList<>();
                switch (this.getReimbursementHolder()) {
                    case "si":
                        this.setPlansList(this.plansFacadeLocal.findPlansAvailableByPatient(this.getPatientsList().get(0).getId(), idTypeAtenncion, 1));
                        if (this.getPlansList().isEmpty()) {
                            addMessage("Error", "DISCULPE, UD. HA GENERADO LA TOTALIDAD DE SOLICITUDES PERMITIDAS POR ESTE AÑO O YA POSEE SOLICITUDES DE REEMBOLSO EN PROCESO.");
                            break;
                        } else {
                            this.setIsHolder(false);
                            this.setCompleteNameholder(this.getPatientsList().get(0).getCompleteName());

                            this.setCompleteName(this.getCompleteNameholder());
                            this.setIdentityNumber(this.getIdentityNumberHolder());
                            this.setVisible2(true);
                            this.setDisabledPlans(false);
                            this.setRequired(false);
                            this.setDisabled(true);
                            this.setSeeUploadIdentityHolder(false);
                            this.setRequerimentsRendered(true);

                            RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                            RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                        }
                        break;
                    case "no":
                        beneficiaryAux = this.patientsFacadeLocal.findBeneficiaries(this.getPatientsList().get(0).getIdentityNumberHolder());
                        if (beneficiaryAux.isEmpty()) {
                            addMessage("Error", "Paciente no posee beneficarios");
                            break;
                        } else {
                            this.setPatientsBeneficiary(this.patientsFacadeLocal.findBeneficiariesAvailable(this.getPatientsList().get(0).getId(), idTypeAtenncion));
                        }
                        if (this.getPatientsBeneficiary().isEmpty()) {
                            addMessage("Error", "DISCULPE, UD. HA GENERADO LA TOTALIDAD DE SOLICITUDES PERMITIDAS POR ESTE AÑO O YA POSEE SOLICITUDES DE REEMBOLSO EN PROCESO");
                            break;
                        }
                        else {
                            this.setCompleteNameholder(this.getPatientsList().get(0).getCompleteName());
                            this.setIsHolder(true);
                            this.setVisible(true);
                            this.setVisible2(true);
                            this.setRequired(true);
                            this.setDisabled(true);
                            this.setDisabledPlans(true);
                            this.setSeeUploadIdentityHolder(true);
                            this.setRequerimentsRendered(true);
                            RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                            RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                        }
                        break;
                }
            } else {
                this.setIdentityNumberHolderSet("");
                this.setReimbursementHolder("");
                this.setMaskBanks("");
                this.setDisabled(true);
                RequestContext.getCurrentInstance().update("form1:form2:panelOpcion");
                RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                addMessage("Error", "Paciente no posee planes de oftalmología");
            }
        } else {
            this.setIdentityNumberHolderSet("");
            this.setReimbursementHolder("");
            this.setMaskBanks("");
            this.setDisabled(true);
            RequestContext.getCurrentInstance().update("form1:form2:panelOpcion");
            RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
            addMessage("Error", "La cédula ingresada no se encuentra asignada a un plan de seguro, comuníquese con su aseguradora.");
        }

    }

    /**
     * add Message
     *
     * @param type
     * @param summary
     */
    public void addMessage(String type, String summary) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(type, summary));
        RequestContext.getCurrentInstance().update("form1:form2:growFormReimbursement");
    }

    /**
     * Method to check selected option
     */
    public void onChangeReimbursementHolder() {
        try {
            this.setMaskBanks("");
            switch (this.getReimbursementHolder()) {
                case "si":
                    if (this.getIdentityNumberHolderSet() == null || this.getIdentityNumberHolderSet().equalsIgnoreCase("")) {
                        this.setPlanSelected(new Plans());
                        this.setDisabled(true);
                        this.setVisible2(false);
                        this.setVisible3(false);
                        this.setVisible4(false);
                        this.setRequerimentsRendered(false);
                        this.setVisible(false);
                        this.setReimbursementHolder("");
                        this.setSeeUploadIdentityHolder(false);
                        addMessage("Error", "Debe ingresar una cédula antes de seleccionar");
                        RequestContext.getCurrentInstance().update("form1:form2:msgIdentityNumberHolder");
                        RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
                        RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                        RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                    } else if (this.getIdentityNumberHolderSet() != null || !this.getIdentityNumberHolderSet().equalsIgnoreCase("")) {
                        this.setPlanSelected(new Plans());
                        this.setDisabled(false);
                        this.setVisible2(false);
                        this.setVisible3(false);
                        this.setVisible4(false);
                        this.setVisible(false);
                        this.setRequerimentsRendered(false);
                        this.setSeeUploadIdentityHolder(false);
                        clearFormBySelection("panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                    }

                    break;
                case "no":
                    if (this.getIdentityNumberHolderSet() == null || this.getIdentityNumberHolderSet().equalsIgnoreCase("")) {
                        this.setPlanSelected(new Plans());
                        this.setDisabled(true);
                        this.setVisible2(false);
                        this.setVisible3(false);
                        this.setVisible4(false);
                        this.setVisible(false);
                        this.setSeeUploadIdentityHolder(false);
                        this.setRequerimentsRendered(false);
                        this.setReimbursementHolder("");
                        addMessage("Error", "Debe ingresar una cédula antes de seleccionar");
                        RequestContext.getCurrentInstance().update("form1:form2:msgIdentityNumberHolder");
                        RequestContext.getCurrentInstance().update("form1:form2:reimbursementChoised");
                        RequestContext.getCurrentInstance().update("form1:form2:acceptButton");
                        RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                    } else if (this.getIdentityNumberHolderSet() != null || !this.getIdentityNumberHolderSet().equalsIgnoreCase("")) {
                        this.setPlanSelected(new Plans());
                        this.setDisabled(false);
                        this.setVisible2(false);
                        this.setVisible3(false);
                        this.setVisible4(false);
                        this.setVisible(false);
                        this.setSeeUploadIdentityHolder(false);
                        this.setRequerimentsRendered(false);
                        clearFormBySelection("panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
                        RequestContext.getCurrentInstance().update("form1:form2:formSendButton");
                    }

                    break;
            }

            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
            RequestContext.getCurrentInstance().update("form1:form2:formSendButton");

        } catch (NullPointerException ex) {
            System.out.println("Null pointer!");
        }

    }

    /**
     * on Change lan
     */
    public void onChangePlan() {

        this.setVisible3(true);

        RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
    }

    /**
     * on Change Bank
     */
    public void onChangeBank() {
        if (bank != null) {
            Banks searchBanks = bankFacadeLocal.find(bank.getId());

            this.setMaskBanks(searchBanks.getCode());
            this.setPlaceHolderbanks(searchBanks.getCode());
            this.setAccountNumber("");
            this.setDisabledInputBank(false);
            RequestContext.getCurrentInstance().update("form1:form2:numeroCuenta");
        } else {
            this.setMaskBanks("");
            this.setAccountNumber("");
            this.setDisabledInputBank(true);
            RequestContext.getCurrentInstance().update("form1:form2:numeroCuenta");
        }
    }

    /**
     * Method to check the selected option
     */
    public void onChangeBeneficiary() {
        if (this.beneficiarySelected.getId() != null) {
            Patients searchPatients = patientsFacadeLocal.find(beneficiarySelected.getId());

            this.setVisible4(true);

            this.setCompleteName(searchPatients.getCompleteName());
            this.setIdentityNumber(searchPatients.getIdentityNumber().toString());
            findPlanPatients();

            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
        } else {
            this.setCompleteName("");
            this.setIdentityNumber("");

            this.setVisible4(false);

            RequestContext.getCurrentInstance().update("form1:form2:panelPlanilla");
            RequestContext.getCurrentInstance().update("form1:form2:identityNumber");
            RequestContext.getCurrentInstance().update("form1:form2:nameIdentity");
        }
    }

    /**
     * reset
     */
    public void reset() {
        log.log(Level.INFO, "reset()");
        this.setIdentityNumberHolderSet("");

    }

    /**
     * Method to send an email with change status of reimbursement
     *
     * @throws ParseException
     * @throws IOException
     */
    public void sendMailPatient() throws ParseException, IOException {
        log.log(Level.INFO, "sendMailPatient()");
        if (this.getEmailConfirm() != null) {
            String title = readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_CONTENT_SUBTITLE);
            String titleAnalist = readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_ANALIST_MAIL_TITLE);
            String subjectMessage = readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_TITLE);
            String subjectAnalistMessage = titleAnalist + " - " + this.getCompleteNameholder();
            String contentMessage = buildMessagePatiens();
            String contentMessageAnalist = buildMessageAnalist();
            this.mailSenderLocal.send(Email.MAIL_REEMBOLSO, subjectMessage, title, contentMessage, new ArrayList<File>(), this.getEmailConfirm());
            this.mailSenderLocal.send(Email.MAIL_REEMBOLSO, subjectAnalistMessage, titleAnalist, contentMessageAnalist, new ArrayList<File>(), readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_ANALIST_MAIL));
        } else {
            log.log(Level.INFO, "El paciente no tiene un correo registrado");
        }
        log.log(Level.INFO, "Fin - Proceso de envio de correos a pacientes");

    }

    /**
     * Method to read the properties file constants payment reports
     *
     * @return Properties
     * @throws IOException
     */
    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansReimbursementForm.FILENAME_REIMBURSEMENT_REQUEST);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansReimbursementForm.FILENAME_REIMBURSEMENT_REQUEST
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }

    /**
     * Method to construct the message body for the email that will be sent to
     * patient
     *
     * @param patient
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessagePatiens() throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Estimado Paciente,").append("<br><br>");
        stringBuilder.append(readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_CONTENT_REQUEST));
        stringBuilder.append(this.getNumberRequest());
        stringBuilder.append(".");
        stringBuilder.append(" ");
        stringBuilder.append(readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_CONTENT_REQUEST2));
        stringBuilder.append("<br><br>");
        stringBuilder.append(readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_CONTENT_GREETINGS));
        stringBuilder.append("<br>");
        stringBuilder.append("<b>");
        stringBuilder.append(readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_EMAIL_CONTENT_FIRM));
        stringBuilder.append("</b>");
        return stringBuilder.toString();
    }

    /**
     * build Message Analist
     *
     * @return String
     * @throws ParseException
     * @throws IOException
     */
    private String buildMessageAnalist() throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(readFileProperties().getProperty(ConstansReimbursementForm.PROPERTY_ANALIST_MAIL_CONTENT));
        stringBuilder.append(" ");
        stringBuilder.append(this.getNumberRequest());
        return stringBuilder.toString();
    }

}
