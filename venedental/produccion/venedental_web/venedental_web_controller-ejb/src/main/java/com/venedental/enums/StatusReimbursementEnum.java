/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.enums;

/**
 *
 * @author AKDESK12
 */
public enum StatusReimbursementEnum {
    
    SOLICITADO(1), APROBADO(2), RECHAZADO(3),POR_ABONAR(4), ABONADO(5) ;
    
    Integer value;
    
    private StatusReimbursementEnum(Integer v){
        value=v;
    }

    public Integer getValor() {
        return value;
    }

    public void setValor(Integer valor) {
        this.value = valor;
    }
}
