/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.read;

import com.venedental.carga_masiva.base.Constants;
import com.venedental.carga_masiva.base.CustomLogger;
import com.venedental.carga_masiva.base.Permission;
import com.venedental.carga_masiva.base.Terminal;
import com.venedental.carga_masiva.dto.RowDTO;
import com.venedental.carga_masiva.exceptions.ReadingFileException;
import com.venedental.carga_masiva.exceptions.MessageException;
import com.venedental.carga_masiva.exceptions.NotFileException;
import com.venedental.carga_masiva.exceptions.NotExcelFileException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class FileReader {

    private static final Logger log = CustomLogger.getLogger(FileReader.class.getName());
    private final SheetReader sheetReader;
    private String directory;
    private String user;

    public FileReader(SheetReader sheetReader) {
        this.sheetReader = sheetReader;
    }

    public void execute(String pathDirectory, File fileDirectory, String pathFile, File file, String[] fieldTemplate) throws MessageException {
        validate(file);
        read(pathDirectory, fileDirectory, pathFile, file, fieldTemplate);
    }

    protected void validate(File input) throws MessageException {
        log.log(Level.INFO, "Validando archivo: {0}", input.getName());
        if (input.isDirectory()) {
            throw new NotFileException(input);
        } else {
            int index = input.getName().lastIndexOf(Constants.EXTENSION_SEPARATOR);
            String ext = input.getName().substring(index + 1).toLowerCase();
            if (!ext.equals(Constants.EXTENSION_XLS) && !ext.equals(Constants.EXTENSION_XLSX)) {
                throw new NotExcelFileException(input);
            }
        }
        log.log(Level.INFO, "Archivo valido", input.getName());
    }

    public void read(String pathDirectory, File fileDirectory, String pathFile, File file, String[] fieldTemplate) throws MessageException {
        log.log(Level.INFO, "Inicio - Lectura de archivo: {0}", file.getName());
        try {
            convertExcelToCsv(pathDirectory, pathFile);
            String[] sheetFiles = fileDirectory.list();
            for (String fileName : sheetFiles) {
                if (fileName.endsWith(Constants.SUFIJO_GEN_CSV)) {
                    String pathFileCsv = pathDirectory + Constants.DIRECTORY_SEPARATOR + fileName;
                    File fileCsv = new File(pathFileCsv);
                    try {
                        sheetReader.execute(file.getName(), fileName.replace(Constants.SUFIJO_GEN_CSV, ""), pathFileCsv, fileCsv, fieldTemplate);
                    } catch (MessageException e) {
                        log.log(Level.WARNING, e.getError());
                    }
                }
            }
            move(file);
        } catch (Exception e) {
            String[] sheetFiles = fileDirectory.list();
            for (String fileName : sheetFiles) {
                if (fileName.endsWith(Constants.SUFIJO_GEN_CSV)) {
                    String pathFileCsv = pathDirectory + Constants.DIRECTORY_SEPARATOR + fileName;
                    File fileCsv = new File(pathFileCsv);
                    fileCsv.delete();
                }
            }
            throw new ReadingFileException(file, e);
        }
        log.log(Level.INFO, "Fin - Lectura de archivo");
    }

    private void move(File file) {
        log.log(Level.INFO, "Inicio - Mover archivo a carpeta Procesados");
        String absolutePath = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('/'));

        String processedPath = absolutePath + Constants.DIRECTORY_PROCESSED;
        Permission.permitWrite(processedPath);

        String datePath = processedPath + directory + Constants.DIRECTORY_SEPARATOR;
        File directoryFile = new File(datePath);
        if (!directoryFile.exists()) {
            directoryFile.mkdir();
        }

        String filename = file.getName();
        String newFilename = datePath + filename;
        file.renameTo(new File(newFilename));

        Permission.permitReadOnly(processedPath, user);
        log.log(Level.INFO, "Fin - Mover archivo a carpeta Procesados");
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public void setUser(String user) {
        this.user = user;
    }

    private void convertExcelToCsv(String pathDirectory, String pathFile) {
        log.log(Level.INFO, "Inicio - Convertir Excel a CSV");
        String name = "\"" + pathFile + "\"";
        String instructionExcelToCsv = "ssconvert --export-type=Gnumeric_stf:stf_csv -S " + name + " \"" + pathDirectory + Constants.DIRECTORY_SEPARATOR + "%s" + Constants.SUFIJO_GEN_CSV + "\"";
        Terminal.exec(instructionExcelToCsv);
        log.log(Level.INFO, "Fin - Convertir Excel a CSV");
    }

}
