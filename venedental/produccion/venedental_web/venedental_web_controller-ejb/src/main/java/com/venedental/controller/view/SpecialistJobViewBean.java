/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.controller.view;

import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.facade.SpecialistJobFacadeLocal;
import com.venedental.model.Addresses;
import com.venedental.model.MedicalOffice;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.SpecialistJob;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "specialistJobViewBean")
@ViewScoped
public class SpecialistJobViewBean {
    private List<SpecialistJob> listSpecialistJob;
    private List<SpecialistJob> newListSpecialistJob;
    private List<SpecialistJob> editListSpecialistJob;
    private List<SpecialistJob> beforeListSpecialistJob;
    private SpecialistJob editSpecialistJob;
    private SpecialistJob selectedSpecialistJob;
    private SpecialistJob newSpecialistJob;
    private String[] newSpecialists;
    private Addresses addresses = new Addresses();
    
    @EJB
    private SpecialistJobFacadeLocal specialistJobFacadeLocal;
    
    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    @ManagedProperty(value = "#{clinicsViewBean}")
    private ClinicsViewBean clinicsViewBean;
    
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    
    
    
    public List<SpecialistJob> getBeforeListSpecialistJob() {
        return this.beforeListSpecialistJob;
    }
    
    public void setBeforeListSpecialistJob(List<SpecialistJob> beforeListSpecialistJob) {
        if(this.beforeListSpecialistJob == null){
            this.beforeListSpecialistJob = new ArrayList<>();
        }
        this.beforeListSpecialistJob = beforeListSpecialistJob;
    }
    
    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }
    
    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }
    
    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }
    
    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }
    
    
    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }
    
    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }
    
    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }
    
    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }
    
    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }
    
    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }
    
    public ClinicsViewBean getClinicsViewBean() {
        return clinicsViewBean;
    }
    
    public void setClinicsViewBean(ClinicsViewBean clinicsViewBean) {
        this.clinicsViewBean = clinicsViewBean;
    }
    
    
    public List<SpecialistJob> getEditListSpecialistJob() {
        return editListSpecialistJob;
    }
    
    public void setEditListSpecialistJob(List<SpecialistJob> editListSpecialistJob) {
        if(this.editListSpecialistJob==null){
            this.editListSpecialistJob=new ArrayList<>();
        }
        this.editListSpecialistJob = editListSpecialistJob;
    }
    
    public List<SpecialistJob> getNewListSpecialistJob() {
        return newListSpecialistJob;
    }
    
    public void setNewListSpecialistJob(List<SpecialistJob> newListSpecialistJob) {
        if(this.newListSpecialistJob==null){
            this.newListSpecialistJob=new ArrayList<>();
        }
        this.newListSpecialistJob = newListSpecialistJob;
    }
    public SpecialistJobViewBean() {
        this.editSpecialistJob = new SpecialistJob();
        this.newSpecialistJob = new SpecialistJob();
        this.beforeListSpecialistJob = new ArrayList<>();
        
    }
    
    public List<SpecialistJob> getListSpecialistJob() {
        if (this.listSpecialistJob == null) {
            this.listSpecialistJob = new ArrayList<>();
        }
        return listSpecialistJob;
    }
    
    public void setListSpecialistJob(List<SpecialistJob> listSpecialistJob) {
        this.listSpecialistJob = listSpecialistJob;
    }
    
    public SpecialistJob getEditSpecialistJob() {
        return editSpecialistJob;
    }
    
    public void setEditSpecialistJob(SpecialistJob editSpecialistJob) {
        this.editSpecialistJob = editSpecialistJob;
    }
    
    public SpecialistJob getSelectedSpecialistJob() {
        return selectedSpecialistJob;
    }
    
    public void setSelectedSpecialistJob(SpecialistJob selectedSpecialistJob) {
        this.selectedSpecialistJob = selectedSpecialistJob;
    }
    
    public SpecialistJob getNewSpecialistJob() {
        return newSpecialistJob;
    }
    
    public void setNewSpecialistJob(SpecialistJob newSpecialistJob) {
        this.newSpecialistJob = newSpecialistJob;
    }
    
    public String[] getNewSpecialists() {
        return newSpecialists;
    }
    
    public void setNewSpecialists(String[] newSpecialists) {
        this.newSpecialists = newSpecialists;
    }
    
    public Addresses getAddresses() {
        return addresses;
    }
    
    public void setAddresses(Addresses addresses) {
        this.addresses = addresses;
    }
    
    public SpecialistJobFacadeLocal getSpecialistJobFacadeLocal() {
        return specialistJobFacadeLocal;
    }
    
    public void setSpecialistJobFacadeLocal(SpecialistJobFacadeLocal specialistJobFacadeLocal) {
        this.specialistJobFacadeLocal = specialistJobFacadeLocal;
    }
    
    public void createCliniscToSpecialistEdit(MedicalOffice item, String tipo) {
        if (item == null) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir una Clínica"));
        } else if (tipo.equals("clinicaNew")) {
            this.getEditSpecialistJob().setMedicalOfficeId(item);
            
            this.getEditListSpecialistJob().add(this.getEditSpecialistJob());
            //context.update("form:clinicsList");
        } else if (tipo.equals("consultorioNew")) {
            MedicalOffice medicalOffice = new MedicalOffice();
            medicalOffice.setName(this.getClinicsViewBean().getNewClinics()[1]);
            medicalOffice.setRif(this.getClinicsViewBean().getNewClinics()[2]);
            medicalOffice.setAddressId(this.getAddressesViewBean().getNewClinicAddresses());
            this.getEditSpecialistJob().setPhoneClinicList(this.getPhoneClinicViewBean().getNewListPhoneClinic());
            
            this.getEditSpecialistJob().setMedicalOfficeId(medicalOffice);
            this.getCitiesViewBean().getNewClinicCities().setStateId(this.getStateViewBean().getNewClinicState());
            this.getAddressesViewBean().getNewClinicAddresses().setCityId(this.getCitiesViewBean().getNewClinicCities());
            this.getEditListSpecialistJob().add(this.getEditSpecialistJob());
//            this.getClinicsViewBean().setNewClinics(new String [17]);
//            this.getStateViewBean().setNewClinicState(new State());
//            this.getCitiesViewBean().setNewClinicCities(new Cities());
//            this.getAddressesViewBean().setNewClinicAddresses(new Addresses());
//            this.getPhoneClinicViewBean().setNewListPhoneClinic(new ArrayList<PhoneClinic>());
            //this.getSpecialistJobViewBean().setNewSpecialistJob(new SpecialistJob());
        }
    }

    
    public void onCellEdit(CellEditEvent event) {
        if (!event.getOldValue().equals(event.getNewValue())) {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            FacesContext contexto = FacesContext.getCurrentInstance();
            DataTable table = (DataTable) event.getSource();
            SpecialistJob specialistJob = (SpecialistJob) table.getRowData();
            
            if(specialistJob.getMedicalOfficeId().getAddressId().getAddress() == null){
                System.out.println("es nulo");
                SpecialistJob specialistJobOld = this.specialistJobFacadeLocal.find(specialistJob.getId());
                Addresses addresses = new Addresses();
                addresses.setAddress(specialistJobOld.getMedicalOfficeId().getAddressId().getAddress());
                specialistJobOld.getMedicalOfficeId().getAddressId().getAddress();
                contexto.addMessage(null, new FacesMessage("Error", "Debe ingresar la dirección"));
                //requestContext.update("form:growBaremo");
                requestContext.update("form:directorioEdit");
  }
        }
    }
    
}

