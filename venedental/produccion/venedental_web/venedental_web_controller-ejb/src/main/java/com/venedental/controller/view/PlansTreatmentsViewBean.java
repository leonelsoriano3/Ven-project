/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacade;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.PlansTreatments;
import com.venedental.model.PropertiesTreatments;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "plansTreatmentsViewBean")
@ViewScoped
public class PlansTreatmentsViewBean {

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;
     @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;
    private final RequestContext context = RequestContext.getCurrentInstance();
    private List<PlansTreatments> listPlansTreatments;
    
    private List<PlansTreatments> auxListPlansTreatments;
    private List<PlansTreatments> listPlansTreatmentsFiltrada;
    private List<PlansTreatments> listPlansTreatmentsSeleccionados;
    private List<PlansTreatments> listPlansTreatmentsValidate;
    private List<PlansTreatments> newListPlansTreatments;
    private List<PlansTreatments> listPlansTreatmentsType;
    private List<PlansTreatments> editListPlansTreatments;
    private List<PlansTreatments> beforeListPlansTreatments;
    private PlansTreatments editPlansTreatments;
    private PlansTreatments selectedPlansTreatments;
    private PlansTreatments newPlansTreatmentsObj;
    private String[] newPlansTreatments;
    private boolean visibleList;
    private String nameTreatments;
    private Boolean btnPlanTreatments;
    
   

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
//    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
//    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    @ManagedProperty(value = "#{plansViewBean}")
    private PlansViewBean plansViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    
    

    public List<PlansTreatments> getListPlansTreatmentsValidate() {
        if (this.listPlansTreatmentsValidate == null) {
            this.listPlansTreatmentsValidate = new ArrayList<>();
        }
        return listPlansTreatmentsValidate;
    }

    public void setListPlansTreatmentsValidate(List<PlansTreatments> listPlansTreatmentsValidate) {
        this.listPlansTreatmentsValidate = listPlansTreatmentsValidate;
    }

    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }

    public List<PlansTreatments> getBeforeListPlansTreatments() {
        return this.beforeListPlansTreatments;
    }

    public void setBeforeListPlansTreatments(List<PlansTreatments> beforeListPlansTreatments) {
        if (this.beforeListPlansTreatments == null) {
            this.beforeListPlansTreatments = new ArrayList<>();
        }
        this.beforeListPlansTreatments = beforeListPlansTreatments;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }

    public List<PlansTreatments> getEditListPlansTreatments() {
        return editListPlansTreatments;
    }

    public void setEditListPlansTreatments(List<PlansTreatments> editListPlansTreatments) {
        if (this.editListPlansTreatments == null) {
            this.editListPlansTreatments = new ArrayList<>();
        }
        this.editListPlansTreatments = editListPlansTreatments;
    }

    public List<PlansTreatments> getNewListPlansTreatments() {
        if (this.newListPlansTreatments == null) {
            this.newListPlansTreatments = new ArrayList<>();
        }
        return newListPlansTreatments;
    }

    public void setNewListPlansTreatments(List<PlansTreatments> newListPlansTreatments) {
        
        this.newListPlansTreatments = newListPlansTreatments;
    }

    public List<PlansTreatments> getAuxListPlansTreatments() {

        if (this.auxListPlansTreatments == null) {
            this.auxListPlansTreatments = new ArrayList<>();
        }
        return this.auxListPlansTreatments;

    }

    public void setAuxListPlansTreatments(List<PlansTreatments> auxListPlansTreatments) {
        this.auxListPlansTreatments = auxListPlansTreatments;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansTreatmentsViewBean() {
        this.editPlansTreatments = new PlansTreatments();
        //this.selectedPlansTreatments = new PlansTreatments();
        this.newPlansTreatments = new String[4];
        this.beforeListPlansTreatments = new ArrayList<>();
        this.visibleList = false;
        this.btnPlanTreatments=true;

    }

    public Boolean getBtnPlanTreatments() {
        return btnPlanTreatments;
    }

    public void setBtnPlanTreatments(Boolean btnPlanTreatments) {
        this.btnPlanTreatments = btnPlanTreatments;
    }
    

    public List<PlansTreatments> getListPlansTreatmentsFiltrada() {
        if (this.listPlansTreatmentsFiltrada == null) {
            this.listPlansTreatmentsFiltrada = new ArrayList<>();
        }
        return listPlansTreatmentsFiltrada;
    }

    public void setListPlansTreatmentsFiltrada(List<PlansTreatments> listPlansTreatmentsFiltrada) {
        this.listPlansTreatmentsFiltrada = listPlansTreatmentsFiltrada;
    }

    public List<PlansTreatments> getListPlansTreatments() {
        if (this.listPlansTreatments == null) {
            this.listPlansTreatments = new ArrayList<>();       
            this.listPlansTreatments = this.plansTreatmentsFacadeLocal.findAll();            
            
        }
        
       
        Collections.sort(this.listPlansTreatments, new Comparator<PlansTreatments>() {

            @Override
            public int compare(PlansTreatments o1, PlansTreatments o2) {
                return o1.getTreatmentsId().getName().compareTo(o2.getTreatmentsId().getName());
            }

        });

        return listPlansTreatments;
    }
    
    public void setListPlansTreatments(List<PlansTreatments> listPlansTreatments) {
        this.listPlansTreatments = listPlansTreatments;
    }
    

    public PlansTreatments getEditPlansTreatments() {
        return editPlansTreatments;
    }

    public void setEditPlansTreatments(PlansTreatments editPlansTreatments) {
        this.editPlansTreatments = editPlansTreatments;
    }

    public PlansTreatments getSelectedPlansTreatments() {
        return selectedPlansTreatments;
    }

    public void setSelectedPlansTreatments(PlansTreatments selectedPlansTreatments) {

        this.selectedPlansTreatments = selectedPlansTreatments;
    }

    public String[] getNewPlansTreatments() {
        return newPlansTreatments;
    }

    public void setNewPlansTreatments(String[] newPlansTreatments) {
        this.newPlansTreatments = newPlansTreatments;
    }

    public PlansTreatments getNewPlansTreatmentsObj() {
        return newPlansTreatmentsObj;
    }

    public void setNewPlansTreatmentsObj(PlansTreatments newPlansTreatmentsObj) {
        this.newPlansTreatmentsObj = newPlansTreatmentsObj;
    }

    public PlansViewBean getPlansViewBean() {
        return plansViewBean;
    }

    public void setPlansViewBean(PlansViewBean plansViewBean) {
        this.plansViewBean = plansViewBean;
    }

//    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
//        return propertiesTreatmentsViewBean;
//    }
//
//    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
//        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
//    }
    public List<PlansTreatments> getListPlansTreatmentsSeleccionados() {
        if (this.listPlansTreatmentsSeleccionados == null) {
            this.listPlansTreatmentsSeleccionados = new ArrayList<>();
        }
        return listPlansTreatmentsSeleccionados;
    }

    public void setListPlansTreatmentsSeleccionados(List<PlansTreatments> listPlansTreatmentsSeleccionados) {
        this.listPlansTreatmentsSeleccionados = listPlansTreatmentsSeleccionados;
    }

    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public PropertiesTreatmentsFacadeLocal getPropertiesTreatmentsFacadeLocal() {
        return propertiesTreatmentsFacadeLocal;
    }

    public void setPropertiesTreatmentsFacadeLocal(PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal) {
        this.propertiesTreatmentsFacadeLocal = propertiesTreatmentsFacadeLocal;
    }

    public PlansTreatmentsFacadeLocal getPlansTreatmentsFacadeLocal() {
        return plansTreatmentsFacadeLocal;
    }

    public void setPlansTreatmentsFacadeLocal(PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal) {
        this.plansTreatmentsFacadeLocal = plansTreatmentsFacadeLocal;
    }

    public List<PlansTreatments> getListPlansTreatmentsType() {
        
         if (this.listPlansTreatmentsType == null) {
            this.listPlansTreatmentsType = new ArrayList<>();
        }
        
        return listPlansTreatmentsType;
    }

    public void setListPlansTreatmentsType(List<PlansTreatments> listPlansTreatmentsType) {
        this.listPlansTreatmentsType = listPlansTreatmentsType;
    }
    
    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }

//    public void addTreatments(String id, PlansTreatments treatmentsSelected) {
//
//        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatments().addAll(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(treatmentsSelected.getTreatmentsId().getId()));
//        if (!this.getPropertiesTreatmentsViewBean().getListPropertiesTreatments().isEmpty()) {
//            //
//            this.desplegarDialogoCreate(id, true);
//            this.visibleList = true;
//        }
//    }
//    
//    public void addProperties(PropertiesTreatments properties){
//    
//      this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().add(properties);
//      this.getPropertiesTreatmentsViewBean().setColor("#79B2F8");
//
//    
//    }
//    
//    public void filterPropertiesList(PlansTreatments selectedPlansTreatments) {
//        
//        this.getSelectedPlansTreatments();
//
//        this.getPropertiesTreatmentsViewBean().setListPropertiesTreatments(selectedPlansTreatments.getTreatmentsId().getPropertiesTreatmentsList());
//    }
}
