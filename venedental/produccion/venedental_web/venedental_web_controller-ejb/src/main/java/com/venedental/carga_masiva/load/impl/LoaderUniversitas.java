/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.load.impl;

import com.venedental.carga_masiva.exceptions.MessageException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class LoaderUniversitas extends LoaderBasic {

    @Override
    public List<Integer> readPlans(String filename, String sheetName, String[] row, String[] field, int index) throws MessageException {
        List<Integer> ids = new ArrayList<>();
        if (filename.toUpperCase().contains("ODONT")) {
            ids.add(1);
        } else if (filename.toUpperCase().contains("OFT")) {
            String name = sheetName.replace(" ", "").replace("(", "").replace(")", "").toUpperCase();
            if (name.contains("300")) {
                ids.add(2);
            } else if (name.contains("700")) {
                ids.add(3);
            } else if (name.contains("1000")) {
                ids.add(4);
            } else if (name.contains("1500")) {
                ids.add(5);
            } else if (name.contains("2000")) {
                ids.add(6);
            } else if (name.contains("4000")) {
                ids.add(7);
            } else if (name.contains("5000")) {
                ids.add(8);
            }
        }
        return ids;
    }

}
