/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.SpecialityViewBean;
import com.venedental.model.Speciality;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Usuario
 */
@FacesConverter(value = "specialityConverter", forClass = Speciality.class)
public class SpecialityConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {

        SpecialityViewBean specialityViewBean = (SpecialityViewBean) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "specialityViewBean");

        if (value.trim().equals("")) {
            return null;
        } else {
            try {
                int numero = Integer.parseInt(value);
                for (Speciality s : specialityViewBean.getListaSpeciality()) {
                    if (s.getId() == numero) {
                        return s;
                    }
                }
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Especialidad invalido"));
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        SpecialityViewBean specialityViewBean = (SpecialityViewBean) context.getApplication().getELResolver().getValue(context.getELContext(), null, "specialityViewBean");

        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Speciality) value).getId());
        }

    }

}
