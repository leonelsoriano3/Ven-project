
package com.venedental.constans;

public class ConstansReportRetention {

    // PATHS FILES RESOURCES
    public static String PROPERTIES_PATH = "src/main/resources/";
    
    // ENVIRONMENT VARIABLE REIMBURSEMENT
    public static String FILENAME_RETENTION_REPORT = "retentionReport.properties";
    
   
    // --> NAME TYPE PERSONS NATURAL
    public static String PROPERTY_TYPE_PERSON_NATURAL = "PROFESSIONAL_FESS_INDIVIDUAL";
    
    // --> NAME TYPE PERSONS JURIDICAL
    public static String PROPERTY_TYPE_PERSON_JURIDICAL = "PROFESSIONAL_FESS_INDIVIDUAL_JURIDICAL";


}

