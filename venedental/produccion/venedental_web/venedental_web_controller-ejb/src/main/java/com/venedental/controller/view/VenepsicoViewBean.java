/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.ListaLinks;

/**
 *
 * @author akdesarrollo
 */
@ManagedBean(name = "venepsicoViewBean")
@ViewScoped
public class VenepsicoViewBean extends BaseBean {

    private static final long serialVersionUID = -5547104134772667835L;

    private List<ListaLinks> listaLinks;
    private List<ListaLinks> listaCaracteristicas;

    public VenepsicoViewBean() {
    }

    @PostConstruct
    public void init() {
        this.setListaLinks(new ArrayList<ListaLinks>());
        this.listaLinks.add(new ListaLinks("Servicios", "venepsico.xhtml", "/recursos/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Coberturas", "planVenepsico.xhtml", "/recursos/img/list-bullets2.png"));
        this.listaLinks.add(new ListaLinks("Centro de Atención", "directorio.xhtml", "/recursos/img/list-bullets2.png"));

        this.setListaCaracteristicas(new ArrayList<ListaLinks>());
        this.listaCaracteristicas.add(new ListaLinks("Hasta 5 consultas presenciales*", "", "/recursos/img/list-bullets2.png"));
        this.listaCaracteristicas.add(new ListaLinks("Consulta de evaluación para lograr un plan de acción personal: historia clínica, evaluación integral, detección de fuentes de angustias, establecimiento de objetivos de mejora", "", "/recursos/img/list-bullets2.png"));
        this.listaCaracteristicas.add(new ListaLinks("Hasta 5 sesiones posteriores (presenciales) siempre y cuando sean medicadas por el profesional", "", "/recursos/img/list-bullets2.png"));
        this.listaCaracteristicas.add(new ListaLinks("Duración máxima de cada consulta 45 minutos", "", "/recursos/img/list-bullets2.png"));

    }

    public List<ListaLinks> getListaLinks() {
        return listaLinks;
    }

    public void setListaLinks(List<ListaLinks> listaLinks) {
        this.listaLinks = listaLinks;
    }

    public List<ListaLinks> getListaCaracteristicas() {
        return listaCaracteristicas;
    }

    public void setListaCaracteristicas(List<ListaLinks> listaCaracteristicas) {
        this.listaCaracteristicas = listaCaracteristicas;
    }

}
