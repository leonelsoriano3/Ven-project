/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.dto;

import com.venedental.facade.InsurancesFacadeLocal;
import com.venedental.facade.InsurancesPatientsFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.RelationshipFacadeLocal;
import com.venedental.facade.store_procedure.CleanStatusFacadeLocal;
import com.venedental.mail.ejb.MailSenderLocal;
import java.io.Serializable;

/**
 *
 * @author usuario
 */
public class ServiceFacadeDTO implements Serializable {

    private CleanStatusFacadeLocal cleanStatusFacadeLocal;
    private InsurancesFacadeLocal insuranceFacadeLocal;
    private InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal;
    private PatientsFacadeLocal patientsFacadeLocal;
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;
    private PlansFacadeLocal plansFacadeLocal;
    private RelationshipFacadeLocal relationshipFacadeLocal;
    private MailSenderLocal mailSenderLocal;

    public CleanStatusFacadeLocal getCleanStatusFacadeLocal() {
        return cleanStatusFacadeLocal;
    }

    public void setCleanStatusFacadeLocal(CleanStatusFacadeLocal cleanStatusFacadeLocal) {
        this.cleanStatusFacadeLocal = cleanStatusFacadeLocal;
    }

    public InsurancesFacadeLocal getInsuranceFacadeLocal() {
        return insuranceFacadeLocal;
    }

    public void setInsuranceFacadeLocal(InsurancesFacadeLocal insuranceFacadeLocal) {
        this.insuranceFacadeLocal = insuranceFacadeLocal;
    }

    public InsurancesPatientsFacadeLocal getInsurancesPatientsFacadeLocal() {
        return insurancesPatientsFacadeLocal;
    }

    public void setInsurancesPatientsFacadeLocal(InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal) {
        this.insurancesPatientsFacadeLocal = insurancesPatientsFacadeLocal;
    }

    public PatientsFacadeLocal getPatientsFacadeLocal() {
        return patientsFacadeLocal;
    }

    public void setPatientsFacadeLocal(PatientsFacadeLocal patientsFacadeLocal) {
        this.patientsFacadeLocal = patientsFacadeLocal;
    }

    public PlansFacadeLocal getPlansFacadeLocal() {
        return plansFacadeLocal;
    }

    public void setPlansFacadeLocal(PlansFacadeLocal plansFacadeLocal) {
        this.plansFacadeLocal = plansFacadeLocal;
    }

    public MailSenderLocal getMailSenderLocal() {
        return mailSenderLocal;
    }

    public void setMailSenderLocal(MailSenderLocal mailSenderLocal) {
        this.mailSenderLocal = mailSenderLocal;
    }

    public RelationshipFacadeLocal getRelationshipFacadeLocal() {
        return relationshipFacadeLocal;
    }

    public void setRelationshipFacadeLocal(RelationshipFacadeLocal relationshipFacadeLocal) {
        this.relationshipFacadeLocal = relationshipFacadeLocal;
    }

    public PlansPatientsFacadeLocal getPlansPatientsFacadeLocal() {
        return plansPatientsFacadeLocal;
    }

    public void setPlansPatientsFacadeLocal(PlansPatientsFacadeLocal plansPatientsFacadeLocal) {
        this.plansPatientsFacadeLocal = plansPatientsFacadeLocal;
    }

}
