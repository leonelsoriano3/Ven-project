/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.helper;

import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.massiveLoading.base.Constants;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.base.Permission;
import com.venedental.dto.massiveLoading.RowLoad;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class WriterHelper {

    private static final Logger log = CustomLogger.getLogger(WriterHelper.class.getName());

    private final String user;
    private final String processedPath;
    private final String datePath;

    private final Map<String, Object> mapWrite;
    public final StringBuilder sbCorrect;
    public final StringBuilder sbIncorrect;
    public  Boolean bCorrect;
    public  Boolean bIncorrect;

    public WriterHelper(String user, String processedPath, String dateDirectory) {
        this.user = user;
        this.processedPath = processedPath;
        this.datePath = processedPath + dateDirectory;
        mapWrite = new TreeMap<>();
        mapWrite.put("totalRead", 0);
        mapWrite.put("totalCorrect", 0);
        mapWrite.put("totalIncorrect", 0);
        mapWrite.put("fileList", new ArrayList<String>());
        mapWrite.put("fileSheetMap", new TreeMap<String, Integer>());
        mapWrite.put("exceptionMap", new TreeMap<String, Integer>());
        sbCorrect = new StringBuilder();
        sbIncorrect = new StringBuilder();
        bCorrect = false;
        bIncorrect = false;
        
    }

    private File writeCorrectRow() {

        log.log(Level.INFO, "Inicio - Generar archivo de registros válidos");
        File file = writeList(sbCorrect.toString(), "RegistrosValidos.txt", user);
        log.log(Level.INFO, "Fin - Generar archivo de registros válidos");
        return file;

    }
    
    private File writeIncorrectRow() {

        log.log(Level.INFO, "Inicio - Generar archivo de registros inválidos");
        File file = writeList(sbIncorrect.toString(), "RegistrosInvalidos.txt", user);
        log.log(Level.INFO, "Fin - Generar archivo de registros inválidos");
        return file;

    }

    private File writeResume(InsuranceLoad insuranceLoading,String modeLoad) {
        log.log(Level.INFO, "Inicio - Generar archivo de resumen");
        String content = getContentResume(modeLoad);
        // INIT
        Permission.permitWrite(processedPath);
        File directoryFile = new File(datePath);
        if (!directoryFile.exists()) {
            directoryFile.mkdir();
        }
        File file = new File(datePath + "ResumenAseguradora-"+insuranceLoading.getInsurance().getName()+".txt");
        BufferedWriter bOut = bufferedOutput(file);
        // WRITE
        try {
            bOut.write(content);
        } catch (Exception e) {
            log.log(Level.WARNING, "Error en la escritura");
        }
        //CLOSE
        closeOutput(bOut);
        Permission.permitReadOnly(processedPath, user);
        // END
        log.log(Level.INFO, "Fin - Generar archivo de resumen");
        return file;
    }

    private File writeList(String text, String filename, String user) {
        // INIT
        Permission.permitWrite(processedPath);

        File directoryFile = new File(datePath);
        if (!directoryFile.exists()) {
            directoryFile.mkdir();
        }

        File file = new File(datePath + filename);
        BufferedWriter bOut = bufferedOutput(file);
        // WRITE
        try {
            bOut.write(text);
        } catch (Exception e) {
            log.log(Level.WARNING, "Error en la escritura");
        }
        //CLOSE
        closeOutput(bOut);
        Permission.permitReadOnly(processedPath, user);
        // END
        return file;
    }

    private BufferedWriter bufferedOutput(File file) {
        try {
            FileWriter fileOutput = new FileWriter(file);
            BufferedWriter bufferedOutput = new BufferedWriter(fileOutput);
            return bufferedOutput;
        } catch (IOException e) {
            log.log(Level.WARNING, "Error al abrir el bufferedWriter");
            return null;
        }
    }

    private void closeOutput(BufferedWriter bufferedOutput) {
        try {
            bufferedOutput.close();
        } catch (IOException e) {
            log.log(Level.WARNING, "Error al cerrar el bufferedWriter");
        }
    }

    private String getContentResume(String modeLoad) {
        StringBuilder builder = new StringBuilder();

        Integer totalRead = (Integer) mapWrite.get("totalRead");
        Integer totalCorrect = (Integer) mapWrite.get("totalCorrect");
        Integer totalIncorrect = (Integer) mapWrite.get("totalIncorrect");
        String headerTxt="---- RESUMEN CARGA MASIVA ("+modeLoad+") ----";
        builder.append(headerTxt).append("\n");
        builder.append(Constants.REG_READ).append(totalRead).append("\n");
        builder.append(Constants.REG_VALID).append(totalCorrect).append("\n");
        builder.append(Constants.REG_INVALID).append(totalIncorrect).append("\n");
        List<String> fileList = (List<String>) mapWrite.get("fileList");
        Map<String, Integer> mapFileSheet = (Map<String, Integer>) mapWrite.get("fileSheetMap");
        Map<String, Integer> mapExceptions = (Map<String, Integer>) mapWrite.get("exceptionMap");
        if (fileList.isEmpty()) {
            builder.append("- Actualmente no existen archivos en el directorio").append("\n");
        } else {
            builder.append("- ARCHIVOS").append("\n");
            for (String file : fileList) {
                builder.append("   + Archivo: ").append(file).append("\n");
                builder.append(process(mapFileSheet, mapExceptions, file, 6));
                builder.append("      - HOJAS").append("\n");
                for (String k : mapFileSheet.keySet()) {
                    if (k.startsWith(file + Constants.SEPARATOR_SHEET) && !k.contains(Constants.SEPARATOR_RESULT)) {
                        builder.append("         + Hoja: ").append(k.replace(file, "").replace(Constants.SEPARATOR_SHEET, "")).append("\n");
                        builder.append(process(mapFileSheet, mapExceptions, k, 12));
                    }
                }
            }
        }
        return builder.toString();
    }

    private String process(Map<String, Integer> map, Map<String, Integer> mapExceptions, String start, int numSpaces) {
        StringBuilder builder = new StringBuilder();
        StringBuilder builderSpaces = new StringBuilder();
        for (int i = 0; i < numSpaces; i++) {
            builderSpaces.append(" ");
        }
        String keyValid = start + Constants.SEPARATOR_RESULT + Constants.REG_VALID;
        String keyInvalid = start + Constants.SEPARATOR_RESULT + Constants.REG_INVALID;
        Integer vAll = map.get(start);
        if (vAll == null) {
            vAll = 0;
        }
        Integer vValid = map.get(keyValid);
        if (vValid == null) {
            vValid = 0;
        }
        Integer vInvalid = map.get(keyInvalid);
        if (vInvalid == null) {
            vInvalid = 0;
        }
        builder.append(builderSpaces.toString()).append(Constants.REG_READ).append(vAll).append("\n");
        builder.append(builderSpaces.toString()).append(Constants.REG_VALID).append(vValid).append("\n");
        builder.append(builderSpaces.toString()).append(Constants.REG_INVALID).append(vInvalid).append("\n");
        if (map.get(keyInvalid) != null) {
            builder.append(builderSpaces.toString()).append("  - ERRORES").append("\n");
            for (String k : mapExceptions.keySet()) {
                if (k.startsWith(start + Constants.SEPARATOR_EXCEPTION)) {
                    String exception = k.replace(start + Constants.SEPARATOR_EXCEPTION, "");
                    builder.append(builderSpaces.toString()).append("    - ").append(exception).append(": ").append(mapExceptions.get(k)).append("\n");
                }
            }
        }
        return builder.toString();
    }

    public void registerRow(RowLoad row, String printRow) {
        if (row.hasError()) {
            registerIncorrectRow(row, printRow);
        } else {
            registerCorrectRow(row, printRow);
        }
    }

    private void registerCorrectRow(RowLoad row, String printRow) {
        log.log(Level.INFO, "Inicio - Creacion de archivo de filas correctas");
        this.bCorrect=true;
        sbCorrect.append(printRow).append("\n");
        mapWrite.put("totalRead", ((Integer) mapWrite.get("totalRead")) + 1);
        mapWrite.put("totalCorrect", ((Integer) mapWrite.get("totalCorrect")) + 1);
        List<String> fileList = (List<String>) mapWrite.get("fileList");
        Map<String, Integer> fileSheetMap = (Map<String, Integer>) mapWrite.get("fileSheetMap");
        String fileSheet = row.getFileSheet();
        fileSheet = fileSheet.replace("\t", Constants.SEPARATOR_SHEET);
        String file = fileSheet.substring(0, fileSheet.indexOf(Constants.SEPARATOR_SHEET));
        if (!fileList.contains(file)) {
            fileList.add(file);
        }
        keyMap(fileSheetMap, file, 1);
        keyMap(fileSheetMap, fileSheet, 1);
        keyMap(fileSheetMap, file + Constants.SEPARATOR_RESULT + Constants.REG_VALID, 1);
        keyMap(fileSheetMap, fileSheet + Constants.SEPARATOR_RESULT + Constants.REG_VALID, 1);
        log.log(Level.INFO, "Fin - Creacion de archivo de filas correctas");
    }

    private void registerIncorrectRow(RowLoad row, String printRow) {
        log.log(Level.INFO, "Inicio - Escribir fila incorrecta");
        this.bIncorrect=true;
        sbIncorrect.append(printRow).append("\n");
        mapWrite.put("totalRead", ((Integer) mapWrite.get("totalRead")) + 1);
        mapWrite.put("totalIncorrect", ((Integer) mapWrite.get("totalIncorrect")) + 1);
        List<String> fileList = (List<String>) mapWrite.get("fileList");
        Map<String, Integer> fileSheetMap = (Map<String, Integer>) mapWrite.get("fileSheetMap");
        Map<String, Integer> exceptionMap = (Map<String, Integer>) mapWrite.get("exceptionMap");
        String fileSheet = row.getFileSheet() + "";
        fileSheet = fileSheet.replace("\t", Constants.SEPARATOR_SHEET);
        String file = fileSheet.substring(0, fileSheet.indexOf(Constants.SEPARATOR_SHEET));

        if (!fileList.contains(file)) {
            fileList.add(file);
        }
        keyMap(fileSheetMap, file, 1);
        keyMap(fileSheetMap, fileSheet, 1);
        keyMap(fileSheetMap, file + Constants.SEPARATOR_RESULT + Constants.REG_INVALID, 1);
        keyMap(fileSheetMap, fileSheet + Constants.SEPARATOR_RESULT + Constants.REG_INVALID, 1);
        String[] tokensExceptions = row.getMessageError().split(" / ");
        for (String token : tokensExceptions) {
            if (token != null && !token.isEmpty()) {
                keyMap(exceptionMap, file + Constants.SEPARATOR_EXCEPTION + token, 1);
                keyMap(exceptionMap, fileSheet + Constants.SEPARATOR_EXCEPTION + token, 1);
            }
        }
        log.log(Level.INFO, "Fin - Escribir fila incorrecta");
    }

    public void registerCommonLine(String line) {
        registerCorrectLine(line);
        registerIncorrectLine(line);
    }

    public void registerCorrectLine(String line) {
        sbCorrect.append(line).append("\n");
    }

    public void registerIncorrectLine(String line) {
        sbIncorrect.append(line).append("\n");
    }

    private void keyMap(Map<String, Integer> map, String key, int increment) {
        if (map.get(key) == null) {
            map.put(key, 0);
        }
        map.put(key, map.get(key) + increment);
    }

    public List<File> generateFiles(InsuranceLoad insuranceLoading,String modeLoad) {
        List<File> list = new ArrayList<>();
        File fileCorrect = this.writeCorrectRow();
        if (fileCorrect != null) {
            list.add(fileCorrect);
        }
        File fileIncorrect = this.writeIncorrectRow();
        if (fileIncorrect != null) {
            list.add(fileIncorrect);
        }
        
        File fileResume = this.writeResume(insuranceLoading,modeLoad);
        if (fileResume != null) {
            list.add(fileResume);
        }
        return list;
    }

    public Boolean getbCorrect() {
        return bCorrect;
    }

    public void setbCorrect(Boolean bCorrect) {
        this.bCorrect = bCorrect;
    }

    public Boolean getbIncorrect() {
        return bIncorrect;
    }

    public void setbIncorrect(Boolean bIncorrect) {
        this.bIncorrect = bIncorrect;
    }
    
    
    

}
