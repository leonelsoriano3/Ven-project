/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.model.KeysPatients;
import com.venedental.model.Specialists;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServiceSpecialists;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 * Where class all about the first stage of analysis of key performs
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "receiveKeyAnalysisViewBean")
@ViewScoped
public class ReceiveKeyAnalysisViewBean extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();
    List<KeysPatients> keysPatients;
    List<KeysPatients> keysPatientsFiltered;
    List<KeysPatients> selectedKeysPatients;
    private Specialists specialist;
    private Specialists selectedSpecialist;
    private Date dateFilter;
    private boolean renderedColumn = false;
    private boolean diabledBtnRecive = true;
    private boolean inputTables = true;
    private Date maxDate;
    private static final Logger log = CustomLogger.getGeneralLogger(ReceiveKeyAnalysisViewBean.class.getName());

    @EJB
    private ServiceKeysPatients serviceKeysPatients;
    @EJB
    private ServiceSpecialists serviceSpecialists;
    
     @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    public Date getMaxDate() throws ParseException {
        log.log(Level.INFO, "getMaxDate()()");
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public boolean isInputTables() {
        return inputTables;
    }

    public void setInputTables(boolean inputTables) {
        this.inputTables = inputTables;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public boolean isDiabledBtnRecive() {
        return diabledBtnRecive;
    }

    public void setDiabledBtnRecive(boolean diabledBtnRecive) {
        this.diabledBtnRecive = diabledBtnRecive;
    }

    public boolean isRenderedColumn() {
        return renderedColumn;
    }

    public void setRenderedColumn(boolean renderedColumn) {
        this.renderedColumn = renderedColumn;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public List<KeysPatients> getKeysPatientsFiltered() {
        return keysPatientsFiltered;
    }

    public void setKeysPatientsFiltered(List<KeysPatients> keysPatientsFiltered) {
        this.keysPatientsFiltered = keysPatientsFiltered;
    }

    public List<KeysPatients> getKeysPatients() {
        if (this.keysPatients == null) {
            this.keysPatients = new ArrayList<>();
        }
        return keysPatients;
    }

    public List<KeysPatients> getSelectedKeysPatients() {
        return selectedKeysPatients;
    }

    public void setSelectedKeysPatients(List<KeysPatients> selectedKeysPatients) {
        this.selectedKeysPatients = selectedKeysPatients;
    }

    public void setKeysPatients(List<KeysPatients> keysPatients) {
        this.keysPatients = keysPatients;
    }

    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        log.log(Level.INFO, "filterSpecialist()");
        List<Specialists> filteredSpecialists = this.serviceSpecialists.findByIdentityOrName(query);
        return filteredSpecialists;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }
    
    

    /**
     * Method called when I select a specialist from the list
     *
     * @param event
     * @throws IOException
     */
    public void onItemSelect(SelectEvent event) throws IOException {
        log.log(Level.INFO, "onItemSelect()");
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        filterKeys();
    }

    /**
     * Method that allows you to find the keys generated or state analysis and also treatments that have generated or
     * executed , is executed when selected a specialist *
     */
    public void filterKeys() {
        log.log(Level.INFO, "filterKeys()");
        resetDataTableKeys();
        Integer status1 = EstatusKeysPatients.GENERADO.getValor();
        Integer status2 = EstatusKeysPatients.RECIBIDO.getValor();
        Integer status3 = EstatusKeysPatients.POR_FACTURAR.getValor();
        Integer idUser=this.getSecurityViewBean().obtainUser().getId();
        this.setSelectedKeysPatients(new ArrayList<KeysPatients>());
        this.setKeysPatients(new ArrayList<KeysPatients>());
        this.setKeysPatients(this.serviceKeysPatients.findKeysBySpecialistAndStatus(this.getSelectedSpecialist().getId(),
                status1, status2, status3,idUser));
        if (!this.getKeysPatients().isEmpty()) {
            this.setInputTables(false);
            this.setRenderedColumn(true);
        } else {
            this.setInputTables(true);
            this.setRenderedColumn(false);
        }

        this.setDiabledBtnRecive(true);
        RequestContext.getCurrentInstance().update("form:receiveAnalysisKeysList");
        RequestContext.getCurrentInstance().update("form:btnReciveKeys");
    }

    public void resetDataTableKeys() {
        log.log(Level.INFO, "resetDataTableKeys()");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:receiveAnalysisKeysList");
        if (dataTable != null) {
            dataTable.reset();
            this.setSelectedKeysPatients(new ArrayList<KeysPatients>());
            this.setDateFilter(null);
            RequestContext.getCurrentInstance().reset("form:receiveAnalysisKeysList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:receiveAnalysisKeysList");
        }
    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelect(SelectEvent event) {
        log.log(Level.INFO, "handleDateSelect()");
        if (!this.getKeysPatientsFiltered().isEmpty()) {


            RequestContext.getCurrentInstance().execute("PF('receiveAnalysisTable').filter()");
            RequestContext.getCurrentInstance().execute("PF('receiveAnalysisTable').filter()");
        }
    }

    /**
     * DateTrasnformar method that allows a date to the form dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public Date formatDate(Date date) throws ParseException {
        log.log(Level.INFO, "formatDate()");
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;

    }

    public void selectRow() {
        log.log(Level.INFO, "selectRow()");
        this.setDiabledBtnRecive(false);
        RequestContext.getCurrentInstance().update("form:btnReciveKeys");
    }

    public void unSelectRow() {
        log.log(Level.INFO, "unSelectRow()");
        if (this.getSelectedKeysPatients().isEmpty()) {
            this.setDiabledBtnRecive(true);
            RequestContext.getCurrentInstance().update("form:btnReciveKeys");
        }
    }

    public void toggleSelect() {
        log.log(Level.INFO, "toggleSelect()");
        if (this.getSelectedKeysPatients().isEmpty()) {
            this.setDiabledBtnRecive(true);
        } else {
            this.setDiabledBtnRecive(false);
        }
        RequestContext.getCurrentInstance().update("form:btnReciveKeys");
    }

    public void clearSelection() {
        log.log(Level.INFO, "clearSelection()");
        this.setSelectedKeysPatients(new ArrayList<KeysPatients>());
        this.setDiabledBtnRecive(true);
        RequestContext.getCurrentInstance().update("form:receiveAnalysisKeysList");
        RequestContext.getCurrentInstance().update("form:btnReciveKeys");
    }

}
