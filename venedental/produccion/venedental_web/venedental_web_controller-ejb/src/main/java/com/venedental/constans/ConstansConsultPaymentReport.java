package com.venedental.constans;

/**
 * Created by luis.carrasco@arkiteck.biz on 15/11/16.
 */
public class ConstansConsultPaymentReport {

    public static String FILENAME_PAYMENT_REPORT = "paymentReport.properties";
    public static String PROPERTY_PATH_REPORT = "venedental_web-web-1.1.1_war/reports/specialist/";
    public static String PROPERTY_PATH_JASPER = "venedental_web-web-1.1.1_war/reports/specialist/paymentReport.jasper";
    public static String PROPERTY_PATH_LOGO = "venedental_web-web-1.1.1_war/recursos/img/heal_logo2.png";

}
