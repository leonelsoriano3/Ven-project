package com.venedental.controller.angular;

import com.venedental.constans.ConstansGlobalReport;
import com.venedental.constans.ConstansTreatmentsReport;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalInfoOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalTreatmentsInput;
import com.venedental.dto.reportDentalTreatments.ReportDentalTreatmentsOutput;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceKeysPatients;
import com.venedental.services.ServiceReportDentalTreatments;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import services.utils.CustomLogger;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

//import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
//import org.glassfish.jersey.media.multipart.FormDataParam;


/**
 * Created by luis.carrasco@arkiteck.biz on 14/07/16.
 */

@ApplicationPath("/resources")
@Path("/reportDentalTreatments")
public class ReportDentalTreatments extends Application {

    private static String NOMBRE_REPORTE =  "";
    private static String NOMBRE =  "";
    private static Integer NUM_REPORTE = -1;
    private static java.util.Date DATEFOREPORT;

    @EJB
    private ServiceReportDentalTreatments serviceReportDentalTreatments;


    @EJB
    private ServiceCreateUser serviceCreateUser;


    private static final Logger log = CustomLogger.getGeneralLogger(ReportDentalTreatments.class.getName());

    private JRBeanCollectionDataSource beanCollectionDataSource;

    private List<ReportDentalTreatments> listTreatments;

    @EJB
    private ServiceKeysPatients serviceKeysPatients;

    private JasperPrint jasperPrint;


    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getListDentalTreatments")
    public List<ReportDentalTreatmentsOutput> getListDentalTreatments() throws ParseException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
        Integer idUser = user.getId();

        List<ReportDentalTreatmentsOutput> result = serviceReportDentalTreatments.getReportDentalTreatements(idUser);
   for(ReportDentalTreatmentsOutput filterReport:result){

            //fecha maxima y minima para el filtro de detalle
            String dateKey = new SimpleDateFormat("dd/MM/yyyy").format(filterReport.getDateKey());
            filterReport.setDateKeyS(dateKey);
            String startDate = new SimpleDateFormat("dd/MM/yyyy").format(filterReport.getStartDate());
            filterReport.setStartDateS(startDate);
            String endDate = new SimpleDateFormat("dd/MM/yyyy").format(filterReport.getEndDate());
            filterReport.setEndDateS(endDate);
        }

        return result;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("getCorrelativeNumber")
    public ReportDentalInfoOutput getCorrelativeNumber(@QueryParam("idUser") Integer idUser) {
         ReportDentalInfoOutput reportDentalInfoOutput;
    /*
       TODO: Si el id del usuario puede obtenerse desde esta capa se eliminaría el parametro idUser
     */
        Integer correlativeNumber = -1;
        reportDentalInfoOutput = serviceReportDentalTreatments.getCorrelativeNumber(idUser);
        this.NUM_REPORTE=reportDentalInfoOutput.getNumCorrelativo();
        this.DATEFOREPORT=reportDentalInfoOutput.getDateForReport();

        return reportDentalInfoOutput;

    }
    
    

    public Integer getIdUserLogged(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());

        return user.getId();
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("updateKeysStatus")
    public Integer updateKeysStatus(@QueryParam("idKey") Integer idKey){
        // params Integer idKey, Integer numReport,Integer idSpecialist

        Boolean updated;
        Integer response = 0;
        Integer idUser = this.getIdUserLogged();
         ReportDentalInfoOutput reportDentalInfoOutput;
         reportDentalInfoOutput=this.getCorrelativeNumber(idUser);
        Integer numReport=reportDentalInfoOutput.getNumCorrelativo();

          //  updated = serviceReportDentalTreatments.updateKeysStatus(idKey,numReport,idUser);
       // if(updated){
            response = 1;
//        }else{
//            response = 0;
//        }
//        return response;
        return 1;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("findInfoKey")
    public KeysDetailsOutput findInfoKey(@QueryParam("idKey") Integer idKey){

        KeysDetailsOutput keysDetailsOutput;
        keysDetailsOutput = serviceKeysPatients.findDetails(idKey);

        if(keysDetailsOutput != null){

            List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutputs;




           consultKeysPatientsDetailTreatmentOutputs = serviceKeysPatients.detailsConsultKeysPatientsTreatments(idKey);
              for(ConsultKeysPatientsDetailTreatmentOutput filterReportDetail:consultKeysPatientsDetailTreatmentOutputs){

                    String dateForTratament = new SimpleDateFormat("dd/MM/yyyy").format(filterReportDetail.getDateForTreatment());
            filterReportDetail.setDateFilterTreatment(dateForTratament);

              }

              keysDetailsOutput.setListConsultKeysPatientsDetailTreatmentOutput
                    (consultKeysPatientsDetailTreatmentOutputs);

            if(keysDetailsOutput.getListConsultKeysPatientsDetailTreatmentOutput()!=null){

                return keysDetailsOutput;
            }
            else{
                keysDetailsOutput = new KeysDetailsOutput();
                keysDetailsOutput.setId(-1);
                ConsultKeysPatientsDetailTreatmentOutput consultKeysPatientsDetailTreatmentOutput = null;
                List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutputs1 = null;
                consultKeysPatientsDetailTreatmentOutputs1.add(consultKeysPatientsDetailTreatmentOutput);
                keysDetailsOutput.setListConsultKeysPatientsDetailTreatmentOutput(consultKeysPatientsDetailTreatmentOutputs1);
                return keysDetailsOutput;
            }

        }
        else{

            keysDetailsOutput = new KeysDetailsOutput();
            keysDetailsOutput.setId(-1);
            return keysDetailsOutput;

        }

    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Path("getDate")
    public Date getDateFirstTreatment(){

        return serviceReportDentalTreatments.getDateFirstTreatmentGenerate(getIdUserLogged());

    }


    @POST
    @Path("/downloadTreatRep")
    @Produces("application/pdf")
    public Response downloadExcelFile() {
        // set file (and path) to be download
        File file = new File(ReportDentalTreatments.NOMBRE_REPORTE);

        ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename=\"+"+ReportDentalTreatments.NOMBRE+"+\"");
        return responseBuilder.build();

    }




    @PUT
//    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("generatePDF")
    public  void generatePDF(ArrayList<ReportDentalTreatmentsOutput> arrayTreatments) throws IOException {
        String jasperPath ="";

        // Obtengo la información del especialista para el reporte

       // System.out.println(arrayTreatments.get(0).getNumberKey());

        try {

            SpecialistOutput specialistOutput = serviceReportDentalTreatments.getSpecialistToReport(this.getIdUserLogged());

            if(specialistOutput != null){


                String nameReport = "RP_" + specialistOutput.getName() + "_"
                        + specialistOutput.getCadrId().toString();

                Map<String, Object> params = new HashMap<String, Object>();
                String logoPath = this.obtainDirectory() + this.readFileProperties().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_LOGO);
                    jasperPath = this.obtainDirectory() + this.readFileProperties().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_JASPER);


                ReportDentalTreatments.NOMBRE_REPORTE = jasperPath + nameReport + ".pdf";
                ReportDentalTreatments.NOMBRE = nameReport + ".pdf";

            // Información del Especialista para el reporte
                params.put("name",specialistOutput.getName());
                params.put("cardId",specialistOutput.getCadrId());
                params.put("phone",specialistOutput.getPhone());
                params.put("address",specialistOutput.getAddress());
                params.put("especialidad",specialistOutput.getSpecialityName());

             // logo de venedental
                params.put("heal_logo2", logoPath);
            //  lista

                params.put("listTreatments", new JRBeanCollectionDataSource(arrayTreatments));

                //
                params.put("numCorrelative",this.getCorrelativeNumber(this.getIdUserLogged()));

                try {

                    this.jasperPrint = JasperFillManager.fillReport(jasperPath, params, beanCollectionDataSource);

                } catch (JRException e) {

                    e.printStackTrace();

                }
                buildReport(nameReport,arrayTreatments, params, jasperPath);



            }



        }
        catch (IOException e){

            System.out.println("Error al generar pdf");
        }


        /*




         */
    }

    public Properties readFileProperties() throws IOException {
        Properties myProperties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + ConstansTreatmentsReport.FILENAME_TREATMENT_REPORT
                    + "' not found in the classpath");
        }
        myProperties.load(inputStream);
        return myProperties;
    }



    public List<File> findFile(String nameReport) throws IOException {
        List<File> files = new ArrayList<File>();
        String reportPath = readFileProperties().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_REPORT);
        String reportExtension = readFileProperties().getProperty(ConstansGlobalReport.PROPERTY_REPORT_EXTENSION);
        nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
        String path = reportPath + nameReport + reportExtension;
        File file = new File(path);
        files.add(file);
        return files;
    }

    public static String obtainDirectory() {
        log.log(Level.INFO, "obtainDirectory()");
        URL rutaURL = ReportDentalTreatments.class.getProtectionDomain().getCodeSource().getLocation();
        String path = rutaURL.getPath();

        return path.substring(0, path.indexOf("lib"));
    }

    public void buildReport(String nameReport, List<ReportDentalTreatmentsOutput> listTreatments,
                            Map<String, Object> params, String jasperPath) throws IOException {
        try {
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
                    listTreatments);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperPath, params, beanCollectionDataSource);
            nameReport = StringUtils.replace(StringUtils.stripAccents(nameReport), " ", "_");
            String reportPath = readFileProperties().getProperty(ConstansTreatmentsReport.PROPERTY_PATH_REPORT);
            String reportExtension = readFileProperties().getProperty(ConstansGlobalReport.PROPERTY_REPORT_EXTENSION);
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + nameReport + reportExtension);



        } catch (JRException ex) {

            Logger.getLogger(ReportDentalTreatments.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

}
