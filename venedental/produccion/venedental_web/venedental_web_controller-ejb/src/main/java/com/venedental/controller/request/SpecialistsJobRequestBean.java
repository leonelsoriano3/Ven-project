package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.UsersSessionBean;
import com.venedental.controller.view.AddressesViewBean;

import com.venedental.controller.view.CitiesViewBean;
import com.venedental.controller.view.ClinicsViewBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.MedicalOfficeViewBean;
import com.venedental.controller.view.PhoneClinicViewBean;
import com.venedental.controller.view.ScaleViewBean;
import com.venedental.controller.view.SpecialistJobViewBean;
import com.venedental.controller.view.SpecialistsViewBean;
import com.venedental.controller.view.SpecialityViewBean;
import com.venedental.controller.view.StateViewBean;
import com.venedental.controller.view.TypeSpecialistViewBean;
import com.venedental.enums.TipoClinica;
import com.venedental.facade.ClinicsFacadeLocal;
import com.venedental.facade.SpecialistJobFacadeLocal;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.UsersFacadeLocal;
import com.venedental.model.Addresses;
import com.venedental.model.Cities;
import com.venedental.model.Clinics;
import com.venedental.model.MedicalOffice;
import com.venedental.model.PhoneClinic;
import com.venedental.model.SpecialistJob;
import com.venedental.model.Specialists;
import com.venedental.model.State;
import com.venedental.model.TypeSpecialist;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

@ManagedBean(name = "specialistsJobRequestBean")
@RequestScoped
public class SpecialistsJobRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(SpecialistsJobRequestBean.class.getName());

    @EJB
    private UsersFacadeLocal usersFacadeLocal;

    @EJB
    private ClinicsFacadeLocal clinicsFacadeLocal;

    @EJB
    private SpecialistJobFacadeLocal specialistJobFacadeLocal;

    @EJB
    private SpecialistsFacadeLocal specialistsFacadeLocal;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
    @ManagedProperty(value = "#{clinicsViewBean}")
    private ClinicsViewBean clinicsViewBean;
    @ManagedProperty(value = "#{medicalOfficeViewBean}")
    private MedicalOfficeViewBean medicalOfficeViewBean;
    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{usersSessionBean}")
    private UsersSessionBean usersSessionBean;
    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;
   @ManagedProperty(value = "#{scaleViewBean}")
    private ScaleViewBean scaleViewBean;
    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;
    @ManagedProperty(value = "#{specialistJobViewBean}")
    private SpecialistJobViewBean specialistJobViewBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    private List<Specialists> listaSpecialists;
     private Specialists beforeEditSpecialists;
    

    private String valor = "";

    public MedicalOfficeViewBean getMedicalOfficeViewBean() {
        return medicalOfficeViewBean;
    }

    public void setMedicalOfficeViewBean(MedicalOfficeViewBean medicalOfficeViewBean) {
        this.medicalOfficeViewBean = medicalOfficeViewBean;
    }
    
    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public SpecialistJobViewBean getSpecialistJobViewBean() {
        return specialistJobViewBean;
    }

    public void setSpecialistJobViewBean(SpecialistJobViewBean specialistJobViewBean) {
        this.specialistJobViewBean = specialistJobViewBean;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistsViewBean) {
        this.typeSpecialistViewBean = typeSpecialistsViewBean;
    }

    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }

    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public UsersSessionBean getUsersSessionBean() {
        return usersSessionBean;
    }

    public void setUsersSessionBean(UsersSessionBean usersSessionBean) {
        this.usersSessionBean = usersSessionBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public ScaleViewBean getScaleViewBean() {
        return scaleViewBean;
    }

    public void setScaleViewBean(ScaleViewBean scaleViewBean) {
        this.scaleViewBean = scaleViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public ClinicsViewBean getClinicsViewBean() {
        return clinicsViewBean;
    }

    public void setClinicsViewBean(ClinicsViewBean clinicsViewBean) {
        this.clinicsViewBean = clinicsViewBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public List<Specialists> getListaSpecialists() {
        return listaSpecialists;
    }

    public void setListaSpecialists(List<Specialists> listaSpecialists) {
        this.listaSpecialists = listaSpecialists;
    }

    public Specialists getBeforeEditSpecialists() {
        return beforeEditSpecialists;
    }

    public void setBeforeEditSpecialists(Specialists beforeEditSpecialists) {
        this.beforeEditSpecialists = beforeEditSpecialists;
    }
    
    public void prepareNewSpecialists() {
        this.getSpecialistsViewBean().setNewSpecialists(new String[1]);
        this.getStateViewBean().setNewState(new State());
        this.getCitiesViewBean().setNewCities(new Cities());
        this.getCitiesViewBean().getListCities().clear();
        this.getAddressesViewBean().setNewAddresses(new Addresses());
        this.getTypeSpecialistViewBean().setNewTypeSpecialist(new TypeSpecialist());
        this.getSpecialistJobViewBean().setNewSpecialistJob(new SpecialistJob());
        if (this.getClinicsViewBean().getListaClinicsSpecialist() != null) {
            this.getClinicsViewBean().getListaClinicsSpecialist().clear();
        }
        if (this.getSpecialityViewBean().getNewListaSpecialists() != null) {
            this.getSpecialityViewBean().getNewListaSpecialists().clear();
        }

    }
    
    public void createCliniscToSpecialistEdit(Clinics item, String tipo) {
         if (tipo.equals("clinicaNew")) {
             if (item == null) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir una Clínica"));
          }else{
            MedicalOffice medicalOffice = new MedicalOffice();
            medicalOffice.setName("Consultorio en : " + item.getName());
            medicalOffice.setRif(item.getRif());
            medicalOffice.setClinicId(item);
            medicalOffice.setAddressId(item.getAddressId());
            this.getSpecialistJobViewBean().getEditSpecialistJob().setMedicalOfficeId(medicalOffice);
            this.getSpecialistJobViewBean().getEditSpecialistJob().setPhoneClinicList(item.getPhoneClinicList());
            this.getSpecialistJobViewBean().getEditListSpecialistJob().add(this.getSpecialistJobViewBean().getEditSpecialistJob());
            this.getSpecialistJobViewBean().setEditSpecialistJob(new SpecialistJob());
            this.getClinicsViewBean().setNewClinicsObj(new Clinics());
            this.getMedicalOfficeViewBean().setNewMedicalOffice(new String[7]);
            context.update("directorioEdit");
             }
        } else if (tipo.equals("medicalOfficeNew")) {
            MedicalOffice medicalOffice = new MedicalOffice();
            medicalOffice.setName(this.getMedicalOfficeViewBean().getNewMedicalOffice()[0]);
            medicalOffice.setRif(this.getSpecialistsViewBean().getEditSpecialists()[7]);
            medicalOffice.setAddressId(this.getAddressesViewBean().getNewClinicAddresses());
            this.getSpecialistJobViewBean().getEditSpecialistJob().setPhoneClinicList(this.getPhoneClinicViewBean().getNewListPhoneClinic());
            this.getSpecialistJobViewBean().getEditSpecialistJob().setMedicalOfficeId(medicalOffice);
            
            this.getCitiesViewBean().getNewClinicCities().setStateId(this.getStateViewBean().getNewClinicState());
            this.getAddressesViewBean().getNewClinicAddresses().setCityId(this.getCitiesViewBean().getNewClinicCities());
            this.getSpecialistJobViewBean().getEditListSpecialistJob().add(this.getSpecialistJobViewBean().getEditSpecialistJob());
            this.getClinicsViewBean().setNewClinics(new String [17]);
            this.getMedicalOfficeViewBean().setNewMedicalOffice(new String[7]);
            this.getStateViewBean().setNewClinicState(new State());
            this.getCitiesViewBean().setNewClinicCities(new Cities());
            this.getAddressesViewBean().setNewClinicAddresses(new Addresses());
            this.getPhoneClinicViewBean().setNewListPhoneClinic(new ArrayList<PhoneClinic>());
             this.getSpecialistJobViewBean().setEditSpecialistJob(new SpecialistJob());
            context.update("directorioEdit");
          
           
        }
    }

    public void createCliniscToSpecialist() {
        if (specialistJobViewBean.getNewSpecialistJob() != null) {
            
            MedicalOffice medicalOffice = new MedicalOffice();
            medicalOffice.setName("CONSULTORIO EN " + this.getClinicsViewBean().getNewClinicsObj().getName());
            medicalOffice.setRif(this.getClinicsViewBean().getNewClinicsObj().getRif());
            medicalOffice.setClinicId(this.getClinicsViewBean().getNewClinicsObj());
            medicalOffice.setAddressId(this.getClinicsViewBean().getNewClinicsObj().getAddressId());
            medicalOffice.setLocation(this.getMedicalOfficeViewBean().getNewMedicalOffice()[2]);
            this.getSpecialistJobViewBean().getNewSpecialistJob().setMedicalOfficeId(medicalOffice);
            this.getSpecialistJobViewBean().getNewSpecialistJob().setPhoneClinicList(this.getClinicsViewBean().getNewClinicsObj().getPhoneClinicList());
            this.getClinicsViewBean().getListaClinicsSpecialist().add(this.getSpecialistJobViewBean().getNewSpecialistJob());
            this.getClinicsViewBean().setNewClinicsObj(new Clinics());
            this.getMedicalOfficeViewBean().setNewMedicalOffice(new String[7]);
            this.getMedicalOfficeViewBean().getNewMedicalOffice()[0]="";
            this.getSpecialistJobViewBean().setNewSpecialistJob(new SpecialistJob());
            
            
      
        }  else {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir una Clínica"));
        }
    }

    public void deleteCliniscToSpecialist(int item) {
        this.getClinicsViewBean().getListaClinicsSpecialist().remove(item);
        if (this.getClinicsViewBean().getListaClinicsSpecialist().isEmpty()) {
            this.getComponenteViewBean().setUltimoPaso(false);
        }
    }

    public void inicializarCamposRequeridos(Boolean valor) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valor);
        this.getComponenteSessionBean().setCamposRequeridosEdit(valor);
    }

    public void mascaraRif(String tipoRif) {
        this.specialistsViewBean.setMascaraRifEdit("V999999999");
        switch (tipoRif) {
            case "V":
                this.specialistsViewBean.setMascaraRifEdit("V999999999");
                break;
            case "J":
                this.specialistsViewBean.setMascaraRifEdit("J999999999");
                break;
            case "E":
                this.specialistsViewBean.setMascaraRifEdit("E999999999");
                break;
            case "G":
                this.specialistsViewBean.setMascaraRifEdit("G999999999");
                break;
        }
    }

    public void focusGrowl() {
        RequestContext.getCurrentInstance().scrollTo("growl");
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    
    public boolean telefonoDuplicado(String telefono, boolean nuevo) {
        List<PhoneClinic> listaTelefono = null;
        if (nuevo) {
            listaTelefono = phoneClinicViewBean.getNewListPhoneClinic();
        } else {
            listaTelefono = phoneClinicViewBean.getEditListPhoneClinic();
        }

        for (PhoneClinic p : listaTelefono) {
            if (p.getPhone().equals(telefono)) {
                return true;
            }
        }
        return false;
    }
    
        public void addPhoneClinic(String phone, boolean nuevo) {
        if (phone == null || phone.equals("")) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe ingresar un número Telefonico"));
        } else if (telefonoDuplicado(phone, nuevo)) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "El número Telefonico ya existe"));
            if (nuevo) {
                phoneClinicViewBean.setNewPhoneClinic(new String[1]);
            } else {
                phoneClinicViewBean.setEditPhoneClinic(new String[1]);
            }
        } else {
            if (nuevo) {
                this.getPhoneClinicViewBean().getNewListPhoneClinic().add(new PhoneClinic(phone));
                phoneClinicViewBean.setNewPhoneClinic(new String[1]);
            } else {
                this.getPhoneClinicViewBean().getEditListPhoneClinic().add(new PhoneClinic(phone));
                phoneClinicViewBean.setEditPhoneClinic(new String[1]);
            }
        }
    }
    
     public void makeCompleteName(String especialista) {
         
       String nombreCompleto = "";
      
          if(especialista !=null && especialista.trim().compareTo("") !=0){
         
            nombreCompleto = nombreCompleto + especialista.trim() + " ";
         }
          if (nombreCompleto !=null && nombreCompleto.trim().compareTo("") != 0) {
            this.specialistsViewBean.getNewSpecialists()[5]= this.specialistsViewBean.getNewSpecialists()[5]+ nombreCompleto;
        }
     
    }
     
      public void revisarCedula(String especialista, String id) {
          
          String revisar=especialista;
          
          
//        int cedula = Integer.parseInt(especialista[10]);
//        int idSpecialist = Integer.parseInt(especialista[0]);
//        for (Specialists e : getListaSpecialists()) {
//            if (e.getIdentityNumber() != null && !e.getId().equals(idSpecialist) && e.getIdentityNumber() == cedula) {
//                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El Especialista ya se encuentra registrado"));
//                
//                if (idSpecialist !=0) {
//                    String identityNumber= this.getBeforeEditSpecialists().getIdentityNumber().toString();
//                    
//                    especialista[10]=identityNumber;
//                    
//  
//                    ((UIInput) findComponent(id)).setValue("");
//                } else {
//                    especialista[10]=null;
//                }
//                RequestContext.getCurrentInstance().update(findComponent(id).getClientId());
//                break;
//            }
//        }
   }
   
  public void createConsultToSpecialist() {

        if (this.getMedicalOfficeViewBean().getNewMedicalOffice()[0] == null || this.getMedicalOfficeViewBean().getNewMedicalOffice()[0].trim().compareTo("") == 0) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe especificar el nombre del consultorio "));
        } else if (this.getAddressesViewBean().getNewClinicAddresses() == null || this.getAddressesViewBean().getNewClinicAddresses().toString().trim().compareTo("") == 0) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe especificar la dirección del consultorio"));
        } else if (this.getStateViewBean().getNewClinicState() == null || this.getStateViewBean().getNewClinicState().toString().trim().compareTo("") == 0) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe especificar el estado del consultorio" ));
        } else if (this.getCitiesViewBean().getNewClinicCities() == null || this.getCitiesViewBean().getNewClinicCities().toString().trim().compareTo("") == 0) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe especificar la ciudad del consultorio"));
        } else if (this.getSpecialistJobViewBean().getNewSpecialistJob().getSchedule() == null || this.getSpecialistJobViewBean().getNewSpecialistJob().getSchedule().trim().compareTo("") == 0) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe especificar el horario"));
        } else {
            
              
            MedicalOffice medicalOffice = new MedicalOffice();
            medicalOffice.setName(this.getMedicalOfficeViewBean().getNewMedicalOffice()[0]);
            medicalOffice.setRif(this.getSpecialistsViewBean().getNewSpecialists()[7]);
            medicalOffice.setLocation(this.getMedicalOfficeViewBean().getNewMedicalOffice()[2]);
            medicalOffice.setAddressId(this.getAddressesViewBean().getNewClinicAddresses());
            this.getSpecialistJobViewBean().getNewSpecialistJob().setPhoneClinicList(this.getPhoneClinicViewBean().getNewListPhoneClinic());
            this.getSpecialistJobViewBean().getNewSpecialistJob().setMedicalOfficeId(medicalOffice);
            this.getCitiesViewBean().getNewClinicCities().setStateId(this.getStateViewBean().getNewClinicState());
            this.getAddressesViewBean().getNewClinicAddresses().setCityId(this.getCitiesViewBean().getNewClinicCities());
            this.getClinicsViewBean().getListaClinicsSpecialist().add(this.getSpecialistJobViewBean().getNewSpecialistJob());
            this.getMedicalOfficeViewBean().setNewMedicalOffice(new String[7]);
            this.getMedicalOfficeViewBean().getNewMedicalOffice()[0]="";
            this.getStateViewBean().setNewClinicState(new State());
            this.getCitiesViewBean().setNewClinicCities(new Cities());
            this.getAddressesViewBean().setNewClinicAddresses(new Addresses());
            this.getPhoneClinicViewBean().setNewListPhoneClinic(new ArrayList<PhoneClinic>());
            this.getSpecialistJobViewBean().setNewSpecialistJob(new SpecialistJob());
        }
    }
     
     
     

}
