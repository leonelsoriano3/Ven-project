/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.SecurityViewBean;
import com.venedental.controller.view.SpecialistsViewBean;
import com.venedental.controller.view.TreatmentsViewBean;
import com.venedental.enums.TypeScale;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.facade.ScaleTreatmentsSpecialistFacadeLocal;
import com.venedental.model.Scale;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.ScaleTreatmentsSpecialist;
import com.venedental.model.Treatments;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceScaleTreatments;
import com.venedental.services.ServicesScaleTreatmentSpecific;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Desarrollo
 */
@ManagedBean(name = "scaleTreatmentsSpecialistRequestBean")
@RequestScoped
public class ScaleTreatmentsSpecialistRequestBean {

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;

    @EJB
    private ScaleTreatmentsSpecialistFacadeLocal scaleTreatmentsSpecialistFacadeLocal;

    @EJB
    private ServiceScaleTreatments serviceScaleTreatments;

    @EJB
    private ServicesScaleTreatmentSpecific servicesScaleTreatmentSpecific;

    private final RequestContext context = RequestContext.getCurrentInstance();

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistViewBean;
    @ManagedProperty(value = "#{treatmentsViewBean}")
    private TreatmentsViewBean treatmentsViewBean;

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public ScaleTreatmentsFacadeLocal getScaleTreatmentsFacadeLocal() {
        return scaleTreatmentsFacadeLocal;
    }

    public void setScaleTreatmentsFacadeLocal(ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal) {
        this.scaleTreatmentsFacadeLocal = scaleTreatmentsFacadeLocal;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public SpecialistsViewBean getSpecialistViewBean() {
        return specialistViewBean;
    }

    public void setSpecialistViewBean(SpecialistsViewBean specialistViewBean) {
        this.specialistViewBean = specialistViewBean;
    }

    public TreatmentsViewBean getTreatmentsViewBean() {
        return treatmentsViewBean;
    }

    public void setTreatmentsViewBean(TreatmentsViewBean treatmentsViewBean) {
        this.treatmentsViewBean = treatmentsViewBean;
    }

    public void onCellEdit(CellEditEvent event) throws SQLException {
        if (!event.getOldValue().equals(event.getNewValue())) {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            FacesContext contexto = FacesContext.getCurrentInstance();
            DataTable table = (DataTable) event.getSource();
            ScaleTreatmentsSpecialist scaleTreatmentSpecialist = (ScaleTreatmentsSpecialist) table.getRowData();
            if (scaleTreatmentSpecialist.getIdScaleTreatments().getPrice() == null) {
                ScaleTreatments scaleTreatmentOld = this.scaleTreatmentsFacadeLocal.find(scaleTreatmentSpecialist.getIdScaleTreatments().getId());
                scaleTreatmentSpecialist.getIdScaleTreatments().setPrice(scaleTreatmentOld.getPrice());
                contexto.addMessage(null, new FacesMessage("Error", "Debe ingresar el precio"));
                requestContext.update("form:growBaremoSpecialistEdit");
                requestContext.update("form:configBaremoEspecifico");
                requestContext.update("form:growBaremoSpecialistEditAdd");
                requestContext.update("form:configBaremoEspecificoAdd");
                //this.getSpecialistViewBean().findScaleTreatmentsSpecialist();

            } else {
                ScaleTreatments scaleTreatmentsNew = new ScaleTreatments();
                scaleTreatmentsNew.setIdTreatments(scaleTreatmentSpecialist.getIdScaleTreatments().getIdTreatments());
                scaleTreatmentsNew.setIdTypeScale(scaleTreatmentSpecialist.getIdScaleTreatments().getIdTypeScale());
                scaleTreatmentsNew.setPrice(scaleTreatmentSpecialist.getIdScaleTreatments().getPrice());
                Date dateScale = new Date();
                scaleTreatmentsNew.setDateScale(dateScale);
                ScaleTreatments scaleTreatCreated = this.scaleTreatmentsFacadeLocal.create(scaleTreatmentsNew);
                //Nuevo ScaleTreatmentsSpecialist                    
                ScaleTreatmentsSpecialist scaleTreatmentsSpecialistNew = new ScaleTreatmentsSpecialist();
                scaleTreatmentsSpecialistNew.setIdSpecialist(scaleTreatmentSpecialist.getIdSpecialist());
                scaleTreatmentsSpecialistNew.setIdScaleTreatments(scaleTreatCreated);
                this.scaleTreatmentsSpecialistFacadeLocal.create(scaleTreatmentsSpecialistNew);
                this.serviceScaleTreatments.registerScaleProTre(scaleTreatCreated.getId());
                contexto.addMessage(null, new FacesMessage("Éxito", "Cambios realizados satisfactoriamente"));
                requestContext.update("form:growBaremoSpecialistEdit");
                requestContext.update("form:growBaremoSpecialistEditAdd");
                this.getSpecialistViewBean().findScaleTreatmentsSpecialist();
                //this.getConfigBaremoViewBean().filterListTreatments();

            }

        }
    }

    public void createScaleTreatmentsSpecialist() throws SQLException {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        FacesContext contexto = FacesContext.getCurrentInstance();

        if (getTreatmentsViewBean().getNewTreatments() == null || getTreatmentsViewBean().getNewTreatments().getId() == null) {

            contexto.addMessage(null, new FacesMessage("Error", "Debe ingresar el tratamiento"));
            requestContext.update("form:growBaremoSpecialistEdit");
            requestContext.update("form:growBaremoSpecialistEditAdd");
            getTreatmentsViewBean().getNewTreatmentsString()[1] = "";
            getTreatmentsViewBean().setNewTreatments(new Treatments());

        } else if (getTreatmentsViewBean().getNewTreatmentsString()[1] == null || getTreatmentsViewBean().getNewTreatmentsString()[1].isEmpty() || getTreatmentsViewBean().getNewTreatmentsString()[1].equals("")) {

            contexto.addMessage(null, new FacesMessage("Error", "Debe ingresar el precio"));
            requestContext.update("form:growBaremoSpecialistEdit");
            requestContext.update("form:growBaremoSpecialistEditAdd");

            getTreatmentsViewBean().getNewTreatments().setPriceNew(0.00);
            getTreatmentsViewBean().getNewTreatmentsString()[1] = "";
            getTreatmentsViewBean().setNewTreatments(new Treatments());

        } else {

            ScaleTreatments scaleTreatmentsNew = new ScaleTreatments();
            Integer idTreatment = getTreatmentsViewBean().getNewTreatments().getId();
            Integer idSpecialist = getSpecialistViewBean().getEditSpecialistsObj().getId();
            double precioDouble = Double.parseDouble(getTreatmentsViewBean().getNewTreatmentsString()[1]);
            Date date = new Date();
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdfDate.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
            String dateScale = sdfDate.format(date);
            
            
            // Obtain Users
            Users user = this.securityViewBean.obtainUser();
            user.getId();
            System.out.println(idTreatment);
            System.out.println(idSpecialist);
            System.out.println(user);
            System.out.println(precioDouble);
//            java.sql.Date dateDetailSql = this.convertJavaDateToSqlDate(date);
//            
            this.servicesScaleTreatmentSpecific.registerScaleTreatment(idTreatment, precioDouble, dateScale, idSpecialist, user.getId());
//          
            this.getSpecialistViewBean()
                    .findScaleTreatmentsSpecialist();

            this.getSpecialistViewBean()
                    .fillTreatmentsSpecialist();
            getTreatmentsViewBean()
                    .getNewTreatments().setPriceNew(0.00);
            getTreatmentsViewBean()
                    .getNewTreatmentsString()[1] = "";
            getTreatmentsViewBean()
                    .setNewTreatments(new Treatments());
//
            contexto.addMessage(
                    null, new FacesMessage("Éxito", "Nuevo Baremo Agregado Exitosamente"));
            requestContext.update(
                    "form:growBaremoSpecialistEdit");
            requestContext.update(
                    "form:configBaremoEspecifico");
        }

    }

    /**
     * Method at converter dateUtil and dateSql
     *
     * @param date
     * @return
     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        if (date != null) {
            return new java.sql.Date(date.getTime());
        } else {
            return null;
        }
    }
}
