/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.enums.TypeScale;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.facade.TypeSpecialistFacadeLocal;
import com.venedental.dto.baremo.ConsultBaremoGeneralOutput;
import com.venedental.dto.baremo.ConsultBaremoTreatmentForConfigOutput;
import com.venedental.dto.baremo.HistoryBaremosOutput;
import com.venedental.dto.baremo.ConfigBaremoDateMinFilterOutput;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.Treatments;
import com.venedental.model.TypeSpecialist;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceBaremos;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import services.utils.Transformar;

/**
 *
 * @author AKDESKDEV90
 */
@ManagedBean(name = "configBaremoViewBean")
@ViewScoped
public class ConfigBaremoViewBean extends BaseBean implements Serializable {

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;

    @EJB
    private TypeSpecialistFacadeLocal typeSpecialistFacadeLocal;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    private final RequestContext context = RequestContext.getCurrentInstance();

    private List<ScaleTreatments> scaleTreatmentsList;

    private ScaleTreatments scaleSelected;

    private List<ScaleTreatments> scaleTreatmentsListFiltered;

    private List<TypeSpecialist> typeSpecialityList;

    private TypeSpecialist selectedTypeSpeciality;
    private TypeSpecialist selectednameSpeciality;

    /* Attributes */
    private List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutput;

    private List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutputFilter;

    /* Services */
    @EJB
    private ServiceBaremos serviceBaremos;

    private String nameStreament, nameConsult, dateValidate, userHistory;
    private Date dateDetail, dateHistory, dateCompare, startDate, maxDate, vigencia, endDate, dateFilter, dateSqlFilter;
    private Double priceDetail, priceHistory, priceComparate, price;
    private boolean btnActive, imputBaremo, dateEnd, btnRest, btnBuscar, btnHistory,
            btnSave, btnActiveprueba, btnExport, columDateBaremo, columNameTratment, columBaremo, minDateF;
    private String minDate, maxDateCalendar, nameTreatment, maxDateFilter;

    private int validate, idTreatment;
    private HistoryBaremosOutput historyBaremosOutput;
    private List<HistoryBaremosOutput> historyBaremosOutputList;
    private List<ConsultBaremoTreatmentForConfigOutput> consultBaremoTreatmentForConfigOutput;
    private List<ConsultBaremoTreatmentForConfigOutput> consultBaremoTreatmentForConfigOutputFilter;
    private List<ConfigBaremoDateMinFilterOutput> configBaremoDateMinFilterOutput;

    @PostConstruct
    public void init() {
        this.getTypeSpecialityList();
        this.setBtnActive(true);
        this.setImputBaremo(true);
        this.setImputDateEnd(true);
        this.setBtnRest(true);
        this.setBtnBuscar(true);
        this.setBtnSave(true);
        this.dateLimit();
//        this.dateMinFilter();

    }

    public List<ConsultBaremoGeneralOutput> getConsultBaremoGeneralOutput() {
        return consultBaremoGeneralOutput;
    }

    public void setConsultBaremoGeneralOutput(List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutput) {
        this.consultBaremoGeneralOutput = consultBaremoGeneralOutput;
    }

    public List<ConsultBaremoGeneralOutput> getConsultBaremoGeneralOutputFilter() {
        return consultBaremoGeneralOutputFilter;
    }

    public void setConsultBaremoGeneralOutputFilter(List<ConsultBaremoGeneralOutput> consultBaremoGeneralOutputFilter) {
        this.consultBaremoGeneralOutputFilter = consultBaremoGeneralOutputFilter;
    }

    public List<ConsultBaremoTreatmentForConfigOutput> getConsultBaremoTreatmentForConfigOutput() {
        return consultBaremoTreatmentForConfigOutput;
    }

    public void setConsultBaremoTreatmentForConfigOutput(List<ConsultBaremoTreatmentForConfigOutput> consultBaremoTreatmentForConfigOutput) {
        this.consultBaremoTreatmentForConfigOutput = consultBaremoTreatmentForConfigOutput;
    }

    public List<ConsultBaremoTreatmentForConfigOutput> getConsultBaremoTreatmentForConfigOutputFilter() {
        return consultBaremoTreatmentForConfigOutputFilter;
    }

    public void setConsultBaremoTreatmentForConfigOutputFilter(List<ConsultBaremoTreatmentForConfigOutput> consultBaremoTreatmentForConfigOutputFilter) {
        this.consultBaremoTreatmentForConfigOutputFilter = consultBaremoTreatmentForConfigOutputFilter;
    }

    public List<ConfigBaremoDateMinFilterOutput> getConfigBaremoDateMinFilterOutput() {
        return configBaremoDateMinFilterOutput;
    }

    public void setConfigBaremoDateMinFilterOutput(List<ConfigBaremoDateMinFilterOutput> configBaremoDateMinFilterOutput) {
        this.configBaremoDateMinFilterOutput = configBaremoDateMinFilterOutput;
    }

    /*----------------------------------------------------------------------------------------*
     Comienzo de Getter y Setter
     *----------------------------------------------------------------------------------------*
     */
    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMaxDateCalendar() {
        return maxDateCalendar;
    }

    public void setMaxDateCalendar(String maxDateCalendar) {
        this.maxDateCalendar = maxDateCalendar;
    }

    public Date getDateCompare() {
        return dateCompare;
    }

    public void setDateCompare(Date dateCompare) {
        this.dateCompare = dateCompare;
    }

    public boolean isBtnActive() {
        return btnActive;
    }

    public void setBtnActive(boolean btnActive) {
        this.btnActive = btnActive;
    }

    public boolean isBtnActiveprueba() {
        return btnActiveprueba;
    }

    public void setBtnActiveprueba(boolean btnActiveprueba) {
        this.btnActiveprueba = btnActiveprueba;
    }

    public boolean isBtnBuscar() {
        return btnBuscar;
    }

    public void setBtnBuscar(boolean btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public boolean isBtnSave() {
        return btnSave;
    }

    public void setBtnSave(boolean btnSave) {
        this.btnSave = btnSave;
    }

    public boolean isBtnHistory() {
        return btnHistory;
    }

    public void setBtnHistory(boolean btnHistory) {
        this.btnHistory = btnHistory;
    }

    public boolean isImputBaremo() {
        return imputBaremo;
    }

    public void setImputBaremo(boolean imputBaremo) {
        this.imputBaremo = imputBaremo;
    }

    public boolean isImputDateEnd() {
        return dateEnd;
    }

    public void setImputDateEnd(boolean dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isBtnRest() {
        return btnRest;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public void setBtnRest(boolean btnRest) {
        this.btnRest = btnRest;
    }

    public Double getPriceComparate() {
        return priceComparate;
    }

    public void setPriceComparate(Double priceComparate) {
        this.priceComparate = priceComparate;
    }

    public String getNameStreament() {
        return nameStreament;
    }

    public void setNameStreament(String nameStreament) {
        this.nameStreament = nameStreament;
    }

    public String getNameConsult() {
        return nameConsult;
    }

    public void setNameConsult(String nameConsult) {
        this.nameConsult = nameConsult;
    }

    public Date getDateDetail() {
        return dateDetail;
    }

    public void setDateDetail(Date dateDetail) {
        this.dateDetail = dateDetail;
    }

    public Double getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(Double priceDetail) {
        this.priceDetail = priceDetail;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<ScaleTreatments> getScaleTreatmentsListFiltered() {
        return scaleTreatmentsListFiltered;
    }

    public void setScaleTreatmentsListFiltered(List<ScaleTreatments> scaleTreatmentsListFiltered) {
        this.scaleTreatmentsListFiltered = scaleTreatmentsListFiltered;
    }

    public ScaleTreatmentsFacadeLocal getScaleTreatmentsFacadeLocal() {
        return scaleTreatmentsFacadeLocal;
    }

    public List<ScaleTreatments> getScaleTreatmentsList() {
        return scaleTreatmentsList;
    }

    public void setScaleTreatmentsList(List<ScaleTreatments> scaleTreatmentsList) {
        this.scaleTreatmentsList = scaleTreatmentsList;
    }

    public void setTypeSpecialityList(List<TypeSpecialist> typeSpecialityList) {
        this.typeSpecialityList = typeSpecialityList;
    }

    public TypeSpecialistFacadeLocal getTypeSpecialistFacadeLocal() {
        return typeSpecialistFacadeLocal;
    }

    public void setTypeSpecialistFacadeLocal(TypeSpecialistFacadeLocal typeSpecialistFacadeLocal) {
        this.typeSpecialistFacadeLocal = typeSpecialistFacadeLocal;
    }

    public TypeSpecialist getSelectedTypeSpeciality() {
        return selectedTypeSpeciality;
    }

    public void setSelectedTypeSpeciality(TypeSpecialist selectedTypeSpeciality) {
        this.selectedTypeSpeciality = selectedTypeSpeciality;
    }

    public String getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(String userHistory) {
        this.userHistory = userHistory;
    }

    public Date getDateHistory() {
        return dateHistory;
    }

    public void setDateHistory(Date dateHistory) {
        this.dateHistory = dateHistory;
    }

    public Double getPriceHistory() {
        return priceHistory;
    }

    public void setPriceHistory(Double priceHistory) {
        this.priceHistory = priceHistory;
    }

    public HistoryBaremosOutput getHistoryBaremosOutput() {
        return historyBaremosOutput;
    }

    public void setHistoryBaremosOutput(HistoryBaremosOutput historyBaremosOutput) {
        this.historyBaremosOutput = historyBaremosOutput;
    }

    public List<HistoryBaremosOutput> getHistoryBaremosOutputList() {
        return historyBaremosOutputList;
    }

    public void setHistoryBaremosOutputList(List<HistoryBaremosOutput> historyBaremosOutputList) {
        this.historyBaremosOutputList = historyBaremosOutputList;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public List<TypeSpecialist> getTypeSpecialityList() {
        if (this.typeSpecialityList == null) {
            this.typeSpecialityList = this.typeSpecialistFacadeLocal.findAll();
        }

        Collections.sort(typeSpecialityList, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                TypeSpecialist tp1 = (TypeSpecialist) o1;
                TypeSpecialist tp2 = (TypeSpecialist) o2;
                return tp1.getName().compareTo(tp2.getName());

            }
        });
        return typeSpecialityList;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public ScaleTreatments getScaleSelected() {
        return scaleSelected;
    }

    public void setScaleSelected(ScaleTreatments scaleSelected) {
        this.scaleSelected = scaleSelected;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getMaxDateFilter() {
        return maxDateFilter;
    }

    public void setMaxDateFilter(String maxDateFilter) {
        this.maxDateFilter = maxDateFilter;
    }

    public Date getDateSqlFilter() {
        return dateSqlFilter;
    }

    public void setDateSqlFilter(Date dateSqlFilter) {
        this.dateSqlFilter = dateSqlFilter;
    }

    public boolean isMinDateF() {
        return minDateF;
    }

    public void setMinDateF(boolean minDateF) {
        this.minDateF = minDateF;
    }

    /*----------------------------------------------------------------------------------------*
     Comienzo de los metodos para el manejo de las vistas (viewBean)
     *----------------------------------------------------------------------------------------**/
    public void filterListTreatments() throws SQLException {

        if (this.getSelectedTypeSpeciality() != null) {
            Integer speciality = this.getSelectedTypeSpeciality().getId();
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:configBaremo");
            dataTable.reset();
            dataTable.getFilters().clear();
            this.setDateFilter(null);
            this.setPrice(null);
            this.setNameTreatment(null);

            RequestContext.getCurrentInstance().update("form:configBaremo");
            RequestContext.getCurrentInstance().update("form:configBaremo:filter");
            this.setConsultBaremoTreatmentForConfigOutput(this.serviceBaremos.findConsultbaremoForConf(speciality));
            this.setScaleTreatmentsList(new ArrayList<ScaleTreatments>());
            this.setScaleTreatmentsList(scaleTreatmentsFacadeLocal.findByTypeSpeciality(
                    TypeScale.General.getValor(), this.getSelectedTypeSpeciality().getId()));
            for (ConsultBaremoTreatmentForConfigOutput consultBaremoTreatmentForConfigOutput : this.consultBaremoTreatmentForConfigOutput) {
                java.util.Date utilDate = new java.util.Date(consultBaremoTreatmentForConfigOutput.getDateBaremo().getTime());
                consultBaremoTreatmentForConfigOutput.setDateUtil(utilDate);
            }

            RequestContext.getCurrentInstance().update("form:configBaremo");
        } else {
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:configBaremo");
            dataTable.reset();
            dataTable.getFilters().clear();
            this.setScaleTreatmentsList(new ArrayList<ScaleTreatments>());
            RequestContext.getCurrentInstance().update("form:configBaremo");

        }

        for (ConsultBaremoTreatmentForConfigOutput consultBaremoTreatmentForConfigOutput : this.consultBaremoTreatmentForConfigOutput) {
            Integer speciality = this.getSelectedTypeSpeciality().getId();
            this.getConsultBaremoTreatmentForConfigOutput();
            this.setHistoryBaremosOutputList(this.serviceBaremos.detailsHistoryBaremoInteger(idTreatment,
                    speciality, null, null));
            if (this.historyBaremosOutputList == null) {
                this.setBtnHistory(true);
                RequestContext.getCurrentInstance().update("form:configBaremo");

            } else {
                this.setBtnHistory(false);
            }
            RequestContext.getCurrentInstance().update("form:configBaremo");

        }

    }

    /**
     * Filtar baremo
     *
     * @param value datos a evaluar
     * @param filter palabra en el filtro
     * @param locale
     * @return regresa verdadero si el value cumple con el filtro
     */
    public boolean filterBaremo(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        BigDecimal bigValue = new BigDecimal(value.toString());

        return bigValue.toString().matches("(?i).*" + (filter.toString().replaceFirst("\\.0*$", "")) + ".*");
    }

    /**
     * Funcion designada para cargar los valores del dialogo de detalles
     *
     * @param
     */
    public void chargeDetail(ConsultBaremoTreatmentForConfigOutput consultBaremoTreatmentForConfigOutput) {

        this.setDateDetail(consultBaremoTreatmentForConfigOutput.getDateBaremo());
        this.setDateCompare(consultBaremoTreatmentForConfigOutput.getDateBaremo());
        this.setNameStreament(consultBaremoTreatmentForConfigOutput.getNameTreatment());
        this.setPriceDetail(consultBaremoTreatmentForConfigOutput.getBaremo());
        this.setPriceComparate(consultBaremoTreatmentForConfigOutput.getBaremo());
        this.dateMin(consultBaremoTreatmentForConfigOutput.getDateBaremo());
        this.idTreatment = consultBaremoTreatmentForConfigOutput.getIdTreatment();
        RequestContext.getCurrentInstance().update("form:panelBaremoDetail");
        RequestContext.getCurrentInstance().update("form:starDate");
    }

    public void updateBaremoGeneral() throws SQLException {

        this.getPriceDetail();
        this.getDateDetail();
        java.sql.Date dateDetailSql = this.convertJavaDateToSqlDate(
                this.getDateDetail());

        // Obtain Users
        Users user = this.securityViewBean.obtainUser();
        user.getId();
        this.serviceBaremos.ConfigBaremoGeneralSave(this.idTreatment, 0, user.getId(), priceDetail, dateDetailSql);

        FacesContext.getCurrentInstance().addMessage("form:msgBaremo", new FacesMessage("Éxito",
                "Operación realizada con éxito"));
        this.resetDataTable();
        this.filterListTreatments();
        this.setImputBaremo(true);
        RequestContext.getCurrentInstance().update("form:imputBaremos");
        RequestContext.getCurrentInstance().update("form:growBaremo");
        RequestContext.getCurrentInstance().update("form:detailBaremo");
        RequestContext.getCurrentInstance().update("form:msgBaremo");

    }

    public void cerrarDialogoView(String dialogo) throws SQLException {
        context.update("form:" + dialogo);
        context.execute("PF('" + dialogo + "').hide()");
        this.filterListTreatments();

    }

    /**
     * Funcion designada para cargar los valores del dialogo de historia
     *
     * @param
     */
    public void detailHistory(ConsultBaremoTreatmentForConfigOutput consultBaremoTreatmentForConfigOutput) throws SQLException {
        this.idTreatment = consultBaremoTreatmentForConfigOutput.getIdTreatment();
        this.setNameStreament(consultBaremoTreatmentForConfigOutput.getNameTreatment());
        this.setHistoryBaremosOutputList(this.serviceBaremos.detailsHistoryBaremoInteger(idTreatment, this.getSelectedTypeSpeciality().getId(), null, null));
        for (HistoryBaremosOutput historyBaremosOutput : this.historyBaremosOutputList) {
            if (historyBaremosOutput.getProcessData() != null) {
                String[] process = historyBaremosOutput.getProcessData().split("-");
                String user = process[0].toString();
                String dayAndHrsProc = process[3].toString();
                String monthProc = process[2].toString();
                String yearProc = process[1].toString().trim();
                String day = dayAndHrsProc.substring(0, 2);
                String hrs = dayAndHrsProc.substring(2);
                String dateP = day + "/" + monthProc + "/" + yearProc + " " + hrs;
                Date d = new Date(dateP);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
                String fechaProceso = sdf.format(d);
                historyBaremosOutput.setProcessData(user + fechaProceso);
            }
        }

        RequestContext.getCurrentInstance().update("form:panelHistoryBaremo");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:historyBaremoTable");
        dataTable.reset();
    }

    /**
     * Funcion que valida que el baremo sea mayor que 0 en caso de no ser asi
     * muestra un mensaje en la vista
     *
     * @param event
     */
    public void validateChange(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        try {
            if (this.getPriceDetail() <= 0.00) {
                FacesMessage msg = new FacesMessage("El baremo es un campo obligatorio.");
                this.setBtnActive(true);
//                this.setBtnActiveprueba(false);
//                this.setBtnActiveprueba(true);
                RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
                RequestContext.getCurrentInstance().update("form:btnUpdateDataFinnancial");

                msg.setSeverity(FacesMessage.SEVERITY_ERROR);

                comp.setValid(false);
                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();

            } else {
                this.writerValuesDialogDetail();
                this.setBtnActive(false);
                RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage("El baremo es un campo obligatorio.");
            this.setBtnActive(false);
            this.setBtnActiveprueba(false);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        }

    }

    public void validateDate(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            //dateValidate=this.dateDetail.toString();
            if (this.getDateDetail() != null) {
//                this.setBtnActive(true);
//                this.setBtnActiveprueba(true);
                this.setImputBaremo(true);
                RequestContext.getCurrentInstance().update("form:imputBaremos");
//                RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
                RequestContext.getCurrentInstance().update("form:msgBaremo");

            } else {
                FacesMessage msg = new FacesMessage("Fecha de Vigencia es Obligatoria. Ingresar fecha con el formato dd/mm/yyyy");
//                this.setBtnSave(false);
//
//                RequestContext.getCurrentInstance().update("form:btnSaveBaremo");

                msg.setSeverity(FacesMessage.SEVERITY_ERROR);

                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();
                RequestContext.getCurrentInstance().update("form:msgBaremo");

            }
        } catch (Exception e) {
            // FacesMessage msg = new FacesMessage("Fecha de Vigencia es obligatoria. Ingresar fecha con el formato dd/mm/yyyy");
            // this.setBtnBuscar(false);
            //msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            //FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            // FacesContext.getCurrentInstance().renderResponse();
            // RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
        }

    }

    /**
     * Con este metodo valdidamos que se realizo algun cambio con el precio de
     * entrada en el dialogo details (priceCompare) con el precio de que se
     * modifica en la vista priceDetails, este evento se dispara al escribir
     */
    public void writerValuesDialogDetail() {
        if (this.getPriceComparate() != this.getPriceDetail() && this.getDateDetail() != this.getDateCompare()) {
            this.setBtnActive(false);
            RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
        } else {
            this.setBtnActive(true);
            RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
        }
        RequestContext.getCurrentInstance().update("form:btnSaveBaremo");
    }

    /**
     * Metodo que se encarga de resetear la tabla sin cambiar el filtro ademas
     * que a su vez setea el valor por defecto del boton para su deshabilitacion
     */
    public void resetDataTable() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:configBaremo");
        if (dataTable != null) {
            this.setBtnActive(true);
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:configBaremo");
            RequestContext.getCurrentInstance().reset("form");
            RequestContext.getCurrentInstance().update("form:configBaremo");
            RequestContext.getCurrentInstance().update("form");
        }

    }

    /**
     * Con esta función podemos asignarle un límite al calendario del dialogo de
     * detalles en la bandeja de baremos, con este le estamos diciendo al
     * componente que la fecha mínima para poder funcionar es a partir del día
     * siguiente que entrará en vigencia el baremo
     */
    public void dateMin(Date vigencia) {
        int day;
        int month;
        int year;
        Calendar c = Calendar.getInstance();
        c.setTime(vigencia); // set object Date
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);
        int dayIgual=c.get(Calendar.DATE);
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        int numDays = c.getActualMaximum(c.DATE);
        if (c.get(Calendar.DATE) == numDays) {
            day = 1;
            month = month + 2;
        } else {
            day = c.get(Calendar.DATE) + 1;
            month = month + 1;
        }
        if (c.get(Calendar.DATE) == numDays && c.get(Calendar.MONTH) == 11) {
            day = 1;
            month = 1;
            year = c.get(Calendar.YEAR) + 1;
        } else {
            year = c.get(Calendar.YEAR);
        }

        int minYear = year;
        this.setMinDate(String.valueOf(dayIgual) + "/" + String.valueOf(month) + "/" + String.valueOf(year));

    }

    public void dateLimit() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // set object Date

        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        int limYear = year + 1;
        int minYear = year - 1;

        this.setMinDate("01/01/" + String.valueOf(minYear));
        this.setMaxDateCalendar("31/12/" + String.valueOf(limYear));
        this.dateSqlFilter = this.serviceBaremos.findDateMinFilter(0);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.maxDateFilter = sdf.format(this.getDateSqlFilter());
    }

    public void selectDate() {
        this.setImputBaremo(false);
        RequestContext.getCurrentInstance().update("form:imputBaremos");

    }

    public void selectSalir() {
        this.setImputBaremo(true);
        RequestContext.getCurrentInstance().update("form:imputBaremos");

    }

    public void selectDateConsultBaremo() {
        this.setImputDateEnd(false);
        RequestContext.getCurrentInstance().update("form:endDate");

    }

    public void selectDateEndConsultBaremo() {
        this.setBtnRest(false);
        this.setBtnBuscar(false);
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");

    }

    public void activecmbInsurances() {

        if (this.getStartDate() != null && this.getEndDate() != null) {
            this.setBtnRest(false);

            this.setBtnBuscar(false);
            //this.setBtnExportData(true);

            //findAllInsurances();
        } else {
            this.setBtnRest(false);

            this.setBtnBuscar(false);
            //this.setBtnActiveInsurances(true);
            //this.setBtnSearchData(true);

            //this.setBtnExportData(true);
        }

        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:btnFindKey");

    }

    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    public boolean filterAmountPay(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        BigDecimal bigValue = new BigDecimal(value.toString());

        return round(bigValue.doubleValue(), 2).toString().equals(filter.toString());
    }

    public Double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelect(SelectEvent event) throws ParseException {
        log.log(Level.INFO, "handleDateSelect()");

        RequestContext.getCurrentInstance().execute("PF('configBaremo').filter()");
        RequestContext.getCurrentInstance().execute("PF('configBaremo').filter()");

    }

}
