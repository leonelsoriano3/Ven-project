/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.helper;

import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.dto.massiveLoading.MassiveUpdateExecution;
import com.venedental.dto.massiveLoading.RowLoad;
import com.venedental.dto.massiveLoading.TemporaryPatient;
import com.venedental.massiveLoading.base.Constants;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.base.enums.EFields;
import com.venedental.massiveLoading.exceptions.MessageException;
import com.venedental.model.Insurances;
import com.venedental.services.ServiceMassiveLoading;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akdesk01
 */
public class PersisterHelper {

    private static final Logger log = CustomLogger.getLogger(PersisterHelper.class.getName());

    public static void persistRow(RowLoad rowLoad, ServiceMassiveLoading serviceMassiveLoading,
            Insurances insurance) throws MessageException {
        List<TemporaryPatient> list = generateTemporaryPatient(rowLoad.getData(),insurance);
        for (TemporaryPatient temporaryPatient : list) {
            try {
                serviceMassiveLoading.registerTemporaryPatient(temporaryPatient);
                System.out.printf("%d_%d_%d\n", temporaryPatient.getIdentityNumberHolder(), temporaryPatient.getIdentityNumber(), temporaryPatient.getIdPlan());
            } catch (SQLException ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
    }

    private static List<TemporaryPatient> generateTemporaryPatient(List<String> data,Insurances insurance) {
        List<TemporaryPatient> list = new ArrayList<>();
        List<Integer> plansList = getPlans(data);
        for (Integer plan : plansList) {
            TemporaryPatient temporaryPatient = new TemporaryPatient();
            temporaryPatient.setIdentityNumberHolder(getIdentityNumberHolder(data));
            temporaryPatient.setIdentityNumber(getIdentityNumber(data));
            temporaryPatient.setRelationshipId(getRelationship(data));
            temporaryPatient.setCompleteName(getCompleteName(data));
            temporaryPatient.setDateOfBirth(getBirthday(data));
            temporaryPatient.setSex(getSex(data));
            temporaryPatient.setIdPlan(plan);
            temporaryPatient.setIdInsurances(insurance.getId());
            temporaryPatient.setCompany(getCompany(data));
            list.add(temporaryPatient);
        }
        return list;
    }

    private static Integer getIdentityNumberHolder(List<String> data) {
        EFields field = EFields.IDENTITY_NUMBER_HOLDER;
        String fieldString = data.get(field.getOrder());
        return Integer.parseInt(fieldString);
    }

    private static Integer getIdentityNumber(List<String> data) {
        EFields field = EFields.IDENTITY_NUMBER;
        String fieldString = data.get(field.getOrder());
        return Integer.parseInt(fieldString);
    }

    private static Integer getRelationship(List<String> data) {
        EFields field = EFields.RELATIONSHIP;
        String fieldString = data.get(field.getOrder());
        return Integer.parseInt(fieldString);
    }

    private static String getCompleteName(List<String> data) {
        EFields field = EFields.COMPLETE_NAME;
        String fieldString = data.get(field.getOrder());
        return fieldString;
    }

    private static Date getBirthday(List<String> data) {
        EFields field = EFields.BIRTHDAY;
        String fieldString = data.get(field.getOrder());
        SimpleDateFormat format;
        if (fieldString.length() > 8) {
            String[] tokens = fieldString.split(Constants.DIRECTORY_SEPARATOR);
            if (tokens[0].length() == 4) {
                format = new SimpleDateFormat(Constants.FORMAT_DATE_4_YEAR);
            } else {
                format = new SimpleDateFormat(Constants.FORMAT_DATE_4);
            }
        } else {
            format = new SimpleDateFormat(Constants.FORMAT_DATE_2);
        }
        try {
            return format.parse(fieldString);
        } catch (ParseException ex) {
        }
        return null;
    }

    private static String getSex(List<String> data) {
        EFields field = EFields.SEX;
        String fieldString = data.get(field.getOrder());
        return fieldString;
    }

    private static List<Integer> getPlans(List<String> data) {
        EFields field = EFields.PLANS;
        String fieldString = data.get(field.getOrder());
        List<Integer> list = new ArrayList<>();
        for (String plan : fieldString.split(",")) {
            list.add(Integer.parseInt(plan));
        }
        return list;
    }

    private static String getCompany(List<String> data) {
        EFields field = EFields.COMPANY;
        String fieldString = data.get(field.getOrder());
        return fieldString;
    }

    public static void executeMassiveUpdate(InsuranceLoad insuranceLoading, ServiceMassiveLoading serviceMassiveLoading,Boolean completLoading, Boolean include, Boolean exclude) {
        try {
            MassiveUpdateExecution massiveUpdateExecution = new MassiveUpdateExecution(insuranceLoading.getCurrentYear(), insuranceLoading.getInsurance().getId(), completLoading, include,exclude);
            
            serviceMassiveLoading.executeMassiveUpdate(massiveUpdateExecution,completLoading,include, exclude);
        } catch (SQLException ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

}
