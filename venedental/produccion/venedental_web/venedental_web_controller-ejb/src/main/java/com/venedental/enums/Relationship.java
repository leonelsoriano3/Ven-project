package com.venedental.enums;

public enum Relationship {
    
    
    PADRE_MADRE(1),HIJO_HIJA(2),ABUELO_ABUELA(3),NIETO_NIETA(4),CONGUYE(5),HERMANO_HERMANA(6),SOBRINO_SOBRINA(7),TIO_TIA(8),FAMILIAR(9),TITULAR(10);
    
    Integer valor;

    private Relationship(Integer valor) {
        this.valor = valor;
    }
    
    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
