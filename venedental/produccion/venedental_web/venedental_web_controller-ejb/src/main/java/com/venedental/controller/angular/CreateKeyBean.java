package com.venedental.controller.angular;


import com.venedental.dto.createUser.FindUserByCardIdOutput;
import com.venedental.mail.base.enums.Email;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServicePlansTreatmentKeys;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonelsoriano3@gmail.com on 18/03/16.
 */


@SessionScoped
@ApplicationPath("/resources")
@Path("createKeyBean")
public class CreateKeyBean extends Application {


    @EJB
    private ServicePlansTreatmentKeys servicePlansTreatmentKeys;


    @EJB
    private ServiceAnalysisKeys serviceAnalysisKeys;

    @EJB
    private ServiceCreateUser serviceCreateUser;

    @EJB
    private MailSenderLocal mailSenderLocal;



    @Path("existEspecialist")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public FindUserByCardIdOutput existEspecialist(@QueryParam("cardId") Integer cardId) {
        FindUserByCardIdOutput user = serviceCreateUser.findSpecialistByCardId(cardId);

        /*
        *   si el usuario existe y el id del usuario es nulo
        * */
        if(user !=null && user.getUsersId() == 0){

            if(user.getEmail() !=null){

                user.setLogin(this.serviceCreateUser.getUserNameByIdSpecialist(user.getFirstName(),user.getLastName()));
            }
            else{
                user =  new FindUserByCardIdOutput();
                user.setId(-2);
            }


        }

        if(user == null){
            user =  new FindUserByCardIdOutput();
            user.setId(-1);
            user.setCompleteName("");
        }

        return  user;
    }

    /**
     *
     * @param cardId
     * @param idSpecialist
     * @param password
     * @return
     *      0- Ya el Especialista está Registrado
     *      1-Se logró Guardar La información del Usuario
     *      2- Se envió el correo
     *      3- El Especialista No posee correo
     *      4- Error Actualizando los Ids
     * @throws IOException
     * @throws ParseException
     */
    @Path("insertUserData")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Integer insertUserData(@QueryParam("cardId") Integer cardId,@QueryParam("idSpecialist") Integer idSpecialist,@QueryParam("password") String password)
            throws IOException, ParseException {
        /**
         * Obtengo la información del usuario dada la cedula
         */
        FindUserByCardIdOutput user = serviceCreateUser.findSpecialistByCardId(cardId);
        // Le asignamos el Login al usuario Dato el nombre y apellido del mismo.
        user.setLogin(this.serviceCreateUser.getUserNameByIdSpecialist(user.getFirstName(),user.getLastName()));

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        //insertamos los datos del usuario en base de datos y obtenemos el id de dicho usuario
        Integer idUser = serviceCreateUser.getUserIdIsertData(user.getCompleteName(),
                user.getLogin(),hashedPassword, user.getEmail());

        if (idUser != null) {
            // falta preguntar que tipo de usuario es para modificar los ids
            if(user.getUser_type().equals("S")){

                try{
                    serviceCreateUser.updateIdUserByIdSpecialist(user.getId(),idUser);
                }
                catch (SQLException e){
                    System.out.println(e);
                }

            }
            else if(user.getUser_type().equals("E")){

                serviceCreateUser.updateIdUserByIdEmployee(user.getIdEmployee(),idUser);

            }
            else if(user.getUser_type().equals("SE")){

                serviceCreateUser.updateIdUserByIdEmployee(user.getIdEmployee(),idUser);


                try{
                    serviceCreateUser.updateIdUserByIdSpecialist(user.getId(),idUser);
                }
                catch (SQLException e){
                    System.out.println(e);
                }
            }
            else{
                return 4;
            }

            if(user.getEmail() != null) {
                sendEmail(null, this.buildMessage(user.getLogin(), password), user.getEmail());
                return 2;
            }else{
                return 3;
            }

        }
        return 1;

    }


    private void sendEmail(List<File> files, String buildMessage, String email) throws ParseException, IOException{

        String subjectMessage = "Grupo Veneden - Notificación de generación de credenciales";
        String title ="";
        String contentMessage = buildMessage;
        files = new ArrayList<File>();
        boolean send = this.mailSenderLocal.send(Email.MAIL_CREDENCIALES, subjectMessage,title, contentMessage,files, email);

    }

    private String buildMessage(String login,String password) throws ParseException, IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<strong><center>Información de la creación de credenciales de ingreso</strong></center>").append("<br><br>");
        stringBuilder.append("Estimado usuario, ").append("<br><br>");
        stringBuilder.append("Se ha efectuado exitosamente la generación de sus credenciales de ingreso. ").append("<br><br>");
        stringBuilder.append("Su información de acceso es: ").append("<br><br>");
        stringBuilder.append("Usuario: ").append(login).append("<br>");
        stringBuilder.append("Clave: ").append(password).append("<br><br>");
        stringBuilder.append("Siempre dispuestos a prestarle un mejor servicio.").append("<br><br>");
        stringBuilder.append("<br>");
        stringBuilder.append("<strong>Grupo VENEDEN</strong>").append("<br><br>");
        return stringBuilder.toString();
    }
}
