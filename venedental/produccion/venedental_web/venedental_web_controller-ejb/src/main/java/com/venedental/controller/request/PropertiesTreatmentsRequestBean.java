package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.view.ComponenteViewBean;
import com.venedental.controller.view.KeysPatientsViewBean;
import com.venedental.controller.view.PlansTreatmentsKeysViewBean;
import com.venedental.controller.view.PlansTreatmentsViewBean;
import com.venedental.controller.view.PropertiesTreatmentsViewBean;
import com.venedental.controller.view.SpecialistsViewBean;
import com.venedental.enums.TypeScale;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.PlansTreatments;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.PropertiesTreatments;
import com.venedental.model.ScalePropertiesTreatments;
import com.venedental.model.ScaleTreatments;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.context.RequestContext;
import services.utils.CustomLogger;

@ManagedBean(name = "propertiesTreatmentsRequestBean")
@RequestScoped
public class PropertiesTreatmentsRequestBean extends BaseBean implements Serializable {

    private final RequestContext context = RequestContext.getCurrentInstance();
    private static final Logger log = CustomLogger.getGeneralLogger(PropertiesTreatmentsRequestBean.class.getName());

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;

    private List<PropertiesTreatments> newListPropertiesTreatments;

    private List<PropertiesTreatments> listPropertiesTreatmentsSeleccionados;

    private List<PropertiesTreatments> listPropertiesTreatmentsFiltrada;

    private List<PropertiesTreatments> listOdontodiagrama;

    private List<String> listSelectedOptions;

    private String selectedOptions;

    private boolean visibleList;

    private boolean visibleProperties;

    private boolean encontrado;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;
    @ManagedProperty(value = "#{accionesRequestBean}")
    private AccionesRequestBean accionesRequestBean;
    @ManagedProperty(value = "#{componenteViewBean}")
    private ComponenteViewBean componenteViewBean;
    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;
    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;
    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;
    @ManagedProperty(value = "#{keysPatientsViewBean}")
    private KeysPatientsViewBean keysPatientsViewBean;

    public List<PropertiesTreatments> getListOdontodiagrama() {
        if (this.listOdontodiagrama == null) {
            this.listOdontodiagrama = new ArrayList<>();
        }
        return listOdontodiagrama;
    }

    public void setListOdontodiagrama(List<PropertiesTreatments> listOdontodiagrama) {
        this.listOdontodiagrama = listOdontodiagrama;
    }

    public boolean isVisibleProperties() {
        return visibleProperties;
    }

    public void setVisibleProperties(boolean visibleProperties) {
        this.visibleProperties = visibleProperties;
    }

    public String getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(String selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public List<String> getListSelectedOptions() {

        return listSelectedOptions;
    }

    public void setListSelectedOptions(List<String> listSelectedOptions) {
        this.listSelectedOptions = listSelectedOptions;
    }

    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteViewBean getComponenteViewBean() {
        return componenteViewBean;
    }

    public void setComponenteViewBean(ComponenteViewBean componenteViewBean) {
        this.componenteViewBean = componenteViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public List<PropertiesTreatments> getListPropertiesTreatmentsFiltrada() {
        return listPropertiesTreatmentsFiltrada;
    }

    public void setListPropertiesTreatmentsFiltrada(List<PropertiesTreatments> listPropertiesTreatmentsFiltrada) {
        this.listPropertiesTreatmentsFiltrada = listPropertiesTreatmentsFiltrada;
    }

    public void focusGrowl() {
        RequestContext.getCurrentInstance().scrollTo("growl");
    }

    public AccionesRequestBean getAccionesRequestBean() {
        return accionesRequestBean;
    }

    public void setAccionesRequestBean(AccionesRequestBean accionesRequestBean) {
        this.accionesRequestBean = accionesRequestBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public List<PropertiesTreatments> getNewListPropertiesTreatments() {
        if (this.newListPropertiesTreatments == null) {
            this.newListPropertiesTreatments = new ArrayList<>();
        }
        return newListPropertiesTreatments;
    }

    public void setNewListPropertiesTreatments(List<PropertiesTreatments> newListPropertiesTreatments) {

        this.newListPropertiesTreatments = newListPropertiesTreatments;
    }

    public List<PropertiesTreatments> getListPropertiesTreatmentsSeleccionados() {
        if (this.listPropertiesTreatmentsSeleccionados == null) {
            this.listPropertiesTreatmentsSeleccionados = new ArrayList<>();
        }
        return listPropertiesTreatmentsSeleccionados;
    }

    public void setListPropertiesTreatmentsSeleccionados(List<PropertiesTreatments> listPropertiesTreatmentsSeleccionados) {
        this.listPropertiesTreatmentsSeleccionados = listPropertiesTreatmentsSeleccionados;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public KeysPatientsViewBean getKeysPatientsViewBean() {
        return keysPatientsViewBean;
    }

    public void setKeysPatientsViewBean(KeysPatientsViewBean keysPatientsViewBean) {
        this.keysPatientsViewBean = keysPatientsViewBean;
    }

    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }

    public boolean isEncontrado() {
        return encontrado;
    }

    public void setEncontrado(boolean encontrado) {
        this.encontrado = encontrado;
    }

    public void cleanListProperties() {

        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());

    }

    public void addTreatments(String idDialog) {

        this.getPropertiesTreatmentsViewBean().limpiarLista();
        this.getNewListPropertiesTreatments().clear();
        cleanListProperties();
        this.getPropertiesTreatmentsViewBean().setEncontrado(false);

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            if (getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId() != null) {
                this.getPlansTreatmentsViewBean().setNameTreatments(getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getName());
            }

            this.setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));
            if (!this.getNewListPropertiesTreatments().isEmpty()) {

                if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments())) {

                    if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {

                        for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados()) {

                            if (plansTreatmentsKeys.getPlansTreatmentsId().getId().equals(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getId())) {

                                //Obtener el valor del tratamiento que se repite 
                                this.getPropertiesTreatmentsViewBean().setEncontrado(true);
                                this.getPlansTreatmentsKeysViewBean().setNewPlansTreatmentsKeysObj(plansTreatmentsKeys);
                                for (PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey : plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList()) {

                                    if (this.getNewListPropertiesTreatments().contains(propertiesPlansTreatmentsKey.getPropertiesTreatments())) {

                                        this.getNewListPropertiesTreatments().remove(propertiesPlansTreatmentsKey.getPropertiesTreatments());

                                    }
                                }

                            }

                        }

                    }

                }
                if (this.getSpecialistsViewBean().getEditSpecialistsObj().getScale() != null) {

                    fillListProperties(this.getNewListPropertiesTreatments());

                }
//                this.getPropertiesTreatmentsViewBean().setEditListPropertiesTreatments();
//                this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsII(this.getNewListPropertiesTreatments());
                this.visibleProperties = true;
                if (idDialog.equals("keysOdontodiagram")) {
                    this.desplegarDialogoCreate(idDialog, true);

                }
                this.getPropertiesTreatmentsViewBean().setVisiblePanel(true);

            } else {

                this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
                this.getKeysPatientsViewBean().updateBtnPlanTreatments(false);

            }
        }

    }

    public void addTreatmentsNew(String idDialog) {

        this.getPropertiesTreatmentsViewBean().limpiarListaNew();

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            if (getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId() != null) {
                this.getPlansTreatmentsViewBean().setNameTreatments(getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getName());

                this.setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));
            }
            if (!this.getNewListPropertiesTreatments().isEmpty()) {
                fillListPropertiesEdit(this.getNewListPropertiesTreatments());
                this.visibleProperties = true;

                this.getPropertiesTreatmentsViewBean().setVisiblePanel(true);
                

            } else {

                this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);

            }

        }

    }

    public void fillListProperties(List<PropertiesTreatments> newListPropertiesTreatments) {

        this.getPropertiesTreatmentsViewBean().limpiarLista();

        if (!newListPropertiesTreatments.isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : newListPropertiesTreatments) {
                int cuadrante = propertiesTreatments.getPropertiesId().getQuadrant();
                switch (cuadrante) {
                    case 1:

                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatments().add(propertiesTreatments);

                        break;
                    case 2:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsII().add(propertiesTreatments);
                        break;
                    case 3:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsIII().add(propertiesTreatments);

                        break;

                    case 4:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsIV().add(propertiesTreatments);
                        break;

                    case 5:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsV().add(propertiesTreatments);
                        break;

                    case 6:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsVI().add(propertiesTreatments);
                        break;

                    case 7:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsVII().add(propertiesTreatments);
                        break;

                    case 8:
                        this.getPropertiesTreatmentsViewBean().getEditListPropertiesTreatmentsVIII().add(propertiesTreatments);
                        break;
                }

            }
        } else {
            context.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Las piezas para este tratamiento ya fueron seleccionadas , no se puede agregar tratamiento"));

        }

    }

    public void fillListPropertiesEdit(List<PropertiesTreatments> newListPropertiesTreatments) {

        for (PropertiesTreatments propertiesTreatments : newListPropertiesTreatments) {
            int cuadrante = propertiesTreatments.getPropertiesId().getQuadrant();
            switch (cuadrante) {
                case 1:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments().add(propertiesTreatments);

                    break;
                case 2:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsII().add(propertiesTreatments);
                    break;
                case 3:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIII().add(propertiesTreatments);

                    break;

                case 4:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIV().add(propertiesTreatments);
                    break;

                case 5:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsV().add(propertiesTreatments);
                    break;

                case 6:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVI().add(propertiesTreatments);
                    break;

                case 7:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVII().add(propertiesTreatments);
                    break;

                case 8:
                    this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVIII().add(propertiesTreatments);
                    break;
            }

        }

    }

    public void addTreatmentsProperties() {

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {

            PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
            plansTreatmentsKeys.setPlansTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments());
            plansTreatmentsKeys.setPlans_treatments_id(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getId());
            this.getPlansTreatmentsKeysViewBean().setNewPlansTreatmentsKeysObj(plansTreatmentsKeys);
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());

        } else {
            context.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir un tratamiento"));
        }
        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(null);
        this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());

    }

    //Método para agregar tratamientos y piezas a la lista desde la creación
    public void addTreatmentsPlansProperties(String idForm) {

        if (idForm.equals("treatmentII")) {
            addPropertiesByList("treatmentII");
        }
        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {
            this.getNewListPropertiesTreatments().clear();
            this.getNewListPropertiesTreatments().addAll(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));

            if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments()) && this.getNewListPropertiesTreatments().isEmpty()) {

                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Este tratamiento ya se encuentra en la lista "));
            } else {

                //if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments())) {
                this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().add(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments());
                PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
                plansTreatmentsKeys.setPlansTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments());
                plansTreatmentsKeys.setPlans_treatments_id(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getId());

                if (!this.getPropertiesTreatmentsViewBean().isEncontrado()) {
                    this.getPlansTreatmentsKeysViewBean().setNewPlansTreatmentsKeysObj(plansTreatmentsKeys);

                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList().clear();
                }
                if (!this.getNewListPropertiesTreatments().isEmpty()) {

                    //Desde aquí se agregan las piezas al objeto de la lista ListPlansTreatmentsKeysSeleccionados
                    this.addPropertiesNew();
                    if (idForm.equals("treatmentII")) {

                        addPropertiesByListEdit("treatmentII");
                    }

                }
                if (this.getNewListPropertiesTreatments().isEmpty()) {
                    //if (this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments().isEmpty()) {
                    this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                    if (!idForm.equals("treatmentII")) {
                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                    }
                } else if (!this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().isEmpty()) {

                    if (this.getPropertiesTreatmentsViewBean().isEncontrado()) {

                        for (int i = 0; i < this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().size(); i++) {

                            PlansTreatmentsKeys plansTreatmentsKeyR = this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().get(i);

                            if (plansTreatmentsKeyR.getPlansTreatmentsId().getId().equals(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getId())) {
                                plansTreatmentsKeyR.setPropertiesPlansTreatmentsKeyList(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList());
                            }

                        }

                        RequestContext.getCurrentInstance().update("form:createKeys");
                        RequestContext.getCurrentInstance().update("form:lstTreatment");

                    } else {
                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                    }
                    if (!idForm.equals("treatmentII")) {
                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().clear();
                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().addAll(this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados());
                    }

                }

            }
            this.getKeysPatientsViewBean().setCreate(false);
            RequestContext.getCurrentInstance().update("form:createKeys");
            this.getKeysPatientsViewBean().updateBtnPlanTreatments(true);

        } else {
            RequestContext.getCurrentInstance().update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir un tratamiento"));
        }
        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(null);
        this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());
        this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
        this.getPropertiesTreatmentsViewBean().setListPropertiesTreatmentsSeleccionados(new ArrayList<PropertiesTreatments>());

    }

    //Método para agregar tratamientos y piezas a la lista desde la edición 
    public void addTreatmentsPlansPropertiesNew(String idForm) {

        System.out.println("Tratamiento Seleccionado" + this.plansTreatmentsViewBean.getSelectedPlansTreatments());

        addPropertiesByListEdicion("treatmentII");

        if (this.getPlansTreatmentsViewBean().getSelectedPlansTreatments() != null) {
            if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments())) {
                this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().add(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments());
                PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
                plansTreatmentsKeys.setPlansTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments());
                plansTreatmentsKeys.setPlans_treatments_id(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getId());

                this.getPlansTreatmentsKeysViewBean().setNewPlansTreatmentsKeysObj(plansTreatmentsKeys);
                this.setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getId()));

                if (!this.getNewListPropertiesTreatments().isEmpty()) {
                    for (PropertiesTreatments properties : this.getNewListPropertiesTreatments()) {
                        if (properties.getPropertiesId().getQuadrant() == 1) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilter().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 2) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterII().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 3) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterIII().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 4) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterIV().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 5) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterV().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 6) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterVI().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 7) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterVII().add(properties);
                        } else if (properties.getPropertiesId().getQuadrant() == 8) {
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListFilterVIII().add(properties);
                        }

                    }

                    this.addPropertiesEdit();
                    if (idForm.equals("treatmentII")) {

                        //addPropertiesByListEdit("treatmentII");
                    }

                }
                if (this.getNewListPropertiesTreatments().isEmpty()) {
                    //if (this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments().isEmpty()) {
                    this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                    if (!idForm.equals("treatmentII")) {
                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                    }
                } else if (!this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().isEmpty()) {

                    if (this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getComponentTooth()) {
                        //Se actualiza el precio del baremo de acuerdo a la cantidad de piezas seleccionadas
                        Integer cantidad = 0;
                        Double cantidadLong = 0.00;
                        cantidad = this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().size() + this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewII().size() + this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewIII().size();
                        cantidad += this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewIV().size() + this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewV().size();
                        cantidad += this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVI().size() + this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVII().size() + this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVIII().size();

                        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj();

                        cantidadLong = cantidad * 1.00;
                        for (ScaleTreatments scaleTreatments : this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter()) {

                            Double precioBaremo = (this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getTreatmentsId().getPrice()) * cantidadLong;
                            scaleTreatments.setPrice(precioBaremo);
                            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setScaleTreatmentsKeys(scaleTreatments);
                        }
                        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getScaleTreatmentsKeys());
                        //Fin actualización de baremo 

                        this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                        if (!idForm.equals("treatmentII")) {
                            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().add(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj());
                        }

                    }
                }

            } else {
                context.update("form:campoValor");
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Este tratamiento ya se encuentra en la lista "));
                this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(null);
                this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());
                this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsAllEdit(new ArrayList<PropertiesTreatments>());
            }

        } else {
            context.update("form:campoValor");
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir un tratamiento"));
            this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(null);
            this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());
            this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
            this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().clear();
        }
        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(null);
        this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());
        this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
        this.getPropertiesTreatmentsViewBean().setSelectedOptionsAllEdit(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().limpiarListaNew();
        //lIMPIAR LAS PropertiesPlansTreatmentsKeyListNew PARA OBTENER LUEGO LA SUMA REAL EN EL BAREMO

        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());

    }

    public void deletePropertiesTreatments(int item) {
        if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {
            if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().size() == item) {
                item = item - 1;
            }

            PlansTreatmentsKeys planstreatmentsKey = this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().get(item);

            if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().isEmpty()) {
                if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(planstreatmentsKey.getPlansTreatmentsId())) {
                    this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().remove(planstreatmentsKey.getPlansTreatmentsId());
                }
            }
            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().remove(item);
            if (!this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().isEmpty()) {
                this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().remove(item);
            }
            if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().isEmpty()) {
                this.getComponenteViewBean().setUltimoPaso(false);
                this.getKeysPatientsViewBean().setCreate(true);
                RequestContext.getCurrentInstance().update("form:createKeys");
                //this.getKeysPatientsViewBean().updateCreateKeys(true);

            }
        }

    }

    public void editPropertiesTreatments(int item) {

        PlansTreatmentsKeys plansTreatmentsKeysEdit = this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysFiltrada().get(item);
        PlansTreatments plansTreatmentsEdit = plansTreatmentsKeysEdit.getPlansTreatmentsId();
        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(plansTreatmentsEdit);
        for (PropertiesPlansTreatmentsKey propertiesPlanstTreatmentsKeys : plansTreatmentsKeysEdit.getPropertiesPlansTreatmentsKeyList()) {

            this.propertiesTreatmentsViewBean.getSelectedOptionsI().add(propertiesPlanstTreatmentsKeys.getPropertiesTreatments().getPropertiesId().getStringValue());

        }
    }

    public void addSelectedOptions(List<String> selectedOptions, String idlista) {

        // se llaman desde los properties del KeysDetails 
        switch (idlista) {
            case "propertiesOptionI":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsI(selectedOptions);

                break;
            case "propertiesOptionII":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsII(selectedOptions);
                break;
            case "propertiesOptionIII":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsIII(selectedOptions);
                break;

            case "propertiesOptionIV":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsIV(selectedOptions);
                break;

            case "propertiesOptionV":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsV(selectedOptions);
                break;

            case "propertiesOptionVI":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsVI(selectedOptions);
                break;

            case "propertiesOptionVII":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsVII(selectedOptions);
                break;

            case "propertiesOptionVIII":
                this.getPropertiesTreatmentsViewBean().setSelectedOptionsVIII(selectedOptions);
                break;
        }

    }

    public void addPropertiesByListNew(String idDialog) {

        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsII().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIII().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIV().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsV().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVI().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVI()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVII().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVIII().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);

            }
        }

        if (idDialog.equals("keysOdontodiagram")) {
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());
        }
        this.getAccionesRequestBean().cerrarDialogoView("keysOdontodiagram");
        this.getPlansTreatmentsViewBean().setNameTreatments("");

    }

    public void addPropertiesByList(String idDialog) {
        this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().clear();
        this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll();

        // Este se está llamando en el aceptar del odontodiagrama !!
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }

        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIV().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsIV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsV().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVI().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVI()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVIII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatmentsVIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().add(propertiesTreatments);
            }

        }
        if (idDialog.equals("keysOdontodiagram")) {

            //Agregar lo que se ejecuta en tratamiento y agrega el percio del baremo por las piezas seleccionadas 
            if (!this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().isEmpty()) {
                int cantidad = this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().size();
                Long cantidadLong = (long) cantidad;

                for (ScaleTreatments scaleTreatments : this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getScaleTreatmentsListListFilter()) {
                    Double precioBaremo = scaleTreatments.getPrice() * cantidadLong;
                    this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().setPrice(precioBaremo);
                    scaleTreatments.setPrice(precioBaremo);
                }
            }
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());

            //Agrega las piezas a la lista de tratamiento seleccionado 
            this.addTreatmentsPlansProperties("treatment");
        }
        this.getAccionesRequestBean().cerrarDialogoView("keysOdontodiagram");
        this.getPlansTreatmentsViewBean().setNameTreatments("");
        this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().clear();

    }

    public void addPropertiesByListEdicion(String idDialog) {

        this.getPropertiesTreatmentsViewBean().setSelectedOptionsAllEdit(new ArrayList<PropertiesTreatments>());

        //Este método asigna las piezas a la lista general 
        // Este se está llamando desde el método  de  el agregar tratamiento en la edición addTreatmentsPlansPropertiesNew
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatments().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatments()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }

        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIV().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsV().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsV()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVI().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVI()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVIII().isEmpty()) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVIII()) {
                this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().add(propertiesTreatments);
            }

        }

        this.getPlansTreatmentsViewBean().setNameTreatments("");

    }

    public void addPropertiesByListEdit(String idDialog) {

        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatments().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatments()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsII().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsII()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIII().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIII()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIV().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsIV()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsV().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsV()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVI().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVI()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVII().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVII()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (!this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVIII().isEmpty()) {
            for (PropertiesTreatments properties : this.getPropertiesTreatmentsViewBean().getSelectedListPropertiesTreatmentsVIII()) {
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(properties);

            }
        }
        if (idDialog.equals("treatmentII")) {
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatments(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsIII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsIV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsV(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVI(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVII(new ArrayList<PropertiesTreatments>());
            this.getPropertiesTreatmentsViewBean().setSelectedListPropertiesTreatmentsVIII(new ArrayList<PropertiesTreatments>());

        }

    }

    //agregar tambien el baremo para los tratamientos que tienen piezas y se repiten
    public void addPropertiesNew() {

        if (!this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll().isEmpty()) {

            for (PropertiesTreatments propertiesSelected : this.getPropertiesTreatmentsViewBean().getSelectedOptionsAll()) {

                PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
                propertiesPlansTreatmentsKey.setPropertiesTreatments(propertiesSelected);
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList().add(propertiesPlansTreatmentsKey);
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListString().add(selectedOptions);
                this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().add(propertiesSelected);

            }

            int cantidad = this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList().size();
            Double cantidadLong = cantidad * 1.0;

            for (ScaleTreatments scaleTreatments : this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getScaleTreatmentsListListFilter()) {
                Double precioBaremo = this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().getPriceNew() * cantidadLong;
                this.getPlansTreatmentsViewBean().getSelectedPlansTreatments().getTreatmentsId().setPrice(precioBaremo);
                DecimalFormat decf = new DecimalFormat("0.00");
//                String precio = (decf.format(precioBaremo * 1.00));
//                
//                double precioDoubleFormato = Double.parseDouble(precio);
                scaleTreatments.setPrice(precioBaremo);
            }

            this.visibleList = true;
            this.getPropertiesTreatmentsViewBean().setVisibleList(true);
            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setComponentTooth(true);
            //this.getKeysPatientsViewBean().updateCreateKeys(true);

        } else {
            context.update("form:campoValor");
            this.getPropertiesTreatmentsViewBean().setSelectedOptionsAll(null);
            if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().isEmpty()) {
                if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId())) {
                    this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().remove(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId());
                }
            }
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir al menos una pieza"));
        }

    }

    public void addPropertiesEdit() {

        //Limpiar todas las listas de properties seleccionadas en la edición
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNew(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewII(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewIII(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewIV(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewV(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewVI(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewVII(new ArrayList<PropertiesTreatments>());
        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setPropertiesPlansTreatmentsKeyListNewVIII(new ArrayList<PropertiesTreatments>());

        //Metodo que agrega las piezas a la lista de cada plan y es llamado desde el metodo addTreatmentsPlansPropertiesNew
        if (!this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit().isEmpty()) {

            for (PropertiesTreatments propertiesSelected : this.getPropertiesTreatmentsViewBean().getSelectedOptionsAllEdit()) {
                PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
                propertiesPlansTreatmentsKey.setPropertiesTreatments(propertiesSelected);
                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList().add(propertiesPlansTreatmentsKey);

                if (propertiesSelected.getPropertiesId().getQuadrant() == 1) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNew().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 2) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewII().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 3) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewIII().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 4) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewIV().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 5) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewV().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 6) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVI().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 7) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVII().add(propertiesSelected);

                }
                if (propertiesSelected.getPropertiesId().getQuadrant() == 8) {
                    this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListNewVIII().add(propertiesSelected);

                }

                this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListString().add(selectedOptions);

                this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().add(propertiesSelected);

            }
            this.visibleList = true;
            this.getPropertiesTreatmentsViewBean().setVisibleList(true);
            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setComponentTooth(true);
        } else {
            context.update("form:campoValor");
            this.getPropertiesTreatmentsViewBean().setSelectedOptionsAllEdit(null);
            if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().isEmpty()) {
                if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId())) {
                    this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().remove(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId());
                }
            }
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir al menos una pieza"));

        }

    }

    public void addProperties() {

        if (!this.getPropertiesTreatmentsViewBean().getSelectedOptions().isEmpty()) {
            for (PropertiesTreatments propertiesTreatments : this.getNewListPropertiesTreatments()) {
                for (String propertiesSelected : this.getPropertiesTreatmentsViewBean().getSelectedOptions()) {
                    if (propertiesTreatments.getPropertiesId().getStringValue().equals(propertiesSelected) && Objects.equals(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId().getTreatmentsId().getId(), propertiesTreatments.getTreatmentsId().getId())) {
                        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
                        propertiesPlansTreatmentsKey.setPropertiesTreatments(propertiesTreatments);
                        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyList().add(propertiesPlansTreatmentsKey);
                        this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPropertiesPlansTreatmentsKeyListString().add(selectedOptions);
                        this.getPropertiesTreatmentsViewBean().getListPropertiesTreatmentsSeleccionados().add(propertiesTreatments);
                    }
                }
            }
            this.visibleList = true;
            this.getPropertiesTreatmentsViewBean().setVisibleList(true);
            this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().setComponentTooth(true);
        } else {
            context.update("form:campoValor");
            this.getPropertiesTreatmentsViewBean().setSelectedOptions(null);
            if (!this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().isEmpty()) {
                if (this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().contains(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId())) {
                    this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().remove(this.getPlansTreatmentsKeysViewBean().getNewPlansTreatmentsKeysObj().getPlansTreatmentsId());
                }
            }
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Debe elegir al menos una pieza"));
        }

    }

}
