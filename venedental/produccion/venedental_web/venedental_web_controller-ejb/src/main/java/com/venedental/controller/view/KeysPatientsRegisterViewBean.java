/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.dto.keyPatients.HistoricalPatientOutput;
import com.venedental.dto.keyPatients.HistoricalPatientGCOutput;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.dto.keyPatients.DateMinHistoricoOutput;
import com.venedental.dto.keyPatients.HeadboardEmailOutput;
import com.venedental.dto.keyPatients.ReadTreatmentsEmailOutput;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import com.venedental.model.MedicalOffice;
import com.venedental.model.PropertiesTreatments;
import com.venedental.model.Specialists;
import com.venedental.model.Speciality;
import com.venedental.model.TypeAttention;
import com.venedental.services.ServiceMedicalOffice;
import com.venedental.services.ServicePlansPatients;
import com.venedental.services.ServicePropertiesTreatments;
import com.venedental.services.ServiceSpecialists;
import com.venedental.services.ServiceTypeAttention;
import com.venedental.services.ServicePlansTreatments;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import com.venedental.enums.PropertiesEnums;
import com.venedental.dto.planTreatments.TreatmentsAvailableOutput;
import com.venedental.enums.TreatmentsRepeat;
import com.venedental.services.ServiceKeysPatients;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.datatable.DataTable;
import services.utils.CustomLogger;
import services.utils.Transformar;

/**
 *
 * @author Francelys Heredia
 */
@ManagedBean(name = "keysPatientsRegisterViewBean")
@ViewScoped
public class KeysPatientsRegisterViewBean extends BaseBean {

    private List<FilterPlanPatientOutput> filterPlanPatientList;
    private List<Speciality> specialityOfSpecialist;
    private List<MedicalOffice> medicalOfficeList;
    private List<TypeAttention> typeAttentionList;
    private List<PropertiesTreatments> propertiesTreatmentsPermanent;
    private List<PropertiesTreatments> propertiesTreatmentsPermanentSelected;
    private List<PropertiesTreatments> propertiesTreatmentsTemporary;
    private List<PropertiesTreatments> propertiesTreatmentsTemporarySelected;
    private List<TreatmentsAvailableOutput> treatmentsAvailableOutputList;
    private List<TreatmentsAvailableOutput> listPlansTreatSelected;
    private List<TreatmentsAvailableOutput> listPlansTreatSelectedFilter;
    private List<HistoricalPatientGCOutput> historicalPatientGCOutput;
    private List<HistoricalPatientGCOutput> HistoricalPatientGCOutputFilter;
    private List<DateMinHistoricoOutput> dateMinHistoricoOutput;
    private List<HeadboardEmailOutput> headboardEmailOutput;
    private List<ReadTreatmentsEmailOutput> readTreatmentsEmailOutput;
    private List<Specialists> listSpecialists;
    private List<RegisterKeyOutput> registerKeyOutput;
    private TreatmentsAvailableOutput treatmentsAvailableOutputSelected;
    private Specialists specialist;
    private Specialists selectedSpecialist;
    private FilterPlanPatientOutput filterPlanPatientOutput;
    private FilterPlanPatientOutput filterPlanPatientOutputSelected;
    private TypeAttention typeAttentionSelected;
    private MedicalOffice medicalOfficeSelected;
    private KeysDetailsOutput keysDetailsOutput;
    private Integer numberColumnsPermanent;
    private Integer numberColumnsTemporary;
    private boolean disableMedicalOffice = true;
    private boolean disabledTypeAttention = true;
    private boolean visiblePanelTemporary = false;
    private boolean visiblePanelPermanent = false;
    private boolean disabledListTreatments = true;
    private boolean disabledBtnSave = true;
    private boolean labelSuccessfulMailMessage = true;
    private boolean labelMailMessageFailed = true;
    private boolean labelHistoricalPatient = true;
    private boolean disabledBtnAddTreatments = true;
    private boolean visibleColumnPropertiesPermanent = false;
    private boolean visibleColumnPropertiesTemporary = false;
    private boolean disabledPatient = true;
    private boolean disabledHistoricalPatient = true;
    private boolean disabledBtnHistoricalPatient = true;
    private boolean disabledInputTreatment = true;
    private boolean visibleSpecialist = false;
    private boolean visibleMedicalOficce = false;
    private String styleTypeAttention = "disabledStyle";
    private String styleTreatments = "disabledStyle";
    private String messageEmpty = "SELECCIONAR TRATAMIENTO";
    private String dateKeyDetails,medicalOfDetails;

    private Date dateFilter, maxDate,mindateHistorico;

    private static final Logger log = CustomLogger.getGeneralLogger(KeysPatientsRegisterViewBean.class.getName());

    @EJB
    private ServiceSpecialists serviceSpecialists;
    @EJB
    private ServicePlansPatients servicePlansPatients;
    @EJB
    private ServiceMedicalOffice serviceMedicalOffice;
    @EJB
    private ServiceTypeAttention serviceTypeAttention;
    @EJB
    private ServicePropertiesTreatments servicePropertiesTreatments;
    @EJB
    private ServicePlansTreatments servicePlansTreatments;
    @EJB
    private ServiceKeysPatients serviceKeysPatients;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    public String getMessageEmpty() {
        return messageEmpty;
    }

    public void setMessageEmpty(String messageEmpty) {
        this.messageEmpty = messageEmpty;
    }

    public String getStyleTypeAttention() {
        return styleTypeAttention;
    }

    public void setStyleTypeAttention(String styleTypeAttention) {
        this.styleTypeAttention = styleTypeAttention;
    }

    public String getStyleTreatments() {
        return styleTreatments;
    }

    public void setStyleTreatments(String styleTreatments) {
        this.styleTreatments = styleTreatments;
    }

    public boolean getVisibleMedicalOficce() {
        return visibleMedicalOficce;
    }

    public void setVisibleMedicalOficce(boolean visibleMedicalOficce) {
        this.visibleMedicalOficce = visibleMedicalOficce;
    }

    public List<HistoricalPatientGCOutput> getHistoricalPatientGCOutputFilter() {
        return HistoricalPatientGCOutputFilter;
    }

    public void setHistoricalPatientGCOutputFilter(List<HistoricalPatientGCOutput> HistoricalPatientGCOutputFilter) {
        this.HistoricalPatientGCOutputFilter = HistoricalPatientGCOutputFilter;
    }



    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public List<HistoricalPatientGCOutput> getHistoricalPatientGCOutput() {
        if (this.historicalPatientGCOutput == null) {
            this.historicalPatientGCOutput = new ArrayList<>();
        }
        return historicalPatientGCOutput;
    }

    public void setHistoricalPatientGCOutput(List<HistoricalPatientGCOutput> historicalPatientGCOutput) {
        this.historicalPatientGCOutput = historicalPatientGCOutput;
    }

    public KeysDetailsOutput getKeysDetailsOutput() {
        return keysDetailsOutput;
    }

    public void setKeysDetailsOutput(KeysDetailsOutput keysDetailsOutput) {
        this.keysDetailsOutput = keysDetailsOutput;
    }

    public boolean isVisibleSpecialist() {
        return visibleSpecialist;
    }

    public void setVisibleSpecialist(boolean visibleSpecialist) {
        this.visibleSpecialist = visibleSpecialist;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public boolean isDisabledInputTreatment() {
        return disabledInputTreatment;
    }

    public void setDisabledInputTreatment(boolean disabledInputTreatment) {
        this.disabledInputTreatment = disabledInputTreatment;
    }

    public List<Specialists> getListSpecialists() {
        if (this.listSpecialists == null) {
            this.listSpecialists = new ArrayList<>();
        }
        return listSpecialists;
    }

    public void setListSpecialists(List<Specialists> listSpecialists) {
        this.listSpecialists = listSpecialists;
    }

    public boolean getDisabledPatient() {
        return disabledPatient;
    }

    public void setDisabledPatient(boolean disabledPatient) {
        this.disabledPatient = disabledPatient;
    }

    public boolean isDisabledHistoricalPatient() {
        return disabledHistoricalPatient;
    }

    public void setDisabledHistoricalPatient(boolean disabledHistoricalPatient) {
        this.disabledHistoricalPatient = disabledHistoricalPatient;
    }

    public boolean isVisibleColumnPropertiesPermanent() {
        return visibleColumnPropertiesPermanent;
    }

    public void setVisibleColumnPropertiesPermanent(boolean visibleColumnPropertiesPermanent) {
        this.visibleColumnPropertiesPermanent = visibleColumnPropertiesPermanent;
    }

    public boolean isVisibleColumnPropertiesTemporary() {
        return visibleColumnPropertiesTemporary;
    }

    public void setVisibleColumnPropertiesTemporary(boolean visibleColumnPropertiesTemporary) {
        this.visibleColumnPropertiesTemporary = visibleColumnPropertiesTemporary;
    }

    public List<TreatmentsAvailableOutput> getListPlansTreatSelected() {
        if (this.listPlansTreatSelected == null) {
            this.listPlansTreatSelected = new ArrayList<>();
        }
        return listPlansTreatSelected;
    }

    public void setListPlansTreatSelected(List<TreatmentsAvailableOutput> listPlansTreatSelected) {
        this.listPlansTreatSelected = listPlansTreatSelected;
    }

    public List<TreatmentsAvailableOutput> getListPlansTreatSelectedFilter() {
        return listPlansTreatSelectedFilter;
    }

    public void setListPlansTreatSelectedFilter(List<TreatmentsAvailableOutput> listPlansTreatSelectedFilter) {
        this.listPlansTreatSelectedFilter = listPlansTreatSelectedFilter;
    }

    public boolean isDisabledBtnAddTreatments() {
        return disabledBtnAddTreatments;
    }

    public void setDisabledBtnAddTreatments(boolean disabledBtnAddTreatments) {
        this.disabledBtnAddTreatments = disabledBtnAddTreatments;
    }

    public boolean isDisabledBtnSave() {
        return disabledBtnSave;
    }

    public void setDisabledBtnSave(boolean disabledBtnSave) {
        this.disabledBtnSave = disabledBtnSave;
    }

    public boolean isDisabledListTreatments() {
        return disabledListTreatments;
    }

    public void setDisabledListTreatments(boolean disabledListTreatments) {
        this.disabledListTreatments = disabledListTreatments;
    }

    public List<TreatmentsAvailableOutput> getTreatmentsAvailableOutputList() {
        if (this.treatmentsAvailableOutputList == null) {
            this.treatmentsAvailableOutputList = new ArrayList<>();
        }
        return treatmentsAvailableOutputList;
    }

    public void setTreatmentsAvailableOutputList(List<TreatmentsAvailableOutput> TreatmentsAvailableOutputList) {
        this.treatmentsAvailableOutputList = TreatmentsAvailableOutputList;
    }

    public TreatmentsAvailableOutput getTreatmentsAvailableOutputSelected() {
        return treatmentsAvailableOutputSelected;
    }

    public void setTreatmentsAvailableOutputSelected(TreatmentsAvailableOutput treatmentsAvailableOutputSelected) {
        this.treatmentsAvailableOutputSelected = treatmentsAvailableOutputSelected;
    }

    public Integer getNumberColumnsPermanent() {
        return numberColumnsPermanent;
    }

    public void setNumberColumnsPermanent(Integer numberColumnsPermanent) {
        this.numberColumnsPermanent = numberColumnsPermanent;
    }

    public Integer getNumberColumnsTemporary() {
        return numberColumnsTemporary;
    }

    public void setNumberColumnsTemporary(Integer numberColumnsTemporary) {
        this.numberColumnsTemporary = numberColumnsTemporary;
    }

    public boolean isVisiblePanelTemporary() {
        return visiblePanelTemporary;
    }

    public void setVisiblePanelTemporary(boolean visiblePanelTemporary) {
        this.visiblePanelTemporary = visiblePanelTemporary;
    }

    public boolean isVisiblePanelPermanent() {
        return visiblePanelPermanent;
    }

    public void setVisiblePanelPermanent(boolean visiblePanelPermanent) {
        this.visiblePanelPermanent = visiblePanelPermanent;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsPermanent() {
        if (this.propertiesTreatmentsPermanent == null) {
            this.propertiesTreatmentsPermanent = new ArrayList<>();
        }
        return propertiesTreatmentsPermanent;
    }

    public void setPropertiesTreatmentsPermanent(List<PropertiesTreatments> propertiesTreatmentsPermanent) {
        this.propertiesTreatmentsPermanent = propertiesTreatmentsPermanent;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsPermanentSelected() {
        if (this.propertiesTreatmentsPermanentSelected == null) {
            this.propertiesTreatmentsPermanentSelected = new ArrayList<>();
        }
        return propertiesTreatmentsPermanentSelected;
    }

    public void setPropertiesTreatmentsPermanentSelected(List<PropertiesTreatments> propertiesTreatmentsPermanentSelected) {
        this.propertiesTreatmentsPermanentSelected = propertiesTreatmentsPermanentSelected;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsTemporary() {
        if (this.propertiesTreatmentsTemporary == null) {
            this.propertiesTreatmentsTemporary = new ArrayList<>();
        }
        return propertiesTreatmentsTemporary;
    }

    public void setPropertiesTreatmentsTemporary(List<PropertiesTreatments> propertiesTreatmentsTemporary) {
        this.propertiesTreatmentsTemporary = propertiesTreatmentsTemporary;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsTemporarySelected() {
        if (this.propertiesTreatmentsTemporarySelected == null) {
            this.propertiesTreatmentsTemporarySelected = new ArrayList<>();
        }
        return propertiesTreatmentsTemporarySelected;
    }

    public void setPropertiesTreatmentsTemporarySelected(List<PropertiesTreatments> propertiesTreatmentsTemporarySelected) {
        this.propertiesTreatmentsTemporarySelected = propertiesTreatmentsTemporarySelected;
    }

    public boolean isDisabledTypeAttention() {
        return disabledTypeAttention;
    }

    public void setDisabledTypeAttention(boolean disabledTypeAttention) {
        this.disabledTypeAttention = disabledTypeAttention;
    }

    public boolean isDisableMedicalOffice() {
        return disableMedicalOffice;
    }

    public void setDisableMedicalOffice(boolean disableMedicalOffice) {
        this.disableMedicalOffice = disableMedicalOffice;
    }

    public List<MedicalOffice> getMedicalOfficeList() {
        return medicalOfficeList;
    }

    public void setMedicalOfficeList(List<MedicalOffice> medicalOfficeList) {
        this.medicalOfficeList = medicalOfficeList;
    }

    public List<TypeAttention> getTypeAttentionList() {
        if (this.typeAttentionList == null) {
            this.typeAttentionList = new ArrayList<>();
        }
        return typeAttentionList;
    }

    public void setTypeAttentionList(List<TypeAttention> typeAttentionList) {
        this.typeAttentionList = typeAttentionList;
    }

    public TypeAttention getTypeAttentionSelected() {
        return typeAttentionSelected;
    }

    public void setTypeAttentionSelected(TypeAttention typeAttentionSelected) {
        this.typeAttentionSelected = typeAttentionSelected;
    }

    public MedicalOffice getMedicalOfficeSelected() {
        return medicalOfficeSelected;
    }

    public void setMedicalOfficeSelected(MedicalOffice medicalOfficeSelected) {
        this.medicalOfficeSelected = medicalOfficeSelected;
    }

    public List<FilterPlanPatientOutput> getFilterPlanPatientList() {
        if (this.filterPlanPatientList == null) {
            this.filterPlanPatientList = new ArrayList<>();
        }
        return filterPlanPatientList;
    }

    public void setFilterPlanPatientList(List<FilterPlanPatientOutput> filterPlanPatientList) {
        this.filterPlanPatientList = filterPlanPatientList;
    }

    public List<Speciality> getSpecialityOfSpecialist() {
        if (this.specialityOfSpecialist == null) {
            this.specialityOfSpecialist = new ArrayList<>();
        }
        return specialityOfSpecialist;
    }

    public void setSpecialityOfSpecialist(List<Speciality> specialityOfSpecialist) {
        this.specialityOfSpecialist = specialityOfSpecialist;
    }

    public FilterPlanPatientOutput getFilterPlanPatientOutput() {
        return filterPlanPatientOutput;
    }

    public void setFilterPlanPatientOutput(FilterPlanPatientOutput filterPlanPatientOutput) {
        this.filterPlanPatientOutput = filterPlanPatientOutput;
    }

    public FilterPlanPatientOutput getFilterPlanPatientOutputSelected() {
        return filterPlanPatientOutputSelected;
    }

    public void setFilterPlanPatientOutputSelected(FilterPlanPatientOutput filterPlanPatientOutputSelected) {
        this.filterPlanPatientOutputSelected = filterPlanPatientOutputSelected;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public Specialists getSelectedSpecialist() {
        return selectedSpecialist;
    }

    public void setSelectedSpecialist(Specialists selectedSpecialist) {
        this.selectedSpecialist = selectedSpecialist;
    }

    public boolean isLabelHistoricalPatient() {
        return labelHistoricalPatient;
    }

    public void setLabelHistoricalPatient(boolean labelHistoricalPatient) {
        this.labelHistoricalPatient = labelHistoricalPatient;
    }

    public boolean isDisabledBtnHistoricalPatient() {
        return disabledBtnHistoricalPatient;
    }

    public void setDisabledBtnHistoricalPatient(boolean disabledBtnHistoricalPatient) {
        this.disabledBtnHistoricalPatient = disabledBtnHistoricalPatient;
    }

    public boolean isLabelSuccessfulMailMessage() {
        return labelSuccessfulMailMessage;
    }

    public void setLabelSuccessfulMailMessage(boolean labelSuccessfulMailMessage) {
        this.labelSuccessfulMailMessage = labelSuccessfulMailMessage;
    }

    public boolean isLabelMailMessageFailed() {
        return labelMailMessageFailed;
    }

    public void setLabelMailMessageFailed(boolean labelMailMessageFailed) {
        this.labelMailMessageFailed = labelMailMessageFailed;
    }

    public List<RegisterKeyOutput> getRegisterKeyOutput() {
        return registerKeyOutput;
    }

    public void setRegisterKeyOutput(List<RegisterKeyOutput> registerKeyOutput) {
        this.registerKeyOutput = registerKeyOutput;
    }

    public String getDateKeyDetails() {
        return dateKeyDetails;
    }

    public void setDateKeyDetails(String dateKeyDetails) {
        this.dateKeyDetails = dateKeyDetails;
    }

    public String getMedicalOfDetails() {
        return medicalOfDetails;
    }

    public void setMedicalOfDetails(String medicalOfDetails) {
        this.medicalOfDetails = medicalOfDetails;
    }

    public List<DateMinHistoricoOutput> getDateMinHistoricoOutput() {
        return dateMinHistoricoOutput;
    }

    public void setDateMinHistoricoOutput(List<DateMinHistoricoOutput> dateMinHistoricoOutput) {
        this.dateMinHistoricoOutput = dateMinHistoricoOutput;
    }

    public Date getMindateHistorico() {
        return mindateHistorico;
    }

    public void setMindateHistorico(Date mindateHistorico) {
        this.mindateHistorico = mindateHistorico;
    }

    public List<HeadboardEmailOutput> getHeadboardEmailOutput() {
        return headboardEmailOutput;
    }
    
    
    
    
    public void setHeadboardEmailOutput(List<HeadboardEmailOutput> headboardEmailOutput) {
        this.headboardEmailOutput = headboardEmailOutput;
    }

    public List<ReadTreatmentsEmailOutput> getReadTreatmentsEmailOutput() {
        return readTreatmentsEmailOutput;
    }

    public void setReadTreatmentsEmailOutput(List<ReadTreatmentsEmailOutput> readTreatmentsEmailOutput) {
        this.readTreatmentsEmailOutput = readTreatmentsEmailOutput;
    }





    @PostConstruct
    public void init() {
        this.setLabelHistoricalPatient(false);
        this.setDisabledBtnAddTreatments(false);
        RequestContext.getCurrentInstance().update("form:labelHistoricalPatient");
        RequestContext.getCurrentInstance().update("form:btnHistoricalPatient");
      
            this.setDisabledPatient(true);
            this.setVisibleSpecialist(true);
        

        RequestContext.getCurrentInstance().update("form:autocompletePatients");
        RequestContext.getCurrentInstance().update("form:panelDataKey");

    }

    /**
     * method that lets you filter plan patient by name or identity card
     *
     * @param query
     * @return
     */
    public List<FilterPlanPatientOutput> filterPlansPatients(String query) {
        this.setFilterPlanPatientList(this.servicePlansPatients.findByIdentityOrName(query,
                this.getSelectedSpecialist().getTypeSpecialist_id(),this.securityViewBean.obtainUser().getId()));
        return this.getFilterPlanPatientList();
    }

    /**
     * Method called when I select a plan patient from the list
     *
     * @param event
     * @throws IOException
     */
    public void onItemSelectPlansPatients(SelectEvent event) throws IOException {
        this.setFilterPlanPatientOutputSelected(new FilterPlanPatientOutput());
        this.setFilterPlanPatientOutputSelected((FilterPlanPatientOutput) event.getObject());
        findHistoricalPatient();
        clearTreatment();
        clearPropertiesTreatment();
        clearListTreatmentSelected();
        findTypeAttention();
        this.findMedicalOffice();
        validateBtnSave();
        if (this.getMedicalOfficeSelected().getId() == null) {
            this.setDisabledBtnSave(true);
        } else {
            this.setDisabledBtnSave(false);
        }
        RequestContext.getCurrentInstance().update("form:btnSaveKey");
    }

    /**
     * method that lets you filter specialist by name or identity card
     *
     * @param query
     * @return
     */
    public List<Specialists> filterSpecialist(String query) {
        this.setListSpecialists(this.serviceSpecialists.findByIdentityOrNameActive(query, this.securityViewBean.obtainUser().getId()));
        return this.getListSpecialists();
    }

    /**
     * Method called when I select a specialist from the list
     *
     * @param event
     * @throws IOException
     */
    public void onItemSelectSpecialist(SelectEvent event) throws IOException {
        this.setSelectedSpecialist(new Specialists());
        this.setSelectedSpecialist((Specialists) event.getObject());
        RequestContext.getCurrentInstance().update("form:speciality");
        clearMedicalOffice();
        findMedicalOffice();
        clearSelectedPatient();
        clearTreatment();
        clearPropertiesTreatment();
        clearListTreatmentSelected();
        clearTypeAttention();
        this.findMedicalOffice();
        validateBtnSave();

    }

    public void selectMedicalOffice() {
        validateBtnSave();
    }

    public void findMedicalOffice() {
        this.setMedicalOfficeList(this.serviceMedicalOffice.findByIdSpecialist(this.getSelectedSpecialist().getId()));
        if (this.getMedicalOfficeList().size() == 1) {
            this.setDisabledPatient(true);
            RequestContext.getCurrentInstance().update("form:autocompletePatients");
            this.setVisibleMedicalOficce(false);
            this.setDisableMedicalOffice(true);
            this.setDisabledPatient(true);
            this.setMedicalOfficeSelected(this.getMedicalOfficeList().get(0));
            RequestContext.getCurrentInstance().update("form:autocompletePatients");
            validateBtnSave();
            if (this.medicalOfficeSelected.equals(null)) {
                this.setDisabledPatient(true);
                RequestContext.getCurrentInstance().update("form:autocompletePatients");
            } else {
                this.setDisabledPatient(false);
                RequestContext.getCurrentInstance().update("form:autocompletePatients");
            }
        } else {
            this.setDisabledPatient(false);
            this.setVisibleMedicalOficce(true);
            this.setDisableMedicalOffice(false);
            validateBtnSave();
            // this.setMedicalOfficeSelected(null);
        }

        RequestContext.getCurrentInstance().update("form:rowSpecialist");
        RequestContext.getCurrentInstance().update("form:medicalOfficeSpecialist");
        RequestContext.getCurrentInstance().update("form:labelMedicalOfficeSpecialist");
        RequestContext.getCurrentInstance().update("form:medicalOfficeName");
        RequestContext.getCurrentInstance().update("form:medicalOfficeAdress");
        RequestContext.getCurrentInstance().update("form:btnSaveKey");

    }

    public void clearMedicalOffice() {
        this.setMedicalOfficeList(new ArrayList<MedicalOffice>());
        this.setVisibleMedicalOficce(false);
        this.setDisableMedicalOffice(false);
        this.setMedicalOfficeSelected(new MedicalOffice());
        RequestContext.getCurrentInstance().update("form:rowSpecialist");
        RequestContext.getCurrentInstance().update("form:medicalOfficeSpecialist");
        RequestContext.getCurrentInstance().update("form:labelMedicalOfficeSpecialist");
        RequestContext.getCurrentInstance().update("form:medicalOfficeName");
        RequestContext.getCurrentInstance().update("form:medicalOfficeAdress");
        RequestContext.getCurrentInstance().update("form");

    }

    public void findTypeAttention() {
        this.setTypeAttentionList(this.serviceTypeAttention.findAll());
        this.setDisabledTypeAttention(false);
        this.setStyleTypeAttention("enabledStyle");
        this.setTypeAttentionSelected(null);
        RequestContext.getCurrentInstance().update("form:typeAttentionName");
        RequestContext.getCurrentInstance().update("form:typeAttentionList");
    }

    public void findPropertiesPermanent(Integer idTreatment) {
        this.setPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsPermanent().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTFOUR.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsPermanent().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTTWO.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsPermanent().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTTRHEE.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsPermanent().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTONE.getValor(), this.getSelectedSpecialist().getId()));
        if (!this.getPropertiesTreatmentsPermanent().isEmpty()) {
            this.setVisiblePanelPermanent(true);
            if (this.getPropertiesTreatmentsPermanent().size() == 1) {
                this.setNumberColumnsPermanent(1);
            } else if ((this.getPropertiesTreatmentsPermanent().size() % 2) == 0) {
                this.setNumberColumnsPermanent(this.getPropertiesTreatmentsPermanent().size() / 2);
            } else {
                this.setNumberColumnsPermanent((this.getPropertiesTreatmentsPermanent().size() / 2) + 1);
            }
        } else {
            this.setVisiblePanelPermanent(false);
        }
    }

    public void findPropertiesTemporary(Integer idTreatment) {
        this.setPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
        this.getPropertiesTreatmentsTemporary().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTEIGHT.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsTemporary().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTSIX.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsTemporary().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTSEVEN.getValor(), this.getSelectedSpecialist().getId()));
        this.getPropertiesTreatmentsTemporary().addAll(0, this.servicePropertiesTreatments.findProTreatByTreatments(
                idTreatment, PropertiesEnums.QUADRANTFIVE.getValor(), this.getSelectedSpecialist().getId()));
        if (!this.getPropertiesTreatmentsTemporary().isEmpty()) {
            this.setVisiblePanelTemporary(true);
            if (this.getPropertiesTreatmentsTemporary().size() == 1) {
                this.setNumberColumnsTemporary(1);
            } else if ((this.getPropertiesTreatmentsTemporary().size() % 2) == 0) {
                this.setNumberColumnsTemporary(this.getPropertiesTreatmentsTemporary().size() / 2);
            } else {
                this.setNumberColumnsTemporary((this.getPropertiesTreatmentsTemporary().size() / 2) + 1);
            }
        } else {
            this.setVisiblePanelTemporary(false);
        }
        this.setDisabledBtnAddTreatments(false);
    }

    public void findPropertiesTreatments() {
        if (this.getTreatmentsAvailableOutputSelected() == null) {
            this.setDisabledBtnAddTreatments(true);
            clearPropertiesTreatment();
        } else {
            Integer idTreatment = this.getTreatmentsAvailableOutputSelected().getIdTreatment();
            findPropertiesPermanent(idTreatment);
            findPropertiesTemporary(idTreatment);
            RequestContext.getCurrentInstance().update("form:propertiesTemporaryKeyRegister");
            RequestContext.getCurrentInstance().update("form:propertiesPermanentKeyRegister");
        }
        RequestContext.getCurrentInstance().update("form:btnAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:cancelAddTreatmentKeys");
    }

    public void findPlansTreatments() {
        clearListTreatmentSelected();
        clearPropertiesTreatment();
        validateBtnSave();
        if (this.getTypeAttentionSelected() == null) {
            clearTreatment();
        } else {
            this.setTreatmentsAvailableOutputList(this.servicePlansTreatments.findTreatmentsAvailableRegister(
                    this.getFilterPlanPatientOutputSelected().getIdPatient(), this.getFilterPlanPatientOutputSelected().
                            getIdPlan(), this.getSelectedSpecialist().getId(), this.getTypeAttentionSelected().getId()));
            verifyListTreatments();
            RequestContext.getCurrentInstance().update("form:treatmentsList");
        }
    }

    public void addTreatment() {
        if (this.getTreatmentsAvailableOutputSelected() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Debe seleccionar un "
                    + "tratamiento"));
            RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");
        } else if (this.isVisiblePanelPermanent() || this.isVisiblePanelTemporary()) {
            if (this.getPropertiesTreatmentsPermanentSelected().isEmpty()
                    && this.getPropertiesTreatmentsTemporarySelected().isEmpty()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error",
                        "Debe seleccionar al menos una pieza para el tratamiento"));
                RequestContext.getCurrentInstance().update("form:growlKeyPatientDetails");
            } else {
                addProperties();
                addTreatmentClear();
            }
        } else {
            this.getTreatmentsAvailableOutputSelected().setPriceAcumulate(this.getTreatmentsAvailableOutputSelected()
                    .getPrice());
            addTreatmentClear();
        }
    }

    public void addProperties() {
        Double price1 = 0.0;
        Double price2 = 0.0;
        if (!this.getPropertiesTreatmentsPermanentSelected().isEmpty()) {
            this.setVisibleColumnPropertiesPermanent(true);
            this.getTreatmentsAvailableOutputSelected().setNumberColumnPermanent(
                    this.getNumberColumnsPermanent());
            this.getTreatmentsAvailableOutputSelected().setVisiblePanelPermanent(true);
            this.getTreatmentsAvailableOutputSelected().setPropertiesTreatmentsPermanent(
                    this.getPropertiesTreatmentsPermanent());
            this.getTreatmentsAvailableOutputSelected().setPropertiesTreatmentsPermanentSelected(
                    this.getPropertiesTreatmentsPermanentSelected());
            price1 = this.getTreatmentsAvailableOutputSelected().getPropertiesTreatmentsPermanentSelected().
                    size() * this.getTreatmentsAvailableOutputSelected().getPrice();
            RequestContext.getCurrentInstance().update("form:columnPermanentPropertie");
            RequestContext.getCurrentInstance().update("form:panelPermanentPropertie");
        }
        if (!this.getPropertiesTreatmentsTemporarySelected().isEmpty()) {
            this.setVisibleColumnPropertiesTemporary(true);
            this.getTreatmentsAvailableOutputSelected().setNumberColumnTemporary(this.getNumberColumnsTemporary());
            this.getTreatmentsAvailableOutputSelected().setVisiblePanelTemporary(true);
            this.getTreatmentsAvailableOutputSelected().setPropertiesTreatmentsTemporary(this.
                    getPropertiesTreatmentsTemporary());
            this.getTreatmentsAvailableOutputSelected().setPropertiesTreatmentsTemporarySelected(
                    this.getPropertiesTreatmentsTemporarySelected());
            price2 = this.getTreatmentsAvailableOutputSelected().getPropertiesTreatmentsTemporarySelected().
                    size() * this.getTreatmentsAvailableOutputSelected().getPrice();
            RequestContext.getCurrentInstance().update("form:columnTemporaryPropertie");
            RequestContext.getCurrentInstance().update("form:panelTemporaryPropertie");
        }
        this.getTreatmentsAvailableOutputSelected().setPriceAcumulate(price1 + price2);
    }

    public void addTreatmentClear() {
        this.getListPlansTreatSelected().add(0, this.getTreatmentsAvailableOutputSelected());
        //validates whether the treatment is coronal or Periapical to remain available in the list of treatments
        if (!this.getTreatmentsAvailableOutputSelected().getIdTreatment().equals(
                TreatmentsRepeat.RADIOGRAFIA_CORONAL.getValor()) && !this.getTreatmentsAvailableOutputSelected().
                getIdTreatment().equals(TreatmentsRepeat.RADIOGRAFIA_PERIAPICAL.getValor())) {
            this.getTreatmentsAvailableOutputList().remove(this.getTreatmentsAvailableOutputSelected());
        }
        verifyListTreatments();
        this.setTreatmentsAvailableOutputSelected(null);
        this.setDisabledBtnAddTreatments(true);
        clearPropertiesTreatment();
        validateBtnSave();
        validateInputTreatment();
        RequestContext.getCurrentInstance().update("form:btnAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:cancelAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:treatmentsList");
        RequestContext.getCurrentInstance().update("form:planTreatList");

    }

    public void verifyListTreatments() {
        if (!this.getTreatmentsAvailableOutputList().isEmpty()) {
            this.setDisabledListTreatments(false);
            this.setMessageEmpty("SELECCIONAR TRATAMIENTO");
            this.setStyleTreatments("enabledStyle");
        } else {
            this.setMessageEmpty("NO POSEE TRATAMIENTOS DISPONIBLES");
            this.setDisabledListTreatments(true);
            this.setStyleTreatments("disabledStyle");
        }
    }

    public void calculatePrice(Integer index) {
        Double price1 = 0.0;
        Double price2 = 0.0;
        RequestContext.getCurrentInstance().update("form:chekPropertiesTemporary");
        if (this.getListPlansTreatSelected().get(index).isVisiblePanelPermanent()) {
            price1 = this.getListPlansTreatSelected().get(index).getPropertiesTreatmentsPermanentSelected().size()
                    * this.getListPlansTreatSelected().get(index).getPrice();
        }
        if (this.getListPlansTreatSelected().get(index).isVisiblePanelTemporary()) {
            price2 = this.getListPlansTreatSelected().get(index).getPropertiesTreatmentsTemporarySelected().size()
                    * this.getListPlansTreatSelected().get(index).getPrice();
        }
        if (price1 == 0.00 && price2 == 0.00) {
            this.setDisabledBtnSave(true);
        } else {
            this.setDisabledBtnSave(false);
        }
        this.getListPlansTreatSelected().get(index).setPriceAcumulate(price1 + price2);
        RequestContext.getCurrentInstance().update("form:planTreatList");
        RequestContext.getCurrentInstance().update("form:scaleTratments");
        RequestContext.getCurrentInstance().update("form:btnSaveKey");

    }

    public void clearTreatment() {
        this.setTreatmentsAvailableOutputList(new ArrayList<TreatmentsAvailableOutput>());
        this.setTreatmentsAvailableOutputSelected(null);
        this.setDisabledListTreatments(true);
        this.setDisabledBtnAddTreatments(true);
        this.setStyleTreatments("enabledStyle");
        RequestContext.getCurrentInstance().update("form:btnAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:cancelAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:treatmentsList");
        clearPropertiesTreatment();
    }

    public void clearPropertiesTreatment() {
        this.setVisiblePanelPermanent(false);
        this.setVisiblePanelTemporary(false);
        this.setPropertiesTreatmentsPermanent(new ArrayList<PropertiesTreatments>());
        this.setPropertiesTreatmentsTemporary(new ArrayList<PropertiesTreatments>());
        this.setPropertiesTreatmentsPermanentSelected(new ArrayList<PropertiesTreatments>());
        this.setPropertiesTreatmentsTemporarySelected(new ArrayList<PropertiesTreatments>());
        RequestContext.getCurrentInstance().update("form:panelPropertiesTreatmentsKeys");
        RequestContext.getCurrentInstance().update("form:permanentTeethKeysRegister");
        RequestContext.getCurrentInstance().update("form:temporaryTeehtKeyRegister");
    }

    public void clearSelectedPatient() {
        this.setFilterPlanPatientOutputSelected(null);
//        this.setDisabledHistoricalPatient(true);
//        this.setDisabledBtnHistoricalPatient(true);
//        this.setLabelHistoricalPatient(true);
        AutoComplete autoComplete = (AutoComplete) FacesContext.getCurrentInstance().getViewRoot().findComponent(
                "form:autocompletePatients");
        autoComplete.setItemLabel("");
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:autocompletePatients");
        requestContext.update("form:autocompletePatients");
        requestContext.update("form:company");
        requestContext.update("form:planPatient");
        requestContext.update("form:insurance");
        requestContext.update("form:relationShip");
        requestContext.update("form:namePatient");
        requestContext.update("form:identityNumberPatient");
//        requestContext.update("form:btnHistoricalPatient");
//        requestContext.update("form:labelHistoricalPatient");
    }

    public void clearAutocompletePatient() {
        this.setDisabledPatient(true);
        RequestContext.getCurrentInstance().update("form:autocompletePatients");
    }

    public void clearTypeAttention() {
        this.setDisabledTypeAttention(true);
        this.setTypeAttentionSelected(null);
        this.setStyleTypeAttention("enabledStyle");
        RequestContext.getCurrentInstance().update("form:typeAttentionName");
        RequestContext.getCurrentInstance().update("form:typeAttentionList");
    }

    public void clearListTreatmentSelected() {
        this.setListPlansTreatSelected(new ArrayList<TreatmentsAvailableOutput>());
        this.setVisibleColumnPropertiesPermanent(false);
        this.setVisibleColumnPropertiesTemporary(false);
        RequestContext.getCurrentInstance().update("form:planTreatList");
    }

    public void resetListTreatmentSelected() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:planTreatList");
        if (dataTable != null) {
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:planTreatList");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:planTreatList");
        }
    }

    public void validateInputTreatment() {
        if (this.getListPlansTreatSelected().isEmpty()) {
            this.setDisabledInputTreatment(true);
        } else {
            this.setDisabledInputTreatment(false);
        }
        RequestContext.getCurrentInstance().update("form:columnInputTreatment");
    }

    public void validateBtnSave() {
        if (this.getSelectedSpecialist() != null && this.getFilterPlanPatientOutputSelected() != null
                && this.getMedicalOfficeSelected() != null) {
            this.setDisabledBtnSave(false);
        } else {
            this.setDisabledBtnSave(true);
        }

        RequestContext.getCurrentInstance().update("form:btnSaveKey");

    }

    public void deleteTratment(Integer index) {
        this.getListPlansTreatSelected().get(index).setPropertiesTreatmentsPermanent(
                new ArrayList<PropertiesTreatments>());
        this.getListPlansTreatSelected().get(index).setPropertiesTreatmentsPermanentSelected(
                new ArrayList<PropertiesTreatments>());
        this.getListPlansTreatSelected().get(index).setPropertiesTreatmentsTemporary(
                new ArrayList<PropertiesTreatments>());
        this.getListPlansTreatSelected().get(index).setPropertiesTreatmentsTemporarySelected(
                new ArrayList<PropertiesTreatments>());
        if (!this.getTreatmentsAvailableOutputList().contains(this.getListPlansTreatSelected().get(index))) {
            this.getTreatmentsAvailableOutputList().add(this.getListPlansTreatSelected().get(index));
        }
        this.getListPlansTreatSelected().remove(this.getListPlansTreatSelected().get(index));
        orderListTreatment();
        verifyListTreatments();
        RequestContext.getCurrentInstance().update("form:planTreatList");
        RequestContext.getCurrentInstance().update("form:treatmentsList");
        validateBtnSave();
        validateInputTreatment();
    }

    public void orderListTreatment() {
        Collections.sort(this.getTreatmentsAvailableOutputList(),
                new Comparator<TreatmentsAvailableOutput>() {

                    @Override
                    public int compare(TreatmentsAvailableOutput o1, TreatmentsAvailableOutput o2
                    ) {
                        return o1.getDescriptionTreatment().compareTo(o2.getDescriptionTreatment());
                    }

                }
        );
    }

    public void clearSpecialist() {
//        this.setSelectedSpecialist(null);
        this.setMedicalOfficeList(new ArrayList<MedicalOffice>());
        this.setMedicalOfficeSelected(null);
        this.setDisableMedicalOffice(true);
        AutoComplete autoComplete = (AutoComplete) FacesContext.getCurrentInstance().getViewRoot().findComponent(
                "form:autocompleteSpecialists");
        autoComplete.setItemLabel("");
        RequestContext.getCurrentInstance().update("form:autocompleteSpecialists");
        RequestContext.getCurrentInstance().update("form:medicalOfficeSpecialist");
        RequestContext.getCurrentInstance().update("form:medicalOfficeName");
        RequestContext.getCurrentInstance().update("form:medicalOfficeAdress");
        RequestContext.getCurrentInstance().update("form:autocompleteSpecialists");
        RequestContext.getCurrentInstance().update("form:specialist");
        RequestContext.getCurrentInstance().update("form:speciality");

    }

    public void clearGeneral() {
        clearSpecialist();
        clearSelectedPatient();
        clearTypeAttention();
        clearTreatment();
        clearPropertiesTreatment();
        clearListTreatmentSelected();
        clearAutocompletePatient();
        validateBtnSave();
        RequestContext.getCurrentInstance().update("form:panelDataKey");
        RequestContext.getCurrentInstance().reset("form");
    }

    public void findDetailsKey(Integer idKey) throws ParseException {
        this.setKeysDetailsOutput(this.serviceKeysPatients.findDetails(idKey));
        String baseDate= this.getKeysDetailsOutput().getDateKey();
        String day = baseDate.substring(8,10);
        String month=baseDate.substring(5, 7);
        String year=baseDate.substring(0,4);
        String hrs=baseDate.substring(11,19);
        this.setDateKeyDetails(day+"/"+month+"/"+year+" "+hrs);
//        String medicalOf= this.getKeysDetailsOutput().getNameMedicalOffice().substring(16);
//        this.setMedicalOfDetails(medicalOf);

        RequestContext.getCurrentInstance().update("form:panelKeyConsult");
    }

    public void findHistoricalPatient() {
        this.setHistoricalPatientGCOutputFilter(null);
        this.setDateMinHistoricoOutput(this.serviceKeysPatients.findDateMinHistorico(this.
                getFilterPlanPatientOutputSelected().getIdPatient(), this.getSelectedSpecialist().getId()));
        this.setHistoricalPatientGCOutput(this.serviceKeysPatients.findHistoricalPatientRegister(this.
                getFilterPlanPatientOutputSelected().getIdPatient(), this.getSelectedSpecialist().getId()));
        if (!this.getHistoricalPatientGCOutput().isEmpty()) {
            this.setDisabledHistoricalPatient(true);
            this.setDisabledBtnHistoricalPatient(false);
            this.setLabelHistoricalPatient(false);
            RequestContext.getCurrentInstance().update("form:keyPatientHistorical");
            RequestContext.getCurrentInstance().update("form:btnHistoricalPatient");
            RequestContext.getCurrentInstance().update("form:panelDataKey");
            RequestContext.getCurrentInstance().update("form:labelHistoricalPatient");
        } else {
            this.setDisabledHistoricalPatient(false);
            this.setLabelHistoricalPatient(true);
            this.setDisabledBtnHistoricalPatient(false);
            RequestContext.getCurrentInstance().update("form:keyPatientHistorical");
            RequestContext.getCurrentInstance().update("form:labelHistoricalPatient");
            RequestContext.getCurrentInstance().update("form:panelDataKey");
            RequestContext.getCurrentInstance().update("form:btnHistoricalPatient");

        }

        for (DateMinHistoricoOutput dateMin: this.getDateMinHistoricoOutput()) {
            this.setMindateHistorico(dateMin.getDateMin());


        }

    }

    /**
     * Method that is executed when a date is selected in the filter key
     *
     * @param event
     */
    public void handleDateSelect(SelectEvent event) {
        RequestContext.getCurrentInstance().execute("PF('wigetHistorical').filter()");
    }

    /**
     * Date method that allows a date to the form dd/mm/yyyy
     *
     * @param date
     * @return Date
     * @throws ParseException
     */
    public Date formatDate(Date date) throws ParseException {
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;
    }

    public void resetHistorical() {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().
                findComponent("form:keyPatientHistorical");
        if (dataTable != null) {
            this.setDateFilter(null);
            findHistoricalPatient();
            dataTable.reset();
            RequestContext.getCurrentInstance().reset("form:keyPatientHistorical");
            dataTable.getFilters().clear();
            RequestContext.getCurrentInstance().update("form:keyPatientHistorical");
        }

    }

    public void clearTreatmentAndPropertie() {
        this.setTreatmentsAvailableOutputSelected(null);
        this.setDisabledBtnAddTreatments(true);
        this.setStyleTreatments("enabledStyle");
        RequestContext.getCurrentInstance().update("form:btnAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:cancelAddTreatmentKeys");
        RequestContext.getCurrentInstance().update("form:treatmentsList");
        clearPropertiesTreatment();

    }


}