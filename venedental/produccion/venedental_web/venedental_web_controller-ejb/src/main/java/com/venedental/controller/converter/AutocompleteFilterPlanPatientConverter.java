/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.converter;

import com.venedental.controller.view.KeysPatientsRegisterViewBean;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author akdeskdev90
 */
@FacesConverter("autocompleteFilterPlanPatientConverter")
public class AutocompleteFilterPlanPatientConverter implements Converter {
    
    @ManagedProperty(value = "#{keysPatientsRegisterViewBean}")
    private KeysPatientsRegisterViewBean keysPatientsRegisterViewBean;

    private Boolean encontrado = false;

    public KeysPatientsRegisterViewBean getKeysPatientstRegisterViewBean() {
        return keysPatientsRegisterViewBean;
    }

    public void setKeysPatientstRegisterViewBean(KeysPatientsRegisterViewBean keysPatientsRegisterViewBean) {
        this.keysPatientsRegisterViewBean = keysPatientsRegisterViewBean;
    }
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        int posicion = 0;

        if (value != null && value.trim().length() > 0) {
            try {

                encontrado = false;

                KeysPatientsRegisterViewBean keysPatientsRegisterViewBean = (KeysPatientsRegisterViewBean) context.getApplication().getELResolver().getValue(context.getELContext(), null, "keysPatientsRegisterViewBean");
                int numero = Integer.parseInt(value);
                int j = 0;

                for (int i = 0; i < keysPatientsRegisterViewBean.getFilterPlanPatientList().size(); i++) {

                    FilterPlanPatientOutput s = keysPatientsRegisterViewBean.getFilterPlanPatientList().get(i);
                    if (s.getIdLine() == numero) {
                        posicion = j;
                    }

                    j++;
                }

                return keysPatientsRegisterViewBean.getFilterPlanPatientList().get(posicion);

            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        } else {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof FilterPlanPatientOutput) {
            FilterPlanPatientOutput filterPlanPatientOutput = (FilterPlanPatientOutput) value;
            return filterPlanPatientOutput.getIdLine().toString();
        }
        return "";
    }
    
}