package com.venedental.controller.view;

import com.venedental.controller.BaseBean;
import com.venedental.controller.request.AccionesRequestBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.controller.session.ErrorHttpSessionBean;
import com.venedental.dto.consultKeys.ConsultDateMinDetalleOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsOutput;
import com.venedental.dto.consultKeys.LoadKeyStatesOutput;
import com.venedental.dto.consultKeys.ConsultUserRolesOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import com.venedental.dto.keyPatients.KeysConsultDateMinOutput;
import com.venedental.dto.keyPatients.DateMinHistoricoOutput;
import com.venedental.dto.keyPatients.HeadboardEmailOutput;
import com.venedental.dto.keyPatients.ReadTreatmentsEmailOutput;
import com.venedental.enums.EstatusKeysPatients;
import com.venedental.enums.TypeScale;
import com.venedental.enums.EstatusPlansTreatmentsKeys;
import com.venedental.facade.BinnaclePlansTreatmentsKeysFacadeLocal;
import com.venedental.facade.InsurancesPatientsFacadeLocal;
import com.venedental.facade.KeysPatientsFacadeLocal;
import com.venedental.facade.MenuFacadeLocal;
import com.venedental.facade.MenuRolFacadeLocal;
import com.venedental.facade.MenuRolOperationFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.PlansTreatmentsFacadeLocal;
import com.venedental.facade.PlansTreatmentsKeysFacadeLocal;
import com.venedental.facade.PropertiesPlansTreatmentsKeyFacadeLocal;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.facade.ScaleTreatmentsFacadeLocal;
import com.venedental.facade.ScaleTreatmentsSpecialistFacadeLocal;
import com.venedental.facade.SpecialistsFacadeLocal;
import com.venedental.facade.StatusAnalysisKeysFacadeLocal;
import com.venedental.model.BinnaclePlansTreatmentsKeys;
import com.venedental.model.InsurancesPatients;
import com.venedental.model.KeysPatients;
import com.venedental.model.Patients;
import com.venedental.model.PlansPatients;
import com.venedental.model.PlansTreatments;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import com.venedental.model.PropertiesTreatments;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.Specialists;
import com.venedental.model.StatusAnalysisKeys;
import com.venedental.model.TypeAttention;
import com.venedental.model.TypeSpecialist;
import com.venedental.model.security.Menu;
import com.venedental.model.security.MenuRol;
import com.venedental.model.security.MenuRolOperation;
import com.venedental.model.security.Users;
import com.venedental.services.ServiceAnalysisKeys;
import com.venedental.services.ServiceKeysPatients;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import services.utils.Transformar;

@ManagedBean(name = "keysPatientsViewBean")
@ViewScoped
public class KeysPatientsViewBean extends BaseBean {

    private final RequestContext context = RequestContext.getCurrentInstance();

    @EJB
    private ServiceKeysPatients serviceKeysPatients;

    @EJB
    private KeysPatientsFacadeLocal keysFacadeLocal;

    @EJB
    private PlansTreatmentsFacadeLocal plansTreatmentsFacadeLocal;

    @EJB
    private PlansTreatmentsKeysFacadeLocal plansTreatmentsKeysFacadeLocal;

    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;

    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;

    @EJB
    private InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal;

    @EJB
    private ScaleTreatmentsSpecialistFacadeLocal scaleTreatmentsSpecialistFacadeLocal;

    @EJB
    private ScaleTreatmentsFacadeLocal scaleTreatmentsFacadeLocal;

    @EJB
    private PropertiesPlansTreatmentsKeyFacadeLocal propertiesPlansTreatmentsKeyFacadeLocal;

    @EJB
    private MenuRolFacadeLocal menuRolFacadeLocal;

    @EJB
    private MenuFacadeLocal menuFacadeLocal;

    @EJB
    private SpecialistsFacadeLocal specialistFacadeLocal;

    @EJB
    private MenuRolOperationFacadeLocal menuRolOperationFacadeLocal;

    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;

    @EJB
    private BinnaclePlansTreatmentsKeysFacadeLocal binnaclePlansTreatmentsKeysFacade;

    @EJB
    private StatusAnalysisKeysFacadeLocal statusAnalysisKeysFacadeLocal;

    @ManagedProperty(value = "#{errorHttpSessionBean}")
    private ErrorHttpSessionBean errorHttpSessionBean;

    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{plansTreatmentsKeysViewBean}")
    private PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean;

    @ManagedProperty(value = "#{specialistsViewBean}")
    private SpecialistsViewBean specialistsViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;

    @ManagedProperty(value = "#{plansPatientsViewBean}")
    private PlansPatientsViewBean plansPatientsViewBean;

    @ManagedProperty(value = "#{typeSpecialistViewBean}")
    private TypeSpecialistViewBean typeSpecialistViewBean;

    @ManagedProperty(value = "#{plansTreatmentsViewBean}")
    private PlansTreatmentsViewBean plansTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;

    @ManagedProperty(value = "#{propertiesPlansTreatmentsKeyViewBean}")
    private PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean;

    @ManagedProperty(value = "#{specialityViewBean}")
    private SpecialityViewBean specialityViewBean;

    @ManagedProperty(value = "#{scaleTreatmentsSpecialistViewBean}")
    private ScaleTreatmentsSpecialistViewBean scaleTreatmentsSpecialistViewBean;

    @ManagedProperty(value = "#{securityViewBean}")
    private SecurityViewBean securityViewBean;

    @ManagedProperty(value = "#{typeAttentionViewBean}")
    private TypeAttentionViewBean typeAttentionViewBean;

    private Boolean operationCreate, operationRead, operationUpdate, operationDelete, operationAddTreatment, operationDeleteTreatment, operationSaveKey;

    private Boolean dataSpecialists, disabledBtnBuscar, disabledBtnRest,disableBtnGuardar,messageDataTable;

    private Date startDate, endDate, maxDate, dateSelect, startDateConsult, expirationKey,dateMin, dateMinSql,dateMinDetalle;

    private Date endDateConsult, dateKeyDetail, dateExpirationDetail, dateBrinthaDetail, birthdateP, dateFilter;

    private Specialists specialist;

    private Integer idKeyDelete, idStatusKey,reasonDeleteOpen;

    private String obserDelete,dateMinCalendar;

    private String[] reasonDelete;

    private String observation;

    private String styleClassOdontodiagramI;

    private String styleClassOdontodiagramII;

    private String dueDate, queryPatient, querySpecialist,nameTreatment;

    private String numberKey, key, nameP, nameS, indetityNumberS, identityNumberP, dateKey, speciality, planName,enteName,consultorioName,aseguradoraName,parentesco;

    private Boolean create, disableBtnConsult, disableBtnDelete,inputDateEnd;

    private Boolean disabledColumState, disabledColumDateKey, disabledColumKey,
            disabledColumPatient, disabledColumNumberP, disabledColumNumberS, disabledColumSpecialist, disabledColumObservation;

    private List<ConsultKeysPatientsOutput> consultKeysPatientsOutput;
    
    private List<ConsultKeysPatientsOutput> consultKeysPatientsOutputFilter;

    private LoadKeyStatesOutput loadKeyStatesOutputselect;

    private List<LoadKeyStatesOutput> loadKeyStatesOutput;
    
    private List<LoadKeyStatesOutput> loadKeyStatesOutputComplement;

    private List<ConsultKeysPatientsDetailOutput> consultKeysPatientsDetailOutput;

    private List<ConsultKeysPatientsDetailTreatmentOutput> consultKeysPatientsDetailTreatmentOutput;
    
    private List<ConsultDateMinDetalleOutput> consultDateMinDetalleOutput;

    private List<ConsultUserRolesOutput> consultUserRolesOutput;
    
    
    private KeysConsultDateMinOutput keysConsultDateMinOutput;
    
    private DateMinHistoricoOutput dateMinHistoricoOutput;

    private List<KeysPatients> listaKeys = new ArrayList<>();

    private List<InsurancesPatients> listaInsurePatients;

    private List<KeysPatients> listaKeysFiltrada = new ArrayList<>();

    private List<KeysPatients> listHistoryKeys;

    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysList;

    private boolean visible;

    private KeysPatients keysPatientsNew;

    private boolean calendarHasta;

    private KeysPatients keysPatientsEdit;

    private KeysPatients keysPatientsHistory;

    private TypeAttention newType;

    private boolean createKeyDisable;

    private boolean plan = false;

    private boolean deleteKey = false;
    
    

    public boolean isPlan() {
        return plan;
    }

    public void setPlan(boolean plan) {
        this.plan = plan;
    }

    public TypeAttention getNewType() {
        return newType;
    }

    public void setNewType(TypeAttention newType) {
        this.newType = newType;
    }

    public Boolean getDisabledBtnRest() {
        return disabledBtnRest;
    }

    public void setDisabledBtnRest(Boolean disabledBtnRest) {
        this.disabledBtnRest = disabledBtnRest;
    }

    public KeysPatientsViewBean() {
        this.visible = false;
        this.create = true;
        this.createKeyDisable = true;
        this.reasonDelete = new String[2];

    }

    @PostConstruct
    public void init() {
        stateKeys();
        this.calendarHasta = true;
        this.componenteSessionBean.setCamposRequeridosCreate(false);
        this.componenteSessionBean.setCamposRequeridosEdit(false);
        this.setInputDateEnd(true);
        this.obtainUserSpecialists();
        this.operationSecurity();
        this.getPlansTreatmentsViewBean().getAuxListPlansTreatments().clear();
        this.setDisabledBtnBuscar(true);
        this.setDisabledBtnRest(true);
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        this.setDisableBtnGuardar(true);
        this.dateMin();
        this.setMessageDataTable(true);

    }

    public KeysPatients getKeysPatientsHistory() {
        return keysPatientsHistory;
    }

    public void setKeysPatientsHistory(KeysPatients keysPatientsHistory) {

        if (keysPatientsHistory != null && keysPatientsHistory.getId() != null) {

            this.getPlansPatientsViewBean().setNewListPlansPatients(new ArrayList<PlansPatients>());

            this.getPlansPatientsViewBean().setNewListPlansPatients(this.plansPatientsFacadeLocal.findByPatientsIdAndTypeSpecialist(keysPatientsHistory.getPatientsId().getId(), keysPatientsHistory.getSpecialistId().getTypeSpecialistid().getId()));

            this.getPlansPatientsViewBean().setEditPlansPatients(this.getPlansPatientsViewBean().getNewListPlansPatients().get(0));

            //Buscar planesTreatments and exclude the treatments with status eliminado 
            StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
            List<PlansTreatmentsKeys> beforeListPlansTreatmentsKeys;
            beforeListPlansTreatmentsKeys = new ArrayList<PlansTreatmentsKeys>();
            beforeListPlansTreatmentsKeys = this.plansTreatmentsKeysFacadeLocal.findByKeysAndExcludeStatus(keysPatientsHistory.getId(), status.getId());

            for (PlansTreatmentsKeys plansTreatmentsKeys : beforeListPlansTreatmentsKeys) {
                plansTreatmentsKeys.setComponentTooth(true);

                RequestContext.getCurrentInstance().update("form:lstTreatmentIIH:permanentTeethXLH");
                if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList().size() > 0) {
                    this.getPropertiesTreatmentsViewBean().setVisibleListKeysHistory(true);

                    this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getId()));
                    this.getPropertiesTreatmentsViewBean().limpiarLista();
                    cleanerListProperties(plansTreatmentsKeys);
                    // this.getPropertiesTreatmentsViewBean().fillListProperties(this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments());
                    this.fillListPropertiesNew(plansTreatmentsKeys);
                    this.fillListPropertiesFilter(plansTreatmentsKeys);

                }
            }
        }

        this.keysPatientsHistory = keysPatientsHistory;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

    public String[] getReasonDelete() {
        return reasonDelete;
    }

    public void setReasonDelete(String[] reasonDelete) {
        if (reasonDelete == null) {
            reasonDelete[1] = "";
        }
        this.reasonDelete = reasonDelete;
    }

    public boolean getCreateKeyDisable() {
        return createKeyDisable;
    }

    public void setCreateKeyDisable(boolean createKeyDisable) {
        this.createKeyDisable = createKeyDisable;
    }

    public boolean isCalendarHasta() {
        return calendarHasta;
    }

    public void setCalendarHasta(boolean calendarHasta) {
        this.calendarHasta = calendarHasta;
    }

    public Date getDateSelect() {
        return dateSelect;
    }

    public void setDateSelect(Date dateSelect) {
        this.dateSelect = dateSelect;
    }

    public SpecialistsFacadeLocal getSpecialistFacadeLocal() {
        return specialistFacadeLocal;
    }

    public void setSpecialistFacadeLocal(SpecialistsFacadeLocal specialistFacadeLocal) {
        this.specialistFacadeLocal = specialistFacadeLocal;
    }

    public Specialists getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Specialists specialist) {
        this.specialist = specialist;
    }

    public Boolean getDataSpecialists() {
        return dataSpecialists;
    }

    public void setDataSpecialists(Boolean dataSpecialists) {
        this.dataSpecialists = dataSpecialists;
    }

    public Integer getIdKeyDelete() {
        return idKeyDelete;
    }

    public void setIdKeyDelete(Integer idKeyDelete) {
        this.idKeyDelete = idKeyDelete;
    }

    public Date getMaxDate() throws ParseException {
        maxDate = Transformar.currentDate();
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public String getStyleClassOdontodiagramI() {
        return styleClassOdontodiagramI;
    }

    public void setStyleClassOdontodiagramI(String styleClassOdontodiagramI) {
        this.styleClassOdontodiagramI = styleClassOdontodiagramI;
    }

    public String getStyleClassOdontodiagramII() {
        return styleClassOdontodiagramII;
    }

    public void setStyleClassOdontodiagramII(String styleClassOdontodiagramII) {
        this.styleClassOdontodiagramII = styleClassOdontodiagramII;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNameS() {
        return nameS;
    }

    public void setNameS(String nameS) {
        this.nameS = nameS;
    }

    public String getIndetityNumberS() {
        return indetityNumberS;
    }

    public void setIndetityNumberS(String indetityNumberS) {
        this.indetityNumberS = indetityNumberS;
    }

    public String getIdentityNumberP() {
        return identityNumberP;
    }

    public void setIdentityNumberP(String identityNumberP) {
        this.identityNumberP = identityNumberP;
    }

    public String getNameP() {
        return nameP;
    }

    public void setNameP(String nameP) {
        this.nameP = nameP;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Date getExpirationKey() {
        return expirationKey;
    }

    public void setExpirationKey(Date expirationKey) {
        this.expirationKey = expirationKey;
    }

    public Date getBirthdateP() {
        return birthdateP;
    }

    public void setBirthdateP(Date birthdateP) {
        this.birthdateP = birthdateP;
    }

    public Date getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(Date dateFilter) {
        this.dateFilter = dateFilter;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Date getDateKeyDetail() {
        return dateKeyDetail;
    }

    public void setDateKeyDetail(Date dateKeyDetail) {
        this.dateKeyDetail = dateKeyDetail;
    }

    public Date getDateExpirationDetail() {
        return dateExpirationDetail;
    }

    public void setDateExpirationDetail(Date dateExpirationDetail) {
        this.dateExpirationDetail = dateExpirationDetail;
    }

    public Date getDateBrinthaDetail() {
        return dateBrinthaDetail;
    }

    public void setDateBrinthaDetail(Date dateBrinthaDetail) {
        this.dateBrinthaDetail = dateBrinthaDetail;
    }

    public SpecialityViewBean getSpecialityViewBean() {
        return specialityViewBean;
    }

    public void setSpecialityViewBean(SpecialityViewBean specialityViewBean) {
        this.specialityViewBean = specialityViewBean;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDateMin() {
        return dateMin;
    }

    public void setDateMin(Date dateMin) {
        this.dateMin = dateMin;
    }
    
    

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDateConsult() {
        return startDateConsult;
    }

    public void setStartDateConsult(Date startDateConsult) {
        this.startDateConsult = startDateConsult;
    }

    public Date getEndDateConsult() {
        return endDateConsult;
    }

    public void setEndDateConsult(Date endDateConsult) {
        this.endDateConsult = endDateConsult;
    }

    public Boolean getDisabledBtnBuscar() {
        return disabledBtnBuscar;
    }

    public void setDisabledBtnBuscar(Boolean disabledBtnBuscar) {
        this.disabledBtnBuscar = disabledBtnBuscar;
    }

    public boolean isDeleteKey() {
        return deleteKey;
    }

    public void setDeleteKey(boolean deleteKey) {
        this.deleteKey = deleteKey;
    }

    public Boolean getDisableBtnConsult() {
        return disableBtnConsult;
    }

    public void setDisableBtnConsult(Boolean disableBtnConsult) {
        this.disableBtnConsult = disableBtnConsult;
    }

    public PropertiesPlansTreatmentsKeyViewBean getPropertiesPlansTreatmentsKeyViewBean() {
        return propertiesPlansTreatmentsKeyViewBean;
    }

    public void setPropertiesPlansTreatmentsKeyViewBean(PropertiesPlansTreatmentsKeyViewBean propertiesPlansTreatmentsKeyViewBean) {
        this.propertiesPlansTreatmentsKeyViewBean = propertiesPlansTreatmentsKeyViewBean;
    }

    public PropertiesTreatmentsViewBean getPropertiesTreatmentsViewBean() {
        return propertiesTreatmentsViewBean;
    }

    public InsurancesPatientsFacadeLocal getInsurancesPatientsFacadeLocal() {
        return insurancesPatientsFacadeLocal;
    }

    public void setInsurancesPatientsFacadeLocal(InsurancesPatientsFacadeLocal insurancesPatientsFacadeLocal) {
        this.insurancesPatientsFacadeLocal = insurancesPatientsFacadeLocal;
    }

    public void setPropertiesTreatmentsViewBean(PropertiesTreatmentsViewBean propertiesTreatmentsViewBean) {
        this.propertiesTreatmentsViewBean = propertiesTreatmentsViewBean;
    }

    public PlansTreatmentsViewBean getPlansTreatmentsViewBean() {
        return plansTreatmentsViewBean;
    }

    public MenuRolFacadeLocal getMenuRolFacadeLocal() {
        return menuRolFacadeLocal;
    }

    public void setMenuRolFacadeLocal(MenuRolFacadeLocal menuRolFacadeLocal) {
        this.menuRolFacadeLocal = menuRolFacadeLocal;
    }

    public MenuFacadeLocal getMenuFacadeLocal() {
        return menuFacadeLocal;
    }

    public void setMenuFacadeLocal(MenuFacadeLocal menuFacadeLocal) {
        this.menuFacadeLocal = menuFacadeLocal;
    }

    public MenuRolOperationFacadeLocal getMenuRolOperationFacadeLocal() {
        return menuRolOperationFacadeLocal;
    }

    public void setMenuRolOperationFacadeLocal(MenuRolOperationFacadeLocal menuRolOperationFacadeLocal) {
        this.menuRolOperationFacadeLocal = menuRolOperationFacadeLocal;
    }

    public PatientsFacadeLocal getPatientsFacadeLocal() {
        return patientsFacadeLocal;
    }

    public void setPatientsFacadeLocal(PatientsFacadeLocal patientsFacadeLocal) {
        this.patientsFacadeLocal = patientsFacadeLocal;
    }

    public SecurityViewBean getSecurityViewBean() {
        return securityViewBean;
    }

    public void setSecurityViewBean(SecurityViewBean securityViewBean) {
        this.securityViewBean = securityViewBean;
    }

    public TypeAttentionViewBean getTypeAttentionViewBean() {
        return typeAttentionViewBean;
    }

    public void setTypeAttentionViewBean(TypeAttentionViewBean typeAttentionViewBean) {
        this.typeAttentionViewBean = typeAttentionViewBean;
    }

    public Boolean getOperationCreate() {
        return operationCreate;
    }

    public void setOperationCreate(Boolean operationCreate) {
        this.operationCreate = operationCreate;
    }

    public Boolean getOperationRead() {
        return operationRead;
    }

    public void setOperationRead(Boolean operationRead) {
        this.operationRead = operationRead;
    }

    public Boolean getOperationUpdate() {
        return operationUpdate;
    }

    public void setOperationUpdate(Boolean operationUpdate) {
        this.operationUpdate = operationUpdate;
    }

    public Boolean getOperationDelete() {
        return operationDelete;
    }

    public void setOperationDelete(Boolean operationDelete) {
        this.operationDelete = operationDelete;
    }

    public Boolean getOperationAddTreatment() {
        return operationAddTreatment;
    }

    public void setOperationAddTreatment(Boolean operationAddTreatment) {
        this.operationAddTreatment = operationAddTreatment;
    }

    public Boolean getOperationDeleteTreatment() {
        return operationDeleteTreatment;
    }

    public void setOperationDeleteTreatment(Boolean operationDeleteTreatment) {
        this.operationDeleteTreatment = operationDeleteTreatment;
    }

    public Boolean getOperationSaveKey() {
        return operationSaveKey;
    }

    public void setOperationSaveKey(Boolean operationSaveKey) {
        this.operationSaveKey = operationSaveKey;
    }

    public void setPlansTreatmentsViewBean(PlansTreatmentsViewBean plansTreatmentsViewBean) {
        this.plansTreatmentsViewBean = plansTreatmentsViewBean;
    }

    public TypeSpecialistViewBean getTypeSpecialistViewBean() {
        return typeSpecialistViewBean;
    }

    public void setTypeSpecialistViewBean(TypeSpecialistViewBean typeSpecialistViewBean) {
        this.typeSpecialistViewBean = typeSpecialistViewBean;
    }

    public PlansPatientsViewBean getPlansPatientsViewBean() {
        return plansPatientsViewBean;
    }

    public void setPlansPatientsViewBean(PlansPatientsViewBean plansPatientsViewBean) {
        this.plansPatientsViewBean = plansPatientsViewBean;
    }

    public SpecialistsViewBean getSpecialistsViewBean() {
        return specialistsViewBean;
    }

    public void setSpecialistsViewBean(SpecialistsViewBean specialistsViewBean) {
        this.specialistsViewBean = specialistsViewBean;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }

    public PlansTreatmentsKeysViewBean getPlansTreatmentsKeysViewBean() {
        return plansTreatmentsKeysViewBean;
    }

    public void setPlansTreatmentsKeysViewBean(PlansTreatmentsKeysViewBean plansTreatmentsKeysViewBean) {
        this.plansTreatmentsKeysViewBean = plansTreatmentsKeysViewBean;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }

    public ScaleTreatmentsSpecialistViewBean getScaleTreatmentsSpecialistViewBean() {
        return scaleTreatmentsSpecialistViewBean;
    }

    public void setScaleTreatmentsSpecialistViewBean(ScaleTreatmentsSpecialistViewBean scaleTreatmentsSpecialistViewBean) {
        this.scaleTreatmentsSpecialistViewBean = scaleTreatmentsSpecialistViewBean;
    }

    public KeysPatientsFacadeLocal getKeysFacadeLocal() {
        return keysFacadeLocal;
    }

    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatementsKeysList() {

        if (this.propertiesPlansTreatementsKeysList == null) {
            this.propertiesPlansTreatementsKeysList = new ArrayList<>();
        }
        return propertiesPlansTreatementsKeysList;
    }

    public void setPropertiesPlansTreatementsKeysList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatementsKeysList) {
        this.propertiesPlansTreatementsKeysList = propertiesPlansTreatementsKeysList;
    }

    public List<ConsultKeysPatientsOutput> getConsultKeysPatientsOutput() {
        return consultKeysPatientsOutput;
    }

    public void setConsultKeysPatientsOutput(List<ConsultKeysPatientsOutput> consultKeysPatientsOutput) {
        this.consultKeysPatientsOutput = consultKeysPatientsOutput;
    }

    public List<ConsultKeysPatientsDetailOutput> getConsultKeysPatientsDetailOutput() {
        return consultKeysPatientsDetailOutput;
    }

    public void setConsultKeysPatientsDetailOutput(List<ConsultKeysPatientsDetailOutput> ConsultKeysPatientsDetailOutput) {
        this.consultKeysPatientsDetailOutput = ConsultKeysPatientsDetailOutput;
    }

    public List<ConsultKeysPatientsDetailTreatmentOutput> getConsultKeysPatientsDetailTreatmentOutput() {
        return consultKeysPatientsDetailTreatmentOutput;
    }

    public void setConsultKeysPatientsDetailTreatmentOutput(List<ConsultKeysPatientsDetailTreatmentOutput> ConsultKeysPatientsDetailTreatmentOutput) {
        this.consultKeysPatientsDetailTreatmentOutput = ConsultKeysPatientsDetailTreatmentOutput;
    }

    public LoadKeyStatesOutput getLoadKeyStatesOutputselect() {
        return loadKeyStatesOutputselect;
    }

    public void setLoadKeyStatesOutputselect(LoadKeyStatesOutput loadKeyStatesOutputselect) {
        this.loadKeyStatesOutputselect = loadKeyStatesOutputselect;
    }

    public ServiceKeysPatients getServiceKeysPatients() {
        return serviceKeysPatients;
    }

    public void setServiceKeysPatients(ServiceKeysPatients serviceKeysPatients) {
        this.serviceKeysPatients = serviceKeysPatients;
    }

    public String getQueryPatient() {
        return queryPatient;
    }

    public void setQueryPatient(String queryPatient) {
        this.queryPatient = queryPatient;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Boolean getDisableBtnDelete() {
        return disableBtnDelete;
    }

    public void setDisableBtnDelete(Boolean disableBtnDelete) {
        this.disableBtnDelete = disableBtnDelete;
    }

    public String getObserDelete() {
        return obserDelete;
    }

    public void setObserDelete(String obserDelete) {
        this.obserDelete = obserDelete;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        if (observation == "") {
            this.observation = null;
        }
        this.observation = observation;
    }

    public Integer getReasonDeleteOpen() {
        return reasonDeleteOpen;
    }

    public void setReasonDeleteOpen(Integer reasonDeleteOpen) {
        this.reasonDeleteOpen = reasonDeleteOpen;
    }
    
    

    public List<LoadKeyStatesOutput> getLoadKeyStatesOutput() {
        Collections.sort(loadKeyStatesOutput, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                LoadKeyStatesOutput tp1 = (LoadKeyStatesOutput) o1;
                LoadKeyStatesOutput tp2 = (LoadKeyStatesOutput) o2;
                return tp1.getNameStatusKeys().compareTo(tp2.getNameStatusKeys());

            }
        });
        return loadKeyStatesOutput;
    }

    public void setLoadKeyStatesOutput(List<LoadKeyStatesOutput> loadKeyStatesOutput) {
        this.loadKeyStatesOutput = loadKeyStatesOutput;
    }

    public String getQuerySpecialist() {
        return querySpecialist;
    }

    public void setQuerySpecialist(String querySpecialist) {
        this.querySpecialist = querySpecialist;
    }

    public Integer getIdStatusKey() {
        return idStatusKey;
    }

    public void setIdStatusKey(Integer idStatusKey) {
        this.idStatusKey = idStatusKey;
    }

    public Boolean getDisabledColumState() {
        return disabledColumState;
    }

    public void setDisabledColumState(Boolean disabledColumState) {
        this.disabledColumState = disabledColumState;
    }

    public Boolean getDisabledColumDateKey() {
        return disabledColumDateKey;
    }

    public void setDisabledColumDateKey(Boolean disabledColumDateKey) {
        this.disabledColumDateKey = disabledColumDateKey;
    }

    public Boolean getDisabledColumKey() {
        return disabledColumKey;
    }

    public void setDisabledColumKey(Boolean disabledColumKey) {
        this.disabledColumKey = disabledColumKey;
    }

    public Boolean getDisabledColumPatient() {
        return disabledColumPatient;
    }

    public void setDisabledColumPatient(Boolean disabledColumPatient) {
        this.disabledColumPatient = disabledColumPatient;
    }

    public Boolean getDisabledColumNumberP() {
        return disabledColumNumberP;
    }

    public void setDisabledColumNumberP(Boolean disabledColumNumberP) {
        this.disabledColumNumberP = disabledColumNumberP;
    }

    public Boolean getDisabledColumNumberS() {
        return disabledColumNumberS;
    }

    public void setDisabledColumNumberS(Boolean disabledColumNumberS) {
        this.disabledColumNumberS = disabledColumNumberS;
    }

    public Boolean getDisabledColumSpecialist() {
        return disabledColumSpecialist;
    }

    public void setDisabledColumSpecialist(Boolean disabledColumSpecialist) {
        this.disabledColumSpecialist = disabledColumSpecialist;
    }

    public Boolean getDisabledColumObservation() {
        return disabledColumObservation;
    }

    public void setDisabledColumObservation(Boolean disabledColumObservation) {
        this.disabledColumObservation = disabledColumObservation;
    }

    public Boolean getInputDateEnd() {
        return inputDateEnd;
    }

    public void setInputDateEnd(Boolean inputDateEnd) {
        this.inputDateEnd = inputDateEnd;
    }
    
    public Boolean obtainUserSpecialists() {
        dataSpecialists = true;
        Users user = this.securityViewBean.obtainUser();
        this.setSpecialist(this.specialistFacadeLocal.findByUser(user.getId()));
        if (this.getSpecialist() != null) {
            this.setDataSpecialists(false);
            this.listaKeys = this.keysFacadeLocal.findByKeysSpecialist(this.getSpecialist().getId());
            this.listaKeysFiltrada.clear();
            this.listaKeysFiltrada.addAll(listaKeys);
            return true;
        } else {
            return false;
        }
    }

    public List<InsurancesPatients> getListaInsurePatients() {
        if (this.listaInsurePatients == null) {
            this.listaInsurePatients = new ArrayList<>();
        }

        return listaInsurePatients;
    }

    public void setInsurePatients(List<InsurancesPatients> listaInsurePatients) {
        this.listaInsurePatients = listaInsurePatients;
    }

    public void setKeysFacadeLocal(KeysPatientsFacadeLocal keysFacadeLocal) {
        this.keysFacadeLocal = keysFacadeLocal;
    }

    public List<KeysPatients> getListaKeys() {
        if (this.listaKeys == null) {
            this.listaKeys = new ArrayList<>();
        }

        return listaKeys;
    }

    public void setListaKeys(List<KeysPatients> listaKeys) {
        this.listaKeys = listaKeys;
    }

    public List<KeysPatients> getListaKeysFiltrada() {
        return listaKeysFiltrada;
    }

    public void setListaKeysFiltrada(List<KeysPatients> listaKeysFiltrada) {
        this.listaKeysFiltrada = listaKeysFiltrada;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public ErrorHttpSessionBean getErrorHttpSessionBean() {
        return errorHttpSessionBean;
    }

    public void setErrorHttpSessionBean(ErrorHttpSessionBean errorHttpSessionBean) {
        this.errorHttpSessionBean = errorHttpSessionBean;
    }

    public void clearPlanTreament(String url) throws IOException {

        ExternalContext context2 = FacesContext.getCurrentInstance().getExternalContext();
        //this.typeAttentionViewBean.clearNewType();
        this.plansTreatmentsViewBean.setListPlansTreatmentsType(new ArrayList<PlansTreatments>());
        RequestContext.getCurrentInstance().update("form:panelTreatments");
        RequestContext.getCurrentInstance().update("form:treatment");
        context2.redirect(url);

    }

    public void mostrarComponente() {
        this.visible = true;
        RequestContext.getCurrentInstance().update(findComponent("form").getClientId());
    }

    public KeysPatients getKeysPatientsNew() {
        return keysPatientsNew;
    }

    public void setKeysPatientsNew(KeysPatients keysPatientsNew) {
        this.keysPatientsNew = keysPatientsNew;
    }

    public List<KeysPatients> getListHistoryKeys() {
        return listHistoryKeys;
    }

    public void setListHistoryKeys(List<KeysPatients> listHistoryKeys) {
        if (this.listHistoryKeys == null) {
            this.listHistoryKeys = new ArrayList<>();
        }
        this.listHistoryKeys = listHistoryKeys;
    }

    public List<ConsultUserRolesOutput> getConsultUserRolesOutput() {
        return consultUserRolesOutput;
    }

    public void setConsultUserRolesOutput(List<ConsultUserRolesOutput> consultUserRolesOutput) {
        this.consultUserRolesOutput = consultUserRolesOutput;
    }

    public KeysPatients getKeysPatientsEdit() {
        return keysPatientsEdit;
    }

    public List<LoadKeyStatesOutput> getLoadKeyStatesOutputComplement() {
        return loadKeyStatesOutputComplement;
    }

    public void setLoadKeyStatesOutputComplement(List<LoadKeyStatesOutput> loadKeyStatesOutputComplement) {
        this.loadKeyStatesOutputComplement = loadKeyStatesOutputComplement;
    }

    public String getEnteName() {
        return enteName;
    }

    public void setEnteName(String enteName) {
        this.enteName = enteName;
    }

    public String getConsultorioName() {
        return consultorioName;
    }

    public void setConsultorioName(String consultorioName) {
        this.consultorioName = consultorioName;
    }

    public String getAseguradoraName() {
        return aseguradoraName;
    }

    public void setAseguradoraName(String aseguradoraName) {
        this.aseguradoraName = aseguradoraName;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public List<ConsultKeysPatientsOutput> getConsultKeysPatientsOutputFilter() {
        return consultKeysPatientsOutputFilter;
    }

    public void setConsultKeysPatientsOutputFilter(List<ConsultKeysPatientsOutput> consultKeysPatientsOutputFilter) {
        this.consultKeysPatientsOutputFilter = consultKeysPatientsOutputFilter;
    }

    public Boolean getDisableBtnGuardar() {
        return disableBtnGuardar;
    }

    public void setDisableBtnGuardar(Boolean disableBtnGuardar) {
        this.disableBtnGuardar = disableBtnGuardar;
    }

    public Date getDateMinSql() {
        return dateMinSql;
    }

    public void setDateMinSql(Date dateMinSql) {
        this.dateMinSql = dateMinSql;
    }

    public String getDateMinCalendar() {
        return dateMinCalendar;
    }

    public void setDateMinCalendar(String dateMinCalendar) {
        this.dateMinCalendar = dateMinCalendar;
    }

    public KeysConsultDateMinOutput getKeysConsultDateMinOutput() {
        return keysConsultDateMinOutput;
    }

    public void setKeysConsultDateMinOutput(KeysConsultDateMinOutput keysConsultDateMinOutput) {
        this.keysConsultDateMinOutput = keysConsultDateMinOutput;
    }

    public DateMinHistoricoOutput getDateMinHistoricoOutput() {
        return dateMinHistoricoOutput;
    }

    public void setDateMinHistoricoOutput(DateMinHistoricoOutput dateMinHistoricoOutput) {
        this.dateMinHistoricoOutput = dateMinHistoricoOutput;
    }

    public List<ConsultDateMinDetalleOutput> getConsultDateMinDetalleOutput() {
        return consultDateMinDetalleOutput;
    }

    public void setConsultDateMinDetalleOutput(List<ConsultDateMinDetalleOutput> consultDateMinDetalleOutput) {
        this.consultDateMinDetalleOutput = consultDateMinDetalleOutput;
    }

    public Date getDateMinDetalle() {
        return dateMinDetalle;
    }

    public void setDateMinDetalle(Date dateMinDetalle) {
        this.dateMinDetalle = dateMinDetalle;
    }

    public Boolean getMessageDataTable() {
        return messageDataTable;
    }

    public void setMessageDataTable(Boolean messageDataTable) {
        this.messageDataTable = messageDataTable;
    }

   

    

    
    
    
    

    
    

    public void setKeysPatientsEdit(KeysPatients keysPatientsEdit) {

        if (keysPatientsEdit != null) {
            int i = 0;
            int cantidadProperties = 0;
            Boolean insurances = false;
            DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");

            if (keysPatientsEdit.getApplicationDate() != null) {
                String convertido = fecha.format(keysPatientsEdit.getApplicationDate());
                keysPatientsEdit.setApplicationDateString(convertido);
            }

            Boolean properties = false;
            this.getPlansTreatmentsViewBean().getListPlansTreatments().clear();
            this.getPlansPatientsViewBean().getListFilterPlansPatients().clear();
            this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(new PlansTreatments());

            //Buscar planesTreatments and exclude the treatments with status eliminado 
            StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
            this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(this.plansTreatmentsKeysFacadeLocal.findByKeysAndExcludeStatus(keysPatientsEdit.getId(), status.getId()));

            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados()) {
                plansTreatmentsKeys.setComponentTooth(false);

                plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().clear();

                this.getPlansTreatmentsViewBean().getListPlansTreatmentsValidate().add(plansTreatmentsKeys.getPlansTreatmentsId());
                if (plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList().size() > 0) {
                    properties = true;

                    this.getPropertiesTreatmentsViewBean().setNewListPropertiesTreatments(this.propertiesTreatmentsFacadeLocal.findByTreatmentsId(plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getId()));
                    this.getPropertiesTreatmentsViewBean().limpiarLista();
                    this.getPropertiesTreatmentsViewBean().fillListProperties(this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments());
                    this.fillListPropertiesNew(plansTreatmentsKeys);
                    this.fillListPropertiesFilter(plansTreatmentsKeys);

                    //Se actualiza el precio del baremo de acuerdo a la cantidad de piezas seleccionadas
                    Integer cantidad = 0;
                    Double cantidadLong = 0.00;
                    Double precioBaremo = 0.00;
                    cantidad = Integer.parseInt(this.propertiesPlansTreatmentsKeyFacadeLocal.countProperties(plansTreatmentsKeys.getId(), status.getId()));
                    // cantidad = plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyList().size();
                    cantidadLong = cantidad * 1.00;

                    if (plansTreatmentsKeys.getScaleTreatmentsKeys() != null) {
                        plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().setPriceNew(plansTreatmentsKeys.getScaleTreatmentsKeys().getPrice());
                        precioBaremo = plansTreatmentsKeys.getScaleTreatmentsKeys().getPrice() * cantidadLong;
                        precioBaremo = Math.rint(precioBaremo * 100) / 100;
                        DecimalFormat decf = new DecimalFormat("0.00");
                        String precio = (decf.format(plansTreatmentsKeys.getScaleTreatmentsKeys().getPrice()));
                        plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().setPriceString(precio);
                        String precioString = precio.replaceAll(",", ".");
                        plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().setPrice(Double.parseDouble(precioString));
                        plansTreatmentsKeys.getScaleTreatmentsKeys().setPrice(precioBaremo);
                    }

                    plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                    plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().add(plansTreatmentsKeys.getScaleTreatmentsKeys());

                    //Fin actualizaci�n de baremo 
                    this.getPropertiesTreatmentsViewBean().setVisibleList(true);
                    plansTreatmentsKeys.setComponentTooth(true);

                } else {

                    plansTreatmentsKeys.setComponentTooth(false);
                    plansTreatmentsKeys.getPlansTreatmentsId().getTreatmentsId().getScaleTreatmentsListListFilter().add(plansTreatmentsKeys.getScaleTreatmentsKeys());

                }
            }
            if (properties) {
                PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
                this.getPlansTreatmentsKeysViewBean().setEditPlansTreatmentsKeys(plansTreatmentsKeys);

            }

            this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados();
            if (keysPatientsEdit.getSpecialistId() != null) {
                this.getSpecialistsViewBean().getEditSpecialists()[1] = keysPatientsEdit.getSpecialistId().getCompleteName();
                this.getSpecialistsViewBean().getEditSpecialists()[6] = keysPatientsEdit.getSpecialistId().getIdentityNumber().toString();
                this.getTypeSpecialistViewBean().setEditTypeSpecialist(keysPatientsEdit.getSpecialistId().getTypeSpecialistid());
                this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(keysPatientsEdit.getSpecialistId().getTypeSpecialistid().getName());
                this.getSpecialityViewBean().setEditListaSpecialists(keysPatientsEdit.getSpecialistId().getSpecialityList());
            }

            this.getPatientsViewBean().getEditPatients()[6] = keysPatientsEdit.getPatientsId().getCompleteName();
            this.getPatientsViewBean().getEditPatients()[1] = keysPatientsEdit.getPatientsId().getIdentityNumber().toString();
            String dateOfBirth = fecha.format(keysPatientsEdit.getPatientsId().getDateOfBirth());
            this.getPatientsViewBean().getEditPatients()[2] = dateOfBirth;
            this.getListaInsurePatients().clear();
            this.getListaInsurePatients().addAll(this.insurancesPatientsFacadeLocal.findByPatientsId(keysPatientsEdit.getPatientsId().getId()));
            this.getPlansPatientsViewBean().setNewListPlansPatients(keysPatientsEdit.getPatientsId().getPlansPatientsList());

            if (this.getPlansPatientsViewBean().getNewListPlansPatients() != null) {

                if (listaInsurePatients != null) {

                    while (i < this.getPlansPatientsViewBean().getNewListPlansPatients().size()) {
                        insurances = false;
                        PlansPatients newListPlansPatient = this.getPlansPatientsViewBean().getNewListPlansPatients().get(i);

                        int idInsurances = newListPlansPatient.getPlansId().getInsuranceId().getId();

                        for (InsurancesPatients insurePatient : listaInsurePatients) {

                            int idInsurePatient = insurePatient.getInsurances().getId();

                            if (idInsurances == idInsurePatient) {
                                insurances = true;
                            }
                        }

                        if (!insurances) {
                            this.getPlansPatientsViewBean().getNewListPlansPatients().remove(newListPlansPatient);
                            i--;
                        }

                        i++;
                    }
                }

            }

            for (PlansPatients plansPatients : this.getPlansPatientsViewBean().getNewListPlansPatients()) {
                if (Objects.equals(keysPatientsEdit.getSpecialistId().getTypeSpecialistid().getId(), plansPatients.getPlansId().getTypeSpecialistId().getId())) {
                    this.getPlansPatientsViewBean().getListFilterPlansPatients().add(plansPatients);

                }
            }

            for (PlansPatients plansPatients : this.getPlansPatientsViewBean().getListFilterPlansPatients()) {
                this.getPlansPatientsViewBean().setNewListPlansTreatments(this.plansTreatmentsFacadeLocal.findByPlansId(plansPatients.getPlansId().getId()));
                DateFormat fechaplan = new SimpleDateFormat("dd/MM/yyyy");
                String convertidofecha = fechaplan.format(plansPatients.getDueDate());
                plansPatients.setDueDateString(convertidofecha);
                this.setDueDate(convertidofecha);
                //Verifica que tipo de baremo tiene el especialista para poder asignarle el precio a los tratamientos 
                if (keysPatientsEdit.getSpecialistId().getScale() != null) {

                    if (Objects.equals(keysPatientsEdit.getSpecialistId().getScale().getId(), TypeScale.General.getValor())) {
                        for (PlansTreatments planTreatments : this.getPlansPatientsViewBean().getNewListPlansTreatments()) {
                            // planTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                            ScaleTreatments scaleTreatments = this.scaleTreatmentsFacadeLocal.findByTreatments(planTreatments.getTreatmentsId().getId());

                            if (scaleTreatments != null) {
                                DecimalFormat decf = new DecimalFormat("0.00");
                                String precio = decf.format(scaleTreatments.getPrice());
                                String precioString = precio.replaceAll(",", ".");
                                planTreatments.getTreatmentsId().setPriceString(precio);
                                planTreatments.getTreatmentsId().setPrice(Math.rint(Double.parseDouble(precioString) * 100) / 100);

                                planTreatments.getTreatmentsId().setPriceNew(Math.rint(Double.parseDouble(precioString) * 100) / 100);
                                planTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().add(scaleTreatments);

                                this.getPlansTreatmentsViewBean().getListPlansTreatments().add(planTreatments);
                            }

                        }
                    } else if (Objects.equals(keysPatientsEdit.getSpecialistId().getScale().getId(), TypeScale.Especifico.getValor())) {
                        for (PlansTreatments plansTreatments : this.getPlansPatientsViewBean().getNewListPlansTreatments()) {
                            plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                            ScaleTreatments scaleTreatments = this.scaleTreatmentsFacadeLocal.findByTreatmentsAndSpecialist(plansTreatments.getTreatmentsId().getId(), keysPatientsEdit.getSpecialistId().getId());
                            if (scaleTreatments != null) {

                                plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().add(scaleTreatments);
//                                                   
                                DecimalFormat decf = new DecimalFormat("0.00");
                                String precio = decf.format(scaleTreatments.getPrice());
                                String precioString = precio.replaceAll(",", ".");
                                plansTreatments.getTreatmentsId().setPriceString(precio);
                                plansTreatments.getTreatmentsId().setPrice(Math.rint(Double.parseDouble(precioString) * 100) / 100);
                                plansTreatments.getTreatmentsId().setPriceNew(Math.rint(Double.parseDouble(precioString) * 100) / 100);
                                this.getPlansTreatmentsViewBean().getListPlansTreatments().add(plansTreatments);
                            }

                        }

                    }
                }
            }

        }

        this.keysPatientsEdit = keysPatientsEdit;
    }

    public void fillListProperties(List<PropertiesPlansTreatmentsKey> listpropertiesPlansTreatmentsKeys) {

        // Se llenan la lista de los string en la Edici�n !!
        if (listpropertiesPlansTreatmentsKeys.size() > 0) {

            for (PropertiesPlansTreatmentsKey propertiesPlansKey : listpropertiesPlansTreatmentsKeys) {

                if (Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) > 10 && Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) < 19) {

                    propertiesPlansKey.getPlansTreatmentsKeys().getPropertiesPlansTreatmentsKeyListString().add(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue());
                    this.getPropertiesPlansTreatmentsKeyViewBean().getEditListPropertiesPlansTreatmentsKey().add(propertiesPlansKey);

                } else if (Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) > 20 && Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) < 29) {

                    propertiesPlansKey.getPlansTreatmentsKeys().getPropertiesPlansTreatmentsKeyListStringII().add(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue());
                } else if (Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) > 40 && Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) < 49) {

                    propertiesPlansKey.getPlansTreatmentsKeys().getPropertiesPlansTreatmentsKeyListStringIII().add(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue());
                } else if (Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) > 30 && Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) < 39) {

                    propertiesPlansKey.getPlansTreatmentsKeys().getPropertiesPlansTreatmentsKeyListStringIV().add(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue());
                } else if (Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) > 30 && Integer.parseInt(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue()) < 39) {

                    propertiesPlansKey.getPlansTreatmentsKeys().getPropertiesPlansTreatmentsKeyListStringV().add(propertiesPlansKey.getPropertiesTreatments().getPropertiesId().getStringValue());
                }

            }
        }

    }

    // Metodo para llenar la lista de properties por tratamiento en la edici�n y llenar los que ya vienen con el tratamiento 
    public void fillListPropertiesNew(PlansTreatmentsKeys plansTreatmentsKey) {
        // Se llenan la lista de los string en la Edici�n !!
        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
        this.setPropertiesPlansTreatementsKeysList(this.propertiesPlansTreatmentsKeyFacadeLocal.findByKeysAndStatus(plansTreatmentsKey.getKeysId().getId(), status.getId()));

        plansTreatmentsKey.setPropertiesPlansTreatmentsKeyList(this.getPropertiesPlansTreatementsKeysList());
        if (plansTreatmentsKey.getPropertiesPlansTreatmentsKeyList().size() > 0) {

            for (PropertiesPlansTreatmentsKey propertiesTreatmentsKey : plansTreatmentsKey.getPropertiesPlansTreatmentsKeyList()) {
                int cuadrante = propertiesTreatmentsKey.getPropertiesTreatments().getPropertiesId().getQuadrant();
                PropertiesTreatments propertiesTreatments = propertiesTreatmentsKey.getPropertiesTreatments();
                switch (cuadrante) {
                    case 1:

                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNew().add(propertiesTreatments);

                        break;
                    case 2:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewII().add(propertiesTreatments);
                        break;
                    case 3:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewIII().add(propertiesTreatments);

                        break;

                    case 4:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewIV().add(propertiesTreatments);
                        break;

                    case 5:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewV().add(propertiesTreatments);
                        break;

                    case 6:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewVI().add(propertiesTreatments);
                        break;

                    case 7:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewVII().add(propertiesTreatments);
                        break;

                    case 8:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListNewVIII().add(propertiesTreatments);
                        break;
                }

            }
        }

    }

    public void fillListPropertiesFilter(PlansTreatmentsKeys plansTreatmentsKey) {
        // Se llenan la lista de los string en la Edici�n !!

        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(EstatusKeysPatients.ELIMINADO.getValor());
        this.setPropertiesPlansTreatementsKeysList(this.propertiesPlansTreatmentsKeyFacadeLocal.findByKeys(plansTreatmentsKey.getKeysId().getId()));
        plansTreatmentsKey.setPropertiesPlansTreatmentsKeyList(this.getPropertiesPlansTreatementsKeysList());

        if (plansTreatmentsKey.getPropertiesPlansTreatmentsKeyList().size() > 0) {

            for (PropertiesTreatments propertiesTreatments : this.getPropertiesTreatmentsViewBean().getNewListPropertiesTreatments()) {
                int cuadrante = propertiesTreatments.getPropertiesId().getQuadrant();

                switch (cuadrante) {
                    case 1:

                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilter().add(propertiesTreatments);
                        break;
                    case 2:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterII().add(propertiesTreatments);
                        break;
                    case 3:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterIII().add(propertiesTreatments);

                        break;

                    case 4:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterIV().add(propertiesTreatments);
                        break;

                    case 5:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterV().add(propertiesTreatments);
                        break;

                    case 6:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterVI().add(propertiesTreatments);
                        break;

                    case 7:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterVII().add(propertiesTreatments);
                        break;

                    case 8:
                        plansTreatmentsKey.getPropertiesPlansTreatmentsKeyListFilterVIII().add(propertiesTreatments);
                        break;
                }

            }
        }

    }

    public void cleanerKeysPatientsHistory(String idDialog) {
        RequestContext contextC = RequestContext.getCurrentInstance();
        this.getPropertiesTreatmentsViewBean().setVisibleListKeysHistory(false);
        if (!this.getKeysPatientsHistory().getPlansTreatmentsKeysList().isEmpty()) {
            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getKeysPatientsHistory().getPlansTreatmentsKeysList()) {
                cleanerListProperties(plansTreatmentsKeys);
            }
        }
        this.setKeysPatientsHistory(new KeysPatients());

        //; 
        contextC.update("form:" + idDialog);
        contextC.execute("PF('" + idDialog + "').hide()");

    }

    public void limpiar(String idDialog) {

        limpiarListaProperties();

        if (!idDialog.equals("keysDetailsDialogConsultar")) {

            this.setKeysPatientsEdit(null);
            this.getPlansTreatmentsViewBean().setListPlansTreatments(new ArrayList<PlansTreatments>());
        }

        this.setKeysPatientsHistory(new KeysPatients());
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(new ArrayList<PlansTreatmentsKeys>());
        this.getPlansTreatmentsViewBean().setListPlansTreatmentsValidate(new ArrayList<PlansTreatments>());
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysFiltrada(this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados());
        this.getSpecialistsViewBean().getEditSpecialists()[1] = null;
        this.getSpecialistsViewBean().getEditSpecialists()[6] = null;
        this.getTypeSpecialistViewBean().setEditTypeSpecialist(new TypeSpecialist());
        this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(null);
        this.getPatientsViewBean().getEditPatients()[6] = null;
        this.getPatientsViewBean().getEditPatients()[1] = null;
        this.getPatientsViewBean().getEditPatients()[2] = null;
        this.getPropertiesTreatmentsViewBean().setVisibleList(false);
        this.getPropertiesTreatmentsViewBean().setVisiblePanel(false);
        this.getPlansPatientsViewBean().setNewListPlansPatients(new ArrayList<PlansPatients>());
        this.getPlansPatientsViewBean().setListFilterPlansPatients(new ArrayList<PlansPatients>());
        this.getListaInsurePatients().clear();

        this.getPlansTreatmentsViewBean().setSelectedPlansTreatments(new PlansTreatments());
        this.getPropertiesTreatmentsViewBean().setSelectedOptions(new ArrayList<String>());
        RequestContext contextC = RequestContext.getCurrentInstance();
        contextC.update("form:" + idDialog);
        contextC.execute("PF('" + idDialog + "').hide()");
        this.getPropertiesTreatmentsViewBean().limpiarLista();
        contextC.update("form:lstTreatmentII");

    }

    public String getMyFormattedDateStart(Date fecha) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha);
    }

    public void filterByRange() {
        try {
            if (startDate == null || endDate == null) {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el rango de fechas"));
                RequestContext.getCurrentInstance().update("form:keysSpecialistGrow");
            } else {
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                String fechaStr = formatter.format(endDate);

                Date date = formatter.parse(fechaStr);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
                Date fechaFinalTransformada = cal.getTime();

                String fechaInicialStr = this.getMyFormattedDateStart(startDate);
                String fechaFinalStr = this.getMyFormattedDateStart(endDate);

                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date fechaInicial = sdf2.parse(fechaInicialStr);
                Date fechaFinal = sdf2.parse(fechaFinalStr);
                if (fechaInicial != null && fechaFinal != null) {
                    listaKeys = this.keysFacadeLocal.findByApplicationDateSpecialist(fechaInicial, fechaFinalTransformada, specialist.getId());
                    this.listaKeysFiltrada.clear();
                    this.listaKeysFiltrada.addAll(listaKeys);
                    RequestContext.getCurrentInstance().update("form:keysSpecialistsList");
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(LazyListKeysPatientsViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void resetFechas() {
        this.setStartDate(null);
        this.setEndDate(null);
        this.setDateSelect(null);
        this.listaKeys = this.keysFacadeLocal.findByKeysSpecialist(specialist.getId());
        this.listaKeysFiltrada.clear();
        this.listaKeysFiltrada.addAll(listaKeys);
        RequestContext.getCurrentInstance().update("form:keysSpecialistsList");
    }

    public void handleDateSelect(SelectEvent event) {
        RequestContext.getCurrentInstance().execute("PF('keysTable').filter()");
    }
    
   public void handleDateSelectDetail(SelectEvent event) throws ParseException {
        log.log(Level.INFO, "handleDateSelect()");

        RequestContext.getCurrentInstance().execute("PF('treatmentKeyDetail').filter()");
        RequestContext.getCurrentInstance().execute("PF('treatmentKeyDetail').filter()");
   }

    public void selectDate() throws ParseException {
        if (dateSelect != null) {
            String dateTransform = new SimpleDateFormat("dd/MM/yyyy").format(dateSelect);
            listaKeys = this.keysFacadeLocal.findByApplicationDateAndSpecialist(dateTransform, specialist.getId());
            this.listaKeysFiltrada.clear();
            this.listaKeysFiltrada.addAll(listaKeys);
            RequestContext.getCurrentInstance().update("form:keysSpecialistsList");
        }
    }

    public Date formatDate(Date date) throws ParseException {

        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
        Date fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        return fechaTransformada;

    }

    public void buscarSpecialistByIdentityNumber(Specialists especialista, String id) {
        clearListTreatments();
        this.plansPatientsViewBean.setActiveTratament(true);
        RequestContext.getCurrentInstance().update("form:panelTreatments");
        RequestContext.getCurrentInstance().update("form:treatment");
        boolean encontrado = false;
        this.getSpecialistsViewBean().getEditSpecialists()[5] = "";
        this.getSpecialityViewBean().getEditListaSpecialists().clear();
        this.getPatientsViewBean().setPatientsAutocomplete(null);
        this.getPatientsViewBean().getEditPatients()[6] = "";
        this.getPlansPatientsViewBean().getListFilterPlansPatients().clear();
        this.getTypeAttentionViewBean().setNewType(new TypeAttention());
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(new ArrayList<PlansTreatmentsKeys>());
        RequestContext.getCurrentInstance().update("form:lstTreatment");

        if (especialista != null) {
            int cedula = especialista.getIdentityNumber();
            for (Specialists specialist : this.getSpecialistsViewBean().getListaSpecialists()) {
                if (specialist.getIdentityNumber() != null && specialist.getIdentityNumber() == cedula) {
                    this.getSpecialistsViewBean().getEditSpecialists()[5] = specialist.getCompleteName();
                    this.getSpecialistsViewBean().getEditSpecialists()[1] = specialist.getCompleteName();
                    this.getSpecialistsViewBean().getEditSpecialists()[0] = specialist.getId().toString();

                    this.getSpecialistsViewBean().setEditSpecialistsObj(specialist);
                    this.getTypeSpecialistViewBean().setEditTypeSpecialist(specialist.getTypeSpecialistid());
                    this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(specialist.getTypeSpecialistid().getName());
                    this.getSpecialityViewBean().setEditListaSpecialists(this.getSpecialistsViewBean().getEditSpecialistsObj().getSpecialityList());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                this.getSpecialistsViewBean().getEditSpecialists()[6] = "";
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado especialistas con ese n�mero de c�dula"));
            }
        }

    }

    public void buscarIdentityNumber(Patients patients, String id) {
        int i = 0;
        boolean encontrado = false;
        boolean insurances = false;

        this.clearKeysAdd();

        RequestContext.getCurrentInstance().update("form:panelTreatments");
        RequestContext.getCurrentInstance().update("form:treatment");
        RequestContext.getCurrentInstance().update("form:attentionTypeCreate");

        //Para limpiar 
        this.getPlansTreatmentsViewBean().setListPlansTreatmentsSeleccionados(new ArrayList<PlansTreatments>());

        if (patients != null) {
            int cedula = patients.getIdentityNumber();
            this.getPatientsViewBean().setNewPatientsObj(patients);
            this.getPatientsViewBean().getEditPatients()[6] = patients.getCompleteName();
            this.getPatientsViewBean().getEditPatients()[0] = patients.getId().toString();
            listaInsurePatients = this.insurancesPatientsFacadeLocal.findByPatientsId(patients.getId());
            this.getPlansPatientsViewBean().setNewListPlansPatients(this.plansPatientsFacadeLocal.findByPatientsId(patients.getId()));
            if (!this.getPlansPatientsViewBean().getNewListPlansPatients().isEmpty()) {

                if (!listaInsurePatients.isEmpty()) {

                    while (i < this.getPlansPatientsViewBean().getNewListPlansPatients().size()) {
                        insurances = false;
                        PlansPatients newListPlansPatient = this.getPlansPatientsViewBean().getNewListPlansPatients().get(i);

                        int idInsurances = newListPlansPatient.getPlansId().getInsuranceId().getId();

                        for (InsurancesPatients insurePatient : listaInsurePatients) {

                            int idInsurePatient = insurePatient.getInsurances().getId();

                            if (idInsurances == idInsurePatient) {
                                insurances = true;
                            }
                        }

                        if (!insurances) {
                            this.getPlansPatientsViewBean().getNewListPlansPatients().remove(newListPlansPatient);
                            i--;
                        }

                        i++;
                    }

                    Integer typeSpeciality;

                    if (this.specialist != null) {

                        typeSpeciality = this.specialist.getTypeSpecialistid().getId();

                    } else {
                        typeSpeciality = this.getSpecialityViewBean().getEditListaSpecialists().get(0).getTypeSpecialist().getId();
                    }

                    for (PlansPatients plansPatients : this.getPlansPatientsViewBean().getNewListPlansPatients()) {

                        if (Objects.equals(typeSpeciality, plansPatients.getPlansId().getTypeSpecialistId().getId())) {
                            this.setPlan(true);
                            this.getPlansPatientsViewBean().getListFilterPlansPatients().add(plansPatients);

                        }

                    }
                    //Habilitar combo de tipo de atenci�n
                    this.setListHistoryKeys(this.keysFacadeLocal.findByIdPatients(patients.getId()));
                    this.plansPatientsViewBean.setActiveTratament(true);
                    RequestContext.getCurrentInstance().update("form:panelTreatments");
                    RequestContext.getCurrentInstance().update("form:treatment");
                    this.updateCmbTypeAttention(false);

                } else {
                    RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El paciente no posee planes activos con ninguna aseguradora"));

                }

            } else {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El paciente no posee planes activos con ninguna aseguradora"));
            }

        }

    }

    public void clearListTreatments() {
        // this.typeAttentionViewBean.clearNewType();
        this.plansTreatmentsViewBean.setListPlansTreatmentsType(new ArrayList<PlansTreatments>());
        this.getPlansPatientsViewBean().setNewListPlansTreatments(new ArrayList<PlansTreatments>());
        this.getPlansTreatmentsViewBean().setListPlansTreatments(new ArrayList<PlansTreatments>());
    }

    public void filterPlansTreatments() {
        clearListTreatments();
        if (this.isPlan()) {
            if (this.getSpecialist() != null) {
                this.getSpecialistsViewBean().setEditSpecialistsObj(this.specialist);
                this.getTypeSpecialistViewBean().setEditTypeSpecialist(specialist.getTypeSpecialistid());
                this.getTypeSpecialistViewBean().getEditTypeSpecialist().setName(specialist.getTypeSpecialistid().getName());
                this.getSpecialityViewBean().setEditListaSpecialists(this.specialist.getSpecialityList());

            }
            for (PlansPatients plansPatients : this.getPlansPatientsViewBean().getListFilterPlansPatients()) {
                DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
                String convertido = fecha.format(plansPatients.getDueDate());
                plansPatients.setDueDateString(convertido);
                String birthDay = fecha.format(plansPatients.getPatientsId().getDateOfBirth());
                plansPatients.getPatientsId().setDateOfBirthString(birthDay);
                this.getPatientsViewBean().getEditPatients()[2] = plansPatients.getPatientsId().getDateOfBirthString();

                this.getPlansPatientsViewBean().setNewListPlansTreatments(this.plansTreatmentsFacadeLocal.findByPlansId(plansPatients.getPlansId().getId()));
                if (this.getSpecialistsViewBean().getEditSpecialistsObj().getScale() != null) {
                    if (Objects.equals(this.getSpecialistsViewBean().getEditSpecialistsObj().getScale().getId(), TypeScale.General.getValor())) {
                        for (PlansTreatments plansTreatments : this.getPlansPatientsViewBean().getNewListPlansTreatments()) {
                            plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                            ScaleTreatments scaleTreatments = this.scaleTreatmentsFacadeLocal.findByTreatments(plansTreatments.getTreatmentsId().getId());

                            if (scaleTreatments != null) {
                                Double temp = 0.00;
                                Double priceFormat = 0.00;

                                plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().add(scaleTreatments);

                                DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
                                simbolos.setDecimalSeparator('.');
                                DecimalFormat decf = new DecimalFormat("0.00");
                                scaleTreatments.setPrice(Math.rint(scaleTreatments.getPrice() * 100) / 100);
                                plansTreatments.getTreatmentsId().setPrice(scaleTreatments.getPrice());
                                plansTreatments.getTreatmentsId().setPriceNew(scaleTreatments.getPrice());
                                plansTreatments.getTreatmentsId().setPriceString(decf.format(scaleTreatments.getPrice()));
                                this.getPlansTreatmentsViewBean().getListPlansTreatments().add(plansTreatments);
                            }

                        }

                    } else if (Objects.equals(this.getSpecialistsViewBean().getEditSpecialistsObj().getScale().getId(), TypeScale.Especifico.getValor())) {
                        for (PlansTreatments plansTreatments : this.getPlansPatientsViewBean().getNewListPlansTreatments()) {
                            // plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().clear();
                            ScaleTreatments scaleTreatments = this.scaleTreatmentsFacadeLocal.findByTreatmentsAndSpecialist(plansTreatments.getTreatmentsId().getId(), this.getSpecialistsViewBean().getEditSpecialistsObj().getId());
                            if (scaleTreatments != null) {

                                plansTreatments.getTreatmentsId().getScaleTreatmentsListListFilter().add(scaleTreatments);

                                DecimalFormat decf = new DecimalFormat("0.00");
                                plansTreatments.getTreatmentsId().setPrice(Math.rint(scaleTreatments.getPrice() * 100) / 100);
                                plansTreatments.getTreatmentsId().setPriceNew(Math.rint(scaleTreatments.getPrice() * 100) / 100);
                                plansTreatments.getTreatmentsId().setPriceString(decf.format(scaleTreatments.getPrice()));
                                this.getPlansTreatmentsViewBean().getListPlansTreatments().add(plansTreatments);

                            }

                        }

                    }
                }
//                            //this.getPlansTreatmentsViewBean().setListPlansTreatments(this.getPlansPatientsViewBean().getNewListPlansTreatments());
                this.getPlansTreatmentsViewBean().getAuxListPlansTreatments().addAll(this.getPlansTreatmentsViewBean().getListPlansTreatments());

//
//                            Collections.sort(this.getPlansTreatmentsViewBean().getListPlansTreatments(), new Comparator<PlansTreatments>() {
//
//                                @Override
//                                public int compare(PlansTreatments o1, PlansTreatments o2) {
//                                    return o1.getTreatmentsId().getName().compareTo(o2.getTreatmentsId().getName());
//                                }
//
//                            });
            }

        } else {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El paciente no cuenta con planes que concuerden con la especialidad del Doctor"));
        }

    }

    /**
     * Method that allows to clean the elements of the view by adding key It is
     * used in the buscarIdentityNumber and in this same class of
     * validatePatient
     */
    public void clearKeysAdd() {

        this.getPatientsViewBean().getEditPatients()[6] = "";
        this.getPlansPatientsViewBean().getListFilterPlansPatients().clear();
        this.getPlansPatientsViewBean().getNewListPlansPatients().clear();
        this.getPlansPatientsViewBean().getListPlansPatients().clear();
        this.getListaInsurePatients().clear();
        this.getPlansTreatmentsViewBean().getAuxListPlansTreatments().clear();
        this.getPlansTreatmentsViewBean().getListPlansTreatments().clear();
        this.getPlansTreatmentsViewBean().getAuxListPlansTreatments().clear();
        this.getTypeAttentionViewBean().setNewType(new TypeAttention());
        this.updateCmbTypeAttention(true);
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(new ArrayList<PlansTreatmentsKeys>());
        this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysFiltrada(new ArrayList<PlansTreatmentsKeys>());
        this.setListHistoryKeys(new ArrayList<KeysPatients>());
        RequestContext.getCurrentInstance().update("form:lstTreatment");
        RequestContext.getCurrentInstance().update("form:historyPatients");

        RequestContext.getCurrentInstance().update("form:attentionTypeCreate");

    }

    public List<Patients> validate() {

        if (this.getPatientsViewBean().getListaPatients().isEmpty()) {

            this.clearKeysAdd();
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado Pacientes con ese número de cédula"));

        }

        return this.getPatientsViewBean().getListaPatients();

    }

    public void limpiarListaProperties() {

        if (this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados().size() > 0) {

            for (PlansTreatmentsKeys plansTreatmentsKeys : this.getPlansTreatmentsKeysViewBean().getListPlansTreatmentsKeysSeleccionados()) {

                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNew().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewIV().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewV().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVI().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNewVIII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilter().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterIV().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterV().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVI().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVII().clear();
                plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListFilterVIII().clear();

            }

        }

    }

    public void cleanerListProperties(PlansTreatmentsKeys plansTreatmentsKeys) {

        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNew(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewIII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewIV(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewV(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewVI(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewVII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListNewVIII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilter(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterIII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterIV(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterV(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterVI(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterVII(new ArrayList<PropertiesTreatments>());
        plansTreatmentsKeys.setPropertiesPlansTreatmentsKeyListFilterVIII(new ArrayList<PropertiesTreatments>());

    }

    public void actualizarScalePropertiesEditKey(PlansTreatmentsKeys plansTreatmentsKeys) {
        int cantidad = 0;
        Double cantidadLong = 0.00;
        cantidad = plansTreatmentsKeys.getPropertiesPlansTreatmentsKeyListNew().size() + 1;
        cantidadLong = cantidad * 1.0;
        Double precioBaremo = plansTreatmentsKeys.getScaleTreatmentsKeys().getPrice() * cantidadLong;
        DecimalFormat decf = new DecimalFormat("0.00");
        String precio = (decf.format(precioBaremo * 1.00));
        plansTreatmentsKeys.getScaleTreatmentsKeys().setPrice(Double.parseDouble(precio));
    }

    public boolean updateCreateKeys(Boolean disable) {

        this.setCreateKeyDisable(disable);
        RequestContext.getCurrentInstance().update("form:createKeys");
        //this.updateBtnPlanTreatments(disable);
        return this.getCreateKeyDisable();

    }

    public Boolean updateBtnPlanTreatments(Boolean disable) {

        this.getPlansTreatmentsViewBean().setBtnPlanTreatments(disable);
        RequestContext.getCurrentInstance().update("form:btnAddTraatment2");
        return this.getPlansTreatmentsViewBean().getBtnPlanTreatments();
    }

    public void operationSecurity() {

        this.operationCreate = false;
        this.operationDelete = false;
        this.operationRead = false;
        this.operationUpdate = false;
        this.operationAddTreatment = false;
        this.operationDeleteTreatment = false;
        this.operationSaveKey = false;
        Boolean searchOperation = false;

        List<MenuRol> listMenuRol = new ArrayList<>();
        List<MenuRolOperation> listMenuRolOperation = new ArrayList<>();

        Menu menu = this.menuFacadeLocal.findByName("Gestionar");
        listMenuRol = this.securityViewBean.obtainMenuByOperations();

        for (int i = 0; i < listMenuRol.size(); i++) {

            Menu menuRol = listMenuRol.get(i).getMenuId();

            if (menu != null) {

                if (menu.getId() == menuRol.getId()) {

                    listMenuRolOperation = this.securityViewBean.obtainMenuRolOperations(listMenuRol.get(i).getId());

                    for (int j = 0; j < listMenuRolOperation.size(); j++) {

                        String operation = listMenuRolOperation.get(j).getOperation().getName();

                        if (operation.equals("Crear")) {

                            this.operationCreate = true;

                        } else if (operation.equals("Consultar")) {

                            this.operationRead = true;

                        } else if (operation.equals("Modificar")) {

                            this.operationUpdate = true;
                            this.operationSaveKey = true;

                        } else if (operation.equals("Eliminar")) {

                            this.operationDelete = true;

                        } else if (operation.equals("Agregar Tratamiento")) {

                            this.operationAddTreatment = true;
                            this.operationSaveKey = true;

                        } else if (operation.equals("Eliminar Tratamiento")) {

                            this.operationDeleteTreatment = true;
                            this.operationSaveKey = true;

                        }

                    }

                }
            }

        }

    }

    public List<Patients> filterPatients(String query) {

        this.getPatientsViewBean().setListaPatients(this.getPatientsFacadeLocal().findByIdentityNumberOrNamePatients(query));
        this.validatePatient();
        this.clearKeysAdd();
        return this.getPatientsViewBean().getListaPatients();

    }

    public List<Specialists> filterSpecialist(String query) {

        this.getSpecialistsViewBean().setListSpecialistFiltered(this.getSpecialistFacadeLocal().findByIdentityNumberOrName(query));
        this.validateSpecialist();
        this.clearKeysAdd();
        return this.getSpecialistsViewBean().getListSpecialistFiltered();
    }

    public void enableCalendarHasta() {

        setCalendarHasta(false);
        RequestContext.getCurrentInstance().update("form:keysSpecialistsList");
        RequestContext.getCurrentInstance().update("form:endDate");

    }

    public boolean validatePatient() {

        if (this.getPatientsViewBean().getListaPatients().isEmpty()) {
            this.clearKeysAdd();
            // RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado Pacientes con ese n�mero de c�dula"));

            return false;
        }

        return true;
    }

    public boolean validateSpecialist() {

        if (this.getSpecialistsViewBean().getListSpecialistFiltered().isEmpty()) {
            this.clearKeysAdd();
            this.getSpecialistsViewBean().getEditSpecialists()[5] = "";
            this.getSpecialityViewBean().getEditListaSpecialists().clear();
            this.getPatientsViewBean().setPatientsAutocomplete(null);
            this.getPatientsViewBean().getEditPatients()[6] = "";
            this.getPlansPatientsViewBean().getListFilterPlansPatients().clear();
            this.getTypeAttentionViewBean().setNewType(new TypeAttention());
            this.getPlansTreatmentsKeysViewBean().setListPlansTreatmentsKeysSeleccionados(new ArrayList<PlansTreatmentsKeys>());
            RequestContext.getCurrentInstance().update("form:lstTreatment");
            RequestContext.getCurrentInstance().update("form:specialist");
            RequestContext.getCurrentInstance().update("form:speciality");
            RequestContext.getCurrentInstance().update("form:namePatient");
            RequestContext.getCurrentInstance().update("form:plans");
            RequestContext.getCurrentInstance().update("form:autocompletePatients");
            RequestContext.getCurrentInstance().update("form:attentionTypeCreate");

            //RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se han encontrado Especialistas con ese n�mero de c�dula"));
            return false;
        }

        return true;
    }

    public Boolean updateCmbTypeAttention(Boolean disable) {
        this.getTypeAttentionViewBean().setCmbTypeAttention(disable);
        RequestContext.getCurrentInstance().update("form:attentionTypeCreate");
        return this.getTypeAttentionViewBean().getCmbTypeAttention();

    }

    public void filtrarTypeAttention(TypeAttention type) {

        int i = 0;
        Boolean typeEncontrado = false;
        TypeAttention typeAttentionTreatments = new TypeAttention();
        this.filterPlansTreatments();

        if (type == null) {

            //this.getPlansTreatmentsViewBean().getListPlansTreatmentsType().clear();
            this.plansPatientsViewBean.setActiveTratament(true);
            RequestContext.getCurrentInstance().update("form:panelTreatments");
            RequestContext.getCurrentInstance().update("form:treatment");
        } else {
            this.plansPatientsViewBean.setActiveTratament(false);

            while (i < this.getPlansTreatmentsViewBean().getListPlansTreatments().size()) {

                typeEncontrado = false;
                PlansTreatments plansTreatments = this.getPlansTreatmentsViewBean().getListPlansTreatments().get(i);

                for (int j = 0; j < this.getPlansTreatmentsViewBean().getListPlansTreatments().get(i).getTreatmentsId().getTypeAttentionList().size(); j++) {

                    typeAttentionTreatments = this.getPlansTreatmentsViewBean().getListPlansTreatments().get(i).getTreatmentsId().getTypeAttentionList().get(j);

                    if (Objects.equals(type.getId(), typeAttentionTreatments.getId())) {
                        typeEncontrado = true;
                    }
                }

                if (typeEncontrado != true) {

                    this.getPlansTreatmentsViewBean().getListPlansTreatments().remove(i);
                    i--;

                }

                i++;

            }

            this.getPlansTreatmentsViewBean().setListPlansTreatmentsType(this.getPlansTreatmentsViewBean().getListPlansTreatments());
            //this.getPlansTreatmentsViewBean().getListPlansTreatmentsType().addAll(this.getPlansTreatmentsViewBean().getListPlansTreatments());

            RequestContext.getCurrentInstance().update("form:panelTreatments");
            RequestContext.getCurrentInstance().update("form:treatment");

        }
    }

    /**
     * method that create the binnaclePlansTreatmentsKeys
     *
     * @param plansTreatmentsKeysRegistrado
     * @param state
     * @throws java.text.ParseException
     */
    public void createBinnaclePlansTreatmentsKeys(PlansTreatmentsKeys plansTreatmentsKeysRegistrado, Integer state) throws ParseException {
        log.log(Level.INFO, "createBinnaclePlansTreatmentsKeys()");
        BinnaclePlansTreatmentsKeys binnaclePlansTreatmentsKeys = new BinnaclePlansTreatmentsKeys();
        Users user = this.securityViewBean.obtainUser();
        StatusAnalysisKeys status = this.statusAnalysisKeysFacadeLocal.findById(state);
        Date dateActual = this.obtainsDateActual();
        plansTreatmentsKeysRegistrado.setStatusId(state);
        binnaclePlansTreatmentsKeys.setIdPlansTreatmentsKeys(plansTreatmentsKeysRegistrado);
        binnaclePlansTreatmentsKeys.setDateBinnacleInitiation(dateActual);
        binnaclePlansTreatmentsKeys.setDateBinnacleEnd(dateActual);
        binnaclePlansTreatmentsKeys.setIdStatusPlans(status);
        binnaclePlansTreatmentsKeys.setIdUsers(user);
        this.binnaclePlansTreatmentsKeysFacade.create(binnaclePlansTreatmentsKeys);
        this.plansTreatmentsKeysFacadeLocal.edit(plansTreatmentsKeysRegistrado);

    }

    /**
     * method for get the date actual in format dd/MM/yyyy HH:mm:ss
     *
     * @return Date
     * @throws java.text.ParseException
     */
    public Date obtainsDateActual() throws ParseException {

        Date fecha = new Date();
        SimpleDateFormat sdfClave = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat sdfSolicitud = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdfSolicitud.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
        String fechaClave = sdfClave.format(fecha);
        String fechaSolicitud = sdfSolicitud.format(fecha);
        Date dateApplication = Transformar.StringToDate(fechaSolicitud);
        return dateApplication;
    }

    /**
     * Method for verify enabled button delete
     *
     * @param obj
     * @return
     */
    public boolean enableBtnDelete(ConsultKeysPatientsOutput obj) {
        if(obj.getIdStatusKey()!=null){
        this.setIdKeyDelete(obj.getIdKey());
        this.userRoles();
        if (this.isDeleteKey() == true) {
            if (obj.getIdStatusKey() == 1) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }else{
            return true;
        }
    }

//      * Method at converter dateUtil and dateSql
//     *
//     * @param date
//     * @return
//     */
    public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {

        return new java.sql.Date(date.getTime());
    }

    public void reset() throws ParseException {
        this.setStartDateConsult(null);
        this.setEndDateConsult(null);
        this.setConsultKeysPatientsOutput(null);
        this.setNumberKey(null);
        this.setQueryPatient(null);
        this.setQuerySpecialist(null);
        this.setDisabledBtnBuscar(true);
        this.setDateFilter(null);
        this.setConsultKeysPatientsOutput(null);
        this.setConsultKeysPatientsDetailOutput(null);
        this.setConsultKeysPatientsOutputFilter(null);
        this.setConsultKeysPatientsDetailTreatmentOutput(null);
        this.formatDateFilter(null);
        this.setDisableBtnConsult(false);
        this.setDisabledBtnRest(true);
        this.setLoadKeyStatesOutputselect(null);
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:keysPatientsConsults");
        dataTable.reset();
        this.setMessageDataTable(true);
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults:filter");
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        RequestContext.getCurrentInstance().update("form:dateKeyPatient");
        RequestContext.getCurrentInstance().update("form:key");
        RequestContext.getCurrentInstance().update("form:patient");
        RequestContext.getCurrentInstance().update("form:identityNumberPatient");
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        RequestContext.getCurrentInstance().update("form:starDate");
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:numberKeys");
        RequestContext.getCurrentInstance().update("form:queryKeys");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        RequestContext.getCurrentInstance().update("form:search");
        RequestContext.getCurrentInstance().update("form:dateKeyPatients");
        RequestContext.getCurrentInstance().update("form:consult");
        RequestContext.getCurrentInstance().update("form:keysConsultDialogDetails");
        RequestContext.getCurrentInstance().update("form:btnResetKey");
        RequestContext.getCurrentInstance().update("form:stateKey");

    }
    
//    public void resetDataTable(){
//        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:keysPatientsConsults");
//        dataTable.reset();
//        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
//    }
//Metodo para validar la activaci�n del boton buscar
    public void validateActivateButtonSearch(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        Integer idState = null;
        String nameState = null;
        if (this.getLoadKeyStatesOutputselect() == null) {
            idState = null;
            nameState = null;
        }else{
            idState=this.getLoadKeyStatesOutputselect().getIdstatusKey();
            nameState=this.getLoadKeyStatesOutputselect().getNameStatusKeys();
        }
        try {
            if ((this.getStartDateConsult() == null) && this.getEndDateConsult() == null && this.getQueryPatient() == null && this.getNumberKey() == null && idState== null && this.getQuerySpecialist() == null) {
                this.setDisabledBtnBuscar(true);
                RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
                RequestContext.getCurrentInstance().update("form:btnFindKey");
                RequestContext.getCurrentInstance().update("form:btnResetKey");

            } else if (this.getStartDateConsult() == null && idState == null && this.getEndDateConsult() == null && "".equals(this.getQueryPatient()) && "".equals(this.getNumberKey()) && "".equals(this.getQuerySpecialist())) {
                this.setDisabledBtnBuscar(true);
                this.setDisabledBtnRest(false);
                RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
                RequestContext.getCurrentInstance().update("form:btnFindKey");
                RequestContext.getCurrentInstance().update("form:btnResetKey");

            } else if ((this.getStartDateConsult() == null) && this.getEndDateConsult() == null && "".equals(this.getQueryPatient()) && this.getNumberKey() == null && idState== null) {
                this.setDisabledBtnBuscar(true);
                this.setDisabledBtnRest(false);
                RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
                RequestContext.getCurrentInstance().update("form:btnFindKey");
                RequestContext.getCurrentInstance().update("form:btnResetKey");
            } else if ((this.getStartDateConsult() == null) && this.getEndDateConsult() == null && this.getQueryPatient() == null && "".equals(this.getNumberKey()) && idState== null) {
                this.setDisabledBtnBuscar(true);
                this.setDisabledBtnRest(false);
                RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
                RequestContext.getCurrentInstance().update("form:btnFindKey");
                RequestContext.getCurrentInstance().update("form:btnResetKey");
            } else {

                this.setDisabledBtnBuscar(false);
                this.setDisabledBtnRest(false);
                RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
                RequestContext.getCurrentInstance().update("form:btnFindKey");
                RequestContext.getCurrentInstance().update("form:btnResetKey");
//             
            }

        } catch (Exception e) {

        }
    }

//    public void detailsKeyConsult() {
//        Integer key = Integer.parseInt(this.getNumberKey());
//        this.setConsultKeysPatientsDetailOutput(this.serviceKeysPatients.detailsConsultKeysPatients(key));
//
//    }

    /**
     * Funcion designada para cargar los valores del dialogo de detalles
     *
     * @param consultKeysPatientsOutput
     */
    public void chargeDetail(ConsultKeysPatientsOutput consultKeysPatientsOutput) throws ParseException {
        
        Integer key = consultKeysPatientsOutput.getIdKey();
        this.setConsultKeysPatientsDetailOutput(this.serviceKeysPatients.detailsConsultKeysPatients(key));
        for (ConsultKeysPatientsDetailOutput consultKeysPatientsDetailOutput : this.consultKeysPatientsDetailOutput) {
            this.setKey(consultKeysPatientsDetailOutput.getNumberKey());
            this.setNameS(consultKeysPatientsDetailOutput.getNameSpecialist());
            this.setNameP(consultKeysPatientsDetailOutput.getNamePatient());
            this.setSpeciality(consultKeysPatientsDetailOutput.getSpecialitySpecialist());
            this.setPlanName(consultKeysPatientsDetailOutput.getNamePlan());
            this.setDateKeyDetail(consultKeysPatientsDetailOutput.getDateKey());
            this.setExpirationKey(consultKeysPatientsDetailOutput.getExpiratioKey());
            this.setBirthdateP(consultKeysPatientsDetailOutput.getBrithayPatient());
            this.setEnteName(consultKeysPatientsDetailOutput.getNameEnte());
            this.setConsultorioName(consultKeysPatientsDetailOutput.getNameConsultorio());
            this.setAseguradoraName(consultKeysPatientsDetailOutput.getNameAseguradora());
            this.setParentesco(consultKeysPatientsDetailOutput.getParentesco());
            
            RequestContext.getCurrentInstance().update("form:keysConsultDialog");
            RequestContext.getCurrentInstance().update("form:panelKeyDetail");
            RequestContext.getCurrentInstance().update("form:treatmentKeyDetail");
        }
        this.setDateFilter(null);
        this.setNameTreatment(null);
         RequestContext.getCurrentInstance().update("form:treatmentKeyDetail");
        this.setConsultKeysPatientsDetailTreatmentOutput(this.serviceKeysPatients.detailsConsultKeysPatientsTreatments(key));
        for (ConsultKeysPatientsDetailTreatmentOutput consultKeysPatientsDetailTreatmentOutput : this.consultKeysPatientsDetailTreatmentOutput) {
                java.util.Date utilDate = new java.util.Date(consultKeysPatientsDetailTreatmentOutput.getDateForTreatment().getTime());
                consultKeysPatientsDetailTreatmentOutput.setDateForTreatmentUtil(utilDate);
            }
        RequestContext.getCurrentInstance().update("form:keysConsultDialog");
        RequestContext.getCurrentInstance().update("form:panelKeyDetail");
        RequestContext.getCurrentInstance().update("form:treatmentKeyDetail");
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:treatmentKeyDetail");
        dataTable.reset();
        this.setConsultDateMinDetalleOutput(this.serviceKeysPatients.findDateMinDetalle(key));
        for(ConsultDateMinDetalleOutput consultDateMinDetalleOutput: this.consultDateMinDetalleOutput){
            this.setDateMinDetalle(consultDateMinDetalleOutput.getDateMinTreatment());
            
        }
        
    }

    public void filterDate() {
        System.out.println("com.venedental.controller.view.KeysPatientsViewBean.filterDate()");
        RequestContext.getCurrentInstance().execute(" PF('keysPatientsConsults').filter();");

    }
    
    public void filterDateDetail() {
        System.out.println("com.venedental.controller.view.KeysPatientsViewBean.filterDate()");
        RequestContext.getCurrentInstance().execute(" PF('keysPatientsConsults').filter();");

    }

    public void selectDateFilter() throws ParseException {
        String dateValue = null;
        if (dateKey != null) {
            this.setStartDate(null);
            this.setEndDate(null);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            dateValue = sdf.format(dateKey);
            RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        } else {
            dateValue = null;
            RequestContext.getCurrentInstance().update("form:keysPatientsConsults");

        }
    }

    public Date formatDateFilter(Date date) throws ParseException {
        Date fechaTransformada;
        if (date == null) {
            fechaTransformada = null;
        } else {
            log.log(Level.INFO, "formatDate()");
            String fecha = new SimpleDateFormat("dd/MM/yyyy").format(date);
            fechaTransformada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
        }
        return fechaTransformada;

    }

//    
    /**
     * // * Metodo que permite buscar los roles segun el id del usuario // * //
     */
    public void userRoles() {
        Integer idUser = this.securityViewBean.obtainUser().getId();
        this.setConsultUserRolesOutput(this.serviceKeysPatients.searchUserRoles(idUser));
        for (ConsultUserRolesOutput consultUserRolesOutput : this.consultUserRolesOutput) {
            if (consultUserRolesOutput.getUserRoles() == 28) {
                this.setDeleteKey(true);
            }
        }

    }

    public void observationCreate() {
        this.setReasonDelete(reasonDelete);
    }

    public void stateKeys() {
        Integer parameter = 0;
        this.setLoadKeyStatesOutput(this.serviceKeysPatients.statesOfKey(parameter));
        this.loadKeyStatesOutput.add(new LoadKeyStatesOutput(-1,"TODOS LOS ESTADOS"));
        this.getLoadKeyStatesOutput();
        
        this.setDisabledColumKey(true);
        this.setDisabledColumNumberP(true);
        this.setDisabledColumNumberS(true);
        this.setDisabledColumObservation(true);
        this.setDisabledColumPatient(true);
        this.setDisabledColumSpecialist(true);
        this.setDisabledColumState(true);
        RequestContext.getCurrentInstance().update("form:growlKeyPattientsGrow");
        RequestContext.getCurrentInstance().update("form:dateKeyPatients");
        RequestContext.getCurrentInstance().update("form:keys");
        RequestContext.getCurrentInstance().update("form:patients");
        RequestContext.getCurrentInstance().update("form:identityNumberPatients");
        RequestContext.getCurrentInstance().update("form:keysPatientsConsults");
        RequestContext.getCurrentInstance().update("form:state");
        RequestContext.getCurrentInstance().update("form:specialist");
        RequestContext.getCurrentInstance().update("form:identityNumberSpecialist");
        RequestContext.getCurrentInstance().update("form:observation");
    }
    
    public void selectDateStar(){
        this.setInputDateEnd(false);
        this.setDisabledBtnBuscar(true);
        RequestContext.getCurrentInstance().update("form:endDate");
        RequestContext.getCurrentInstance().update("form:btnFindKey");
        
    }
    
    public void selectDateEnd(){
        this.setDisabledBtnBuscar(false);
        RequestContext.getCurrentInstance().update("form:btnFindKey");
    }
    
    public void selectSalir() {
        
        if (reasonDelete == null) {
            reasonDelete[1] = "";
        }else{
            reasonDelete[1] = "";
        }
        RequestContext.getCurrentInstance().update("form:txtObservationsData");
        

    }
    
    public void closeDialog(String idDialog) {
        
        if (reasonDelete == null) {
            reasonDelete[1] = "";
        }else{
            reasonDelete[1] = "";
        }
        RequestContext.getCurrentInstance().update("form:txtObservationsData");
        
         RequestContext contextC = RequestContext.getCurrentInstance();
        

        //; .desplegarDialogoView('keysPatientsReasonDelete')
        contextC.openDialog(idDialog);
        contextC.update("form:" + idDialog);
        contextC.execute("PF('" + idDialog + "').hide()");

    }
    /**
     * Funcion que valida que el campo observaci�n no este vacio
     *
     * @param event
     */
    public void validateObservation(AjaxBehaviorEvent event) {
        UIInput comp = (UIInput) event.getComponent();
        try {
            if (this.reasonDelete[1].isEmpty()) {
                FacesMessage msg = new FacesMessage("Observación es un campo obligatorio.");
                this.setDisableBtnGuardar(true);
                RequestContext.getCurrentInstance().update("form:btnsaveReason");

                msg.setSeverity(FacesMessage.SEVERITY_ERROR);

                comp.setValid(false);
                FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
                FacesContext.getCurrentInstance().renderResponse();

            } else {
                this.setDisableBtnGuardar(false);
                RequestContext.getCurrentInstance().update("form:btnsaveReason");
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage("Observación es un campo obligatorio.");
           this.setDisableBtnGuardar(true);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            comp.setValid(false);
            FacesContext.getCurrentInstance().addMessage(comp.getClientId(), msg);
            FacesContext.getCurrentInstance().renderResponse();
        }

    }
    
   public void dateMin(){
       this.setKeysConsultDateMinOutput(this.serviceKeysPatients.findDateMin(1));
//       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        this.dateMinCalendar = sdf.format(this.getKeysConsultDateMinOutput());
//        System.out.println("com.venedental.controller.view.KeysPatientsViewBean.dateMin()"+dateMinCalendar);
       
   }
    
   

}
