/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading;

import com.venedental.massiveLoading.base.Constants;
import controller.utils.CustomLogger;
import com.venedental.massiveLoading.helper.WriterHelper;
import com.venedental.massiveLoading.read.ReaderFile;
import com.venedental.dto.massiveLoading.FileLoad;
import com.venedental.dto.massiveLoading.InsuranceLoad;
import com.venedental.dto.massiveLoading.RowLoad;
import com.venedental.dto.plans.FindPlanByIdOutput;
import com.venedental.mail.ejb.MailSenderLocal;
import com.venedental.massiveLoading.exceptions.MessageException;
import com.venedental.massiveLoading.helper.NotifierHelper;
import com.venedental.massiveLoading.helper.PersisterHelper;
import com.venedental.massiveLoading.helper.ValidatorHelper;
import com.venedental.model.Insurances;
import com.venedental.services.ServiceMassiveLoading;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akdesk01
 */
public class LoaderInsurance {

    private static final Logger log = CustomLogger.getLogger(LoaderInsurance.class.getName());

    private final ServiceMassiveLoading serviceMassiveLoading;

    private final MailSenderLocal mailSenderLocal;

    private WriterHelper writerHelper;

    public LoaderInsurance(ServiceMassiveLoading serviceMassiveLoading, MailSenderLocal mailSenderLocal) {
        this.serviceMassiveLoading = serviceMassiveLoading;
        this.mailSenderLocal = mailSenderLocal;
    }

    public void load(InsuranceLoad insuranceLoading, List<String> emails,Boolean disassociate, 
            Insurances insurance, Boolean include, Boolean exclude,String modeLoad,List<FindPlanByIdOutput> planList) {
        log.log(Level.INFO, "Inicio - Lectura de la carpeta: {0}", insuranceLoading.getFolderPath());
        String directoryDate = getDirectoryDate();
        insuranceLoading.configureFolderCurrentLoadPath(directoryDate);
        writerHelper = new WriterHelper(insuranceLoading.getFolderName(), insuranceLoading.getFolderPath() + Constants.DIRECTORY_PROCESSED, directoryDate + Constants.DIRECTORY_SEPARATOR);
        if (insuranceLoading.getFolder().exists()) {
            for (FileLoad file : insuranceLoading.getFiles()) {
                ReaderFile readerFile = new ReaderFile(file, this);
                readerFile.load(insurance,planList);
            }
            executeMassiveUpdate(insuranceLoading,disassociate, include, exclude);
            sendMail(insuranceLoading, emails, modeLoad);
        } else {
            log.log(Level.INFO, "La carpeta actual \"{0}\" no existe", insuranceLoading.getFolderPath());
        }
        log.log(Level.INFO, "Fin - Lectura de la carpeta: {0}", insuranceLoading.getFolderPath());
    }

    private String getDirectoryDate() {
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_DIRECTORY);
        return format.format(new Date());
    }

    public WriterHelper getWriterHelper() {
        return writerHelper;
    }

    public void validate(RowLoad rowLoad,List<FindPlanByIdOutput> planList) throws MessageException {
        ValidatorHelper.validateData(rowLoad.getData(), planList);
    }

    public void persistRow(RowLoad rowLoad,Insurances insurance) throws MessageException {
        PersisterHelper.persistRow(rowLoad, serviceMassiveLoading,insurance);
    }

    private void executeMassiveUpdate(InsuranceLoad insuranceLoading,Boolean disassociate, Boolean include, Boolean exclude) {
        PersisterHelper.executeMassiveUpdate(insuranceLoading, serviceMassiveLoading, disassociate, include, exclude);
    }

    private void sendMail(InsuranceLoad insuranceLoading, List<String> emails,String modeLoad) {
        List<File> files = writerHelper.generateFiles(insuranceLoading,modeLoad);
        NotifierHelper notifierHelper = new NotifierHelper(mailSenderLocal);
        notifierHelper.notifyInsurance(files, insuranceLoading.getInsurance(), emails, modeLoad);
    }

}
