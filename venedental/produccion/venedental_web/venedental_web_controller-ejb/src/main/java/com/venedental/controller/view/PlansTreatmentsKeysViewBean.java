/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.view;
import com.venedental.controller.application.CitiesApplicationBean;
import com.venedental.controller.session.ComponenteSessionBean;
import com.venedental.facade.PropertiesTreatmentsFacadeLocal;
import com.venedental.model.PlansTreatmentsKeys;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "plansTreatmentsKeysViewBean")
@ViewScoped
public class PlansTreatmentsKeysViewBean {
    
    
   
    @EJB
    private PropertiesTreatmentsFacadeLocal propertiesTreatmentsFacadeLocal;
    private final RequestContext context = RequestContext.getCurrentInstance();
    private List<PlansTreatmentsKeys> listPlansTreatmentsKeys;
    private List<PlansTreatmentsKeys> listPlansTreatmentsKeysFiltrada;
    private List<PlansTreatmentsKeys> listPlansTreatmentsKeysSeleccionados;
    private List<PlansTreatmentsKeys> newListPlansTreatmentsKeys;
    private List<PlansTreatmentsKeys> editListPlansTreatmentsKeys;
    private List<PlansTreatmentsKeys> beforeListPlansTreatmentsKeys;
    private PlansTreatmentsKeys editPlansTreatmentsKeys;
    private PlansTreatmentsKeys selectedPlansTreatmentsKeys;
    private PlansTreatmentsKeys newPlansTreatmentsKeysObj;
    private String[] newPlansTreatmentsKeys;
    private boolean visibleList;
    
     private List<String> cities;
   
    
    @ManagedProperty(value = "#{componenteSessionBean}")
    private ComponenteSessionBean componenteSessionBean;

    @ManagedProperty(value = "#{stateViewBean}")
    private StateViewBean stateViewBean;
    @ManagedProperty(value = "#{citiesViewBean}")
    private CitiesViewBean citiesViewBean;
//    @ManagedProperty(value = "#{propertiesTreatmentsViewBean}")
//    private PropertiesTreatmentsViewBean propertiesTreatmentsViewBean;

    @ManagedProperty(value = "#{patientsViewBean}")
    private PatientsViewBean patientsViewBean;
    @ManagedProperty(value = "#{plansViewBean}")
    private PlansViewBean plansViewBean;
    @ManagedProperty(value = "#{addressesViewBean}")
    private AddressesViewBean addressesViewBean;
    @ManagedProperty(value = "#{phoneClinicViewBean}")
    private PhoneClinicViewBean phoneClinicViewBean;
    @ManagedProperty(value = "#{citiesApplicationBean}")
    private CitiesApplicationBean citiesApplicationBean;
    
    
    

    public List<PlansTreatmentsKeys> getBeforeListPlansTreatmentsKeys() {
        return this.beforeListPlansTreatmentsKeys;
    }

    public void setBeforeListPlansTreatmentsKeys(List<PlansTreatmentsKeys> beforeListPlansTreatmentsKeys) {
        if (this.beforeListPlansTreatmentsKeys == null) {
            this.beforeListPlansTreatmentsKeys = new ArrayList<>();
        }
        this.beforeListPlansTreatmentsKeys = beforeListPlansTreatmentsKeys;
    }

    public StateViewBean getStateViewBean() {
        return stateViewBean;
    }

    public void setStateViewBean(StateViewBean stateViewBean) {
        this.stateViewBean = stateViewBean;
    }

    public CitiesViewBean getCitiesViewBean() {
        return citiesViewBean;
    }

    public void setCitiesViewBean(CitiesViewBean citiesViewBean) {
        this.citiesViewBean = citiesViewBean;
    }

    public AddressesViewBean getAddressesViewBean() {
        return addressesViewBean;
    }

    public void setAddressesViewBean(AddressesViewBean addressesViewBean) {
        this.addressesViewBean = addressesViewBean;
    }

    public PhoneClinicViewBean getPhoneClinicViewBean() {
        return phoneClinicViewBean;
    }

    public void setPhoneClinicViewBean(PhoneClinicViewBean phoneClinicViewBean) {
        this.phoneClinicViewBean = phoneClinicViewBean;
    }

    public CitiesApplicationBean getCitiesApplicationBean() {
        return citiesApplicationBean;
    }

    public void setCitiesApplicationBean(CitiesApplicationBean citiesApplicationBean) {
        this.citiesApplicationBean = citiesApplicationBean;
    }


    public List<PlansTreatmentsKeys> getEditListPlansTreatmentsKeys() {
        return editListPlansTreatmentsKeys;
    }

    public void setEditListPlansTreatmentsKeys(List<PlansTreatmentsKeys> editListPlansTreatmentsKeys) {
        if (this.editListPlansTreatmentsKeys == null) {
            this.editListPlansTreatmentsKeys = new ArrayList<>();
        }
        this.editListPlansTreatmentsKeys = editListPlansTreatmentsKeys;
    }

    public List<PlansTreatmentsKeys> getNewListPlansTreatmentsKeys() {
        return newListPlansTreatmentsKeys;
    }

    public void setNewListPlansTreatmentsKeys(List<PlansTreatmentsKeys> newListPlansTreatmentsKeys) {
        if (this.newListPlansTreatmentsKeys == null) {
            this.newListPlansTreatmentsKeys = new ArrayList<>();
        }
        this.newListPlansTreatmentsKeys = newListPlansTreatmentsKeys;
    }

    public PatientsViewBean getPatientsViewBean() {
        return patientsViewBean;
    }

    public void setPatientsViewBean(PatientsViewBean patientsViewBean) {
        this.patientsViewBean = patientsViewBean;
    }
    
    public PlansTreatmentsKeysViewBean() {
        this.editPlansTreatmentsKeys = new PlansTreatmentsKeys();
        //this.selectedPlansTreatmentsKeys = new PlansTreatmentsKeys();
        this.newPlansTreatmentsKeys = new String[4];
        this.beforeListPlansTreatmentsKeys = new ArrayList<>();
        this.visibleList = false;

    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public List<PlansTreatmentsKeys> getListPlansTreatmentsKeysFiltrada() {
        if (this.listPlansTreatmentsKeysFiltrada == null) {
            this.listPlansTreatmentsKeysFiltrada = new ArrayList<>();
        }
        return listPlansTreatmentsKeysFiltrada;
    }

    public void setListPlansTreatmentsKeysFiltrada(List<PlansTreatmentsKeys> listPlansTreatmentsKeysFiltrada) {
        this.listPlansTreatmentsKeysFiltrada = listPlansTreatmentsKeysFiltrada;
    }
    public List<PlansTreatmentsKeys> getListPlansTreatmentsKeys() {
        if (this.listPlansTreatmentsKeys == null) {
            this.listPlansTreatmentsKeys = new ArrayList<>();
        }
        return listPlansTreatmentsKeys;
    }

    public void setListPlansTreatmentsKeys(List<PlansTreatmentsKeys> listPlansTreatmentsKeys) {
        this.listPlansTreatmentsKeys = listPlansTreatmentsKeys;
    }

    public PlansTreatmentsKeys getEditPlansTreatmentsKeys() {
        return editPlansTreatmentsKeys;
    }

    public void setEditPlansTreatmentsKeys(PlansTreatmentsKeys editPlansTreatmentsKeys) {
        this.editPlansTreatmentsKeys = editPlansTreatmentsKeys;
    }

    public PlansTreatmentsKeys getSelectedPlansTreatmentsKeys() {
        return selectedPlansTreatmentsKeys;
    }

    public void setSelectedPlansTreatmentsKeys(PlansTreatmentsKeys selectedPlansTreatmentsKeys) {
        
        this.selectedPlansTreatmentsKeys = selectedPlansTreatmentsKeys;
    }

    public String[] getNewPlansTreatmentsKeys() {
        return newPlansTreatmentsKeys;
    }

    public void setNewPlansTreatmentsKeys(String[] newPlansTreatmentsKeys) {
        this.newPlansTreatmentsKeys = newPlansTreatmentsKeys;
    }

    public PlansTreatmentsKeys getNewPlansTreatmentsKeysObj() {
        return newPlansTreatmentsKeysObj;
    }

    public void setNewPlansTreatmentsKeysObj(PlansTreatmentsKeys newPlansTreatmentsKeysObj) {
        this.newPlansTreatmentsKeysObj = newPlansTreatmentsKeysObj;
    }

    public PlansViewBean getPlansViewBean() {
        return plansViewBean;
    }

    public void setPlansViewBean(PlansViewBean plansViewBean) {
        this.plansViewBean = plansViewBean;
    }
    
    public List<PlansTreatmentsKeys> getListPlansTreatmentsKeysSeleccionados() {
         if (this.listPlansTreatmentsKeysSeleccionados == null) {
            this.listPlansTreatmentsKeysSeleccionados = new ArrayList<>();
        }
        return listPlansTreatmentsKeysSeleccionados;
    }

    public void setListPlansTreatmentsKeysSeleccionados(List<PlansTreatmentsKeys> listPlansTreatmentsKeysSeleccionados) {
        this.listPlansTreatmentsKeysSeleccionados = listPlansTreatmentsKeysSeleccionados;
    }

    public boolean isVisibleList() {
        return visibleList;
    }

    public void setVisibleList(boolean visibleList) {
        this.visibleList = visibleList;
    }

    public ComponenteSessionBean getComponenteSessionBean() {
        return componenteSessionBean;
    }

    public void setComponenteSessionBean(ComponenteSessionBean componenteSessionBean) {
        this.componenteSessionBean = componenteSessionBean;
    }
    
    
    public void desplegarDialogoCreate(String dialogo, boolean valoresRequeridos) {
        this.getComponenteSessionBean().setCamposRequeridosCreate(valoresRequeridos);
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.update("form:" + dialogo);
        requestContext.execute("PF('" + dialogo + "').show()");
    }

 
}
