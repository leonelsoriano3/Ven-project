package com.venedental.enums;

/**
 *
 * @author Usuario
 */
public enum PropertiesEnums {

    PERMANENT(1), TEMPORARY(2), QUADRANTONE(1), QUADRANTTWO(2), QUADRANTTRHEE(3), QUADRANTFOUR(4),
    QUADRANTFIVE(5), QUADRANTSIX(6), QUADRANTSEVEN(7), QUADRANTEIGHT(8);

    Integer valor;

    private PropertiesEnums(Integer v) {
        valor = v;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}