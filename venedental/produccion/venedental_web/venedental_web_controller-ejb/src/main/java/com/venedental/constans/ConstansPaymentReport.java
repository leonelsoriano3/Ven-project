/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.constans;

/**
 *
 * @author AKDESKDEV90
 */
public class ConstansPaymentReport {
    
    public static String PROPERTIES_PATH = "src/main/resources/";

    public static String FILENAME_PAYMENT_REPORT = "paymentReport.properties";
    
    public static String PROPERTY_PATH_REPORT = "directoryPaymentReport.path";
    public static String PROPERTY_PATH_JASPER = "directoryPaymentReport.pathJasper";
    public static String PROPERTY_PATH_JASPER_SUBCRIBER = "directoryPaymentReportSubcriber.pathJasper";
    public static String PROPERTY_PATH_LOGO = "directoryPaymentReport.pathLogo";
    public static String PROPERTY_ADMIN_VENEDEN = "admin.veneden";
    public static String PROPERTY_REPORT_EXTENSION = "reportPaymentReport.extension";
    public static String PROPERTY_REPORT_TITLE_PART = "reportPaymentReport.titlePart";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT =  "emailPaymentReport.specialistContentP1";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT2 = "emailPaymentReport.specialistContentP2";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT3 = "emailPaymentReport.specialistContentP3";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT4 = "emailPaymentReport.specialistContentP4";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT5 = "emailPaymentReport.specialistContentP5";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT6 = "emailPaymentReport.specialistContentP6";
    public static String PROPERTY_EMAIL_SPECIALIST_CONTENT7 = "emailPaymentReport.specialistContentP7";
          
    public static String PROPERTY_EMAIL_ERROR_ADMIN_CONTENT = "emailPaymentReport.adminContentError";
    public static String PROPERTY_EMAIL_EXITO_ADMIN_CONTENT = "emailPaymentReport.adminContentExito";
    public static String PROPERTY_EMAIL_TITLE_SPECIALIST = "emailPaymentReport.titleEpecialist";
    public static String PROPERTY_EMAIL_TITLE_VENEDEN = "emailPaymentReport.titleVeneden";
    public static String PROPERTY_EMAIL_SUBJECT_VENEDEN = "emailPaymentReport.subjectVeneden";
    public static String PROPERTY_NAME_USER = "nameUser";
    public static String EMAIL_VENEDEN_RP="backupreportes@yahoo.com";
    public static String PROPERTY_EMAIL_TITLE_REPORT_TREATMENT = "emailPaymentReport.titleReportTreatment";
    
    
    
}
