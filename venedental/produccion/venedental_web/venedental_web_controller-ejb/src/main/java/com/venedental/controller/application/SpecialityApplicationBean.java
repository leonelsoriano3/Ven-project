/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.application;

import com.venedental.controller.BaseBean;
import com.venedental.facade.SpecialityFacadeLocal;
import com.venedental.model.Speciality;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Admin
 */
@ManagedBean(name = "SpecialityApplicationBean")
@ApplicationScoped
public class SpecialityApplicationBean extends BaseBean {

    @EJB
    private SpecialityFacadeLocal specialityFacadeLocal;

    private List<Speciality> listSpeciality;

    public List<Speciality> getListSpeciality() {
        if (this.listSpeciality == null) {
            this.listSpeciality = new ArrayList<>();
        }
        return listSpeciality;
    }

    public void setListSpeciality(List<Speciality> listSpeciality) {
        this.listSpeciality = listSpeciality;
    }

    public SpecialityApplicationBean() {

    }

    @PostConstruct
    public void init() {
        this.getListSpeciality().addAll(this.specialityFacadeLocal.findAll());
    }
    
}
