/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base.enums;

/**
 *
 * @author akdesk01
 */
public enum EFields {

    IDENTITY_NUMBER_HOLDER(0, "Cédula de identidad del titular", 8),
    IDENTITY_NUMBER(1, "Cédula de identidad del beneficiario", 8),
    COMPLETE_NAME(2, "Nombre completo", 50),
    SEX(3, "Sexo", 3),
    BIRTHDAY(4, "Fecha de nacimiento", 10),
    RELATIONSHIP(5, "Parentesco", 10),
    PLANS(6, "Planes", 14),
    COMPANY(7, "Ente", 20);

    private final Integer order;
    private final String name;
    private final Integer width;

    private EFields(Integer order, String name, Integer width) {
        this.order = order;
        this.name = name;
        this.width = width;
    }

    public Integer getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public Integer getWidth() {
        return width;
    }

}
