/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.controller.request;

import com.venedental.controller.BaseBean;
import com.venedental.controller.view.ReimbursementRequeriment;
import com.venedental.enums.Relationship;
import com.venedental.enums.StatusReimbursementEnum;
import com.venedental.enums.TypeSpecialistsEnums;
import com.venedental.facade.BanksPatientsFacadeLocal;
import com.venedental.facade.BinnacleReimbursementFacadeLocal;
import com.venedental.facade.PatientsFacadeLocal;
import com.venedental.facade.PlansFacadeLocal;
import com.venedental.facade.PlansPatientsFacadeLocal;
import com.venedental.facade.ReimbursementFacadeLocal;
import com.venedental.facade.RequirementFacadeLocal;
import com.venedental.facade.StatusReimbursementFacadeLocal;
import com.venedental.model.BanksPatients;
import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Patients;
import com.venedental.model.Plans;
import com.venedental.model.Reimbursement;
import com.venedental.model.Requirement;
import com.venedental.model.StatusReimbursement;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Anthony Torres
 */
@ManagedBean(name = "reimbursementFormRequestBean")
@RequestScoped
public class ReimbursementFormRequestBean extends BaseBean implements Serializable {
    
    @ManagedProperty(value = "#{reimbursementRequeriment}")
    private ReimbursementRequeriment reimbursementRequeriment;
    @EJB
    private PlansFacadeLocal plansFacadeLocal;
    @EJB
    private PlansPatientsFacadeLocal plansPatientsFacadeLocal;
    @EJB
    private ReimbursementFacadeLocal reimbursementFacadeLocal;
    @EJB
    private StatusReimbursementFacadeLocal statusReimbursmentFacadeLocal;
    @EJB
    private BanksPatientsFacadeLocal banksPatientsFacadeLocal;
    @EJB
    private PatientsFacadeLocal patientsFacadeLocal;
    @EJB
    private RequirementFacadeLocal requirementFacadeLocal;
    @EJB
    private BinnacleReimbursementFacadeLocal binnacleReimbursementFacadeLocal;
    
    private List<Plans> plansVerificatorList;
    
    private UploadedFile identityHolderImage;
    
    public UploadedFile getIdentityHolderImage() {
        return identityHolderImage;
    }
    
    public List<Plans> getPlansVerificatorList() {
        return plansVerificatorList;
    }
    
    public void setPlansVerificatorList(List<Plans> plansVerificatorList) {
        this.plansVerificatorList = plansVerificatorList;
    }
    
    public ReimbursementRequeriment getReimbursementRequeriment() {
        return reimbursementRequeriment;
    }
    
    public void setReimbursementRequeriment(ReimbursementRequeriment reimbursementRequeriment) {
        this.reimbursementRequeriment = reimbursementRequeriment;
    }
    
    public void setIdentityHolderImage(UploadedFile identityHolderImage) {
        this.identityHolderImage = identityHolderImage;
    }
    
   

    /**
     * Method to send the reimbursement form
     *
     * @throws java.io.IOException
     * @throws java.text.ParseException
     */
    public void sendForm() throws IOException, ParseException {
        if (!this.getReimbursementRequeriment().getEmail().equalsIgnoreCase(this.getReimbursementRequeriment().getEmailConfirm())) {
            this.getReimbursementRequeriment().addMessage("Error", "Las direcciones de correo no coinciden");
        } else {
            String bankAccount = this.getReimbursementRequeriment().getMaskBanks() + this.getReimbursementRequeriment().getAccountNumber();
            this.getReimbursementRequeriment().setBanksPatientsList(this.banksPatientsFacadeLocal.findByAccountNumber(bankAccount));
            BanksPatients newAccountNumber = new BanksPatients();
            
            if (this.getReimbursementRequeriment().getBanksPatientsList().isEmpty()) {
                
                newAccountNumber.setPatientsId(this.patientsFacadeLocal.find(this.getReimbursementRequeriment().getPatientsList().get(0).getId()));
                newAccountNumber.setAccountNumber(bankAccount);
                newAccountNumber.setBankId(this.getReimbursementRequeriment().getBank());
                this.banksPatientsFacadeLocal.create(newAccountNumber);
            } else {
                newAccountNumber.setId(this.getReimbursementRequeriment().getBanksPatientsList().get(0).getId());
            }
            createReimbursement(newAccountNumber);
        }
    }

    /**
     * Method to create a Reimbursement request
     *
     * @param banksPatients
     * @throws java.io.IOException
     * @throws java.text.ParseException
     */
    public void createReimbursement(BanksPatients banksPatients) throws IOException, ParseException {
        Plans plans;
        List<Patients> patients = new ArrayList<>();
        Reimbursement sendForm = new Reimbursement();
        Plans verifyPlan = new Plans();
        StatusReimbursement statusReimbursement;
        
        statusReimbursement = this.statusReimbursmentFacadeLocal.find(StatusReimbursementEnum.SOLICITADO.getValor());
        
        if (this.getReimbursementRequeriment().getReimbursementHolder().equals("si")) {
            patients = this.patientsFacadeLocal.findByIdentityNumberAndRelationShip(Integer.valueOf(this.getReimbursementRequeriment().getIdentityNumberHolder()), Relationship.TITULAR.getValor());
        } else if (this.getReimbursementRequeriment().getReimbursementHolder().equals("no")) {
            patients = this.patientsFacadeLocal.findById(this.getReimbursementRequeriment().getBeneficiarySelected().getId());
        }
        plans = this.getReimbursementRequeriment().getPlanSelected();
        if (this.reimbursementFacadeLocal.patientHasReimbursement(patients.get(0).getId(), plans.getId())) {
            this.getReimbursementRequeriment().addMessage("Error", "DISCULPE, UD. HA GENERADO LA TOTALIDAD DE SOLICITUDES PERMITIDAS POR ESTE AÑO O YA POSEE SOLICITUDES DE REEMBOLSO EN PROCESO.");
            this.getReimbursementRequeriment().clearForm();
        } else {
            
            this.getReimbursementRequeriment().setNumberRequest(this.reimbursementFacadeLocal.generateRequestNumber(new Date()));
            
            sendForm.setNumberRequest(this.getReimbursementRequeriment().getNumberRequest());
            sendForm.setDate_request(new Date());
            sendForm.setPlansId(plans);
            sendForm.setPatientsId(patients.get(0));
            sendForm.setBanks_patients_id(banksPatients);
            
            if (this.getReimbursementRequeriment().getCompany().length() != 0 && !this.getReimbursementRequeriment().getCompany().equalsIgnoreCase("") && this.getReimbursementRequeriment().getCompany() != null) {
                sendForm.setCompany(this.getReimbursementRequeriment().getCompany());
            }
            
            sendForm.setInvoice_number(this.getReimbursementRequeriment().getInvoiceNumber());
            sendForm.setAmount(this.getReimbursementRequeriment().getInvoiceAmount());
            sendForm.setInvoice_date(this.getReimbursementRequeriment().getInvoiceDate());
            sendForm.setResponse_email(this.getReimbursementRequeriment().getEmail());
            sendForm.setResponse_phone(this.getReimbursementRequeriment().getPhone());
            sendForm.setResponse_cellphone(this.getReimbursementRequeriment().getCellular());
            sendForm.setStatusReimbursmentId(statusReimbursement);
            sendForm.setMonto_remb_pagar(plans.getReimbursement_amount());
            this.getReimbursementRequeriment().setReimbursementSave(this.reimbursementFacadeLocal.create(sendForm));
            createRequirement();
            createBinnacle();
            clearRequirements();
            this.getReimbursementRequeriment().sendMailPatient();
            this.getReimbursementRequeriment().addMessage("Información", "Se ha generado la solicitud de reembolso nro. " + this.getReimbursementRequeriment().getReimbursementSave().getNumberRequest());
            this.getReimbursementRequeriment().clearForm();
            
        }
        
    }

    /**
     * Method to create a new requirement of reimbursement
     *
     * @throws java.io.IOException
     * @throws java.text.ParseException
     */
    public void createRequirement() throws IOException, ParseException {
        this.getReimbursementRequeriment().createPdf();
        
        Requirement sendRequirement = new Requirement();
        
        sendRequirement.setReimbursementId(this.getReimbursementRequeriment().getReimbursementSave());
        
        sendRequirement.setDateOfReception(this.getReimbursementRequeriment().getReimbursementSave().getDate_request());
        
        sendRequirement.setFileName(this.getReimbursementRequeriment().getFileName());
        
        sendRequirement.setFileSystemPath(this.getReimbursementRequeriment().getFilePath());
        
        sendRequirement.setFileType("document/pdf");
        
        sendRequirement.setFileSize(this.getReimbursementRequeriment().getFileSize());
        //sendRequirement.
        this.requirementFacadeLocal.create(sendRequirement);
        
    }

    /**
     * Method to create a binnacle of reimbursement
     *
     * @throws java.text.ParseException
     * @throws java.io.IOException
     */
    public void createBinnacle() throws ParseException, IOException {
        BinnacleReimbursement sendBinnacleReimbursement = new BinnacleReimbursement();
        StatusReimbursement statusReimbursement = this.statusReimbursmentFacadeLocal.findById(StatusReimbursementEnum.SOLICITADO.getValor());
        sendBinnacleReimbursement.setReimbursementId(this.getReimbursementRequeriment().getReimbursementSave());
        sendBinnacleReimbursement.setStatusReimbursementId(statusReimbursement);
        sendBinnacleReimbursement.setDate(new Date());
        sendBinnacleReimbursement.setUserId(this.getReimbursementRequeriment().getReimbursementSave().getPatientsId().getId());
        this.binnacleReimbursementFacadeLocal.create(sendBinnacleReimbursement);
        
    }
    
    public void clearRequirements() {
        this.getReimbursementRequeriment().setIdentityHolder(false);
        this.getReimbursementRequeriment().setIdentityBeneficiary(false);
        this.getReimbursementRequeriment().setMedicalReport(false);
        this.getReimbursementRequeriment().setOpticalFormula(false);
        this.getReimbursementRequeriment().setInvoiceReport(false);
    }
    
    public Boolean recordable() {
        Integer idTypeAtenncion = TypeSpecialistsEnums.OFTALMOLOGIA.getValor();
        switch (this.getReimbursementRequeriment().getReimbursementHolder()) {
            case "si":
                for (int i = 0; i < this.getReimbursementRequeriment().getPatientsList().size(); i++) {
                    if (this.getReimbursementRequeriment().getPatientsList().get(i).getRelationshipId().getId() == 10) {
                        this.setPlansVerificatorList(this.plansFacadeLocal.findPlansAvailableByPatient(this.getReimbursementRequeriment().getPatientsList().get(i).getId(), idTypeAtenncion, 1));
                        break;
                    }
                }
                
                break;
            case "no":
                for (int i = 0; i < this.getReimbursementRequeriment().getPatientsList().size(); i++) {
                    if (this.getReimbursementRequeriment().getPatientsList().get(i).getRelationshipId().getId() == 10) {
                        this.setPlansVerificatorList(this.plansFacadeLocal.findPlansAvailableByPatient(this.reimbursementRequeriment.getBeneficiarySelected().getId(), idTypeAtenncion, 0));
                        break;
                    }
                }
                
                break;
        }
        if (this.getPlansVerificatorList().isEmpty()) {
            return true;
        }
        
        return false;
    }
}
