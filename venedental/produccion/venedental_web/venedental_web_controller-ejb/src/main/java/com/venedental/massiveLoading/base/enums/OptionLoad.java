/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.massiveLoading.base.enums;

/**
 *
 * @author akdesk01
 */
public enum OptionLoad {

    CargaCompleta(0, "COMPLETA"),
    Inclusiones(1, "INCLUSIONES"),
    Exclusiones(2, "EXCLUSIONES");
   

    private final  Integer order;
    private final  String name;
   

    private OptionLoad(Integer order, String name) {
        this.order = order;
        this.name = name;
        
    }

    public Integer getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

  
}
