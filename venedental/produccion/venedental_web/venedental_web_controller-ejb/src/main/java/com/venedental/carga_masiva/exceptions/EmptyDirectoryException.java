/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.carga_masiva.exceptions;

import com.venedental.carga_masiva.base.Messages;
import java.io.File;

/**
 *
 * @author usuario
 */
public class EmptyDirectoryException extends FileException {

    public EmptyDirectoryException(File file) {
        super(Messages.EXCEPTION_EMPTY_DIRECTORY, file);
    }

    public EmptyDirectoryException(File file, Throwable throwable) {
        super(Messages.EXCEPTION_EMPTY_DIRECTORY, file, throwable);
    }
}
