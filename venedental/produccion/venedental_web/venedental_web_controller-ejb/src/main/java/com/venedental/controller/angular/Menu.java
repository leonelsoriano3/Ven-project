package com.venedental.controller.angular;

import com.venedental.dto.createUser.UserByLoginDTO;
import com.venedental.dto.menu.MenuOut;
import com.venedental.services.ServiceCreateUser;
import com.venedental.services.ServiceMenu;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ejb.EJB;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by leonelsoriano3@gmail.com on 18/07/16.
 */
@ApplicationPath("/resources")
@Path("menu")
public class Menu {

    @EJB
    private ServiceMenu serviceMenu;

    @EJB
    private ServiceCreateUser serviceCreateUser;

    @Path("getMenuByUserId")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MenuOut> getMenuByUserId(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserByLoginDTO user = serviceCreateUser.findUserByLogin(auth.getName());
  //      UserByLoginDTO user = serviceCreateUser.findUserByLogin("jestanga");
        List<MenuOut> menuOutList = serviceMenu.findMenuByUserId(user.getId());

        return  menuOutList;
    }

}
