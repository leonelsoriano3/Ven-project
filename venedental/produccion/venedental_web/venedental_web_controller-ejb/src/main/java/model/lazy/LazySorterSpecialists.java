/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model.lazy;

import com.venedental.model.Specialists;
import org.primefaces.model.SortOrder;
import java.util.Comparator;

/**
 *
 * @author SpecialistslosDaniel
 */
public class LazySorterSpecialists implements Comparator<Specialists> {
 
    private final String sortField;
     
    private final SortOrder sortOrder;
     
    public LazySorterSpecialists(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Specialists specialists1, Specialists specialists2) {
        try {
            Object value1 = Specialists.class.getField(this.sortField).get(specialists1);
            Object value2 = Specialists.class.getField(this.sortField).get(specialists2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
