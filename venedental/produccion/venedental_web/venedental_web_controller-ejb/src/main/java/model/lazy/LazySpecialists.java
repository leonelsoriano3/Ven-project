/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.lazy;

import com.venedental.model.Specialists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author akdesarrollo
 */
public class LazySpecialists extends LazyDataModel<Specialists> {

    private List<Specialists> listaCompleta;

    public LazySpecialists(List<Specialists> listaLazySpecialists) {
        this.listaCompleta = listaLazySpecialists;
    }

    public List<Specialists> getListaCompleta() {
        return listaCompleta;
    }

    public void setListaCompleta(List<Specialists> listaCompleta) {
        this.listaCompleta = listaCompleta;
    }

    @Override
    public Specialists getRowData(String rowKey) {
        for (Specialists especialista : listaCompleta) {
            if (especialista.getId().toString().equals(rowKey)) {
                return especialista;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Specialists especialista) {
        return especialista.getId();
    }
    
    @Override
    public List<Specialists> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Specialists> data = new ArrayList<>();
 
        //filter
        for(Specialists specialists : listaCompleta) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(specialists.getClass().getField(filterProperty).get(specialists));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(specialists);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new LazySorterSpecialists(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }

}
