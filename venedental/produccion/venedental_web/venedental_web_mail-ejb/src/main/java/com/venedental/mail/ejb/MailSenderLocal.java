/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.mail.ejb;

import com.venedental.mail.base.enums.Email;
import java.io.File;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author usuario
 */
@Local
public interface MailSenderLocal {

    String getUsername(Email email);

    boolean send(Email email, String subject, String text, List<File> files, String... receiverList);
    
    boolean send(Email email, String subject, String title, String text, List<File> files, String... receiverList);

}
