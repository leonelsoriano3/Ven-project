/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.mail.utils;

import com.venedental.mail.base.Constants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author usuario
 */
public class PropertiesConfiguration {

    private static Properties props;

    static {
        props = new Properties();
        try {
            PropertiesConfiguration util = new PropertiesConfiguration();
            props = util.getPropertiesFromClasspath("config.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String get(String key) {
        return props.getProperty(key);
    }

    private Properties getPropertiesFromClasspath(String propFileName)
            throws IOException {
        Properties myProp = new Properties();
        InputStream inputStream
                = this.getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName
                    + "' not found in the classpath");
        }

        myProp.load(inputStream);
        return myProp;
    }

    public static Properties getProperties() {
        return props;
    }

}
