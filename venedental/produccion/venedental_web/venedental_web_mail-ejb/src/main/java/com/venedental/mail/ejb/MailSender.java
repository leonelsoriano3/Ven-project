/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.mail.ejb;

import com.venedental.mail.base.Constants;
import com.venedental.mail.base.CustomLogger;
import com.venedental.mail.base.enums.Email;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author usuario
 */
@Stateless
public class MailSender implements MailSenderLocal {
    
    @Resource(name = "mail/veneden_reembolso")
    private Session sessionVenedenReembolso;
    
    @Resource(name = "mail/veneden_carga-masiva")
    private Session sessionVenedenCargaMasiva;
    
    @Resource(name = "mail/veneden_reporte-pago")
    private Session sessionVenedenReportePago;

    @Resource(name = "mail/veneden_credenciales")
    private Session sessionVenedenCredenciales;
    
    private static final Logger log = CustomLogger.getLogger(MailSender.class.getName());
    
    private Session getSession(Email email) {
        if (Email.MAIL_CARGA_MASIVA == email) {
            return sessionVenedenCargaMasiva;
        }else if(Email.MAIL_REPORTE_PAGO == email){
            return sessionVenedenReportePago;
        }
        else if(Email.MAIL_CREDENCIALES == email){
            return sessionVenedenCredenciales;
        }
        return sessionVenedenReembolso;
    }
    
    @Override
    public String getUsername(Email email) {
        String username = "";
        try {
            username = getSession(email).getTransport().getURLName().getUsername();
        } catch (NoSuchProviderException ex) {
            log.log(Level.SEVERE, null, ex);
        }
        return username;
    }
    
    @Override
    public boolean send(final Email email, String subject, String text, List<File> files, String... receiverList) {
        return send(email, subject, subject, text, files, receiverList);
    }
    
    @Override
    public boolean send(final Email email, final String subject, final String title, final String text, final List<File> files, final String... receiverList) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                sendMail(email, subject, title, text, files, receiverList);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        return true;
    }
    
    private void sendMail(Email email, String subject, String title, String text, List<File> files, String... receiverList) {

        try {

            MimeMessage message = new MimeMessage(getSession(email));
            for (String receiver : receiverList) {
                log.log(Level.INFO, "ENVIANDO CORREO A: {0}", receiver);
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
            }

            message.setSubject(subject);
            message.setContent(buildContent(title, text, files));
            Transport.send(message);

            log.log(Level.INFO, "CORREO ENVIADO EXITOSAMENTE");
        } catch (MessagingException ex) {
            log.log(Level.SEVERE, "ERROR ENVIANDO EL CORREO", ex);
        }
    }
    
    private MimeMultipart buildContent(String title, String text, List<File> files) throws MessagingException {
        MimeMultipart messageMultipart = new MimeMultipart(Constants.MAIL_MULTIPART_RELATED);
        
        BodyPart partHtmlMessage = buildContentHtml(title, text);
        messageMultipart.addBodyPart(partHtmlMessage);
        
        BodyPart partImage = buildContentImage();
        if (partImage != null) {
            messageMultipart.addBodyPart(partImage);
        }
        
        List<BodyPart> attachments = buildAttachments(files);
        for (BodyPart partAttachment : attachments) {
            messageMultipart.addBodyPart(partAttachment);
        }
        
        return messageMultipart;
    }
    
    private BodyPart buildContentHtml(String title, String text) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        StringBuilder sb = new StringBuilder();
        sb.append("<div>");
        sb.append(buildHeader(title));
        sb.append(text);
        sb.append("</div>");
        bodyPart.setContent(sb.toString(), Constants.MAIL_TYPE_CONTENT);
        return bodyPart;
    }
    
    private String buildHeader(String title) {
        StringBuilder sb = new StringBuilder();
        sb.append("<div style=\"text-align:center\">").append("<img src=\"cid:image\">").append("</div>");
        sb.append("<div style=\"text-align:center; color: black;\">").append("<h2><b>").append(title).append("</b></h2>").append("</div>");
        return sb.toString();
    }
    
    private List<BodyPart> buildAttachments(List<File> files) throws MessagingException {
        List<BodyPart> list = new ArrayList<>();
        if (files != null) {
            for (File file : files) {
                BodyPart bodyPart = attachment(file);
                if (bodyPart != null) {
                    list.add(bodyPart);
                }
            }
        }
        return list;
    }
    
    private BodyPart attachment(File file) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        if (file.exists()) {
            DataSource source = new FileDataSource(file.getPath());
            bodyPart.setDataHandler(new DataHandler(source));
            bodyPart.setFileName(file.getName());
            return bodyPart;
        }
        return null;
    }
    
    private BodyPart buildContentImage() throws MessagingException {
        try {
            BodyPart bodyPart = new MimeBodyPart();
            DataSource imageSource = findImage();
            bodyPart.setDataHandler(new DataHandler(imageSource));
            bodyPart.setHeader(Constants.MAIL_CONTENT_ID, Constants.MAIL_TAG_IMAGE);
            return bodyPart;
        } catch (Exception e) {
            return null;
        }
    }
    
    private DataSource findImage() throws Exception {
        URL url = this.getClass().getClassLoader().getResource(Constants.IMAGE_LOGO_PATH);
        BufferedImage img = ImageIO.read(url);
        File file = new File("logo.png");
        ImageIO.write(img, "png", file);
        return new FileDataSource(file);
    }
    
}
