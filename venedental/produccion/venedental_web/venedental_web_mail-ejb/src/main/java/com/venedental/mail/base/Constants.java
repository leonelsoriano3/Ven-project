/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.mail.base;

/**
 *
 * @author usuario
 */
public class Constants {

    public static String PROPERTIES_PATH = "src/main/resources/";
    public static String FILENAME_CONFIGURATION = "configuration.properties";

    public static String PROPERTY_STATUS_LOGGER = "logger";

    public static String STATUS_ON = "on";
    public static String STATUS_OFF = "off";

    public static String MAIL_TYPE_CONTENT = "text/html";
    public static String MAIL_MULTIPART_RELATED = "related";
    public static String MAIL_CONTENT_ID = "Content-ID";
    public static String MAIL_TAG_IMAGE = "<image>";
    public static String IMAGE_LOGO_PATH = "img/logo_white.png";

}
