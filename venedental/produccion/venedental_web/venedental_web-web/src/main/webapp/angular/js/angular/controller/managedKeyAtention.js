/* global minDat */

/**
 * Created by leonelsoriano3@gmail.com on 13/07/16.
 * .update by luis.carrasco@arkiteck.biz on 3/8/16
 */





var app = angular.module("AKmodule", ["angular-growl", "angular.jquery", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap', 'daterangepicker', 'ngTable']);

app.controller("AKctrl", function ($scope, growl, $window, $http, $sce, deleteKeyDialogService, redirecFactory, ConfirmationDialogService,
        detalleClaveDialogService, NgTableParams) {
    $scope.ocultar = true;
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        // console.log("Error Interno");
    });

    $(".growl").css("z-index", "-1");
    $scope.isDisabledButtSearch = true;
    $scope.isDisabledButtRestore = true;
    $scope.isDisabledButtSave = true;
    $scope.isHiddenMessageDefaultTable = false;
    $scope.isHiddenMessageDefault = false;
    $scope.search = {};
    $scope.searchTratament = {};
    $scope.isDisabledEndDate = true;

    $scope.showAjax = false;

    $scope.endDate = "";
    //  $scope.maxDate = new Date();
    //$scope.minDate = "2016-06-29";
   // $scope.mineDate = "2016/06/29";


    $scope.startDate = "";
    $scope.myLabel = "hola";
    $scope.idKey = "";


    $scope.idCardOrName = "";
    $scope.numKey = "";
    $scope.Report = "";
    $scope.idKeyDet ="";



    $scope.keys = "";
    $scope.numberKeyDetail = "";
    $scope.namespecialistDetail = "";
    $scope.detailkeys = "";
    $scope.detailTreatments = "";
    $scope.namePatient = "";
    $scope.identityNumberPatient = "";
    $scope.numberKey = "";
    $scope.dateKey = "";
    $scope.numberkeyDetail = "";
    $scope.appdateDetail = "";
    $scope.birthdateDetail = "";
    $scope.consultingnameDetail = "";
    $scope.specialityDetail = "";
    $scope.namepatientDetail = "";
    $scope.relationshipDetail = "";
    $scope.insuranceDetail = "";
    $scope.planDetail = "";
    $scope.expirationKeyDetail = "";
    $scope.sytleScroll = "";
    $scope.styleScrollTable = "";
    $scope.Key="";
    $scope.specialist = "";
    $scope.name = "";
    $scope.nameEnte="";
    $scope.idEspecialidad = "";
    $scope.idEspecialista = "";
    $scope.idSpecialist = "";
    $scope.diagnosticoDataTable = "";
    $scope.keyDelete = 0;
    $scope.idKeyForDelete = "";
    $scope.idKeyForDetail = "";
    $scope.dateFilter = "";
    $scope.dateFilterDetail = "";
    $scope.patient="";
    $scope.idSpecialistParameter = "";
    $scope.idSpecialistforFind = "";
    $scope.patientParameter = "";
    $scope.keyParameter = "";
    $scope.dateStartParameter = "";
    $scope.dateEndParameter = "";
    $scope.startdatekeys = "";
    $scope.enddatekeys = "";
    $scope.dateMinFilter = "";
    $scope.dateEndFilter = "";
    $scope.idnumberKeyRedi="";
    $scope.idDateStartRedi ="";
    $scope.idDateEndRedi ="";
    $scope.idPatientRedi ="";
    $scope.keysParamRedi ="";
    $scope.isDisabledBtnDelete=false;
    $scope.validateButtSaveDeleteKey=true;
$scope.observation="";


    $scope.specialist = "";
    $scope.name = "";
    $scope.idEspecialidad = "";
    $scope.idEspecialista = "";
    $scope.isVisiblebtnRD = "";
    $scope.isVisiblebtnCD = "";
    var dateDetail = null;
    var dateTable = null;
//    $scope.dateMiaxTreatment = "";
//    $scope.dateMinTreatment = "";

    $scope.hideTableDetail = true;

    $scope.date = {startDate: null, endDate: null};

    $scope.deleteOnly = function (evt) {
        // console.log("Borrar");
        var key = evt.which;
        key = String.fromCharCode(key);
        var regex = /^[\b]+$/;
        if (regex.test(key)) {
            // console.log("Borrar activo");
            $scope.search.dateKeyTable = '';
            $scope.searchTratament.dateFilterTreatment = '';
            $('#datefilterDetail').val('');
            // setTimeout(function() {
            //     $("#datefilterDetail").trigger("click");
            //      $scope.evalArrayDetail();
            // }, 250);
            $scope.getDetailTreatment($scope.idKeyDet);
        }
    }


    // $scope.deleteOnly = function(evt){
    //
    //     var key = evt.which;
    //     key = String.fromCharCode(key);
    //     var regex = /^[\b]+$/;
    //     if (regex.test(key)) {
    //         $('#datefilterDetail').val('');
    //         $scope.searchReport.dateKeyS='';
    //         $scope.searchTratament.dateFilterTreatment='';
    //         $('#datefilterDetail').val('');
    //         $("#datefilterDetail").trigger("click");
    //         $("#datefilterDetail").trigger("click");
    //     }
    // }

    $('#datefilterDetail').keyup(function(e){
        if(e.keyCode == 8)
            $scope.deleteOnly(e);

    })
    $scope.deleteOnlyDateFilter = function (evt) {
        var key = evt.which;
        key = String.fromCharCode(key);
        var regex = /^[\b]+$/;
        if (regex.test(key)) {
            $scope.search.dateKeyTable = '';
            $('#datefilter').val('');
            $scope.searchTratament.dateFilterTreatment = '';
            $('#datefilter').val('');
            // $scope.$digest();
            // setTimeout(function() {
                // $("#datefilter").trigger("click");
                // $scope.evalTable();
                // $("#datefilter").keyup();
            //     var e = $.Event('keypress');
            //     e.which = 65; // Character 'A'
            //     $('#datefilter').trigger(e);
            // }, 500);
            $scope.reloadRangeCalendar();
            setTimeout(function() {
                // $("#datefilter").trigger("click");
                // $("#datefilter").trigger("click");
                // $scope.evalTable();
                // $("#datefilter").keyup();
                var e = $.Event('keydown');
                e.which = 65; // Character 'A'
                $('#datefilter').trigger(e);
            }, 500);

        }
        // $("#datefilter").trigger("click");
        // $scope.evalTable();
    }

    $scope.reset = function () {
        $scope.idCardOrName = "";
        $scope.numKey = "";
    }

    $scope.enableButtGenRep = function () {



    }

    $scope.enableEndDate = function () {

        $("#startDate").val($scope.startdatekeys);

        $scope.isDisabledEndDate = false;

    };

    //cerrar dialog
    $scope.closeDialog = function closeDialog() {
        limpiar();
        deleteKeyDialogService.closeDialog({});
    };

    function limpiar() {
        $scope.observation = "";
    }

    $scope.reloadRangeCalendar = function(){


        var dateMinFilter = $scope.dateMinFilter;
        var dateEnFilter = $scope.dateEndFilter;
        $("#datefilter").daterangepicker({
            onSelect: function ()
            {
                this.focus();

            },
            autoUpdateInput: false,
            minDate: dateMinFilter,
            maxDate: dateEnFilter,
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY',
                "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]

            }
        }, function (start, end, label) {
            $scope.search.dateKeyTable = start.format('DD/MM/YYYY');
            if( ($scope.keys != null) && ($scope.keys.length != 0)) {
                $scope.isHiddenMessageDefaultTable=false;
            }else{
                $scope.isHiddenMessageDefaultTable=true;
                $scope.isDisabledButtSearch=true;
            }
            $scope.$digest();
            $("#datefilter").val(start.format('DD/MM/YYYY'));
            var e = $.Event('keydown');
            e.which = 65; // Character 'A'
            $('#datefilter').trigger(e);
        });
    }

    $scope.testDialog = function (idKey) {

        $scope.idKeyForDelete = idKey;
        deleteKeyDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
                // alert('You entered ' + result.name);
            }
        });
    };
    $scope.closeDialogDetailKey = function closeDialog() {
        detalleClaveDialogService.closeDialog({});
    }



    $scope.testDialogDetailKey = function () {

        detalleClaveDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
                // alert('You entered ' + result.name);
            }
        });
    };





    $http({
        url: '../resources/ConsultKeysSpecialist/findDataSpecialist',
        method: 'GET',
        params: {
        }

    }).then(function success(response) {
        //console.log(response.data.nameSpecialist);
        $scope.specialist = response.data;
        var array = $scope.specialist;
        for (var i = 0; i < array.length; i++) {
            $scope.name = array[0].nameSpecialist;
            $scope.idEspecialidad = array[0].idSpeciality;
            $scope.nameSpecialist = array[0].nameSpecialist;
            $scope.nameSpeciality = array[0].nameSpeciality;
            $scope.idEspecialista = array[0].id_Specialist;
            $scope.idSpecialist = array[0].id_Specialist;
            $scope.idSpecialist = array[0].id_Specialist;
            $scope.idSpecialistforFind = array[0].id_Specialist;
            // console.log($scope.idSpecialistforFind);
            


        }

    }, function myError(response) {
        console.log("hay un error buscando especialista" + response.data);

    });
    
   //metodo para buscar la ultima consulta, cuando se redirecciona desde registrar tratamiento 
     $scope.getConsultKeys = function () {

        $scope.showAjax = true;

        $http({
        url: '../resources/ConsultKeysSpecialist/findDataSpecialist',
        method: 'GET',
        params: {
        }

    }).then(function success(response) {
        //console.log(response.data.nameSpecialist);
        $scope.specialist = response.data;
        var array = $scope.specialist;
        for (var i = 0; i < array.length; i++) {
            $scope.name = array[0].nameSpecialist;
            $scope.idEspecialidad = array[0].idSpeciality;
            $scope.nameSpecialist = array[0].nameSpecialist;
            $scope.nameSpeciality = array[0].nameSpeciality;
            $scope.idEspecialista = array[0].id_Specialist;
            $scope.idSpecialist = array[0].id_Specialist;
            $scope.idSpecialist = array[0].id_Specialist;
            $scope.idSpecialistforFind = array[0].id_Specialist;
            // console.log($scope.idSpecialistforFind);
             $scope.getKeys($scope.idSpecialistforFind,$scope.idDateStartRedi,$scope.idDateEndRedi,$scope.idnumberKeyRedi,$scope.idPatientRedi);
            $scope.isDisabledButtRestore=false;

        }

    }, function myError(response) {
        console.log("hay un error buscando especialista" + response.data);

    });
    
     }

redirecFactory.getParameterDelete("managedKeyAtention.html", ['1'], 'index.xhtml').then(function (resolve) {
        
        $scope.keysParamRedi= resolve[0].value;
        var variable=$scope.keysParamRedi.split(",");
        if(variable[0]=="patient"){
            var patient=variable[1];
           $scope.idPatientRedi=patient;
            $scope.idnumberKeyRedi="";
            $scope.idDateStartRedi=null;
           $scope.idDateEndRedi=null;
           var redirec=1;
        }else if(variable[0]=="number"){
            $scope.idPatientRedi="";
            $scope.idnumberKeyRedi=variable[1];
            $scope.idDateStartRedi=null;
           $scope.idDateEndRedi=null;
           var redirec=1;
        }else if(variable[0]=="date"){
            $scope.idPatientRedi="";
             $scope.idnumberKeyRedi="";
             
           $scope.idDateStartRedi=variable[1];
           $scope.idDateEndRedi=variable[2];
           var redirec=1;
        }else{
            var redirec=0;
        }
       $scope.getConsultKeys();
       resolve=[];
         
    });
    


    $scope.getKeys = function (idSpecialistforFind, startdatekeys, enddatekeys, Key, patient) {
        $scope.idSpecialistParameter = idSpecialistforFind;
        
 //$("#endDate").val($scope.enddatekeys);
 
  // console.log("$scope.enddatekeys");
  // console.log($scope.enddatekeys);
  //       console.log("LULAPALUZA");
  //       console.log(idSpecialistforFind);
  //        console.log("startdatekeys");
  //       console.log(startdatekeys);
  //        console.log("enddatekeys");
  //       console.log(enddatekeys);
  //       console.log(Key);
  //       console.log(patient);

        $scope.idSpecialistParameter = idSpecialistforFind;
        $scope.patientParameter = patient;
        $scope.keyParameter = Key;
        
        $scope.dateStartParameter = startdatekeys;
       // console.log("$scope.dateStartParameter");
       //  console.log($scope.dateStartParameter);
        $scope.dateEndParameter = enddatekeys;
        //  console.log("$scope.dateEndParameter");
        // console.log($scope.dateEndParameter);
        
        $scope.showAjax = true;
         
        $scope.hideTableDetail = false;
        $scope.search = {};
        $("#datefilter").val('');
        
        $http({
            url: '/resources/ConsultKeysSpecialist/findKeys',
            method: 'GET',
            params: {
                idSpecialistforFind: idSpecialistforFind,
                startdatekeys: startdatekeys,
                enddatekeys: enddatekeys,
                Key: Key,
                patient: patient
            }
        }).then(function succes(response) {
              $("#startDate").val($scope.startdatekeys);
             $("#endDate").val($scope.enddatekeys);
            $scope.keys = response.data;
              // console.log("$scope.keys");
            // console.log($scope.keys);
            $scope.idPatientRedi=null;
            $scope.idnumberKeyRedi=null;
            $scope.idDateStartRedi=null;
           $scope.idDateStartRedi=null;
        if( ($scope.keys != null) && ($scope.keys.length != 0)) {  
            // console.log("entro")
    $scope.dateMinFilter = response.data[0].dateMinFilter;
    $scope.dateEndFilter = response.data[0].dataEndDateFilter;
    $scope.isHiddenMessageDefaultTable=false;


            var dateMinFilter = $scope.dateMinFilter;
            var dateEnFilter = $scope.dateEndFilter;
            $("#datefilter").daterangepicker({
                onSelect: function ()
                {
                    this.focus();
                },
                autoUpdateInput: false,
                minDate: dateMinFilter,
                maxDate: dateEnFilter,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]

                }
            }, function (start, end, label) {
                $scope.search.dateKeyTable = start.format('DD/MM/YYYY');
                if( ($scope.keys != null) && ($scope.keys.length != 0)) { 
                     $scope.isHiddenMessageDefaultTable=false;
                }else{
                    $scope.isHiddenMessageDefaultTable=true;
                    $scope.isDisabledButtSearch=true;
                }
                $scope.$digest();
                $("#datefilter").val(start.format('DD/MM/YYYY'));
                var e = $.Event('keydown');
                e.which = 65; // Character 'A'
                $('#datefilter').trigger(e);
            });
}else if(($scope.keys == null) && ($scope.keys.length == 0)){
    $scope.isHiddenMessageDefaultTable=true;
}

            $scope.showAjax = false;
            $scope.isDisabledEndDate = true;

            this.tableParams = new NgTableParams({}, {dataset: $scope.keys});
            $scope.isDisabledButtSearch = true;
            if ($scope.keys !== null && $scope.keys.length !== 0) {
                $scope.isHiddenMessageDefaultTable = true;
                
               

            } else if ($scope.keys === null || $scope.keys.length === 0) {
                $scope.isHiddenMessageDefaultTable = false;
            }
//            if ($scope.keys.length > 10) {
//                $scope.styleScrollTable = "scrollGeneral";
//
//            } else {
//
//                $scope.styleScrollTable = "";
//
//            }
        }, function Error(response) {
            $scope.isHiddenMessageDefaultTable = false;
            console.log("Error buscando claves" + response.statusText);
            $scope.showAjax = false;

        });

    }



    $scope.rest = function () {
            setTimeout(function () {
                document.location.pathname = "web/managedKeyAtention.html";
            },1);

        $scope.startdatekeys = "";
        $("#endDate").val('');
        // $("#startDate").val('');
        $("#endDate").attr("disabled", true);
        $scope.enddatekeys = "";
        $("#datefilter").val('');
        $scope.patient = "";
        $scope.Key = "";
        $scope.keys = null;
        $scope.observation = "";
        $scope.isDisabledButtRestore = true;
        $scope.isDisabledButtSearch = true;
        $scope.isHiddenMessageDefaultTable = false;
        $scope.styleScrollTable = "";
        $scope.hideTableDetail = true;
        $("#startDate").val($scope.startdatekeys);
        $("#endDate").val($scope.enddatekeys);
    }

    $scope.chargeDetailKeys = function (idKey) {

        $scope.getDetailKeys(idKey);
        $scope.getDetailTreatment(idKey);
       // console.log($scope.getDetailTreatment(idKey));


    }


    $scope.enablButtFind = function () {
       
            if($scope.patient.length>0 || $scope.Key.length>0||$scope.startdatekeys != null || $scope.enddatekeys != null){
                 $("#startDate").val($scope.startdatekeys);
                  $("#endDate").val($scope.enddatekeys);
            
        if ($scope.patient != "" || $scope.Key != ""  || $scope.enddatekeys !="") {
           
            $scope.isDisabledButtSearch = false;
            $scope.isDisabledButtRestore = false;

        } 
        }else {
            $scope.isDisabledButtSearch = true;
            $scope.isDisabledButtRestore = true;
        }



    }


    $scope.getDetailKeys = function (idKey) {

        $scope.showAjax = true;

        $http({
            url: '/resources/ConsultKeysSpecialist/cargarDetalleClave',
            method: 'GET',
            params: {
                idKey: idKey,
            }

        }).then(function succes(response) {
            $scope.detailkeys = response.data;

            $scope.numberKeyDetail = $scope.detailkeys.numberKey;

            $scope.appdateDetail = $scope.detailkeys.dateKeyForDetail;
            $scope.birthdateDetail = $scope.detailkeys.brithayPatientDetail;
            $scope.namespecialistDetail = $scope.detailkeys.nameSpecialist;
            $scope.consultingnameDetail = $scope.detailkeys.nameConsultorio;
            $scope.specialityDetail = $scope.detailkeys.specialitySpecialist;
            $scope.namepatientDetail = $scope.detailkeys.namePatient;
            $scope.identityNumberPatient = $scope.detailkeys.numberForIdentityPatient;
            $scope.relationshipDetail = $scope.detailkeys.parentesco;
            $scope.insuranceDetail = $scope.detailkeys.nameAseguradora;
            $scope.expirationKeyDetail = $scope.detailkeys.expirationKeyDetail;
            $scope.planDetail = $scope.detailkeys.namePlan;
            $scope.nameEnte=$scope.detailkeys.nameEnte;



        }, function Error(response) {
            console.log("Error");
            $scope.showAjax = false;
        });
    }



    $scope.getDetailTreatment = function (idKey) {

        $("#datefilterDetail").val('');
        $scope.showAjax = true;

        $http({
            url: '/resources/ConsultKeysSpecialist/cargarDetalleClaveTratamiento',
            method: 'GET',
            params: {
                idKey: idKey,
            }

        }).then(function succes(response) {

            $scope.searchTratament = {};
            $scope.detailTreatments = response.data;
            // console.log($scope.detailTreatments);

            $scope.testDialogDetailKey();
            $scope.showAjax = false;

            if ($scope.detailTreatments !== null && $scope.detailTreatments.length !== 0) {
                $scope.isHiddenMessageDefault = true;
                // console.log($scope.isHiddenMessageDefault);

            } else if ($scope.detailTreatments === null || $scope.detailTreatments.length === 0) {
                $scope.isHiddenMessageDefault = false;
            }
            
            $scope.dateMinYmaxDetail(idKey);
            $scope.idKeyDet = idKey;
              if ($scope.detailTreatments.length > 5) {
                  $scope.sytleScroll = "styleScroll";
                  

            } else {

                $scope.sytleScroll = "";

            }
        }, function Error(response) {
            $scope.isHiddenMessageDefault = false;
            // console.log("Error");
            $scope.showAjax = false;
        });
    }

    $scope.redirectToRegisterTreat = function (idKey,startdatekeys,enddatekeys,Key,patient,keys) {
        var param="";
       if(patient!=""){
           param="patient,"+patient;
       }else if(Key!=""){
           param="number,"+Key;
       }else{
           param="date,"+startdatekeys+","+enddatekeys;
       }

        redirecFactory.redirectWithParam("registerTreatment.html", [{'key': '1', 'value': idKey+","+param}]);
    }

    $scope.redirectToConsultDiagnosis = function (idKey) {

        redirecFactory.redirectWithParam("consultDiagnosis.html", [{'key': '1', 'value': idKey}]);

    }




    $scope.redirect = function (idKey) {

        redirecFactory.redirectWithParam("registerDiagnosis.html", [{'key': '1', 'value': idKey}]);

    }

    $scope.deleteKey = function (idKeyForDelete, observation) {
       
        $http({
            url: '/resources/ConsultKeysSpecialist/deleteKey',
            method: 'GET',
            params: {
                idKeyForDelete: idKeyForDelete,
                observation: observation
            }

        }).then(function succes(response) {
            $scope.keyDelete = response.data;
            // console.log($scope.idSpecialistParameter);
            // console.log($scope.patientParameter);
            // console.log($scope.keyParameter);
            // console.log($scope.dateStartParameter);
            // console.log($scope.dateEndParameter);
            $scope.getKeys($scope.idSpecialistParameter, $scope.dateStartParameter, $scope.dateEndParameter, $scope.keyParameter, $scope.patientParameter);

            ConfirmationDialogService.closeDialog({});
            $scope.closeDialog();
            $scope.observation = "";
            $scope.mostrarMensajeExito();
            $scope.showAjax = false;




        }, function Error(response) {
            console.log("Error");
            $scope.showAjax = false;
        });

    }

    $scope.createMessages = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
                growMsg($scope.msgGrow, isError)
                ), config);
        arriba();
    };


    $scope.closeYesConfirmationDeleteKey = function closeDialogConfirm() {

        ConfirmationDialogService.closeDialog({});

    }

    $scope.closeNoConfirmationDeleteKey = function closeDialogConfirm() {

        ConfirmationDialogService.closeDialog({});

    }



    $scope.openConfirmationDeleteKey = function (idKeyForDelete) {

        ConfirmationDialogService.openDialog({}).then(function (result) {
            if (result.ok) {

                // alert('You entered ' + result.name);
            }
        });
    };

    $scope.mostrarMensajeExito = function () {

        $scope.msgGrow = "OPERACIÓN REALIZADA CON ÉXITO!";
        $scope.observation = "";
        $scope.createMessages(false);
        abajo();
    }

    $scope.updateTable = function () {
// console.log("entro update");
        $scope.getKeys($scope.idSpecialistParameter, $scope.dateStartParameter, $scope.dateEndParameter, $scope.keyParameter, $scope.patientParameter);
        $scope.msgGrow = "ACTUALIZACIÓN REALIZADA CON EXITO!";
        $scope.createMessages(false);
        abajo();
    }

    $scope.validateButtSaveDeleteKey = function (observation) {
        if ((observation != null)||(observation !="")) {
            $scope.isDisabledButtSave = false;
        } else {
            $scope.isDisabledButtSave = true;
        }
        if (observation =="") {
            $scope.isDisabledButtSave = true;
        }
        if(observation.length!=0){
            $scope.isDisabledButtSave = false;
        }else{
            $scope.isDisabledButtSave = true;
        }


    }

    function toDate(dateStr) {
        var parts = dateStr.split("-");
        return new Date(parts[0], parts[1] - 1, parts[2]);
    }

    var startdatekey;
    var enddatekey;


    $(document).ready(function () {

        var mindate;
        var f = new Date();
        var max = (f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear());


        $http({
            url: '/resources/ConsultKeysSpecialist/getDateMin',
            method: 'GET',
            params: {
            }
        }).then(function success(response) {
            mindate = response.data;
            // console.log("mindate");
            console.log(mindate);
           var formet = toDate(mindate);
           var mindateformet= (formet.getDate()+ "/" + (formet.getMonth() + 1) + "/" + formet.getFullYear());
            
            
            // console.log("formet");
             console.log("Fecha minima: "+formet);
            
            // console.log("minidateformet");
            // console.log(mindateformet);
             
            $scope.isDisabledEndDate = true;
            $("#startDate").daterangepicker({
                onSelect: function ()
                {
                    this.focus();
                },
                autoUpdateInput: false,
                minDate: mindateformet,
                maxDate: max,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            }, function (start, end, label) {
                $("#endDate").attr("disabled", false);
                $scope.startdatekeys = start.format('DD/MM/YYYY');
                startdatekey = start.format('DD/MM/YYYY');
               // var max = new Date();
                
                // console.log("max1");
                 console.log("Fecha maxima"+max);
                $("#startDate").val(start.format('DD/MM/YYYY'));
                $("#endDate").val('');
                $("#endDate").daterangepicker({
                    onSelect: function ()
                    {
                        this.focus();
                    },
                    autoUpdateInput: false,
                    minDate: startdatekey,
                    maxDate: max,
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'DD/MM/YYYY',
                        "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                        "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                    }
                }, function (start, end, label) {
                    $("#startDate").val(startdatekey);
                    $scope.enddatekeys = start.format('DD/MM/YYYY');
                    enddatekey = start.format('DD/MM/YYYY');
                    $("#endDate").val(start.format('DD/MM/YYYY'));
                     $("#endDate").trigger("click");
                   
                });
            });
             $("#endDate").val(enddatekey);
              $("#endDate").trigger("click");
            return mindate;


        }, function myError(response) {
            console.log("hay un error" + response.data);
        });

    });

    $scope.dateMinYmaxDetail = function (idKey) {

  $http({
            url: '/resources/ConsultKeysSpecialist/consultDateDetailTreatment',
            method: 'GET',
            params: {
                 idKey: idKey,
            }
        }).then(function success(response) {
            $scope.dateMinyMax = response.data;
             // console.log("$scope.dateMinyMax");
            if ($scope.dateMinyMax !== null && $scope.dateMinyMax.length !== 0){
                // console.log("entro");
           $scope.dateMiaxTreatment = response.data[0].dateMiaxTreatmentFilter;
             $scope.dateMinTreatment = response.data[0].dateMinTreatmentFilter;
         
                var dateMinDetail = $scope.dateMinTreatment ;
               // console.log($scope.dateMinTreatment);
                var dateMaxDetail = $scope.dateMiaxTreatment;
               // console.log($scope.dateMiaxTreatment);
               $("#datefilterDetail").daterangepicker({
                   onSelect: function ()
                   {
                       this.focus();
                   },
                autoUpdateInput: false,
                minDate: dateMinDetail,
                maxDate: dateMaxDetail,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },function (start, end, label) {
               $scope.searchTratament.dateFilterTreatment=start.format('DD/MM/YYYY');
                 $scope.$digest();
                  $("#datefilterDetail").val(start.format('DD/MM/YYYY'));
                   // $("#datefilterDetail").trigger("click");

                   setTimeout(function() {
                        $scope.evalArrayDetail();
                   }, 250);
                   var e = $.Event('keydown');
                   e.which = 65; // Character 'A'
                   $('#datefilterDetail').trigger(e);
                   $scope.evalArrayDetail();
            });
            // $("#datefilterDetail").trigger("click");
}
        

        }, function myError(response) {
            // console.log("hay un error" + response.data);
        });
 
    }

    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6500);
    }

    $scope.evalTable = function(){

        var numItems = $('.dteKeyT').length;
        // console.log("hay "+numItems+" elementos");
        if(numItems >0){

            $scope.isHiddenMessageDefaultTable = true;
        }
        else if(numItems == 0){
            $scope.isHiddenMessageDefaultTable = false;
        }


    }
     $scope.evalArrayDetail = function(){

        var numItems = $('.dateForDetail').length;
        if(numItems >0){
            $scope.isHiddenMessageDefault = true;
        }
        else if(numItems ==0){
            $scope.isHiddenMessageDefault = false;
         }
    }

    1

    $('#startDate').keydown(function(e) {
        if (e.keyCode == 8){return false;}
    });
    $('#startDate').keyup(function(e) {
        if (e.keyCode == 8){return false;}
    });
    $('#startDate').keypress(function(e) {
        if (e.keyCode == 8){return false;}
    });
    $('#endDate').keydown(function(e) {
        if (e.keyCode == 8){return false;}
    });
    
    
    $scope.hideBtnRegisterTreatment=function(registerValue){
        if(registerValue==0){
            return true;
        }else{
          return  false;
        }
        
    }
    
    $scope.hideBtnDelete=function(deleteValue){
        if(deleteValue==0){
            return true;
        }else{
            return false;
        }
        
    }
});
