/**
 * Created by akdeskdev90 on 09/06/16.
 */


$(function() {
    $( "#dialog" ).dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 10000

        }
    });

});



var app = angular.module("AKmodule", [ "angular-growl","angular.jquery", "ngAnimate","akmodule",'ngMaterial','ui.bootstrap']);


// app.factory('Akfactory',function(){
//
//     var data = {
//         getInfoDiagnosis : function($http){
//             var model = new ModelInfoDiagnosis();
//             model.key = "XXXX";
//             model.doctor = "Leonel Soriano";
//             model.patient = "Ramon anthonio";
//             model.request = " 01/01/2000 ";
//             model.especiality = " Oftalmologia ";
//             model.bornDate = "11/01/1900";
//             model.dateExpiration = "01/01/2999";
//             model.plan = "Yo si te curo";
//             return model;
//         }
//     }
//
//     return data;
// });

// in {id, modelo}   out  {id,modelo}
app.factory('aktreatmentscomponent', function() {
    
        return {

        getDataCheckbox: function(array) {
            var arrayTmp =  [];
            

            for(var i=0;i < array.length; i++){

                if(array[i].modelo){
                    arrayTmp.push(array[i]);
                }

            }

            return arrayTmp;
        }
    };
});



var diente = function (number) {
    
    this.isSelected = false;
    this.number = number;
    
    return this;
};


app.controller("AKctrl", function($scope,growl,$http,$sce,$window,TreatmentsDialogService,aktreatmentscomponent,redirecFactory,jqueryAutocompleteUntil) {

    //redirecFactory.redirectWithParam("index.xhtml",[{ 'key': '1','value': '12'},{ 'key': '25','value': '12'}]);

    // redirecFactory.getParameter("index.xhtml",['25']).then(function(resolve){
    //  alert(resolve[0].value);
    //  alert(resolve[0].key);
    //  });

    $scope.showAjax = true;
    $scope.infoSpecialist=[];
    $scope.idKeyParam = "";
    $scope.tooths = [];
    //$scope.selected = "";
    $scope.toothsSelected = [];
    $scope.toothSelected = [];
    $scope.treatmentsSelected = [];
    $scope.treatselect = []; // array del tratamiento seleccionado
    $scope.treatmentSelected = "";  // nombre del tratamiento seleccionado
    $scope.listTemporalTreat = [];
    $scope.listTempbyTooth = null;
    $scope.styleScrollTable  = "";
    $scope.ejemplo=[];
    $scope.hola ="";
    // $scope.akmodel="";
    $scope.arrayChecked = [];

    $scope.responsePlantTreatmentKey = "";
    $scope.responseUpdateToothTreatment = "";
    // $scope.listDistinctTreatment = [];
    $scope.listIdTreatment = [];
    $scope.responseP = "";
    $scope.isDisableButtonSave = true;
    $scope.isDisabledButtSaveModal= true;
    $scope.listCheck  = '';
     $(".growl").css("z-index", "-1");

    var cnTreatSucces = 0;


    $scope.dientesArray = [];

    $scope.setSelectedTooth = function (tooth){

       if($scope.listTemporalTreat.length == 0){

            $scope.toothSelected.isSelected = false;

       }
        else{

           for(var i=0;i<$scope.listTemporalTreat.length;i++){

               if($scope.listTemporalTreat[i].tooth == tooth){
                   $scope.toothSelected.isSelected = true;
                   break;
               }
               else{
                   $scope.toothSelected.isSelected = false;
               }
           }
       }
    }

    $scope.findTreatment = function(nameTreatment) {

          $scope.treatselect = [];

        for(var i= 0; i<$scope.treatments.length;i++){

            if(nameTreatment == $scope.treatments[i].nameTreatment)
            {
                $scope.listTemporalTreat.push({
                    idT: $scope.treatments[i].idTreatment,
                    nameT: $scope.treatments[i].nameTreatment,
                    tooth:$scope.toothSelected.idPiece,
                    idPlanTreat : $scope.treatments[i].idPlanTreatment,
                    idPieceTreat : $scope.treatments[i].idPieceTreatment,
                    idBareTreat : $scope.treatments[i].idBaremoTreatment,
                    idPieceTreatment : $scope.treatments[i].idPieceTreatment,
                    idBarePiece : $scope.treatments[i].idBaremoPiece});  // IMPORTANTE

                $scope.setSelectedTooth($scope.toothSelected.idPiece);
                console.log('tratamiento encontrado: (dentro del for)'+JSON.stringify($scope.listTemporalTreat));
             //   $scope.listTemporalTreat.push($scope.treatselect);
            }

            // $scope.addNewTreatment($scope.treatselect.idT,$scope.treatselect.nameT,$scope.treatselect.tooth);

            TreatmentsDialogService.closeDialog({});
        }
             console.log('Lista temporal: '+JSON.stringify($scope.listTemporalTreat));

        // console.log(JSON.stringify(response.data));
    }

    $scope.hola="holasssss";


    $scope.getFindInfoSpecialist = function (key) {

        $scope.showAjax = true;

        $http({
            url: '../resources/ConsultKeysSpecialistBean/getFindInfoSpecialist',
            method: 'GET',
            params: {
                key: key
            }
        }).then(function success(response) {

            $scope.infoSpecialist= response.data;
            if($scope.infoSpecialist.idKey == -1){

                console.log("No hay datos de la clave");
            }
            else{

                console.log("Hay datos de la clave");
            }

            $scope.showAjax = false;


        }, function myError(response) {

            console.log("Error obt Info");
        });

    };

// trae los dientes desde base de datos
    $scope.getFindPiece = function () {


        $scope.showAjax = true;

        $http({
            url: '../resources/managementSpecialist/getFindPiece',
            method: 'GET',
            params: {
                key: $scope.idKeyParam
            }
        }).then(function success(response) {
            console.log('hay datos en dientes');
            $scope.tooths = response.data;
      //      console.log(JSON.stringify(response.data));

        }, function myError(response) {

            console.log('error datos en dientes');
        });

    };



//recibe los valores de la otra pagina
    redirecFactory.getParameter("registerDiagnosis.html",['1'],'index.xhtml').then(function(resolve){
        $scope.idKeyParam =  resolve[0].value;
        $scope.getFindInfoSpecialist($scope.idKeyParam);
        $scope.getFindPiece();


    });


    $scope.loadTooth = function(){

        variable.push(new dientes(i));

    }

    // Trae los tratamientos asociados a la clave y dientes
    $scope.getTreatmentstoTooth = function (key,tooth) {

        $scope.showAjax = true;

        $http({
            url: '../resources/ConsultKeysSpecialistBean/getFindKeysToAuditByTooth',
            method: 'GET',
            params: {
                key: key,
                tooth:tooth
            }
        }).then(function success(response) {

            $scope.treatments = response.data;
            if($scope.treatments.idPiece == -1){

                console.log("No hay info del tratamiento");
                console.log(JSON.stringify($scope.treatments));
                $scope.showAjax = false;

            }
            else{

                console.log("hay info del tratamiento");
                $scope.showAjax = false;

            }


        }, function myError(response) {

            console.log("error al traer info del tratamiento");
            $scope.showAjax = false;
        });



    }

    $scope.holas = function(){

        console.log(JSON.stringify($scope.ejemplo));

    }



    // Obtiene los trartamientos sin piezas
    // $scope.getListTreatWP = function () {
    //
    //
    //     $scope.showAjax = true;
    //
    //     $http({
    //         url: '../resources/ConsultKeysSpecialistBean/getListTreatmentWithoutPiece',
    //         method: 'GET',
    //         params: {
    //             key: $scope.idKeyParam
    //         }
    //     }).then(function success(response) {
    //         console.log('hay datos en tratamientos sin piezas');
    //         $scope.treatmentsWP = response.data;
    //
    //
    //     }, function myError(response) {
    //
    //         console.log('error datos en tratamientos sin piezas');
    //     });
    //
    // };

    //
    //
    // $scope.seleccionado = "XXX";
    //
    //
    // $scope.leonel =  [
    //     "ActionScript",
    //     "AppleScript",
    //     "Asp",
    //     "BASIC",
    //     "C",
    //     "C++",
    //     "Clojure",
    //     "COBOL",
    //     "ColdFusion",
    //     "Erlang",
    //     "Fortran",
    //     "Groovy",
    //     "Haskell",
    //     "Java",
    //     "JavaScript",
    //     "Lisp",
    //     "Perl",
    //     "PHP",
    //     "Python",
    //     "Ruby",
    //     "Scala",
    //     "Scheme"
    // ];

    $scope.showAjax = false;

    //$scope.infoDiagnosis = Akfactory.getInfoDiagnosis();

    // $scope.cambia = function(){

        //
        // var leonel2 =  [
        //     "leonel",
        //     "AppleScript",
        //     "Asp",
        //     "BASIC",
        //     "C",
        //     "C++",
        //     "Clojure",
        //     "COBOL",
        //     "ColdFusion",
        //     "Erlang",
        //     "Fortran",
        //     "Groovy",
        //     "Haskell",
        //     "Java",
        //     "JavaScript",
        //     "Lisp",
        //     "Perl",
        //     "PHP",
        //     "Python",
        //     "Ruby",
        //     "Scala",
        //     "Scheme"
        // ];

//$("#prueba").autocomplete('option', 'source', leonel2);

    //     jqueryAutocompleteUntil.reload("prueba",leonel2);
    // }


    //cerrar dialog
    $scope.closeDialog = function closeDialog(){

        $scope.setSelectedTooth($scope.toothSelected.namePiece);
        TreatmentsDialogService.closeDialog({});
    }

    $scope.testDialog = function(key,tooth) {
         $scope.hola = tooth.namePiece;
        // alert('Nombre diente: '+$scope.hola);
        $scope.toothSelected = null;
        $scope.toothSelected = [];
        $scope.toothSelected = tooth;

        //
         $scope.setSelectedTooth( $scope.toothSelected.namePiece);    //   marca o desmarca el check diente
         $scope.getTreatmentstoTooth(key,tooth.namePiece);    //  info del combo
         $scope.loadListbyTooth(tooth.namePiece);    //  filtra la tabla segun el diente
         $scope.loadStyle();

        $scope.treatmentSelected = "";
        TreatmentsDialogService.openDialog({}).then(function(result) {

            // TreatmentsDialogService.changeTitle();
            $scope.isDisabledButtSaveModal= true;


            if (result.ok) {
                // alert('You entered ' + result.name);
            }
        });
    };

    $scope.loadStyle = function() {

        if($scope.listTempbyTooth.length > 5){

            $scope.styleScrollTable  = "scrollTableDialog";

        }

    }




    $scope.loadListbyTooth = function (namePiece){
        $scope.listTempbyTooth = [];

        for(var i=0; i< $scope.listTemporalTreat.length;i++){

            if($scope.listTemporalTreat[i].tooth == namePiece){

                $scope.listTempbyTooth.push($scope.listTemporalTreat[i].nameT);

            }

        }
       // console.log('lista de tratamientos asociadas al diente> '+JSON.stringify($scope.listTempbyTooth));




    }

    // $scope.getFindKeysSpecialist = function (idSpecialist,dateStart,dateEnd,numberForKey,patient) {
    //
    //         $scope.showAjax = true;
    //
    //         $http({
    //             url: '../resources/ConsultKeysSpecialistBean/getFindKeysToAuditBySpecialistAndDate',
    //             method: 'GET',
    //             params: {
    //                 idSpecialist : idSpecialist,
    //                 dateStart : dateStart,
    //                 numberForKey : numberForKey,
    //                 patient : patient
    //             }
    //         }).then(function success(response) {
    //
    //
    //         }, function myError(response) {
    //
    //
    //         });
    // };


    var valueToot =-1;
    var vo=$scope.toothSelected.namePiece;

    $scope.titleHeader = function (hola){
        valueToot = vo;
        return 'REGISTRAR TRATAMIENTO DIENTE ';

        // $(".ui-dialog-title").html("REGISTRAR TRATAMIENTO A DIENTE "+$scope.hola);

    }

    $scope.loadT = function (valor){

        // valueToot = valor;
        $(".ui-dialog-title").html("REGISTRAR TRATAMIENTO A DIENTE "+valueToot);

    }

//     $scope.searchIdTreatment = function (id){
//$scope.listIdTreatment
//         for(var i=0; i<$scope.listTemporalTreat.length;i++){
//
//                 if(id === $scope.listTemporalTreat[i].idTreatment){
//                     return true;
//
//                 }
//             else{
//                     return false;
//                 }
//         }
//
//     }

    // $scope.loadIdListTreatments = function (){
    //
    //     console.log('Alistando todo para guardar los tratamientos con pieza');
    //
    //     $scope.listIdTreatment.push($scope.listTemporalTreat[0].idT);
    //
    //     for(var i= 0;i<$scope.listTemporalTreat.length;i++){
    //
    //         // for(var j=0;j<$scope.listIdTreatment.length;j++){
    //
    //             if($scope.encontrado($scope.listTemporalTreat[i].idT)){
    //
    //
    //                 console.log("id encontrado");
    //
    //             }
    //             else{
    //                 $scope.listIdTreatment.push($scope.listTemporalTreat[i].idT);
    //                 console.log("id agregado al aray");
    //             }
    //
    //
    //         // }
    //
    //     }
    //     console.log("ids en la lista: "+JSON.stringify($scope.listIdTreatment));
    //
    // }

    $scope.filterList = function () {

        var temp = {};
        for (var i = 0; i < $scope.listTemporalTreat.length; i++) {
            temp[$scope.listTemporalTreat[i].idT] = true;
            $scope.listIdTreatment = [];
            for (var k in temp)
                $scope.listIdTreatment .push(k);

        }
    }

    $scope.saveAllTreatments = function () {

//      Busco los tratamiento sin piezas seleccionados.
        var arrayTreatProcess = [];
        for (var i = 0; i < $('.ak-checkbox').length; i++) {

            if ($(".ak-checkbox").get(i).checked){

                var idPlanTreat = $(".ak-checkbox").get(i).getAttribute("ak-idplantreat");
                var idbaremo = $(".ak-checkbox").get(i).getAttribute("ak-idbaremo");
                var id = $(".ak-checkbox").get(i).getAttribute("id");

            arrayTreatProcess.push({
                id: id,
                idBaremo: idbaremo,
                idPlanTreat: idPlanTreat
            });

        }
    }
        // Proceso los tratamientos sin piezas seleccionados
         for(var i=0;i < arrayTreatProcess.length;i++){

            $scope.createPlanTreatmentKey($scope.idKeyParam,arrayTreatProcess[i].idPlanTreat,
                                            arrayTreatProcess[i].idBaremo);

         }

        $scope.filterList();
        var cnTreatSucces = 0;


        //tratamientos con pieza
        var processed = false;

        for (var i = 0; i < $scope.listIdTreatment.length; i++) {

            processed = false;

            $scope.responsePlantTreatmentKey = "";

                for (var j = 0; j < $scope.listTemporalTreat.length; j++) {

                    console.log("procesado: "+processed);

                    if ($scope.listIdTreatment[i] == $scope.listTemporalTreat[j].idT) {

                            if(!processed){

                                $scope.createPlanTreatmentKey($scope.idKeyParam,$scope.listTemporalTreat[j].idPlanTreat,
                                                              $scope.listTemporalTreat[j].idBareTreat);
                                console.log($scope.responsePlantTreatmentKey);
                                  //   $scope.updateTT($scope.listTemporalTreat[j].idPlanTreat,$scope.listTemporalTreat[j].idBareTreat);
                                $scope.updateToothTreatmentRest($scope.responsePlantTreatmentKey.idTreatmentKey,
                                                                $scope.listTemporalTreat[j].idPieceTreat,
                                                                $scope.listTemporalTreat[j].idBarePiece,
                                                                $scope.responsePlantTreatmentKey.idTreatmentKeyDiagnosis);
                                console.log($scope.responseUpdateToothTreatment);
                                     processed = true;

                              
                                    // .then(function success(response) {
                                    //     console.log("entra en el then del metodo 1");
                                    //     $scope.responseP = response.data;
                                    //     $scope.updateTT($scope.listTemporalTreat[j].idPlanTreat,$scope.listTemporalTreat[j].idBareTreat);
                                    //     processed = true;
                                    //
                                    // });
                            }
                            else {

                               // $scope.updateTT($scope.listTemporalTreat[j].idPlanTreat,$scope.listTemporalTreat[j].idBareTreat);
                                $scope.updateToothTreatmentRest($scope.responsePlantTreatmentKey.idTreatmentKey,
                                                                $scope.listTemporalTreat[j].idPlanTreat,
                                                                $scope.listTemporalTreat[j].idBareTreat);
                                console.log($scope.responseUpdateToothTreatment);
                            }

                    }
                    else {
                        console.log('no hay ids que concuerden');
                    }

                }


            }
        console.log("se registraron " + cnTreatSucces + " tratamientos con pieza");
                     $scope.openSuccess();

    }

    $scope.saveTreatment = function (){
        $scope.isDisableButtonSave = false;
        $scope.findTreatment($scope.treatmentSelected);



    }
/*

*/
    // metodo que se llamara por cada tratamiento seleccionado sea con pieza o sin pieza
    $scope.createPlanTreatmentKey = function(key,idPlanTreat,idBareTreat){

        console.log("clave : " +key);
        console.log("id plan tratamiento : " +idPlanTreat);
        console.log("id Baremo Tratamiento: " +idBareTreat);

        // return new Promise(function(resolve, reject) {
        $.ajax({
            // $http({
            url: '../resources/ConsultKeysSpecialistBean/insertPlanTreatmentKey',
            //  method: 'GET',
            type: 'GET',
            async: false,
            data: {
                key: key,
                idPlanTreat: idPlanTreat,
                idBareTreat: idBareTreat
            },
            success: function (response) {
                $scope.responsePlantTreatmentKey = response;


            },
            error: function (response) {
                console.log('entra al error');
            }
            //  }).then(function success(response) {
        })
        


    }

    /*
     Este Metodo debe llamarse  por cada tratamiento con diente seleccionado
    Parametros :
             idTreatKey : id Tratamiento Clave Es el id que retorna el Metodo createPlanTreatmentKey
             idTreatPiece: id tratamiento pieza
             idBareTreatPiece : id baremo tratamiento pieza
     */
    $scope.updateToothTreatmentRest = function(idTreatKey,idTreatPiece,idBareTreatPiece, idKeyTreatDiag ){
        console.log("clave : " +idTreatKey);
        console.log("id treat pieza : " +idTreatPiece);
        console.log("id Baremo Tratamiento: " +idBareTreatPiece);
        console.log("id clave tratamiento digagnostico: " +idKeyTreatDiag);


        $scope.responseUpdateToothTreatment = null;
       // $scope.showAjax = true;

        $.ajax({
            url: '../resources/ConsultKeysSpecialistBean/updateToothTreatment',
            type: 'GET',
            async : false,
            data: {
                idTreatKey : idTreatKey,
                idTreatPiece: idTreatPiece,
                idBareTreatPiece : idBareTreatPiece,
                idKeyTreatDiag : idKeyTreatDiag
            },
            success : function(response){
                console.log('ENtra al success metodo 2');
                $scope.responseUpdateToothTreatment = response;

            },
            error : function(response){
                console.log("Entra en el error metodo 2");
            }
        });


    }


    $scope.enableButton = function(){

        $scope.isDisabledButtSaveModal = false;

    }

    $scope.openSuccess = function() {
        SuccessDialogService.openDialog({}).then(function(result) {
            if (result.ok) {
                // alert('You entered ' + result.name);
            }
         //   redirecFactory.redirectWithParam('managedKeyAtention.html',{});
        });
    };

    // No funciona  Borrar
    $scope.checkTreatment = function(){
        console.log('Entra al metodo de ng change');
        if($scope.listCheck.model){
            $scope.isDisableButtonSave = false;
            return false;
        }
        else{
            $scope.isDisableButtonSave = true;
            return true;
        }
        
    }

function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6500);
    }

});



