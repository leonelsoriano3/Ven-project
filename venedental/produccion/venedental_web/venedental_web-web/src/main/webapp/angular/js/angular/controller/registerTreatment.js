/**
 * Created by akdeskdev90 on 09/06/16.
 */


$(function () {
    $("#dialog").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

});

// function showOptions(s) {
// console.log(s[s.selectedIndex].value); // get value
// console.log(s[s.selectedIndex].id); // get id
// }



//model
function ModelInfoDiagnosis() {
    this.key;
    this.doctor;
    this.patient;
    this.request;
    this.especiality;
    this.bornDate;
    this.dateExpiration;
    this.plan;

}
;

var app = angular.module("AKmodule", ["angular-growl", "angular.jquery", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap', 'daterangepicker']);





app.factory('aktreatments', function () {
    return {
        getDataCheckbox: function (array) {

            var arrayTmp = [];


            for (var i = 0; i < array.length; i++) {



                if (array[i].modelo) {
                    arrayTmp.push(array[i]);
                }

            }

            return arrayTmp;
        }
    };
});


var diente = function (number) {

    this.isSelected = false;
    this.number = number;

    return this;
};
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);



app.service('fileUpload', ['$http', function ($http, redirecFactory) {
    this.uploadFileToUrlRecaudos = function (file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);
        var fname = file.name;
        // console.log("El archivo es" + file);
        $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })

            .success(function (redirecFactory) {


            })
            .error(function () {
            });
    }
}]);

app.controller("AKctrl", function ($scope, growl, $http, $sce, $window, MensajeExitoDialogService, jqueryAutocompleteUntil, ConfirmationRDialogService,dateTreatmentDialogService, aktreatments, recaudosDialogService, treatmentsPropertiesDialogService, redirecFactory, jqueryAutocompleteUntil, fileUpload) {
    $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        // console.log("Error Interno");
    });
    $scope.showAjax = true;
    $scope.idTreatment = "";
    $scope.idTreatmentAddFile = "";
    $scope.infoSpecialist = [];
    $scope.param = "";
    $scope.idKeyParam = "";
    $scope.idPatientParam = "";
    $scope.idnumberKeyParam = "";
    $scope.idDateStartParam = "";
    $scope.idDateEndParam = "";
    $scope.keysParam = "";
    $scope.tooths = [];
    $scope.classTooths = "";
    $scope.classToothsSelected = "";
    $scope.toothsAssigned = [];
    $scope.toothsAssignedPrueba = [];
    $scope.toothAssigned = "";
    $scope.idTreatmentKeyDelete = "";
    $scope.idPieceTreatmentKey = "";
    $scope.selected = "";
    $scope.toothsSelected = [];
    $scope.toothSelected = [];
    $scope.idtoothSelected = "";
    $scope.valuetoothSelected = "";
    $scope.id_piece_treatment_key = "";
    $scope.treatmentsSelected = [];
    $scope.treatselect = "";
    $scope.responseFileName = "";
    $scope.isHiddenMessage = false;
    $scope.treatmentSelectedRT = "";  // nombre del tratamiento seleccionado
    $scope.treatmentsForPiece = "";
    $scope.idPieceTreatment = "";
    $scope.bd = "";
    $scope.filesBD=[];
    $scope.filesProcesado="";
    $scope.treatmentsToothsTemp = [];// Lista de tratamientos por pieza temporal
    $scope.fileTreatmentAGuardar = [];//Lista de recaudos utilizada para guardar en base de datos
    $scope.treatmentsToothsTable = [];//Lista temporal de los tratamientos con pieza para visualizar en la tabla
    $scope.fileTemp = [];// Lista de los recaudos temporal
    $scope.fileTempTable = [];// Lista de los recaudos temporal ya visualizados en la tabla
    $scope.fileTempBD = [];// Lista de los recaudos temporal de los tratamientos ya asignados en bd para subir al servidor
    $scope.fileServer = [];// Lista de los recaudos que ya estan en el servidor y necesitan descargarse
    $scope.fileTempXtreament = [];//Lista para manejar los archivos por tratamiento
    $scope.dateTreatmentTemp = [];// Lista de la fecha de los tratamientos generales temporal
    $scope.selecFileHide = "";
    $scope.styleScrollTableTreatments = "";
    $scope.listIdTreatment = [];
    $scope.idPropertiesPlansTreatmentsKey = [];
    $scope.responseUpdateToothTreatment = "";
    $scope.idTreatmentChecked = "";//id del tratamiento general chekeado
    $scope.idTreatmentTemp = "";//id del tratamiento temporal para comparar
    $scope.arrayTreatChecked = [];
    $scope.arrayTreatForDelete = [];//array de los tratamientos generales a eliminar
    $scope.arrayTreatForAdd = [];//array de los tratamientos generales para agregar
    $scope.idPieceTreatmentDelete="";
    $scope.eventSaveTreatment="";
    $scope.bdDelete="";
    $scope.btnDownload0 = "";
    $scope.btnDownload1 = "";
    $scope.btnDownload2 = "";
    $scope.btnDownload3 = "";
    $scope.btnDownload4 = "";
    $scope.idFileNew="";
    $scope.startdateTreatment="";
    $scope.enddatekeys;
    $scope.isDisabledBtnAddT = true;
    $scope.isDisabledBtnAddTr = true;
    $scope.isDisabledBtnSave = true;
    $scope.isDisabledBtnSaveDate = false;
    $scope.isDisabledBtnAddFile = false;
    $scope.isDisabledBtnDelete = true;
    $scope.isDisabledbtnUpload = true;
    $scope.isDisabledbtnSelectFile = true;
    $scope.isDisabledBtnCanc = true;
    $scope.isDisabledBtnElegir = true;
    $scope.selectFileHide=true;
    $scope.dateTreatmentG=true;
    $scope.dateTreatmentConsult="";
    $scope.labelDateTreatment=true;
    $scope.isDisabledDateTreat = true;
    $scope.isDisabledDatePiece=true;
    $scope.eventUploadFile=0;
    $scope.isDisabledAddFile=true;
    $scope.fileAddXCancelar="";
    $scope.tituloModal = "";
    $scope.toohArraySelect=[];
    $scope.arrayTreatProcess = [];




//recibe los valores de la otra pagina
    redirecFactory.getParameter("registerTreatment.html", ['1'], 'index.xhtml').then(function (resolve) {

        if(resolve.length!=0){
            var parametro=resolve[0].value;
            var variable=parametro.split(",");
            $scope.idKeyParam =variable[0];
            if(variable[1]=="date"){
                $scope.keysParam=variable[1]+","+variable[2]+","+variable[3];
            }else{
                $scope.keysParam=variable[1]+","+variable[2];
            }


            $scope.getFindInfoSpecialist($scope.idKeyParam);
//        $scope.getFindPieceAssigned();
            $scope.getFindPiece();
            $scope.getListTreatWP($scope.idKeyParam);
        }else if(resolve.length==0){
            $scope.getFindInfoSpecialist($scope.idKeyParam);
//        $scope.getFindPieceAssigned();
            $scope.getFindPiece();
            $scope.getListTreatWP($scope.idKeyParam);
        }

    });






//Busca la información del especialista
    $scope.getFindInfoSpecialist = function (key) {

        $scope.showAjax = true;

        $http({
            url: '../resources/ConsultKeysSpecialist/getFindInfoSpecialist',
            method: 'GET',
            params: {
                key: key
            }
        }).then(function success(response) {

            $scope.infoSpecialist = response.data;
            if ($scope.infoSpecialist.idKey == -1) {

                // console.log("No hay datos de la clave");

            } else {

                // console.log("Hay datos de la clave");
            }

            $scope.showAjax = false;


        }, function myError(response) {

            // console.log("Error obt Info");
        });

    };

// trae los dientes desde base de datos
    $scope.getFindPiece = function () {


        $scope.showAjax = true;

        $http({
            url: '../resources/ConsultKeysSpecialist/getFindPiece',
            method: 'GET',
            params: {
                key: $scope.idKeyParam
            }
        }).then(function success(response) {
            // console.log('hay datos en dientes');

            $scope.tooths = response.data;
            for (var i = 0; i < $scope.tooths.length; i++) {
                if ($scope.tooths[i].treatments == 1) {
                    $scope.tooths[i].isSelected = true;
                    $scope.classTooths == "true";
                    $scope.toothAssigned == 1;
                } else if ($scope.tooths[i].treatments == 0) {
                    $scope.toothAssigned == 0;
                    $scope.tooths[i].isSelected = false;
                    $scope.classTooths == "false";
                }
            }


            $scope.showAjax = false;
        }, function myError(response) {

            // console.log('error datos en dientes');
        });

    };

    // trae los tratamientos asignados a una pieza
    $scope.getPieceAssigned = function (idKey, idPiece) {


        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/findPropertiesForKey',
            method: 'GET',
            params: {
                idKey: idKey,
                idPiece: idPiece
            }
        }).then(function success(response) {
            // console.log('hay datos en dientes asignados ya a la clave');
            $scope.toothsAssigned = response.data;
            if (($scope.toothsAssigned == "") || ($scope.toothsAssigned == null)) {
                // console.log('no hay tratamientos asignados para esta pieza');
            } else {
                for (var x = 0; x < $scope.toothsAssigned.length; x++) {

                    var dateTreatment = $scope.toothsAssigned[x].date;

                    $scope.filesBD = $scope.toothsAssigned[x].listRecaudos;
                    if ($scope.toothsAssigned[x].reportePago == 1) {
                        //recordar colocar en true
                        $scope.isDisabledBtnAddFile == true;
                        $scope.isDisabledBtnDelete = true;
                    } else {
                        $scope.isDisabledBtnAddFile == false;
                        $scope.isDisabledBtnDelete = false;
                    }

                    if($scope.toothsAssigned[x].listRecaudos==null){
                        $scope.btnDownload0 = 0;
                        $scope.btnDownload1 = 0;
                        $scope.btnDownload2 = 0;
                        $scope.btnDownload3 = 0;
                        $scope.btnDownload4 = 0;
                        $scope.treatmentsToothsTable.push({
                            idT: $scope.toothsAssigned[x].id_treatment,
                            tooth: $scope.idtoothSelected,
                            idPlanTreat: $scope.toothsAssigned[x].id_treatment_key,
                            fecha: dateTreatment,
                            nameTreatment: $scope.toothsAssigned[x].nameTreatment,
                            idPieceTreatmentKey: $scope.toothsAssigned[x].id_piece_treatment_key,
                            idPieceTreat:$scope.toothsAssigned[x].id_piece_treatment_key,
                            idBareTreat: null,
                            idBarePiece: null,
                            bd: 1,
                            file1:$scope.btnDownload0,
                            file2:$scope.btnDownload1,
                            file3:$scope.btnDownload2,
                            file4:$scope.btnDownload3,
                            file5:$scope.btnDownload4});

                    }
                    else{

                        if($scope.filesBD.length==1){
                            $scope.btnDownload0 = 1;
                            $scope.btnDownload1 = 0;
                            $scope.btnDownload2 = 0;
                            $scope.btnDownload3 = 0;
                            $scope.btnDownload4 = 0;
                        }else if($scope.filesBD.length==2){
                            $scope.btnDownload0 = 1;
                            $scope.btnDownload1 = 1;
                            $scope.btnDownload2 = 0;
                            $scope.btnDownload3 = 0;
                            $scope.btnDownload4 = 0;
                        }else if($scope.filesBD.length==3){
                            $scope.btnDownload0 = 1;
                            $scope.btnDownload1 = 1;
                            $scope.btnDownload2 = 1;
                            $scope.btnDownload3 = 0;
                            $scope.btnDownload4 = 0;
                        }else if($scope.filesBD.length==4){
                            $scope.btnDownload0 = 1;
                            $scope.btnDownload1 = 1;
                            $scope.btnDownload2 = 1;
                            $scope.btnDownload3 = 1;
                            $scope.btnDownload4 = 0;
                        }else if($scope.filesBD.length==5){
                            $scope.btnDownload0 = 1;
                            $scope.btnDownload1 = 1;
                            $scope.btnDownload2 = 1;
                            $scope.btnDownload3 = 1;
                            $scope.btnDownload4 = 1;
                        }

                        $scope.treatmentsToothsTable.push({
                            idT: $scope.toothsAssigned[x].id_treatment,
                            tooth: $scope.idtoothSelected,
                            idPlanTreat: $scope.toothsAssigned[x].id_treatment_key,
                            fecha: dateTreatment,
                            nameTreatment: $scope.toothsAssigned[x].nameTreatment,
                            idPieceTreatmentKey: $scope.toothsAssigned[x].id_piece_treatment_key,
                            idPieceTreat: $scope.toothsAssigned[x].id_piece_treatment_key,
                            idBareTreat: null,
                            idBarePiece: null,
                            bd: 1,
                            file1:$scope.btnDownload0,
                            file2:$scope.btnDownload1,
                            file3:$scope.btnDownload2,
                            file4:$scope.btnDownload3,
                            file5:$scope.btnDownload4});

                    }
                    $scope.fileServer.push({idT:$scope.toothsAssigned[x].id_treatment,
                        recaudos:$scope.filesBD});

                    $scope.findPathforDownload($scope.toothsAssigned[x].id_piece_treatment_key);
                }


                if ($scope.treatmentsToothsTable.length != 0) {
                    $scope.isHiddenMessage = true;
                } else {
                    $scope.isHiddenMessage = false;
                }







            }




        }, function myError(response) {

            // console.log('error datos en dientes asignados a la clave');
        });

    };

    // Buscar los datos de la pieza seleccionada, para poder guardar nombre de archivo, piezas agregadas en el registrar tratamiento
    $scope.getFindPieceAssignedFileName = function (idPieceTreatmentKey) {


        $scope.showAjax = true;


        for (var x = 0; x < $scope.toothsAssignedPrueba.length; x++) {
            if (idPieceTreatmentKey === $scope.toothsAssignedPrueba[x].idPieceTreatmentKey) {
                $scope.idPieceTreatmentKey = $scope.toothsAssignedPrueba[x].idPieceTreatmentKey;
            } else {
                $scope.idPieceTreatmentKey = $scope.idPieceTreatmentKey;
            }

        }
        $scope.openDialogRecaudos();
    };





    $scope.loadTooth = function () {

        variable.push(new dientes(i));

    }

    $scope.validateDateTreatment = function (dateTreatmentG) {
        if ((dateTreatmentG!=null)&&(dateTreatmentG!="")) {
            $("#dateTreatmentG").val($scope.dateTreatmentG);
            $scope.isDisabledBtnSaveDate = false;
        }else{
            $scope.isDisabledBtnSaveDate = true;
        }





        $("#dateTreatmentG").blur();
        $("#txtFocus").focus();
        $("#txtFocus").click();
        dateTreatmentDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
                $cambiotitle($scope.valuetoothSelected);
                // alert('You entered ' + result.name);
            }
        });

    }



    // Trae los tratamientos segun la clave y dientes para cargar el combo
    $scope.getTreatmentstoPiece = function (idKey, idPiece) {

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/findPropertiesTreatments',
            method: 'GET',
            params: {
                idKey: idKey,
                idPiece: idPiece
            }
        }).then(function success(response) {

            $scope.treatmentsForPiece = response.data;

            // console.log(JSON.stringify($scope.treatmentsForPiece));
            $scope.showAjax = false;

            // console.log("hay info del tratamiento");


        }, function myError(response) {

            // console.log("error al traer info del tratamiento de las piezas");
            $scope.showAjax = false;
        });



    }


//Busca la lista de los tratamientos sin pieza de base de datos
    $scope.getListTreatWP = function (idKey) {

        $scope.showAjax = true;

        $http({
            url: '../resources/ConsultKeysSpecialist/getListTreatmentWithoutPiece',
            method: 'GET',
            params: {
                key:idKey
            }
        }).then(function success(response) {
            // console.log('hay datos en tratamientos sin piezas');
            $scope.treatmentsWP = response.data;
            //   $scope.tooths = response.data;
            // console.log(JSON.stringify(response.data));

        }, function myError(response) {

            // console.log('error datos en tratamientos sin piezas');
        });

    };

//Metodo de guia
    $scope.saveTreatment = function () {
        //Guardo el diente seleccionado
        $scope.toothsSelected.push($scope.toothSelected);

        // guardo el tratamiento
        $scope.treatmentsSelected.push($scope.treatselect);
        // alert('tratamiento '+$scope.treatselect);
        // alert('Diente '+$scope.toothSelected);
    }


//guarda en la lista temporar los tartamientos sin pieza que son agregados
    $scope.saveTreatmentRT = function (treatmentSelectedRT,startdateTreatment) {
        var paramDate="";
        var dateTreatmentScope=$scope.startDateTreatment;
        var dateTreat=startdateTreatment;

        if(dateTreat==""){
            paramDate=dateTreatmentScope;
        }else{
            paramDate=dateTreat;
        }


        $scope.showAjax = true;
        $scope.isDisableButtonSave = false;
        $scope.findTreatmentRT(treatmentSelectedRT, paramDate);
//        $scope.assignButton();



    }

//    //guarda en la lista temporal los archivos de los tratamientos agregados en el registrar tratamiento
//    $scope.saveFileTreatmentRT = function () {
//        var fileSupport = $scope.myFile;
//        $scope.fileTemp.push({
//            file: fileSupport,
//            namefile: fileSupport.name
//        });
//
//        console.log("nombre de archivo" + $scope.fileTemp.file + $scope.fileTemp.namefile)
//
//
//
//    }


    $scope.dientesArray = [];


//Guarda el diente seleccionado, para saber si posee tratamientos o no(cambio de css)
    $scope.setSelectedToothRT = function (tooth) {

        if ($scope.treatmentsToothsTemp.length == 0) {

            $scope.toothSelected.isSelected = false;

        } else {

            for (var i = 0; i < $scope.treatmentsToothsTemp.length; i++) {

                if ($scope.treatmentsToothsTemp[i].tooth == tooth.idPiece) {

                    $scope.toothSelected.isSelected = true;
                    break;

                } else {

                    $scope.toothSelected.isSelected = false;

                }
            }
        }
    }


//busca toda la información del tratamiento segun su nombre, para ir guardando en la lista temporal la información de los tratamientos que se van agregando.
    $scope.findTreatmentRT = function (treatmentSelectedRT, startdateTreatment) {

        $scope.treatselect = [];

        for (var i = 0; i < $scope.treatmentsForPiece.length; i++) {

            if (treatmentSelectedRT == $scope.treatmentsForPiece[i].nameTreatment)

            {
                var idTreatmentPorAgregar = $scope.treatmentsForPiece[i].idPieceTreatment;
                $scope.addListaRecaudosAguardar(idTreatmentPorAgregar);
                $scope.validateBtnFiles(idTreatmentPorAgregar);


                var dateTreatment = startdateTreatment;
                $scope.treatmentsToothsTemp.push({
                    idT: $scope.treatmentsForPiece[i].idTreatment,
                    tooth: $scope.idtoothSelected,
                    idPlanTreat: $scope.treatmentsForPiece[i].idPlanTreatment,
                    fecha: dateTreatment,
                    nameTreatment: $scope.treatmentsForPiece[i].nameTreatment,
                    idPieceTreatmentKey: $scope.treatmentsForPiece[i].idPieceTreatment,
                    idPieceTreat: $scope.treatmentsForPiece[i].idPieceTreatment,
                    idBareTreat: $scope.treatmentsForPiece[i].idPriceTreatment,
                    idBarePiece: $scope.treatmentsForPiece[i].idPricePiece,
                    bd:0,
                    file1:$scope.btnDownload0,
                    file2:$scope.btnDownload1,
                    file3:$scope.btnDownload2,
                    file4:$scope.btnDownload3,
                    file5:$scope.btnDownload4});
//                $scope.pushListTable($scope.treatmentsForPiece[i].idTreatment,
//                                      $scope.idtoothSelected,
//                                      $scope.treatmentsForPiece[i].idPlanTreatment,
//                                      dateTreatment,
//                                      $scope.treatmentsForPiece[i].nameTreatment,
//                                      $scope.treatmentsForPiece[i].idPieceTreatment,
//                                      $scope.treatmentsForPiece[i].idPieceTreatment,
//                                     $scope.treatmentsForPiece[i].idPricePiece,
//                                     0,
//                                     recaudos);

                $scope.treatmentsToothsTable.push({
                    idT: $scope.treatmentsForPiece[i].idTreatment,
                    tooth: $scope.idtoothSelected,
                    idPlanTreat: $scope.treatmentsForPiece[i].idPlanTreatment,
                    fecha: dateTreatment,
                    nameTreatment: $scope.treatmentsForPiece[i].nameTreatment,
                    idPieceTreatmentKey: $scope.treatmentsForPiece[i].idPieceTreatment,
                    idPieceTreat: $scope.treatmentsForPiece[i].idPieceTreatment,
                    idBareTreat: $scope.treatmentsForPiece[i].idPriceTreatment,
                    idBarePiece: $scope.treatmentsForPiece[i].idPricePiece,
                    bd:0,
                    file1:$scope.btnDownload0,
                    file2:$scope.btnDownload1,
                    file3:$scope.btnDownload2,
                    file4:$scope.btnDownload3,
                    file5:$scope.btnDownload4});

                $scope.fileAddXCancelar="";
                $("#treatmentSelected").val('');
                $scope.startdateTreatment = "";
                $("#startDateTreatment").val('');
                $scope.isDisabledBtnCanc=true;
                $scope.isDisabledBtnSave = false;
                $scope.isDisabledBtnAddTr = true;
                $scope.isDisabledbtnSelectFile=true;

                $scope.isDisabledBtnElegir = true;
                $scope.showAjax = false;
                $scope.isHiddenMessage = true;


//                $scope.setSelectedToothRT($scope.toothSelected);
//                 console.log('tratamiento encontrado: (dentro del for)' + JSON.stringify($scope.treatmentsToothsTemp));
//                $scope.closeDialogRegisterTreatmentProperties();
                //   $scope.listTemporalTreat.push($scope.treatselect);
            }

            // $scope.addNewTreatment($scope.treatselect.idT,$scope.treatselect.nameT,$scope.treatselect.tooth);


        }


        if ($scope.treatmentsToothsTable.length != 0) {
            $scope.isHiddenMessage = true;
        } else {
            $scope.isHiddenMessage = false;
        }

        if ($scope.treatmentsToothsTable.length >= 5) {
            $scope.styleScrollTableTreatments = "scrollAddTreatment";
        } else {
            $scope.styleScrollTableTreatments = "";
        }



//
    }





//    $scope.pushListTable=function(){
//
//        $scope.treatmentsToothsTable.push({
//                    idT: $scope.treatmentsForPiece[i].idTreatment,
//                    tooth: $scope.idtoothSelected,
//                    idPlanTreat: $scope.treatmentsForPiece[i].idPlanTreatment,
//                    fecha: dateTreatment,
//                    nameTreatment: $scope.treatmentsForPiece[i].nameTreatment,
//                    idPieceTreatmentKey: $scope.treatmentsForPiece[i].idPieceTreatment,
//                    idPieceTreat: $scope.treatmentsForPiece[i].idPieceTreatment,
//                    idBareTreat: $scope.treatmentsForPiece[i].idPriceTreatment,
//                    idBarePiece: $scope.treatmentsForPiece[i].idPricePiece,
//                    bd:0,
//                    recaudos:recaudos});
//
//    }

    $scope.addListaRecaudosAguardar = function (idTreatmentPorAgregar) {
        for (var x = 0; x < $scope.fileTemp.length; x++) {
            if ($scope.fileTemp[x].idT == idTreatmentPorAgregar) {
                $scope.fileTreatmentAGuardar.push($scope.fileTemp[x]);

            }
        }


    }

    $scope.isHidenBtnFile = function (idT,idfile) {
        for (var x = 0; x < $scope.treatmentsToothsTable.length; x++) {
            if ($scope.treatmentsToothsTable[x].idPieceTreat == idT) {
                if(idfile==1){
                    if($scope.treatmentsToothsTable[x].file1 == 1){
                        return false;
                    }else if($scope.treatmentsToothsTable[x].file1 == 0){
                        return true;
                    }
                } else if(idfile==2){
                    if($scope.treatmentsToothsTable[x].file2 == 1){
                        return false;
                    }else if($scope.treatmentsToothsTable[x].file2 == 0){
                        return true;
                    }
                }else if(idfile==3){
                    if($scope.treatmentsToothsTable[x].file3 == 1){
                        return false;
                    }else if($scope.treatmentsToothsTable[x].file3 == 0){
                        return true;
                    }
                }else if(idfile==4){
                    if($scope.treatmentsToothsTable[x].file4 == 1){
                        return false;
                    }else if($scope.treatmentsToothsTable[x].file4 == 0){
                        return true;
                    }
                }else if(idfile==5){
                    if($scope.treatmentsToothsTable[x].file5 == 1){
                        return false;
                    }else if($scope.treatmentsToothsTable[x].file5 == 0){
                        return true;
                    }
                }


            }
        }


    }


    $scope.validateCancelFile=function(){

    }


    $scope.validateBtnFiles = function (idTreatmentPorAgregar) {
        for (var x = 0; x < $scope.fileTemp.length; x++) {
            if ($scope.fileTemp[x].idT == idTreatmentPorAgregar) {
                var filesAdd = $scope.fileTemp[x].recaudos.length;
                switch (filesAdd) {
                    case 1:
                        $scope.btnDownload0 = 1;
                        $scope.btnDownload1 = 0;
                        $scope.btnDownload2 = 0;
                        $scope.btnDownload3 = 0;
                        $scope.btnDownload4 = 0;

                        break;
                    case 2:
                        $scope.btnDownload0 = 1;
                        $scope.btnDownload1 = 1;
                        $scope.btnDownload2 = 0;
                        $scope.btnDownload3 = 0;
                        $scope.btnDownload4 = 0;

                        break;

                    case 3:
                        $scope.btnDownload0 = 1;
                        $scope.btnDownload1 = 1;
                        $scope.btnDownload2 = 1;
                        $scope.btnDownload3 = 0;
                        $scope.btnDownload4 = 0;

                        break;
                    case 4:
                        $scope.btnDownload0 = 1;
                        $scope.btnDownload1 = 1;
                        $scope.btnDownload2 = 1;
                        $scope.btnDownload3 = 1;
                        $scope.btnDownload4 = 0;

                        break;

                    case 5:
                        $scope.btnDownload0 = 1;
                        $scope.btnDownload1 = 1;
                        $scope.btnDownload2 = 1;
                        $scope.btnDownload3 = 1;
                        $scope.btnDownload4 = 1;

                }
            }

        }

    }


    $scope.isHidenBtnADDFile = function (idT, idfile) {
        for (var x = 0; x < $scope.treatmentsToothsTable.length; x++) {
            if ($scope.treatmentsToothsTable[x].idPieceTreat == idT) {
                if(idfile==5){

                    if($scope.treatmentsToothsTable[x].file5 == 1){
                        return true;
                    }else  if($scope.treatmentsToothsTable[x].file5 == 0){
                        return false;
                    }
                }


            }
        }
    }

    $scope.validateDeleteProperties = function () {


        if ($scope.bdDelete== 1) {
            $scope.deletePropertiesBD($scope.idPieceTreatmentDelete);
            for (var i = 0; i < $scope.treatmentsToothsTable.length; i++) {
                if ($scope.treatmentsToothsTable[i].idPieceTreatmentKey == $scope.idPieceTreatmentDelete) {
                    $scope.treatmentsToothsTable.pop(i);
                }
            }

        } else if ($scope.bdDelete == 0) {
            $scope.deleteProperties($scope.idPieceTreatmentDelete);
            $scope.mostrarMensajeExito();
            $scope.closeNoConfirmationDeleteKeyR();
        }

        if($scope.treatmentsToothsTable.length!=0){
            $scope.isHiddenMessage = true;
        }else{
            $scope.isHiddenMessage = false;
        }


    }



    //Elimina los tratamientos agregados a las piezas, en la sesión actual
    $scope.deleteProperties = function (idPieceTreatmentKey) {
        for (var i = 0; i < $scope.treatmentsToothsTemp.length; i++) {

            if (idPieceTreatmentKey == $scope.treatmentsToothsTemp[i].idPieceTreatmentKey)
            {
                $scope.idPieceTreatmentKey = $scope.treatmentsToothsTemp[i].id_piece_treatment_key;

                $scope.showAjax = true;

                $scope.treatmentsToothsTemp.pop(i);
                $scope.treatmentsToothsTable.pop(i);
                $scope.showAjax = false;
                $scope.mostrarMensajeExito();
                $scope.closeNoConfirmationDeleteKeyR();
            }
        }

    };


    //Elimina los tratamientos con piezas agregados en la clave epreviamente
    $scope.deletePropertiesBD = function (idPieceTreatmentKey) {

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/deletePropertiesKey',
            method: 'GET',
            params: {
                idPieceTreatmentKey: idPieceTreatmentKey
            }
        }).then(function success(response) {
            // console.log('se elimino el tratamiento con pieza con exito');
            $scope.idTreatmentKeyDelete = response.data;
            $scope.showAjax = false;
            $scope.mostrarMensajeExito();
            $scope.closeNoConfirmationDeleteKeyR();
        }, function myError(response) {

            // console.log('error no se puedo eliminar el tratamiento con pieza');
        });


    };




    $scope.document = {};

    $scope.setTitle = function (fileInput) {

        var file = fileInput.value;
        var filename = file.replace(/^.*[\\\/]/, '');
        var title = filename.substr(0, filename.lastIndexOf('.'));
        $("#title").val(title);
        $("#title").focus();
        $scope.document.title = title;
    };


//Metodo para subir archivos al servidor(de tratamientos ya registrados a la clave)
    $scope.uploadRecaudos = function (idT,fileArray) {
        $scope.filesProcesado=0;
        for( var x=0; x<fileArray.length;x++){
            var file = fileArray[x].file;
            var name = fileArray[x].name;

            var uploadUrl = "/resources/treatmentRegister/upload";

            fileUpload.uploadFileToUrlRecaudos(file, uploadUrl);
//        $scope.closeDialogRecaudos();

            $scope.saveFileName(idT, name);
            $scope.filesProcesado=1;
            countRestEvent++;
        }





        $scope.myFile === null;

    };


    $scope.redirectToManagedKey = function () {
        redirecFactory.redirectWithParam("managedKeyAtention.html", [ {'key': '1', 'value':$scope.keysParam}]);
    }


    $scope.deleteFileXAdd = function(){
        var treartmentCancelFile = "";
        for (var x = 0; x < $scope.fileTemp.length; x++){
            for (var y = 0; y < $scope.fileTemp[x].recaudos.length; y++){
                if ($scope.fileTemp[x].recaudos[y].name == $scope.fileAddXCancelar){
                    $scope.fileTemp[x].recaudos.pop(y);
                    treartmentCancelFile = $scope.fileTemp[x].idT;
                    $scope.fileAddXCancelar="";
                }

            }
        }

    }



    $scope.saveRecaudosTemp = function (treatmentSelectedRT) {
        var file = $scope.myFile;
        file.name = file.name;
        var idFile="";
        $scope.fileAddXCancelar=file.name;

        for (var i = 0; i < $scope.treatmentsForPiece.length; i++) {

            if (treatmentSelectedRT == $scope.treatmentsForPiece[i].nameTreatment)
            {
                var encontrado="";

                var idPieceTreatment = $scope.treatmentsForPiece[i].idPieceTreatment;
                if($scope.fileTemp.length!=0){

                    for(var x=0;x<$scope.fileTemp.length;x++){
                        if (idPieceTreatment == $scope.fileTemp[x].idT){
                            encontrado=1;
                            var cantRecaudos=$scope.fileTemp[x].recaudos;
                            var cantidad=cantRecaudos.length;
                            idFile=cantidad+1;

                            var recaudosTemp=$scope.fileTemp[x].recaudos;
                            var recaudos = [];
                            recaudos=recaudosTemp;
                            $scope.fileTemp.pop(x);
                            recaudos.push({idFile: idFile, file: file, name: file.name});
                            $scope.fileTemp.push({idT: idPieceTreatment, recaudos: recaudos});


                        }else{
                            encontrado=0;
                        }
                    }
                    if(encontrado==0){
                        var recaudos=[];
                        recaudos.push({idFile: 1, file: file, name: file.name});
                        $scope.fileTemp.push({idT: idPieceTreatment, recaudos: recaudos});
                    }

                }else{
                    var recaudos=[];
                    recaudos.push({idFile: 1, file: file, name: file.name});
                    $scope.fileTemp.push({idT: idPieceTreatment, recaudos: recaudos});
                }

            }
//            $scope.filterListFiles(idTreatment);


        }
//validar que el boton se active solo cuando tiene - de 5 recaudos

        if (idFile == 5) {
            $scope.isDisabledbtnSelectFile=true;
            $scope.isDisabledBtnCanc = false;
            $scope.isDisabledBtnAddTr = false;
            $scope.isDisabledbtnUpload = true;
            $scope.openMensajeExito();
            $scope.myFile = null;
        }else{
            $scope.isDisabledBtnCanc = false;
            $scope.isDisabledBtnAddTr = false;
            $scope.isDisabledbtnUpload = true;
            $scope.isDisabledbtnSelectFile=false;

            $scope.openMensajeExito();
            $scope.myFile = null;
        }



    };


//    $scope.assignFile = function () {
//        for (var i = 0; i < $scope.fileTemp.length; i++) {
//
//        }
//
//    }

//    $scope.assignButton = function () {
//
//
//
//        var insertar_en = document.querySelector("#hola table #rec");
//
//
//        for (var i = 0, f; f = $scope.fileTemp[i]; ++i) {
//            var archivo = document.createElement("li");
//            archivo.innerHTML = f.name + ' <b><input type="button" class="myDeleteButton ui-button" title="Eliminar" style="min-width: 10px;margin-top: 5px;" \n\
//                                                                            onclick="remover()" ></b>';
//            console.log(archivo.innerHTML);
//            insertar_en.appendChild(archivo);
//
//
//
//
//        }
//
//        remover = function (e) {
//        }
//
//    }



    //método para guardar la fecha de ejecucion de los tratamientos generales
    $scope.saveDateTReatmentG = function (dateTreatmentG) {
        $scope.arrayTreatForAdd.push({id: $scope.idTreatmentChecked, date: dateTreatmentG});

        dateTreatmentDialogService.closeDialog({});
        $scope.isDisabledBtnSave = false;
        $scope.mostrarMensajeExito();
        return true;//colocamos el check

    }



//    //Metodo para subir archivos al servidor(de tratamientos ya registrados a la clave)
//    $scope.uploadRecaudosGenerales = function () {
//        var file = $scope.myFile;
//        file.name = file.name;
//
//        console.log('file is ');
//        console.dir(file);
//
//        var uploadUrl = "/resources/treatmentRegister/upload";
//
//        fileUpload.uploadFileToUrlRecaudos(file, uploadUrl, $scope.idPieceTreatmentKey);
//        $scope.openMensajeExito();
//        $("li").remove();
//        $scope.myFile === null;
//
//    };

    $scope.mostrarMensajeExitoRedirect = function () {
        $scope.mostrarMensajeExito();

//        $scope.redirectToManagedKey();
    };


    $scope.mostrarMensajeExito = function () {

        $scope.msgGrow = "OPERACIÓN REALIZADA CON ÉXITO!";
        $scope.observation = "";
//        redirecFactory.redirectWithParam('managedKeyAtention.html', {});
        $scope.createMessages(false);
        abajo();
    };

    $scope.createMessages = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
            growMsg($scope.msgGrow, isError)
        ), config);
        arriba();

    };

    $scope.guardargeneral = function () {
        $scope.mostrarMensajeExito();
    }


//Metodo para descargar recaudos
    $scope.downloadRecaudos = function (idTrearmentPieceKey) {
//        var file = $scope.myFile;
//
//        console.log('file is ');
//        console.dir(file);

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/downloadFileSupport',
            method: 'GET',
            params: {
                idTrearmentPieceKey: idTrearmentPieceKey
            }
        }).then(function success(response) {


        }, function myError(response) {


        });

    };

    //Metodo para guardar nombre de recaudo
    $scope.saveFileName = function (idPieceTreatment, name) {
//

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/saveFileName',
            method: 'GET',
            params: {
                idTreatmentPiece: idPieceTreatment,
                name: name
            }
        }).then(function success(response) {

            $scope.responseFileName = response.data;

            $scope.consultNameFile(idPieceTreatment);
            $scope.showAjax = false;


        }, function myError(response) {
            $scope.showAjax = false;

        });

    };
    $scope.validateCheckbox = function () {


        $(".ak-checkbox").click(function () {
            var id = $(this).attr('id');
            $scope.idTreatmentChecked = id;
            if ($(this).is(':checked')) {
                // Hacer algo si el checkbox ha sido seleccionado
                for (var i = 0; i < $scope.treatmentsWP.length; i++) {
                    $scope.idTreatmentTemp = $scope.treatmentsWP[i].idTreament;
                    if (($scope.idTreatmentChecked == $scope.idTreatmentTemp) && ($scope.treatmentsWP[i].treatmentAssigned == 1)) {
                        for (var x = 0; x < $scope.arrayTreatForDelete.length; x++) {
                            if ($scope.idTreatmentChecked == $scope.arrayTreatForDelete[x].id) {
                                $scope.arrayTreatForDelete.pop(x);//elimino de la lista eliminados al tratamiento que ya esta asignado a la clave, ya que esta volviendolo a seleccionar
                                return true;//colocamos el check

                            }
                        }

                    } else if (($scope.idTreatmentChecked == $scope.idTreatmentTemp) && ($scope.treatmentsWP[i].treatmentAssigned == 0)) {
                        $("#txtFocus").focus();
                        $scope.openDialogdateTreatment();

                    }
                    $scope.idTreatmentTemp = null;
                }

            } else {

                // Hacer algo si el checkbox ha sido deseleccionado


                for (var i = 0; i < $scope.treatmentsWP.length; i++) {
                    if (($scope.idTreatmentChecked == $scope.treatmentsWP[i].idTreament) && ($scope.treatmentsWP[i].treatmentAssigned == 1)) {
                        var idTreatmentKey = $(this).attr('ak-idTreatmentKey');

                        $scope.arrayTreatForDelete.push({id: $scope.idTreatmentChecked, idTreatmentKey: idTreatmentKey});//agregar el id planTreatmentKey y otros a necesitar para eliminar de bd

                        $scope.validateBtnSave();
                    } else if (($scope.idTreatmentChecked == $scope.treatmentsWP[i].idTreament) && ($scope.treatmentsWP[i].treatmentAssigned == 0)) {
                        for (var x = 0; x < $scope.arrayTreatForAdd.length; x++) {
                            if ($scope.idTreatmentChecked == $scope.arrayTreatForAdd[x].id) {
                                $scope.arrayTreatForAdd.pop(x);// se elimina de la lista de tratamientos a agregar
                                $scope.validateBtnSave();
                            }
                        }

                    }

                }


            }
        });
    }


//    $scope.getFileNameBD = function () {
//        $scope.showAjax = true;
//
//        $http({
//            url: '../resources/treatmentRegister/insertTreatmentKey',
//            method: 'GET',
//            async: false,
//            params: {
//                key: key,
//                idPlanTreat: idPlanTreat,
//                idBareTreat: idBareTreat,
//                date: date
//            }
//        }).then(function success(response) {
//
//            $scope.plantTreatmentKey = response.data;
//
//
//            $scope.showAjax = false;
//
//
//
//
//        }, function myError(response) {
//
//
//            $scope.showAjax = false;
//        });
//
//
//
//    }
//




    //cerrar dialog para agregar recaudos a los tratamientos asignados en registrar diagnostico
    $scope.closeDialogRecaudos = function closeDialog() {

        recaudosDialogService.closeDialog({});
    }


    //cerrar dialog para agregar tratamientos a las piezas
    $scope.closeDialogRegisterTreatmentProperties = function closeDialog() {
        $scope.setSelectedToothRT($scope.toohArraySelect);
        $scope.treatmentSelectedRT="";
        $scope.isDisabledDatePiece=true;

        treatmentsPropertiesDialogService.closeDialog({});
    }


    //  abrir dialogo para agregar recaudos a los tratamientos de registrar diagnostico
    $scope.openDialogRecaudos = function (idTreatment, bd) {
        $scope.idTreatmentAddFile= idTreatment;
        $scope.bd = bd;
        recaudosDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
//                $cambiotitle($scope.valuetoothSelected);
                // alert('You entered ' + result.name);
            }
        });

    };

    $scope.valideAddFile = function () {
        var idTreatSelectFile=$scope.idTreatmentAddFile;
        var recaudos = $scope.fileTemp;
        var idTemp = 1;
        var file = $scope.files;
        file.name = file.name;
        var encon="";
        for (var y = 0; y <$scope.fileTemp.length; y++) {
            if ($scope.fileTemp[y].idT== $scope.idTreatmentAddFile) {
                encon=1;
                var recaudosTemp=$scope.fileTemp[y].recaudos;

                var cantidadRegistros=recaudosTemp.length;
                if(cantidadRegistros==4){
                    $scope.isDisabledBtnAddFile==true;
                }
                $scope.idFileNew=cantidadRegistros+1;
                var recaudos=[];
                $scope.fileTemp[y].recaudos.push({idFile:$scope.idFileNew, file: file, name: file.name});
            }else{
                encon=0;
            }

        }



        $scope.closeDialogRecaudos();
        $scope.openMensajeExito();

        if ($scope.bd == 1) {
            $scope.validateBtnFiles($scope.idTreatmentAddFile);
            var idTreatmentAddFileTemp="";
            idTreatmentAddFileTemp=$scope.idTreatmentAddFile;
            $scope.fileTempBD.push({idT: $scope.idTreatmentAddFile, idFile: $scope.idFileNew, file: file, name: file.name});
            var fileA=[];
            fileA.push({idT: $scope.idTreatmentAddFile, idFile: $scope.idFileNew, file: file, name: file.name})

            $scope.uploadRecaudos($scope.idTreatmentAddFile,fileA);

            $scope.validateBtnFiles($scope.idTreatmentAddFile);
            for (var x = 0; x < $scope.treatmentsToothsTable.length; x++) {
                var treatmentTableTemp=[];
                if ($scope.treatmentsToothsTable[x].idPieceTreat==idTreatSelectFile) {

                    treatmentTableTemp.push({
                        idT: $scope.treatmentsToothsTable[x].idT,
                        tooth: $scope.treatmentsToothsTable[x].tooth,
                        idPlanTreat: $scope.treatmentsToothsTable[x].idPlanTreat,
                        fecha: $scope.treatmentsToothsTable[x].fecha,
                        nameTreatment: $scope.treatmentsToothsTable[x].nameTreatment,
                        idPieceTreatmentKey: $scope.treatmentsToothsTable[x].idPieceTreatmentKey,
                        idPieceTreat: $scope.treatmentsToothsTable[x].idPieceTreat,
                        idBareTreat: null,
                        idBarePiece: null,
                        bd: 1,
                        file1:$scope.btnDownload0,
                        file2:$scope.btnDownload1,
                        file3:$scope.btnDownload2,
                        file4:$scope.btnDownload3,
                        file5:$scope.btnDownload4});
                    $scope.treatmentsToothsTable.pop(x);
                    $scope.treatmentsToothsTable.push({
                        idT: treatmentTableTemp[0].idT,
                        tooth:treatmentTableTemp[0].tooth,
                        idPlanTreat:treatmentTableTemp[0].idPlanTreat,
                        fecha: treatmentTableTemp[0].fecha,
                        nameTreatment: treatmentTableTemp[0].nameTreatment,
                        idPieceTreatmentKey: treatmentTableTemp[0].idPieceTreatmentKey,
                        idPieceTreat: treatmentTableTemp[0].idPieceTreat,
                        idBareTreat: null,
                        idBarePiece: null,
                        bd: 1,
                        file1:treatmentTableTemp[0].file1,
                        file2:treatmentTableTemp[0].file2,
                        file3:treatmentTableTemp[0].file3,
                        file4:treatmentTableTemp[0].file4,
                        file5:treatmentTableTemp[0].file5});
                    if($scope.treatmentsToothsTable[x].file5==1){
                        $scope.isDisabledBtnAddFile==true;
                    }else{
                        $scope.isDisabledBtnAddFile==false;
                    }
//                    var recaudos=[];
//                recaudos.push({file1: $scope.btnDownload0, file2: $scope.btnDownload1, file3: $scope.btnDownload2,file4:$scope.btnDownload3,file5:$scope.btnDownload4});
//                                  $scope.treatmentsToothsTable[x].file1=$scope.btnDownload0;
//                $scope.treatmentsToothsTable[x].file2=$scope.btnDownload1;
//                $scope.treatmentsToothsTable[x].file3=$scope.btnDownload2;
//                $scope.treatmentsToothsTable[x].file4=$scope.btnDownload3;
//                $scope.treatmentsToothsTable[x].file5=$scope.btnDownload4;
                }



            }

        } else if ($scope.bd == 0) {
            $scope.validateBtnFiles($scope.idTreatmentAddFile);
            for (var x = 0; x < $scope.treatmentsToothsTable.length; x++) {
                var treatmentTableTemp=[];
                if ($scope.treatmentsToothsTable[x].idPieceTreat== $scope.idTreatmentAddFile) {
                    treatmentTableTemp.push({
                        idT: $scope.treatmentsToothsTable[x].idT,
                        tooth: $scope.treatmentsToothsTable[x].tooth,
                        idPlanTreat: $scope.treatmentsToothsTable[x].idPlanTreat,
                        fecha: $scope.treatmentsToothsTable[x].fecha,
                        nameTreatment: $scope.treatmentsToothsTable[x].nameTreatment,
                        idPieceTreatmentKey: $scope.treatmentsToothsTable[x].idPieceTreatmentKey,
                        idPieceTreat: $scope.treatmentsToothsTable[x].idPieceTreat,
                        idBareTreat: null,
                        idBarePiece: null,
                        bd: 1,
                        file1:$scope.btnDownload0,
                        file2:$scope.btnDownload1,
                        file3:$scope.btnDownload2,
                        file4:$scope.btnDownload3,
                        file5:$scope.btnDownload4});
                    $scope.treatmentsToothsTable.pop(x);
                    $scope.treatmentsToothsTable.push({
                        idT: treatmentTableTemp[0].idT,
                        tooth:treatmentTableTemp[0].tooth,
                        idPlanTreat:treatmentTableTemp[0].idPlanTreat,
                        fecha: treatmentTableTemp[0].fecha,
                        nameTreatment: treatmentTableTemp[0].nameTreatment,
                        idPieceTreatmentKey: treatmentTableTemp[0].idPieceTreatmentKey,
                        idPieceTreat: treatmentTableTemp[0].idPieceTreat,
                        idBareTreat: null,
                        idBarePiece: null,
                        bd: 0,
                        file1:treatmentTableTemp[0].file1,
                        file2:treatmentTableTemp[0].file2,
                        file3:treatmentTableTemp[0].file3,
                        file4:treatmentTableTemp[0].file4,
                        file5:treatmentTableTemp[0].file5});
                    if($scope.treatmentsToothsTable[x].file5==1){
                        $scope.isDisabledBtnAddFile==true;
                    }else{
                        $scope.isDisabledBtnAddFile==false;
                    }

                }


            }
            $scope.addListaRecaudosAguardar({idT: $scope.idTreatmentAddFile, idFile: $scope.idFileNew, file: file, name: file.name});

        }


        $scope.files = null;
    }

    $scope.validateBtnSave = function () {
        if (($scope.arrayTreatForAdd.length < 0) || ($scope.arrayTreatForDelete.length < 0)) {
            $scope.isDisabledBtnSave = true;

        } else {
            $scope.isDisabledBtnSave = false;
        }
    }



    //  abrir dialogo para seleccioanr la fecha para agregar tratamiento general
    $scope.openDialogdateTreatment = function () {
        $scope.dateTreatmentConsult="";
        $scope.dateTreatmentG="";
        var f = new Date();
        var dd=f.getDate();
        var mm=f.getMonth()+1;
        var yyyy=f.getFullYear();
        if(dd<9){
            dd="0"+dd;
        }else{
            dd==dd;

        }
        if(mm<9){
            mm="0"+mm;
        }else{
            mm==mm;
        }
        $("#dateTreatmentG").val(dd+"/"+mm+"/"+yyyy);
        $scope.isDisabledBtnSaveDate = false;
        $scope.dateTreatmentG=dd+"/"+mm+"/"+yyyy;
        $scope.dateTreatmentConsult=$scope.dateTreatmentG;








        var month=f.getMonth()+1;
        var DayOfMonth= $scope.getDays(month,f.getFullYear());
        var minDate="01/"+month+"/"+f.getFullYear();
        var maxDate=DayOfMonth+"/"+month+"/"+f.getFullYear();

//
//
        $("#dateTreatmentG").daterangepicker({
                autoUpdateInput: false,
                minDate: minDate,
                maxDate: maxDate,
                singleDatePicker: true,

                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                },
                defaultDate: '29/12/2016'
            },
            function (start, end, label) {

                $scope.dateTreatmentG = start.format('DD/MM/YYYY');
                $("#dateTreatmentG").val(start.format('DD/MM/YYYY'));
                $("#dateEjecution").val($scope.dateTreatmentG);
                $scope.labelDateTreatment=false;
                $scope.isDisabledBtnSaveDate=false;
                $scope.dateTreatmentConsult= $scope.dateTreatmentG;
                $("#dateTreatmentG").trigger("click");

            });

        $("#dateTreatmentG").blur();
        $("#txtFocus").focus();
        $("#txtFocus").click();
        dateTreatmentDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
                $cambiotitle($scope.valuetoothSelected);
                // alert('You entered ' + result.name);
            }
        });

    };

    $scope.closeDialogdateTreatment = function closeDialog() {
        $scope.isDisabledBtnSaveDate=true;
        $("#botonPrueba").click();
        $("#botonPrueba").focus();
        dateTreatmentDialogService.closeDialog({});
        for (var i = 0; i < $('.ak-checkbox').length; i++) {


            if ($(".ak-checkbox").get(i).checked) {

                var id = $(".ak-checkbox").get(i).getAttribute("id");

                if (id == $scope.idTreatmentChecked) {

                    return false;//colocamos el check
                }else{
                    return false;//quitamos el check
                }

            }
        }
        return false;//quitamos el check
    }

    $scope.getDays= function(humanMonth, year) {
        return new Date(year || new Date().getFullYear(), humanMonth, 0).getDate();
    }

    $scope.enablButtAdd = function () {
        if($scope.startdateTreatment!=""){
            $scope.isDisabledbtnSelectFile = false;

        }else{
            $scope.isDisabledbtnSelectFile = false;

        }


    }






//  abrir dialogo para agregar tratamientos a la pieza
    $scope.openDialogRegisterTreatmentProperties = function (key, tooth) {

        $scope.toohArraySelect=tooth;
        $scope.treatmentsToothsTable = [];
        var f = new Date();
        var dd=f.getDate();
        var yyyy=f.getFullYear();
        var month=f.getMonth()+1;
        if(dd<9){
            dd="0"+dd;
        }else{
            dd==dd;

        }
        if(month<9){
            month="0"+month;
        }else{
            month==month;
        }


        var DayOfMonth= $scope.getDays(month,f.getFullYear());
        var minDate="01/"+month+"/"+f.getFullYear();
        var maxDate=DayOfMonth+"/"+month+"/"+f.getFullYear();
        $("#startDateTreatment").val(dd+"/"+month+"/"+yyyy);
        $scope.startDateTreatment=dd+"/"+month+"/"+yyyy;





//
//
        $("#startDateTreatment").daterangepicker({
                autoUpdateInput: false,
                minDate: minDate,
                maxDate: maxDate,
                singleDatePicker: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },
            function (start, end, label) {
                $scope.startdateTreatment = start.format('DD/MM/YYYY');
                $("#startDateTreatment").val(start.format('DD/MM/YYYY'));
                $("#startDateTreatment").trigger("click");


            });


        for (var i = 0; i < $scope.tooths.length; i++) {
            if ($scope.tooths[i].exodoncia == 1) {
                $scope.isDisabledSelect = true;
                $scope.isDisabledDateTreat = true;
                $scope.isDisabledBtnElegir = true;
            } else {
                $scope.isDisabledSelect = false;
                $scope.isDisabledDateTreat = false;
                $scope.isDisabledBtnElegir = false;
            }

        }

        $scope.valuetoothSelected = tooth;
        $scope.tituloModal = "Registrar tratamiento diente " + $scope.valuetoothSelected.idPiece;
        console.log($scope.tituloModal);
        $scope.toothSelected = null;
        $scope.toothSelected = [];
        $scope.toothSelected = tooth;
        $scope.idtoothSelected = tooth.idPiece;
        $scope.getTreatmentstoPiece(key, tooth.namePiece);

        $scope.getPieceAssigned($scope.idKeyParam, $scope.idtoothSelected);
        $scope.loadListbyTooth(tooth.namePiece);
        $('#treatmentsProperties').dialog('option','title',$scope.tituloModal);
        treatmentsPropertiesDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
//                $cambiotitle($scope.valuetoothSelected);
                // alert('You entered ' + result.name);
            }
        });

    };

    $scope.closeDialogMensajeExito = function closeDialogConfirm() {

        MensajeExitoDialogService.closeDialog({});

    }

    $scope.obtenerIdFIle=function(idTreatment){
        for(var x=0; x< $scope.fileTemp.length; x++){
            if($scope.fileTemp[x].idT=idTreatment){
                var cantidadRegistros=$scope.fileTemp[x].recaudos.legth;
                $scope.idFileNew=cantidadRegistros+1;
            }
        }

    }




    $scope.openMensajeExito = function () {

        MensajeExitoDialogService.openDialog({}).then(function (result) {
            if (result.ok) {

                // alert('You entered ' + result.name);
            }
        });
    };

    $scope.selectFile = function () {

        $scope.isDisabledbtnUpload = false;
        $scope.selectFileHide=false;
    }
    $scope.selectAddFile=function(){
        $scope.isDisabledAddFile=false;
        $scope.btnFiles=true;
    }


    $scope.filterList = function () {

        var temp = {};
        for (var i = 0; i < $scope.treatmentsToothsTemp.length; i++) {
            temp[$scope.treatmentsToothsTemp[i].idT] = true;
            $scope.listIdTreatment = [];
            for (var k in temp)
                $scope.listIdTreatment.push(k);

        }
    }

    $scope.loadListbyTooth = function (idPiece) {
        $scope.listTempbyTooth = [];

        for (var i = 0; i < $scope.treatmentsToothsTemp.length; i++) {
            var idTooh = $scope.treatmentsToothsTemp[i].tooth;
            if (idTooh == idPiece) {

                $scope.listTempbyTooth.push($scope.treatmentsToothsTemp[i].nameT);
                $scope.treatmentsToothsTable.push({
                    idT: $scope.treatmentsToothsTemp[i].idT,
                    tooth: $scope.treatmentsToothsTemp[i].tooth,
                    idPlanTreat: $scope.treatmentsToothsTemp[i].idPlanTreat,
                    fecha: $scope.treatmentsToothsTemp[i].fecha,
                    nameTreatment: $scope.treatmentsToothsTemp[i].nameTreatment,
                    idPieceTreatmentKey: $scope.treatmentsToothsTemp[i].idPieceTreatmentKey,
                    idPieceTreat: $scope.treatmentsToothsTemp[i].idPieceTreat,
                    idBareTreat: $scope.treatmentsToothsTemp[i].idBareTreat,
                    idBarePiece: $scope.treatmentsToothsTemp[i].idBarePiece,
                    bd:$scope.treatmentsToothsTemp[i].bd,
                    file1:$scope.treatmentsToothsTemp[i].file1,
                    file2:$scope.treatmentsToothsTemp[i].file2,
                    file3:$scope.treatmentsToothsTemp[i].file3,
                    file4:$scope.treatmentsToothsTemp[i].file4,
                    file5:$scope.treatmentsToothsTemp[i].file5});

            } else {
                $scope.treatmentsToothsTable.pop(i);
            }



        }


        if ($scope.treatmentsToothsTable.length != 0) {
            $scope.isHiddenMessage = true;
        } else {
            $scope.isHiddenMessage = false;
        }
    }





    $scope.saveAllTreatments = function () {
        $scope.isDisabledBtnSave = true;
        $scope.eventSaveTreatment=1;


//      Busco los tratamiento sin piezas seleccionados.

        for (var i = 0; i < $('.ak-checkbox').length; i++) {


            if ($(".ak-checkbox").get(i).checked) {

                var id = $(".ak-checkbox").get(i).getAttribute("id");
                for (var x = 0; x < $scope.arrayTreatForAdd.length; x++) {
                    if (id == $scope.arrayTreatForAdd[x].id) {

                        var idPlanTreat = $(".ak-checkbox").get(i).getAttribute("ak-idplantreat");
                        var idbaremo = $(".ak-checkbox").get(i).getAttribute("ak-idbaremo");

                        var dateTreatmentG = $scope.arrayTreatForAdd[x].date;
                        $scope.arrayTreatProcess.push({
                            id: id,
                            idBareTreat: idbaremo,
                            idPlanTreat: idPlanTreat,
                            date: dateTreatmentG
                        });
                    }

                }


//

            }
        }


//          // proceso los tratamientos generales a eliminar.
//            //recorro lista de eliminados
//
        for (var i = 0; i < $scope.arrayTreatForDelete.length; i++) {

            $scope.deleteTreatmentsKey($scope.arrayTreatForDelete[i].idTreatmentKey);
        }


        // Proceso los tratamientos sin piezas seleccionados oara agregar a la clave
        for (var i = 0; i < $scope.arrayTreatProcess.length; i++) {

            $scope.saveTreatmentForKey($scope.idKeyParam, $scope.arrayTreatProcess[i].idPlanTreat,
                $scope.arrayTreatProcess[i].idBareTreat, $scope.arrayTreatProcess[i].date);
        }

        //recorro lista de eliminados


        $scope.filterList();

        //tratamientos con pieza
        var processed = false;

        for (var i = 0; i < $scope.listIdTreatment.length; i++) {

            processed = false;

            $scope.responsePlantTreatmentKey = "";

            for (var j = 0; j < $scope.treatmentsToothsTemp.length; j++) {



                if ($scope.listIdTreatment[i] == $scope.treatmentsToothsTemp[j].idT) {

                    if (!processed) {

                        //metodo que guarda tanto el tratamiento a la clave como las piezas.
                        $scope.saveTreatmentofPiece($scope.idKeyParam, $scope.treatmentsToothsTemp[j].idPlanTreat,
                            $scope.treatmentsToothsTemp[j].idBareTreat, $scope.treatmentsToothsTemp[j].fecha,
                            $scope.treatmentsToothsTemp[j].idPieceTreat,$scope.treatmentsToothsTemp[j].idT,
                            $scope.treatmentsToothsTemp[j].idBarePiece);
                        processed = true;
                        //llamar metodo que suba al servidor los archivos. for que recorra la lista de los recaudos, y se guardan todos
                        //los recaudos del tratamiento que se esta procesando para todas las piezas que lo tengan asignado//
                        //dentro del metodo de subir recaudos llamo al se guardar nombre, donde le paso la lista temporal con el nombre y idTreatmentPiece

                    } else {

                        // metodo que solo actualiza las piezas ya que el tratamiento ya esta agregado a la clave
                        // $scope.updateTT($scope.treatmentsToothsTemp[j].idPlanTreat,$scope.treatmentsToothsTemp[j].idBareTreat);
                        $scope.updateToothRT($scope.idPropertiesPlansTreatmentsKey.idTreatmentKey,
                            $scope.treatmentsToothsTemp[j].idPieceTreat,
                            $scope.treatmentsToothsTemp[j].idBarePiece,
                            $scope.treatmentsToothsTemp[j].date,
                            $scope.treatmentsToothsTemp[j].idT);
                    }

                }



            }

        }

        callRp(function(){
            $scope.redirectToManagedKey();
            $scope.showAjax = true;
        });



        $scope.mostrarMensajeExitoRedirect();

    }

    // metodo que se llamara por cada tratamiento seleccionado con pieza

    $scope.saveTreatmentofPiece = function (idKey, idPlanT, idBareT, fechaT, idPieceT,idT,idBarePiece) {
        $scope.eventUploadFile=1;
        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/insertTreatmentofKey',
            method: 'GET',
            params: {
                key: idKey,
                idPlanTreat: idPlanT,
                idBareTreat: idBareT,
                date: fechaT,
                idPieceT: idPieceT,
                idBareT: idBareT,
                idBarePiece:idBarePiece
            }
        }).then(function success(response) {
            countRestEvent++;


            $scope.idPropertiesPlansTreatmentsKey = response.data;
            var numerRecaudos="";

            for (var x = 0; x < $scope.fileTreatmentAGuardar.length; x++) {
                //recordar comparar con idT y no con idPieceTreatment
                if ($scope.fileTreatmentAGuardar[x].idT == idPieceT) {

                    $scope.fileTreatmentAGuardar[x].idT= $scope.idPropertiesPlansTreatmentsKey.idPropertiesPlanTreatmentKey;
                    var fileTempUpload=[];
                    var recaudos=[];
                    recaudos=$scope.fileTreatmentAGuardar[x].recaudos;
                    fileTempUpload.push({idT:$scope.fileTreatmentAGuardar[x].idT, file:recaudos.file, name:recaudos.name});
                    numerRecaudos==x+1;


                    $scope.uploadRecaudos($scope.fileTreatmentAGuardar[x].idT,recaudos);


                }


            }
            $scope.showAjax = false;





        }, function myError(response) {


            $scope.showAjax = false;
        });

    }

    // metodo que se llamara por cada tratamiento seleccionado  sin pieza

    $scope.saveTreatmentForKey = function (key, idPlanTreat, idBareTreat, date) {

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/insertTreatmentKey',
            method: 'GET',
            async: false,
            params: {
                key: key,
                idPlanTreat: idPlanTreat,
                idBareTreat: idBareTreat,
                date: date
            }
        }).then(function success(response) {
            countRestEvent++;

            $scope.plantTreatmentKey = response.data;


            $scope.showAjax = false;




        }, function myError(response) {


            $scope.showAjax = false;
        });



    }





    /*
     Este Metodo debe llamarse  por cada tratamiento con diente seleccionado
     Parametros :
     idTreatKey : id Tratamiento Clave Es el id que retorna el Metodo createPlanTreatmentKey
     idTreatPiece: id tratamiento pieza
     idBareTreatPiece : id baremo tratamiento pieza
     */
    $scope.updateToothRT = function (idTreatmentKey,
                                     idPieceT, idBarePiece, fechaT,idT) {

        $scope.responseUpdateToothTreatment = null;
        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/updateTooth',
            method: 'GET',
            params: {
                id: idTreatmentKey,
                idTreatPiece: idPieceT,
                idBareTreatPiece: idBarePiece,
                date: fechaT
            }
        }).then(function success(response) {
            countRestEvent++;


            $scope.responseUpdateToothTreatment = response;
            for (var x = 0; x < $scope.fileTreatmentAGuardar.length; x++) {
                //recordar comparar con idT y no con idPieceTreatment
                if ($scope.fileTreatmentAGuardar[x].idT == idPieceT) {

                    $scope.fileTreatmentAGuardar[x].idT = $scope.responseUpdateToothTreatment.id;
                    var fileTempUpload=[];
                    var recaudos=[];
                    recaudos=$scope.fileTreatmentAGuardar[x].recaudos;
                    fileTempUpload.push({idT:$scope.fileTreatmentAGuardar[x].idT, file:recaudos.file, name:recaudos.name});



                    $scope.uploadRecaudos($scope.fileTreatmentAGuardar[x].idT,recaudos);

                }
            }

            $scope.showAjax = false;





        }, function myError(response) {
            $scope.showAjax = false;
        });



    }

    //metodo para consultar nombre file

    $scope.consultNameFile = function (idPieceTreatmentKey) {


        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/getFileNameRecaudos',
            method: 'GET',
            params: {
                idTreatmentKey: idPieceTreatmentKey,
            }
        }).then(function success(response) {


            var nameFileBD = response.data;
            $scope.redirectToManagedKey();

            $scope.showAjax = false;




        }, function myError(response) {
            $scope.redirectToManagedKey();
            $scope.showAjax = false;
        });



    }

    //metodo para eliminar tratamientos generales a la clave

    $scope.deleteTreatmentsKey = function (idTreatmentKey) {
        var respondeDelteTreatment = null;

        $scope.showAjax = true;

        $http({
            url: '../resources/treatmentRegister/deleteTreatmentsKey',
            method: 'GET',
            params: {
                idTreatmentKey: idTreatmentKey,
            }
        }).then(function success(response) {
            countRestEvent++;


            respondeDelteTreatment = response.data;

            $scope.showAjax = false;





        }, function myError(response) {
            $scope.showAjax = false;
        });



    }

    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6500);
    }



    $scope.findPathforDownload=function(idTreatment){

        for( var x=0; x<$scope.filesBD.length; x++){

            var filesCantidad=$scope.filesBD.length;
            var idFile=x+1;

            $scope.findFileServer($scope.filesBD[x].nameFile,idTreatment,idFile);
        }

    }



    $scope.findFileServer = function (nameFile,idTreatment,idFile) {


        $http({
            url: '../resources/treatmentRegister/downloadTreatRep',
            method: 'POST',
            params: {
                nameFile: nameFile
            },
            headers: {
                'Content-type': 'application/png'
            },
            responseType: 'arraybuffer'

        }).success(function (data, status, headers) {
            // console.log('entra a la desacrga');
            // TODO when WS success
            var file = new Blob([data], {
                type: 'application/png'
            });

            //En la lista donde tengo todos los path separados, buscar el del id correspondiente al numberFile, y asignarsela a nameReport

            var nameFile= $scope.getFileNameSupportFromHeader(headers('content-disposition'));
            var recaudos=[];
            recaudos.push({idFile: idFile, file: file, name: nameFile});
            if($scope.fileTemp.length==0){



                $scope.fileTemp.push({idT: idTreatment, recaudos: recaudos});



            } else{
                for( var x=0; x<$scope.fileTemp.length; x++){
                    if($scope.fileTemp[x].idT==idTreatment){
                        var files=[];
                        files= $scope.fileTemp[x].recaudos;
                        var cantidad=files.length;
                        var y=cantidad+1;
                        for(var z=0;z<files.length;z++){
                            recaudos.push({idFile: files[z].idFile, file: files[z].file, name: files[z].name});
                        }


                        $scope.fileTemp[x].recaudos=recaudos;
                    }else{
                        $scope.fileTemp.push({idT: idTreatment, recaudos: recaudos});
                    }

                }

            }




            //trick to download store a file having its URL
//            var fileURL = URL.createObjectURL(file);
//            var a = document.createElement('a');
//            a.href = fileURL;
//            a.target = '_blank';
//            a.download = nameReport;
//            document.body.appendChild(a);
//            a.click();

        }).error(function (error) {
            // console.log(error);
            // console.log("Error al descargar archivo");

        });

    };

    $scope.descargarFileTemp = function (idFile, idT) {
        //trick to download store a file having its URL
        for (var x = 0; x < $scope.fileTemp.length; x++) {
            if ($scope.fileTemp[x].idT == idT) {
                for (var i = 0; i < $scope.fileTemp[x].recaudos.length; i++) {
                    if ($scope.fileTemp[x].recaudos[i].idFile == idFile) {
                        var nameReport = $scope.fileTemp[x].recaudos[i].name;
                        var file = $scope.fileTemp[x].recaudos[i].file;
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = nameReport;
                        document.body.appendChild(a);
                        a.click();

                    }
                }
            }
        }
    }



    $scope.filterListFiles = function (idTreatment) {
        $scope.fileTempXtreament = [];
        for (var x = 0; x < $scope.fileTemp.length; x++) {
            if ($scope.fileTemp[x].idT == idTreatment) {
                $scope.fileTempXtreament.push($scope.fileTemp[x]);

            }

        }
//        alert(JSON.stringify($scope.fileTempXtreament));

    }



    $scope.getFileNameSupportFromHeader = function (header) {
        if (!header)
            return null;

        var result = header.split(";")[1].trim().split("=")[1];
        var resultname=result.replace("+","");

        return resultname;
    };


    $scope.cambioCombo=function(){
        $scope.isDisabledDatePiece=false;
        $scope.isDisabledbtnSelectFile=false;
    }

    $scope.closeNoConfirmationDeleteKeyR = function closeDialogConfirm() {

        ConfirmationRDialogService.closeDialog({});

    }


    $scope.oPenConfirmationDeleteKeyR = function (idPieceTreatmentKey,bd) {
        $scope.idPieceTreatmentDelete=idPieceTreatmentKey;
        $scope.bdDelete=bd;

        ConfirmationRDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
                // alert('You entered ' + result.name);
            }
        });
    };



    var countme = 0;
    var countRestEvent = 0;
    function callRp(miCallback){

        var cantTreatmentFile=0;
        var cantFile=0;
        var proceso=0;
        for (var x = 0; x < $scope.fileTreatmentAGuardar.length; x++) {

            for (var y = 0; y < $scope.fileTreatmentAGuardar[x].recaudos.length; y++) {
                cantFile=cantFile+1;
            }

        }
        proceso=cantFile+$scope.arrayTreatForDelete.length+$scope.arrayTreatProcess.length+$scope.treatmentsToothsTemp.length;
        if(countRestEvent ==proceso){

            miCallback();
            redirecFactory.redirectWithParam("managedKeyAtention.html", [ {'key': '1', 'value':$scope.keysParam}]);
        }else if (countme < 10) {
            window.setTimeout(callRp, 1500,miCallback);
        }else{

        }
    }





});



