/**
 * Created by  leonelsoriano3@gmail.com on 18/03/16.
 */
//"angularBeans"

var app = angular.module("AKmodule", ["angular-growl", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap']);


app.controller("AKctrlChangePassword", function ($scope, growl, $http, $sce) {


    $scope.htmlPopover = $sce.trustAsHtml('<div style="color: #cd2313"><span style="font-weight: bold">Recuerde: la contraseña debe cumplir ' +
            'con las siguientes características:</span><br/>' +
            '-Su longitud debe ser entre seis(6) y doce(12) caracteres como máximo<br/>' +
            '-Al menos una letra<br/>' +
            '-Al menos un carácter especial ¡#$& ()?¿[ ]*+.,;<br/>' +
            '-Al menos un número<br/>' +
            '-Es sensible a mayúsculas y minúsculas<br/>' +
            '-No debe contener espacios en blanco<br/>' +
            '</div>');


    $scope.showAjax = false;
    $scope.buttonSubmit = true;
    $scope.firstPassword = "";
    $scope.secondPassWord = "";
    $scope.oldPassword = "1234";
    $scope.idcard = "";
    $scope.resultSendInfo = "";
    $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        console.log("Error Interno");
    });


    $scope.name = "";

    $scope.msgGrow = "";


    $scope.activeSecondPassWord = true;
    $scope.activefirstPassWord = false;
    $scope.firstPasswordMatchRegex = false;
    $scope.stylefirstPassword = '';
    $scope.isMachtPassword = false;
    $scope.isHideLabel = true;

    $scope.messageMatchPassWord = false;
    $scope.oldPassword = "";



    $scope.createMessage = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
                growMsg($scope.msgGrow, isError)
                ), config);
        arriba();
    };

    $scope.validePassWord = function () {

        var re = /^((?=.*\d)(?=.*[a-zA-Z])(?=.*[¡#\$&\(\)?¿\]\[\*\+\.;,]).{6,12})$/;
        var str = $scope.firstPassword;
        var m;

        if ((m = re.exec(str)) !== null) {
            if (m.index === re.lastIndex) {

                $scope.activeSecondPassWord = false;
                $scope.firstPasswordMatchRegex = true;
                $scope.stylefirstPassword = "";

                return;
            }
        } else {
            $scope.activeSecondPassWord = true;
        }

        if (str.length != 0) {
            $scope.stylefirstPassword = "input-error";
        } else if (str.length == 0) {
            $scope.stylefirstPassword = '';
        }
        $scope.secondPassWord = "";
        $scope.firstPasswordMatchRegex = false;
        $scope.activeSecondPassWord = true;

    };

    $scope.matchPassword = function () {

        if ($scope.secondPassWord.length > 0 && $scope.oldPassword.length > 0) {
            if ($scope.firstPassword == $scope.secondPassWord) {
                $scope.buttonSubmit = false;
                $scope.messageMatchPassWord = false;
            } else {
                $scope.buttonSubmit = true;
                $scope.messageMatchPassWord = true;
            }
        } else {

            $scope.buttonSubmit = true;
            $scope.messageMatchPassWord = false;
        }
        if ($scope.oldPassword.length > 0 && $scope.resultSendInfo === 0) {
            $scope.isHideLabel = false;
        } else {
            $scope.isHideLabel = true;
        }

    };


    function getMenajeByNumber(number) {
        if (number == 0) {
            return "LA CONTRASEÑA ANTERIOR INGRESADA NO ES CORRECTA";
        } else if (number == 1) {
            return "EL USUARIO NO POSEE CORREO ASOCIADO";
        } else if (number == 2) {
            return "OPERACIÓN REALIZADA CON ÉXITO,SE HA ENVIADO UN MENSAJE A SU CUENTA DE CORREO ELECTRÓNICO"
        }
    }


    $scope.sendInfo = function (newPassword, oldPassword) {
        if (newPassword.length > 0)
        {
            $scope.showAjax = true;

            $http({
                url: '../resources/updatePassword/newPassword/',
                method: 'POST',
                params: {
                    newpasswordParam: newPassword,
                    previouspasswordParam: oldPassword
                }

            }).then(function success(response) {

                $scope.resultSendInfo = response.data;
                if ($scope.resultSendInfo.number > 0) {
                    $scope.msgGrow = getMenajeByNumber($scope.resultSendInfo.number);
                    $scope.createMessage(false);
                }

                if ($scope.resultSendInfo.number == 2) {
                    $scope.activeSecondPassWord = false;
                    $scope.oldPassword = "";
                    $scope.firstPassword = "";
                    $scope.secondPassWord = "";
                    $scope.buttonSubmit = true;
                } else if ($scope.resultSendInfo.number === 0) {
                    $scope.isHideLabel = false;
                    $scope.oldPassword = "";
                    $scope.firstPassword = "";
                    $scope.secondPassWord = "";
                    $scope.buttonSubmit = true;
                    $scope.activeSecondPassWord = true;
                }

                $scope.showAjax = false;
                abajo();

            }, function myError(response) {

                //$scope.myWelcome = response.statusText;
                $scope.msgGrow = "HA OCURRIDO UN ERROR EN EL SERVICIO. INTENTE SU PETICIÓN NUEVAMENTE";
                $scope.showAjax = false;
                $scope.createMessage(true);
                abajo();

            });
        }
    };

    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6200);
    }

});


