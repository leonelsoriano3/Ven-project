
$(function () {
    $("#dialog").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

});



var app = angular.module("AKmodule", ["angular-growl", "ngAnimate", "angular.jquery", "akmodule", 'ngMaterial', 'ui.bootstrap', 'angular-jquery-autocomplete', 'autocomplete']);


app.controller("AKctrl", function ($scope, $window, growl, $http, $sce, keyDetailDialogService, consultHistoryDialogService, ConfirmationDialogService) {
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        console.log("Error Interno");
    });
     $(".growl").css("z-index", "-1");

    $scope.ocultar = true;







    $scope.isVisibleDataSpecialist = true;
    $scope.isVisibleMedicalO = true;
    $scope.isDisabledDataPatient = true;
    $scope.isDisabledMedicalO = true;
    $scope.isDisabledButtHistorial = true;
    $scope.isDisabledAutocomplete = true;
    $scope.isDisabledButtGenerar = true;
    $scope.isDisabledDataPatient = true;
    $scope.isDisabledLabelHistorial = true;
    $scope.seleccionadoAuto = false;
    $scope.dateKey = "";
    $scope.patient = "";
    $scope.idPatient = "";
    $scope.datapatient = "";
    $scope.specialist = "";
    $scope.namePatient = "";
    $scope.identityNumberPatient = "";
    $scope.namePlan = "";
    $scope.idPlanSelect = "";
    $scope.idMedicalSelect = "";
    $scope.idCompanySelect = "";
    $scope.idPatientSelect = "";
    $scope.idPatientSelectHistory = "";
    $scope.datePatient = "";
    $scope.dataHistorial = "";
    $scope.dataConsultorios = "";
    $scope.relationPatient = "";
    $scope.nameAseguradora = "";
    $scope.nameEnte = "";
    $scope.historial = 0;
    $scope.name = "";
    $scope.nameSpecialist = "";
    $scope.isUser = "";
    $scope.status = '  ';
    $scope.idEspecialidad = "";
    $scope.idEspecialista = "";
    $scope.idSpecialist = "";
    $scope.nameSpeciality = "";
    $scope.nameConsultorio = "";
    $scope.idUserSpecialist = "";
    $scope.consultorio = "";
    $scope.direccionConsultorio = "";
    $scope.numberKey = "";
    $scope.fechaSolicitud = "";
    $scope.fechaExpiracion = "";
    $scope.doctor = "";
    $scope.consultorio = "";
    $scope.consultorioDetalleClave = "";
    $scope.namePatientDetalleClave = "";
    $scope.consultorioSelect = "";
    $scope.consultoriosVarios = "";
    $scope.especialidadClave = "";
    $scope.idUser = "";
    $scope.isVisibleLabelCorreo = false;
    $scope.isVisibleLabelNoCorreo = false;
    $scope.emailforData = 0;
    $scope.especialidad;
    $scope.styleScrollTable = "";
    $scope.patientSelect = "";
    $scope.idLineaSelect = "";
    $scope.tablePartients = "";
    $scope.styleScrollTablePatients = "";
    $scope.seccionTablePatient = true;
    $scope.btnnBuscarPatient = true;
    $scope.hideTablePatient = true;
    $scope.seccionPatients = true;
    $scope.isHiddenMessageDefaultTable = true;
    $scope.isMessTableHisto=true;
    $scope.isHiddenMessageDefaultTableHisto= true;
    $scope.maxCaracteres = false;
    $scope.maxCaracteresDir = false;
    $scope.isDisablePatient=true;
    $scope.searchHistorial= {};
     $scope.search= {};
    $scope.direccionConsultorio="";
    $scope.isHiddenTable = true;


      $scope.deleteOnly = function (evt) {
        var key = evt.which;
        key = String.fromCharCode(key);
        var regex = /^[\b]+$/;
        if (regex.test(key)) {
            $scope.searchHistorial.dateForHistorial = '';
            $('#dateForHistorial').val('');
             $scope.isHiddenTable = true;
            $scope.dateMiaxTreatment = $scope.dataHistorial[0].dateEndFilter;
            $scope.dateMinTreatment = $scope.dataHistorial[0].dateStartFilter;

            var dateMinHistorial = $scope.dateMinTreatment ;
            var dateMaxHistorial = $scope.dateMiaxTreatment;
            $("#dateForHistorial").daterangepicker({
                onSelect: function ()
                {
                    this.focus();
                },
                autoUpdateInput: false,
                minDate: dateMinHistorial,
                maxDate: dateMaxHistorial,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },function (start, end, label) {
                $scope.searchHistorial.dateForHistorial=start.format('DD/MM/YYYY');
                $scope.$digest();
                $("#dateForHistorial").val(start.format('DD/MM/YYYY'));
                $("#dateForHistorial").trigger("click");

            });
            $("#dateForHistorial").trigger("click");
            $scope.showAjax = false;
            //$scope.$digest();
        }
        
        $scope.isHiddenTable = true;
    }

    
    $http({
        url: '../resources/keysAttentionSpecialist/findDataSpecialist',
        method: 'GET',
        params: {
        }


    }).then(function success(response) {
        $scope.specialist = response.data;
        var array = $scope.specialist;
        if (array.length > 1) {
            for (var i = 0; i < array.length; i++) {
                $scope.name = array[0].nameSpecialist;
                $scope.idEspecialidad = array[0].idSpeciality;
                $scope.nameSpecialist = array[0].nameSpecialist;
                $scope.nameSpeciality = array[0].nameSpeciality;
                $scope.idEspecialista = array[0].id_Specialist;
                $scope.idSpecialist = array[0].id_Specialist;
                $scope.consultoriosVarios = array[0].medicalOfficeVarious;
                $scope.isDisabledDataPatient = true;
                $scope.isDisabledAutocomplete = true;
//            $scope.nameConsultorio = array[0].nameMedicalOffice;
//            $scope.direccionConsultorio = array[0].addressMedicalOffice;
                if ($scope.consultoriosVarios == 1) {

                    $scope.isVisibleMedicalO = false;
                    $scope.isVisibleDataSpecialist = true;
                    $scope.isDisablePatient=true;
//                $scope.idMedicalSelect = array[0].idMedicalOficce;
                } else if ($scope.consultoriosVarios == 0) {

                    $scope.isVisibleMedicalO = true;
                    $scope.isVisibleDataSpecialist = false;
                    $scope.isDisablePatient=false;
//                $scope.idMedicalSelect = array[0].idMedicalOficce;
                }

            }
        } else {
            for (var i = 0; i < array.length; i++) {
                $scope.name = array[0].nameSpecialist;
                $scope.idEspecialidad = array[0].idSpeciality;
                $scope.nameSpecialist = array[0].nameSpecialist;
                $scope.nameSpeciality = array[0].nameSpeciality;
                $scope.idEspecialista = array[0].id_Specialist;
                $scope.idSpecialist = array[0].id_Specialist;
                $scope.consultoriosVarios = array[0].medicalOfficeVarious;
                $scope.nameConsultorio = array[0].nameMedicalOffice;
                $scope.idMedicalSelect = array[0].idMedicalOficce;
                $scope.direccionConsultorio = array[0].addressMedicalOffice;
                $scope.isDisabledDataPatient = false;
                $scope.isDisabledAutocomplete = false;
                if ($scope.consultoriosVarios == 1) {
                    $scope.isVisibleMedicalO = false;
                    $scope.isVisibleDataSpecialist = true;
                    $scope.isDisablePatient=true;
                } else if ($scope.consultoriosVarios == 0) {
                    $scope.isVisibleMedicalO = true;
                    $scope.isVisibleDataSpecialist = false;
                    $scope.isDisablePatient=false;
                }
                $scope.isDisabledDataPatient = false;
                $scope.isDisabledAutocomplete = false;
            }

            maxCaracteresConsultorio();
            maxCaracteresDireccion();

//            var texto = document.getElementById("nameConsultorio");
//        var txt = $scope.nameConsultorio;
//        if (txt !== "") {
//            var tamano = txt.length;
//            tamano *= 5.1; //el valor multiplicativo debe cambiarse dependiendo del tamaño de la fuente
//            texto.style.width = tamano + "px";
//        }
        }
    }, function myError(response) {
        console.log("hay un error");
    });
    $scope.serviceGetMedicalOficce = function (specialistMedical) {
        if (specialistMedical !== undefined) {

            $scope.nameConsultorio = specialistMedical.nameMedicalOffice;
            $scope.direccionConsultorio = specialistMedical.addressSpecislist;
            maxCaracteresConsultorio();
            maxCaracteresDireccion();
        }
        if ($scope.consultoriosVarios !== 0) {
            $scope.idMedicalSelect = specialistMedical;
            if ($scope.idMedicalSelect === -1) {
                $scope.isDisabledDataPatient === true;
                $scope.isDisabledAutocomplete = true;
//                alert("consultorio id " + idMedicalSelect); 
                $scope.showAjax = false;
            } else if ($scope.idMedicalSelect !== -1) {
                $scope.isDisabledDataPatient === false;
                $scope.isDisabledAutocomplete = false;
//                alert("consultorio id " + idMedicalSelect); 
                $scope.showAjax = false;
            }
        } else {
            $scope.idMedicalSelect === $scope.idMedicalSelect;
            $scope.showAjax = false;
        }
//               


    };
    
    $scope.ComboConsult=function(){
   $scope.isDisablePatient=false;
}




    $scope.serviceGetPatientforKey = function (patient, idEspecialidad, idSpecialist) {
       


        
            $scope.showAjax = true;
            $scope.seccionPatients = true;
            $http({
                url: '../resources/keysAttentionSpecialist/findPatient',
                method: 'GET',
                params: {
                    patient: patient,
                    idEspecialidad: idEspecialidad,
                    idSpecialist: idSpecialist

                }
            }).then(function success(response) {

                $scope.datapatient = response.data;
                $scope.seccionTablePatient = false;
                if ($scope.datapatient.length > 5) {

                    $scope.styleScrollTablePatients = "scrollPatient";
                    $scope.hideTablePatient = false;
                } else if ($scope.datapatient.length === 0) {
                    $scope.hideTablePatient = false;
                    $scope.isHiddenMessageDefaultTable = false;
                    $scope.styleScrollTablePatients = "";
                } else {
                    $scope.hideTablePatient = false;
                }
                $scope.isDisabledButtGenerar = true;
//                $scope.cambia($scope.datapatient, patient);
                $scope.showAjax = false;
//                
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
                $scope.showAjax = false;
                $scope.createMessage();
                abajo();
            });
        
    };
    $scope.findHistorial = function (idPatientSelect, idSpecialist) {

        $scope.showAjax = true;
        $http({
            url: '../resources/keysAttentionSpecialist/BuscarHistorial',
            method: 'GET',
            params: {
                idPatientSelect: idPatientSelect,
                idSpecialist: idSpecialist

            }
        }).then(function success(response) {
            

            $scope.dataHistorial = response.data;
            if ($scope.dataHistorial.length !=0) {
               $scope.isMessTableHisto=true;
                
            }else{
                $scope.isMessTableHisto=false;
            }
            // console.log($scope.dataHistorial);
            consultHistoryDialogService.openDialog({}).then(function (result) {
                if (result.ok) {
                    // alert('You entered ' + result.name);
                }
            });
            
            $scope.dateMiaxTreatment = $scope.dataHistorial[0].dateEndFilter;
             $scope.dateMinTreatment = $scope.dataHistorial[0].dateStartFilter;
         
                var dateMinHistorial = $scope.dateMinTreatment ;
               // console.log($scope.dateMinTreatment);
                var dateMaxHistorial = $scope.dateMiaxTreatment;
               // console.log($scope.dateMiaxTreatment);
               $("#dateForHistorial").daterangepicker({
                   onSelect: function ()
                        {
                        this.focus();
                    },
                autoUpdateInput: false,
                minDate: dateMinHistorial,
                maxDate: dateMaxHistorial,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar",   "Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },function (start, end, label) {
           $scope.searchHistorial.dateForHistorial=start.format('DD/MM/YYYY');
            $scope.$digest();
           $("#dateForHistorial").val(start.format('DD/MM/YYYY'));   
           $("#dateForHistorial").trigger("click");
          
            });
           $("#dateForHistorial").trigger("click");
            $scope.showAjax = false;
            
//            $("#dateForHistorial").trigger("click");
//                
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
            $scope.showAjax = false;
            $scope.createMessage();
            abajo();
        });
    };
    $scope.selectPatient = function (patientSelect) {
//        alert("llego");
        $scope.showAjax = true;
        if (patientSelect !== "") {
            $scope.namePatient = patientSelect.namePatient;
            $scope.identityNumberPatient = patientSelect.identityNumberPatient;
            $scope.namePlan = patientSelect.namePlan;
            $scope.brithdayP = patientSelect.birthDatePatientSelect;
            $scope.nameRelationship = patientSelect.nameRelationship;
            $scope.nameInsurance = patientSelect.nameInsurance;
            $scope.namecompany = patientSelect.nameCompany;
            $scope.idPlanSelect = patientSelect.idPlan;
            $scope.idPatientSelect = patientSelect.idPatient;
            $scope.idCompanySelect = patientSelect.idCompany;
            $scope.findHistorialPatienSelect($scope.idPatientSelect, $scope.idSpecialist);
            $scope.isDisabledButtGenerar = false;
            $scope.btnnBuscarPatient = true;
            $scope.hideTablePatient = true;
            $scope.seccionTablePatient = true;
            $scope.seccionPatients = false;
            $scope.showAjax = false;
        } else {
            $scope.idLineaSelect = "";
            $scope.showAjax = false;
            $scope.isDisabledButtGenerar = true;
        }
    };
    $scope.showAjax = false;
//    $scope.cambia = function (datapatient, patient) {
//
//        var leonel2 = [];
//
//        for (var i = 0; i < datapatient.length; i++) {
////           
//            leonel2.push({
//                label: [datapatient[i].identityNumberPatient + "     " + datapatient[i].namePatient + "     " + datapatient[i].birthDatePatientSelect + "     " +
//                        datapatient[i].nameInsurance + "     " + datapatient[i].namePlan + "     " + datapatient[i].nameRelationship + "     "+
//                        datapatient[i].nameCompany],
//                 
//                value: datapatient[i].namePatient,
//                id: datapatient[i].idLinea,
//                identityNumber: datapatient[i].identityNumberPatient,
//                namePatient: datapatient[i].namePatient,
//                brithdayP: datapatient[i].birthDatePatientSelect,
//                nameInsurance: datapatient[i].nameInsurance,
//                namePlan: datapatient[i].namePlan,
//                nameRelationship: datapatient[i].nameRelationship,
//                namecompany: datapatient[i].nameCompany,
//                idCompanySelect:datapatient[i].idCompany,
//                idInsurance:datapatient[i].idInsurance,
//                idPlan:datapatient[i].idPlan,
//                idPatient:datapatient[i].idPatient
//
//
//            })
//
//        }
//
//
//        console.log(JSON.stringify(leonel2));
//
//        jqueryAutocompleteUntil.reload("patient", leonel2);
//
//    };

    $scope.findHistorialPatienSelect = function (idPatientSelect, idSpecialist) {

        $scope.showAjax = true;
        $http({
            url: '../resources/keysAttentionSpecialist/BuscarHistorial',
            method: 'GET',
            params: {
                idPatientSelect: idPatientSelect,
                idSpecialist: idSpecialist

            }
        }).then(function success(response) {

            $scope.dataHistorial = response.data;
            if ($scope.dataHistorial.length === 0) {
                $scope.isDisabledButtHistorial = true;
                $scope.isDisabledLabelHistorial = false;
            } else {
                $scope.isDisabledButtHistorial = false;
                $scope.isDisabledLabelHistorial = true;
            }

            $scope.showAjax = false;
//                
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
            $scope.showAjax = false;
            $scope.createMessage();
            abajo();
        });
    };
    $scope.serviceGetKey = function (idPatientSelect, idEspecialista, idMedicalSelect, idCompanySelect, idPlanSelect) {
        var comboValor = "";
        if (!$scope.isVisibleMedicalO) {
            comboValor = idMedicalSelect.idMedicalOficce;
        } else {
            comboValor = idMedicalSelect;
        }
        $scope.showAjax = true;
        $http({
            url: '../resources/keysAttentionSpecialist/RegistrarClave',
            method: 'GET',
            params: {
                idPatientSelect: idPatientSelect,
                idEspecialista: idEspecialista,
                idMedicalSelect: comboValor,
                idCompanySelect: idCompanySelect,
                idPlanSelect: idPlanSelect
            }
        }).then(function success(response) {
            var results = [];
            $scope.dataKey = response.data;
            $scope.closeDialogConfirm();
            var array = $scope.dataKey;
            $scope.numberKey = $scope.dataKey.number;
            $scope.fechaSolicitud = $scope.dataKey.dateKeyDetail;
            $scope.fechaExpiracion = $scope.dataKey.dateExpirationDetail;
            $scope.nameSpecialist = $scope.dataKey.nameSpecialist;
            $scope.nameSpeciality = $scope.dataKey.specialitys;
            $scope.consultorioDetalleClave = $scope.dataKey.nameMedicalOffice;
            $scope.namePatientDetalleClave = $scope.dataKey.namePatient;
            $scope.identityNumberPatient = $scope.dataKey.identityNumberForPatient;
            $scope.datePatient = $scope.dataKey.dateBrithayDetail;
            $scope.namePlan = $scope.dataKey.namePlan;
            $scope.nameAseguradora = $scope.dataKey.nameInsurances;
            $scope.relationPatient = $scope.dataKey.nameRelationShip;
            $scope.nameEnte = $scope.dataKey.ente;
            $scope.emailforData = $scope.dataKey.emailSpecialist;
            if ($scope.emailforData === 1) {
                $scope.isVisibleLabelCorreo = false;
                $scope.isVisibleLabelNoCorreo = true;
            } else {
                $scope.isVisibleLabelCorreo = true;
                $scope.isVisibleLabelNoCorreo = false;
            }
            results.push($scope.dataKey);
            keyDetailDialogService.openDialog({}).then(function (result) {
                if (result.ok) {
                    // alert('You entered ' + result.name);
                }
            });
            $scope.showAjax = false;
            return results;
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
            $scope.showAjax = false;
            $scope.createMessage();
            abajo();
        });
    };
    $scope.findMedicalOffice = function (idSpecialist) {
        $scope.showAjax = true;
        $http({
            url: '../resources/keysAttentionSpecialist/BuscarConsultorios',
            method: 'GET',
            params: {
                idSpecialist: idSpecialist

            }
        }).then(function success(response) {

            $scope.dataConsultorios = response.data;
            $scope.showAjax = false;
//                
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
            $scope.showAjax = false;
            $scope.createMessage();
            abajo();
        });
    };
    $scope.closeDialogconsultHistory = function closeDialog() {
       $scope.searchHistorial.dateForHistorial="";
       $scope.search.numberKey="";
       $scope.search.nameTratment="";
       $scope.search.numberPropertie="";
       $scope.search.specialist="";
       $scope.search.relationShip="";
       $scope.search.insurance="";
       $scope.search.plan="";
       $scope.search.company="";
      $scope.isHiddenTable = true;
        consultHistoryDialogService.closeDialog({});
    }



    $scope.closeDialogKeyDetail = function closeDialog() {
        keyDetailDialogService.closeDialog({});
    }

    $scope.openDialogKeyDetail = function () {
        keyDetailDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
// alert('You entered ' + result.name);
            }
        });
    }
    $scope.closeYesConfirmationGenerateKey = function closeDialogConfirm() {

        ConfirmationDialogService.closeDialog({});
    }

    $scope.closeNoConfirmationGenerateKey = function closeDialogConfirm() {

        ConfirmationDialogService.closeDialog({});
    }



    $scope.openConfirmationGenerateKey = function () {

        ConfirmationDialogService.openDialog({}).then(function (result) {
            if (result.ok) {

// alert('You entered ' + result.name);
            }
        });
    };
    //cerrar dialog
    $scope.closeDialogConfirm = function closeDialog() {
        ConfirmationDialogService.closeDialog({});
    }





    $scope.createMessageSuccess = function () {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = false;
        $scope.ocultar = false;
        growl.addSuccessMessage("OPERACIÓN EXITOSA", config);
        arriba();
    };
    $scope.createMessage = function () {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = false;
        $scope.ocultar = false;
        growl.addErrorMessage("HA OCURRIDO UN ERROR", config);
        arriba();
    };
    $scope.mensajehola = function () {
    }

    $scope.go = function () {
    }


    $scope.validateButtnBuscar = function (patient) {
        if (patient.length >= 3) {
            $scope.btnnBuscarPatient = false;
        }
    }

    function maxCaracteresConsultorio() {
        if ($scope.nameConsultorio.length >= 28) {
            $scope.maxCaracteres = true;
        } else {
            $scope.maxCaracteres = false;
        }
    }
    function maxCaracteresDireccion() {
        if ($scope.direccionConsultorio.length >= 28) {
            $scope.maxCaracteresDir = true;
        } else {
            $scope.maxCaracteresDir = false;
        }
    }
    
    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6500);
    }

    $(".validateAlphanum").bind("keypress", function(event) {
        var charCode = event.which;

        if(charCode == 8 || charCode == 32 || charCode == 241   || charCode == 209)
        {
            return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode);
            return /[a-zA-Z0-9]/.test(keyChar);
        }
    });
    $(".validateAlphabetic").bind("keypress", function(event) {
        var charCode = event.which;

        if(charCode == 8 || charCode == 32 || charCode == 241   || charCode == 209)
        {
            return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode);
            return /^[A-z]+$/.test(keyChar);
        }


    });

    $scope.evalArray = function(){
            console.log("Entra al metodo");
        var numItems = $('.dateForHist').length;

            console.log("numero de registros"+numItems);
        if(numItems >0){
            $scope.isHiddenTable = true;
        }
        else if(numItems ==0){
            $scope.isHiddenTable = false;
   }
    }


});

