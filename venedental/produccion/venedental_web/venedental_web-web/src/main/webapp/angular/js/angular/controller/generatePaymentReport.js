
$(function () {
    $("#dialog").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

});

var app = angular.module("AKmodule", ["angular-growl", "angular.jquery", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap', 'daterangepicker']);

app.controller("AKctrlgenPaymentR", function ($scope, growl, $http, $sce, ConfirmationDialogService,
                                              SuccessDialogService, TreatmentsDialogService) {

    $scope.user = "";
    $(".growl").css("z-index", "-1");
    obtenerNombreUsuario();
    function obtenerNombreUsuario() {
        $http({
            url: '../resources/ManagerParam/getUsername',
            method: 'GET'
        }).then(function success(response) {
            $scope.user = response.data.name;
        }, function error(response) {
        });
    }

    $scope.searchReport = {};
    $scope.searchTratament = {};
    $scope.report = [];
    $scope.listAuxReport = [];


    $scope.checkboxm = "";
    $scope.styleTable = "";

    $scope.isDisabledButtGenRep = true;
    $scope.isDisabledFilters = true;
    $scope.isDisabledCheckboxOnly = false;

    $scope.isHiddenMessageDefault = true;
    $scope.isHiddenTableContent = true;
    $scope.isHiddenTable = true;
    $scope.isHideTableTreat = false;
    $scope.inputHidden = "";


    $scope.showAjax = false;

    //  Variables para controlar el contenido del modal: Detalle de la clave
    $scope.numberkey = "";
    $scope.appdate = "";
    $scope.expireddate = "";
    $scope.namespecialist = "";
    $scope.speciality = "";
    $scope.namepatient = "";
    $scope.idcard = "";
    $scope.birthdate = "";
    $scope.relationship = "";
    $scope.insurance = "";
    $scope.plan = "";
    $scope.consultingname = "";
    $scope.resultKey = "";
    $scope.listTreatments = [];
    $scope.idKey = "";
    $scope.sytleScroll="";
    $scope.msgGrow ="";

    // variables para el area de filtro en la tabla del detalle de clave
    $scope.dateFilterTreatment="";
    $scope.date = "";
    $scope.dateKeyS = "";
    $scope.nameTreat = "";
    $scope.nameReport = "";
    $scope.itemsSelected = [];

    $scope.numReport = "";
    $scope.date = {startDate: null, endDate: null};
    $scope.names = "";
    $scope.fileTempBD = [];// Lista de los recaudos temporal de los tratamientos ya asignados en bd
    $scope.namePatientFilter="";
    $scope.numberKeyFilter="";
    $scope.fileTemp = [];
    $scope.filesBD =[] ;
    $scope.modelTable = "";
    $scope.isDisabledCheckbox = false;   // TRUE es desahibilitado   - FALSE habilitado
    $scope.treatFilt = "";
    $scope.valueDefault = "";
    $scope.listChecked = [];

    $scope.createMessages = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
            growMsg($scope.msgGrow, isError)
        ), config);
        arriba();

    };

    $scope.createMessage = function () {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = false;
        growl.addErrorMessage("PRUEBA", config);
        arriba();

    };

    //cerrar dialogo
    $scope.closeDialog = function closeDialog() {
        TreatmentsDialogService.closeDialog({});
        $scope.searchTratament.dateFilterTreatment="";
        $scope.treatment ="";
    };



    $scope.testDialog = function () {
        TreatmentsDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
            }
        });
    };




// ¿generar Reporte?
    $scope.closeYesConfirmation = function closeDialogConfirm() {

        ConfirmationDialogService.closeDialog({});
        $scope.getNumberCorrelative();



    };

    $scope.closeNoConfirmation = function closeDialogConfirm() {
        ConfirmationDialogService.closeDialog({});
    };



    $scope.openConfirmation = function () {
        ConfirmationDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
            }
        });
    };


    $scope.deleteFile = function (){
        $scope.showAjax = true;

        $http({
            url: '../resources/auditConsultTraceBean/deleteFile',
            method: 'PUT',
        }).then(function success(response) {
            if (response) {
                $scope.showAjax = false;
            }
            $scope.showAjax = false;

        }, function myError(response) {
            $scope.showAjax = false;
        });
    }

    $scope.getDataforReport = function () {
        $scope.showAjax = true;
        $http({
            method: 'GET',
            url: '../resources/reportDentalTreatmentsBean/generatePDF',
            headers: {
                'Content-type': 'application/pdf'
            },
            responseType: 'arraybuffer'
        }).success(function (data, status, headers, config) {
             $scope.reloadPage();
            $scope.msgGrow = "OPERACIÓN REALIZADA CON ÉXITO. SE HA GENERADO EL REPORTE DE TRATAMIENTOS ODONTOLÓGICOS NRO. "+$scope.numReport;
            $scope.createMessages(false);
            var file = new Blob([data], {
                type: 'application/pdf'
            });
            function redirect() {
                setTimeout(function () {
                    document.location.pathname = "web/index.xhtml";
                }, 1000);
            }

            var nameReport = $scope.getFileNameFromHeader(headers('content-disposition'));
            var fileURL = URL.createObjectURL(file);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = nameReport;
            document.body.appendChild(a);
            a.click();
//            $scope.deleteFile();
            // $scope.$digest();
            // $("#namePatient").trigger("click");
            // console.log("click en namePatient...."+$scope.treatFilt);
            setTimeout(function () {
            if($scope.treatFilt.length == 0){
                $scope.isHiddenTable = false;
                $scope.isDisabledCheckboxOnly = true;
            }
            else{
                $scope.isHiddenTable = true;
                $scope.isDisabledCheckboxOnly = false;
            }
            }, 100);

        }).error(function (data, status, headers, config) {

        });
    };

    var countme = 0;
    var countRestEvent = 0;
    function callRp(miCallback){
        if(countRestEvent == $scope.itemsSelected.length){
            miCallback();
        }else if (countme < 10) {
            window.setTimeout(callRp, 1500,miCallback);
        }else{

        }
    }



    $scope.generateReport = function () {
        $scope.showAjax = false;
        $scope.itemsSelected = $scope.getTreatmentSelected();

        for (var i = 0; i < $scope.itemsSelected.length; i++) {
            $scope.serviceUpdateKeysStatus($scope.itemsSelected[i].idKey,$scope.itemsSelected[i].idKeyTreatment,$scope.itemsSelected[i].idToothTreatment, $scope.numReport);
            $scope.getIngoArrayTreatments($scope.itemsSelected[i].dateKeyS, $scope.itemsSelected[i].namePatient,
                $scope.itemsSelected[i].numberKey, $scope.itemsSelected[i].numTooth,
                $scope.itemsSelected[i].nameTreatemnent);
        }


        callRp(function(){
            $scope.getDataforReport();
            $scope.showAjax = true;
        });

    };

    $scope.isObjectEmpty = function isEmptyJSON(obj) {
        for (var i in obj) {
            return false;
        }
        return true;
    };

    // Operación realizada con éxito
    $scope.closeSuccess = function closeDialogSuccess() {
        SuccessDialogService.closeDialog({});
    }


    $scope.openSuccess = function () {
        SuccessDialogService.openDialog({}).then(function (result) {
            if (result.ok) {
            }
        });
    };

    $scope.findKeyDetailsTable = function(idKey){
        $scope.showAjax = true;

        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatmentsBean/findInfoKeyDetails',
            params: {
                idKey: idKey
            }

        }).then(function success(response) {

            if (!$scope.isObjectEmpty(response.data)) {
                $scope.isDisabledFilters = false;
                $scope.listTreatments = response.data;
                // Consultamos y colocamos el scroll sólo si es necesario
                if($scope.listTreatments.length > 5){
                    $scope.styleTable = "styleScroll";
                }
                else{
                    $scope.styleTable = "";
                }

                for (var i = 0; i < $scope.listTreatments.length; i++) {

                    $scope.filesBD = $scope.listTreatments[i].LConsultNameFileOutputList;

                    if($scope.filesBD.length == 0){
                        $scope.listTreatments[i].valueDefault = "-";
                    }else{

                    if($scope.filesBD.length==1){
                        $scope.listTreatments[i].btnDownload0 = 1;
                        $scope.listTreatments[i].btnDownload1 = 0;
                        $scope.listTreatments[i].btnDownload2 = 0;
                        $scope.listTreatments[i].btnDownload3 = 0;
                        $scope.listTreatments[i].btnDownload4 = 0;
                        $scope.listTreatments[i].valueDefault = "";

                    }else if($scope.filesBD.length==2){
                        $scope.listTreatments[i].btnDownload0 = 1;
                        $scope.listTreatments[i].btnDownload1 = 1;
                        $scope.listTreatments[i].btnDownload2 = 0;
                        $scope.listTreatments[i].btnDownload3 = 0;
                        $scope.listTreatments[i].btnDownload4 = 0;
                        $scope.listTreatments[i].valueDefault = "";
                    }else if($scope.filesBD.length==3){
                        $scope.listTreatments[i].btnDownload0 = 1;
                        $scope.listTreatments[i].btnDownload1 = 1;
                        $scope.listTreatments[i].btnDownload2 = 1;
                        $scope.listTreatments[i].btnDownload3 = 0;
                        $scope.listTreatments[i].btnDownload4 = 0;
                        $scope.listTreatments[i].valueDefault = "";
                    }else if($scope.filesBD.length==4){
                        $scope.listTreatments[i].btnDownload0 = 1;
                        $scope.listTreatments[i].btnDownload1 = 1;
                        $scope.listTreatments[i].btnDownload2 = 1;
                        $scope.listTreatments[i].btnDownload3 = 1;
                        $scope.listTreatments[i].btnDownload4 = 0;
                        $scope.listTreatments[i].valueDefault = "";
                    }else if($scope.filesBD.length==5){
                        $scope.listTreatments[i].btnDownload0 = 1;
                        $scope.listTreatments[i].btnDownload1 = 1;
                        $scope.listTreatments[i].btnDownload2 = 1;
                        $scope.listTreatments[i].btnDownload3 = 1;
                        $scope.listTreatments[i].btnDownload4 = 1;
                        $scope.listTreatments[i].valueDefault = "";
                    }
                    else {

                    }

                    for(var z = 0; z < $scope.filesBD.length; z++){
                        var filesCantidad=$scope.filesBD.length;
                        var idFile=z+1;
                        $scope.findFileServer($scope.filesBD[z].nameFile,$scope.listTreatments[i].idTreatment,idFile);
                    }
                    }// else

                }
                if ($scope.listTreatments == null) {

                    $scope.isHiddenMessageDefault = false;
                    $scope.isHiddenTableContent = true;
                } else {
                    $scope.isHiddenMessageDefault = true;
                    $scope.isHiddenTableContent = false;

                }
                $scope.dateMinYmaxDetail(idKey);
                $scope.showAjax = false;
                $("#treatementDet").focus();
                $scope.testDialog();
                // $scope.styleTable = "sytleScroll";

            } else {
                // $scope.styleTable = "";
                $scope.isDisabledFilters = true;
            }

            $scope.showAjax = false;
        }, function myError(response) {
            $scope.showAjax = false;
        });

    }

    $scope.findKeyDetails = function (idKey) {
        $scope.showAjax = true;
        $("#treat").show().focus();
        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatmentsBean/findInfoKey',
            params: {
                idKey: idKey
            }

        }).then(function success(response) {

            if (!$scope.isObjectEmpty(response.data)) {
                $scope.showAjax = false;
                $scope.resultKey = response.data;
                $scope.numberkey = $scope.resultKey.number;
                $scope.appdate = $scope.resultKey.aplicationDateDetails;
                $scope.expireddate = $scope.resultKey.dateExpirationDetail;
                $scope.namespecialist = $scope.resultKey.nameSpecialist;
                $scope.speciality = $scope.resultKey.specialitys;
                $scope.namepatient = $scope.resultKey.namePatient;
                $scope.idcard = $scope.resultKey.identityNumberForPatient;
                $scope.birthdate = $scope.resultKey.dateBrithayDetail;
                $scope.relationship = $scope.resultKey.nameRelationShip;
                $scope.insurance = $scope.resultKey.nameInsurances;
                $scope.plan = $scope.resultKey.namePlan;
                $scope.consultingname = $scope.resultKey.nameMedicalOffice;

                $scope.findKeyDetailsTable(idKey);
                $scope.dateMinYmaxDetail(idKey);
                if ($scope.listTreatments === null) {
                    $scope.isHiddenMessageDefault = false;
                    $scope.isHiddenTableContent = true;
                } else {
                    $scope.isHiddenMessageDefault = true;
                    $scope.isHiddenTableContent = false;
                }
                $scope.showAjax = false;
                $scope.testDialog();
            } else {

            }
            $scope.showAjax = false;

        }, function myError(response) {
            $scope.showAjax = false;
        });

    };

    function obtenerFecha() {
        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatmentsBean/getDate'

        }).then(function success(response) {
            $scope.showAjax = false;
            $scope.minDate = response.data;
            $scope.showAjax = false;
        }, function myError(response) {

        });
    }

    function toDate(dateStr) {
        var parts = dateStr.split("-");
        return new Date(parts[0], parts[1] - 1, parts[2]);
    }

    $(document).ready(function () {
        $scope.showAjax = true;

        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatmentsBean/getListDentalTreatments',
            params: {}
        }).then(function succes(response) {
            $scope.searchReport = {};

            if (response.data.dateKey !== null) {
                $scope.names = response.data;
                for (var i = 0; i < $scope.names.length; i++) {
                    $scope.names[i].isSelected = false;
                    // $scope.treatFilt[i].isSelected = false;
                }


                if($scope.names.length == 0) {
                    $scope.isHiddenTable = false;
                    $scope.isDisabledCheckbox = true;
                    $scope.isDisabledCheckboxOnly = true;
                }
                else{
                    $scope.isHiddenTable = true;
                    $scope.isDisabledCheckbox = false;
                    $scope.isDisabledCheckboxOnly = false;
                }

                for (var i = 0; i < $scope.report.length; i++) {
                    $scope.report[i].isSeledted = false;
                }
                $scope.showAjax = false;
            } else {
                $scope.showAjax = false;
            }

            var dateMin = response.data[0].startDateS;
            var dateMax = response.data[0].endDateS;


            $("#dateFilterReport").daterangepicker({
                    onSelect: function ()
                    {
                        this.focus();
                    },
                    autoUpdateInput: false,
                    minDate: dateMin,
                    maxDate: dateMax,
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'DD/MM/YYYY',
                        daysOfWeek: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                        monthNames: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
                    }
                },
                function (start, end, label) {
                    $scope.searchReport.dateKeyS =start.format('DD/MM/YYYY');
                    $scope.$digest();
                    $("#dateFilterReport").val(start.format('DD/MM/YYYY'));
                    $("#dateFilterReport").trigger("click");

                });
        }, function Error(response) {
            $scope.showAjax = false;
        });
    });


    $scope.maxIteration = 0;
// Cambia el estatus de la clave
    $scope.serviceUpdateKeysStatus = function (idKey,idKeyTreatment,idToothTreatment, numReport) {

        $scope.showAjax = false;

        $http({
            url: '../resources/reportDentalTreatmentsBean/updateKeysStatus',
            method: 'PUT',
            params: {
                idKey: idKey,
                idKeyTreatment:idKeyTreatment,
                idToothTreatment: idToothTreatment,
                numReport: numReport
            }
        }).then(function success(response) {
            if (response.data == 1) {

            }
            countRestEvent++;

        }, function myError(response) {
            $scope.showAjax = false;
        });
        // $.ajax({
        //     // $http({
        //     url: '../resources/reportDentalTreatmentsBean/updateKeysStatus',
        //     //  method: 'GET',
        //     type: 'PUT',
        //     async: false,
        //     data: {
        //         idKey: idKey,
        //                 numReport: numReport
        //     },
        //     success: function (response) {
        //         $scope.responsePlantTreatmentKey = response;
        //
        //
        //     },
        //     error: function (response) {
        //         console.log('entra al error');
        //     }
        //     //  }).then(function success(response) {
        // })
    };

    $scope.enableReport = function () {
        for (var i = 0; i < $scope.treatFilt.length; i++) {
            if ($scope.treatFilt[i].isSelected) {
                $scope.isDisabledButtGenRep = false;
                return;
            }
        }
        $scope.isDisabledButtGenRep = true;


    };

    $scope.getTreatmentSelected = function () {
        var array = [];
        for (var i = 0; i < $scope.treatFilt.length; i++) {
            if ($scope.treatFilt[i].isSelected) {
                array.push($scope.treatFilt[i]);
            }
        }
        return array;
    };


    $scope.enablesReportAll = function (check) {
        for (var i = 0; i < $scope.treatFilt.length; i++) {
            $scope.treatFilt[i].isSelected = check;
        }
        $scope.isDisabledButtGenRep = !check;
    };


    $scope.enableButtGenRepAll = function () {
        if (!document.querySelector('.ak-checkbox-table').checked) {
            $scope.isDisabledButtGenRep = true;
        } else {
            $scope.isDisabledButtGenRep = false;
        }
    };

    $scope.getFileNameFromHeader = function (header) {
        if (!header)
            return null;
        var result = header.split(";")[1].trim().split("=")[1];
        return result.replace(/"/g, '');
    };

    $scope.getNumberCorrelative = function () {

        $scope.showAjax = true;

        $http({
            url: '../resources/reportDentalTreatmentsBean/getCorrelativeNumber',
            method: 'GET',
            params: {}
        }).then(function success(response) {
            if (response.data !== -1) {
                $scope.numReport = response.data;
                $scope.showAjax = false;
                $scope.generateReport();
            } else {
                $scope.showAjax = false;
            }


        }, function myError(response) {
            $scope.showAjax = false;
        });


    };

    $scope.getIngoArrayTreatments = function (dateKey, namePatient, numberKey, numTooth, nameTreatemnent) {
        $scope.showAjax = false;
        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatmentsBean/getInfoArrayTreat',
            params: {
                dateKey: dateKey,
                namePatient: namePatient,
                numberKey: numberKey,
                numTooth: numTooth,
                nameTreatemnent: nameTreatemnent
            }

        }).then(function success(response) {

        }, function myError(response) {


        });

    };

    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6200);
    }

    $scope.dateMinYmaxDetail = function (idKey) {

        $http({
            url: '/resources/ConsultKeysSpecialist/consultDateDetailTreatment',
            method: 'GET',
            params: {
                idKey: idKey,
            }
        }).then(function success(response) {
            $scope.searchTratament = {};
            $scope.dateMiaxTreatment = response.data[0].dateMiaxTreatmentFilter;
            $scope.dateMinTreatment = response.data[0].dateMinTreatmentFilter;
            var dateMinDetail = $scope.dateMinTreatment ;
            var dateMaxDetail = $scope.dateMiaxTreatment;
            $("#datefilterDetail").daterangepicker({
                onSelect: function ()
                {
                    this.focus();
                },
                autoUpdateInput: false,
                minDate: dateMinDetail,
                maxDate: dateMaxDetail,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    daysOfWeek: ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    monthNames: ["Ene","Feb","Mar","Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },function (start, end, label) {
                $scope.searchTratament.dateFilterTreatment=start.format('DD/MM/YYYY');
                $scope.$digest();
                $("#datefilterDetail").val(start.format('DD/MM/YYYY'));
                $("#datefilterDetail").trigger("click");
                // $scope.evalTreats();
            });
        }, function myError(response) {
        });

    }

    $scope.deleteFile = function (){
        $scope.showAjax = true;

        $http({
            url: '../resources/reportDentalTreatmentsBean/deleteFile',
            method: 'PUT',
        }).then(function success(response) {
            if (response) {
                $scope.showAjax = false;
            }

        }, function myError(response) {
            $scope.showAjax = false;
        });
    }

    $scope.getFileNameRecaudos = function (idTreatmentPiece) {

        $http({
            url: '../resources/treatmentRegister/getFileNameRecaudos',
            method: 'GET',
            async: false,
            params: {
                idTreatmentPiece: idTreatmentPiece,
            }
        }).then(function success(response) {

            $scope.namefile = response.data;

        }, function myError(response) {

        });



    }
    $scope.evaluateTableFilter = function(){
        if($scope.treatFilt.length > 0){
            $scope.isDisabledCheckboxOnly = false;
        }else{
            $scope.isDisabledCheckboxOnly = true;
        }

    }
    $scope.ReloadTable = function (){
        $scope.showAjax = true;

        $http({
            method: 'PUT',
            url: '../resources/reportDentalTreatments/getListDentalTreatments',
            params: {}
        }).then(function succes(response) {
            $scope.searchReport = {};

            if (response.data.dateKey !== null) {
                $scope.names = response.data;
                for (var i = 0; i < $scope.names.length; i++) {
                    $scope.names[i].isSelected = false;
                }

                if ($scope.names.length == 0) {
                    $scope.isHiddenTable = false;
                    $scope.isDisabledCheckbox = true;
                    $scope.isDisabledCheckboxOnly = true;
                }
                else{
                    $scope.isHiddenTable = true;
                    $scope.isDisabledCheckbox = false;
                    $scope.isDisabledCheckboxOnly = false
                }

                for (var i = 0; i < $scope.report.length; i++) {
                    $scope.report[i].isSeledted = false;
                }

            } else {

            }
            var dateMin = response.data[0].startDateS;
            var dateMax = response.data[0].endDateS;

            $("#dateFilterReport").daterangepicker({
                onSelect: function ()
                {
                    this.focus();
                },
                autoUpdateInput: false,
                minDate: dateMin,
                maxDate: dateMax,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ["Do", "Lu","Ma", "Mi","Ju","Vi", "Sa"],
                    "monthNames": ["Ene","Feb","Mar","Abr",  "May",  "Jun", "Jul","Ago","Sep","Oct","Nov","Dic"]
                }
            },function (start, end, label) {
                $scope.searchReport.dateKeyS =start.format('DD/MM/YYYY');
                $scope.$digest();
                $("#dateFilterReport").val(start.format('DD/MM/YYYY'));
                $("#dateFilterReport").trigger("click");
            });


        }, function Error(response) {
            $scope.showAjax = false;
        });
        $scope.checkboxm = false;
    }
    $scope.reloadPage = function(){
        var dateFilter,key,namePatient;

        $scope.isDisabledButtGenRep = true;
        dateFilter = $scope.dateFilterReport;
        key = $scope.numberKeyFilter;
        namePatient = $scope.namePatientFilter;
        $scope.ReloadTable();
        $scope.dateFilterReport = dateFilter;
        $('#namePatient').val(namePatient);
        $('#numberKey').val(key);
        $("#dateFilterReport").val(dateFilter);
        $scope.showAjax = false;


    }

    $scope.desCheckItems = function(){
        for (var i = 0; i < $scope.treatFilt.length; i++) {
            $scope.treatFilt[i].isSelected = false;
        }
    }

    $scope.evalteVar = function(isHidden){
        if(isHidden){
            $scope.isHiddenTable = true;
        }
        else{
            $scope.isHiddenTable = false;
        }
    }
    $scope.descargarFileTemp = function (idFile, idT) {


        for (var x = 0; x < $scope.fileTemp.length; x++) {
            if ($scope.fileTemp[x].idT == idT) {
                for (var i = 0; i < $scope.fileTemp[x].recaudos.length; i++) {
                    if ($scope.fileTemp[x].recaudos[i].idFile == idFile) {
                        var nameReport = $scope.fileTemp[x].recaudos[i].name;
                        var file = $scope.fileTemp[x].recaudos[i].file;
                        var fileURL = URL.createObjectURL(file);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = nameReport;
                        document.body.appendChild(a);
                        a.click();
                    }
                }
            }
        }
    }




    $scope.isHidenBtnFile = function (idT, idfile) {

        for (var x = 0; x < $scope.listTreatments.length; x++) {
            if ($scope.listTreatments[x].idTreatment == idT) {
                if(idfile==1){
                    if($scope.listTreatments[x].btnDownload0 == 1){
                        return false;
                    }else if($scope.listTreatments[x].btnDownload0  == 0){
                        return true;
                    }
                } else if(idfile==2){
                    if($scope.listTreatments[x].btnDownload1 == 1){
                        return false;
                    }else if($scope.listTreatments[x].btnDownload1 == 0){
                        return true;
                    }
                }else if(idfile==3){
                    if($scope.listTreatments[x].btnDownload2 == 1){
                        return false;
                    }else if($scope.listTreatments[x].btnDownload2 == 0){
                        return true;
                    }
                }else if(idfile==4){
                    if($scope.listTreatments[x].btnDownload3 == 1){
                        return false;
                    }else if($scope.listTreatments[x].btnDownload3 == 0){
                        return true;
                    }
                }else if(idfile==5){
                    if($scope.listTreatments[x].btnDownload4 == 1){
                        return false;
                    }else if($scope.listTreatments[x].btnDownload4 == 0){
                        return true;
                    }
                }


            }

        }


    }

    $scope.getFileNameSupportFromHeader = function (header) {
        if (!header)
            return null;

        var result = header.split(";")[1].trim().split("=")[1];
        var resultname=result.replace("+","");

        return resultname;
    };

    $scope.findFileServer = function (nameFile,idTreatment,idFile) {

        $http({
            url: '../resources/treatmentRegister/downloadFile',
            method: 'POST',
            params: {
                nameFile: nameFile
            },
            headers: {
                'Content-type': 'application/png,jpg'
            },
            responseType: 'arraybuffer'

        }).success(function (data, status, headers) {

            var file = new Blob([data], {type: 'application/png,jpg'});
            var nameFile= $scope.getFileNameSupportFromHeader(headers('content-disposition'));
            var recaudos=[];
            recaudos.push({idFile: idFile, file: file, name: nameFile});
            if($scope.fileTemp.length==0){
                $scope.fileTemp.push({idT: idTreatment, recaudos: recaudos});
            } else{
                for( var x=0; x<$scope.fileTemp.length; x++){
                    if($scope.fileTemp[x].idT==idTreatment){
                        var files=[];
                        files= $scope.fileTemp[x].recaudos;
                        var cantidad=files.length;
                        var y=cantidad+1;
                        for(var z=0;z<files.length;z++){
                            recaudos.push({idFile: files[z].idFile, file: files[z].file, name: files[z].name});
                        }
                        $scope.fileTemp[x].recaudos=recaudos;
                    }else{
                        $scope.fileTemp.push({idT: idTreatment, recaudos: recaudos});
                    }
                }
            }
        }).error(function (error) {


        });

    };

//para activar la tecla borrar del calendario
    $scope.deleteOnlyDetails = function (evt) {

        var key = evt.which;
        key = String.fromCharCode(key);
        var regex = /^[\b]+$/;
        if (regex.test(key)) {
            $scope.reloadPage();
            $('#dateFilterReport').val('');
            $scope.searchReport.dateKeyS='';
            $scope.searchTratament.dateFilterTreatment='';
            $('#datefilterDetail').val('');



        }
    }


    $scope.deleteOnly = function(evt){

        var key = evt.which;
        key = String.fromCharCode(key);
        var regex = /^[\b]+$/;
        if (regex.test(key)) {
            $scope.reloadPage();
            $('#dateFilterReport').val('');
            $scope.searchReport.dateKeyS='';
            $scope.searchTratament.dateFilterTreatment='';
            $('#datefilterDetail').val('');
            $scope.$digest();
            $("#datefilterDetail").trigger("click");
            $("#dateFilterReport").trigger("click");
        }
    }
    $scope.assignedCheck = function (){
        for(var i = 0; i<$scope.listChecked.length; i++){
            for(var j = 0; j < $scope.treatFilt.length; j++){
                if($scope.listChecked[i].numLine == $scope.treatFilt[j].numLine && $scope.listChecked[i].numLine ==true){
                    $scope.treatFilt[j].isSelected = true;
                }
            }
        }

       // var items = $('.chckBx');
       //  for(var k = 0; k < $scope.treatFilt.length; k++){
       //      for(var l = 0; items.length ; l++){
       //          if($scope.treatFilt[k].isSelected && items[l].isSelected && $scope.treatFilt[k].numLine == items[l].numLine){
       //              $( "#" + items[l].numLine).prop('checked', true);
       //          }
       //      }
       //  }
    }

    $scope.evalArray = function(){
        $scope.saveTemporalItems();
        // var numItems = $('.dateKeyS').length;
        var numItmTmp = $scope.treatFilt.length;
        // if(numItems >0){
        //     $scope.isHiddenTable = true;
        // }
        // else{
        //     $scope.isHiddenTable = false;
        // }

        if(numItmTmp >0){
            $scope.isHiddenTable = true;
        }else{
            $scope.isHiddenTable = false;
        }
        $scope.evaluateTableFilter();
        $scope.assignedCheck();
    }

    $scope.evalTreats = function(){
        // console.log("Entra al metodo");
        var numItems = $('.namTre').length;
        if(numItems >0){
            $scope.isHiddenMessageDefault = true;
        }
        else{
            $scope.isHiddenMessageDefault = false;
        }

    }

    $('#dateFilterReport').keyup(function(e){
        if(e.keyCode == 8)
            $scope.deleteOnly(e);

    })

    $scope.saveTemporalItems = function()  {

        for(var i = 0; i < $scope.treatFilt.length; i++){

            if($scope.treatFilt[i].isSelected){
                $scope.listChecked.push($scope.treatFilt[i]);
            }

        }
      //  console.log(JSON.stringify($scope.listChecked));
    }
    // $scope.disabledCheboxMaster = function(){
    //     if($scope.treatFilt.length == 0){
    //
    //
    //     }
    // }
});
