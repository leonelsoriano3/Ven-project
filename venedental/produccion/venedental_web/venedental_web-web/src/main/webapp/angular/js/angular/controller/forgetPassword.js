/**
 * Created by  leonelsoriano3@gmail.com on 18/03/16.
 */
//"angularBeans"

var app = angular.module("AKmodule", [ "angular-growl", "ngAnimate","akmodule",'ngMaterial','ui.bootstrap']);


app.controller("AKctrlChangePassword", function($scope,growl,$http,$sce) {

    var msgCardIdNoFound = "LA CÉDULA INGRESADA NO EXISTE. SE RECOMIENDA" +
        " COMUNIQUESE A LAS OFICINAS DEL GRUPO VENEDEN A TRAVÉS DEL SIGUIENTE NÚMERO:58 (0212) 8212600";
    var msgsucces = "OPERACIÓN REALIZADA CON ÉXITO,SE HA ENVIADO UN MENSAJE A SU CUENTA DE CORREO ELECTRÓNICO";
    var msgNoFoundEmail = "EL USUARIO NO TIENE CORREO ASOCIADO";
     $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        console.log("Error Interno");
    });

    $scope.showAjax = false;
    $scope.idCard = "";
    $scope.completeName = "";
    $scope.disabledSubmit = true;
    $scope.styleButton = "linkDisabled";
    $scope.nameImg = "../img/ak-find-new-user-disabled.png";

    $scope.msgGrow = "";

    $scope.cleanData = function () {
        $scope.completeName = "";
        $scope.disabledSubmit = true;
        $scope.msgGrow = "";
        $scope.specialist = "";
    };

    $scope.createMessage = function (isError) {
        var config = {};
        config.ttl = 3000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
            growMsg($scope.msgGrow,isError)
        ), config);
arriba();
    };

    $scope.enableButtonSearch = function(){

        if($scope.idCard !== null  && $scope.idCard.length>0){
            $scope.styleButton = "linkEnabled";
            $scope.nameImg = "../img/ak-find-new-user.png";
        }
        else{
            $scope.styleButton = "linkDisabled";
            $scope.nameImg = "../img/ak-find-new-user-disabled.png";

        }
    }

    $scope.findEspecialistByIdCard = function (idCard) {

        $scope.showAjax = true;
        $scope.disabledSubmit = true;

        if(idCard == null || idCard.length !== 0){

            $http({
                url: '../resources/forgetPassword/findUserByIdCard',
                method: 'GET',
                params: {
                    cardId: idCard
                }

            }).then(function success(response) {
                $scope.specialist = response.data;
                if($scope.specialist.id == -1){
                    $scope.msgGrow = "DEBE CREAR SU USUARIO Y CLAVE A TRAVÉS DE LA OPCIÓN REGÍSTRATE";
                    $scope.idCard = "";
                    $scope.createMessage(false);
                    $scope.showAjax = false;
                }
                else{
                    if(response.data.completeName.length === 0){
                        $scope.msgGrow = msgCardIdNoFound;
                        $scope.createMessage(true);
                    }else if(response.data.email.length === 0){
                        $scope.msgGrow = msgNoFoundEmail;
                        $scope.createMessage(true);

                    }else{
                        $scope.completeName = response.data.completeName;
                        $scope.disabledSubmit = false;
                    }
                }
                $scope.showAjax = false;
                abajo();

            }, function myError(response) {

                $scope.completeName  = "";
                $scope.showAjax = false;

            });
        }

    };



    $scope.submmitData = function (idCard) {

        $scope.showAjax = true;
        $scope.disabledSubmit = true;

        $http({
            url: '../resources/forgetPassword/sendData',
            method: 'GET',
            params: {
                cardId: idCard
            }

        }).then(function success(response) {
            $scope.completeName  = "";
            $scope.idCard = "";
            $scope.disabledSubmit = true;
            $scope.msgGrow = msgsucces;
            $scope.createMessage(false);
            $scope.showAjax = false;
            $scope.styleButton = "linkDisabled";
            $scope.nameImg = "../img/ak-find-new-user-disabled.png";
            abajo();

        }, function myError(response) {
            $scope.disabledSubmit = false;
            $scope.showAjax = false;

        });

    };
    
    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6200);
    }


});


