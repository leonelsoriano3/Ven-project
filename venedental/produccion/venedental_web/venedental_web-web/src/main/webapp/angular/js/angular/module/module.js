/**
 * Created by leonelsoriano3@gmail.com on 14/06/16.
 */

/*global angular */
angular.module('angular.jquery', []).config(['$provide', function($provide) {
    'use strict';
    angular.module('angular.jquery').provide = $provide;
}]);
