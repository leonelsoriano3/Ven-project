/**
 * Created by akdeskdev90 on 19/05/16.
 */


/**
 * Created by  leonelsoriano3@gmail.com on 18/03/16.
 */
//"angularBeans"

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}



var app = angular.module("AKmodule", ["angular-growl", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap']);


app.controller("AKctrlRequestRecuperate", function ($scope, growl, $http, $sce) {

    var msgError = "EL LINK ENVIADO A SU CUENTA DE CORREO YA FUE UTILIZADO. REPETIR EL PROCESO NUEVAMENTE";
    var msgComplete = "CAMBIO DE CONTRASEÑA EXITOSO";
    var msgErrorSerivce = "ERROR EN EL SISTEMA";
    $scope.token = getParameterByName('token');
    $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        console.log("Error Interno");
    });

    $scope.idCard = "";
    $scope.completeName = "";
    $scope.userName = "";
    $scope.firstPassword = "";
    $scope.idUsuario = "";





    $scope.activefirstPassWord = true;

    ///////////////////////
    $scope.showAjax = true;




    $http({
        url: '../resources/forgetPassword/findUserByToken',
        method: 'GET',
        params: {
            token: getParameterByName('token')
        }

    }).then(function success(response) {

        if (response.data.idCard.length == 0) {
            $scope.idCard = "";
            $scope.completeName = "";
            $scope.userName = "";
            $scope.msgGrow = msgError;

            $scope.createMessage(false);
            $scope.showAjax = false;

        } else {
            $scope.idCard = response.data.idCard;
            $scope.completeName = response.data.completeNameSpecialist;
            $scope.idUser = response.data.id;
            $scope.userName = response.data.login;
            $scope.activefirstPassWord = false;
            $scope.showAjax = false;
        }

        $scope.showAjax = false;
        abajo();

    }, function myError(response) {
        $scope.userName = "";
        $scope.msgGrow = msgErrorSerivce;
        $scope.createMessage(false);
        $scope.showAjax = false;
        abajo();

    });

    $http({
        url: '../resources/forgetPassword/consumeTokenWhenClicking',
        method: 'GET',
        params: {
            token: $scope.token
        }
    }).then(function success(response) {
        $scope.idUsuario = response.data.id;

    }, function myError(response) {

        $scope.msgGrow = msgError;
        $scope.createMessage(false);
        $scope.showAjax = false;
        abajo();

    });
    //////////////////

    $scope.messageMatchPassWord = false;
    $scope.buttonSubmit = true;
    $scope.activeSecondPassWord = true;
    $scope.firstPasswordMatchRegex = false;
    $scope.stylefirstPassword = "";
    $scope.disabledSubmit = true;
    $scope.showAjax = false;
    $scope.idUser = "";


    $scope.htmlPopover = $sce.trustAsHtml('<div style="color: #cd2313"><span style="font-weight: bold">Recuerde: la contraseña debe cumplir ' +
            'con las siguientes características:</span><br/>' +
            '-Su longitud debe ser entre seis(6) y doce(12) caracteres como máximo<br/>' +
            '-Al menos una letra<br/>' +
            '-Al menos un carácter especial ¡#$& ()?¿[ ]*+.,;<br/>' +
            '-Al menos un número<br/>' +
            '-Es sensible a mayúsculas y minúsculas<br/>' +
            '-No debe contener espacios en blanco<br/>' +
            '</div>');

    $scope.createMessage = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
                growMsg($scope.msgGrow, isError)
                ), config);
        arriba();

    };






    $scope.validePassWord = function () {

        var re = /^((?=.*\d)(?=.*[a-zA-Z])(?=.*[¡#\$&\(\)?¿\]\[\*\+\.;,]).{6,12})$/;
        var str = $scope.firstPassword;
        var m;

        if ((m = re.exec(str)) !== null) {
            if (m.index === re.lastIndex) {

                $scope.activeSecondPassWord = false;
                $scope.firstPasswordMatchRegex = true;
                $scope.stylefirstPassword = "";
                return;
            }
        } else {
            $scope.activeSecondPassWord = true;
        }

        if (str.length != 0) {
            $scope.stylefirstPassword = "input-error";
        } else if (str.length == 0) {
            $scope.stylefirstPassword = '';
        }
        $scope.secondPassWord = "";
        $scope.firstPasswordMatchRegex = false;
        $scope.activeSecondPassWord = true;

    };

    $scope.matchPassword = function () {

        if ($scope.secondPassWord.length > 0) {
            if ($scope.firstPassword == $scope.secondPassWord) {
                $scope.buttonSubmit = false;
                $scope.messageMatchPassWord = false;
            } else {
                $scope.buttonSubmit = true;
                $scope.messageMatchPassWord = true;
            }
        } else {

            $scope.buttonSubmit = true;
            $scope.messageMatchPassWord = false;
        }

    };

    $scope.consumeToken = function (idUs, password) {
        $scope.showAjax = true;

        $http({
            url: '../resources/forgetPassword/consumeToken',
            method: 'POST',
            params: {
                idUs: idUs,
                newPassword: password
            }
        }).then(function success(response) {

            if (!response) {
                $scope.idCard = "";
                $scope.completeName = "";
                $scope.userName = "";
                $scope.msgGrow = msgError;
                $scope.buttonSubmit = true;
                $scope.createMessage(true);
                $scope.showAjax = false;
            } else {
                $scope.idCard = "";
                $scope.completeName = "";
                $scope.userName = "";
                $scope.msgGrow = msgComplete;
                $scope.createMessage(false);
                $scope.showAjax = false;
                $scope.secondPassWord = "";
                $scope.firstPassword = "";
                setTimeout(function () {
                    window.location.href = "/";
                }, 3000);
            }

            $scope.showAjax = false;
            abajo();

        }, function myError(response) {
            $scope.idCard = "";
            $scope.completeName = "";
            $scope.userName = "";
            $scope.msgGrow = msgError;
            $scope.createMessage(false);
            $scope.showAjax = false;
            abajo();

        });
    }

    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6500);
    }

});


