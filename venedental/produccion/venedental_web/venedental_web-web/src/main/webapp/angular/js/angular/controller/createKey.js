/**
 * Created by  leonelsoriano3@gmail.com on 18/03/16.
 * Updated by luiscarrasco1991@gmail.com on 06/05/16
 */
//"angularBeans"








var app = angular.module("AKmodule", ["angular-growl", "ngAnimate", "akmodule", 'ngMaterial', 'ui.bootstrap']);


app.controller("AKctrl", function ($scope, growl, $http, $sce, $timeout) {



    $scope.htmlPopover = $sce.trustAsHtml('<div style="color: #cd2313"><span style="font-weight: bold">Recuerde: la contraseña debe cumplir ' +
            'con las siguientes características:</span><br/>' +
            '-Su longitud debe ser entre seis(6) y doce(12) caracteres como máximo<br/>' +
            '-Al menos una letra<br/>' +
            '-Al menos un carácter especial ¡#$& ()?¿[ ]*+.,;<br/>' +
            '-Al menos un número<br/>' +
            '-Es sensible a mayúsculas y minúsculas<br/>' +
            '-No debe contener espacios en blanco<br/>' +
            '</div>');
     $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
        console.log("Error Interno");
    });



    $scope.especialistCardId = '';
    $scope.showAjax = false;
    $scope.buttonSubmit = true;
    $scope.specialist = "";
    $scope.firstPassword = "";
    $scope.secondPassWord = "";
    $scope.name = "";
    $scope.loginView = "";
    $scope.idSpecialistView = "";
    $scope.styleButton = "linkDisabled";
    $scope.nameImg = "../img/ak-find-new-user-disabled.png";
    $scope.styleButtSalir = 'move-button-beforeConfirm';


    $scope.activeSecondPassWord = true;
    $scope.activefirstPassWord = true;
    $scope.firstPasswordMatchRegex = false;
    $scope.stylefirstPassword = '';
    $scope.isMachtPassword = false;
    $scope.activeCardId = false;
    $scope.hideComponents = false;

    $scope.messageMatchPassWord = false;

    $scope.createMessages = function (isError) {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = true;
        growl.addErrorMessage($sce.trustAsHtml(
                growMsg($scope.msgGrow, isError)
                ), config);
        arriba();

    };

    $scope.createMessage = function () {
        var config = {};
        config.ttl = 6000;
        config.enableHtml = false;
        growl.addErrorMessage("LA CÉDULA INGRESADA NO EXISTE. SE RECOMIENDA COMUNICARSE A LAS OFICINAS DEL" +
                " GRUPO VENEDEN A TRAVÉS DEL SIGUIENTE NÚMERO: 58 (0212) 8212600", config);
        arriba();

    };

    function resetComponent() {
        $scope.especialistCardId = "";
    }



    $scope.validePassWord = function () {

        var notSpace = /^\S*$/;
        var re = /^((?=.*\d)(?=.*[a-zA-Z])(?=.*[¡#\$&\(\)?¿\]\[\*\+\.;,]).{6,12})$/;
        var str = $scope.firstPassword;
        var m;
        var notSpaceRegex;


        if (notSpaceRegex = notSpace.exec(str) === null) {
            if (notSpaceRegex.index === notSpaceRegex.lastIndex) {
                $scope.activeSecondPassWord = true;
                $scope.stylefirstPassword = "input-error";
                $scope.firstPasswordMatchRegex = false;
                return;
            }
        }

        if ((m = re.exec(str)) !== null) {

            if (m.index === re.lastIndex) {

                $scope.activeSecondPassWord = false;
                $scope.firstPasswordMatchRegex = true;
                $scope.stylefirstPassword = "";
                return;
            }
        } else {
            $scope.activeSecondPassWord = true;
        }

        if (str.length != 0) {
            $scope.stylefirstPassword = "input-error";
        } else if (str.length == 0) {
            $scope.stylefirstPassword = '';
        }
        $scope.secondPassWord = "";
        $scope.firstPasswordMatchRegex = false;
        $scope.activeSecondPassWord = true;

    };



    $scope.enableButtonSearch = function () {

        if ($scope.especialistCardId !== null && $scope.especialistCardId.length > 0) {
            $scope.styleButton = "linkEnabled";
            $scope.nameImg = "../img/ak-find-new-user.png";
        } else {
            $scope.styleButton = "linkDisabled";
            $scope.nameImg = "../img/ak-find-new-user-disabled.png";

        }
    }

    $scope.matchPassword = function () {

        if ($scope.secondPassWord.length > 0) {
            if ($scope.firstPassword == $scope.secondPassWord) {
                $scope.buttonSubmit = false;
                $scope.messageMatchPassWord = false;
            } else {
                $scope.buttonSubmit = true;
                $scope.messageMatchPassWord = true;
            }
        } else {

            $scope.buttonSubmit = true;
            $scope.messageMatchPassWord = false;
        }

    };


    $scope.insertUserData = function (cardId, idSpecialist, password) {
        $scope.showAjax = true;
        $http({
            method: 'POST',
            url: '../resources/createKeyBean/insertUserData',
            params: {
                cardId: cardId,
                idSpecialist: idSpecialist,
                password: password
            }

        }).then(function success(response) {
            $scope.showAjax = false;
            if (response.data == 1) {
                $scope.msgGrow = "EL ESPECIALISTA YA POSEE UN USUARIO ASIGNADO"
                $scope.createMessages(false);
                $scope.showAjax = false;

            } else if (response.data == 2) {
                $scope.msgGrow = "OPERACIÓN EXITOSA. SE HA ENVIADO UN MENSAJE A SU CUENTA DE CORREO ELECTRÓNICO"
                $scope.createMessages(false);
                $scope.showAjax = false;
            } else if (response.data == 3) {
                $scope.msgGrow = "OPERACIÓN EXITOSA. NO SE LOGRÓ ENVIAR EL CORREO ELECTRÓNICO"
                $scope.createMessages(false);
                $scope.showAjax = false;
            }
            $scope.styleButtSalir = 'move-button-afterConfirm';
            $scope.hideComponents = true;
            $scope.showAjax = false;
            abajo();

        }), function Error(response) {

            $scope.showAjax = false;
            $scope.msgGrow = "HA OCURRIDO UN ERROR EN EL SERVICIO. INTENTE SU PETICIÓN NUEVAMENTE"
            $scope.createMessages(true);
            abajo();

        }
        $scope.showAjax = false;

    };

    $scope.serviceGetEspecialist = function (cardId) {

        if (cardId.length > 0)
        {
            $scope.showAjax = true;

            $http({
                url: '../resources/createKeyBean/existEspecialist',
                method: 'GET',
                params: {
                    cardId: cardId
                }
            }).then(function success(response) {
                $scope.specialist = response.data;
                $scope.showAjax = false;
                if ($scope.specialist.id == -1) {
                    $scope.msgGrow = "LA CÉDULA INGRESADA NO EXISTE. SE RECOMIENDA COMUNICARSE A LAS OFICINAS DEL" +
                            " GRUPO VENEDEN A TRAVÉS DEL SIGUIENTE NÚMERO:58 (0212) 8212600"
                    $scope.createMessages(true);
                } else if (typeof $scope.specialist.usersId !== "undefined" && $scope.specialist.usersId != 0) {
                    $scope.msgGrow = "EL USUARIO YA SE ENCUENTRA REGISTRADO EN EL SISTEMA"
                    $scope.createMessages(true);
                    $timeout(resetComponent, 6000);
                } else if ($scope.specialist.id == -2) {
                    $scope.msgGrow = "USTED NO POSEE UN CORREO ELECTRóNICO ASOCIADO. SE RECOMIENDA COMUNICARSE A LAS OFICINAS DEL" +
                            " GRUPO VENEDEN A TRAVÉS DEL SIGUIENTE NÚMERO:58 (0212) 8212600"
                    $scope.createMessages(true);

                } else {
                    $scope.activefirstPassWord = true;
                    $scope.name = $scope.specialist.completeName;
                    $scope.loginView = $scope.specialist.login;
                    $scope.activeCardId = true;
                    $scope.idSpecialistView = $scope.specialist.idSpecialist;
                    $scope.activefirstPassWord = false;
                }
                
                abajo();


            }, function myError(response) {
                $scope.myWelcome = response.statusText;
                $scope.showAjax = false;
                $scope.msgGrow = "HA OCURRIDO UN ERROR EN EL SERVICIO. INTENTE SU PETICIÓN NUEVAMENTE"
                $scope.createMessages(true);
                $scope.activefirstPassWord = true;
                $scope.activeSecondPassWord = true;
                $scope.activeCardId = false;
                abajo();
            });
        }
    };
    
    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6200);
    }

});


