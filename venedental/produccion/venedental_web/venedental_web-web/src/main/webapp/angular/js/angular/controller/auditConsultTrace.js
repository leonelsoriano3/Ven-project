/**
 * Created by luis.carrasco@arkiteck.biz on 15/07/16.
 */

var app = angular.module("AKmodule", [ "angular-growl", "ngAnimate","akmodule",'ngMaterial','ui.bootstrap']);


app.controller("AKctrlAuditConsultTrace", function($scope,growl,$http,$sce) {
     $(".growl").css("z-index", "-1");
    $scope.user = "";
    $http({
        url: '../resources/ManagerParam/getUsername',
        method: 'GET'
    }).then(function success(response) {
        $scope.user = response.data.name;
    }, function error(response) {
    });

    //$scope.showAjax = true;

    $scope.names = [];

    $scope.isHiddenMessageDefault = true;
    $scope.isHiddenTableContent = false;
    $scope.isHiddenButtBlue = false;
    $scope.showAjax = true;

    $scope.numreptrat = "";
    $scope.keyst="";
    $scope.numreppay = "";
    $scope.status ="";
    $scope.traceAudit ="";
    $scope.treatment = "";


    $http({
        url: '../resources/auditConsultTraceBean/getListAuditConsultTrace',
        method: 'PUT',
        params : {

        }
    }).then(function success(response) {
        $scope.showAjax = true;

         if(response.data.length > 0){
             $scope.traceAudit = response.data;
             $scope.isHiddenMessageDefault = true;
             $scope.isHiddenTableContent = false;
             $scope.showAjax = false;
             $scope.names = $scope.traceAudit;

         }
         else{
         $scope.isHiddenMessageDefault = false;
         $scope.isHiddenTableContent = true;
             $scope.showAjax = false;

         }



    }, function myError(response) {

        $scope.isHiddenMessageDefault = false;
        $scope.isHiddenTableContent = false;
        $scope.showAjax = false


    });

    $scope.isHidden = function (valor) {
        
        if(valor == -1){
            return true;
        }
        else{
            return false;
        }
    };
    $scope.consultTreatmentReport = function (numReportTreat) {

        $scope.showAjax = true;

        $http({
            method: 'GET',
            url: '../resources/auditConsultTraceBean/generateTreatmentReport',
            params: {
                numReportTreat: numReportTreat
            },
            headers: {
                'Content-type': 'application/pdf'
            },
            responseType: 'arraybuffer'
        }).success(function (data, status, headers, config) {

            var file = new Blob([data], {
                type: 'application/pdf'
            });

            var nameReport = $scope.getFileNameFromHeader(headers('content-disposition'));
            var fileURL = URL.createObjectURL(file);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = nameReport;
            document.body.appendChild(a);
            a.click();
            $scope.showAjax = false;
            $scope.deleteFile();

        }).error(function (data, status, headers, config) {

        });
    };

    $scope.downloadPDF  = function (idPayReport) {
        $scope.showAjax = true;

        $http({
            method: 'GET',
            url: '../resources/auditConsultTraceBean/downloadPDF',
            params : {idPayReport : idPayReport},
            headers: {
                'Content-type': 'application/pdf'
            },
            responseType: 'arraybuffer'
        }).success(function (data, status, headers, config) {

            var file = new Blob([data], {
                type: 'application/pdf'
            });

            var nameReport = $scope.getFileNameFromHeader(headers('content-disposition'));

            var fileURL = URL.createObjectURL(file);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = nameReport;
            document.body.appendChild(a);
            a.click();
            $scope.showAjax = false;
            $scope.deleteFile();

        }).error(function (data, status, headers, config) {
            $scope.showAjax = false;

        });

    }

    $scope.getFileNameFromHeader = function (header) {
        if (!header)
            return null;

        var result = header.split(";")[1].trim().split("=")[1];

        return result.replace(/"/g, '');
    }


    function arriba() {
        $(".growl").css("z-index", "5");
    }
    function abajo() {
        setTimeout(function () {
            $(".growl").css("z-index", "-1");
        }, 6200);
    }

    $scope.deleteFile = function (){
        $scope.showAjax = true;

            $http({
                url: '../resources/auditConsultTraceBean/deleteFile',
                method: 'PUT',
            }).then(function success(response) {
                if (response) {
                    $scope.showAjax = false;
                }
            }, function myError(response) {
                $scope.showAjax = false;
            });
    }

    $scope.evalArray = function(){

       var numItems = $('.numRT').length;

        if(numItems >0){
            $scope.isHiddenMessageDefault = true;

            // alert("Es mayor a 0");
        }
        else{
            $scope.isHiddenMessageDefault = false;

        }
    }

    // $(".validateAlphabetic").bind("keypress", function(event) {
    //     var charCode = event.which;
    //
    //     if(charCode == 8 || charCode == 32 || charCode == 241   || charCode == 209)
    //     {
    //         return;
    //     }
    //     else
    //     {
    //         var keyChar = String.fromCharCode(charCode);
    //         return /^[A-z]+$/.test(keyChar);
    //     }
    // });
    //
    // $(".validateAlphanum").bind("keypress", function(event) {
    //     var charCode = event.which;
    //
    //     if(charCode == 8 || charCode == 32 || charCode == 241   || charCode == 209)
    //     {
    //         return;
    //     }
    //     else
    //     {
    //         var keyChar = String.fromCharCode(charCode);
    //         return /[a-zA-Z0-9]/.test(keyChar);
    //     }
    // });


});



