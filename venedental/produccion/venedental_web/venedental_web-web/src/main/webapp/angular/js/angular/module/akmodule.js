
	/**
 @autor leonelsoriano3@gmail.com
 */

angular.module('akmodule', [])


    .directive('onlyNumber', function() {
        return {
            restrict : "A",
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (inputValue) {

                    var transformedInput = inputValue ? inputValue.replace(/[^\d]/,'') : null;


                    //
                    // str.replace(/\s/g, '');

                    if(/\s/g.test(inputValue)){
                        modelCtrl.$setViewValue(transformedInput.replace(/\s/g, ''));
                        modelCtrl.$render();

                    }else if (transformedInput!=inputValue) {

                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    // transformedInput = transformedInput.replace(' ','');
                    if (transformedInput!=inputValue) {

                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }
                    return transformedInput;
                });
            }
        };
    })



    .directive('numbersOnly',function(){
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            var validateNumber = function (inputValue) {
                 var maxLength;
                if (attrs.max) {
                    maxLength = attrs.max;
                }
                if (inputValue === undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                transformedInput = transformedInput.replace(' ','');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                if (transformedInput.length > maxLength) {
                    transformedInput = transformedInput.substring(0, maxLength);
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                var isNotEmpty = (transformedInput.length === 0) ? true : false;
                ctrl.$setValidity('notEmpty', isNotEmpty);
                return transformedInput;
            };

            ctrl.$parsers.unshift(validateNumber);
            ctrl.$parsers.push(validateNumber);
            attrs.$observe('notEmpty', function () {
                validateNumber(ctrl.$viewValue);
            });
        }
    };
  })

    .directive('jqueryAutocomplete', function ($parse,jqueryAutocompleteUntil) {
        return {
            restrict: 'E',
            replace: true,
            transclude: false,
            template: '<input >',
            scope:{
                arrayValues:'=',
                isSelected: '='
            },

            compile: function (element, attrs) {

                var modelAccessor = $parse(attrs.ngModel);


                return function (scope, element, attrs, controller) {



                    var selected = function(event, selectedItem){
//                        alert("item seleccionado"+selectedItem);

                        scope.isSelected = selectedItem.item;
                        $("buttnFocus").click();
                        $("buttnFocus").click();

                        jqueryAutocompleteUntil.getSelect(selectedItem.item.id);
                        jqueryAutocompleteUntil.focus("buttnFocus");
                        // event.preventDefault();
                    }

                    element.autocomplete({
                        source: scope.arrayValues,

                        select: selected,


                    })

                };



            }
        };

    })

    .directive('teeth', function () {
        return {
            restrict: 'E',
            replace: true,
            scope:{
                toothA:'@',
                numeroDiente:'@',
                akmodel: '=',
            },

            template: '<div style="display: inline;margin-right: -10px"> <input ng-model="akmodel" id="teeth-{{numeroDiente}}" class="checkbox-teeth" name="teeth-{{numeroDiente}}" type="checkbox" checked>' +
            '<label  for="teeth-{{numeroDiente}}"  class="checkbox-teeth-label" ></label>' +
            ' <style>#teeth-{{numeroDiente}} + label[for=teeth-{{numeroDiente}}]:before{content: "{{numeroDiente}}"}</style>'+
            '</div>',

        }
    })

    .directive('teethConsult', function () {
        return {
            restrict: 'E',
            replace: true,
            scope:{
                toothA:'@',
                numeroDiente:'@',
                akmodel: '=',
            },

            template: '<div style="display: inline;margin-right: -10px"> <input ng-model="akmodel" id="teeth-{{numeroDiente}}" class="checkbox-teeth" name="teeth-{{numeroDiente}}" type="checkbox" checked>' +
            '<label  for="teeth-{{numeroDiente}}"  class="checkbox-teeth-label" ng-disabled="toothA"></label>' +
            ' <style>#teeth-{{numeroDiente}} + label[for=teeth-{{numeroDiente}}]:before{content: "{{numeroDiente}}"}</style>'+
            '</div>',

        }
    })

    // .directive('validateAlphaNumeric', function() {
    //     return {
    //         require: 'ngModel',
    //         link: function(scope, element, attr, ngModelCtrl) {
    //             function fromUser(text) {
    //                 var transformedInput = text.replace(/[^0-9a-zA-Z\-\s]/g, '');
    //                 console.log(transformedInput);
    //                 if (transformedInput !== text) {
    //                     ngModelCtrl.$setViewValue(transformedInput);
    //                     ngModelCtrl.$render();
    //                 }
    //                 return transformedInput; // or return Number(transformedInput)
    //             }
    //             ngModelCtrl.$parsers.push(fromUser);
    //         }
    //     };
    // })
    .directive('allowPattern',function() {
        return {
            restrict: "A",
            compile: function(tElement, tAttrs) {
                return function(scope, element, attrs) {
                    // I handle key events
                    element.bind("keypress", function(event) {
                        var keyCode = event.which || event.keyCode;
                        var keyCodeChar = String.fromCharCode(keyCode);
                        // permite utilizar el backspace
                        if(keyCode == 8){
                            return;
                        }
                        else if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                            event.preventDefault();
                            return false;
                        }

                    });
                };
            }
        };
    })
    // .directive('validateAlphaNumeric', function() {
    //     return {
    //         require: 'ngModel',
    //         restrict: 'A',
    //         link: function(scope, elem, attr, ngModel) {
    //
    //             var validator = function(value) {
    //                 if (/^[a-zA-Z0-9]*$/.test(value)) {
    //                     ngModel.$setValidity('alphanumeric', true);
    //                     return value;
    //                 } else {
    //                     ngModel.$setValidity('alphanumeric', false);
    //                     return undefined;
    //                 }
    //             };
    //             ngModel.$parsers.unshift(validator);
    //             ngModel.$formatters.unshift(validator);
    //         }
    //     };
    // })

    .directive('validateAlphabetic', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elem, attr, ngModel) {

                var validator = function(value) {
                    if (/^[A-z]+$/.test(value)) {
                        ngModel.$setValidity('alphanumeric', true);
                        return value;
                    } else {
                        ngModel.$setValidity('alphanumeric', false);
                        return undefined;
                    }
                };
                ngModel.$parsers.unshift(validator);
                ngModel.$formatters.unshift(validator);
            }
        };
    })


    .directive('selectWatcher', function ($timeout) {
        return {
            link: function (scope, element, attr) {
                var last = attr.last;
                if (last === "true") {
                    $timeout(function () {
                        $(element).parent().selectpicker('val', 'any');
                        $(element).parent().selectpicker('refresh');
                    });
                }
            }
        };
    })


    .directive('akmenu', function () {
        "use strict";
        return {
            restrict: 'AE',
            transclude: true,
            replace: 'true',
            template: '<div ng-bind-html="akmenuMessage()" ></div>',
            controller: ['$scope','$http','$sce', function ($scope,$http,$sce) {


                $scope.renderHtml = "";


                $http({
                    url: '../resources/menu/getMenuByUserId',
                    method: 'GET',
                    params: {

                    }
                }).then(function success(response) {

                    var data = response.data;

                    var wrapperMenu = new WrapperMenu();


                    for(var i = 0; i< data.length; i++){

                        var actualItemMenu = new MunuItem().__construct(
                            data[i].idFather,
                            data[i].idMenu,
                            data[i].menuName,
                            data[i].nameFather,
                            data[i].position,
                            data[i].url
                        );


                        wrapperMenu.addChildren(actualItemMenu);

                    }

                    wrapperMenu.loopData();

                    //console.log("tamaño " + wrapperMenu.getOrderMenus().length);
                    // for(var b = 0 ; b < wrapperMenu.getOrderMenus().length ; b ++ ){
                        //  console.log(wrapperMenu.getOrderMenus()[b].getMenuName());
                    // }


                    $scope.renderHtml = wrapperMenu.renderHTML();


                }, function myError(response) {

                    console.log("error al cargar la informacion de base de datos para elm menu "+response.data);

                });




                $scope.akmenuMessage = function () {
                  //  console.log($scope.renderHtml);
                    return  $sce.trustAsHtml($scope.renderHtml);
                }

            }]
        };
    })

    .factory('redirecFactory', ['$http','$window','$q',function($http,$window,$q) {
        return {
            redirectWithParam: function(url,params) {


                $http({
                    url: '../resources/ManagerParam/saveParameter',
                    method: 'POST',
                    params: {
                        url: url,
                        parameters: JSON.stringify(params)
                    }

                }).then(function success(response) {
                    $window.location.href = url;

                }, function error(response) {

                    $window.location.href = url;

                });


            },
             
           
            getParameter: function (url,params,urlError=null) {
                function modelParam(){this.key; this.value;};
                var deferred = $q.defer();
                parametes = [];
                $http({
                    url: '../resources/ManagerParam/getParameter',
                    method: 'GET',
                    params: {
                        url: url,
                        parameters: JSON.stringify(params)
                    }

                }).then(function success(response) {


                    for(var i = 0; i< response.data.length; i++){
                        var tmpModel = new modelParam();

                        if(urlError !== null && (response.data[i].value === null || response.data[i].value === undefined)){
                            $window.location.href = urlError;
                            //
                            // parametes.push(tmpModel);
                            // return deferred.promise;
                        }

                        tmpModel.key = response.data[i].key.split('<AKparam/>').pop();
                        tmpModel.value = response.data[i].value;

                        parametes.push(tmpModel);
                    }

                    //console.log("salgo");

                    deferred.resolve(parametes);

                }, function error(response) {
                    $window.location.href = urlError;
                    deferred.resolve('error');
                } );



                return deferred.promise;


            },
            
            
             getParameterDelete: function (url,params,urlError=null) {
                function modelParam(){this.key; this.value;};
                var deferred = $q.defer();
                parametes = [];
                $http({
                    url: '../resources/ManagerParam/getParameterDelete',
                    method: 'GET',
                    params: {
                        url: url,
                        parameters: JSON.stringify(params)
                    }

                }).then(function success(response) {


                    for(var i = 0; i< response.data.length; i++){
                        var tmpModel = new modelParam();

                        if(urlError !== null && (response.data[i].value === null || response.data[i].value === undefined)){
                            $window.location.href = urlError;
                            //
                            // parametes.push(tmpModel);
                            // return deferred.promise;
                        }

                        tmpModel.key = response.data[i].key.split('<AKparam/>').pop();
                        tmpModel.value = response.data[i].value;

                        parametes.push(tmpModel);
                    }

                    //console.log("salgo");

                    deferred.resolve(parametes);

                }, function error(response) {
                    $window.location.href = urlError;
                    deferred.resolve('error');
                } );



                return deferred.promise;


            },
            
           
            consumeParam: function (url,params,urlError=null) {
                function modelParam(){this.key; this.value;};
                var deferred = $q.defer();
                parametes = [];
                $http({
                    url: '../resources/ManagerParam/putParameter',
                    method: 'GET',
                    params: {
                        url: url,
                        parameters: JSON.stringify(params)
                    }

                }).then(function success(response) {


                    for(var i = 0; i< response.data.length; i++){
                        var tmpModel = new modelParam();


                        if(urlError !== null && (response.data[i].value === null || response.data[i].value === undefined)){
                            $window.location.href = urlError;
                        }

                        tmpModel.key = response.data[i].key.split('<AKparam/>').pop();
                        tmpModel.value = response.data[i].value;

                        parametes.push(tmpModel);
                    }


                    deferred.resolve(parametes);

                }, function error(response) {
                    $window.location.href = urlError;
                    deferred.resolve('error');
                } );

                return deferred.promise;


            }
        };
    }])

    .directive('aktreatmentscomponent', function() {
        "use strict";
        return {
            restrict: 'AE',
            transclude: true,
            replace: 'true',
            scope:{
                akmodel: '='
            },
            template: '<div ng-bind-html="data()" ></div>',
            controller: ['$scope','$http','$sce','redirecFactory', function ($scope,$http,$sce,redirecFactory) {

                $scope.treatmentsWP =[];
                $scope.idKeyParam ="";



                redirecFactory.getParameter("registerDiagnosis.html",['1'],'index.xhtml').then(function(resolve){
                    $scope.idKeyParam =  resolve[0].value;

                    $http({
                        url: '../resources/ConsultKeysSpecialist/getListTreatmentWithoutPiece',
                        method: 'GET',
                        params: {
                            key: $scope.idKeyParam
                        }
                    }).then(function success(response) {
       //                 console.log('hay datos en tratamientos sin piezas module');
                        $scope.treatmentsWP = response.data;

                        $scope.loadHtml($scope.treatmentsWP);


                    }, function myError(response) {

             //           console.log('error datos en tratamientos sin piezas module');
                    });



                });

                $scope.loadHtml = function(array){

                    $scope.isSelected = false;
                    $scope.treatmentsWP = array;
                    $scope.renderHtml = "";
                    var n= array.length;
                    var auxName ="";
                    var countCategory = 0;
                    var nameCategory = $scope.treatmentsWP[0].nameCategory;
                    var arrayNames =[];
                    $scope.akmodel = [];

                    //   Obtengo los nombres de las categorías no repetidas
                    for(var i=0;i<n;i++){

                        auxName = $scope.treatmentsWP[i].nameCategory;

                        if(nameCategory == auxName && countCategory == 0){

                            countCategory++;
                            arrayNames.push(auxName);
                        }
                        else if(nameCategory !== auxName){

                            countCategory++;
                            nameCategory  = $scope.treatmentsWP[i].nameCategory;
                            arrayNames.push(auxName);
                        }
                        else{
                            // nameCategory es igual a auxName
                        }
                    }

                    var m;
                    var r =countCategory/4;
                    m = Math.ceil(r);
                    nameCategory = $scope.treatmentsWP[0].nameCategory;
                    auxName ="";
                    var l = 0;
                    var cnColumn = 0;

                    for(var j=1;j<=m;j++){

                        $scope.renderHtml = '<div class="row">';

                        for(var k=0;k<countCategory;k++){

                            cnColumn++;
                            $scope.renderHtml+=  '<div class="col-md-3" style="font-weight: bold;padding: 2%">' + arrayNames[k];

                            for(l;l<n;l++){

                                if($scope.treatmentsWP[l].nameCategory === nameCategory){

                                    $scope.akmodel.push({
                                        id: $scope.treatmentsWP[l].idTreament,
                                        idPlanTreatSP : $scope.treatmentsWP[l].idPlanTreatment,
                                        modelo: $scope.isSelected,
                                        idBaremoSP : $scope.treatmentsWP[l].idBaremo
                                    });

                                    $scope.renderHtml+= `<div class="checkboxes" style="padding: 2%">
                                         <input ng-model="`+$scope.akmodel[l].modelo+`" style="margin-top: 10px; font-size: 7px;" id="`+$scope.treatmentsWP[l].idTreament+`" 
                                           class="checkbox-custom ak-checkbox" ak-idPlanTreat="`+ $scope.treatmentsWP[l].idPlanTreatment+`" ak-idbaremo="`+ $scope.treatmentsWP[l].idBaremo +`" name="checkbox-3" type="checkbox">
                                         <label for="`+$scope.treatmentsWP[l].idTreament+`" class="checkbox-custom-label" style="font-size:10px;">`+$scope.treatmentsWP[l].nameTreatment+`</label>
                                         </div>`
                                }
                                else{
                                    nameCategory = $scope.treatmentsWP[l].nameCategory;
                                    break;
                                }

                                if(cnColumn%4 == 0){break;}

                            }
                            $scope.renderHtml+=  '</div>';
                        }
                        $scope.renderHtml+= '</div>';
                    }
                }

                $scope.data = function () {
                    return  $sce.trustAsHtml($scope.renderHtml);
                }

                $scope.data();

            }],

        };
    })



    .directive('aktreatmentsconsultdiagnosis', function() {
        "use strict";
        return {
            restrict: 'AE',
            transclude: true,
            replace: 'true',
            scope:{
                idk:'@'
            },
            template: '<div ng-bind-html="data()" ></div>',
            controller: ['$scope','$http','$sce','redirecFactory', function ($scope,$http,$sce,redirecFactory) {

                $scope.treatmentsWP =[];
                $scope.idKeyParam ="";



                redirecFactory.getParameter("consultDiagnosis.html",['1'],'index.xhtml').then(function(resolve){
                    $scope.idKeyParam =  resolve[0].value;



                    $http({
                        url: '../resources/consultDiagnosis/getTreatmentSelect',
                        method: 'GET',
                        params: {
                            key: $scope.idKeyParam
                        }
                    }).then(function success(response) {
                        console.log('hay datos en tratamientos sin piezas module');
                        $scope.treatmentsWP = response.data;

                        $scope.loadHtml($scope.treatmentsWP);

                    }, function myError(response) {

                        console.log('error datos en tratamientos sin piezas module');
                    });



                });



                $scope.loadHtml = function(array){

                    $scope.treatmentsWP = array
                    $scope.renderHtml = "";
                    var n= array.length;
                    var auxName ="";
                    var countCategory = 0;
                    var nameCategory = $scope.treatmentsWP[0].nameCategory;
                    var arrayNames =[];

                    //   Obtengo los nombres de las categorías no repetidas
                    for(var i=0;i<n;i++){

                        auxName = $scope.treatmentsWP[i].nameCategory;

                        if(nameCategory == auxName && countCategory == 0){

                            countCategory++;
                            arrayNames.push(auxName);

                        }
                        else if(nameCategory !== auxName){

                            countCategory++;
                            nameCategory  = $scope.treatmentsWP[i].nameCategory;
                            arrayNames.push(auxName);

                        }
                        else{

                            // nameCategory es igual a auxName

                        }

                    }

                    var m;
                    var r =countCategory/4;
                    //console.log('cantidad de rows sin rdondear: '+r);
                    m = Math.ceil(r);
                    //console.log('cantidad de rows rdondeando: '+m);
                    nameCategory = $scope.treatmentsWP[0].nameCategory;
                    //console.log('categoria 1: '+$scope.treatmentsWP[0].nameCategory);
                    auxName ="";
                    var l = 0;
                    var cnColumn = 0;

                    for(var j=1;j<=m;j++){
                        //  console.log('vuelta numero '+j)

                        $scope.renderHtml = '<div class="row" ng-disabled="true">';

                        for(var k=0;k<countCategory;k++){

                            cnColumn++;
                            $scope.renderHtml+=  '<div class="col-md-3" style="font-weight: bold;padding: 2%">' + arrayNames[k];

                            for(l;l<n;l++){
                                // console.log('vuelta numero '+l);
                                //  console.log('nombre categoria array '+$scope.treatmentsWP[l].nameCategory);
                                //   console.log('nombre categoria var '+nameCategory);

                                if($scope.treatmentsWP[l].nameCategory == nameCategory){

                                    $scope.renderHtml+= `<div class="checkboxes" style="padding: 2%" >
                                         <input style="margin-top: 10px; font-size: 7px;" ng-checked="`+$scope.treatmentsWP[l].isCheck+`" ng-disabled="true"  id="`+$scope.treatmentsWP[l].idTreatment+`" class="checkbox-custom" name="checkbox-3" type="checkbox">
                                         
                                                <label for="`+$scope.treatmentsWP[l].idTreament+`" class="checkbox-custom-label" style="font-size:10px;">`+$scope.treatmentsWP[l].nameTreatment+`</label>
                                         </div>`


                                }
                                else{
                                    nameCategory = $scope.treatmentsWP[l].nameCategory;
                                    break;

                                }

                                //  console.log('cnColumn : '+cnColumn);
                                //   console.log('cnColumn%4 : '+cnColumn%4);

                                if(cnColumn%4 == 0){break;}

                            }

                            $scope.renderHtml+=  '</div>';

                        }


                        $scope.renderHtml+= '</div>';


                    }
                    //   console.log($scope.renderHtml);
                }

                $scope.data = function () {
                    //      console.log($scope.renderHtml);
                    return  $sce.trustAsHtml($scope.renderHtml);
                }

                //      $scope.loadHtml();
                $scope.data();

            }],

        };
    })
    
    .factory('jqueryAutocompleteUntil', function(aktreatments) {
        return {

            
            getSelectTreatment:function(dataselect) {
              
                 alert("factori desde select");

                
            },
            
        };
    })

    .directive('aktreatments', function(jqueryAutocompleteUntil) {
        "use strict";
        return {
            restrict: 'AE',
            transclude: true,
            replace: 'true',
            scope:{
                akmodel: '='
            },
            template: '<div ng-bind-html="data()" ></div>',
            controller: ['$scope','$http','$sce','redirecFactory', function ($scope,$http,$sce,redirecFactory) {

                $scope.treatmentsWP =[];
                $scope.idKeyParam ="";



                redirecFactory.getParameter("registerTreatment.html",['1'],'index.xhtml').then(function(resolve){
                        var parametro = resolve[0].value;
                                var variable = parametro.split(",");
                                $scope.idKeyParam = variable[0];
                    
                    console.log("id clave"+$scope.idKeyParam);

                    $http({
                        url: '../resources/ConsultKeysSpecialist/getListTreatmentWP',
                        method: 'GET',
                        params: {
                            key: $scope.idKeyParam
                            
                        }
                    }).then(function success(response) {
       //                 console.log('hay datos en tratamientos sin piezas module');
                        $scope.treatmentsWP = response.data;
                          console.log(JSON.stringify($scope.treatmentsWP));

                        $scope.loadHtml($scope.treatmentsWP);
                        
                        
//                         akTreatmentsG.getSelectTreatment($scope.treatmentsWP[l].idTreament);


                    }, function myError(response) {

             //           console.log('error datos en tratamientos sin piezas module');
                    });



                });
                        
                

                $scope.loadHtml = function(array){

                    $scope.isSelected = true;
                    $scope.treatmentsWP = array;
                    $scope.renderHtml = "";
                    var n= array.length;
                    var auxName ="";
                    var countCategory = 0;
                    var nameCategory = $scope.treatmentsWP[0].nameCategory;
                    var arrayNames =[];
                    $scope.akmodel = [];

                    //   Obtengo los nombres de las categorías no repetidas
                    for(var i=0;i<n;i++){

                        auxName = $scope.treatmentsWP[i].nameCategory;

                        if(nameCategory == auxName && countCategory == 0){

                            countCategory++;
                            arrayNames.push(auxName);
                        }
                        else if(nameCategory !== auxName){

                            countCategory++;
                            nameCategory  = $scope.treatmentsWP[i].nameCategory;
                            arrayNames.push(auxName);
                        }
                        else{
                            // nameCategory es igual a auxName
                        }
                    }

                    var m;
                    var r =countCategory/4;
                    m = Math.ceil(r);
                    nameCategory = $scope.treatmentsWP[0].nameCategory;
                    auxName ="";
                    var l = 0;
                    var cnColumn = 0;

                    for(var j=1;j<=m;j++){

                        $scope.renderHtml = '<div class="row">';

                        for(var k=0;k<countCategory;k++){

                            cnColumn++;
                            $scope.renderHtml+=  '<div class="col-md-3" style="font-weight: bold;padding: 2%">' + arrayNames[k];

                            for(l;l<n;l++){

                                if($scope.treatmentsWP[l].nameCategory === nameCategory){

                                    $scope.akmodel.push({
                                        id: $scope.treatmentsWP[l].idTreament,
                                        idPlanTreatSP : $scope.treatmentsWP[l].idPlanTreatment,
                                        modelo: $scope.isSelected,
                                        idBaremoSP : $scope.treatmentsWP[l].idBaremo
                                    });
                                    if($scope.treatmentsWP[l].isTreatmentAssigned === false){
                                        $scope.renderHtml+= `<div class="checkboxes" style="padding: 2%">
                                         <input ng-model="`+$scope.akmodel[l].modelo+`" style="margin-top: 10px; font-size: 7px;" id="`+$scope.treatmentsWP[l].idTreament+`" 
                                           class="checkbox-custom ak-checkbox" ak-idPlanTreat="`+ $scope.treatmentsWP[l].idPlanTreatment+`" ak-idbaremo="`+ $scope.treatmentsWP[l].idBaremo +`" name="checkbox-3"
                                            type="checkbox" >
                                         <label for="`+$scope.treatmentsWP[l].idTreament+`" class="checkbox-custom-label" style="font-size:10px;">`+$scope.treatmentsWP[l].nameTreatment+`</label>
                                         </div>`
                                    }else{
                                        $scope.renderHtml+= `<div class="checkboxes" style="padding: 2%">
                                         <input ng-model="`+$scope.akmodel[l].modelo+`" style="margin-top: 10px; font-size: 7px;" id="`+$scope.treatmentsWP[l].idTreament+`" 
                                           class="checkbox-custom ak-checkbox" ak-idPlanTreat="`+ $scope.treatmentsWP[l].idPlanTreatment+`" ak-idbaremo="`+ $scope.treatmentsWP[l].idBaremo +`" name="checkbox-3"
                                           ak-idTreatmentKey="`+ $scope.treatmentsWP[l].idTreatmentKey+`" type="checkbox" checked >
                                         <label for="`+$scope.treatmentsWP[l].idTreament+`" class="checkbox-custom-label" style="font-size:10px;">`+$scope.treatmentsWP[l].nameTreatment+`</label>
                                         </div>`
                                    }
                                    
                                    
                               
                                }
                                else{
                                    nameCategory = $scope.treatmentsWP[l].nameCategory;
                                    break;
                                }

                                if(cnColumn%4 == 0){break;}

                            }
                            $scope.renderHtml+=  '</div>';
                        }
                        $scope.renderHtml+= '</div>';
                    }
                }

                $scope.data = function () {
                    return  $sce.trustAsHtml($scope.renderHtml);
                }

                $scope.data();

            }],

        };
    })


    .directive('openDialog', function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attr, ctrl) {
                var dialogId = '#' + attr.openDialog;
                elem.bind('click', function(e) {
                    $(dialogId).dialog('open');
                });
            }
        };
    });
