/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.propertiesPlanTreatmentKey;

/**
 *
 * @author akdeskdev90
 */
public class FindProPlaTreKeyByPlanTreKeyInput {
    
    private Integer idPlanTreatmentKey;
    private Integer idStatus;

    public FindProPlaTreKeyByPlanTreKeyInput(Integer idPlanTreatmentKey, Integer idStatus) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.idStatus = idStatus;
    }    
    

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }
        
}
