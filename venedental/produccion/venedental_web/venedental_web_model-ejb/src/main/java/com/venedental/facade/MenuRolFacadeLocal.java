/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.MenuRol;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Anzola
 */
@Local
public interface MenuRolFacadeLocal {

    MenuRol create(MenuRol menuRol);

    MenuRol edit(MenuRol menuRol);

    void remove(MenuRol menuRol);

    MenuRol find(Object id);

    List<MenuRol> findAll();

    List<MenuRol> findByRolId(int rolId);

    List<MenuRol> findRange(int[] range);

    int count();

}
