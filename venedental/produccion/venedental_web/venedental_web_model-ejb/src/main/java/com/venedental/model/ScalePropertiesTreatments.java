/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Desarrollo
 */
@Entity
@Table(name = "scale_properties_treatments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScalePropertiesTreatments.findAll", query = "SELECT s FROM ScalePropertiesTreatments s"),
    @NamedQuery(name = "ScalePropertiesTreatments.findById", query = "SELECT s FROM ScalePropertiesTreatments s WHERE s.id = :id"),
    @NamedQuery(name = "ScalePropertiesTreatments.findByPrice", query = "SELECT s FROM ScalePropertiesTreatments s WHERE s.price = :price"),    
    @NamedQuery(name = "ScalePropertiesTreatments.findByDateScale", query = "SELECT s FROM ScalePropertiesTreatments s WHERE s.dateScale = :dateScale")})
public class ScalePropertiesTreatments implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "date_scale")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateScale;

    @JoinColumn(name = "id_scale", referencedColumnName = "id")
    @ManyToOne
    private Scale idScale;

    @JoinColumn(name = "id_properties_treatments", referencedColumnName = "id")
    @ManyToOne
    private PropertiesTreatments idPropertiesTreatments;

    @Column(name = "price")
    private Double price;

    @OneToMany(mappedBy = "idScalePropertiesTreatments")
    private List<ScalePropertiesTreatmentsSpecialist> scalePropertiesTreatmentsSpecialistList;

    @OneToMany(mappedBy = "idScalePropertiesTreatments")
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList;

    public ScalePropertiesTreatments() {
    }

    public ScalePropertiesTreatments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateScale() {
        return dateScale;
    }

    public void setDateScale(Date dateScale) {
        this.dateScale = dateScale;
    }

    public Scale getIdScale() {
        return idScale;
    }

    public void setIdScale(Scale idScale) {
        this.idScale = idScale;
    }

    public PropertiesTreatments getIdPropertiesTreatments() {
        return idPropertiesTreatments;
    }

    public void setIdPropertiesTreatments(PropertiesTreatments idPropertiesTreatments) {
        this.idPropertiesTreatments = idPropertiesTreatments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScalePropertiesTreatments)) {
            return false;
        }
        ScalePropertiesTreatments other = (ScalePropertiesTreatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.ScalePropertiesTreatments[ id=" + id + " ]";
    }

    @XmlTransient
    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeyList() {
        return propertiesPlansTreatmentsKeyList;
    }

    public void setPropertiesPlansTreatmentsKeyList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList) {
        this.propertiesPlansTreatmentsKeyList = propertiesPlansTreatmentsKeyList;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @XmlTransient
    public List<ScalePropertiesTreatmentsSpecialist> getScalePropertiesTreatmentsSpecialistList() {
        return scalePropertiesTreatmentsSpecialistList;
    }

    public void setScalePropertiesTreatmentsSpecialistList(List<ScalePropertiesTreatmentsSpecialist> scalePropertiesTreatmentsSpecialistList) {
        this.scalePropertiesTreatmentsSpecialistList = scalePropertiesTreatmentsSpecialistList;
    }

}
