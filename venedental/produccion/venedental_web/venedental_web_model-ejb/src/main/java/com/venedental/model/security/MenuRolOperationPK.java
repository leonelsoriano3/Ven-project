/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Embeddable
public class MenuRolOperationPK implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_rol_id")
    private Integer menuRolId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "operation_id")
    private Integer operationId;

    public MenuRolOperationPK() {
    }

    public MenuRolOperationPK(Integer menuRolId, Integer operationId) {
        this.menuRolId = menuRolId;
        this.operationId = operationId;
    }

    public int getMenuRolId() {
        return menuRolId;
    }

    public void setMenuRolId(Integer menuRolId) {
        this.menuRolId = menuRolId;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) menuRolId;
        hash += (int) operationId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuRolOperationPK)) {
            return false;
        }
        MenuRolOperationPK other = (MenuRolOperationPK) object;
        if (this.menuRolId != other.menuRolId) {
            return false;
        }
        if (this.operationId != other.operationId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.MenuRolOperationPK[ menuRolId=" + menuRolId + ", operationId=" + operationId + " ]";
    }

}
