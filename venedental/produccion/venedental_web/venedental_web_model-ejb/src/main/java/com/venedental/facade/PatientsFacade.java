/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Patients;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PatientsFacade extends AbstractFacade<Patients> implements PatientsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PatientsFacade() {
        super(Patients.class);
    }

    @Override
    public Patients edit(Patients entity) {
        return super.edit(entity);
    }

    @Override
    public Patients create(Patients entity) {
        return super.create(entity);
    }
    
    @Override
    public List<Patients> findById(Integer id) {
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findById", Patients.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findBeneficiaries(int identity_number) {
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findBeneficiaries", Patients.class);
        query.setParameter("identity_number", identity_number);
        return query.getResultList();
    }
           
    @Override
    public List<Patients> findByIdentityNumber(int identity_number) {
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findByIdentityNumber", Patients.class);
        query.setParameter("identity_number", identity_number);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findByIdentityNumberAndRelationShip(int identityNumber, int relationShip) {
        
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findByIdentityNumberAndRelationShip", Patients.class);
        query.setParameter("identity_number",identityNumber);
        query.setParameter("relationship_id",relationShip);

        return query.getResultList();
    }
    
     @Override
    public List<Patients> findByIdentityNumberOrNamePatients(String filter) {
        String NewFilter = "'%" + filter + "%'";
        String sql= "SELECT * FROM patients where status = 1 and identity_number LIKE " + NewFilter + " OR complete_name LIKE " + NewFilter + " and relationship_id=10";
        Query query = em.createNativeQuery(sql, Patients.class);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findByIdentityNumberHolderOrNamePatients(String filter) {
        String NewFilter = "'%" + filter + "%'";
        String sql = "SELECT * FROM patients where status = 1 and identity_number LIKE " + NewFilter + " OR complete_name LIKE " + NewFilter;
        Query query = em.createNativeQuery(sql, Patients.class);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findByIdentityNumberHolder(int identity_number_holder){
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findByIdentityNumberHolder", Patients.class);
        query.setParameter("identity_number_holder", identity_number_holder);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findAllByIdentityNumberHolder(int filter){
        String sql= "SELECT * FROM patients where identity_number_holder='"+filter+"' and status='1'";
        Query query = em.createNativeQuery(sql, Patients.class);
        return query.getResultList();
    }
    
    @Override
    public List<Patients> findByDuplicate(int identityNumber, int identityNumberHolder, Date birthDate, String completeName) {
        TypedQuery<Patients> query = em.createNamedQuery("Patients.findByDuplicate", Patients.class);
        query.setParameter("identityNumber", identityNumber);
        query.setParameter("identityNumberHolder", identityNumberHolder);
        query.setParameter("dateOfBirth", birthDate);
        query.setParameter("completeName", completeName);
        return query.getResultList();
    }
    
     @Override
    public List<Patients> findBeneficiariesAvailable(Integer idTitular, Integer idTypeAtencion) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_PATIENTS_G_002", Patients.class);
        query.registerStoredProcedureParameter("PI_ID_TITULAR", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_AREA_SALUD", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_TITULAR", idTitular);
        query.setParameter("PI_ID_AREA_SALUD", idTypeAtencion);
        return query.getResultList();
    }
    
    
    
    
    
}
