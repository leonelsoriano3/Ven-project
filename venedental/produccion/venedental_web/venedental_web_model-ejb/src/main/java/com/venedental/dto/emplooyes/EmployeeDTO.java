package com.venedental.dto.emplooyes;

/**
 * Created by akdesk10 on 15/06/16.
 */
public class EmployeeDTO {

    private Integer idEmployee;
    private Integer identityNumber;
    private String completeName;



    public Integer getIdEmployee() {return idEmployee; }

    public void setIdEmployee(Integer idEmployee) { this.idEmployee = idEmployee;}

    public Integer getIdentityNumber() {return identityNumber;}

    public void setIdentityNumber(Integer identityNumber) {this.identityNumber = identityNumber;}

    public String getCompleteName() {return completeName;}

    public void setCompleteName(String completeName) {this.completeName = completeName;}
}
