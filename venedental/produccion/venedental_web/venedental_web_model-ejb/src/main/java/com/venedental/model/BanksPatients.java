/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 * 
 * last edition Anthony Torres <anthonyjtyepez at gmail.com>
 */
@Entity
@Table(name = "banks_patients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BanksPatients.findAll", query = "SELECT b FROM BanksPatients b"),
    @NamedQuery(name = "BanksPatients.findById", query = "SELECT b FROM BanksPatients b WHERE b.id = :id"),
    @NamedQuery(name = "BanksPatients.findByAccountNumber", query = "SELECT b FROM BanksPatients b WHERE b.accountNumber = :accountNumber")})
public class BanksPatients implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "account_number")
    private String accountNumber;

    @JoinColumn(name = "patients_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Patients patientsId;

    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Banks bankId;
    
    @OneToMany(cascade=CascadeType.ALL , mappedBy="banksPatientsId")
    private List<Reimbursement> reimbursementList;

    public BanksPatients() {
    }

    public BanksPatients(Integer id) {
        this.id = id;
    }

    public BanksPatients(Integer id, String accountNumber) {
        this.id = id;
        this.accountNumber = accountNumber;
    }

    public List<Reimbursement> getReimbursementList() {
        return reimbursementList;
    }

    public void setReimbursementList(List<Reimbursement> reimbursementList) {
        this.reimbursementList = reimbursementList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Patients getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Patients patientsId) {
        this.patientsId = patientsId;
    }

    public Banks getBankId() {
        return bankId;
    }

    public void setBankId(Banks bankId) {
        this.bankId = bankId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanksPatients)) {
            return false;
        }
        BanksPatients other = (BanksPatients) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.BanksPatients[ id=" + id + " ]";
    }

}
