/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.insurances.FindInsurancesDAO;
import com.venedental.dao.reportAudited.entityDAO;
import com.venedental.dao.reportAudited.stateDAO;
import com.venedental.dto.AuditReportInput;
import com.venedental.dto.AuditReportOutput;
import com.venedental.dto.reportAudited.EntityOutputDTO;
import com.venedental.dto.reportAudited.StateOutputDTO;
import com.venedental.facade.store_procedure.AuditDAO;
import com.venedental.model.Insurances;
import java.sql.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import javax.validation.ReportAsSingleViolation;

/**
 *
 * @author akdesk10
 */
@Stateless
public class ServicesReportAudited {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds; 
    
    public List<EntityOutputDTO> entity(String query)  {
       
        entityDAO entity = new entityDAO(ds);
        entity.execute(query);
        return entity.getResultList();
    }
    
    
    public List<StateOutputDTO> state(Integer id)  {
       stateDAO state = new stateDAO(ds);
       state.execute(id);
       return state.getResultList();
    }
    
    public List<AuditReportOutput> auditReportOutput(Date startDate,
           Date endDate, Integer idInsurance,Integer idHealthArea, 
           Integer idPlan,Integer idTrat,Integer idPiece, Integer ent, Integer state, Integer idReport)  {
        AuditDAO audit = new AuditDAO(ds);
        AuditReportInput input = new AuditReportInput();
        input.setStartDate(startDate);
        input.setEndDate(endDate);
        input.setIdInsurance(idInsurance);
        input.setHealthArea(idHealthArea);
        input.setIdPlan(idPlan);
        input.setIdTreatment(idTrat);
        input.setIdPieza(idPiece);
        input.setIdEntity(ent);
        input.setIdState(state);
        input.setIdResult(idReport);
        audit.execute(input);
        return audit.getResultList();
        
        }
}
