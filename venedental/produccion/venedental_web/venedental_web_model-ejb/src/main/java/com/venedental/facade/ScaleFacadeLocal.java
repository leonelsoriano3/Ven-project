/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.Scale;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author CarlosDaniel
 */
@Local
public interface ScaleFacadeLocal {

    Scale create(Scale scale);

    Scale edit(Scale scale);

    void remove(Scale scale);

    Scale find(Object id);

    List<Scale> findAll();

    List<Scale> findRange(int[] range);

    int count();
    
}
