package com.venedental.dao.user;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.User.InTokenDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.SQLException;
//import java.sql.SQLType;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class UpdateTokenDAO extends AbstractCommandDAO<InTokenDTO> {

    public UpdateTokenDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_U_002", 3);
    }

    @Override
    public void prepareInput(InTokenDTO input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getUserId());
        if(input.getToken().length() == 0){
            statement.setNull(2, java.sql.Types.NULL);
        }else{
            statement.setString(2, input.getToken());
        }
        if(input.getExpiredDate().length() == 0){
            statement.setNull(3, java.sql.Types.NULL);
        }else{
            statement.setString(3,input.getExpiredDate());
        }
    }


}
