/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "menu_rol")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuRol.findAll", query = "SELECT m FROM MenuRol m"),
    @NamedQuery(name = "MenuRol.findById", query = "SELECT m FROM MenuRol m WHERE m.id = :id"),
    @NamedQuery(name = "MenuRol.findByRolId", query = "SELECT m FROM MenuRol m WHERE m.rolId.id = :rol_id"),
    @NamedQuery(name = "MenuRol.findByMenuId", query = "SELECT m FROM MenuRol m WHERE m.menuId = :menuId"),
    @NamedQuery(name = "MenuRol.findByStatus", query = "SELECT m FROM MenuRol m WHERE m.status = :status")})
public class MenuRol implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuRol")
    private List<MenuRolOperation> menuRolOperationList;
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    
    @JoinColumn(name = "rol_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Rol rolId;
    
    @JoinColumn(name = "menu_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Menu menuId;

    public MenuRol() {
    }

    public MenuRol(Integer id) {
        this.id = id;
    }

    public MenuRol(Integer id, Integer status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Rol getRolId() {
        return rolId;
    }

    public void setRolId(Rol rolId) {
        this.rolId = rolId;
    }

    public Menu getMenuId() {
        return menuId;
    }

    public void setMenuId(Menu menuId) {
        this.menuId = menuId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuRol)) {
            return false;
        }
        MenuRol other = (MenuRol) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.MenuRol[ id=" + id + " ]";
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @XmlTransient
    public List<MenuRolOperation> getMenuRolOperationList() {
        return menuRolOperationList;
    }

    public void setMenuRolOperationList(List<MenuRolOperation> menuRolOperationList) {
        this.menuRolOperationList = menuRolOperationList;
    }

}
