/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Addresses;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface AddressesFacadeLocal {

    Addresses create(Addresses addresses);

    Addresses edit(Addresses addresses);

    void remove(Addresses addresses);

    Addresses find(Object id);

    List<Addresses> findAll();

    List<Addresses> findRange(int[] range);

    int count();
    
}
