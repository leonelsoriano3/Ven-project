/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.SpecialistJob;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface SpecialistJobFacadeLocal {

    SpecialistJob create(SpecialistJob specialistJob);

    SpecialistJob edit(SpecialistJob specialistJob);

    void remove(SpecialistJob specialistJob);

    SpecialistJob find(Object id);

    List<SpecialistJob> findAll();

    List<SpecialistJob> findRange(int[] range);

    int count();
    
    List<SpecialistJob> findBySpecialist(Integer id);
    
    void deleteJobsDependency(Integer id);
    
}
