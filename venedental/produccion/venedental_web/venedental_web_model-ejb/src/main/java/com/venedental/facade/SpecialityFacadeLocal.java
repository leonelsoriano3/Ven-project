/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Speciality;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface SpecialityFacadeLocal {

    Speciality create(Speciality speciality);

    Speciality edit(Speciality speciality);

    void remove(Speciality speciality);

    Speciality find(Object id);

    List<Speciality> findAll();

    List<Speciality> findRange(int[] range);

    int count();
    
}
