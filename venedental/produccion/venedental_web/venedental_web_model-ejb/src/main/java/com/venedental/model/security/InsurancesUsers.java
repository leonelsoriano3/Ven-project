/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.security;

import com.venedental.model.Insurances;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Edgar
 */
@Entity
@Table(name = "insurances_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsurancesUsers.findAll", query = "SELECT i FROM InsurancesUsers i"),
    @NamedQuery(name = "InsurancesUsers.findByInsurancesId", query = "SELECT i FROM InsurancesUsers i WHERE i.insurancesUsersPK.insurancesId = :insurancesId"),
    @NamedQuery(name = "InsurancesUsers.findByUsersId", query = "SELECT i FROM InsurancesUsers i WHERE i.insurancesUsersPK.usersId = :usersId"),
    @NamedQuery(name = "InsurancesUsers.findByStatus", query = "SELECT i FROM InsurancesUsers i WHERE i.status = :status")})
public class InsurancesUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected InsurancesUsersPK insurancesUsersPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @JoinColumn(name = "users_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    @JoinColumn(name = "insurances_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Insurances insurances;

    public InsurancesUsers() {
    }

    public InsurancesUsers(InsurancesUsersPK insurancesUsersPK) {
        this.insurancesUsersPK = insurancesUsersPK;
    }

    public InsurancesUsers(InsurancesUsersPK insurancesUsersPK, int status) {
        this.insurancesUsersPK = insurancesUsersPK;
        this.status = status;
    }

    public InsurancesUsers(int insurancesId, int usersId) {
        this.insurancesUsersPK = new InsurancesUsersPK(insurancesId, usersId);
    }

    public InsurancesUsersPK getInsurancesUsersPK() {
        return insurancesUsersPK;
    }

    public void setInsurancesUsersPK(InsurancesUsersPK insurancesUsersPK) {
        this.insurancesUsersPK = insurancesUsersPK;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Insurances getInsurances() {
        return insurances;
    }

    public void setInsurances(Insurances insurances) {
        this.insurances = insurances;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insurancesUsersPK != null ? insurancesUsersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsurancesUsers)) {
            return false;
        }
        InsurancesUsers other = (InsurancesUsers) object;
        if ((this.insurancesUsersPK == null && other.insurancesUsersPK != null) || (this.insurancesUsersPK != null && !this.insurancesUsersPK.equals(other.insurancesUsersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.InsurancesUsers[ insurancesUsersPK=" + insurancesUsersPK + " ]";
    }

}
