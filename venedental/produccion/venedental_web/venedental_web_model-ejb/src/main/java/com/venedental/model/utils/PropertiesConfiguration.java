/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class PropertiesConfiguration {

    private static Properties props;

    static {
        props = new Properties();
        try {
            PropertiesConfiguration util = new PropertiesConfiguration();
            props = util.getPropertiesFromClasspath("config_massive_loading.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String get(String key) {
        return props.getProperty(key);
    }

    private Properties getPropertiesFromClasspath(String propFileName)
            throws IOException {
        Properties myProp = new Properties();
        InputStream inputStream
                = this.getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName
                    + "' not found in the classpath");
        }
        myProp.load(inputStream);
        return myProp;
    }

}
