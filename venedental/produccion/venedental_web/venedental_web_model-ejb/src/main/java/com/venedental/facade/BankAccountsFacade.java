/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BankAccounts;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class BankAccountsFacade extends AbstractFacade<BankAccounts> implements BankAccountsFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BankAccountsFacade() {
        super(BankAccounts.class);
    }
    
}
