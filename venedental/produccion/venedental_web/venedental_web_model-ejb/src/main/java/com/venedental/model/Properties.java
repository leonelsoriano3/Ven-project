/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "properties", schema = "")
@NamedQueries({
    @NamedQuery(name = "Properties.findByKey", query = "SELECT a FROM Properties a WHERE a.key = :key")})
public class Properties implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "keyprop")
    @NotNull
    private String key;

    @Size(max = 255)
    @Column(name = "string_value")
    private String stringValue;

    @Lob
    @Column(name = "blob_value")
    private byte[] blobValue;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Size(max = 255)
    @Column(name = "quadrant")
    private Integer quadrant;

    @Size(max = 45)
    @Column(name = "propertiescol")
    private String propertiescol;

    @OneToMany(mappedBy = "propertiesId")
    private Collection<PropertiesTreatments> propertiesTreatmentsCollection;

    public Properties() {
    }

    public Properties(String key, String stringValue) {
        this.key = key;
        this.stringValue = stringValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(Integer quadrant) {
        this.quadrant = quadrant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Properties)) {
            return false;
        }
        Properties other = (Properties) object;
        return (this.id != null || other.id == null) && (this.key == null || this.key.equals(other.id));
    }

    public byte[] getBlobValue() {
        return blobValue;
    }

    public void setBlobValue(byte[] blobValue) {
        this.blobValue = blobValue;
    }

    public String getPropertiescol() {
        return propertiescol;
    }

    public void setPropertiescol(String propertiescol) {
        this.propertiescol = propertiescol;
    }

    @XmlTransient
    public Collection<PropertiesTreatments> getPropertiesTreatmentsCollection() {
        return propertiesTreatmentsCollection;
    }

    public void setPropertiesTreatmentsCollection(Collection<PropertiesTreatments> propertiesTreatmentsCollection) {
        this.propertiesTreatmentsCollection = propertiesTreatmentsCollection;
    }

}
