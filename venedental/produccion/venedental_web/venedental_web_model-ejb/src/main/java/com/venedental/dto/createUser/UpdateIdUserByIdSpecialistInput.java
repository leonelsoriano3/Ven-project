package com.venedental.dto.createUser;

/**
 * Created by akdesk10 on 13/05/16.
 */
public class UpdateIdUserByIdSpecialistInput {

    Integer idEspecialist;
    Integer idUser;

    public UpdateIdUserByIdSpecialistInput(Integer idEspecialist, Integer idUser){
        this.idEspecialist= idEspecialist;
        this.idUser = idUser;
    }

    public Integer getIdEspecialist() {return idEspecialist;}

    public void setIdEspecialist(Integer idEspecialist) {this.idEspecialist = idEspecialist;}

    public Integer getIdUser() {return idUser;}

    public void setIdUser(Integer idUser) {this.idUser = idUser;
    }
}
