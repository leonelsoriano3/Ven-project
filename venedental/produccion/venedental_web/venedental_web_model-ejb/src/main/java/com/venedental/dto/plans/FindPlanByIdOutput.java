/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.plans;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPlanByIdOutput {
   
    private Integer idPlan;

    public FindPlanByIdOutput() {
    }

    public FindPlanByIdOutput(Integer idPlan) {        
        this.idPlan = idPlan;
        
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }
    
    


       
}
