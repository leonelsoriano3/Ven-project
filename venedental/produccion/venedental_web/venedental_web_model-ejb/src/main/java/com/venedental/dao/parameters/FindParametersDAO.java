/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.parameters;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.parameters.FindParametersOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindParametersDAO extends AbstractQueryDAO<String, FindParametersOutput> {

    public FindParametersDAO(DataSource dataSource) {
        super(dataSource, "SP_PARAMETER_CORREO", 1);
    }

    @Override
    public void prepareInput(String input, CallableStatement statement) throws SQLException {        
        statement.setString(1, input); 
    }

    @Override
    public FindParametersOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        FindParametersOutput findParametersOutput= new FindParametersOutput();
        findParametersOutput.setIdParameters(rs.getInt("id"));
        findParametersOutput.setName(rs.getString("nombre"));
        findParametersOutput.setCodigo(rs.getString("string_codigo"));
        findParametersOutput.setDescription(rs.getString("descripcion"));
        findParametersOutput.setValueString(rs.getString("valor_string"));
        findParametersOutput.setValueDouble(rs.getDouble("valor_numerico"));
        return findParametersOutput;
    }

}
