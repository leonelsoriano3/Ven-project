package com.venedental.dao.user;

/**
 * Created by luis.carrasco@arkiteck.biz on 22/06/16.
 */

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.createUser.UpdateUserByTokenInput;
import com.venedental.dto.createUser.UpdateUserByTokenOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ConsumeTokenClickingDAO extends AbstractQueryDAO<UpdateUserByTokenInput,UpdateUserByTokenOutput> {
    public ConsumeTokenClickingDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_U_003",1);

    }


    @Override
    public void prepareInput(UpdateUserByTokenInput input, CallableStatement statement) throws SQLException {
        statement.setString(1,input.getToken());
    }

    @Override
    public UpdateUserByTokenOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        UpdateUserByTokenOutput updateUserByTokenOutput = new UpdateUserByTokenOutput();
        updateUserByTokenOutput.setEmail(rs.getString("email"));
        updateUserByTokenOutput.setId(rs.getInt("id"));

        return updateUserByTokenOutput;
    }



}
