/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.reportAuditConsultTrace;

import java.sql.Date;

/**
 *
 * @author AKDESK21
 */
public class ReportStatusAnalysisKeysInput {
    private Date startDate;
    private Date endDate;
    private String idKey;
    private String specialist;

    public ReportStatusAnalysisKeysInput() {
    }
    
    public ReportStatusAnalysisKeysInput(Date startDate, Date endDate, String idKey, String specialist) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.idKey = idKey;
        this.specialist = specialist;
    }
    
     public ReportStatusAnalysisKeysInput(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getIdKey() {
        return idKey;
    }

    public void setIdKey(String idKey) {
        this.idKey = idKey;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }
    
    
}
