/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "properties_treatments")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "PropertiesTreatments.findAll", query = "SELECT p FROM PropertiesTreatments p"),
        @NamedQuery(name = "PropertiesTreatments.findByTreatmenstId", query = "SELECT p FROM PropertiesTreatments p WHERE p.treatmentsId.id = :treatmentsId"),
        @NamedQuery(name = "PropertiesTreatments.findById", query = "SELECT p FROM PropertiesTreatments p WHERE p.id = :id")})
public class PropertiesTreatments implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "properties_id", referencedColumnName = "id")
    @ManyToOne
    private Properties propertiesId;

    @JoinColumn(name = "treatments_id", referencedColumnName = "id")
    @ManyToOne
    private Treatments treatmentsId;




    @OneToMany(mappedBy = "idPropertiesTreatments")
    private List<ScalePropertiesTreatments> scalePropertiesTreatmentsList;

    @JoinTable(name = "rule_properties_treatments", joinColumns = {
            @JoinColumn(name = "id_properties_treatments", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "id_rule_treatments", referencedColumnName = "id")})
    @ManyToMany
    private List<RuleTreatments> ruleTreatmentsList;

    @Transient
    private String priceString;

    @Transient
    private String propertieStringValue;

    @Transient
    private int propertieQuadrant;

    @Transient
    private Integer idPropertie;

    @Transient
    private Integer idScale;

    @Transient
    private Double price;

    @Transient
    private Boolean encontrado;


    public PropertiesTreatments() {
    }

    public PropertiesTreatments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Properties getPropertiesId() {
        return propertiesId;
    }

    public void setPropertiesId(Properties propertiesId) {
        this.propertiesId = propertiesId;
    }

    public Treatments getTreatmentsId() {
        return treatmentsId;
    }

    public void setTreatmentsId(Treatments treatmentsId) {
        this.treatmentsId = treatmentsId;
    }

    public String getPriceString() {
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

    public String getPropertieStringValue() {
        return propertieStringValue;
    }

    public void setPropertieStringValue(String propertieStringValue) {
        this.propertieStringValue = propertieStringValue;
    }

    public int getPropertieQuadrant() {
        return propertieQuadrant;
    }

    public void setPropertieQuadrant(int propertieQuadrant) {
        this.propertieQuadrant = propertieQuadrant;
    }

    @XmlTransient
    public List<ScalePropertiesTreatments> getScalePropertiesTreatmentsList() {
        return scalePropertiesTreatmentsList;
    }

    public void setScalePropertiesTreatmentsList(List<ScalePropertiesTreatments> scalePropertiesTreatmentsList) {
        this.scalePropertiesTreatmentsList = scalePropertiesTreatmentsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropertiesTreatments)) {
            return false;
        }
        PropertiesTreatments other = (PropertiesTreatments) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.venedental.model.PropertiesTreatments[ id=" + id + " ]";
    }

    @XmlTransient
    public List<RuleTreatments> getRuleTreatmentsList() {
        return ruleTreatmentsList;
    }

    public void setRuleTreatmentsList(List<RuleTreatments> ruleTreatmentsList) {
        this.ruleTreatmentsList = ruleTreatmentsList;
    }

    public Integer getIdPropertie() {
        return idPropertie;
    }

    public void setIdPropertie(Integer idPropertie) {
        this.idPropertie = idPropertie;
    }

    public Integer getIdScale() {
        return idScale;
    }

    public void setIdScale(Integer idScale) {
        this.idScale = idScale;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getEncontrado() {
        return encontrado;
    }

    public void setEncontrado(Boolean encontrado) {
        this.encontrado = encontrado;
    }
}