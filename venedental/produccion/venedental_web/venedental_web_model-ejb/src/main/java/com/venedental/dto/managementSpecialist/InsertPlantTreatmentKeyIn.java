package com.venedental.dto.managementSpecialist;

import java.util.Date;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 */
public class InsertPlantTreatmentKeyIn {

     Integer idKey;
    Integer idPlanTreatment;
    Date dateApplication;
    Integer idStatus;
    Integer idScaleTreatments;
    String observation;
    Integer idUser;
    Date dateReception;

    public InsertPlantTreatmentKeyIn() {
    }

    
    
    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(Date dateApplication) {
        this.dateApplication = dateApplication;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdScaleTreatments() {
        return idScaleTreatments;
    }

    public void setIdScaleTreatments(Integer idScaleTreatments) {
        this.idScaleTreatments = idScaleTreatments;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

   
}
