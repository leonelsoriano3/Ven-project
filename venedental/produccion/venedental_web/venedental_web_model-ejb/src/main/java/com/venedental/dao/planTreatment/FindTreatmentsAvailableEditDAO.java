/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatment;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatments.TreatmentsAvailableOutput;
import com.venedental.dto.planTreatments.FindPtByKeyAndPlanInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindTreatmentsAvailableEditDAO extends AbstractQueryDAO<FindPtByKeyAndPlanInput, TreatmentsAvailableOutput> {

    public FindTreatmentsAvailableEditDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PRUEBATREATMENTS_G_003", 2);
    }

    @Override
    public void prepareInput(FindPtByKeyAndPlanInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKeyPatient());
        statement.setInt(2, input.getIdPlan());

    }

    @Override
    public TreatmentsAvailableOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        TreatmentsAvailableOutput treatmentsAvailableByPatientOutput = new TreatmentsAvailableOutput();
        treatmentsAvailableByPatientOutput.setIdPlanTratment(rs.getInt("ID_PLAN_TRATAMIENTO"));
        treatmentsAvailableByPatientOutput.setIdTreatment(rs.getInt("ID_TRATAMIENTO"));
        treatmentsAvailableByPatientOutput.setDescriptionTreatment(rs.getString("NOMBRE_TRATAMIENTO"));
        treatmentsAvailableByPatientOutput.setIdScaleTreatment(rs.getInt("ID_BAREMO"));
        treatmentsAvailableByPatientOutput.setPrice(rs.getDouble("PRECIO_BAREMO_NUMERO"));
        treatmentsAvailableByPatientOutput.setPriceString(rs.getString("PRECIO_BAREMO_CADENA"));
        return treatmentsAvailableByPatientOutput;
    }

}