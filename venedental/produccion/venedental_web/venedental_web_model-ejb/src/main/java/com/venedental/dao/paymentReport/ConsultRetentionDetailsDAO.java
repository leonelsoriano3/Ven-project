/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.paymentReport.ConsultRetentionDetailsOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class ConsultRetentionDetailsDAO extends AbstractQueryDAO<PaymentReportInput, ConsultRetentionDetailsOutput> {
    
    public ConsultRetentionDetailsDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_ConsultarDetalleISLR", 2);
    }

    @Override
    public void prepareInput(PaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setString(2, input.getNumberISLR());
    }

    @Override
    public ConsultRetentionDetailsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultRetentionDetailsOutput consultRetentionDetailsOutput = new ConsultRetentionDetailsOutput();
        consultRetentionDetailsOutput.setBillDate(rs.getDate("fecha_factura"));
        consultRetentionDetailsOutput.setBillNumber(rs.getString("numero_factura"));
        consultRetentionDetailsOutput.setControlNumber(rs.getString("numero_control"));
        consultRetentionDetailsOutput.setAmountTotal(rs.getDouble("monto_total"));
        consultRetentionDetailsOutput.setBaseRetention(rs.getDouble("base_retencion"));
        consultRetentionDetailsOutput.setRateRetentionISLR(rs.getDouble("tasa_retencion_islr"));
        consultRetentionDetailsOutput.setAmountISLR(rs.getDouble("monto_retencion_islr"));
        return consultRetentionDetailsOutput;
    } 
}
