package com.venedental.dao.user;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.User.UserDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 19/05/16.
 */
public class FindUserByTokenDAO extends AbstractQueryDAO<String,UserDTO> {


    public FindUserByTokenDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_G_005", 1);
    }

    @Override
    public void prepareInput(String input, CallableStatement statement) throws SQLException {
        statement.setString(1, input);
    }

    @Override
    public UserDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        UserDTO user = new UserDTO();
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        user.setStatus(rs.getInt("status"));
        user.setEmail(rs.getString("email"));
        user.setToken(rs.getString("token"));
        user.setExpiredDate(rs.getString("expired_date"));

        return user;
    }
}
