
package com.venedental.dto.analysisKeys;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyHaveTreatmentOutPut {

    private Boolean containsTreatment;

    public Boolean getContainsTreatment() {
        return containsTreatment;
    }

    public void SetContainsTreatment(Boolean generateStatus) {
        this.containsTreatment = generateStatus;
    }

}
