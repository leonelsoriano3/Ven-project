/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindDataSpecialistOutput;
import com.venedental.dto.managementSpecialist.ConsultKeysSpecialistOutput;
import com.venedental.dto.managementSpecialist.ConsultKeysSpecialistInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysSpecialistDAO extends AbstractQueryDAO<ConsultKeysSpecialistInput, ConsultKeysSpecialistOutput> {
    
    
    public ConsultKeysSpecialistDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_G_009", 7);
    }
    
     @Override
    public void prepareInput(ConsultKeysSpecialistInput input, CallableStatement statement) throws SQLException {
        if(input.getIdSpecialist()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getIdSpecialist()); 
        }
         if(input.getDate()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setDate(2,input.getDate()); 
        }
          if(input.getDateEnd()==null){
            statement.setNull(3,java.sql.Types.NULL);
        }else{
            statement.setDate(3, input.getDateEnd());  
        }
           if(input.getNumberKey()==null){
            statement.setNull(4,java.sql.Types.NULL);
        }else{
           statement.setString(4, input.getNumberKey()); 
        }
        
           if(input.getPatient()==null){
            statement.setNull(5,java.sql.Types.NULL);
        }else{
           statement.setString(5, input.getPatient()); 
        }
           
        
        statement.registerOutParameter(6, java.sql.Types.DATE); 
        statement.registerOutParameter(7, java.sql.Types.DATE);
         
    }

    @Override
    public ConsultKeysSpecialistOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Date dateMinFilter;
        Date dateEndDateFilter;
        dateMinFilter = statement.getDate(6);
        dateEndDateFilter = statement.getDate(7);
        
       ConsultKeysSpecialistOutput consultKeysSpecialistOutput = new ConsultKeysSpecialistOutput();
        consultKeysSpecialistOutput.setNumberKey(rs.getString("numero_clave"));
        consultKeysSpecialistOutput.setDelete(rs.getInt("puede_eliminar"));
        consultKeysSpecialistOutput.setNamePatient(rs.getString("nombre_paciente"));
        consultKeysSpecialistOutput.setIdentityNumberpatient(rs.getInt("cedula_paciente"));
        consultKeysSpecialistOutput.setIdPatient(rs.getInt("id_paciente"));
        consultKeysSpecialistOutput.setIdKey(rs.getInt("id_clave"));
         consultKeysSpecialistOutput.setIdentityNumberpatient(rs.getInt("cedula_paciente"));
        consultKeysSpecialistOutput.setDateKey(rs.getDate("fecha_clave"));
        consultKeysSpecialistOutput.setRegisterTreatment(rs.getInt("puede_registrar"));
        
        consultKeysSpecialistOutput.setDateStart(dateMinFilter);
        consultKeysSpecialistOutput.setDateEnd(dateEndDateFilter);
        
        return consultKeysSpecialistOutput;
    }
    
    
}
