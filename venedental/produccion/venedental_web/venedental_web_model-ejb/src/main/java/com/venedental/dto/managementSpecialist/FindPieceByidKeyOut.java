package com.venedental.dto.managementSpecialist;

/**
 * Created by akdeskdev90 on 15/07/16.
 */
public class FindPieceByidKeyOut {

    private Integer idPiece;
    private String namePiece;
    private Integer quadrant;
    private Integer line;
    private boolean isSelect;
    private Integer exodoncia;
    private Integer treatments;

    public FindPieceByidKeyOut() {
    }

    public Integer getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(Integer idPiece) {
        this.idPiece = idPiece;
    }

    public String getNamePiece() {
        return namePiece;
    }

    public void setNamePiece(String namePiece) {
        this.namePiece = namePiece;
    }

    public Integer getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(Integer quadrant) {
        this.quadrant = quadrant;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public boolean isIsSelect() {
        return isSelect;
    }

    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    public Integer getExodoncia() {
        return exodoncia;
    }

    public void setExodoncia(Integer exodoncia) {
        this.exodoncia = exodoncia;
    }

    public Integer getTreatments() {
        return treatments;
    }

    public void setTreatments(Integer treatments) {
        this.treatments = treatments;
    }
    
    
    
    
}
