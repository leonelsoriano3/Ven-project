/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade.store_procedure;

import com.venedental.model.Addresses;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author usuario
 * @param <InputType>
 * @param <OutputType>
 */
public abstract class AbstractStoreProcedureFacade<InputType, OutputType> {

    private final String storeProcedureName;
    protected final ParameterMode IN = ParameterMode.IN;
    protected final ParameterMode OUT = ParameterMode.OUT;

    protected AbstractStoreProcedureFacade(String storeProcedureName) {
        this.storeProcedureName = storeProcedureName;
    }

    public List<OutputType> execute(InputType input) {
        List list = new ArrayList();
        StoredProcedureQuery query = getEntityManager().createStoredProcedureQuery(storeProcedureName);
        prepareParameter(query);
        prepareInput(query, input);
        List listResult = query.getResultList();
        for (Object object : listResult) {
            Object[] result = (Object[]) object;
            OutputType output = prepareOutput(result);
            list.add(output);
        }
        return list;
    }

    protected abstract EntityManager getEntityManager();

    protected abstract void prepareParameter(StoredProcedureQuery query);

    protected abstract void prepareInput(StoredProcedureQuery query, InputType input);

    protected abstract OutputType prepareOutput(Object[] result);

}
