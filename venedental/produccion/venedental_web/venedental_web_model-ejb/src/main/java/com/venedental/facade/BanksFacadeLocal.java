/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Banks;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface BanksFacadeLocal {

    Banks create(Banks banks);

    Banks edit(Banks banks);

    void remove(Banks banks);

    Banks find(Object id);

    List<Banks> findAll();
    
    List<Banks> findAllOrderByName();

    List<Banks> findRange(int[] range);

    int count();
    
}
