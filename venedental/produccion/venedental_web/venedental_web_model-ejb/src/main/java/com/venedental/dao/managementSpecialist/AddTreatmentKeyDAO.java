/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.AddTreatmentForKeyInput;
import com.venedental.dto.managementSpecialist.AddTreatmentForKeyOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class AddTreatmentKeyDAO extends AbstractQueryDAO<AddTreatmentForKeyInput, AddTreatmentForKeyOutput>{
    
     public AddTreatmentKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_I_002", 9);
    }
    
     @Override
    public void prepareInput(AddTreatmentForKeyInput input, CallableStatement statement) throws SQLException {
        if(input.getIdKey()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getIdKey()); 
        }
        
         if(input.getIdPlantreatment()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setInt(2,input.getIdPlantreatment()); 
        }
        
         
         if(input.getDate()==null){
            statement.setNull(3,java.sql.Types.NULL);
        }else{
            statement.setDate(3,input.getDate()); 
        }
          
         if(input.getIdStatus()==null){
            statement.setNull(4,java.sql.Types.NULL);
        }else{
            statement.setInt(4,input.getIdStatus()); 
        }
         
          if(input.getBaremoTreatment()==null){
            statement.setNull(5,java.sql.Types.NULL);
        }else{
            statement.setInt(5,input.getBaremoTreatment()); 
        }
          
           if(input.getObservation()==null){
            statement.setNull(6,java.sql.Types.NULL);
        }else{
            statement.setString(6,input.getObservation()); 
        }
           
            if(input.getIdUser()==null){
            statement.setNull(7,java.sql.Types.NULL);
        }else{
            statement.setInt(7,input.getIdUser()); 
        }
            
             if(input.getDateReception()==null){
            statement.setNull(8,java.sql.Types.NULL);
        }else{
            statement.setDate(8,input.getDateReception()); 
        }
         
             
             if(input.getCreatediagnosis()==null){
            statement.setNull(9,java.sql.Types.NULL);
        }else{
            statement.setInt(9,input.getCreatediagnosis()); 
        }
             
            
         
    }

    @Override
    public AddTreatmentForKeyOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
       
       AddTreatmentForKeyOutput addTreatmentForKeyOutput = new AddTreatmentForKeyOutput();
        addTreatmentForKeyOutput.setIdTreatmentKey(rs.getInt(" id_tratamiento_clave"));
        addTreatmentForKeyOutput.setIdTreatmentKeyDiagnosis(rs.getInt("id_tratamiento_clave_diagnostico"));
        
        return addTreatmentForKeyOutput;
    }
    
    
    
}
