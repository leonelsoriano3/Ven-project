/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultTreatmentsKeyRTOutput {
    
    
    private Integer id_categoria,id_plan_tratamiento,id_baremo,id_tratamiento_clave,
            tratamiento_registrado,id_tratamiento;
    
    private String nombre_tratamiento,nombre_categoria;

    public ConsultTreatmentsKeyRTOutput() {
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }

    public Integer getId_plan_tratamiento() {
        return id_plan_tratamiento;
    }

    public void setId_plan_tratamiento(Integer id_plan_tratamiento) {
        this.id_plan_tratamiento = id_plan_tratamiento;
    }

    public Integer getId_baremo() {
        return id_baremo;
    }

    public void setId_baremo(Integer id_baremo) {
        this.id_baremo = id_baremo;
    }

    public Integer getId_tratamiento_clave() {
        return id_tratamiento_clave;
    }

    public void setId_tratamiento_clave(Integer id_tratamiento_clave) {
        this.id_tratamiento_clave = id_tratamiento_clave;
    }

    public Integer getTratamiento_registrado() {
        return tratamiento_registrado;
    }

    public void setTratamiento_registrado(Integer tratamiento_registrado) {
        this.tratamiento_registrado = tratamiento_registrado;
    }

    public Integer getId_tratamiento() {
        return id_tratamiento;
    }

    public void setId_tratamiento(Integer id_tratamiento) {
        this.id_tratamiento = id_tratamiento;
    }

    public String getNombre_tratamiento() {
        return nombre_tratamiento;
    }

    public void setNombre_tratamiento(String nombre_tratamiento) {
        this.nombre_tratamiento = nombre_tratamiento;
    }

    public String getNombre_categoria() {
        return nombre_categoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }
    
    
    
}
