/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.Scale;
import com.venedental.model.ScalePropertiesTreatmentsSpecialist;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ScalePropertiesTreatmentsSpecialistFacadeLocal {

    ScalePropertiesTreatmentsSpecialist create(ScalePropertiesTreatmentsSpecialist scale);

   ScalePropertiesTreatmentsSpecialist edit(ScalePropertiesTreatmentsSpecialist scale);

    void remove(ScalePropertiesTreatmentsSpecialist scale);

   ScalePropertiesTreatmentsSpecialist find(Object id);

    List<ScalePropertiesTreatmentsSpecialist> findAll();

    List<ScalePropertiesTreatmentsSpecialist> findRange(int[] range);

    int count();
    
}
