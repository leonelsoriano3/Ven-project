/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.PaymentRefund.GenerateTxtRefundDAO;
import com.venedental.dto.PaymentRefund.GenerateTxtInput;
import com.venedental.dto.PaymentRefund.GenerateTxtOutput;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
@Stateless
public class ServicePaymentRefund {
    
  @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
  
  /**
     * Metodo para txt en estado por abonar
     * 
     * @param id_paciente
     * @param id_estatus
     * @param start_date
     * @param end_date
     * @return 
     * @throws java.sql.SQLException
  */ 
  public List<GenerateTxtOutput> ConfirmGenerateTxtRefund(Integer id_paciente, Integer id_estatus, Date start_date, Date end_date) throws SQLException {
        GenerateTxtRefundDAO generateTxtRefundDAO = new GenerateTxtRefundDAO(ds);
        GenerateTxtInput input = new GenerateTxtInput(id_paciente, id_estatus,start_date, end_date);
        generateTxtRefundDAO.execute(input);
        return generateTxtRefundDAO.getResultList();
    }
  
}
