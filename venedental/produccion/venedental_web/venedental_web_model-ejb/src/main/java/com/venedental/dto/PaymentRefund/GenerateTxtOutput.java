/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.PaymentRefund;

/**
 *
 * @author akdesk01
 */
public class GenerateTxtOutput {
    
    String accountNumber,patient_Cedula,paymentReference,patientName,emailRecipient,entireAmount,decimalAmount;
    
     public GenerateTxtOutput() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPatient_Cedula() {
        return patient_Cedula;
    }

    public void setPatient_Cedula(String patient_Cedula) {
        this.patient_Cedula = patient_Cedula;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getEmailRecipient() {
        return emailRecipient;
    }

    public void setEmailRecipient(String emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public String getEntireAmount() {
        return entireAmount;
    }

    public void setEntireAmount(String entireAmount) {
        this.entireAmount = entireAmount;
    }

    public String getDecimalAmount() {
        return decimalAmount;
    }

    public void setDecimalAmount(String decimalAmount) {
        this.decimalAmount = decimalAmount;
    }
    
    
    
}
