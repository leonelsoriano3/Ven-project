/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultKeysPatientsInput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsDAO extends AbstractQueryDAO<ConsultKeysPatientsInput, ConsultKeysPatientsOutput>{
    
     public ConsultKeysPatientsDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_GestionarClaves", 6);
    }
    @Override
    public void prepareInput(ConsultKeysPatientsInput input, CallableStatement statement) throws SQLException {
        if(input.getDateStart()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setString(1,input.getDateStart()); 
        }
         if(input.getDateEnd()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setString(2,input.getDateEnd()); 
        }
          if(input.getNumberForKey()==null){
            statement.setNull(3,java.sql.Types.NULL);
        }else{
            statement.setString(3, input.getNumberForKey());  
        }
           if(input.getPatient()==null){
            statement.setNull(4,java.sql.Types.NULL);
        }else{
           statement.setString(4, input.getPatient()); 
        }
        
           if(input.getSpecialist()==null){
            statement.setNull(5,java.sql.Types.NULL);
        }else{
           statement.setString(5, input.getSpecialist()); 
        }
        
         if(input.getIdstatusKey()==null){
            statement.setNull(6,java.sql.Types.NULL);
        }else if (input.getIdstatusKey()==-1){
            statement.setNull(6,java.sql.Types.NULL);
        }else{
            
           statement.setInt(6, input.getIdstatusKey()); 
        }
         
    }

    @Override
    public ConsultKeysPatientsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultKeysPatientsOutput consultKeysPatientsOutput = new ConsultKeysPatientsOutput();
        consultKeysPatientsOutput.setIdPatient(rs.getInt("ID_PACIENTE"));
        consultKeysPatientsOutput.setIdKey(rs.getInt("ID_CLAVE"));
        consultKeysPatientsOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
        consultKeysPatientsOutput.setNumberForKey(rs.getString("NUMERO_CLAVE"));
        consultKeysPatientsOutput.setDate(rs.getDate("FECHA_CLAVE"));
        consultKeysPatientsOutput.setNumberForIdentifyPatient(rs.getInt("CEDULA_PACIENTE"));
        consultKeysPatientsOutput.setIdStatusKey(rs.getInt("ESTATUS_CLAVE"));
        consultKeysPatientsOutput.setIdSpecialist(rs.getInt("ID_ESPECIALISTA"));
        consultKeysPatientsOutput.setIdStatusKey(rs.getInt("ID_ESTATUS"));
        consultKeysPatientsOutput.setNameSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
        consultKeysPatientsOutput.setNumberForIdentifySpecialist(rs.getInt("CEDULA_ESPECIALISTA"));
        consultKeysPatientsOutput.setNameStatusKey(rs.getString("NOMBRE_ESTATUS"));
        consultKeysPatientsOutput.setObservationKey(rs.getString("OBSERVACION_CLAVE"));
        return consultKeysPatientsOutput;
    }
    
}
