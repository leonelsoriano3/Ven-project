/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultDateMinDetalleOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultDateMinDetalleDAO extends AbstractQueryDAO<Integer, ConsultDateMinDetalleOutput>{
    
     public ConsultDateMinDetalleDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_MinimaFechaClaveDetalle", 1);
    }
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
         
    }

    @Override
    public ConsultDateMinDetalleOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultDateMinDetalleOutput consultDateMinDetalleOutput = new ConsultDateMinDetalleOutput();
        consultDateMinDetalleOutput.setDateMinTreatment(rs.getDate("FECHA_TRATAMIENTO"));
        return consultDateMinDetalleOutput;
    }
    
}
