/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

/**
 *
 * @author akdesk01
 */
public class GenerateTxtOutput {

    private String numero_cuenta,referencia_pago,monto_pagar_entero,monto_pagar_decimal;
    private String cedula_especialista,nombre_especialista,email_especialista;
    
    

    public GenerateTxtOutput() {
    }
    
    public GenerateTxtOutput(String numero_cuenta,String cedula_especialista, String nombre_especialista, String email_especialista,
            String monto_pagar_entero, String referencia_pago, String monto_pagar_decimal) {
      this.numero_cuenta=numero_cuenta;
      this.cedula_especialista=cedula_especialista;
      this.nombre_especialista=nombre_especialista;
      this.email_especialista=email_especialista;
      this.monto_pagar_entero=monto_pagar_entero;
      this.referencia_pago=referencia_pago;
      this.monto_pagar_decimal=monto_pagar_decimal;
    }

    public String getNumero_cuenta() {
        return numero_cuenta;
    }

    public void setNumero_cuenta(String numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }

    public String getCedula_especialista() {
        return cedula_especialista;
    }

    public void setCedula_especialista(String cedula_especialista) {
        this.cedula_especialista = cedula_especialista;
    }

    public String getNombre_especialista() {
        return nombre_especialista;
    }

    public void setNombre_especialista(String nombre_especialista) {
        this.nombre_especialista = nombre_especialista;
    }

    public String getEmail_especialista() {
        return email_especialista;
    }

    public void setEmail_especialista(String email_especialista) {
        this.email_especialista = email_especialista;
    }

    public String getMonto_pagar_entero() {
        return monto_pagar_entero;
    }

    public void setMonto_pagar_entero(String monto_pagar_entero) {
        this.monto_pagar_entero = monto_pagar_entero;
    }

    public String getReferencia_pago() {
        return referencia_pago;
    }

    public void setReferencia_pago(String referencia_pago) {
        this.referencia_pago = referencia_pago;
    }

    public String getMonto_pagar_decimal() {
        return monto_pagar_decimal;
    }

    public void setMonto_pagar_decimal(String monto_pagar_decimal) {
        this.monto_pagar_decimal = monto_pagar_decimal;
    }
    
    
}
