/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatments;

/**
 *
 * @author akdeskdev90
 */
public class TreatmentsAvailableInput {

    private Integer idPatient;
    private Integer idPlan;
    private Integer idSpecialist;
    private Integer typeAttention;

    public TreatmentsAvailableInput() {
    }



    public TreatmentsAvailableInput(Integer idPatient, Integer idPlan, Integer idSpecialist, Integer typeAttention) {
        this.idPatient = idPatient;
        this.idPlan = idPlan;
        this.idSpecialist = idSpecialist;
        this.typeAttention = typeAttention;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getTypeAttention() {
        return typeAttention;
    }

    public void setTypeAttention(Integer typeAttention) {
        this.typeAttention = typeAttention;
    }

}