/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Reimbursement;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author AKDESK12
 */
@Stateless
public class ReimbursementFacade extends AbstractFacade<Reimbursement> implements ReimbursementFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReimbursementFacade() {
        super(Reimbursement.class);
    }

 @Override
    public List<Reimbursement> findByStatus(int status) {
       
        TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findAllByStatus", Reimbursement.class);
        query.setParameter("statusReimbursmentId", status);
        return query.getResultList();

}

    @Override
    public List<Reimbursement> findByTwoStatus(int idStatus1, int idStatus2) {
        
        TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findByTwoStatus", Reimbursement.class);
        query.setParameter("statusReimbursmentId", idStatus1);
        query.setParameter("statusReimbursmentIdTwo", idStatus2);
        return query.getResultList();
        
    }

    @Override
    public List<Reimbursement> findByThreeStatus(int idStatus1, int idStatus2, int idStatus3) {
        TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findByThreeStatus", Reimbursement.class);
        query.setParameter("statusReimbursmentId", idStatus1);
        query.setParameter("statusReimbursmentIdTwo", idStatus2);
        query.setParameter("statusReimbursmentIdThree", idStatus3);
        return query.getResultList();
    }
    
    @Override
    public List<Reimbursement> findByNumberRequest(String numberRequest) {
        
        TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findByNumberRequest", Reimbursement.class);
        query.setParameter("numberRequest", numberRequest);
        return query.getResultList();
        
    }
    
    @Override
    public String generateRequestNumber(Date date) {
       try {
           StoredProcedureQuery query = em.createStoredProcedureQuery("SP_NUMERO_REEMBOLSO");
           query.registerStoredProcedureParameter("PI_FECHA", Date.class, ParameterMode.IN);
           query.registerStoredProcedureParameter("PS_NUMERO", String.class, ParameterMode.OUT);
           query.setParameter("PI_FECHA", date);
           query.execute();
           return (String) query.getOutputParameterValue("PS_NUMERO");
       } catch (NoResultException exception) {
           return null;
       }
   }

    @Override
    public List<Reimbursement> findByStatusTransfer(int status) {
    TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findByStatusTransfer", Reimbursement.class);
        query.setParameter("statusReimbursmentId", status);
        return query.getResultList();
    }

    @Override
    public List<Reimbursement> searchReimbursementReportsbyStatus(Integer insuranceId, int statusId, Date startDate, Date endDate) {
       
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_REIMBURSEMEN_G_002", Reimbursement.class);
        query.registerStoredProcedureParameter("PI_ID_ASEGURADORA", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS", Integer.class, ParameterMode.IN);        
        query.registerStoredProcedureParameter("PI_FECHA_INICIO", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_FIN", Date.class, ParameterMode.IN);
        query.setParameter("PI_ID_ASEGURADORA", insuranceId);
        query.setParameter("PI_ID_ESTATUS", statusId);
        query.setParameter("PI_FECHA_INICIO", startDate);
        query.setParameter("PI_FECHA_FIN", endDate);
        
        return query.getResultList();
    }
    
    @Override
    public List<Reimbursement> findByPatientsId(Integer patientsId){
        TypedQuery<Reimbursement> query = em.createNamedQuery("Reimbursement.findByPatientsId", Reimbursement.class);
        query.setParameter("patientsId", patientsId);
        return query.getResultList();
    }
    
    @Override
    public boolean patientHasReimbursement(Integer idPatient, Integer idPlan) {
       try {
           StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_REIMBURSEMEN_G_003");
           query.registerStoredProcedureParameter("PI_ID_PACIENTE", Integer.class, ParameterMode.IN);
           query.registerStoredProcedureParameter("PI_ID_PLAN", Integer.class, ParameterMode.IN);
           query.registerStoredProcedureParameter("PO_TIENE_REEMBOLSO", Boolean.class, ParameterMode.OUT);
           query.setParameter("PI_ID_PACIENTE", idPatient);
           query.setParameter("PI_ID_PLAN", idPlan);
           query.execute();
           return (Boolean) query.getOutputParameterValue("PO_TIENE_REEMBOLSO");                   
       } catch (NoResultException exception) {
           return true;
       }
   }
    
    
}
  
