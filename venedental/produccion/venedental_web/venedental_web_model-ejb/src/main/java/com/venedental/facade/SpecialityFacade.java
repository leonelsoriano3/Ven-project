/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.SpecialistJob;
import com.venedental.model.Speciality;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class SpecialityFacade extends AbstractFacade<Speciality> implements SpecialityFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SpecialityFacade() {
        super(Speciality.class);
    }

    
}
