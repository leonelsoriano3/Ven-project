/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansTreatments;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PlansTreatmentsFacadeLocal {

    PlansTreatments create(PlansTreatments patients);

    PlansTreatments edit(PlansTreatments patients);

    void remove(PlansTreatments patients);

    PlansTreatments find(Object id);

    List<PlansTreatments> findAll();

    List<PlansTreatments> findRange(int[] range);
    
    List<PlansTreatments> findByPlansId(Integer id);

    int count();
    
    List<PlansTreatments> findByPlansAndPlansTratmentsKeys(Integer PI_ID_PLAN, Integer PI_ID_CLAVE);
    
    
}
