/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.planTreatmentKey.UpdatePlanTreatmentKeyObservationInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class UpdatePlanTreatKeyObservationDAO extends AbstractCommandDAO<UpdatePlanTreatmentKeyObservationInput> {

    public UpdatePlanTreatKeyObservationDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_U_002", 2);
    }

    @Override
    public void prepareInput(UpdatePlanTreatmentKeyObservationInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPlanTreatmentKey());
        if (input.getObservation() != null) {
            statement.setString(2, input.getObservation());
        } else {
             statement.setNull(2, java.sql.Types.NULL);
        }
    }
}
