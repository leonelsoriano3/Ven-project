/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.planTreatment.FindTreatmentsAvailableEditDAO;
import com.venedental.dao.planTreatment.FindTreatmentsAvailableRegisterDAO;
import com.venedental.dao.propertiesTreatments.FindProTreByKeyAndTreatmentsDAO;
import com.venedental.dao.propertiesTreatments.FindProTreByTreatmentsAndQuaDAO;
import com.venedental.dao.propertiesTreatments.FindProTreByTreatmentsDAO;
import com.venedental.dto.planTreatments.FindPtByKeyAndPlanInput;
import com.venedental.dto.planTreatments.TreatmentsAvailableInput;
import com.venedental.dto.planTreatments.TreatmentsAvailableOutput;
import com.venedental.dto.properties.FindProTreByTreatmentsInput;
import com.venedental.dto.properties.FindProtByKeyAndTreatmensAndQuaInput;
import com.venedental.dto.properties.FindProtByKeyAndTreatmenstInput;
import com.venedental.model.PropertiesTreatments;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServicePropertiesTreatments {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
    
     /**
     * Servicio que permite buscar las piezas dentales de un tratamiento,
     * diponibles para registraselas a la clave
     *
     * @param idTreatments
     * @param idKeyPatient
     * @return
     */
    public List<PropertiesTreatments> findProTreatByKeyAndTreatments(Integer idTreatments, Integer idKeyPatient) {
        FindProTreByKeyAndTreatmentsDAO findProtkByKeyAndTreatmentsDAO = new FindProTreByKeyAndTreatmentsDAO(ds);
        FindProtByKeyAndTreatmenstInput input = new FindProtByKeyAndTreatmenstInput(idTreatments, idKeyPatient);
        findProtkByKeyAndTreatmentsDAO.execute(input);
        return findProtkByKeyAndTreatmentsDAO.getResultList();
    }



    /**
     * Servicio que permite buscar los tratamientos dispnibles para un paciente al crear clave
     *
     * @param idPatient
     * @param idPlan
     * @param idSpecialist
     * @param typeAttention
     * @return
     */
    public List<TreatmentsAvailableOutput> findTreatmentsAvailableRegister(Integer idPatient, Integer idPlan,
                                                                           Integer idSpecialist, Integer typeAttention) {
        FindTreatmentsAvailableRegisterDAO findTreatmentsAvailableByPatientDAO = new FindTreatmentsAvailableRegisterDAO(ds);
        TreatmentsAvailableInput input = new TreatmentsAvailableInput(idPatient, idPlan, idSpecialist, typeAttention);
        findTreatmentsAvailableByPatientDAO.execute(input);
        return findTreatmentsAvailableByPatientDAO.getResultList();
    }

    /**
     * Servicio que permite buscar los tratamientos disponibles por clave en la edici�n
     *
     * @param idKeyPatient
     * @param idPlan
     * @return
     */
    public List<TreatmentsAvailableOutput> findTreatmentsAvailableEdit(Integer idKeyPatient, Integer idPlan) {
        FindTreatmentsAvailableEditDAO findTreatmentsAvailableByKeyPatientDAO = new FindTreatmentsAvailableEditDAO(ds);
        FindPtByKeyAndPlanInput input = new FindPtByKeyAndPlanInput(idKeyPatient, idPlan);
        findTreatmentsAvailableByKeyPatientDAO.execute(input);
        return findTreatmentsAvailableByKeyPatientDAO.getResultList();
    }


    /**
     * Servicio que permite buscar las piezas dentales de un tratamiento,
     * diponibles para registraselas a la clave en la creaci�n y la edici�n
     *
     * @param idTreatments
     * @param quadrant
     * @param idSpecialist
     * @return
     */
    public List<PropertiesTreatments> findProTreatByTreatments(Integer idTreatments, Integer quadrant, Integer idSpecialist) {
        FindProTreByTreatmentsDAO findProTreByTreatmentsDAO = new FindProTreByTreatmentsDAO(ds);
        FindProTreByTreatmentsInput findProTreByTreatmentsInput =
                new FindProTreByTreatmentsInput(idTreatments, null, quadrant, idSpecialist);
        findProTreByTreatmentsDAO.execute(findProTreByTreatmentsInput);
        return findProTreByTreatmentsDAO.getResultList();
    }

    public List<PropertiesTreatments> findProTreatByTreatmentsAndQua(Integer idKeysPatients,Integer idTreatments, Integer quadrant) {
        FindProTreByTreatmentsAndQuaDAO findProTreByTreatmentsAndQuaDAO = new FindProTreByTreatmentsAndQuaDAO(ds);
        FindProtByKeyAndTreatmensAndQuaInput findProtByKeyAndTreatmensAndQuaInput =
                new FindProtByKeyAndTreatmensAndQuaInput(idKeysPatients,idTreatments,quadrant);
        findProTreByTreatmentsAndQuaDAO.execute(findProtByKeyAndTreatmensAndQuaInput);
        return findProTreByTreatmentsAndQuaDAO.getResultList();
    }
}
