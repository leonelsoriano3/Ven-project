package com.venedental.dao.createUser;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.createUser.UpdatePasswordDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 10/05/16.
 */
public class UpdatePAsswordDao extends AbstractCommandDAO<UpdatePasswordDTO> {


    public UpdatePAsswordDao(DataSource dataSource){
        super(dataSource,"SP_T_USERS_U_001",3);
    }

    @Override
    public void prepareInput(UpdatePasswordDTO input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdUser());
        statement.setString(2,null);
        statement.setString(3,input.getPassword());
    }
}
