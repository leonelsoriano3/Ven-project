package com.venedental.facade;

import com.venedental.model.BinnacleReimbursement;
import com.venedental.model.Reimbursement;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class BinnacleReimbursementFacade extends AbstractFacade<BinnacleReimbursement> implements BinnacleReimbursementFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BinnacleReimbursementFacade() {
        super(BinnacleReimbursement.class);
    }

    @Override
    public List<BinnacleReimbursement> findAllByReimbursement(int reimbursement_id) {
       
        TypedQuery<BinnacleReimbursement> query = em.createNamedQuery("BinnacleReimbursement.findAllByReimbursement", BinnacleReimbursement.class);
        query.setParameter("reimbursementId", reimbursement_id);
        
        return query.getResultList();
    }
    
}
