/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 * DTO 
 */
public class SpecialistToAuditInput {    
    
    private Integer statusAnalysisKeysId;
    private Date dateFrom;
    private Date dateUntil;
    
   

    public SpecialistToAuditInput(Integer statusAnalysisKeysId, Date dateFrom, Date dateUntil) {
        this.statusAnalysisKeysId = statusAnalysisKeysId;
        this.dateFrom = dateFrom;
        this.dateUntil = dateUntil;
    }

    public Integer getStatusAnalysisKeysId() {
        return statusAnalysisKeysId;
    }

    public void setStatusAnalysisKeysId(Integer statusAnalysisKeysId) {
        this.statusAnalysisKeysId = statusAnalysisKeysId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(Date dateUntil) {
        this.dateUntil = dateUntil;
    }
 
    
    
}
