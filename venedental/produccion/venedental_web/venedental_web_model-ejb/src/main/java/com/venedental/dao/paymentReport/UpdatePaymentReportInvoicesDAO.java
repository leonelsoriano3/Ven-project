/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.paymentReport.UpdatePaymentReportInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class UpdatePaymentReportInvoicesDAO extends AbstractCommandDAO<UpdatePaymentReportInput> {

    public UpdatePaymentReportInvoicesDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_U_001", 4);
    }

    @Override
    public void prepareInput(UpdatePaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPaymentReport());
        statement.setDate(2,input.getDate());
        statement.setString(3, input.getBillNumber());
        
        if(input.getControlNumber()!=null){
           statement.setString(4, input.getControlNumber());
        }else{
            statement.setNull(4, java.sql.Types.NULL);
        }
               
    }
}
