/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.SpecialistJob;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class SpecialistJobFacade extends AbstractFacade<SpecialistJob> implements SpecialistJobFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SpecialistJobFacade() {
        super(SpecialistJob.class);
    }
    
    @Override
    public void deleteJobsDependency(Integer id) {
        TypedQuery<SpecialistJob> query = em.createNamedQuery("SpecialistJob.deleteJobsDependency", SpecialistJob.class);
        query.setParameter("specialist_id", id);        
        query.executeUpdate();  
        em.flush();
        em.clear();
    }

    @Override
    public List<SpecialistJob> findBySpecialist(Integer id) {
       TypedQuery<SpecialistJob> query = em.createNamedQuery("SpecialistJob.findBySpecialist", SpecialistJob.class);  
       query.setParameter("specialist_id", id);
       return  query.getResultList();
    
    }
    
}
