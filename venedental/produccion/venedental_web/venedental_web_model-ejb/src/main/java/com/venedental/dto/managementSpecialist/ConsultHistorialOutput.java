/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.sql.Date;
import java.util.logging.Logger;

/**
 *
 * @author akdesk01
 */
public class ConsultHistorialOutput {
   private String dateStartFilter;
   private String dateEndFilter;
   private Integer idPatient;
   private String namePatient;
   private Integer idKeyPatient;
   private Integer idPlanTreatmentKey;
   private Integer idTreatment;
   private String nameTratment;
   private Integer idPropertiePlantreatmentKey;
   private String numberPropertie;
   private Integer idStatus;
   private String observation;
   private String nameStatus;
   private java.util.Date dateApplication;
   private String numberKey;
   private String company;
   private String specialist;
   private String relationShip;
   private String insurance;
   private String plan;
   private Integer idSpecialist;
   private String dateForHistorial;
   private Date dateMinFilter;
   private Date dateMaxFilter;


    public ConsultHistorialOutput() {
    }

    public String getDateStartFilter() {
        return dateStartFilter;
    }

    public void setDateStartFilter(String dateStartFilter) {
        this.dateStartFilter = dateStartFilter;
    }

    public String getDateEndFilter() {
        return dateEndFilter;
    }

    public void setDateEndFilter(String dateEndFilter) {
        this.dateEndFilter = dateEndFilter;
    }

   

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTratment() {
        return nameTratment;
    }

    public void setNameTratment(String nameTratment) {
        this.nameTratment = nameTratment;
    }

    public Integer getIdPropertiePlantreatmentKey() {
        return idPropertiePlantreatmentKey;
    }

    public void setIdPropertiePlantreatmentKey(Integer idPropertiePlantreatmentKey) {
        this.idPropertiePlantreatmentKey = idPropertiePlantreatmentKey;
    }

    public String getNumberPropertie() {
        return numberPropertie;
    }

    public void setNumberPropertie(String numberPropertie) {
        this.numberPropertie = numberPropertie;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public java.util.Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(java.util.Date dateApplication) {
        this.dateApplication = dateApplication;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getRelationShip() {
        return relationShip;
    }

    public void setRelationShip(String relationShip) {
        this.relationShip = relationShip;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getDateForHistorial() {
        return dateForHistorial;
    }

    public void setDateForHistorial(String dateForHistorial) {
        this.dateForHistorial = dateForHistorial;
    }

    public Date getDateMinFilter() {
        return dateMinFilter;
    }

    public void setDateMinFilter(Date dateMinFilter) {
        this.dateMinFilter = dateMinFilter;
    }

    public Date getDateMaxFilter() {
        return dateMaxFilter;
    }

    public void setDateMaxFilter(Date dateMaxFilter) {
        this.dateMaxFilter = dateMaxFilter;
    }
    
    
    
    
    
    
    
    
}
