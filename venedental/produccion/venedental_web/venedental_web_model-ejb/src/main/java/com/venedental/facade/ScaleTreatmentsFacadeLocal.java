/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.ScaleTreatments;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ScaleTreatmentsFacadeLocal {

    ScaleTreatments create(ScaleTreatments scale);

   ScaleTreatments edit(ScaleTreatments scale);

    void remove(ScaleTreatments scale);

    ScaleTreatments find(Object id);

    List<ScaleTreatments> findAll();
    
    List<ScaleTreatments> findByTypeScale(Integer id);

    List<ScaleTreatments> findRange(int[] range);
    
    ScaleTreatments findByTreatmentsAndSpecialist(Integer idTreatments,Integer idSpecialist);
    
    List<ScaleTreatments> findBySpecialist(Integer idSpecialist);
    
    ScaleTreatments findByTreatments(Integer idTreatments);

    int count();
    
    List<ScaleTreatments> findByTypeSpeciality(int idTypeScale, int idTypeSpeciality);  
    
     public ScaleTreatments findByTreatmentSpecialistAndDate(Integer idTreatment, Integer specialistId, Date dateTreatment);
     
     public ScaleTreatments findByTreatmentAndDate(Integer idTreatment, Date dateTreatment);
}
