/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.InsurancesPatients;
import com.venedental.model.ScaleTreatments;
import com.venedental.model.ScaleTreatmentsSpecialist;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class ScaleTreatmentsSpecialitsFacade extends AbstractFacade<ScaleTreatmentsSpecialist> implements ScaleTreatmentsSpecialistFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScaleTreatmentsSpecialitsFacade() {
        super(ScaleTreatmentsSpecialist.class);
    }

    @Override
    public ScaleTreatmentsSpecialist findByScale(Integer idScale, Integer idSpecialist) {
        try {
         TypedQuery<ScaleTreatmentsSpecialist> query = em.createNamedQuery("ScaleTreatmentsSpecialist.findByScale",ScaleTreatmentsSpecialist.class);  
         query.setParameter("id_scale_treatments",idScale);
         query.setParameter("id_specialist",idSpecialist);
         return  query.getSingleResult();
         } catch (NoResultException exception) {
            return null;
        }
    
    
    }
    
    @Override
    public List<ScaleTreatmentsSpecialist> findBySpecialist(Integer PI_ID_ESPECIALISTA) {
        try {
            String sql = "SELECT DISTINCT * FROM scale_treatments str inner join scale_treatments_specialist stsp on stsp.id_scale_treatments = str.id"
                    + " inner join treatments tre on str.id_treatments = tre.id"
                    + " inner join (SELECT str1.id_treatments id_tratamiento, stsp1.id_specialist id_especialista, max(str1.date_scale) fecha_baremo"
                    + " FROM scale_treatments str1 inner join scale_treatments_specialist stsp1 on stsp1.id_scale_treatments = str1.id"
                    + " WHERE str1.id_type_scale = 2 and stsp1.id_specialist = " + PI_ID_ESPECIALISTA + ""
                    + " GROUP BY str1.id_treatments, stsp1.id_specialist) ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.id_especialista = stsp.id_specialist and ultimo_baremo.fecha_baremo = str.date_scale"
                    + " WHERE (stsp.id_specialist = " + PI_ID_ESPECIALISTA + " AND str.id_type_scale = 2)ORDER BY tre.name";

            Query query = em.createNativeQuery(sql, ScaleTreatmentsSpecialist.class);
            List<ScaleTreatmentsSpecialist> scaleTreatmentsSpecialistList = new ArrayList<ScaleTreatmentsSpecialist>(query.getResultList());
            return scaleTreatmentsSpecialistList;
        } catch (NoResultException exception) {
            return null;
        }
    }

}
