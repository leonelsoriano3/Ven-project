/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.properties.FindProtByKeyAndTreatmensAndQuaInput;
import com.venedental.model.PropertiesTreatments;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProTreByTreatmentsAndQuaDAO extends AbstractQueryDAO<FindProtByKeyAndTreatmensAndQuaInput, PropertiesTreatments> {

    public FindProTreByTreatmentsAndQuaDAO(DataSource dataSource) {
        super(dataSource, " SP_T_PROPERTREATM_G_004",3);
    }

    @Override
    public void prepareInput(FindProtByKeyAndTreatmensAndQuaInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKeyPatient());
        statement.setInt(2, input.getIdTreatments());

        if (input.getIdQuadrant() != null) {
            statement.setInt(3, input.getIdQuadrant());
        } else {
            statement.setNull(3, java.sql.Types.NULL);
        }

    }

    @Override
    public PropertiesTreatments prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        propertiesTreatments.setId(rs.getInt("ID_PIEZA_TRATAMIENTO"));
        propertiesTreatments.setIdPropertie(rs.getInt("ID_PIEZA"));
        propertiesTreatments.setPropertieQuadrant(rs.getInt("CUADRANTE_PIEZA"));
        propertiesTreatments.setPropertieStringValue(rs.getString("NOMBRE_PIEZA"));
        propertiesTreatments.setIdScale(rs.getInt("ID_BAREMO"));
        propertiesTreatments.setPrice(rs.getDouble("PRECIO_BAREMO"));
        return propertiesTreatments;
    }

}