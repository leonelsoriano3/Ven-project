/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.consultKeys;

import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;

import java.util.Date;
import java.util.List;


/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsDetailTreatmentOutput {
    private String idKey, idTreatment,nameTreatment,idPieceTreatment,idPieceKey,idPiece,NumberPiece,
            idPriceTreatment, idPieceTreatmentKey, dateFilterTreatment, baremoTreatment;
    private Date dateForTreatment,dateForTreatmentUtil;

    private List<ConsultNameFileOutput> LConsultNameFileOutputList;

    private Integer btnDownload0,btnDownload1,btnDownload2,btnDownload3,btnDownload4;

    private String valueDefault;

    public String getValueDefault() {
        return valueDefault;
    }

    public void setValueDefault(String valueDefault) {
        this.valueDefault = valueDefault;
    }

    public ConsultKeysPatientsDetailTreatmentOutput() {
    }

    public String getBaremoTreatment() {return baremoTreatment;}

    public void setBaremoTreatment(String baremoTreatment) {this.baremoTreatment = baremoTreatment;}

    public String getIdKey() {
        return idKey;
    }

    public void setIdKey(String idKey) {
        this.idKey = idKey;
    }

    public String getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(String idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getIdPieceTreatment() {
        return idPieceTreatment;
    }

    public void setIdPieceTreatment(String idPieceTreatment) {
        this.idPieceTreatment = idPieceTreatment;
    }

    public String getIdPieceKey() {
        return idPieceKey;
    }

    public void setIdPieceKey(String idPieceKey) {
        this.idPieceKey = idPieceKey;
    }

    public String getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(String idPiece) {
        this.idPiece = idPiece;
    }

    public String getNumberPiece() {
        return NumberPiece;
    }

    public void setNumberPiece(String NumberPiece) {
        this.NumberPiece = NumberPiece;
    }

    public String getIdPriceTreatment() {
        return idPriceTreatment;
    }

    public void setIdPriceTreatment(String idPriceTreatment) {
        this.idPriceTreatment = idPriceTreatment;
    }

    public String getIdPieceTreatmentKey() {
        return idPieceTreatmentKey;
    }

    public void setIdPieceTreatmentKey(String idPieceTreatmentKey) {
        idPieceTreatmentKey = idPieceTreatmentKey.replace(".", ",");
        this.idPieceTreatmentKey = idPieceTreatmentKey;
    }

    public Date getDateForTreatment() {
        return dateForTreatment;
    }

    public void setDateForTreatment(Date dateForTreatment) {
        this.dateForTreatment = dateForTreatment;
    }

    public Date getDateForTreatmentUtil() {
        return dateForTreatmentUtil;
    }

    public void setDateForTreatmentUtil(Date dateForTreatmentUtil) {
        this.dateForTreatmentUtil = dateForTreatmentUtil;
    }

    public String getDateFilterTreatment() {
        return dateFilterTreatment;
    }

    public void setDateFilterTreatment(String dateFilterTreatment) {
        this.dateFilterTreatment = dateFilterTreatment;
    }

    public List<ConsultNameFileOutput> getLConsultNameFileOutputList() {
        return LConsultNameFileOutputList;
    }

    public void setLConsultNameFileOutputList(List<ConsultNameFileOutput> LConsultNameFileOutputList) {
        this.LConsultNameFileOutputList = LConsultNameFileOutputList;
    }

    public Integer getBtnDownload0() {
        return btnDownload0;
    }

    public void setBtnDownload0(Integer btnDownload0) {
        this.btnDownload0 = btnDownload0;
    }

    public Integer getBtnDownload4() {
        return btnDownload4;
    }

    public void setBtnDownload4(Integer btnDownload4) {
        this.btnDownload4 = btnDownload4;
    }

    public Integer getBtnDownload3() {
        return btnDownload3;
    }

    public void setBtnDownload3(Integer btnDownload3) {
        this.btnDownload3 = btnDownload3;
    }

    public Integer getBtnDownload2() {
        return btnDownload2;
    }

    public void setBtnDownload2(Integer btnDownload2) {
        this.btnDownload2 = btnDownload2;
    }

    public Integer getBtnDownload1() {
        return btnDownload1;
    }

    public void setBtnDownload1(Integer btnDownload1) {
        this.btnDownload1 = btnDownload1;
    }
}
