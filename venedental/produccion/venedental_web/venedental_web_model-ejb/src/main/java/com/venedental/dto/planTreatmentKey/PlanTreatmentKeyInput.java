/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class PlanTreatmentKeyInput {
    
    Integer idKey;
    Integer idPlanTreatment;
    Date dateApplication;
    Integer idStatus;
    Integer idScaleTreatments;
    String observation;
    Integer idUser;
    Date dateReception;

    public PlanTreatmentKeyInput(Integer idKey, Integer idPlanTreatment, Date dateApplication, Integer idStatus, Integer idScaleTreatments, String observation, Integer idUser, Date dateReception) {
        this.idKey = idKey;
        this.idPlanTreatment = idPlanTreatment;
        this.dateApplication = dateApplication;
        this.idStatus = idStatus;
        this.idScaleTreatments = idScaleTreatments;
        this.observation = observation;
        this.idUser = idUser;
        this.dateReception = dateReception;
    }
    
    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(Date dateApplication) {
        this.dateApplication = dateApplication;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdScaleTreatments() {
        return idScaleTreatments;
    }

    public void setIdScaleTreatments(Integer idScaleTreatments) {
        this.idScaleTreatments = idScaleTreatments;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

   
}
