/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsInput;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.paymentReport.ConfirmKeyDetailsInvoicesTransferOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;


/**
 *
 * @author usuario
 */
public class ConfirmKeyDetailsInvoicesTransferDAO extends AbstractQueryDAO<ConfirmKeyDetailsInput, ConfirmKeyDetailsInvoicesTransferOutput> {

    public ConfirmKeyDetailsInvoicesTransferDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_004",5);
    }

    @Override
    public void prepareInput(ConfirmKeyDetailsInput input, CallableStatement statement) throws SQLException {
        //double monto_total = 0;
        statement.setInt(1, input.getIdPaymentReport());
        statement.setInt(2, input.getIdStatus());
        statement.setDate(3, (Date) input.getDateFrom());
        statement.setDate(4, (Date) input.getDateUtil());
        statement.registerOutParameter(5,java.sql.Types.DOUBLE);
       // statement.setDouble( 5, monto_total);
    }

    @Override
    public ConfirmKeyDetailsInvoicesTransferOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConfirmKeyDetailsInvoicesTransferOutput confirmKeyDetailsInvoicesTransferOutput = new ConfirmKeyDetailsInvoicesTransferOutput();
        confirmKeyDetailsInvoicesTransferOutput.setIdKeyPatient(rs.getInt("id_clave")); //
        confirmKeyDetailsInvoicesTransferOutput.setIdPropertie(rs.getInt("id_pieza")); //
        confirmKeyDetailsInvoicesTransferOutput.setIdSpecialist(rs.getInt("id_especialista")); //
        confirmKeyDetailsInvoicesTransferOutput.setIdTreatment(rs.getInt("id_tratamiento")); //
        confirmKeyDetailsInvoicesTransferOutput.setNameKeyPatient(rs.getString("numero_clave")); //
        confirmKeyDetailsInvoicesTransferOutput.setNameMonth(rs.getString("nombre_mes")); //
        
        if(rs.getString("nombre_pieza").isEmpty()){
            confirmKeyDetailsInvoicesTransferOutput.setNamePropertie("-");
        }else{
            confirmKeyDetailsInvoicesTransferOutput.setNamePropertie(rs.getString("nombre_pieza")); 
        }        
        confirmKeyDetailsInvoicesTransferOutput.setNameSpecialist(rs.getString("nombre_especialista")); //
        confirmKeyDetailsInvoicesTransferOutput.setNameTreatment(rs.getString("nombre_tratamiento")); //
        confirmKeyDetailsInvoicesTransferOutput.setAmountTotal(rs.getDouble("monto_pagar")); //
        //confirmKeyDetailsInvoicesTransferOutput.setAmountTotal(rs.getDouble("PO_MONTO_TOTAL")); 
        
        if(rs.getString("observaciones").isEmpty()){
           confirmKeyDetailsInvoicesTransferOutput.setObservations("-"); // 
        }else{
           confirmKeyDetailsInvoicesTransferOutput.setObservations(rs.getString("observaciones")); // 
        }
        
         double monto_total = statement.getDouble(5);
        confirmKeyDetailsInvoicesTransferOutput.setTotalAmountPaid(monto_total);
        return confirmKeyDetailsInvoicesTransferOutput;
    }

}
