/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

/**
 *
 * @author root
 */
public class CreatePaymentReportPropertiesInput {
    
   Integer propertiesPlansTreatmentsKeysId;
   Integer paymentReportId;

    public CreatePaymentReportPropertiesInput() {
    }

    public CreatePaymentReportPropertiesInput(Integer propertiesPlansTreatmentsKeysId, Integer paymentReportId) {
        this.propertiesPlansTreatmentsKeysId = propertiesPlansTreatmentsKeysId;
        this.paymentReportId = paymentReportId;
    }
 
    public Integer getPaymentReportId() {
        return paymentReportId;
    }

    public void setPaymentReportId(Integer paymentReportId) {
        this.paymentReportId = paymentReportId;
    }

    

    public Integer getPropertiesPlansTreatmentsKeysId() {
        return propertiesPlansTreatmentsKeysId;
    }

    public void setPropertiesPlansTreatmentsKeysId(Integer propertiesPlansTreatmentsKeysId) {
        this.propertiesPlansTreatmentsKeysId = propertiesPlansTreatmentsKeysId;
    }

   

   
    

   
    
}
