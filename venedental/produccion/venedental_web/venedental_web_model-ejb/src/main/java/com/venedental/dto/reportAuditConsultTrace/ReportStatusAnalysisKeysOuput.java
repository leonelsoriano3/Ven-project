/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.reportAuditConsultTrace;

import java.util.Date;

/**
 *
 * @author AKDESK21
 */
public class ReportStatusAnalysisKeysOuput {
    
     private Integer numReportTreatment;

    private Integer idPaymentReport;

    private String numPaymentReport;

    private Integer idKey;

    private String numKey;

    private String treatmentName;

    private String numTooth;

    private String statusName;

    private String isVisible;
    
    private Integer idSpecialist;
    
    private String specialist;
    
    private String numReportTreatmentS;
    
    private boolean btnVisibilityReportTreatment = true;
    
    private boolean btnVisibilityPaymentReport = true;
    
    private boolean btnEnableDetail = false;
    
   // private Date DateMin;
    

    public ReportStatusAnalysisKeysOuput() {
    }

    public ReportStatusAnalysisKeysOuput(Integer numReportTreatment, Integer idPaymentReport, String numPaymentReport, Integer idKey, String numKey, String treatmentName, String numTooth, String statusName, String isVisible, Integer idSpecialist, String specialist) {
        this.numReportTreatment = numReportTreatment;
        this.idPaymentReport = idPaymentReport;
        this.numPaymentReport = numPaymentReport;
        this.idKey = idKey;
        this.numKey = numKey;
        this.treatmentName = treatmentName;
        this.numTooth = numTooth;
        this.statusName = statusName;
        
//        this.btnVisibilityPaymentReport = true;
//        this.btnVisibilityReportTreatment =true;
        if(this.idPaymentReport == -1){

            this.isVisible = "false";
        }
        else{

            this.isVisible = "true";

        }
        this.idSpecialist = idSpecialist;
        this.specialist = specialist;
    }

    public Integer getNumReportTreatment() {
        return numReportTreatment;
    }

    public void setNumReportTreatment(Integer numReportTreatment) {
        this.numReportTreatment = numReportTreatment;
    }

    public Integer getIdPaymentReport() {
        return idPaymentReport;
    }

    public void setIdPaymentReport(Integer idPaymentReport) {
        this.idPaymentReport = idPaymentReport;
    }

    public String getNumPaymentReport() {
        return numPaymentReport;
    }

    public void setNumPaymentReport(String numPaymentReport) {
        this.numPaymentReport = numPaymentReport;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public String getNumKey() {
        return numKey;
    }

    public void setNumKey(String numKey) {
        this.numKey = numKey;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getNumTooth() {
        return numTooth;
    }

    public void setNumTooth(String numTooth) {
        this.numTooth = numTooth;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getNumReportTreatmentS() {
        return numReportTreatmentS;
    }

    public void setNumReportTreatmentS(String numReportTreatmentS) {
        this.numReportTreatmentS = numReportTreatmentS;
    }

    public boolean isBtnVisibilityReportTreatment() {
        return btnVisibilityReportTreatment;
    }

    public void setBtnVisibilityReportTreatment(boolean btnVisibilityReportTreatment) {
        this.btnVisibilityReportTreatment = btnVisibilityReportTreatment;
    }

    public boolean isBtnVisibilityPaymentReport() {
        return btnVisibilityPaymentReport;
    }

    public void setBtnVisibilityPaymentReport(boolean btnVisibilityPaymentReport) {
        this.btnVisibilityPaymentReport = btnVisibilityPaymentReport;
    }

    public boolean isBtnEnableDetail() {
        return btnEnableDetail;
    }

    public void setBtnEnableDetail(boolean btnEnableDetail) {
        this.btnEnableDetail = btnEnableDetail;
    }


    
    
    
}
