/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.KeysConsultDateMinOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */


public class KeysConsultDateMinDAO extends AbstractQueryDAO<Integer, KeysConsultDateMinOutput> {
    
    public KeysConsultDateMinDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_011", 1);
    }
    
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        
                statement.setInt(1, input);
      
    }
    
    @Override
    public KeysConsultDateMinOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        KeysConsultDateMinOutput keysConsultDateMinOutput = new KeysConsultDateMinOutput();
        keysConsultDateMinOutput.setDateMin(rs.getDate("FECHA"));
        return keysConsultDateMinOutput;
    }

}



