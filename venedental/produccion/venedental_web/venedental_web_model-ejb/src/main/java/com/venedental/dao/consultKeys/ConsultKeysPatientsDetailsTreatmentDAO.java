/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsDetailsTreatmentDAO extends AbstractQueryDAO<Integer, ConsultKeysPatientsDetailTreatmentOutput>{
    
     public ConsultKeysPatientsDetailsTreatmentDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_ConsultarClaveDetalle", 1);
    }
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
         
    }

    @Override
    public ConsultKeysPatientsDetailTreatmentOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultKeysPatientsDetailTreatmentOutput consultKeysPatientsDetailTreatmentOutput = new ConsultKeysPatientsDetailTreatmentOutput();
        consultKeysPatientsDetailTreatmentOutput.setNameTreatment(rs.getString("NOMBRE_TRATAMIENTO"));
        consultKeysPatientsDetailTreatmentOutput.setNumberPiece(rs.getString("NUMERO_PIEZA"));
        consultKeysPatientsDetailTreatmentOutput.setIdPieceTreatmentKey(rs.getString("ID_PIEZA_TRATAMIENTO_CLAVE"));
        consultKeysPatientsDetailTreatmentOutput.setDateForTreatment(rs.getDate("FECHA_TRATAMIENTO"));
        consultKeysPatientsDetailTreatmentOutput.setBaremoTreatment(rs.getString("BAREMO_TRATAMIENTO"));
        consultKeysPatientsDetailTreatmentOutput.setIdTreatment(rs.getString("ID_TRATAMIENTO"));
        return consultKeysPatientsDetailTreatmentOutput;
    }
    
}
