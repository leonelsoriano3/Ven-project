/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.MenuRolOperation;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Edgar Anzola
 */
@Stateless
public class MenuRolOperationFacade extends AbstractFacade<MenuRolOperation> implements MenuRolOperationFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuRolOperationFacade() {
        super(MenuRolOperation.class);
    }

    @Override
    public MenuRolOperation edit(MenuRolOperation entity) {
        return super.edit(entity);
    }

    @Override
    public MenuRolOperation create(MenuRolOperation entity) {
        return super.create(entity);
    }

    @Override
    public List<MenuRolOperation> findByMenuRolId (int menuRolId) {
        TypedQuery<MenuRolOperation> query = em.createNamedQuery("MenuRolOperation.findByMenuRolId", MenuRolOperation.class);
        query.setParameter("menuRolId", menuRolId);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

}