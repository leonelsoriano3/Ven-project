/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "menu_rol_operation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuRolOperation.findAll", query = "SELECT m FROM MenuRolOperation m"),
    @NamedQuery(name = "MenuRolOperation.findByMenuRolId", query = "SELECT m FROM MenuRolOperation m WHERE m.menuRolOperationPK.menuRolId = :menuRolId"),
    @NamedQuery(name = "MenuRolOperation.findByOperationId", query = "SELECT m FROM MenuRolOperation m WHERE m.menuRolOperationPK.operationId = :operationId"),
    @NamedQuery(name = "MenuRolOperation.findByStatus", query = "SELECT m FROM MenuRolOperation m WHERE m.status = :status")})
public class MenuRolOperation implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected MenuRolOperationPK menuRolOperationPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;
    
    @JoinColumn(name = "menu_rol_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MenuRol menuRol;
    
    @JoinColumn(name = "operation_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Operation operation;

    public MenuRolOperation() {
    }

    public MenuRolOperation(MenuRolOperationPK menuRolOperationPK) {
        this.menuRolOperationPK = menuRolOperationPK;
    }

    public MenuRolOperation(MenuRolOperationPK menuRolOperationPK, Integer status) {
        this.menuRolOperationPK = menuRolOperationPK;
        this.status = status;
    }

    public MenuRolOperation(int menuRolId, int operationId) {
        this.menuRolOperationPK = new MenuRolOperationPK(menuRolId, operationId);
    }

    public MenuRolOperationPK getMenuRolOperationPK() {
        return menuRolOperationPK;
    }

    public void setMenuRolOperationPK(MenuRolOperationPK menuRolOperationPK) {
        this.menuRolOperationPK = menuRolOperationPK;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MenuRol getMenuRol() {
        return menuRol;
    }

    public void setMenuRol(MenuRol menuRol) {
        this.menuRol = menuRol;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuRolOperationPK != null ? menuRolOperationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuRolOperation)) {
            return false;
        }
        MenuRolOperation other = (MenuRolOperation) object;
        if ((this.menuRolOperationPK == null && other.menuRolOperationPK != null) || (this.menuRolOperationPK != null && !this.menuRolOperationPK.equals(other.menuRolOperationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.MenuRolOperation[ menuRolOperationPK=" + menuRolOperationPK + " ]";
    }

}
