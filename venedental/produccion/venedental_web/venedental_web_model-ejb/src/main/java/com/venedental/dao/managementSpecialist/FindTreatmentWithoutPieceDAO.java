package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindTreatmentWithoutPieceOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 15/07/16.
 */
public class FindTreatmentWithoutPieceDAO extends AbstractQueryDAO<Integer, FindTreatmentWithoutPieceOut> {


    public FindTreatmentWithoutPieceDAO(DataSource dataSource) {
        super(dataSource, "SP_T_TREATMENTS_G_007", 1);

    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public FindTreatmentWithoutPieceOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = new FindTreatmentWithoutPieceOut();

        findTreatmentWithoutPieceOut.setIdTreament(rs.getInt("id_tratamiento"));
        findTreatmentWithoutPieceOut.setNameTreatment(rs.getString("nombre_tratamiento"));
        findTreatmentWithoutPieceOut.setIdCategory(rs.getInt("id_categoria"));
        findTreatmentWithoutPieceOut.setNameCategory(rs.getString("nombre_categoria"));
        findTreatmentWithoutPieceOut.setIdPlanTreatment(rs.getInt("id_plan_tratamiento"));
        findTreatmentWithoutPieceOut.setIdBaremo(rs.getInt("id_baremo"));
        findTreatmentWithoutPieceOut.setTreatmentAssigned(rs.getInt("tratamiento_asignado"));
        findTreatmentWithoutPieceOut.setIdTreatmentKey(rs.getInt("id_tratamiento_clave"));
        findTreatmentWithoutPieceOut.setDateTreatment(rs.getDate("fecha_ejecucion_tratamiento"));

        return findTreatmentWithoutPieceOut;
    }


}
