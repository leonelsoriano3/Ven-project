/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.dto.reportDentalTreatments;

import java.sql.Date;




/**
 *
 * @author akdesk01
 */
public class ReportDentalInfoOutput {

    private Date dateForReport;
    private Integer numCorrelativo;

    public ReportDentalInfoOutput() {
    }



    public Date getDateForReport() {
        return dateForReport;
    }

    public void setDateForReport(Date dateForReport) {
        this.dateForReport = dateForReport;
    }

    public Integer getNumCorrelativo() {
        return numCorrelativo;
    }

    public void setNumCorrelativo(Integer numCorrelativo) {
        this.numCorrelativo = numCorrelativo;
    }



}