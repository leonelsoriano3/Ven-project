/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.facade.store_procedure;


import com.venedental.dto.ReadPlansReportInput;
import com.venedental.dto.ReadPlansReportOutput;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class ReadPlansFacade
extends AbstractStoreProcedureFacade<ReadPlansReportInput, ReadPlansReportOutput>
implements ReadPlansFacadeLocal {
    
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public ReadPlansFacade() {
        super("SP_LEER_PLANES");
    }

    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, Integer.class, IN);
        query.registerStoredProcedureParameter(2, Integer.class, IN);
    }

    @Override
    protected void prepareInput(StoredProcedureQuery query, ReadPlansReportInput input) {
        query.setParameter(1, input.getIdInsurance());
        query.setParameter(2, input.getIdHealthArea());  
    }

    @Override
    protected ReadPlansReportOutput prepareOutput(Object[] result) {
        ReadPlansReportOutput output = new ReadPlansReportOutput();
        output.setIdPlan( (Integer) result[0]);
        output.setNamePlan((String) result[1]);
        return output;
    }

    




 
}
