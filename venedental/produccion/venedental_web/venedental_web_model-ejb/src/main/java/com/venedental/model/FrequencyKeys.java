/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "frequency_keys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FrequencyKeys.findAll", query = "SELECT f FROM FrequencyKeys f"),
    @NamedQuery(name = "FrequencyKeys.findById", query = "SELECT f FROM FrequencyKeys f WHERE f.id = :id"),
    @NamedQuery(name = "FrequencyKeys.findByName", query = "SELECT f FROM FrequencyKeys f WHERE f.name = :name"),
    @NamedQuery(name = "FrequencyKeys.findByDays", query = "SELECT f FROM FrequencyKeys f WHERE f.days = :days")})
public class FrequencyKeys implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name = "days")
    private Integer days;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFrequencyKeys")
    private List<RuleTreatments> ruleTreatmentsList;

    public FrequencyKeys() {
    }

    public FrequencyKeys(Integer id) {
        this.id = id;
    }

    public FrequencyKeys(Integer id, String name, Integer days) {
        this.id = id;
        this.name = name;
        this.days = days;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FrequencyKeys)) {
            return false;
        }
        FrequencyKeys other = (FrequencyKeys) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.FrequencyKeys[ id=" + id + " ]";
    }

    @XmlTransient
    public List<RuleTreatments> getRuleTreatmentsList() {
        return ruleTreatmentsList;
    }

    public void setRuleTreatmentsList(List<RuleTreatments> ruleTreatmentsList) {
        this.ruleTreatmentsList = ruleTreatmentsList;
    }

}
