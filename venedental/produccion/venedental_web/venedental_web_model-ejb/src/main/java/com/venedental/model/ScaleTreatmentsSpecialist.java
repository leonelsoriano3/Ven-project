/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Desarrollo
 */
@Entity
@Table(name = "scale_treatments_specialist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScaleTreatmentsSpecialist.findAll", query = "SELECT s FROM ScaleTreatmentsSpecialist s"),
    @NamedQuery(name = "ScaleTreatmentsSpecialist.findById", query = "SELECT s FROM ScaleTreatmentsSpecialist s WHERE s.id = :id"),
    @NamedQuery(name = "ScaleTreatmentsSpecialist.findByScale", query = "SELECT s FROM ScaleTreatmentsSpecialist s WHERE s.idScaleTreatments.id = :id_scale_treatments and s.idSpecialist.id = :id_specialist")})
public class ScaleTreatmentsSpecialist implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "id_specialist", referencedColumnName = "id")
    @ManyToOne
    private Specialists idSpecialist;

    @JoinColumn(name = "id_scale_treatments", referencedColumnName = "id")
    @ManyToOne
    private ScaleTreatments idScaleTreatments;

    @OneToMany(mappedBy = "idScaleTreatmentsSpecialist")
    private List<ScalePropertiesTreatmentsSpecialist> scalePropertiesTreatmentsSpecialistList;

    public ScaleTreatmentsSpecialist() {
    }

    public ScaleTreatmentsSpecialist(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Specialists getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Specialists idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public ScaleTreatments getIdScaleTreatments() {
        return idScaleTreatments;
    }

    public void setIdScaleTreatments(ScaleTreatments idScaleTreatments) {
        this.idScaleTreatments = idScaleTreatments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScaleTreatmentsSpecialist)) {
            return false;
        }
        ScaleTreatmentsSpecialist other = (ScaleTreatmentsSpecialist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.ScaleTreatmentsSpecialist[ id=" + id + " ]";
    }

    @XmlTransient
    public List<ScalePropertiesTreatmentsSpecialist> getScalePropertiesTreatmentsSpecialistList() {
        return scalePropertiesTreatmentsSpecialistList;
    }

    public void setScalePropertiesTreatmentsSpecialistList(List<ScalePropertiesTreatmentsSpecialist> scalePropertiesTreatmentsSpecialistList) {
        this.scalePropertiesTreatmentsSpecialistList = scalePropertiesTreatmentsSpecialistList;
    }

}
