/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.PlanTreatmentKeyInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class RegisterPlanTreatKeyDAO extends AbstractQueryDAO<PlanTreatmentKeyInput, PlansTreatmentsKeys> {

    public RegisterPlanTreatKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_I_001", 8);
    }

    @Override
    public void prepareInput(PlanTreatmentKeyInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKey());
        statement.setInt(2, input.getIdPlanTreatment());
        statement.setDate(3, new Date(input.getDateApplication().getTime()));
        statement.setInt(4, input.getIdStatus());
        statement.setInt(5, input.getIdScaleTreatments());
        statement.setString(6, input.getObservation());
        statement.setInt(7, input.getIdUser());
        if (input.getDateReception() != null) {
            statement.setDate(8, new Date(input.getDateReception().getTime()));
        } else {
            statement.setNull(8, java.sql.Types.NULL);
        }
    }

    @Override
    public PlansTreatmentsKeys prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
        plansTreatmentsKeys.setId(rs.getInt("id"));
        plansTreatmentsKeys.setStatusId(rs.getInt("id_status"));
        return plansTreatmentsKeys;
    }

}
