/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConsultKeyInStatusGenetaredOutPut;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyInStatusGenetaredDAO extends AbstractQueryDAO<Integer, ConsultKeyInStatusGenetaredOutPut> {

    public ConsultKeyInStatusGenetaredDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_010", 2);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        statement.registerOutParameter(2, java.sql.Types.BOOLEAN);
    }

    @Override
    public ConsultKeyInStatusGenetaredOutPut prepareOutput(ResultSet rs, CallableStatement statement)
            throws SQLException {

        ConsultKeyInStatusGenetaredOutPut consultKeyInStatusGenetaredOutPut
                = new ConsultKeyInStatusGenetaredOutPut();

        consultKeyInStatusGenetaredOutPut.setKeyStatusGenered(statement.getBoolean(2));

        return consultKeyInStatusGenetaredOutPut;

    }

}
