/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import com.venedental.model.InsurancesPatients;
import com.venedental.model.Patients;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface InsurancesPatientsFacadeLocal {

    InsurancesPatients create(InsurancesPatients patientsInsurance);

    InsurancesPatients edit(InsurancesPatients patientsInsurance);

    void remove(InsurancesPatients patientsInsurance);

    InsurancesPatients find(Object id);

    List<InsurancesPatients> findAll();

    List<InsurancesPatients> findRange(int[] range);
      
    List<InsurancesPatients> findByPatientsId (Integer id);
    
    List<InsurancesPatients> findByInsuranceId (Insurances insurance);

    List<InsurancesPatients> findByInsurancesPatientsId(Patients patient, Insurances insurance);
    
    int count();
    
    
}
