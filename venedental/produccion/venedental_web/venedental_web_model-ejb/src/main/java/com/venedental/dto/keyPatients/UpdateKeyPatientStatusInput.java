/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

/**
 *
 * @author AKDESKDEV90
 */
public class UpdateKeyPatientStatusInput {
    
    int idKeyPatient;
    int idStatus;
    int idUser;

    public UpdateKeyPatientStatusInput(int idKeyPatient, int idStatus, int idUser) {
        this.idKeyPatient = idKeyPatient;
        this.idStatus = idStatus;
        this.idUser = idUser;
    }  
    
    public int getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(int idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    
}

