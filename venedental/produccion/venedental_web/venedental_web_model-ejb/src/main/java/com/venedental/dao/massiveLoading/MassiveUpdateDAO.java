/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.massiveLoading;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.massiveLoading.MassiveUpdateExecution;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class MassiveUpdateDAO extends AbstractCommandDAO<MassiveUpdateExecution> {

    public MassiveUpdateDAO(DataSource dataSource) {
        super(dataSource, "CARGA_MASIVA", 4);
    }

    @Override
    public void prepareInput(MassiveUpdateExecution input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdInsurance());   
        statement.setBoolean(2, input.getCompletLoading());
        statement.setBoolean(3, input.getInclude()); 
        statement.setBoolean(4, input.getExclude()); 
        
        
    }

}
