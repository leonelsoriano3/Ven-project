/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "specialist_job", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SpecialistJob.findAll", query = "SELECT s FROM SpecialistJob s"),
    @NamedQuery(name = "SpecialistJob.findById", query = "SELECT s FROM SpecialistJob s WHERE s.id = :id"),
    @NamedQuery(name = "SpecialistJob.findBySchedule", query = "SELECT s FROM SpecialistJob s WHERE s.schedule = :schedule"),
    @NamedQuery(name = "SpecialistJob.findBySpecialist", query = "SELECT s FROM SpecialistJob s WHERE s.specialist_id = :specialist_id"),
    @NamedQuery(name = "SpecialistJob.deleteJobsDependency", query = "DELETE FROM SpecialistJob s WHERE s.specialist_id = :specialist_id")})
public class SpecialistJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 500)
    @Column(name = "schedule")
    private String schedule;

    @JoinColumn(insertable = false, updatable = false, name = "specialist_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Specialists specialistId;

    @JoinColumn(insertable = false, updatable = false, name = "medical_office_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MedicalOffice medicalOfficeId;

    @JoinColumn(name = "specialist_id", referencedColumnName = "id")
    private int specialist_id;

    @JoinColumn(name = "medical_office_id", referencedColumnName = "id")
    private int medical_office_id;

    @ElementCollection
    @CollectionTable(
            name = "phone_clinic",
            joinColumns = @JoinColumn(name = "specialist_job_id")
    )
    private List<PhoneClinic> phoneClinicList;

    public SpecialistJob() {

    }

    public SpecialistJob(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getMedical_office_id() {
        return medical_office_id;
    }

    public void setMedical_office_id(int medical_office_id) {
        this.medical_office_id = medical_office_id;
    }

    public int getSpecialist_id() {
        return specialist_id;
    }

    public void setSpecialist_id(int specialist_id) {
        this.specialist_id = specialist_id;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @XmlTransient
    public Specialists getSpecialistId() {
        return specialistId;
    }

    public void setSpecialistId(Specialists specialistId) {
        this.specialistId = specialistId;
    }

    public MedicalOffice getMedicalOfficeId() {
        return medicalOfficeId;
    }

    public void setMedicalOfficeId(MedicalOffice medicalOfficeId) {
        this.medicalOfficeId = medicalOfficeId;
    }

    public List<PhoneClinic> getPhoneClinicList() {
        return phoneClinicList;
    }

    public void setPhoneClinicList(List<PhoneClinic> phoneClinicList) {
        this.phoneClinicList = phoneClinicList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SpecialistJob)) {
            return false;
        }
        SpecialistJob other = (SpecialistJob) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.SpecialistJob[ id=" + id + " ]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        SpecialistJob obj = null;
        obj = (SpecialistJob) super.clone();
        obj.specialistId = obj.specialistId == null ? null : (Specialists) obj.specialistId.clone();
        return obj;
    }

}
