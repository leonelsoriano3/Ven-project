/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.propertiesPlanTreatmentKey;

/**
 *
 * @author root
 */
public class UpdatePropertiesPlanTreatmentKeyObservationInput {
    int idPropertiesPlanTreatmentKey;
    String observation;

    public UpdatePropertiesPlanTreatmentKeyObservationInput(int idPropertiesPlanTreatmentKey, String observation) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
        this.observation = observation;
    }

    public int getIdPropertiesPlanTreatmentKey() {
        return idPropertiesPlanTreatmentKey;
    }

    public void setIdPropertiesPlanTreatmentKey(int idPropertiesPlanTreatmentKey) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }    
    
}
