/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.medicalOffice;


import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.medicalOffice.FindMedicalOfficeBySepcialistOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
/**
 *
 * @author akdesk01
 */
public class FindMedicalOfficeSpecialistManagementDAO extends AbstractQueryDAO<Integer, FindMedicalOfficeBySepcialistOutput> {
    
     public FindMedicalOfficeSpecialistManagementDAO(DataSource dataSource) {
        super(dataSource, "SP_T_MEDICALOFFIC_G_002", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public FindMedicalOfficeBySepcialistOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        FindMedicalOfficeBySepcialistOutput findMedicalOfficeBySepcialistOutput = new FindMedicalOfficeBySepcialistOutput();
        findMedicalOfficeBySepcialistOutput.setId(rs.getInt("ID_CONSULTORIO"));
        findMedicalOfficeBySepcialistOutput.setName(rs.getString("NOMBRE_CONSULTORIO"));
        findMedicalOfficeBySepcialistOutput.setIdAddress(rs.getInt("id_direccion"));
        findMedicalOfficeBySepcialistOutput.setDescriptionAddress(rs.getString("nombre_direccion"));
        return findMedicalOfficeBySepcialistOutput;
    }

    
}
