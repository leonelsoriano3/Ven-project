/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class GeneratePaymentReportRetentionDAO extends AbstractQueryDAO<PaymentReportInput, String[]> {

    public GeneratePaymentReportRetentionDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_GenerarISLR", 4);
    }

    @Override
    public void prepareInput(PaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdPaymentReport());
        statement.registerOutParameter(3, java.sql.Types.VARCHAR);
        statement.registerOutParameter(4, java.sql.Types.INTEGER);
    }

    @Override
    public String[] prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        String [] output = new String[2];
        String retentionNumberISLR;
        retentionNumberISLR = statement.getString(3);
        Integer type_error = statement.getInt(4);
        output[0] = retentionNumberISLR;
        output[1] = String.valueOf(type_error);
        return output;
    }
    
}
