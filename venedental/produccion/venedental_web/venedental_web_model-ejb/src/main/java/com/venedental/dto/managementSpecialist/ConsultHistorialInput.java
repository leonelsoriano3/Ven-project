/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultHistorialInput {
    private Integer idPatient;
    private Integer idSpecialist;

    public ConsultHistorialInput(Integer idPatient, Integer idSpecialist) {
        this.idPatient = idPatient;
        this.idSpecialist = idSpecialist;
    }

    

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }
    
    
    
}
