/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Properties;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author CarlosDaniel
 */
@Local
public interface PropertiesFacadeLocal {

    Properties create(Properties properties);

    Properties edit(Properties properties);

    void remove(Properties properties);

    Properties find(Object id);

    List<Properties> findAll();

    List<Properties> findRange(int[] range);

    int count();
    
    Properties findByKey(String key);
    
}
