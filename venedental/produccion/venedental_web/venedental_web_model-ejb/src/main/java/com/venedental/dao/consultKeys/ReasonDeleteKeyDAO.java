/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.consultKeys.ReasonDeleteKeyInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ReasonDeleteKeyDAO extends AbstractCommandDAO<ReasonDeleteKeyInput>{
    
     public ReasonDeleteKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_D_001", 3);
    }
   
     @Override
    public void prepareInput(ReasonDeleteKeyInput input, CallableStatement statement) throws SQLException {       
                statement.setInt(1, input.getIdKey());
                statement.setString(2, input.getObservation());
                statement.setInt(3, input.getIdUser());
    }

    
    
}
