/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.Users;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author CarlosDaniel
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> implements UsersFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    @Override
    public Users findByLoginAndPassword(String login, String password) {
        TypedQuery<Users> query = em.createNamedQuery("Users.findByLoginAndPassword", Users.class);
        query.setParameter("login", login);
        query.setParameter("password", password);
        Users usuarioLogueado;
        try {
            usuarioLogueado = query.getSingleResult();
        } catch (NoResultException p) {
            return new Users();
        } catch (Exception p) {
            return null;
        }
        return usuarioLogueado;
    }

    @Override
    public Users findByLogin(String login) {
        TypedQuery<Users> query = em.createNamedQuery("Users.findByLogin", Users.class);
        query.setParameter("login", login);
        Users usuarioLogueado = query.getSingleResult();
        return usuarioLogueado;
    }

}
