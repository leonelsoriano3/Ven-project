/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.medicalOffice;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.MedicalOffice;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindMedicalOfficeByIdSpecialistDAO extends AbstractQueryDAO<Integer, MedicalOffice> {

    public FindMedicalOfficeByIdSpecialistDAO(DataSource dataSource) {
        super(dataSource, "SP_T_MEDICALOFFIC_G_002", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public MedicalOffice prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        MedicalOffice medicalOffice = new MedicalOffice();
        medicalOffice.setId(rs.getInt("ID_CONSULTORIO"));
        medicalOffice.setName(rs.getString("NOMBRE_CONSULTORIO"));
        medicalOffice.setIdAddress(rs.getInt("id_direccion"));
        medicalOffice.setDescriptionAddress(rs.getString("nombre_direccion"));
        return medicalOffice;
    }

}