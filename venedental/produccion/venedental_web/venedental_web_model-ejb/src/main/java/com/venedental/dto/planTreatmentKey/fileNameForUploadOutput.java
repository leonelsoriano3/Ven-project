/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

/**
 *
 * @author akdesk01
 */
public class fileNameForUploadOutput {
    
    private Integer idPropertiesTreatmentKey;
    private String nameFileRegister;
    private Integer idPropertiesPlanTreatmentKey;

    public fileNameForUploadOutput(Integer idPropertiesTreatmentKey,String nameFileRegister, Integer idPropertiesPlanTreatmentKey) {
        this.idPropertiesPlanTreatmentKey=idPropertiesPlanTreatmentKey;
        this.nameFileRegister=nameFileRegister;
        this.idPropertiesTreatmentKey=idPropertiesTreatmentKey;
    }

    public Integer getIdPropertiesTreatmentKey() {
        return idPropertiesTreatmentKey;
    }

    public void setIdPropertiesTreatmentKey(Integer idPropertiesTreatmentKey) {
        this.idPropertiesTreatmentKey = idPropertiesTreatmentKey;
    }

    public String getNameFileRegister() {
        return nameFileRegister;
    }

    public void setNameFileRegister(String nameFileRegister) {
        this.nameFileRegister = nameFileRegister;
    }

    public Integer getIdPropertiesPlanTreatmentKey() {
        return idPropertiesPlanTreatmentKey;
    }

    public void setIdPropertiesPlanTreatmentKey(Integer idPropertiesPlanTreatmentKey) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
    }
    
    
    
}
