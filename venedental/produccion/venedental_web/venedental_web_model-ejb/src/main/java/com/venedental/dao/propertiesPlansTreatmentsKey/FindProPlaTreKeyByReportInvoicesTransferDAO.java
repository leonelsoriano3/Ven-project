/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByReportInvoicesTransferInput;
import com.venedental.dto.propertiesPlanTreatmentKey.FindProPlaTreKeyByPlanTreKeyInput;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author Jorge Chaviel
 */
public class FindProPlaTreKeyByReportInvoicesTransferDAO extends AbstractQueryDAO<FindPlanTreatKeyByReportInvoicesTransferInput, PropertiesPlansTreatmentsKey> {
    
    
       public FindProPlaTreKeyByReportInvoicesTransferDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_006", 4);
    }

    @Override
    public void prepareInput(FindPlanTreatKeyByReportInvoicesTransferInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdReport());
        statement.setInt(2, input.getIdStatus());
        statement.setDate(3, (java.sql.Date) input.getDateFrom()); 
        statement.setDate(4, (java.sql.Date) input.getDateUntil()); 
    }

     @Override
    public PropertiesPlansTreatmentsKey prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new  PropertiesPlansTreatmentsKey();
        propertiesPlansTreatmentsKey.setId(rs.getInt("id"));
        propertiesPlansTreatmentsKey.setIdPlansTreatmentsKey(rs.getInt("plans_treatments_keys"));
        propertiesPlansTreatmentsKey.setIdPropertiesTreatments(rs.getInt("properties_treatments_id"));
        propertiesPlansTreatmentsKey.setIdStatusProTreatments(rs.getInt("id_status"));
        propertiesPlansTreatmentsKey.setIdScalePropertiesTreatment(rs.getInt("id_scale_properties_treatments"));
        propertiesPlansTreatmentsKey.setObservation(rs.getString("observation"));
        return propertiesPlansTreatmentsKey;
    }
    
}
