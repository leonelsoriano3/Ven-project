/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.managementSpecialist.RemoveAssignedPropertiesInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class RemoveAssignedPropertiesDAO extends AbstractCommandDAO<RemoveAssignedPropertiesInput>{
    
    
    public RemoveAssignedPropertiesDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_D_001", 1);
    }

    @Override
    public void prepareInput(RemoveAssignedPropertiesInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input.getIdPieceTreatment());
        
    }
    
}
