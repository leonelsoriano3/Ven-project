/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesTreatmentsRTInput {
    
    private Integer id_Key, id_piece;

    public ConsultPropertiesTreatmentsRTInput() {
    }

    public Integer getId_Key() {
        return id_Key;
    }

    public void setId_Key(Integer id_Key) {
        this.id_Key = id_Key;
    }

    public Integer getId_piece() {
        return id_piece;
    }

    public void setId_piece(Integer id_piece) {
        this.id_piece = id_piece;
    }
    
    
    
}
