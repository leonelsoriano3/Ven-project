/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class ConfirmKeyDAO extends AbstractQueryDAO<Integer, ConfirmKeyOutput> {

    public ConfirmKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_006", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        
    }

    @Override
    public ConfirmKeyOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConfirmKeyOutput confirmKeyOutput = new ConfirmKeyOutput();
        confirmKeyOutput.setDateExecution(rs.getDate("fecha_ejecucion"));
        confirmKeyOutput.setIdMonthExecution(rs.getInt("id_mes_ejecucion"));
        confirmKeyOutput.setNameMonthExecution(rs.getString("nombre_mes_ejecucion"));
        confirmKeyOutput.setStatus(rs.getInt("id_estatus"));
        confirmKeyOutput.setNameStatus(rs.getString("nombre_estatus_ver"));
        confirmKeyOutput.setIdRegistre(rs.getInt("id_registro"));
        return confirmKeyOutput;
    }

}
