/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultPropertiesTreatmentsRTInput;
import com.venedental.dto.managementSpecialist.ConsultPropertiesTreatmentsRTOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesTreatmentsDAO extends AbstractQueryDAO<ConsultPropertiesTreatmentsRTInput, ConsultPropertiesTreatmentsRTOutput> {
    
    public ConsultPropertiesTreatmentsDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_007", 2);
    }
    
     @Override
    public void prepareInput(ConsultPropertiesTreatmentsRTInput input, CallableStatement statement) throws SQLException {
        if(input.getId_Key()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getId_Key()); 
        }
         if(input.getId_piece()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setInt(2,input.getId_piece()); 
        }
         
    }

    @Override
    public ConsultPropertiesTreatmentsRTOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
       ConsultPropertiesTreatmentsRTOutput consultPropertiesTreatmentsRTOutput = new ConsultPropertiesTreatmentsRTOutput();
        consultPropertiesTreatmentsRTOutput.setIdPiece(rs.getInt("id_pieza"));
        consultPropertiesTreatmentsRTOutput.setIdPieceTreatment(rs.getInt("id_pieza_tratamiento"));
        consultPropertiesTreatmentsRTOutput.setIdPlanTreatment(rs.getInt("id_plan_tratamiento"));
        consultPropertiesTreatmentsRTOutput.setIdPricePiece(rs.getInt("id_baremo_pieza"));
        consultPropertiesTreatmentsRTOutput.setIdPriceTreatment(rs.getInt("id_baremo_tratamiento"));
         consultPropertiesTreatmentsRTOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        consultPropertiesTreatmentsRTOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultPropertiesTreatmentsRTOutput.setValuePiece(rs.getString("valor_pieza"));
        
        return consultPropertiesTreatmentsRTOutput;
    }
    
    
}
