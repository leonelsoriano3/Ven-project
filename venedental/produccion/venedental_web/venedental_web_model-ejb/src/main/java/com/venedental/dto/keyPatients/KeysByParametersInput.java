/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class KeysByParametersInput {

    private Integer idKey;
    private String numberKey;
    private Integer idPatiente;
    private Integer idSpecialist;
    private Date dateFrom;
    private Date dateUntil;
    private Integer idStatus;


    public KeysByParametersInput(Integer idKey, String numberKey, Integer idPatiente, Integer idSpecialist, Date dateFrom, Date dateUntil, Integer idStatus) {
        this.idKey = idKey;
        this.numberKey = numberKey;
        this.idPatiente = idPatiente;
        this.idSpecialist = idSpecialist;
        this.dateFrom = dateFrom;
        this.dateUntil = dateUntil;
        this.idStatus = idStatus;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Integer getIdPatiente() {
        return idPatiente;
    }

    public void setIdPatiente(Integer idPatiente) {
        this.idPatiente = idPatiente;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(Date dateUntil) {
        this.dateUntil = dateUntil;
    }


}