/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.speciality;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.Speciality;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindSpecialityOfSpecialistDAO extends AbstractQueryDAO<Integer, Speciality> {

    public FindSpecialityOfSpecialistDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALITY_G_002", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public Speciality prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Speciality speciality = new Speciality();
        speciality.setId(rs.getInt("is_especialidad"));
        speciality.setName(rs.getString("nombre_especialidad"));
        return speciality;
    }
}
