/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.parameters;

/**
 *
 * @author AKDESKDEV90
 */
public class FindParametersOutput {
    private Integer idParameters;
    private String codigo;
    private String name;
    private String description;
    private String valueString;
    private Double valueDouble;

    public FindParametersOutput() {
    }

    public FindParametersOutput(Integer idParameters, String codigo, String name, String description, String valueString, Double valueDouble) {
        this.idParameters = idParameters;
        this.codigo = codigo;
        this.name = name;
        this.description = description;
        this.valueString = valueString;
        this.valueDouble = valueDouble;
    }

    public Integer getIdParameters() {
        return idParameters;
    }

    public void setIdParameters(Integer idParameters) {
        this.idParameters = idParameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public Double getValueDouble() {
        return valueDouble;
    }

    public void setValueDouble(Double valueDouble) {
        this.valueDouble = valueDouble;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
}
