/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.KeysPatients;
import com.venedental.model.security.Menu;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Edgar Anzola
 */
@Stateless
public class MenuFacade extends AbstractFacade<Menu> implements MenuFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuFacade() {
        super(Menu.class);
    }

    @Override
    public List<Menu> findByFather(int father) {
        TypedQuery<Menu> query = em.createNamedQuery("Menu.findByFather", Menu.class);
        query.setParameter("father", father);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String countByFather(int father) {
        Query query = em.createNativeQuery("select count(*) from menu where menu.father = " + father);
        query.setParameter("father", father);
        return query.getSingleResult().toString();
    }

    @Override
    public Menu findById(Integer id) {
        TypedQuery<Menu> query = em.createNamedQuery("Menu.findById", Menu.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public Menu findByName(String name) {
        TypedQuery<Menu> query = em.createNamedQuery("Menu.findByName", Menu.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }
    
    @Override
    public List<Menu> findByUser(Integer idUser) {
        
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_MENU_G_002", Menu.class);
        query.registerStoredProcedureParameter("PI_ID_USUARIO", Integer.class, ParameterMode.IN);       
        query.setParameter("PI_ID_USUARIO", idUser);
        return query.getResultList();
    }
    
    @Override
    public List<Menu> findSonByUserAndFather(Integer idUser, Integer father) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_MENU_G_003", Menu.class);
        query.registerStoredProcedureParameter("PI_ID_USUARIO", Integer.class, ParameterMode.IN);  
        query.registerStoredProcedureParameter("PI_ID_PADRE", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_USUARIO", idUser);
        query.setParameter("PI_ID_PADRE", father);
        return query.getResultList();
    }
    

}
