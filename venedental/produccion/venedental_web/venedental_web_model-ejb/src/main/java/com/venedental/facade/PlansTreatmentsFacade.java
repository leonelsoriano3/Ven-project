/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansTreatments;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PlansTreatmentsFacade extends AbstractFacade<PlansTreatments> implements PlansTreatmentsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlansTreatmentsFacade() {
        super(PlansTreatments.class);
    }

    @Override
    public PlansTreatments edit(PlansTreatments entity) {
        return super.edit(entity);
    }

    @Override
    public PlansTreatments create(PlansTreatments entity) {
        return super.create(entity);
    }

    @Override
    public List<PlansTreatments> findByPlansId(Integer id) {
        TypedQuery<PlansTreatments> query = em.createNamedQuery("PlansTreatments.findByPlansId", PlansTreatments.class);
        query.setParameter("plansId", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    /*Init Stored Procedure */    
    @Override
    public List<PlansTreatments> findByPlansAndPlansTratmentsKeys(Integer PI_ID_PLAN, Integer PI_ID_CLAVE) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_TRATAMIENTOS_NO_ASIGNADO_CLAVES", PlansTreatments.class);
        query.registerStoredProcedureParameter("PI_ID_CLAVE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_PLAN", Integer.class, ParameterMode.IN);        
        query.setParameter("PI_ID_CLAVE", PI_ID_CLAVE);
        query.setParameter("PI_ID_PLAN", PI_ID_PLAN);                
        return query.getResultList();
    }
    
    /*End Stored Procedure*/

}
