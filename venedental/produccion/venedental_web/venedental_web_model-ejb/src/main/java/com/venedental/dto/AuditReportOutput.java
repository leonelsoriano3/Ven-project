/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ARKDEV16
 */
public class AuditReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Date applicationDate;
    
    private String number;
    
    private String specialistIdentityNumber;
    
    private String specialistCompleteName;
    
    private String patientIdentityNumber;
    
    private String patientCompleteName;
    
    private String specialityName;
    
    private String planName;
    
    private String treatment;
    
    private String tooth;
    
    private Double baremoPrice;
    
    private String insuranceName;
    
    private String entity;
    
    private String stateName;
    
    private String healthArea;
    
    
    public Date getApplicationDate() {
        return applicationDate;
    }
    
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }
    
    public String getNumber() {
        return number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public String getSpecialistIdentityNumber() {
        return specialistIdentityNumber;
    }
    
    public void setSpecialistIdentityNumber(String specialistIdentityNumber) {
        this.specialistIdentityNumber = specialistIdentityNumber;
    }
    
    public String getSpecialistCompleteName() {
        return specialistCompleteName;
    }
    
    public void setSpecialistCompleteName(String specialistCompleteName) {
        this.specialistCompleteName = specialistCompleteName;
    }
    
    public String getPatientIdentityNumber() {
        return patientIdentityNumber;
    }
    
    public void setPatientIdentityNumber(String patientIdentityNumber) {
        this.patientIdentityNumber = patientIdentityNumber;
    }
    
    public String getPatientCompleteName() {
        return patientCompleteName;
    }
    
    public void setPatientCompleteName(String patientCompleteName) {
        this.patientCompleteName = patientCompleteName;
    }
    
    public String getPlanName() {
        if(this.planName==null || this.planName.equals(""))
        {
            planName="-";
        } 
        return planName;
    }
    
    public void setPlanName(String planName) {

        this.planName = planName;
    }
    
    public String getTreatment() {
        if(this.treatment == null || this.treatment.equals("")){
  
            treatment="-";         
        }
        return treatment;
    }
    
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }
    
    public String getTooth() {
        if(this.tooth == null || this.tooth.equals(""))
            tooth="-";
        return tooth;
    }
    
    public void setTooth(String tooth) {
        this.tooth = tooth;
    }

    public Double getBaremoPrice() {
        
        return baremoPrice;
    }

    public void setBaremoPrice(Double baremoPrice) {
        this.baremoPrice = baremoPrice;
    }
    
    public String getInsuranceName() {
        if(this.insuranceName==null || this.insuranceName.equals(""))
        {
            insuranceName="-";
        }
        return insuranceName;
    }
    
    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }   

    public String getEntity() {
    if(this.entity == null || this.entity.equals(""))
            entity="-";
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }


    public String getSpecialityName() {
    if(this.specialityName == null || this.specialityName.equals(""))
            specialityName="-";
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public String getStateName() {
    if(this.stateName == null || this.stateName.equals(""))
            stateName="-";
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getHealthArea() {
   if(this.healthArea == null || this.healthArea.equals(""))
            healthArea="-";
        return healthArea;
    }

    public void setHealthArea(String healthArea) {
        this.healthArea = healthArea;
    }
}
