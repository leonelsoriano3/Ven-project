/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPtkByKeyAndStatusInput {
    private Integer idKeyPatient;
    private Integer idStatusOne;
    private Integer idStatusTwo;

    public FindPtkByKeyAndStatusInput(Integer idKeyPatient, Integer idStatusOne, Integer idStatusTwo) {
        this.idKeyPatient = idKeyPatient;
        this.idStatusOne = idStatusOne;
        this.idStatusTwo = idStatusTwo;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public Integer getIdStatusOne() {
        return idStatusOne;
    }

    public void setIdStatusOne(Integer idStatusOne) {
        this.idStatusOne = idStatusOne;
    }

    public Integer getIdStatusTwo() {
        return idStatusTwo;
    }

    public void setIdStatusTwo(Integer idStatusTwo) {
        this.idStatusTwo = idStatusTwo;
    }
       
}
