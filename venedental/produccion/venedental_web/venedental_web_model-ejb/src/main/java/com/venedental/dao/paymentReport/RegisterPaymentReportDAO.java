/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.CreatePaymentReportInput;
import com.venedental.dto.analysisKeys. CreatePaymentReportOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class RegisterPaymentReportDAO extends AbstractQueryDAO<CreatePaymentReportInput,CreatePaymentReportOutput> {

    public RegisterPaymentReportDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_I_001", 2);
    }

    @Override
    public void prepareInput(CreatePaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setDouble(1, input.getAmount());
        statement.setDouble(2, input.getAuditedAmount());
        

    }

    @Override
    public CreatePaymentReportOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

     
        CreatePaymentReportOutput createPaymentReportOutput= new CreatePaymentReportOutput();
        createPaymentReportOutput.setIdPaymentReport(rs.getInt("ID_REPORTE_PAGO"));
        createPaymentReportOutput.setNumber(rs.getString("NUMERO_REPORTE_PAGO"));
        return createPaymentReportOutput;
    }

    

}
