/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;

/**
 *
 * @author ARKDEV16
 */
public class ReadTreatmentsReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer idTreatment;
    
    private String nameTreatment;

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

   
}