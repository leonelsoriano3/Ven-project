/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;


import com.venedental.model.StatusAnalysisKeys;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Mosquera
 */
@Local
public interface StatusAnalysisKeysFacadeLocal {

    StatusAnalysisKeys create(StatusAnalysisKeys statusAnalysisKeys);

    StatusAnalysisKeys edit(StatusAnalysisKeys statusAnalysisKeys);

    void remove(StatusAnalysisKeys statusAnalysisKeys);

    StatusAnalysisKeys find(Object id);

    List<StatusAnalysisKeys> findAll();

    List<StatusAnalysisKeys> findRange(int[] range);
    
    StatusAnalysisKeys findById(Integer id);

    int count();
    
}
