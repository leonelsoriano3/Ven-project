
package com.venedental.dao.createUser;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.createUser.LoginUserInput;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;


/**
 *
 * @author luiscarrasco1991@gmail.com fecha: 4-05-16
 */
public class LoginUserDAO extends AbstractQueryDAO <LoginUserInput,String>{

    public LoginUserDAO(DataSource dataSource) {
        super(dataSource,"SP_M_CRE_ObtenerLoginUsuario",3);
    }

    @Override
    public void prepareInput(LoginUserInput input, CallableStatement statement) throws SQLException {

        statement.setString(1,input.getName());
        statement.setString(2,input.getLastName());

    }

    @Override
    public String prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException{
        String loginUser="";
        loginUser = statement.getString(3);
        return loginUser;

    }

}