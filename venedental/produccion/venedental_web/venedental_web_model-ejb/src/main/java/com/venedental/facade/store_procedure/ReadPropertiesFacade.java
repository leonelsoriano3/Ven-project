/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.facade.store_procedure;



import com.venedental.dto.ReadPropertiesReportInput;
import com.venedental.dto.ReadPropertiesReportOutput;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class ReadPropertiesFacade
extends AbstractStoreProcedureFacade<ReadPropertiesReportInput, ReadPropertiesReportOutput>
implements ReadPropertiesFacadeLocal {
    
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public ReadPropertiesFacade() {
        super("SP_LEER_PIEZAS");
    }

    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, Integer.class, IN);
        query.registerStoredProcedureParameter(2, Integer.class, IN);
        query.registerStoredProcedureParameter(3, Integer.class, IN);
        query.registerStoredProcedureParameter(4, Integer.class, IN);
    }

    @Override
    protected void prepareInput(StoredProcedureQuery query, ReadPropertiesReportInput input) {
        query.setParameter(1, input.getIdInsurance());
        query.setParameter(2, input.getIdHealthArea());
        query.setParameter(3, input.getIdPlan());
        query.setParameter(4, input.getIdTreatment());
    }

    @Override
    protected ReadPropertiesReportOutput prepareOutput(Object[] result) {
        ReadPropertiesReportOutput output = new ReadPropertiesReportOutput();
        output.setIdProperties((Integer) result[0]);
        output.setNameProperties((String) result[1]);
        return output;
    }
   
}
