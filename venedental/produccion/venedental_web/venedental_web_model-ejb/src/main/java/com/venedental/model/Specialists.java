/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import com.venedental.model.security.Users;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "specialists", schema = "")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Specialists.findAll", query = "SELECT s FROM Specialists s"),
        @NamedQuery(name = "Specialists.findById", query = "SELECT s FROM Specialists s WHERE s.id = :id"),
        @NamedQuery(name = "Specialists.findByFirstName", query = "SELECT s FROM Specialists s WHERE s.firstName = :firstName"),
        @NamedQuery(name = "Specialists.findBySecondName", query = "SELECT s FROM Specialists s WHERE s.secondName = :secondName"),
        @NamedQuery(name = "Specialists.findByLastname", query = "SELECT s FROM Specialists s WHERE s.lastname = :lastname"),
        @NamedQuery(name = "Specialists.findBySecondLastname", query = "SELECT s FROM Specialists s WHERE s.secondLastname = :secondLastname"),
        @NamedQuery(name = "Specialists.findByCompleteName", query = "SELECT s FROM Specialists s WHERE s.completeName = :completeName"),
        @NamedQuery(name = "Specialists.findByPhone", query = "SELECT s FROM Specialists s WHERE s.phone = :phone"),
        @NamedQuery(name = "Specialists.findByCellphone", query = "SELECT s FROM Specialists s WHERE s.cellphone = :cellphone"),
        @NamedQuery(name = "Specialists.findByRif", query = "SELECT s FROM Specialists s WHERE s.rif = :rif"),
        @NamedQuery(name = "Specialists.findByIdentityNumber", query = "SELECT s FROM Specialists s WHERE s.identityNumber = :identity_number"),
        @NamedQuery(name = "Specialists.findByMsds", query = "SELECT s FROM Specialists s WHERE s.msds = :msds"),
        @NamedQuery(name = "Specialists.findByMedicalAsociation", query = "SELECT s FROM Specialists s WHERE s.medicalAsociation = :medicalAsociation"),
        @NamedQuery(name = "Specialists.findBySex", query = "SELECT s FROM Specialists s WHERE s.sex = :sex"),
        @NamedQuery(name = "Specialists.findByCivilStatus", query = "SELECT s FROM Specialists s WHERE s.civilStatus = :civilStatus"),
        @NamedQuery(name = "Specialists.findBySchedule", query = "SELECT s FROM Specialists s WHERE s.schedule = :schedule"),
        @NamedQuery(name = "Specialists.findByStatus", query = "SELECT s FROM Specialists s WHERE s.status = :status"),
        @NamedQuery(name = "Specialists.findByUserId", query = "SELECT s FROM Specialists s WHERE s.usersId.id = :users_id"),
        @NamedQuery(name = "Specialists.updateStatus", query = "UPDATE Specialists s SET s.status = 0 WHERE s.id = :specialist_id"),
        @NamedQuery(name = "Specialists.deleteSpecialist", query = "DELETE FROM Specialists s WHERE s.id = :specialist_id")})
public class Specialists implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 255)
    @Column(name = "second_name")
    private String secondName;

    @Size(max = 255)
    @Column(name = "lastname")
    private String lastname;

    @Size(max = 255)
    @Column(name = "second_lastname")
    private String secondLastname;

    @Size(max = 255)
    @Column(name = "complete_name")
    private String completeName;

    @Size(max = 255)
    @Column(name = "phone")
    private String phone;

    @Size(max = 255)
    @Column(name = "cellphone")
    private String cellphone;

    @Size(max = 20)
    @Column(name = "rif")
    private String rif;

    @Column(name = "identity_number")
    private Integer identityNumber;

    @Size(max = 20)
    @Column(name = "msds")
    private String msds;

    @Size(max = 20)
    @Column(name = "medical_asociation")
    private String medicalAsociation;

    @Size(min = 1, max = 10)
    @Column(name = "sex")
    private String sex;

    @Size(max = 20)
    @Column(name = "civil_status")
    private String civilStatus;

    @Size(max = 255)
    @Column(name = "schedule")
    private String schedule;

    @Size(max = 260)
    @Column(name = "observation")
    private String observation;

    @Column(name = "papeleria")
    private Boolean papeleria;

    @JoinTable(name = "specialists_specialities", joinColumns = {
            @JoinColumn(name = "specialist_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "speciality_id", referencedColumnName = "id")})
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private List<Speciality> specialityList;

    @OneToMany(mappedBy = "idOwner")
    private List<BankAccounts> bankAccountsList;

    @OneToMany(mappedBy = "specialistId", cascade = CascadeType.ALL)
    private List<SpecialistJob> specialistJobList;

    @OneToMany(mappedBy = "idSpecialist")
    private List<ScaleTreatmentsSpecialist> scaleTreatmentsSpecialistList;

    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @OneToOne(cascade = CascadeType.ALL)
    private Addresses addressId;

    @JoinColumn(name = "typeSpecialist_id", referencedColumnName = "id")
    @ManyToOne
    private TypeSpecialist typeSpecialistid;

    @OneToMany(mappedBy = "specialistId")
    private Collection<KeysPatients> keysCollection;

    @Size(max = 100)
    @Column(name = "email")
    private String email;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @JoinColumn(name = "users_id", referencedColumnName = "id")
    @ManyToOne
    private Users usersId;

    @JoinColumn(name = "id_scale", referencedColumnName = "id")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Scale scale;

    @Transient
    private String descStatus;

    @Transient
    private List<String> nameMedicalOffice;

    @Transient
    private List<String> cities;

    @Transient
    private List<String> states;

    @Transient
    private List<Clinics> clinicsList;


    /*used in DAO*/
    @Transient
    private Integer typeSpecialist_id;

    @Transient
    private String specialitys;


    /*used in DAO*/
    @Transient
    private Integer  idTypeSpeciality;
    /*end*/


    /*end*/

    public String getDescStatus() {
        return descStatus;
    }

    public void setDescStatus(String descStatus) {
        this.descStatus = descStatus;
    }

    private void modificarEstatus() {

        if (this.status != null) {
            if (this.status == 1) {
                setDescStatus("Habilitado");

            } else {

                if (this.status == 2) {
                    setDescStatus("Suspendido");
                } else {
                    setDescStatus("Inactivo");
                }
            }
        } else {
            setDescStatus("Inactivo");
        }

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getPapeleria() {
        return papeleria;
    }

    public void setPapeleria(Boolean papeleria) {
        this.papeleria = papeleria;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Specialists() {
        this.modificarEstatus();
    }

    public Specialists(Integer id) {
        this.id = id;
        this.modificarEstatus();
    }

    public Specialists(Integer id, String firstName, String secondName, String lastname, String secondLastname, String completeName, String phone, String cellphone, String rif, Integer identityNumber, String msds, String medicalAsociation, String sex, String civilStatus, String schedule, int status, String email) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastname = lastname;
        this.secondLastname = secondLastname;
        this.completeName = completeName;
        this.phone = phone;
        this.cellphone = cellphone;
        this.rif = rif;
        this.identityNumber = identityNumber;
        this.msds = msds;
        this.medicalAsociation = medicalAsociation;
        this.sex = sex;
        this.civilStatus = civilStatus;
        this.schedule = schedule;
        this.status = status;
        this.email = email;
        this.modificarEstatus();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondLastname() {
        return secondLastname;
    }

    public void setSecondLastname(String secondLastname) {
        this.secondLastname = secondLastname;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getRif() {
        return rif;
    }

    public Collection<KeysPatients> getKeysCollection() {
        return keysCollection;
    }

    public void setKeysCollection(Collection<KeysPatients> keysCollection) {
        this.keysCollection = keysCollection;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public Integer getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getMsds() {
        return msds;
    }

    public void setMsds(String msds) {
        this.msds = msds;
    }

    public String getMedicalAsociation() {
        return medicalAsociation;
    }

    public void setMedicalAsociation(String medicalAsociation) {
        this.medicalAsociation = medicalAsociation;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public List<String> getNameMedicalOffice() {
        return nameMedicalOffice;
    }

    public void setNameMedicalOffice(List<String> nameMedicalOffice) {
        this.nameMedicalOffice = nameMedicalOffice;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }

    public List<Speciality> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<Speciality> specialityList) {
        this.specialityList = specialityList;
    }

    @XmlTransient
    public List<BankAccounts> getBankAccountsList() {
        return bankAccountsList;
    }

    public void setBankAccountsList(List<BankAccounts> bankAccountsList) {
        this.bankAccountsList = bankAccountsList;
    }

    public List<SpecialistJob> getSpecialistJobList() {
        return specialistJobList;
    }

    public void setSpecialistJobList(List<SpecialistJob> specialistJobList) {
        this.specialistJobList = specialistJobList;
    }

    public Addresses getAddressId() {
        return addressId;
    }

    public void setAddressId(Addresses addressId) {
        this.addressId = addressId;
    }

    public TypeSpecialist getTypeSpecialistid() {
        return typeSpecialistid;
    }

    public void setTypeSpecialistid(TypeSpecialist typeSpecialistid) {
        this.typeSpecialistid = typeSpecialistid;
    }

    public List<Clinics> getClinicsList() {
        return clinicsList;
    }

    public void setClinicsList(List<Clinics> clinicsList) {
        this.clinicsList = clinicsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Specialists)) {
            return false;
        }
        Specialists other = (Specialists) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.Specialists[ id=" + id + " ]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Specialists obj = null;
        obj = (Specialists) super.clone();
        obj.addressId = obj.addressId == null ? null : (Addresses) obj.addressId.clone();

        if (obj.getBankAccountsList() == null) {
            obj.bankAccountsList = null;
        } else {
            List<BankAccounts> listaBankAccounts = new ArrayList<>();
            for (BankAccounts item : obj.getBankAccountsList()) {
                listaBankAccounts.add((BankAccounts) item.clone());
            }
            obj.bankAccountsList = listaBankAccounts;
        }

        if (obj.getSpecialityList() == null) {
            obj.specialityList = null;
        } else {
            List<Speciality> listaSpeciality = new ArrayList<>();
            for (Speciality item : obj.getSpecialityList()) {
                listaSpeciality.add((Speciality) item.clone());
            }
            obj.specialityList = listaSpeciality;
        }

        obj.typeSpecialistid = obj.typeSpecialistid == null ? null : (TypeSpecialist) obj.typeSpecialistid.clone();

        return obj;
    }

    @Override
    public int compareTo(Object o) {
        Specialists otroEspecialista = (Specialists) o;
        //podemos hacer esto porque String implementa Comparable
        String c1;
        String c2;
        if (addressId == null) {
            c1 = "";
        } else {
            try {
                c1 = addressId.getCityId().getStateId().getNombre();
            } catch (java.lang.NullPointerException e) {
                c1 = "";
            }
        }
        if (otroEspecialista.getAddressId() == null) {
            c2 = "";
        } else {
            try {
                c2 = otroEspecialista.getAddressId().getCityId().getStateId().getNombre();
            } catch (java.lang.NullPointerException e) {
                c2 = "";
            }
        }

        return c1.compareTo(c2);

    }

    public Scale getScale() {
        return scale;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    @XmlTransient
    public List<ScaleTreatmentsSpecialist> getScaleTreatmentsSpecialistList() {
        return scaleTreatmentsSpecialistList;
    }

    public void setScaleTreatmentsSpecialistList(List<ScaleTreatmentsSpecialist> scaleTreatmentsSpecialistList) {
        this.scaleTreatmentsSpecialistList = scaleTreatmentsSpecialistList;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public Integer getTypeSpecialist_id() {
        return typeSpecialist_id;
    }

    public void setTypeSpecialist_id(Integer typeSpecialist_id) {
        this.typeSpecialist_id = typeSpecialist_id;
    }

    public String getSpecialitys() {
        return specialitys;
    }

    public void setSpecialitys(String specialitys) {
        this.specialitys = specialitys;
    }

    public Integer getIdTypeSpeciality() {
        return idTypeSpeciality;
    }

    public void setIdTypeSpeciality(Integer idTypeSpeciality) {
        this.idTypeSpeciality = idTypeSpeciality;
    }
}