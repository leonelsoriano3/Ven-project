package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.InsertPlantTreatmentKeyIn;
import com.venedental.dto.managementSpecialist.InsertPlantTreatmentKeyOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 * updated by luis.carrasco@arkiteck.biz on 14/10/2016
 */
public class InsertPlantTreamentKeyDAO extends AbstractQueryDAO<InsertPlantTreatmentKeyIn,InsertPlantTreatmentKeyOut> {

    public InsertPlantTreamentKeyDAO(DataSource dataSource) {
        super(dataSource,"SP_T_PLANTREAKEYS_I_001",8);
    }


    @Override
    public void prepareInput(InsertPlantTreatmentKeyIn input, CallableStatement statement) throws SQLException {
       statement.setInt(1, input.getIdKey());
        statement.setInt(2, input.getIdPlanTreatment());
        if (input.getDateApplication() != null) {
        statement.setDate(3, new Date(input.getDateApplication().getTime()));
        }else{
            statement.setNull(3, java.sql.Types.NULL);
        }
        statement.setInt(4, input.getIdStatus());
        statement.setInt(5, input.getIdScaleTreatments());
        statement.setString(6, input.getObservation());
        statement.setInt(7, input.getIdUser());
        if (input.getDateReception() != null) {
            statement.setDate(8, new Date(input.getDateReception().getTime()));
        } else {
            statement.setNull(8, java.sql.Types.NULL);
        }

    }

    @Override
    public InsertPlantTreatmentKeyOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        InsertPlantTreatmentKeyOut insertPlantTreatmentKeyOut = new InsertPlantTreatmentKeyOut();
        insertPlantTreatmentKeyOut.setIdTreatmentKey(rs.getInt("id"));
        insertPlantTreatmentKeyOut.setIdTreatmentKeyDiagnosis(rs.getInt("id_status"));

        return insertPlantTreatmentKeyOut;
    }



}
