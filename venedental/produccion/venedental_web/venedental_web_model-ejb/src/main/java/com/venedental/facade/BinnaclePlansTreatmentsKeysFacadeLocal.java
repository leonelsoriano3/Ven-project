/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnaclePlansTreatmentsKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESKDEV90
 */
@Local
public interface BinnaclePlansTreatmentsKeysFacadeLocal {    
    
    BinnaclePlansTreatmentsKeys create(BinnaclePlansTreatmentsKeys binnaclePlansTreatmentsKeys);

    BinnaclePlansTreatmentsKeys edit(BinnaclePlansTreatmentsKeys binnaclePlansTreatmentsKeys);

    void remove(BinnaclePlansTreatmentsKeys statusAnalysisKeys);

    BinnaclePlansTreatmentsKeys find(Object id);

    List<BinnaclePlansTreatmentsKeys> findAll();

    List<BinnaclePlansTreatmentsKeys> findRange(int[] range);

    int count();
    
    List<BinnaclePlansTreatmentsKeys> findSpecialistToAudit(Integer statusId, Date dateFrom, Date dateUntil);
    
    
}
