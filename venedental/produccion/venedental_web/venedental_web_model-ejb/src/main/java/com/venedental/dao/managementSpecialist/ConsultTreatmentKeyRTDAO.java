/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultTreatmentsKeyRTOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/** 
 *
 * @author akdesk01
 */
public class ConsultTreatmentKeyRTDAO extends AbstractQueryDAO<Integer, ConsultTreatmentsKeyRTOutput>{
    
    
    public ConsultTreatmentKeyRTDAO(DataSource dataSource) {
        super(dataSource, "SP_T_TREATMENTS_G_005", 1);
    }
    
   @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public ConsultTreatmentsKeyRTOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultTreatmentsKeyRTOutput consultTreatmentsKeyRTOutput = new ConsultTreatmentsKeyRTOutput();
        consultTreatmentsKeyRTOutput.setId_baremo(rs.getInt("id_baremo"));
        consultTreatmentsKeyRTOutput.setId_categoria(rs.getInt("id_categoria"));
        consultTreatmentsKeyRTOutput.setId_plan_tratamiento(rs.getInt("id_plan_tratamiento"));
        consultTreatmentsKeyRTOutput.setId_tratamiento(rs.getInt("id_tratamiento"));
        consultTreatmentsKeyRTOutput.setId_tratamiento_clave(rs.getInt("id_tratamiento_clave"));
        consultTreatmentsKeyRTOutput.setNombre_categoria(rs.getString("nombre_categoria"));
        consultTreatmentsKeyRTOutput.setNombre_tratamiento(rs.getString("nombre_tratamiento"));
        consultTreatmentsKeyRTOutput.setTratamiento_registrado(rs.getInt("tratamiento_registrado"));
        return consultTreatmentsKeyRTOutput;
    } 
}
