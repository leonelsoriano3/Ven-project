/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.PaymentRefund;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class GenerateTxtInput {
    Integer id_paciente, id_estatus;
    Date start_date, end_date;
    
    public GenerateTxtInput() {
    }
    
    public GenerateTxtInput(Integer id_paciente, Integer id_estatus, Date start_date, Date end_date) {
        this.id_paciente=id_paciente;
        this.id_estatus=id_estatus;
        this.start_date=start_date;
        this.end_date=end_date;
    }

    public Integer getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(Integer id_paciente) {
        this.id_paciente = id_paciente;
    }

    public Integer getId_estatus() {
        return id_estatus;
    }

    public void setId_estatus(Integer id_estatus) {
        this.id_estatus = id_estatus;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
    
    
}
