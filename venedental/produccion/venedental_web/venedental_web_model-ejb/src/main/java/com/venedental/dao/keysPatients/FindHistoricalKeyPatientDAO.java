/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.HistoricalPatientInput;
import com.venedental.dto.keyPatients.HistoricalPatientOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindHistoricalKeyPatientDAO  extends AbstractQueryDAO<HistoricalPatientInput, HistoricalPatientOutput> {
    
     public FindHistoricalKeyPatientDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_HistoricoClavesPaciente", 2);
    }

    @Override
    public void prepareInput(HistoricalPatientInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPatient());   
        statement.setInt(2, input.getIdKeyPatient());  
    }

    @Override
    public HistoricalPatientOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        HistoricalPatientOutput historicalPatientOutput = new HistoricalPatientOutput();
        historicalPatientOutput.setIdPatient(rs.getInt("ID_PACIENTE"));
        historicalPatientOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
        historicalPatientOutput.setIdKeyPatient(rs.getInt("ID_CLAVE"));
        historicalPatientOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
        historicalPatientOutput.setIdPlanTreatmentKey(rs.getInt("ID_TRATAMIENTO_CLAVE"));
        historicalPatientOutput.setIdTreatment(rs.getInt("ID_TRATAMIENTO"));
        historicalPatientOutput.setNameTratment(rs.getString("NOMBRE_TRATAMIENTO"));
        historicalPatientOutput.setIdPropertiePlantreatmentKey(rs.getInt("ID_PIEZA_TRATAMIENTO_CLAVE"));
        historicalPatientOutput.setNumberPropertie(rs.getString("NUMERO_PIEZA"));
        historicalPatientOutput.setIdStatus(rs.getInt("ID_ESTATUS"));
        historicalPatientOutput.setNameStatus(rs.getString("NOMBRE_ESTATUS"));
        historicalPatientOutput.setDateApplication(rs.getDate("FECHA_EJECUCION"));
        historicalPatientOutput.setFechaPrueba(rs.getString("FECHA_EJECUCION"));
        historicalPatientOutput.setObservation(rs.getString("OBSERVACIONES"));
        historicalPatientOutput.setIdSpecialist(rs.getInt("id_especialista"));
        historicalPatientOutput.setNameSpecialist(rs.getString("nombre_especialista"));
        historicalPatientOutput.setPrice(rs.getDouble("price"));
        historicalPatientOutput.setNameHealthArea(rs.getString("nombre_area_salud"));
        return historicalPatientOutput;
    }
    
}










