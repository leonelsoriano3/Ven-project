/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.baremo.ConfigBaremoDateMinFilterDAO;
import com.venedental.dao.consultKeys.*;
import com.venedental.dao.keysPatients.KeysConsultDateMinDAO;
import com.venedental.dao.keysPatients.DateminHistoricoDAO;
import com.venedental.dao.keysPatients.*;
import com.venedental.dao.managementSpecialist.ConsultHistorialDAO;
import com.venedental.dto.consultKeys.*;
import com.venedental.dto.keyPatients.*;
import com.venedental.dto.keyPatients.DateMinHistoricoInput;
import com.venedental.dto.analysisKeys.FindKeysToAuditBySpecialistAndDateInput;
import com.venedental.dto.keyPatients.DateMinHistoricoOutput;
import com.venedental.dto.keyPatients.KeysConsultDateMinOutput;
import com.venedental.dto.managementSpecialist.ConsultHistorialInput;
import com.venedental.dto.managementSpecialist.ConsultHistorialOutput;
import com.venedental.model.KeysPatients;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServiceKeysPatients {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permites buscar las claves a auditar dado un especialista y fecha
     *
     * @param idSpecialist
     * @param idStatus
     * @param dateFrom
     * @param dateUntil
     * @return
     */
    public List<KeysPatients> findKeysToAuditBySpecialistAndDate(Integer idSpecialist, Integer idStatus, Date dateFrom,
                                                                 Date dateUntil) {
        FindKeysToAuditBySpecialistAndDateDAO findKey = new FindKeysToAuditBySpecialistAndDateDAO(ds);
        FindKeysToAuditBySpecialistAndDateInput input = new FindKeysToAuditBySpecialistAndDateInput(idSpecialist,
                idStatus, dateFrom, dateUntil);
        findKey.execute(input);
        return findKey.getResultList();
    }

    /**
     * Servicio que permite actualizar el estado de una clave y su bitacora
     *
     * @param idKeyPatient
     * @param idStatus
     * @param idUser
     * @throws SQLException
     */
    public void updateStatusKeyPatient(int idKeyPatient, int idStatus, int idUser) throws SQLException {
        UpdateKeyPatientStatusDAO updatePlanKeyPatinentStatusDAO = new UpdateKeyPatientStatusDAO(ds);
        UpdateKeyPatientStatusInput input = new UpdateKeyPatientStatusInput(idKeyPatient, idStatus, idUser);
        updatePlanKeyPatinentStatusDAO.execute(input);
    }

    /**
     * Servicio que permite buscar las claves que tiene un especialista por dos estados de la clave.
     *
     * @param idSpecialist
     * @param idStatus
     * @param idStatus2
     * @param idStatus3
     * @return
     */
    public List<KeysPatients> findKeysBySpecialistAndStatus(Integer idSpecialist, Integer idStatus, Integer idStatus2,
                                                            Integer idStatus3, Integer idUser) {
        FindKeysBySpecialistAndStatusDAO findKeys = new FindKeysBySpecialistAndStatusDAO(ds);
        KeysBySpecialistAndStatusInput input = new KeysBySpecialistAndStatusInput(idSpecialist, idStatus, idStatus2,
                idStatus3, idUser);
        findKeys.execute(input);
        return findKeys.getResultList();
    }

    /**
     * Servicio que permite buscar el historial de claves de un paciente
     *
     * @param idPatient
     * @param idKey
     * @return
     */
    public List<HistoricalPatientOutput> findHistoricalPatient(Integer idPatient, Integer idKey) {
        FindHistoricalKeyPatientDAO findKeysBySpecialistAndStatusDAO = new FindHistoricalKeyPatientDAO(ds);
        HistoricalPatientInput historicalPatientInput = new HistoricalPatientInput(idPatient, idKey, null);
        findKeysBySpecialistAndStatusDAO.execute(historicalPatientInput);
        return findKeysBySpecialistAndStatusDAO.getResultList();
    }



    public List<ConsultKeysPatientsOutput> consultKeysPatients(String dateStart, String dateEnd, String numberForKey,String patient,String Specialist,Integer idStatusKey) {
        ConsultKeysPatientsDAO consultKeysPatientsDAO = new ConsultKeysPatientsDAO(ds);
        ConsultKeysPatientsInput input = new ConsultKeysPatientsInput();
        input.setDateStart(dateStart);
        input.setDateEnd(dateEnd);
        input.setNumberForKey(numberForKey);
        input.setPatient(patient);
        input.setSpecialist(Specialist);
        input.setIdstatusKey(idStatusKey);
        consultKeysPatientsDAO.execute(input);
        return consultKeysPatientsDAO.getResultList();
    }

    public List<ConsultKeysPatientsDetailOutput> detailsConsultKeysPatients(Integer key) {
        ConsultKeysPatientsDetailsDAO consultKeysPatientsDetailsDAO = new ConsultKeysPatientsDetailsDAO(ds);
        consultKeysPatientsDetailsDAO.execute(key);
        return consultKeysPatientsDetailsDAO.getResultList();
    }

    public List<ConsultKeysPatientsDetailTreatmentOutput> detailsConsultKeysPatientsTreatments(Integer key) {
        ConsultKeysPatientsDetailsTreatmentDAO consultKeysPatientsDetailsTreatmentDAO = new ConsultKeysPatientsDetailsTreatmentDAO(ds);
        consultKeysPatientsDetailsTreatmentDAO.execute(key);
        return consultKeysPatientsDetailsTreatmentDAO.getResultList();
    }

    public List<ConsultUserRolesOutput> searchUserRoles(Integer idUser){
        ConsultUserRolesDAO consultUserRolesDAO= new ConsultUserRolesDAO(ds);
        consultUserRolesDAO.execute(idUser);
        return consultUserRolesDAO.getResultList();

    }

    public void reasonDeleteKey(Integer idKey, String observation, Integer idUser) throws SQLException{
        ReasonDeleteKeyDAO reasonDeleteKeyDAO=new ReasonDeleteKeyDAO(ds);
        ReasonDeleteKeyInput input= new ReasonDeleteKeyInput();
        input.setIdKey(idKey);
        input.setObservation(observation);
        input.setIdUser(idUser);
        reasonDeleteKeyDAO.execute(input);
    }


    /**
     * Servicio que permite buscar el historial de claves de un paciente y especialista(especialidad)
     *
     * @param idPatient
     * @param idSpecialista
     * @return
     */
    public List<HistoricalDetailsPatientOutput> findHistoricalDetailsPatient(Integer idPatient,Integer idSpecialista) {
        FindHistoricalDetailsKeyPatientDAO findHistoricalDetailsKeyPatientDAO = new FindHistoricalDetailsKeyPatientDAO(ds);
        HistoricalDetailsPatientInput input = new HistoricalDetailsPatientInput(idPatient,idSpecialista);
        findHistoricalDetailsKeyPatientDAO.execute(input);
        return findHistoricalDetailsKeyPatientDAO.getResultList();
    }

    /**
     * Servicio que permite buscar claves por un rango de fecha
     *
     * @param idStatus
     * @param dateFrom
     * @param dateUntil
     * @return
     */
    public List<KeysByParametersOutput> findByDateRangeAndStatus (Integer idStatus, Date dateFrom, Date dateUntil){
        FindKeysByParametersDAO findKeysByParametersDAO = new FindKeysByParametersDAO(ds);
        KeysByParametersInput input = new KeysByParametersInput(null, null,null,null,dateFrom,dateUntil,idStatus);
        findKeysByParametersDAO.execute(input);
        return findKeysByParametersDAO.getResultList();
    }


    /**
     * Servicio que permite cargar los valores en el combo estado de la clave.
     * @param parameter
     * @return
     */

    public List<LoadKeyStatesOutput> statesOfKey(Integer parameter){
        LoadKeyStatesDAO loadKeyStatesDAO=new LoadKeyStatesDAO(ds);
        loadKeyStatesDAO.execute(parameter);
        return loadKeyStatesDAO.getResultList();
    }

    /**
     * Servicio que permite buscar claves por un rango de fecha y id del especialista
     *
     * @param dateFrom
     * @param dateUntil
     * @param idSpecialist
     * @param idStatus
     * @return
     */
    public List<KeysByParametersOutput> findByDateSpecialistAndStatus (Date dateFrom, Date dateUntil,
                                                                       Integer idSpecialist, Integer idStatus){
        FindKeysByParametersDAO findKeysByParametersDAO = new FindKeysByParametersDAO(ds);
        KeysByParametersInput input = new KeysByParametersInput(null, null,null,idSpecialist,dateFrom,dateUntil,idStatus);
        findKeysByParametersDAO.execute(input);
        return findKeysByParametersDAO.getResultList();
    }


    /**
     * Servicio que permite generar el numero de una clave
     * @param idPlan
     * @return
     */
    public String generateNumber (Integer idPlan){
        GenerateNumberKeyDAO generateNumberKeyDAO = new GenerateNumberKeyDAO(ds);
        generateNumberKeyDAO.execute(idPlan);
        return generateNumberKeyDAO.getResult();
    }

    /**
     * Servicio que permite buscar el detalle de una clave dado su id
     * @param idKey
     * @return
     */
    public KeysDetailsOutput findDetails (Integer idKey){
        KeysDetailsDAO keysDetailsDAO = new KeysDetailsDAO(ds);
        keysDetailsDAO.execute(idKey);
        return keysDetailsDAO.getResult();
    }



    public List<RegisterKeyOutput> registerKey (String number, String typeAttention, Integer idPatient, Integer idSpecialist,
                                                Integer idUser, Integer idMedicalOffice, Integer idCompany, Integer idPlan){
        RegisterKeyDAO registerKey = new RegisterKeyDAO(ds);
        RegisterKeyInput registerKeyInput = new RegisterKeyInput(number,typeAttention,idPatient,idSpecialist,idUser,
                idMedicalOffice, idCompany,idPlan);
        registerKey.execute(registerKeyInput);
        return registerKey.getResultList();
    }

    /**
     * Servicio que permite buscar el historial de claves de un paciente
     *
     * @param idPatient
     * @param idSpecialist
     * @return
     */
    public List<HistoricalPatientGCOutput> findHistoricalPatientRegister(Integer idPatient, Integer idSpecialist) {
        FindHistoricalKeyPatientRegisterDAO historical = new FindHistoricalKeyPatientRegisterDAO(ds);
        HistoricalPatientInput historicalPatientInput = new HistoricalPatientInput(idPatient, null, idSpecialist);
        historical.execute(historicalPatientInput);
        return historical.getResultList();
    }
      
      
    
    public KeysConsultDateMinOutput findDateMin(Integer parameter){
        KeysConsultDateMinDAO keysConsultDateMinDAO = new KeysConsultDateMinDAO(ds);
        keysConsultDateMinDAO.execute(parameter);
        return keysConsultDateMinDAO.getResult();
    }


    public List<DateMinHistoricoOutput> findDateMinHistorico(Integer idPatient, Integer idSpecialist){
        DateminHistoricoDAO dateminHistoricoDAO = new DateminHistoricoDAO(ds);
        DateMinHistoricoInput dateMinHistoricoInput= new DateMinHistoricoInput(idPatient,idSpecialist);
        dateminHistoricoDAO.execute(dateMinHistoricoInput);
        return dateminHistoricoDAO.getResultList();
    }

    public List<ConsultDateMinDetalleOutput> findDateMinDetalle(Integer idKey){
        ConsultDateMinDetalleDAO consultDateMinDetalleDAO = new ConsultDateMinDetalleDAO(ds);
        consultDateMinDetalleDAO.execute(idKey);
        return consultDateMinDetalleDAO.getResultList();
    }
   
    public List<ConsultDateDetailTreatmentOutput> findDateMinYMaxDetalle(Integer idKey){
       ConsultDatePatientsDetailsTreatmentDAO consultDatePatientsDetailsTreatmentDAO = new ConsultDatePatientsDetailsTreatmentDAO(ds) {};
        consultDatePatientsDetailsTreatmentDAO.execute(idKey);
        return consultDatePatientsDetailsTreatmentDAO.getResultList();
    }
    
    
     /**
     * Servicio que permite buscar la cabecera del correo que se envia al especialista
     *
     * @param idKey
     * @return
     */
    public List<HeadboardEmailOutput> findCabeceraEmail(Integer idKey) {
        HeadboardEmailDAO headboardEmailDAO = new HeadboardEmailDAO(ds);
        headboardEmailDAO.execute(idKey);
        return headboardEmailDAO.getResultList();
    }
    
    
    /**
     * Servicio que permite buscar los tratamientos por plan del correo que se envia al especialista
     *
     * @param idPlan
     * @return
     */
    public List<ReadTreatmentsEmailOutput> findTreatmentsEmail(Integer idPlan) {
        ReadTreatmentsEmailDAO readTreatmentsEmailDAO = new ReadTreatmentsEmailDAO(ds);
        readTreatmentsEmailDAO.execute(idPlan);
        return readTreatmentsEmailDAO.getResultList();
    }



}
