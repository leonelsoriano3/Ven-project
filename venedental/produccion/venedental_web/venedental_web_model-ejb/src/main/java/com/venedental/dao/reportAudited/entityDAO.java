/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.reportAudited;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.CreatePaymentReportTreatmentsInput;
import com.venedental.dto.reportAudited.EntityOutputDTO;
import com.venedental.dto.reportAudited.StateOutputDTO;
import com.venedental.model.PlansTreatments;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *           
 * @author akdesk10
 */
public class entityDAO extends AbstractQueryDAO<String, EntityOutputDTO> {
    
      public entityDAO(DataSource dataSource) {
        super(dataSource, "SP_T_COMPANY_G_002", 1);  
      }
      
     @Override
     public void prepareInput(String input, CallableStatement statement) throws SQLException {
            if(input==null){
              statement.setNull(1, java.sql.Types.NULL);
            }
            else{
               statement.setString(1, input);   
        }   
            
       }
     
    @Override
    public EntityOutputDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        EntityOutputDTO entity = new EntityOutputDTO();
        entity.setIdEntity(rs.getInt("id_ente"));
        entity.setNameEntity(rs.getString("nombre_ente"));
        return entity;
    }
}
