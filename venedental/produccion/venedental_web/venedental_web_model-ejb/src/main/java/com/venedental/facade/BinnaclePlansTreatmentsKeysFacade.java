/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnaclePlansTreatmentsKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author AKDESKDEV90
 */
@Stateless
public class BinnaclePlansTreatmentsKeysFacade extends AbstractFacade<BinnaclePlansTreatmentsKeys> implements BinnaclePlansTreatmentsKeysFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BinnaclePlansTreatmentsKeysFacade() {
        super(BinnaclePlansTreatmentsKeys.class);
    }
    
    @Override
    public List<BinnaclePlansTreatmentsKeys> findSpecialistToAudit(Integer statusId, Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_ESPECIALISTAS_AUDITAR", BinnaclePlansTreatmentsKeys.class);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
        query.setParameter("PI_ID_ESTATUS", statusId);
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);
        return query.getResultList();
    }
    
}
