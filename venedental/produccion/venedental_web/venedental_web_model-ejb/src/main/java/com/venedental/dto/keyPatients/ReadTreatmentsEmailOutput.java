/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

/**
 *
 * @author akdesk01
 */
public class ReadTreatmentsEmailOutput {
    
    private Integer idLine;
    private String lineTreatments;
    private Integer idColumn;

    public ReadTreatmentsEmailOutput() {
    }

    public Integer getIdLine() {
        return idLine;
    }

    public void setIdLine(Integer idLine) {
        this.idLine = idLine;
    }

    public String getLineTreatments() {
        return lineTreatments;
    }

    public void setLineTreatments(String lineTreatments) {
        this.lineTreatments = lineTreatments;
    }

    

    public Integer getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(Integer idColumn) {
        this.idColumn = idColumn;
    }
    
    
    
}
