/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import java.sql.Date;

/**
 *
 * @author root
 */
public class ConsultTotalBodyRetentionOutput {

    private Double amountISLR;
    private Date dateISLR;
    private String companyName;
    private String companyAddress;
    private String companyRif;
    private String specialistName;
    private String specialistAddress;
    private String retentionConcept;
    private Double totalAmountTotal;
    private Double totalBaseRetention;
    private Double totalAmountRetentionISLR;
    private Double netPay;
    private String numberRetentionISLR;
    private String specialistRif;
    private Double totalMontoSustraendo;
    private Double totalNetoRetention;
    
    public ConsultTotalBodyRetentionOutput() {
    }

    public ConsultTotalBodyRetentionOutput(Double amountISLR, Date dateISLR, String companyName, String companyAddress, String companyRif, String specialistName, String specialistAddress, String retentionConcept, Double totalAmountTotal, Double totalBaseRetention, Double totalAmountRetentionISLR, Double netPay, String numberRetentionISLR, String specialistRif,Double totalMontoSustraendo, Double totalNetoRetention) {
        this.amountISLR = amountISLR;
        this.dateISLR = dateISLR;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyRif = companyRif;
        this.specialistName = specialistName;
        this.specialistAddress = specialistAddress;
        this.retentionConcept = retentionConcept;
        this.totalAmountTotal = totalAmountTotal;
        this.totalBaseRetention = totalBaseRetention;
        this.totalAmountRetentionISLR = totalAmountRetentionISLR;
        this.netPay = netPay;
        this.numberRetentionISLR = numberRetentionISLR;
        this.specialistRif = specialistRif;
        this.totalMontoSustraendo = totalMontoSustraendo;
        this.totalNetoRetention = totalNetoRetention;
    }

    public Double getTotalNetoRetention() {
        return totalNetoRetention;
    }

    public void setTotalNetoRetention(Double totalNetoRetention) {
        this.totalNetoRetention = totalNetoRetention;
    }
    
    public Double getTotalMontoSustraendo() {
        return totalMontoSustraendo;
    }

    public void setTotalMontoSustraendo(Double totalMontoSustraendo) {
        this.totalMontoSustraendo = totalMontoSustraendo;
    }
    
    
    public Double getAmountISLR() {
        return amountISLR;
    }

    public void setAmountISLR(Double amountISLR) {
        this.amountISLR = amountISLR;
    }

    public Date getDateISLR() {
        return dateISLR;
    }

    public void setDateISLR(Date dateISLR) {
        this.dateISLR = dateISLR;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyRif() {
        return companyRif;
    }

    public void setCompanyRif(String companyRif) {
        this.companyRif = companyRif;
    }

    public String getSpecialistName() {
        return specialistName;
    }

    public void setSpecialistName(String specialistName) {
        this.specialistName = specialistName;
    }

    public String getSpecialistAddress() {
        return specialistAddress;
    }

    public void setSpecialistAddress(String specialistAddress) {
        this.specialistAddress = specialistAddress;
    }

    public String getRetentionConcept() {
        return retentionConcept;
    }

    public void setRetentionConcept(String retentionConcept) {
        this.retentionConcept = retentionConcept;
    }

    public Double getTotalAmountTotal() {
        return totalAmountTotal;
    }

    public void setTotalAmountTotal(Double totalAmountTotal) {
        this.totalAmountTotal = totalAmountTotal;
    }

    public Double getTotalBaseRetention() {
        return totalBaseRetention;
    }

    public void setTotalBaseRetention(Double totalBaseRetention) {
        this.totalBaseRetention = totalBaseRetention;
    }

    public Double getTotalAmountRetentionISLR() {
        return totalAmountRetentionISLR;
    }

    public void setTotalAmountRetentionISLR(Double totalAmountRetentionISLR) {
        this.totalAmountRetentionISLR = totalAmountRetentionISLR;
    }

    public Double getNetPay() {
        return netPay;
    }

    public void setNetPay(Double netPay) {
        this.netPay = netPay;
    }

    public String getNumberRetentionISLR() {
        return numberRetentionISLR;
    }

    public void setNumberRetentionISLR(String numberRetentionISLR) {
        this.numberRetentionISLR = numberRetentionISLR;
    }

    public String getSpecialistRif() {
        return specialistRif;
    }

    public void setSpecialistRif(String specialistRif) {
        this.specialistRif = specialistRif;
    }
    
    
    
}
