package com.venedental.services;

import com.venedental.dao.employee.FindEmployeeByIdUserDAO;
import com.venedental.dto.emplooyes.EmployeeDTO;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 * Created by luis.carrasco@arkiteck.biz on 15/06/16.
 */
@Stateless
public class ServiceEmployee {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    public EmployeeDTO findEmployeeByUserid(Integer userId){

        FindEmployeeByIdUserDAO findEmployeeByIdUserDAO = new FindEmployeeByIdUserDAO(ds);
        findEmployeeByIdUserDAO.execute(userId);
        return findEmployeeByIdUserDAO.getResult();

    }
}
