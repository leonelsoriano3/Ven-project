/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.Menu;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Anzola
 */
@Local
public interface MenuFacadeLocal {

    Menu create(Menu menu);

    Menu edit(Menu menu);

    void remove(Menu menu);

    Menu find(Object id);
    
    Menu findById (Integer id);
    
    Menu findByName (String name);

    List<Menu> findAll();

    List<Menu> findRange(int[] range);

    int count();

    String countByFather(int father);

    List<Menu> findByFather(int father);
    
    List<Menu> findByUser(Integer user);
    
    List<Menu> findSonByUserAndFather(Integer user,Integer father);
    
    

}
