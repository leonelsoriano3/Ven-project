package com.venedental.dto.reportAuditConsultTrace;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.List;

/**
 * Created by luis.carrasco@arkiteck.biz on 16/11/16.
 */
public class PaymentReportDataSource implements JRDataSource {

    private int index = -1;
    private final List<ReportPaymentDetailsOutput> listRep;

    public PaymentReportDataSource(List<ReportPaymentDetailsOutput> listRep) {
        this.listRep = listRep;
    }
    @Override
    public boolean next() throws JRException {
        System.out.println("\t\t" + (index + 1) + "\t");
        return ++index < listRep.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        ReportPaymentDetailsOutput to = listRep.get(index);
        System.out.print("\t\t" +jrf.getName());
        switch (jrf.getName()){
            case "numberKey":
                return stringValido(to.getNumKey());
            case "dateTreatment":
                return stringValido(to.getDate());
            case "cardIdPatient":
                return stringValido(to.getCardIdPatient());
            case "namePatient":
                return stringValido(to.getNamePatient());
            case "nameProperties":
                return stringValido(to.getNameTooth());
            case "observation":
                return stringValido(to.getObservation());
            case "nameStatus":
                return stringValido(to.getStatusTooth());
            case "amountPaid":
                return stringValido(String.valueOf(to.getBaremo()));
            case "nameTreatments":
                return stringValido(to.getNameTreatment());

        }
        return null;
    }

    private String stringValido(String str){
        if (str != null){
            return str.trim();
        }
        return "";
    }
}
