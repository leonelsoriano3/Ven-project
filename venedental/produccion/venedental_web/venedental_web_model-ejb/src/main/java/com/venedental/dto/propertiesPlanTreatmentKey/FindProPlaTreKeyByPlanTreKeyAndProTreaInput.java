/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.propertiesPlanTreatmentKey;

import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindProPlaTreKeyByPlanTreKeyAndProTreaInput {
    
    private Integer idPlanTreatmentKey;
    private Integer idPropertieTreatment;
    private Integer idStatus;

    public FindProPlaTreKeyByPlanTreKeyAndProTreaInput(Integer idPlanTreatmentKey, Integer idPropertieTreatment, Integer idStatus) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.idPropertieTreatment = idPropertieTreatment;
        this.idStatus = idStatus;
    }

    public FindProPlaTreKeyByPlanTreKeyAndProTreaInput(DataSource ds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getIdPropertieTreatment() {
        return idPropertieTreatment;
    }

    public void setIdPropertieTreatment(Integer idPropertieTreatment) {
        this.idPropertieTreatment = idPropertieTreatment;
    }    

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }
        
}
