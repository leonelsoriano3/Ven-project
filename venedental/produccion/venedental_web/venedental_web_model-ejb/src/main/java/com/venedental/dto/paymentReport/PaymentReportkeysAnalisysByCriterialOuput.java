package com.venedental.dto.paymentReport;

import java.util.Date;

/**
 * Clase para mostrar la salida de datos para los reportes segun el criterio,
 * para el desarrollo del componente del CUS_013_Reporte_Pagos.
 * 
 *
 * @author David Rivero
 */
public class PaymentReportkeysAnalisysByCriterialOuput {

    /* Attributes */
    
    
    private int paymentReporId;
    private String paymentReportRefundNumber;
    private Date paymentReportRefundDate;
    private int specialistId;
    private String completeNameSpecialists;
    private String identityNumberRIFSpecialistsClinic;
    private String numberInvoices;
    private String controlNumber;
    private Date invoicesDate;
    private String numberTransfer;
    private Date TransferDate;
    private Double islr_rate;
    private Double amountRetention;
    private Double amountInvoices;
    private Double netAmountInvoices;
    
    /* Contructs */
    public PaymentReportkeysAnalisysByCriterialOuput() {
    }

    public PaymentReportkeysAnalisysByCriterialOuput(int paymentReporId, String paymentReportRefundNumber, Date paymentReportRefundDate, int specialistId, String completeNameSpecialists, String identityNumberRIFSpecialistsClinic, String numberInvoices, String controlNumber, Date invoicesDate, String numberTransfer, Date TransferDate, Double islr_rate, Double amountRetention, Double amountInvoices, Double netAmountInvoices) {
        this.paymentReporId = paymentReporId;
        this.paymentReportRefundNumber = paymentReportRefundNumber;
        this.paymentReportRefundDate = paymentReportRefundDate;
        this.specialistId = specialistId;
        this.completeNameSpecialists = completeNameSpecialists;
        this.identityNumberRIFSpecialistsClinic = identityNumberRIFSpecialistsClinic;
        this.numberInvoices = numberInvoices;
        this.controlNumber = controlNumber;
        this.invoicesDate = invoicesDate;
        this.numberTransfer = numberTransfer;
        this.TransferDate = TransferDate;
        this.islr_rate = islr_rate;
        this.amountRetention = amountRetention;
        this.amountInvoices = amountInvoices;
        this.netAmountInvoices = netAmountInvoices;
    }

   
    
    
    /* Getter and Setter */

    /**
     *  Id del reporte de pago
     * 
     * @return 
     */
    public int getPaymentReporId() {
        return paymentReporId;
    }

    /**
     *  Envio de dato al reporte de pago
     * 
     * @param paymentReporId 
     */
    public void setPaymentReporId(int paymentReporId) {
        this.paymentReporId = paymentReporId;
    }

    /**
     * Obtener el numero de reporte de pago
     * 
     * @return
     */
    public String getPaymentReportRefundNumber() {
        return paymentReportRefundNumber;
    }

    /**
     * almacenar datos correspondiente al reporte de pago
     * 
     * @param paymentReportRefundNumber
     */
    public void setPaymentReportRefundNumber(String paymentReportRefundNumber) {
        this.paymentReportRefundNumber = paymentReportRefundNumber;
    }
    
    /**
     * obtener el valor de fecha de reporte de pago
     * 
     * @return 
     */
    public Date getPaymentReportRefundDate() {
        return paymentReportRefundDate;
    }
    
    /**
     *  almacenar un valor a la fecha del reporte de pago
     * 
     * @param paymentReportRefundDate 
     */
    public void setPaymentReportRefundDate(Date paymentReportRefundDate) {
        this.paymentReportRefundDate = paymentReportRefundDate;
    }
    
    /**
     *  obtener el valor del id del especialista
     * 
     * @return 
     */
    public int getSpecialistId() {
        return specialistId;
    }
    
    /**
     * almacenar un valir en el id del espcialista
     * 
     * @param specialistId 
     */
    public void setSpecialistId(int specialistId) {
        this.specialistId = specialistId;
    }
    
    /**
     * obtener el nombre completo del especialista o clinica
     * 
     * @return 
     */
    public String getCompleteNameSpecialists() {
        return completeNameSpecialists;
    }

    /**
     * almacenar el nombre completo del especialista
     * 
     * @param completeNameSpecialists 
     */
    public void setCompleteNameSpecialists(String completeNameSpecialists) {
        this.completeNameSpecialists = completeNameSpecialists;
    }

    /**
     * obtener el valor del rif o clinica asociado al especialista
     * 
     * @return 
     */
    public String getIdentityNumberRIFSpecialistsClinic() {
        return identityNumberRIFSpecialistsClinic;
    }
    
    /**
     * almacenar un valor en el campo rif o clinica asociado al especialista
     * 
     * @param identityNumberRIFSpecialistsClinic 
     */
    public void setIdentityNumberRIFSpecialistsClinic(String identityNumberRIFSpecialistsClinic) {
        this.identityNumberRIFSpecialistsClinic = identityNumberRIFSpecialistsClinic;
    }
    
    /**
     * obtener el valor del numero de factura del reporte de pago
     * 
     * @return 
     */
    public String getNumberInvoices() {
        return numberInvoices;
    }
    
    /**
     * almacenar un valor en el numero de factura del reporte de pago
     * 
     * @param numberInvoices 
     */
    public void setNumberInvoices(String numberInvoices) {
        this.numberInvoices = numberInvoices;
    }
    
    /**
     * obtener el numero de control asociado a la retencion de la factura.
     * 
     * @return 
     */
    public String getControlNumber() {
        return controlNumber;
    }
    
    /**
     * almacenar el numero de control asociado a la retencion de la factura
     * 
     * @param controlNumber 
     */
    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }
    
    /**
     * obtener el valor de la fecha de factura.
     * 
     * @return 
     */
    public Date getInvoicesDate() {
        return invoicesDate;
    }
    
    /**
     * almacenar el valor correspondiente a la fecha de factura
     * 
     * @param invoicesDate 
     */
    public void setInvoicesDate(Date invoicesDate) {
        this.invoicesDate = invoicesDate;
    }
    
    /**
     * obtener el valor del numero de transferencia asociado al reporte abonado
     * 
     * @return 
     */
    public String getNumberTransfer() {
        return numberTransfer;
    }
    
    /**
     * almacenar el valor del numero de transferencia asociado al reporte abonado
     * 
     * @param numberTransfer 
     */
    public void setNumberTransfer(String numberTransfer) {
        this.numberTransfer = numberTransfer;
    }
    
    /**
     * obtener el valor de la fecha de transferencia asociado a la factura.
     * 
     * @return 
     */
    public Date getTransferDate() {
        return TransferDate;
    }
    
    /**
     * almacenar un valor correspondiente a la fecha de transferencia.
     * 
     * @param TransferDate 
     */
    public void setTransferDate(Date TransferDate) {
        this.TransferDate = TransferDate;
    }
    
    /**
     * obtener el valor del numero de retencion asociado a la factura
     * 
     * @return 
     */
    public Double getIslr_rate() {
        return islr_rate;
    }
    
    /**
     * almacenar el valor del numero de retencion asociado a la factura
     * 
     * @param islr_rate 
     */
    public void setIslr_rate(Double islr_rate) {
        this.islr_rate = islr_rate;
    }
    
    /**
     * obtener el valor del monto de retencion asociado a la factura.
     * 
     * @return 
     */
    public Double getAmountRetention() {
        return amountRetention;
    }
    
    /**
     * almacenar el valor del monto de retencion asociado a la factura.
     * 
     * @param amountRetention 
     */
    public void setAmountRetention(Double amountRetention) {
        this.amountRetention = amountRetention;
    }
    
    /**
     * obtener el valor del monto de la factura
     * 
     * @return 
     */
    public Double getAmountInvoices() {
        return amountInvoices;
    }
    
    /**
     * almacenar el valor del monto de la factura
     * 
     * @param amountInvoices 
     */
    public void setAmountInvoices(Double amountInvoices) {
        this.amountInvoices = amountInvoices;
    }
    
    /**
     * obtener el valor del monto neto de la factura.
     * 
     * @return 
     */
    public Double getNetAmountInvoices() {
        return netAmountInvoices;
    }
    
    /**
     * almacenar el valor del monto neto de la factura.
     * 
     * @param netAmountInvoices 
     */
    public void setNetAmountInvoices(Double netAmountInvoices) {
        this.netAmountInvoices = netAmountInvoices;
    }
    
    
}
