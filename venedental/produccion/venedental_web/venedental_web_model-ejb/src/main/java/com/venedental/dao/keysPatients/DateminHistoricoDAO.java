/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.DateMinHistoricoOutput;
import com.venedental.dto.keyPatients.DateMinHistoricoInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */


public class DateminHistoricoDAO extends AbstractQueryDAO<DateMinHistoricoInput, DateMinHistoricoOutput> {
    
    public DateminHistoricoDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_MininaFechaHistorico", 2);
    }
    
    @Override
    public void prepareInput(DateMinHistoricoInput input, CallableStatement statement) throws SQLException {
         statement.setInt(1, input.getIdPatient());
        statement.setInt(2, input.getIdSpecialist());
        
      
    }
    
    @Override
    public DateMinHistoricoOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        DateMinHistoricoOutput dateMinHistoricoOutput = new DateMinHistoricoOutput();
        dateMinHistoricoOutput.setDateMin(rs.getDate("fecha_minima"));
        return dateMinHistoricoOutput;
    }

}



