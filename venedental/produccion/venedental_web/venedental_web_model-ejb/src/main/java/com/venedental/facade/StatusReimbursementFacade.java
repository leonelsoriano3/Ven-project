/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Reimbursement;
import com.venedental.model.StatusReimbursement;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author AKDESK12
 */
@Stateless
public class StatusReimbursementFacade extends AbstractFacade<StatusReimbursement> implements StatusReimbursementFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatusReimbursementFacade() {
        super(StatusReimbursement.class);
    }

    @Override
    public List<StatusReimbursement> findAllRangeStatus(int range1, int range2) {
        
        TypedQuery<StatusReimbursement> query = em.createNamedQuery("StatusReimbursement.findAllRangeStatus", StatusReimbursement.class);
        query.setParameter("range1", range1);
        query.setParameter("range2", range2);
        
        return query.getResultList();
    }
    

    @Override
    public StatusReimbursement findById(int id) {
        try {
            TypedQuery<StatusReimbursement> query = em.createNamedQuery("StatusReimbursement.findById", StatusReimbursement.class);
            query.setParameter("id", id);
            return (StatusReimbursement) query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    
}
