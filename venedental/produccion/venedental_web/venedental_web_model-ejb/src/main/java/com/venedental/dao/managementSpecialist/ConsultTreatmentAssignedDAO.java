/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultTreatmentAssignedOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultTreatmentAssignedDAO extends AbstractQueryDAO<Integer, ConsultTreatmentAssignedOutput> {
    
    public ConsultTreatmentAssignedDAO(DataSource dataSource) {
        super(dataSource, "SP_T_TREATMENTS_G_006", 1);
    }
  @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public ConsultTreatmentAssignedOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultTreatmentAssignedOutput consultTreatmentAssignedOutput = new ConsultTreatmentAssignedOutput();
        consultTreatmentAssignedOutput.setIdBaremo(rs.getInt("id_baremo"));
        consultTreatmentAssignedOutput.setIdCategory(rs.getInt("id_categoria"));
        consultTreatmentAssignedOutput.setIdPlanTreatment(rs.getInt(" id_plan_tratamiento"));
        consultTreatmentAssignedOutput.setIdTreatment(rs.getInt(" id_tratamiento"));
        consultTreatmentAssignedOutput.setIdTreatmentKey(rs.getInt("id_tratamiento_clave"));
        consultTreatmentAssignedOutput.setNameCategory(rs.getString("nombre_categoria"));
        consultTreatmentAssignedOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultTreatmentAssignedOutput.setTreatmentRegister(rs.getInt(" tratamiento_registrado"));
        return consultTreatmentAssignedOutput;
    }
    
}
