/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.insurances.FindInsurancesDAO;
import com.venedental.model.Insurances;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
@Stateless
public class ServiceInsurance {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds; 
    
    public List<Insurances> findInsurances()  {
        
        FindInsurancesDAO findInsurancesDAO = new FindInsurancesDAO(ds);
        findInsurancesDAO.execute(0);
        return findInsurancesDAO.getResultList();
        //return null;
    }
    
}
