/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planPatient;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class FilterPlanPatientOutput {
    private Integer idPatient;
    private Integer identityNumberPatient;
    private String namePatient;
    private Integer idInsurance;
    private String nameInsurance;
    private Integer idPlan;
    private Integer idLinea;
    private String namePlan;
    private Integer idRelationship;
    private Integer historial;
    private Integer idCompany;
    private String nameRelationship;
    private String nameCompany;
    private Date birthDatePatient;
    private String birthDatePatientSelect;
    private Integer IdStatusPatient;
    private String nameStatusPatient;
    private Integer IdLine;
    private String nameLine;

    public String getNameStatusPatient() {
        return nameStatusPatient;
    }

    public void setNameStatusPatient(String nameStatusPatient) {
        this.nameStatusPatient = nameStatusPatient;
    }

    public String getNameLine() {
        return nameLine;
    }

    public void setNameLine(String nameLine) {
        this.nameLine = nameLine;
    }

    public Integer getIdLine() {
        return IdLine;
    }

    public void setIdLine(Integer idLine) {
        IdLine = idLine;
    }

    public Integer getIdStatusPatient() {
        return IdStatusPatient;
    }

    public void setIdStatusPatient(Integer idStatusPatient) {
        IdStatusPatient = idStatusPatient;
    }

    public Integer getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Integer idCompany) {
        this.idCompany = idCompany;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdentityNumberPatient() {
        return identityNumberPatient;
    }

    public void setIdentityNumberPatient(Integer identityNumberPatient) {
        this.identityNumberPatient = identityNumberPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdInsurance() {
        return idInsurance;
    }

    public void setIdInsurance(Integer idInsurance) {
        this.idInsurance = idInsurance;
    }

    public String getNameInsurance() {
        return nameInsurance;
    }

    public void setNameInsurance(String nameInsurance) {
        this.nameInsurance = nameInsurance;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    }

    public Integer getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(Integer idRelationship) {
        this.idRelationship = idRelationship;
    }

    public String getNameRelationship() {
        return nameRelationship;
    }

    public void setNameRelationship(String nameRelationship) {
        this.nameRelationship = nameRelationship;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public Date getBirthDatePatient() {
        return birthDatePatient;
    }

    public void setBirthDatePatient(Date birthDatePatient) {
        this.birthDatePatient = birthDatePatient;
    }

    public Integer getHistorial() {
        return historial;
    }

    public void setHistorial(Integer historial) {
        this.historial = historial;
    }

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }

    public String getBirthDatePatientSelect() {
        return birthDatePatientSelect;
    }

    public void setBirthDatePatientSelect(String birthDatePatientSelect) {
        this.birthDatePatientSelect = birthDatePatientSelect;
    }



}