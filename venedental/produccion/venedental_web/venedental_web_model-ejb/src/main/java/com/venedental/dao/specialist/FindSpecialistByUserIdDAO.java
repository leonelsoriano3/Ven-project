package com.venedental.dao.specialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.specialist.SpecialistDTO;
import com.venedental.model.Specialists;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 19/05/16.
 */
public class FindSpecialistByUserIdDAO extends AbstractQueryDAO<Integer, SpecialistDTO> {


    public FindSpecialistByUserIdDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_G_004", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public SpecialistDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        SpecialistDTO specialistDTO = new SpecialistDTO();
        specialistDTO.setId(rs.getInt("id"));
        specialistDTO.setIdentityNumber(rs.getInt("identity_number"));
        specialistDTO.setCompleteName(rs.getString("complete_name"));

        return specialistDTO;
    }


}