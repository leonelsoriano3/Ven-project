/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.unifyReport;

/**
 *
 * @author akdesk01
 */
public class UnifyReportInput {
    
    private Integer idSpecialist;
    private Integer idStatus;
    private Integer idMonth;
    private Integer yearReport;

    public UnifyReportInput(Integer idSpecialist, Integer idStatus, Integer idMonth, Integer yearReport) {
        
        this.idSpecialist=idSpecialist;
        this.idStatus=idStatus;
        this.idMonth=idMonth;
        this.yearReport=yearReport;
        
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Integer idMonth) {
        this.idMonth = idMonth;
    }

    public Integer getYearReport() {
        return yearReport;
    }

    public void setYearReport(Integer yearReport) {
        this.yearReport = yearReport;
    }

   
    
    
    
    
}
