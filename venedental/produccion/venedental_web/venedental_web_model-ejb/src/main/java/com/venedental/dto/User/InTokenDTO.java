package com.venedental.dto.User;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class InTokenDTO {

    private Integer userId;
    private String token;
    private String expiredDate;

    public InTokenDTO() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }
}
