/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.TypeSpecialist;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface TypeSpecialistFacadeLocal {

    TypeSpecialist create(TypeSpecialist typeSpecialist);

    TypeSpecialist edit(TypeSpecialist typeSpecialist);

    void remove(TypeSpecialist typeSpecialist);

    TypeSpecialist find(Object id);

    List<TypeSpecialist> findAll();

    List<TypeSpecialist> findRange(int[] range);

    int count();
    
}
