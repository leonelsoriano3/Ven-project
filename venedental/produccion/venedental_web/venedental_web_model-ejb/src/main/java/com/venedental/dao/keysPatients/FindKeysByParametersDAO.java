/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.KeysByParametersInput;
import com.venedental.dto.keyPatients.KeysByParametersOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindKeysByParametersDAO extends AbstractQueryDAO<KeysByParametersInput, KeysByParametersOutput> {

    public FindKeysByParametersDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_007", 7);
    }

    @Override
    public void prepareInput(KeysByParametersInput input, CallableStatement statement) throws SQLException {
        // IN PI_ID_ESPECIALISTA INT, IN PI_FECHA_DESDE DATE, IN PI_FECHA_HASTA DATE
        if (input.getIdKey() != null) {
            statement.setInt(1, input.getIdKey());
        } else {
            statement.setNull(1, java.sql.Types.NULL);
        }
        if (input.getNumberKey() != null) {
            statement.setString(2, input.getNumberKey());
        } else {
            statement.setNull(2, java.sql.Types.NULL);
        }
        if (input.getIdPatiente() != null) {
            statement.setInt(3, input.getIdPatiente());
        } else {
            statement.setNull(3, java.sql.Types.NULL);
        }
        if (input.getIdSpecialist() != null) {
            statement.setInt(4, input.getIdSpecialist());
        } else {
            statement.setNull(4, java.sql.Types.NULL);
        }
        if (input.getDateFrom() != null) {
            statement.setDate(5, new Date(input.getDateFrom().getTime()));
        } else {
            statement.setNull(5, java.sql.Types.NULL);
        }
        if (input.getDateUntil() != null) {
            statement.setDate(6, new Date(input.getDateUntil().getTime()));
        } else {
            statement.setNull(6, java.sql.Types.NULL);
        }
        if (input.getIdStatus() != null) {
            statement.setInt(7, input.getIdStatus());
        } else {
            statement.setNull(7, java.sql.Types.NULL);
        }
    }

    @Override
    public KeysByParametersOutput prepareOutput(ResultSet rs, CallableStatement statement) {
        try {
            KeysByParametersOutput findKeysByParametersOutput = new KeysByParametersOutput();
            findKeysByParametersOutput.setApplicationDate(rs.getDate("FECHA_CLAVE"));
            findKeysByParametersOutput.setIdKey(rs.getInt("ID_CLAVE"));
            findKeysByParametersOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
            findKeysByParametersOutput.setIdSpecialist(rs.getInt("ID_ESPECIALISTA"));
            findKeysByParametersOutput.setIdentityNumberSpecialist(rs.getInt("CEDULA_ESPECIALISTA"));
            findKeysByParametersOutput.setNameSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
            findKeysByParametersOutput.setIdPatient(rs.getInt("ID_PACIENTE"));
            findKeysByParametersOutput.setIdentityNumberPatient(rs.getInt("CEDULA_PACIENTE"));
            findKeysByParametersOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
            findKeysByParametersOutput.setIdStatus(rs.getInt("ID_ESTATUS"));
            findKeysByParametersOutput.setNameStatus(rs.getString("NOMBRE_ESTATUS"));
            return findKeysByParametersOutput;
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return null;
    }

}