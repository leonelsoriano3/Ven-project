/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class GenerateTxtInput {
    
    private Integer idStatus;
    private Date dateStart,dateEnd;

    public GenerateTxtInput() {
    }
    
    public  GenerateTxtInput(Integer idStatus, Date datestart,Date dateEnd ){
       this.idStatus=idStatus;
       this.dateStart=dateStart;
       this.dateEnd=dateEnd;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
    
    
    
}
