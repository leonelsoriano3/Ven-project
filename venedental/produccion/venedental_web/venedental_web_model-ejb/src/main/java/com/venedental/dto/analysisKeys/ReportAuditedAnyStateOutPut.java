/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author akdeskdev90
 */
public class ReportAuditedAnyStateOutPut {

    private Integer paidmentReportId;
    private String reportNumber;
    private String dateReport;
    private String specialistName;
    private String idCardSpecialist;
    private String bankName;
    private String numberAccount;
    private String idCardAccunt;
    private String numberInvoice;
    private String numberControl;
    private String dateInvoiceStr;
    private Date dateInvoice;
    private Double amountInvoice;
    private Double percentRetention;
    private Double amountRetentionISLR;
    private Double amountNet;

    private String numberTransfer;

    private String dateTransferStr;
    private Date dateTransfer;
    private Boolean haveRetention;

    private String amountInvoiceFormat;
    private String amountRetentionISLRFormat;
    private String amountNetFormat;
    private String percentRetentionFormat;

    private Double sumAmountNet;
    private String sumAmountNetFormat;

    private Double sumAmountRetentionISLR;
    private String sumAmountRetentionISLRFormat;

    private DecimalFormat formatMoney;


    private String dateMonth;

    public ReportAuditedAnyStateOutPut(Integer paidmentReportId, String reportNumber,
                                       String dateReport, String specialistName,
                                       String idCardSpecialist, String bankName,
                                       String numberAccount, String idCardAccunt,
                                       String numberInvoice, String dateInvoice,
                                       Double amountInvoice, Double percentRetention,
                                       Double amountRetentionISLR, Double amountNet,
                                       Date dateTransfer, String numberTransfer) {

        this.paidmentReportId = paidmentReportId;
        this.reportNumber = reportNumber;
        this.dateReport = dateReport;
        this.specialistName = specialistName;
        this.idCardSpecialist = idCardSpecialist;
        this.bankName = bankName;
        this.numberAccount = numberAccount;
        this.idCardAccunt = idCardAccunt;
        this.numberInvoice = numberInvoice;
        this.dateInvoiceStr = dateInvoice;
        this.amountInvoice = amountInvoice;
        this.percentRetention = percentRetention;
        this.amountRetentionISLR = amountRetentionISLR;
        this.amountNet = amountNet;
        this.dateTransfer = dateTransfer;
        this.numberTransfer = numberTransfer;

        this.formatMoney = new DecimalFormat("#,##0.00");
    }

    public ReportAuditedAnyStateOutPut() {
        this.formatMoney = new DecimalFormat("#,##0.00");
    }

    public Integer getPaidmentReportId() {
        return paidmentReportId;
    }

    public void setPaidmentReportId(Integer paidmentReportId) {
        this.paidmentReportId = paidmentReportId;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getSpecialistName() {
        return specialistName;
    }

    public void setSpecialistName(String specialistName) {
        this.specialistName = specialistName;
    }

    public String getIdCardSpecialist() {
        return idCardSpecialist;
    }

    public void setIdCardSpecialist(String idCardSpecialist) {
        this.idCardSpecialist = idCardSpecialist;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(String numberAccount) {
        this.numberAccount = numberAccount;
    }

    public String getIdCardAccunt() {
        return idCardAccunt;
    }

    public void setIdCardAccunt(String idCardAccunt) {
        this.idCardAccunt = idCardAccunt;
    }

    public String getNumberInvoice() {
        return (numberInvoice == null)? "" : numberInvoice;
    }

    public void setNumberInvoice(String numberInvoice) {
        this.numberInvoice = numberInvoice;
    }

    public String getNumberControl() {

        return (numberControl == null)? "":numberControl;
    }

    public void setNumberControl(String numberControl) {
        this.numberControl = numberControl;
    }

    public Double getAmountInvoice() {
        return amountInvoice;
    }

    public void setAmountInvoice(Double amountInvoice) {
        this.amountInvoice = amountInvoice;
    }

    public Double getPercentRetention() {
        return percentRetention;
    }

    public void setPercentRetention(Double percentRetention) {
        this.percentRetention = percentRetention;
    }

    public Double getAmountRetentionISLR() {
        return amountRetentionISLR;
    }

    public void setAmountRetentionISLR(Double amountRetentionISLR) {
        this.amountRetentionISLR = amountRetentionISLR;
    }

    public Double getAmountNet() {
        return amountNet;
    }

    public void setAmountNet(Double amountNet) {
        this.amountNet = amountNet;
    }

    public String getNumberTransfer() {
        return numberTransfer;
    }

    public void setNumberTransfer(String numberTransfer) {
        this.numberTransfer = numberTransfer;
    }

    public String getDateReport() {
        return dateReport;
    }

    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    public String getDateInvoiceStr() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (dateInvoice == null) {
            return "";
        } else {
            dateInvoiceStr = sdf.format(dateInvoice);
            return dateInvoiceStr;
        }
    }

    public void setDateInvoiceStr(String dateInvoiceStr) {
        this.dateInvoiceStr = dateInvoiceStr;
    }

    public Date getDateInvoice() {
        return dateInvoice;
    }

    public void setDateInvoice(Date dateInvoice) {
        this.dateInvoice = dateInvoice;
    }

    public Boolean getHaveRetention() {
        return haveRetention;
    }

    public void setHaveRetention(Boolean haveRetention) {
        this.haveRetention = haveRetention;
    }

    public Date getDateTransfer() {
        return dateTransfer;
    }

    public void setDateTransfer(Date dateTransfer) {
        this.dateTransfer = dateTransfer;
    }

    public String getDateTransferStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (dateTransfer == null) {
            return "";
        } else {
            dateTransferStr = sdf.format(dateTransfer);
            return dateTransferStr;
        }

    }

    public void setDateTransferStr(String dateTransferStr) {
        this.dateTransferStr = dateTransferStr;
    }

    //get para numeros formateados
    public String getAmountInvoiceFormat() {
        return this.formatMoney.format(Math.floor(amountInvoice * 100) / 100);
    }

    public void setAmountInvoiceFormat(String amountInvoiceFormat) {
        this.amountInvoiceFormat = amountInvoiceFormat;
    }

    public String getAmountRetentionISLRFormat() {

        return this.formatMoney.format(Math.floor(amountRetentionISLR * 100) / 100);
    }

    public void setAmountRetentionISLRFormat(String amountRetentionISLRFormat) {
        this.amountRetentionISLRFormat = amountRetentionISLRFormat;
    }

    public String getAmountNetFormat() {
        return this.formatMoney.format(Math.floor(amountNet * 100) / 100);
    }

    public void setAmountNetFormat(String amountNetFormat) {
        this.amountNetFormat = amountNetFormat;
    }

    public String getPercentRetentionFormat() {
        return this.formatMoney.format(Math.floor(percentRetention * 100) / 100);
    }

    public void setPercentRetentionFormat(String percentRetentionFormat) {
        this.percentRetentionFormat = percentRetentionFormat;
    }

    public Double getSumAmountNet() {
        return sumAmountNet;
    }

    public void setSumAmountNet(Double sumAmountNet) {
        this.sumAmountNet = sumAmountNet;
    }

    public String getSumAmountNetFormat() {
        return this.formatMoney.format(Math.floor(sumAmountNet * 100) / 100);
    }

    public void setSumAmountNetFormat(String sumAmountNetFormat) {
        this.sumAmountNetFormat = sumAmountNetFormat;
    }

    public Double getSumAmountRetentionISLR() {
        return sumAmountRetentionISLR;
    }

    public void setSumAmountRetentionISLR(Double sumAmountRetentionISLR) {
        this.sumAmountRetentionISLR = sumAmountRetentionISLR;
    }

    public String getSumAmountRetentionISLRFormat() {
        return this.formatMoney.format(Math.floor(this.sumAmountRetentionISLR * 100) / 100);
    }

    public void setSumAmountRetentionISLRFormat(String sumAmountRetentionISLRFormat) {
        this.sumAmountRetentionISLRFormat = sumAmountRetentionISLRFormat;
    }


    public String getDateMonth() {
        try {
            return getDateTransferStr().substring(3,getDateTransferStr().length());
        }catch (Exception ex){
            return getDateTransferStr();
        }

    }

    public void setDateMonth(String dateMonth) {
        this.dateMonth = dateMonth;
    }



}
