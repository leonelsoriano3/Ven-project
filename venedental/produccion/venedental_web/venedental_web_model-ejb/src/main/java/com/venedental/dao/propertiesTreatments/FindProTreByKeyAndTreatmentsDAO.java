/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.properties.FindProtByKeyAndTreatmenstInput;
import com.venedental.model.PropertiesTreatments;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProTreByKeyAndTreatmentsDAO extends AbstractQueryDAO<FindProtByKeyAndTreatmenstInput, PropertiesTreatments> {

    public FindProTreByKeyAndTreatmentsDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTREATM_G_002", 2);
    }

    @Override
    public void prepareInput(FindProtByKeyAndTreatmenstInput input, CallableStatement statement) throws SQLException {        
        statement.setInt(1, input.getIdTreatments());
        statement.setInt(2, input.getIdKeyPatient());
       
    }

    @Override
    public PropertiesTreatments prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesTreatments propertiesTreatments= new PropertiesTreatments();
        propertiesTreatments.setId(rs.getInt("id"));
        propertiesTreatments.setPropertieQuadrant(rs.getInt("CUADRANTE_PIEZA"));
        propertiesTreatments.setPropertieStringValue(rs.getString("NUMERO_PIEZA"));
        return  propertiesTreatments;
    }

}
