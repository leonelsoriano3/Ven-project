/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Patients;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PatientsFacadeLocal {

    Patients create(Patients patients);

    Patients edit(Patients patients);

    void remove(Patients patients);

    Patients find(Object id);
    
    List<Patients> findAll();
    
    List<Patients> findById(Integer id);
    
    List<Patients> findByIdentityNumber(int identityNumber);
        
    List<Patients> findByIdentityNumberHolder(int identityNumberHolder);
    
    List<Patients> findByIdentityNumberAndRelationShip(int identityNumber, int relationShip);
    
    List<Patients> findByIdentityNumberOrNamePatients(String filter);
    
    List<Patients> findByIdentityNumberHolderOrNamePatients(String filter);    
    
    List<Patients> findAllByIdentityNumberHolder(int filter);

    List<Patients> findByDuplicate(int identityNumber, int identityNumberHolder, Date birthDate, String completeName);

    List<Patients> findRange(int[] range);
    
    List<Patients> findBeneficiaries(int identity_number);
    
    List<Patients> findBeneficiariesAvailable(Integer idTitular, Integer idTypeAtencion);   
    
    int count();
    
    
}
