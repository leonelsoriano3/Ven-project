/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.plansPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.PlansPatients;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindPlanPatientByIdPatientDAO extends AbstractQueryDAO<Integer, PlansPatients> {

    public FindPlanPatientByIdPatientDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANSPATIENT_G_001", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public PlansPatients prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PlansPatients plansPatients = new PlansPatients();
        plansPatients.setPlantIdDto(rs.getInt("plans_id"));
        plansPatients.setPatientIdDto(rs.getInt("patients_id"));
        plansPatients.setDueDate(rs.getDate("due_date"));
        plansPatients.setPlanTypeSpecialistId(rs.getInt("ID_TIPO_ESPECIALIDAD"));
        plansPatients.setPlanName(rs.getString("NOMRE_PLAN"));
        return plansPatients;
    }
    
}
