/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Desarrollo
 */
@Entity
@Table(name = "type_attention")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeAttention.findAll", query = "SELECT t FROM TypeAttention t"),
    @NamedQuery(name = "TypeAttention.findById", query = "SELECT t FROM TypeAttention t WHERE t.id = :id"),
    @NamedQuery(name = "TypeAttention.findByName", query = "SELECT t FROM TypeAttention t WHERE t.name = :name"),
    @NamedQuery(name = "TypeAttention.findByStatus", query = "SELECT t FROM TypeAttention t WHERE t.status = :status")})
public class TypeAttention implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    
    @Column(name = "status")
    private Integer status;

    @JoinTable(name = "type_attention_treatments", joinColumns = {
        @JoinColumn(name = "type_attention_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "treatments_id", referencedColumnName = "id")})
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private List<Treatments> treatmentsList;

    public List<Treatments> getTreatmentsList() {
        return treatmentsList;
    }

    public void setTreatmentsList(List<Treatments> treatmentsList) {
        this.treatmentsList = treatmentsList;
    }

    public TypeAttention() {
    }

    public TypeAttention(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeAttention)) {
            return false;
        }
        TypeAttention other = (TypeAttention) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.TypeAttention[ id=" + id + " ]";
    }

}
