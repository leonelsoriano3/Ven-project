/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.managementSpecialist.InsertFileSupportInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class InsertFileSupportDAO extends AbstractCommandDAO<InsertFileSupportInput>{
    
    
    public InsertFileSupportDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEYSUP_I_001", 2);
    }

    @Override
    public void prepareInput(InsertFileSupportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input.getIdTreatmentKey());
        statement.setString(2,input.getNameFileSupport());
    }
    
}
