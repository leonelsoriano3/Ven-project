package com.venedental.dto.managementSpecialist;

/**
 * Created by akdesk10 on 04/08/16.
 */
public class DataKeyInput {

    private Integer idKey;
    private Integer numTooth;

    public DataKeyInput(Integer idKey, Integer numTooth) {
        this.idKey = idKey;
        this.numTooth = numTooth;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getNumTooth() {
        return numTooth;
    }

    public void setNumTooth(Integer numTooth) {
        this.numTooth = numTooth;
    }
}
