/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Desarrollo
 */
@Entity
@Table(name = "scale_treatments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScaleTreatments.findAll", query = "SELECT s FROM ScaleTreatments s"),
    @NamedQuery(name = "ScaleTreatments.findById", query = "SELECT s FROM ScaleTreatments s WHERE s.id = :id"),
    @NamedQuery(name = "ScaleTreatments.findByTypeScale", query = "SELECT s FROM ScaleTreatments s WHERE s.id = :id and s.idTypeScale = :idTypeScale"),
    @NamedQuery(name = "ScaleTreatments.findByPrice", query = "SELECT s FROM ScaleTreatments s WHERE s.price = :price"),
    @NamedQuery(name = "ScaleTreatments.findByDateScale", query = "SELECT s FROM ScaleTreatments s WHERE s.dateScale = :dateScale"),})
public class ScaleTreatments implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "price")
    private Double price;

    @Column(name = "date_scale")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateScale;

    @JoinColumn(name = "id_type_scale", referencedColumnName = "id")
    @ManyToOne
    private Scale idTypeScale;

    @OneToMany(mappedBy = "idScaleTreatments")
    private Collection<ScaleTreatmentsSpecialist> scaleTreatmentsSpecialistCollection;

    @JoinColumn(name = "id_treatments", referencedColumnName = "id")
    @ManyToOne
    private Treatments idTreatments;

    @OneToMany(mappedBy = "scaleTreatmentsKeys", cascade = CascadeType.ALL)
    private List<PlansTreatmentsKeys> plansTreatmentsKeyList;

    @Transient
    private Date minDate;

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public ScaleTreatments() {
    }

    public ScaleTreatments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateScale() {
        return dateScale;
    }

    public void setDateScale(Date dateScale) {
        this.dateScale = dateScale;
    }

    @XmlTransient
    public Collection<ScaleTreatmentsSpecialist> getScaleTreatmentsSpecialistCollection() {
        return scaleTreatmentsSpecialistCollection;
    }

    public void setScaleTreatmentsSpecialistCollection(Collection<ScaleTreatmentsSpecialist> scaleTreatmentsSpecialistCollection) {
        this.scaleTreatmentsSpecialistCollection = scaleTreatmentsSpecialistCollection;
    }

    public Scale getIdTypeScale() {
        return idTypeScale;
    }

    public void setIdTypeScale(Scale idTypeScale) {
        this.idTypeScale = idTypeScale;
    }

    public Treatments getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Treatments idTreatments) {
        this.idTreatments = idTreatments;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeyList() {
        return plansTreatmentsKeyList;
    }

    public void setPlansTreatmentsKeyList(List<PlansTreatmentsKeys> plansTreatmentsKeyList) {
        this.plansTreatmentsKeyList = plansTreatmentsKeyList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScaleTreatments)) {
            return false;
        }
        ScaleTreatments other = (ScaleTreatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.ScaleTreatments[ id=" + id + " ]";
    }

}
