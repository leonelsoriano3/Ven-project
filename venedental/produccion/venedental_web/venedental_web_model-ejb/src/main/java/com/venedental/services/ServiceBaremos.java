package com.venedental.services;

import com.venedental.dao.baremo.ConfigBaremoGeneralDAO;
import com.venedental.dao.baremo.ConsultBaremoGeneralDAO;
import com.venedental.dao.baremo.ConsultBaremoTreatmentForConfigDAO;
import com.venedental.dao.baremo.ConfigBaremoDateMinFilterDAO;
import com.venedental.dao.baremo.HistoryBaremoDAO;
import com.venedental.dto.baremo.ConfigBaremoGeneralnput;
import com.venedental.dto.baremo.ConsultBaremoGeneralInput;
import com.venedental.dto.baremo.ConsultBaremoGeneralOutput;
import com.venedental.dto.baremo.ConsultBaremoTreatmentForConfigOutput;
import com.venedental.dto.baremo.HistoryBaremoInput;
import com.venedental.dto.baremo.HistoryBaremosOutput;
import com.venedental.dto.baremo.ConfigBaremoDateMinFilterOutput;
import java.sql.SQLException;
import java.sql.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;


/**
 *
 * Servicio Baremo
 */
@Stateless
public class ServiceBaremos {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * servicio para guardar Baremo General
     *
     * @param idTreatment
     * @param idSpecialist
     * @param idUser
     * @param baremo
     * @param effectiveDate
     * @throws java.sql.SQLException
     */

    public void ConfigBaremoGeneralSave(Integer idTreatment,Integer idSpecialist,Integer idUser, Double  baremo, Date effectiveDate)throws SQLException {
        ConfigBaremoGeneralDAO configBaremoGeneralDAO= new ConfigBaremoGeneralDAO(ds);
        ConfigBaremoGeneralnput input = new ConfigBaremoGeneralnput(idTreatment, idSpecialist,idUser,baremo, effectiveDate);
        configBaremoGeneralDAO.execute(input);

    }

    public List<HistoryBaremosOutput> detailsHistoryBaremoInteger(Integer id_treatment, Integer id_speciality, Date start_date, Date end_date)throws SQLException {
        HistoryBaremoDAO historyBaremoDAO= new HistoryBaremoDAO(ds);
        HistoryBaremoInput input = new HistoryBaremoInput(id_treatment, id_speciality, start_date, end_date);
        historyBaremoDAO.execute(input);
        return historyBaremoDAO.getResultList();
    }



    public List<ConsultBaremoGeneralOutput> findConsultBaremoGeneralOutput(Date fechaInicio, Date fechaFin, Integer especialidad ) {

        ConsultBaremoGeneralDAO consultBaremoGeneralDAO = new ConsultBaremoGeneralDAO(ds);
        ConsultBaremoGeneralInput input = new ConsultBaremoGeneralInput(fechaInicio,fechaFin,especialidad);
        input.setFechaInicio(fechaInicio);
        input.setFechaFin(fechaFin);
        input.setIdEspecialidad(especialidad);
        consultBaremoGeneralDAO.execute(input);
        return consultBaremoGeneralDAO.getResultList();
    }
    
    public List<ConsultBaremoTreatmentForConfigOutput> findConsultbaremoForConf(Integer idSpeciality){
       ConsultBaremoTreatmentForConfigDAO consultBaremoTreatmentForConfigDAO = new ConsultBaremoTreatmentForConfigDAO(ds);
        consultBaremoTreatmentForConfigDAO.execute(idSpeciality);
        return consultBaremoTreatmentForConfigDAO.getResultList(); 
    }
    
   public Date findDateMinFilter(Integer idTreatment){
       ConfigBaremoDateMinFilterDAO configBaremoDateMinFilterDAO = new ConfigBaremoDateMinFilterDAO(ds);
        configBaremoDateMinFilterDAO.execute(idTreatment);
        return configBaremoDateMinFilterDAO.getResult(); 
    }

}