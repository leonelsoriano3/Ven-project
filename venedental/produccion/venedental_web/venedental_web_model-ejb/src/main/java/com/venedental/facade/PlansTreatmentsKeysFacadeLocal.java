/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansTreatmentsKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PlansTreatmentsKeysFacadeLocal {

    PlansTreatmentsKeys create(PlansTreatmentsKeys patients);

    PlansTreatmentsKeys edit(PlansTreatmentsKeys patients);

    void remove(PlansTreatmentsKeys patients);

    PlansTreatmentsKeys find(Object id);

    List<PlansTreatmentsKeys> findAll();

    List<PlansTreatmentsKeys> findRange(int[] range);
    
    List<PlansTreatmentsKeys> findByPlansId(Integer id);
    
    List<PlansTreatmentsKeys> findByKeysId(Integer id);

    int count();   
    
    List<PlansTreatmentsKeys> findByTwoStatusAndSpecialist(int idStatus1, int idStatus2,  int specialistId);
    
    List<PlansTreatmentsKeys> findByStatusAndSpecialist(int idStatus,  int specialistId);
    
    List<PlansTreatmentsKeys> findByKeysAndStatus(int keysId,  int idStatus);
    
    List<PlansTreatmentsKeys> findByKeysAndExcludeStatus(int keysId,  int idStatus);
    
    List<PlansTreatmentsKeys> findByKeysAndTwoStatus(int keysId, int idStatus1, int idStatus2);
    
    List<PlansTreatmentsKeys> findByKeysAndThreeStatus(int keysId, int idStatus1, int idStatus2, int idStatus3);
    
    PlansTreatmentsKeys findByKeysAndPlansTreatment(int keysId, int plansTreatmentId);
    
    List<PlansTreatmentsKeys> filterBySpecialistTratmentStatusAndDateRange(Integer keysId, Date dateFrom, Date dateUntil);
    
    List<PlansTreatmentsKeys> findByPatientId(Integer patientId);
    
      
}