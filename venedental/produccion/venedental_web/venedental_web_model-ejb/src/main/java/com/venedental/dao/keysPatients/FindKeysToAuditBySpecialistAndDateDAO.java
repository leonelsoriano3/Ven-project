/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dto.analysisKeys.FindKeysToAuditBySpecialistAndDateInput;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.KeysPatients;
import com.venedental.model.StatusAnalysisKeys;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindKeysToAuditBySpecialistAndDateDAO extends AbstractQueryDAO<FindKeysToAuditBySpecialistAndDateInput, KeysPatients> {
    
     public FindKeysToAuditBySpecialistAndDateDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_005", 4);
    }

    @Override
    public void prepareInput(FindKeysToAuditBySpecialistAndDateInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdStatus());
        statement.setDate(3, (Date) input.getDateFrom());
        statement.setDate(4, (Date) input.getDateUntil());
    }

    @Override
    public KeysPatients prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        KeysPatients keysPatients = new KeysPatients();
        keysPatients.setId(rs.getInt("id"));
        keysPatients.setStatus(new StatusAnalysisKeys(rs.getInt("status")));
        keysPatients.setNumber(rs.getString("number"));
        keysPatients.setApplicationDate(rs.getDate("application_date"));
        keysPatients.setIdPatient(rs.getInt("patients_id"));
        keysPatients.setIdentityNumberPatient(rs.getString("cedula_paciente"));
        keysPatients.setNamePatient(rs.getString("nombre_paciente"));
        keysPatients.setStatusName(rs.getString("nombre_estatus"));

        return keysPatients;

    }
    
    
    
}
