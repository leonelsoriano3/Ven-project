package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConsultPaymentReportInput;
import com.venedental.dto.analysisKeys.ConsultPaymentReportOutPut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 17/03/16.
 */
public class ConsultPaymentReportDao extends AbstractQueryDAO<ConsultPaymentReportInput,ConsultPaymentReportOutPut> {



    public ConsultPaymentReportDao(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_009", 4);
    }


    @Override
    public void prepareInput(ConsultPaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdStatus());
        statement.setInt(3, input.getIdMonth());
        statement.setDate(4, input.getDateReception());
    }

    @Override
    public ConsultPaymentReportOutPut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultPaymentReportOutPut consultPaymentReportOutPut = new ConsultPaymentReportOutPut();
        consultPaymentReportOutPut.setIdReport(rs.getInt("id_reporte_pago"));
        consultPaymentReportOutPut.setNumberReport(rs.getString("numero_reporte_pago"));
        return consultPaymentReportOutPut;
    }
}
