/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Edgar
 */
@Embeddable
public class ClinicsUsersPK implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "clinics_id")
    private Integer clinicsId;
     
    @Basic(optional = false)
    @NotNull
    @Column(name = "users_id")
    private Integer usersId;

    public ClinicsUsersPK() {
    }

    public ClinicsUsersPK(Integer clinicsId, Integer usersId) {
        this.clinicsId = clinicsId;
        this.usersId = usersId;
    }

    public int getClinicsId() {
        return clinicsId;
    }

    public void setClinicsId(Integer clinicsId) {
        this.clinicsId = clinicsId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) clinicsId;
        hash += (int) usersId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClinicsUsersPK)) {
            return false;
        }
        ClinicsUsersPK other = (ClinicsUsersPK) object;
        if (this.clinicsId != other.clinicsId) {
            return false;
        }
        if (this.usersId != other.usersId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.ClinicsUsersPK[ clinicsId=" + clinicsId + ", usersId=" + usersId + " ]";
    }

}
