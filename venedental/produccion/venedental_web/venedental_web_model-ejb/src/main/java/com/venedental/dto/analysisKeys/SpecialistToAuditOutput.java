/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class SpecialistToAuditOutput {
    
    private Date dateInitiation;
    private Integer idSpecialist;
    private String compĺeteName;
    private String nameStatusAnalysisKeys;

    public SpecialistToAuditOutput(Date dateInitiation, Integer idSpecialist, String compĺeteName) {
        this.dateInitiation = dateInitiation;
        this.idSpecialist = idSpecialist;
        this.compĺeteName = compĺeteName;
    }

    public SpecialistToAuditOutput() {
    }

    public Date getDateInitiation() {
        return dateInitiation;
    }

    public void setDateInitiation(Date dateInitiation) {
        this.dateInitiation = dateInitiation;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getCompĺeteName() {
        return compĺeteName;
    }

    public void setCompĺeteName(String compĺeteName) {
        this.compĺeteName = compĺeteName;
    }

    public String getNameStatusAnalysisKeys() {
        return nameStatusAnalysisKeys;
    }

    public void setNameStatusAnalysisKeys(String nameStatusAnalysisKeys) {
        this.nameStatusAnalysisKeys = nameStatusAnalysisKeys;
    }

}
