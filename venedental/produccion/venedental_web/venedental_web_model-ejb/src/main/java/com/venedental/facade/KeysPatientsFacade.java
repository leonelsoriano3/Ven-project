/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.KeysPatients;
import com.venedental.model.StatusAnalysisKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class KeysPatientsFacade extends AbstractFacade<KeysPatients> implements KeysPatientsFacadeLocal {

    private static int count;

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KeysPatientsFacade() {
        super(KeysPatients.class);
    }

    @Override
    public int count() {
        return super.count(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeysPatients> findRange(int[] range) {
        return super.findRange(range); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<KeysPatients> findByApplicationDate(Date startDate, Date endDate) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByApplicationDate", KeysPatients.class);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> findByApplicationDateAndSpecialist(String applicationDate, int specialistId) {
        String date = "'" + applicationDate + "'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients where DATE_FORMAT(keys_patients.application_date,'%d/%m/%Y') = " + date + " AND keys_patients.specialist_id = " + specialistId, KeysPatients.class);
        query.setParameter("applicationDate", applicationDate);
        return query.getResultList();
    }
    
    @Override
    public List<KeysPatients> findByKeysSpecialist(int specialistId) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByKeysSpecialist", KeysPatients.class);
        query.setParameter("specialistId", specialistId);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> findByApplicationDateSpecialist(Date startDate, Date endDate, int specialistId) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByApplicationDateSpecialist", KeysPatients.class);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("specialistId", specialistId);
        return query.getResultList();
    }    

    @Override
    public List<KeysPatients> findByIdPatients(Integer id) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByIdPatients", KeysPatients.class);
        query.setParameter("patients_id", id);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> findByApplicationDateSpeciality(Date startDate, Date endDate, Integer typeSpecialistId) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByApplicationDateSpeciality", KeysPatients.class);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("typeSpecialistId", typeSpecialistId);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> findByApplicationDateInsurances(Date startDate, Date endDate, Integer insurancesList) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByApplicationDateInsurances", KeysPatients.class);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("insurancesList", insurancesList);
        return query.getResultList();
    }

    /**
     * Find keysPatients paginates in the DB
     *
     * @param startingAt the first "row" db that the query will search
     * @param maxPerPage the amount of records allowed per "trip" in the DB
     * @return a keysPatients list
     */
    @Override
    public List<KeysPatients> findByPage(Integer startingAt, Integer maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByStatus", KeysPatients.class);
        query.setParameter("status",statusAnalysisKeys);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterByNumber(String number, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.filterByNumber", KeysPatients.class);
        query.setParameter("number", "%" + number + "%");
        query.setParameter("status",statusAnalysisKeys);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterBySpecialistIdNumber(String identityNumber, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        String idNumber = "'%" + identityNumber + "%'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients kp, specialists sp where kp.specialist_id = sp.id and sp.identity_number LIKE " + idNumber + " and kp.status = " + statusAnalysisKeys.getId() +" ORDER BY kp.application_date DESC", KeysPatients.class);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterBySpecialistName(String name, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        String completeName = "'%" + name + "%'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients kp, specialists sp where kp.specialist_id = sp.id and sp.complete_name LIKE " + completeName +" and kp.status = " + statusAnalysisKeys.getId() +" ORDER BY kp.application_date DESC", KeysPatients.class);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterByApplicationDate(String applicationDate, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        String date = "'" + applicationDate + "'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients kp where DATE_FORMAT(kp.application_date,'%d/%m/%Y') = " + date +" and kp.status = " + statusAnalysisKeys.getId() +" ORDER BY kp.application_date DESC", KeysPatients.class);      
        count = query.getResultList().size();
        if (startingAt > count) {
            if (count > maxPerPage) {
                query.setFirstResult(count - maxPerPage);
            } else {
                query.setFirstResult(maxPerPage);
            }
        } else {
            query.setFirstResult(startingAt);
        }
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }
    
    @Override
    public List<KeysPatients> filterByApplicationDateRange(Date startDate, Date endDate, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByApplicationDate", KeysPatients.class);
        query.setParameter("status",statusAnalysisKeys);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        count = query.getResultList().size();
        if (startingAt > count) {
            if (count > maxPerPage) {
                query.setFirstResult(count - maxPerPage);
            } else {
                query.setFirstResult(maxPerPage);
            }
        } else {
            query.setFirstResult(startingAt);
        }
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterByPatientIdNumber(String identityNumber, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        String idNumber = "'%" + identityNumber + "%'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients kp, patients pt where kp.patients_id = pt.id and pt.identity_number LIKE " + idNumber +" and kp.status = " + statusAnalysisKeys.getId() +" ORDER BY kp.application_date DESC", KeysPatients.class);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public List<KeysPatients> filterByPatientName(String name, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys) {
        String completeName = "'%" + name + "%'";
        Query query = em.createNativeQuery("SELECT * FROM keys_patients kp, patients pt where kp.patients_id = pt.id and pt.complete_name LIKE " + completeName +" and kp.status = " + statusAnalysisKeys.getId() +" ORDER BY kp.application_date DESC", KeysPatients.class);
        count = query.getResultList().size();
        query.setFirstResult(startingAt);
        query.setMaxResults(maxPerPage);
        return query.getResultList();
    }

    @Override
    public int reciveCount() {
        return count;
    }
    
    @Override
    public List<KeysPatients> findByStatusAndSpecialist(int idStatus, int specialistId){
        TypedQuery<KeysPatients> query = em.createNamedQuery("KeysPatients.findByStatusAndSpecialist", KeysPatients.class);
        query.setParameter("idStatus", idStatus);
        query.setParameter("specialistId", specialistId);
        return query.getResultList();
    }
    
    /*Init Stored Procedure */    
    @Override
    public List<KeysPatients> findByStatusSpecialistAndTreatmentsStatus(Integer idStatus,Integer idStatus2, Integer specialistId, Integer idStatusTreatments1, Integer idStatusTreatments2, Integer idStatusTreatments3) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_CLAVES_ESTATUS_ESPECIALISTA", KeysPatients.class);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS_CLAVE_1", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS_CLAVE_2", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS_1", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS_2", Integer.class, ParameterMode.IN);  
        query.registerStoredProcedureParameter("PI_ID_ESTATUS_3", Integer.class, ParameterMode.IN);  
        query.registerStoredProcedureParameter("PI_ID_ESPECIALISTA", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_ESTATUS_CLAVE_1", idStatus);
        query.setParameter("PI_ID_ESTATUS_CLAVE_2", idStatus2);
        query.setParameter("PI_ID_ESTATUS_1", idStatusTreatments1);  
        query.setParameter("PI_ID_ESTATUS_2", idStatusTreatments2);  
        query.setParameter("PI_ID_ESTATUS_3", idStatusTreatments3);  
        query.setParameter("PI_ID_ESPECIALISTA", specialistId);         
        return query.getResultList();
    }
    
    @Override
    public List<KeysPatients> findByStatusAndTreatmentsStatus(Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_CLAVES_ACEPTADAS_RECHAZADAS", KeysPatients.class);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);        
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);       
        return query.getResultList();
    }
    
    @Override
    public List<KeysPatients> readKeysRecived(Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SPD_LEER_CLAVES_RECIBIDAS", KeysPatients.class);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);        
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);       
        return query.getResultList();
    }
    /*End Stored Procedure */
    
}