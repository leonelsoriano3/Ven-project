/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.PaymentRefund;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.PaymentRefund.GenerateTxtInput;
import com.venedental.dto.PaymentRefund.GenerateTxtOutput;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class GenerateTxtRefundDAO extends AbstractQueryDAO<GenerateTxtInput, GenerateTxtOutput>{
    
   public GenerateTxtRefundDAO(DataSource dataSource) {
        super(dataSource, "SP_M_REM_GenerarArchivoTexto",4);
    }

    @Override
    public void prepareInput(GenerateTxtInput input, CallableStatement statement) throws SQLException {
        if(input.getId_paciente()==0){
            statement.setNull(1, java.sql.Types.NULL);
            
        }else{
           statement.setInt(1, input.getId_paciente()); 
        }
        
        statement.setInt(2, input.getId_estatus());
        statement.setDate(3, (java.sql.Date) input.getStart_date());
        statement.setDate(4, (java.sql.Date) input.getEnd_date());
        
    }

    @Override
    public GenerateTxtOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        GenerateTxtOutput generateTxtOutput = new GenerateTxtOutput();
        generateTxtOutput.setAccountNumber(rs.getString("numero_cuenta"));
        generateTxtOutput.setPatient_Cedula(rs.getString("cedula_paciente"));
        generateTxtOutput.setEntireAmount(rs.getString("monto_pagar_entero"));
        generateTxtOutput.setDecimalAmount(rs.getString("monto_pagar_decimal"));
        generateTxtOutput.setPaymentReference(rs.getString("referencia_pago"));
        generateTxtOutput.setPatientName(rs.getString("nombre_paciente"));
        generateTxtOutput.setEmailRecipient(rs.getString("email_paciente"));
        return generateTxtOutput;
    }

    
}
