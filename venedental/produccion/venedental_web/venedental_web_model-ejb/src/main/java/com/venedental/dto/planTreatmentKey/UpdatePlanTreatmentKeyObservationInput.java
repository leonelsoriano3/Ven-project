/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

/**
 *
 * @author root
 */
public class UpdatePlanTreatmentKeyObservationInput {
    
    int idPlanTreatmentKey;
    String observation;

    public UpdatePlanTreatmentKeyObservationInput(int idPlanTreatmentKey, String observation) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.observation = observation;
    }
    
    public int getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(int idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public void setObservation(String observation){
        this.observation = observation;
    }
    
    public String getObservation(){
        return this.observation;
    }

    
}
