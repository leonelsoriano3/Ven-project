package com.venedental.dto.consultKeys;

import java.util.Date;


public class ConsultDateDetailTreatmentOutput {
  private String  dateMinTreatmentFilter,dateMiaxTreatmentFilter;
    private Date dateMinTreatment,dateMiaxTreatment;

    public ConsultDateDetailTreatmentOutput() {
    }

    public String getDateMinTreatmentFilter() {
        return dateMinTreatmentFilter;
    }

    public void setDateMinTreatmentFilter(String dateMinTreatmentFilter) {
        this.dateMinTreatmentFilter = dateMinTreatmentFilter;
    }

    public String getDateMiaxTreatmentFilter() {
        return dateMiaxTreatmentFilter;
    }

    public void setDateMiaxTreatmentFilter(String dateMiaxTreatmentFilter) {
        this.dateMiaxTreatmentFilter = dateMiaxTreatmentFilter;
    }

    public Date getDateMinTreatment() {
        return dateMinTreatment;
    }

    public void setDateMinTreatment(Date dateMinTreatment) {
        this.dateMinTreatment = dateMinTreatment;
    }

    public Date getDateMiaxTreatment() {
        return dateMiaxTreatment;
    }

    public void setDateMiaxTreatment(Date dateMiaxTreatment) {
        this.dateMiaxTreatment = dateMiaxTreatment;
    }


    

    
   
    
}
