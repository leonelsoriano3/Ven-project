/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.specialist;

/**
 *
 * @author AKDESKDEV90
 */
public class SpecialistByIdentityOrNameOutput {
    
    private Integer id;
    private Integer identityNumber;
    private String completeName;

    public SpecialistByIdentityOrNameOutput() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }
    
    
    
}
