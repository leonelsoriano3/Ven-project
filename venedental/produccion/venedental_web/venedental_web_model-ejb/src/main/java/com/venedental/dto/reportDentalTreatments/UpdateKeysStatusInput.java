package com.venedental.dto.reportDentalTreatments;

/**
 * Created by akdesk10 on 14/07/16.
 */
public class UpdateKeysStatusInput {

    private Long idKey;

    private Integer numReport;

    private Integer idSpecialist;

    private Integer idToothTreatment;

    private Integer idKeyTreatment;

    public UpdateKeysStatusInput(Long idKey,Integer idKeyTreatment,Integer idToothTreatment,Integer numReport, Integer idSpecialist){

        this.idKey = idKey;
        this.numReport = numReport;
        this.idSpecialist = idSpecialist;
        this.idKeyTreatment = idKeyTreatment;
        this.idToothTreatment = idToothTreatment;
    }

    public Integer getIdToothTreatment() {
        return idToothTreatment;
    }

    public void setIdToothTreatment(Integer idToothTreatment) {
        this.idToothTreatment = idToothTreatment;
    }

    public Integer getIdKeyTreatment() {
        return idKeyTreatment;
    }

    public void setIdKeyTreatment(Integer idKeyTreatment) {
        this.idKeyTreatment = idKeyTreatment;
    }

    public Long getIdKey() {return idKey;}

    public void setIdKey(Long idKey) {this.idKey = idKey;}

    public Integer getNumReport() {return numReport;}

    public void setNumReport(Integer numReport) {this.numReport = numReport;}

    public Integer getIdSpecialist() {return idSpecialist;}

    public void setIdSpecialist(Integer idSpecialist) {this.idSpecialist = idSpecialist;}

}
