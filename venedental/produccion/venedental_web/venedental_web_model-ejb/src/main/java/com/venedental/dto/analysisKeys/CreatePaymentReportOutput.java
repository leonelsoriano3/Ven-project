/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author root
 */
public class CreatePaymentReportOutput {
    
    Integer idPaymentReport;
    String number;
    Date date;
    double amount;
    double auditedAmount;

    public CreatePaymentReportOutput() {
    }

    public CreatePaymentReportOutput(String number, Date date, double amount, double auditedAmount) {
        this.number = number;
        this.date = date;
        this.amount = amount;
        this.auditedAmount = auditedAmount;
    }
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAuditedAmount() {
        return auditedAmount;
    }

    public void setAuditedAmount(double auditedAmount) {
        this.auditedAmount = auditedAmount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getIdPaymentReport() {
        return idPaymentReport;
    }

    public void setIdPaymentReport(Integer idPaymentReport) {
        this.idPaymentReport = idPaymentReport;
    }

}
