/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Edgar
 */
@Embeddable
public class InsurancesPatientsPK implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insurances_id")
    private Integer insurancesId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "patients_id")
    private Integer patientsId;

    public InsurancesPatientsPK() {
    }

    public InsurancesPatientsPK(Integer insurancesId, Integer patientsId) {
        this.insurancesId = insurancesId;
        this.patientsId = patientsId;
    }

    public int getInsurancesId() {
        return insurancesId;
    }

    public void setInsurancesId(Integer insurancesId) {
        this.insurancesId = insurancesId;
    }

    public int getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Integer patientsId) {
        this.patientsId = patientsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) insurancesId;
        hash += (int) patientsId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsurancesPatientsPK)) {
            return false;
        }
        InsurancesPatientsPK other = (InsurancesPatientsPK) object;
        if (this.insurancesId != other.insurancesId) {
            return false;
        }
        if (this.patientsId != other.patientsId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.InsurancesPatientsPK[ insurancesId=" + insurancesId + ", patientsId=" + patientsId + " ]";
    }

}
