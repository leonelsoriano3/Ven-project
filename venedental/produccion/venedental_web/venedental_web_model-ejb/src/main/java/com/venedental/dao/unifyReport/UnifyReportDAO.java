/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.unifyReport;
import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.unifyReport.UnifyReportInput;
import com.venedental.dao.AbstractQueryDAO;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class UnifyReportDAO extends AbstractCommandDAO<UnifyReportInput> {

     public UnifyReportDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_Unificar_Reportes", 4);
    }

    @Override
    public void prepareInput(UnifyReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input.getIdSpecialist());
        statement.setInt(2,input.getIdStatus());
        statement.setInt(3,input.getIdMonth());
        statement.setInt(4,input.getYearReport());



    }
}
