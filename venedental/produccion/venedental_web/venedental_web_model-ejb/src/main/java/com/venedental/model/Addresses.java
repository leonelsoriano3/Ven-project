/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "addresses", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Addresses.findAll", query = "SELECT a FROM Addresses a"),
    @NamedQuery(name = "Addresses.findById", query = "SELECT a FROM Addresses a WHERE a.id = :id"),
    @NamedQuery(name = "Addresses.findByAddress", query = "SELECT a FROM Addresses a WHERE a.address = :address")})
public class Addresses implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "address")
    private String address;

    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Cities cityId;

    @OneToMany(mappedBy = "addressId")
    private List<Insurances> insurancesList;

    @OneToMany(mappedBy = "addressId")
    private List<Clinics> clinicsList;

    public Addresses() {
    }

    public Addresses(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Cities getCityId() {
        return cityId;
    }

    public void setCityId(Cities cityId) {
        this.cityId = cityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Addresses)) {
            return false;
        }
        Addresses other = (Addresses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.Addresses[ id=" + id + " ]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Addresses obj = null;
        obj = (Addresses) super.clone();

        obj.cityId = (obj.cityId == null ? null : (Cities) obj.cityId.clone());
        return obj;
    }

    @XmlTransient
    public List<Clinics> getClinicsList() {
        return clinicsList;
    }

    public void setClinicsList(List<Clinics> clinicsList) {
        this.clinicsList = clinicsList;
    }

    @XmlTransient
    public List<Insurances> getInsurancesList() {
        return insurancesList;
    }

    public void setInsurancesList(List<Insurances> insurancesList) {
        this.insurancesList = insurancesList;
    }

}
