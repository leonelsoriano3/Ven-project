/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.baremo.ConfigBaremoGeneralnput;
import com.venedental.dto.managementSpecialist.RemoveAssignedtreatmentInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class RemoveAssignedTreatmentDAO extends AbstractCommandDAO<RemoveAssignedtreatmentInput>{
    
    
    public RemoveAssignedTreatmentDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_D_001", 1);
    }

    @Override
    public void prepareInput(RemoveAssignedtreatmentInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input.getIdTreatmentKey());
        
    }
}
