/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultTreatmentAssignedOutput {
    private Integer idTreatment,idCategory,idPlanTreatment,idBaremo,idTreatmentKey,treatmentRegister;
    private String nameTreatment,nameCategory;

    public ConsultTreatmentAssignedOutput() {
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdBaremo() {
        return idBaremo;
    }

    public void setIdBaremo(Integer idBaremo) {
        this.idBaremo = idBaremo;
    }

    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public Integer getTreatmentRegister() {
        return treatmentRegister;
    }

    public void setTreatmentRegister(Integer treatmentRegister) {
        this.treatmentRegister = treatmentRegister;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
            
            
    
}
