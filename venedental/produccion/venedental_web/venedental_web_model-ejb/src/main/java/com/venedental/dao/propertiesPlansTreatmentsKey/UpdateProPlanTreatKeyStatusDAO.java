/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.planTreatmentKey.UpdatePlanTreatmentKeyStatusInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class UpdateProPlanTreatKeyStatusDAO extends AbstractCommandDAO<UpdatePlanTreatmentKeyStatusInput> {

    public UpdateProPlanTreatKeyStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_U_001", 4);
    }

    @Override
    public void prepareInput(UpdatePlanTreatmentKeyStatusInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPlanTreatmentKey());
        statement.setInt(2, input.getIdStatus());
        statement.setInt(3, input.getIdUser());
        if (input.getDateReception() != null) {
            statement.setDate(4, new Date( input.getDateReception().getTime()));
        } else {
            statement.setNull(4, java.sql.Types.NULL);
        }
    }

}
