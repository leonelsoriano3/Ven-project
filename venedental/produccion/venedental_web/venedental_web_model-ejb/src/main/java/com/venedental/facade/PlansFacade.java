/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import com.venedental.model.Plans;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PlansFacade extends AbstractFacade<Plans> implements PlansFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlansFacade() {
        super(Plans.class);
    }

    @Override
    public Plans edit(Plans entity) {
        return super.edit(entity);
    }

    @Override
    public Plans create(Plans entity) {
        return super.create(entity);
    }

    @Override
    public List<Plans> findByPatientsId(Integer id) {
        TypedQuery<Plans> query = em.createNamedQuery("Plans.findByPatientsId", Plans.class);
        query.setParameter("patients_id", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<Plans> findByInsuranceId(Insurances insurance) {
        TypedQuery<Plans> query = em.createNamedQuery("Plans.findByInsurance", Plans.class);
        query.setParameter("insurance", insurance);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Plans> findById(Integer id) {
        TypedQuery<Plans> query = em.createNamedQuery("Plans.findById", Plans.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
    
    /**
     * Servicio que retorna los planes disponibles para el reembolso del paciente
     * segun el tipo de especialidad si es null el tipo los devuelve para todos
     * @param idPatient
     * @param idTypeSpeciality
     * @param isHolder
     * @return 
     */

    @Override
    public List<Plans> findPlansAvailableByPatient(Integer idPatient, Integer idTypeSpeciality, Integer isHolder) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_PLANS_G_002", Plans.class);
        query.registerStoredProcedureParameter("PI_ID_PACIENTE", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_AREA_SALUD", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ES_TITULAR", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_PACIENTE", idPatient);
        query.setParameter("PI_ID_AREA_SALUD", idTypeSpeciality);
        query.setParameter("PI_ES_TITULAR", isHolder);
        return query.getResultList();
    }

    @Override
    public List<Plans> finPlansPatientsByHealthArea(Integer identityHolder, Integer healtArea) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_PLANS_G_003", Plans.class);
        query.registerStoredProcedureParameter("PI_ID_PACIENTE", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_PACIENTE", identityHolder);
        query.registerStoredProcedureParameter("PI_ID_AREA_SALUD", Integer.class, ParameterMode.IN);
        query.setParameter("PI_ID_AREA_SALUD", healtArea);
        return query.getResultList();
    }
    
    
    
}
