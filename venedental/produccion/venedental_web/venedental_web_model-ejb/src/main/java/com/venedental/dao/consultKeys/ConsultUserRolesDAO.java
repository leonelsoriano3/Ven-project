/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultUserRolesOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultUserRolesDAO extends AbstractQueryDAO<Integer ,ConsultUserRolesOutput>{
    
     public ConsultUserRolesDAO(DataSource dataSource) {
        super(dataSource, "SP_T_ROLUSERS_G_001", 1);
    }
   
     @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {       
                statement.setInt(1, input);
    }

    @Override
    public ConsultUserRolesOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultUserRolesOutput consultUserRolesOutput = new ConsultUserRolesOutput();
        consultUserRolesOutput.setUserRoles(rs.getInt("ROL_ID"));
        return consultUserRolesOutput;
    }
    
}
