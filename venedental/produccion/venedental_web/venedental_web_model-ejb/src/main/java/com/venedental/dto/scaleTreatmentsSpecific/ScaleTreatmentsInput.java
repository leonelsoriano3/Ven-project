/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.scaleTreatmentsSpecific;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class ScaleTreatmentsInput {
    Integer idTreatment;
    Integer idSpecialist;
    Integer idUser;
    Double baremo;
    String receptionDate;
    
    public ScaleTreatmentsInput(Integer idTreatment,Double baremo,String receptionDate,Integer idSpecialist,Integer idUser) {
        this.idTreatment=idTreatment;
        this.idSpecialist=idSpecialist;
        this.idUser = idUser;
        this.baremo=baremo;
        this.receptionDate=receptionDate;
    } 

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Double getBaremo() {
        return baremo;
    }

    public void setBaremo(Double baremo) {
        this.baremo = baremo;
    }

    public String getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(String receptionDate) {
        this.receptionDate = receptionDate;
    }

    

    
    
    
}
