/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "type_specialist", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeSpecialist.findAll", query = "SELECT t FROM TypeSpecialist t"),
    @NamedQuery(name = "TypeSpecialist.findById", query = "SELECT t FROM TypeSpecialist t WHERE t.id = :id"),
    @NamedQuery(name = "TypeSpecialist.findByName", query = "SELECT t FROM TypeSpecialist t WHERE t.name = :name")})
public class TypeSpecialist implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "typeSpecialist")
    private List<Speciality> specialityList;

    @OneToMany(mappedBy = "typeSpecialistid")
    private List<Specialists> specialistsList;

    @OneToMany(mappedBy = "typeSpecialistId")
    private List<Plans> plansList;

    @OneToMany(mappedBy = "idTypeSpeciality")
    private List<Treatments> treatmentsList;

    public TypeSpecialist() {
    }

    public TypeSpecialist(Integer id) {
        this.id = id;
    }

    public TypeSpecialist(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Treatments> getTreatmentsList() {
        return treatmentsList;
    }

    public void setTreatmentsList(List<Treatments> treatmentsList) {
        this.treatmentsList = treatmentsList;
    }

    @XmlTransient
    public List<Speciality> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<Speciality> specialityList) {
        this.specialityList = specialityList;
    }

    @XmlTransient
    public List<Specialists> getSpecialistsList() {
        return specialistsList;
    }

    public void setSpecialistsList(List<Specialists> specialistsList) {
        this.specialistsList = specialistsList;
    }

    @XmlTransient
    public List<Plans> getPlansList() {
        return plansList;
    }

    public void setPlansList(List<Plans> plansList) {
        this.plansList = plansList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeSpecialist)) {
            return false;
        }
        TypeSpecialist other = (TypeSpecialist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.TypeSpecialist[ id=" + id + " ]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        TypeSpecialist obj = null;
        obj = (TypeSpecialist) super.clone();
        return obj;
    }

}
