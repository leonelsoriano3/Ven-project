/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.keyPatients.UpdateKeyPatientStatusInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class UpdateKeyPatientStatusDAO extends AbstractCommandDAO<UpdateKeyPatientStatusInput> {

    public UpdateKeyPatientStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_U_001", 3);
    }

    @Override
    public void prepareInput(UpdateKeyPatientStatusInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKeyPatient());
        statement.setInt(2, input.getIdStatus());
        statement.setInt(3, input.getIdUser());
    }

}
