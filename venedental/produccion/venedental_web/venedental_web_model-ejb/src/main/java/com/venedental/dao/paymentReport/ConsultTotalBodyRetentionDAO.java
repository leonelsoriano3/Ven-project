/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;

import com.venedental.dto.paymentReport.ConsultTotalBodyRetentionOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class ConsultTotalBodyRetentionDAO extends AbstractQueryDAO<PaymentReportInput, ConsultTotalBodyRetentionOutput> {
    
    public ConsultTotalBodyRetentionDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_ConsultarCuerpoTotalesISLR", 2);
    }

    @Override
    public void prepareInput(PaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setString(2, input.getNumberISLR());
    }

    @Override
    public ConsultTotalBodyRetentionOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultTotalBodyRetentionOutput consultTotalBodyRetentionOutput = new ConsultTotalBodyRetentionOutput();
        consultTotalBodyRetentionOutput.setAmountISLR(rs.getDouble("total_monto_retencion_islr"));
        consultTotalBodyRetentionOutput.setDateISLR(rs.getDate("fecha_retencion_islr"));
        consultTotalBodyRetentionOutput.setCompanyName(rs.getString("nombre_empresa"));
        consultTotalBodyRetentionOutput.setCompanyAddress(rs.getString("direccion_empresa"));
        consultTotalBodyRetentionOutput.setCompanyRif(rs.getString("rif_empresa"));
        consultTotalBodyRetentionOutput.setSpecialistRif(rs.getString("rif_especialista"));
        consultTotalBodyRetentionOutput.setSpecialistName(rs.getString("nombre_especialista"));
        consultTotalBodyRetentionOutput.setSpecialistAddress(rs.getString("direccion_especialista"));
        consultTotalBodyRetentionOutput.setRetentionConcept(rs.getString("concepto_retencion"));
        consultTotalBodyRetentionOutput.setTotalAmountTotal(rs.getDouble("total_monto_total"));
        consultTotalBodyRetentionOutput.setTotalBaseRetention(rs.getDouble("total_base_retencion"));
        consultTotalBodyRetentionOutput.setTotalAmountRetentionISLR(rs.getDouble("total_monto_retencion_islr"));
        consultTotalBodyRetentionOutput.setNetPay(rs.getDouble("neto_pagar"));
        consultTotalBodyRetentionOutput.setNumberRetentionISLR(rs.getString("numero_retencion_islr"));
        consultTotalBodyRetentionOutput.setTotalMontoSustraendo(rs.getDouble("total_monto_sustraendo_islr"));
        consultTotalBodyRetentionOutput.setTotalNetoRetention(rs.getDouble("total_neto_retencion_islr"));
        return consultTotalBodyRetentionOutput;
    } 
}
