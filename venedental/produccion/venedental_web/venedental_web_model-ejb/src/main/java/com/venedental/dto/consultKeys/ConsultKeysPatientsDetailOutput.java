/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.consultKeys;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsDetailOutput {

    private String idKey, numberKey, idSpecialist, numberForIdentitySpecialist, nameSpecialist, specialitySpecialist, idPatient, namePatient;
    private String numberForIdentityPatient, idPlan, namePlan, nameEnte,nameConsultorio,nameAseguradora,parentesco,dateKeyForDetail;

    private Date dateKey, expiratioKey, brithayPatient;

    private String DateKeyForDetail,expirationKeyDetail,brithayPatientDetail;



    public String getIdKey() {
        return idKey;
    }

    public void setIdKey(String idKey) {
        this.idKey = idKey;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Date getDateKey() {
        return dateKey;
    }

    public void setDateKey(Date dateKey) {
        this.dateKey = dateKey;
    }

    public Date getExpiratioKey() {
        return expiratioKey;
    }

    public void setExpiratioKey(Date expiratioKey) {
        this.expiratioKey = expiratioKey;
    }

    public String getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(String idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getNumberForIdentitySpecialist() {
        return numberForIdentitySpecialist;
    }

    public void setNumberForIdentitySpecialist(String numberForIdentitySpecialist) {
        this.numberForIdentitySpecialist = numberForIdentitySpecialist;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public String getSpecialitySpecialist() {
        return specialitySpecialist;
    }

    public void setSpecialitySpecialist(String specialitySpecialist) {
        this.specialitySpecialist = specialitySpecialist;
    }

    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getNumberForIdentityPatient() {
        return numberForIdentityPatient;
    }

    public void setNumberForIdentityPatient(String numberForIdentityPatient) {
        this.numberForIdentityPatient = numberForIdentityPatient;
    }

    public String getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    }

    public Date getBrithayPatient() {
        return brithayPatient;
    }

    public void setBrithayPatient(Date brithayPatient) {
        this.brithayPatient = brithayPatient;
    }

    public String getNameEnte() {
        return nameEnte;
    }

    public void setNameEnte(String nameEnte) {
        this.nameEnte = nameEnte;
    }

    public String getNameConsultorio() {
        return nameConsultorio;
    }

    public void setNameConsultorio(String nameConsultorio) {
        this.nameConsultorio = nameConsultorio;
    }

    public String getNameAseguradora() {
        return nameAseguradora;
    }

    public void setNameAseguradora(String nameAseguradora) {
        this.nameAseguradora = nameAseguradora;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getDateKeyForDetail() {
        return dateKeyForDetail;
    }

    public void setDateKeyForDetail(String dateKeyForDetail) {
        this.dateKeyForDetail = dateKeyForDetail;
    }

    public String getExpirationKeyDetail() {
        return expirationKeyDetail;
    }

    public void setExpirationKeyDetail(String expirationKeyDetail) {
        this.expirationKeyDetail = expirationKeyDetail;
    }

    public String getBrithayPatientDetail() {
        return brithayPatientDetail;
    }

    public void setBrithayPatientDetail(String brithayPatientDetail) {
        this.brithayPatientDetail = brithayPatientDetail;
    }
    
    

}
