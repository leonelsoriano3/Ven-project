/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.plansPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planPatient.FilterPlanPatientInput;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPlanPatientByIdentityOrNameDAO extends AbstractQueryDAO<FilterPlanPatientInput, FilterPlanPatientOutput> {

    public FindPlanPatientByIdentityOrNameDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PATIENTS_G_003", 3);
    }

    @Override
    public void prepareInput(FilterPlanPatientInput input, CallableStatement statement) throws SQLException {
        statement.setString(1, input.getNameOrIdentiy());
        statement.setInt(2, input.getIdSpeciality());
        statement.setInt(3, input.getIdUser());
        
    }

    @Override
    public FilterPlanPatientOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        FilterPlanPatientOutput filterPlanPatientOutput = new FilterPlanPatientOutput();
        filterPlanPatientOutput.setIdPatient(rs.getInt("ID_PACIENTE"));
        filterPlanPatientOutput.setIdentityNumberPatient(rs.getInt("CEDULA_PACIENTE"));
        filterPlanPatientOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
        filterPlanPatientOutput.setIdInsurance(rs.getInt("ID_ASEGURADORA"));
        filterPlanPatientOutput.setNameInsurance(rs.getString("NOMBRE_ASEGURADORA"));
        filterPlanPatientOutput.setIdPlan(rs.getInt("ID_PLAN"));
        filterPlanPatientOutput.setNamePlan(rs.getString("NOMBRE_PLAN"));
        filterPlanPatientOutput.setIdRelationship(rs.getInt("ID_PARENTESCO"));
        filterPlanPatientOutput.setNameRelationship(rs.getString("NOMBRE_PARENTESCO"));
        filterPlanPatientOutput.setNameCompany(rs.getString("NOMBRE_ENTE"));
        filterPlanPatientOutput.setIdCompany(rs.getInt("ID_ENTE"));
        filterPlanPatientOutput.setBirthDatePatient(rs.getDate("FECHA_NACIMIENTO"));
        filterPlanPatientOutput.setIdStatusPatient(rs.getInt("ID_ESTATUS_PACIENTE"));
        filterPlanPatientOutput.setNameStatusPatient(rs.getString("NOMBRE_ESTATUS_PACIENTE"));
        filterPlanPatientOutput.setIdLine(rs.getInt("ID_LINEA"));
        filterPlanPatientOutput.setNameLine(rs.getString("ID_LINEA"));
        return filterPlanPatientOutput;
    }
}