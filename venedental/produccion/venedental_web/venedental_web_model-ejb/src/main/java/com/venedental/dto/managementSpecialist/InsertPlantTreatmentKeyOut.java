package com.venedental.dto.managementSpecialist;

/**
 * Created by akdeskdev90 on 15/07/16.
 */
public class InsertPlantTreatmentKeyOut {

    private Integer idTreatmentKey;
    private Integer idTreatmentKeyDiagnosis;
    private Integer idPropertiesPlanTreatmentKey;

    public InsertPlantTreatmentKeyOut() {
    }

    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public Integer getIdTreatmentKeyDiagnosis() {
        return idTreatmentKeyDiagnosis;
    }

    public void setIdTreatmentKeyDiagnosis(Integer idTreatmentKeyDiagnosis) {
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }

    public Integer getIdPropertiesPlanTreatmentKey() {
        return idPropertiesPlanTreatmentKey;
    }

    public void setIdPropertiesPlanTreatmentKey(Integer idPropertiesPlanTreatmentKey) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
    }
    
    
}
