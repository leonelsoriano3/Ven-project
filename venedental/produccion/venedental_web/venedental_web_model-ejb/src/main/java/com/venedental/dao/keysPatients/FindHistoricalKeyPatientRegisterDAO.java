/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.HistoricalPatientInput;
import com.venedental.dto.keyPatients.HistoricalPatientGCOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindHistoricalKeyPatientRegisterDAO  extends AbstractQueryDAO<HistoricalPatientInput, HistoricalPatientGCOutput> {

    public FindHistoricalKeyPatientRegisterDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_HistoricoClavesPacienteEspecialista", 2);
    }

    @Override
    public void prepareInput(HistoricalPatientInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPatient());
        statement.setInt(2, input.getIdSpecialist());
    }

    @Override
    public HistoricalPatientGCOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        HistoricalPatientGCOutput historicalPatientGCOutput = new HistoricalPatientGCOutput();
        historicalPatientGCOutput.setDateApplication(rs.getDate("FECHA_GENERACION_CLAVE"));
        historicalPatientGCOutput.setIdKeyPatient(rs.getInt("ID_CLAVE"));
        historicalPatientGCOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
        historicalPatientGCOutput.setIdTreatment(rs.getInt("ID_TRATAMIENTO"));
        historicalPatientGCOutput.setNameTratment(rs.getString("NOMBRE_TRATAMIENTO"));
        historicalPatientGCOutput.setIdPropertiePlantreatmentKey(rs.getInt("ID_PIEZA_TRATAMIENTO_CLAVE"));
        historicalPatientGCOutput.setNumberPropertie(rs.getString("NUMERO_PIEZA"));
        historicalPatientGCOutput.setIdSpecialist(rs.getInt("ID_ESPECIALISTA"));
        historicalPatientGCOutput.setSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
        historicalPatientGCOutput.setRelationShip(rs.getString("NOMBRE_PARENTESCO"));
        historicalPatientGCOutput.setInsurance(rs.getString("NOMBRE_ASEGURADORA"));
        historicalPatientGCOutput.setPlan(rs.getString("NOMBRE_PLAN"));
        historicalPatientGCOutput.setCompany(rs.getString("NOMBRE_COMPAÑIA"));
        return historicalPatientGCOutput;
    }

}