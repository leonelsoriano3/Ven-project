/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import java.io.File;

/**
 *
 * @author akdesk01
 */
public class SheetLoad {

    private final String sheetName;
    private final String fileName;
    private final FileLoad fileLoad;

    public SheetLoad(String fileName, String sheetName, FileLoad fileLoad) {
        this.fileName = fileName;
        this.sheetName = sheetName;
        this.fileLoad = fileLoad;
    }

    public String getSheetName() {
        return sheetName;
    }

    public String getFileName() {
        return fileName;
    }

    public File getSheetFile() {
        return new File(getSheetPath());
    }

    public String getSheetPath() {
        return fileLoad.getInsuranceLoad().getFolderPath() + "/" + fileName;
    }

    public FileLoad getFileLoad() {
        return fileLoad;
    }

}
