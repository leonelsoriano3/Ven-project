/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "patients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Patients.findAll", query = "SELECT p FROM Patients p"),
    @NamedQuery(name = "Patients.findById", query = "SELECT p FROM Patients p WHERE p.id = :id"),
    @NamedQuery(name = "Patients.findByIdentityNumber", query = "SELECT p FROM Patients p WHERE p.identityNumber = :identity_number AND p.status = 1"),
    @NamedQuery(name = "Patients.findByIdentityNumberHolder", query = "SELECT p FROM Patients p WHERE p.identityNumberHolder = :identityNumberHolder AND p.status = 1"),
    @NamedQuery(name = "Patients.findByFirstName", query = "SELECT p FROM Patients p WHERE p.firstName = :firstName"),
    @NamedQuery(name = "Patients.findBySecondName", query = "SELECT p FROM Patients p WHERE p.secondName = :secondName"),
    @NamedQuery(name = "Patients.findByLastname", query = "SELECT p FROM Patients p WHERE p.lastname = :lastname"),
    @NamedQuery(name = "Patients.findBySecondLastname", query = "SELECT p FROM Patients p WHERE p.secondLastname = :secondLastname"),
    @NamedQuery(name = "Patients.findByCompleteName", query = "SELECT p FROM Patients p WHERE p.completeName = :completeName"),
    @NamedQuery(name = "Patients.findByPhone", query = "SELECT p FROM Patients p WHERE p.phone = :phone"),
    @NamedQuery(name = "Patients.findByCellphone", query = "SELECT p FROM Patients p WHERE p.cellphone = :cellphone"),
    @NamedQuery(name = "Patients.findBySex", query = "SELECT p FROM Patients p WHERE p.sex = :sex"),
    @NamedQuery(name = "Patients.findByCivilStatus", query = "SELECT p FROM Patients p WHERE  p.civilStatus = :civilStatus"),
    @NamedQuery(name = "Patients.findByEmail", query = "SELECT p FROM Patients p WHERE p.email = :email"),
    @NamedQuery(name = "Patients.findByDateOfBirth", query = "SELECT p FROM Patients p WHERE p.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name =  "Patients.findByIdentityNumberAndRelationShip", query = "SELECT p FROM Patients p WHERE p.identityNumberHolder = :identity_number AND p.relationshipId.id = :relationship_id "),
//    @NamedQuery(name =  "Patients.findByIdentityNumberAndRelationShip", query = "SELECT p FROM Patients p WHERE p.identityNumberHolder = :identity_number AND p.relationshipId.id = :relationship_id AND p.status = 1"),
    @NamedQuery(name = "Patients.findBeneficiaries", query = "SELECT p FROM Patients p WHERE p.identityNumberHolder = :identity_number AND p.relationshipId.id NOT IN (10) AND p.status = 1"),
    @NamedQuery(name = "Patients.findByDuplicate", query = "SELECT p FROM Patients p WHERE p.identityNumber = :identityNumber AND p.identityNumberHolder = :identityNumberHolder AND p.status = 1 AND p.dateOfBirth = :dateOfBirth AND p.completeName = :completeName")
})
public class Patients implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "identity_number")
    private Integer identityNumber;

    @Column(name = "identity_number_holder")
    private Integer identityNumberHolder;

    @Size(max = 255)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 255)
    @Column(name = "second_name")
    private String secondName;

    @Column(name = "lastname")
    private String lastname;

    @Size(max = 255)
    @Column(name = "second_lastname")
    private String secondLastname;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "complete_name")
    private String completeName;

    @Size(max = 45)
    @Column(name = "phone")
    private String phone;

    @Size(max = 45)
    @Column(name = "cellphone")
    private String cellphone;

    @Size(max = 10)
    @Column(name = "sex")
    private String sex;

    @Size(max = 20)
    @Column(name = "civil_status")
    private String civilStatus;

    @Size(max = 45)
    @Column(name = "email")
    private String email;

    @Column(name = "status")
    private Integer status;

    @Basic(optional = false)
    @NotNull
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @Column(name = "insured_type")
    private Integer insuredType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patientsId")
    private List<BanksPatients> banksPatientsList;

    @JoinColumn(name = "relationship_id", referencedColumnName = "id")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Relationship relationshipId;

    @Transient
    private String dateOfBirthString;
    
    @Transient
    private Integer idRelationship;
    
    @Transient
    private String company;
    

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patients")
    private List<InsurancesPatients> insurancesPatientsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patientsId")
    private List<PlansPatients> plansPatientsList;

    @OneToMany(mappedBy = "patientsId")
    private List<KeysPatients> listKeys;

    @OneToMany(cascade = CascadeType.ALL,mappedBy="patientsId")
    private List<Reimbursement> reimbursementList;

    public Patients() {
    }

    public Patients(Integer id) {
        this.id = id;
    }

    public Patients(Integer id, String firstName, String lastname, Date dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdentityNumberHolder() {
        return identityNumberHolder;
    }

    public Relationship getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(Relationship relationshipId) {
        this.relationshipId = relationshipId;
    }

    public void setIdentityNumberHolder(Integer identityNumberHolder) {
        this.identityNumberHolder = identityNumberHolder;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondLastname() {
        return secondLastname;
    }

    public void setSecondLastname(String secondLastname) {
        this.secondLastname = secondLastname;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @XmlTransient
    public List<PlansPatients> getPlansPatientsList() {
        return plansPatientsList;
    }

    public void setPlansPatientsList(List<PlansPatients> plansPatientsList) {
        this.plansPatientsList = plansPatientsList;
    }

    public List<KeysPatients> getListKeys() {
        return listKeys;
    }

    public void setListKeys(List<KeysPatients> listKeys) {
        this.listKeys = listKeys;
    }

    public String getDateOfBirthString() {
        return dateOfBirthString;
    }

    public void setDateOfBirthString(String dateOfBirthString) {
        this.dateOfBirthString = dateOfBirthString;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patients)) {
            return false;
        }
        Patients other = (Patients) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.Patients[ id=" + id + " ]";
    }

    @XmlTransient
    public List<InsurancesPatients> getInsurancesPatientsList() {
        return insurancesPatientsList;
    }

    public void setInsurancesPatientsList(List<InsurancesPatients> insurancesPatientsList) {
        this.insurancesPatientsList = insurancesPatientsList;
    }

    public Integer getInsuredType() {
        return insuredType;
    }

    public void setInsuredType(Integer insuredType) {
        this.insuredType = insuredType;
    }

    @XmlTransient
    public List<BanksPatients> getBanksPatientsList() {
        return banksPatientsList;
    }

    public void setBanksPatientsList(List<BanksPatients> banksPatientsList) {
        this.banksPatientsList = banksPatientsList;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(Integer idRelationship) {
        this.idRelationship = idRelationship;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
