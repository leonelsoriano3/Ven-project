/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BanksPatients;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESK12
 */
@Local
public interface BanksPatientsFacadeLocal {

    BanksPatients create(BanksPatients banksPatients);

    BanksPatients edit(BanksPatients banksPatients);

    void remove(BanksPatients banksPatients);

    BanksPatients find(Object id);

    List<BanksPatients> findAll();

    List<BanksPatients> findRange(int[] range);
    
    List<BanksPatients> findByAccountNumber(String accountNumber);

    int count();
    
}
