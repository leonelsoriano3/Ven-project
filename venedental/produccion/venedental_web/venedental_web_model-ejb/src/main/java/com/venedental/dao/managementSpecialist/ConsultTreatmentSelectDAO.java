/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultTreatmentSelectOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultTreatmentSelectDAO extends AbstractQueryDAO<Integer, ConsultTreatmentSelectOutput> {
    
    public ConsultTreatmentSelectDAO(DataSource dataSource) {
        super(dataSource, "SP_T_TREATMENTS_G_005", 1);
    }
  @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public ConsultTreatmentSelectOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultTreatmentSelectOutput consultTreatmentSelectOutput = new ConsultTreatmentSelectOutput();
        consultTreatmentSelectOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        consultTreatmentSelectOutput.setIdCategory(rs.getInt("id_categoria"));
        consultTreatmentSelectOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultTreatmentSelectOutput.setNameCategory(rs.getString("nombre_categoria"));
        consultTreatmentSelectOutput.setTreatmentAssigned(rs.getInt("tratamiento_asignado"));
        return consultTreatmentSelectOutput;
    }
    
}
