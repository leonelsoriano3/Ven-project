/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;
import com.venedental.model.ScaleTreatmentsSpecialist;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ScaleTreatmentsSpecialistFacadeLocal {

    ScaleTreatmentsSpecialist create(ScaleTreatmentsSpecialist scale);

    ScaleTreatmentsSpecialist edit(ScaleTreatmentsSpecialist scale);

    void remove(ScaleTreatmentsSpecialist scale);

    ScaleTreatmentsSpecialist find(Object id);

    List<ScaleTreatmentsSpecialist> findAll();

    List<ScaleTreatmentsSpecialist> findRange(int[] range);
    
    ScaleTreatmentsSpecialist findByScale(Integer idScale,  Integer idSpecialist);
    
    List<ScaleTreatmentsSpecialist> findBySpecialist(Integer idSpecialist);

    int count();
    
}
