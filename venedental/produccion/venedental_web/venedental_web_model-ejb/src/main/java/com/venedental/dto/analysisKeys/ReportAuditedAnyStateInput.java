/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.sql.Date;

/**
 *
 * @author akdeskdev90
 */
public class ReportAuditedAnyStateInput {

    private Integer status;
    private Date startdate;
    private Date endDate;

    public ReportAuditedAnyStateInput(Integer status, Date startdate, Date endDate) {
        this.status = status;
        this.startdate = startdate;
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date dateFrom) {
        this.startdate = dateFrom;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date dateUntil) {
        this.endDate = dateUntil;
    }

}
