/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.SpecialistToAuditInput;
import com.venedental.dto.analysisKeys.SpecialistToAuditOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class SpecialistToAuditDAO extends AbstractQueryDAO<SpecialistToAuditInput, SpecialistToAuditOutput> {

    public SpecialistToAuditDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_EspecialistaAuditar", 3);
    }

    @Override
    public void prepareInput(SpecialistToAuditInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getStatusAnalysisKeysId());
        statement.setDate(2, (Date) input.getDateFrom());
        statement.setDate(3, (Date) input.getDateUntil());
    }

    @Override
    public SpecialistToAuditOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        SpecialistToAuditOutput registerSpecialistToAuditOutput = new SpecialistToAuditOutput();
        registerSpecialistToAuditOutput.setCompĺeteName(rs.getString("nombre_especialista"));
        registerSpecialistToAuditOutput.setDateInitiation(rs.getDate("fecha"));
        registerSpecialistToAuditOutput.setIdSpecialist(rs.getInt("id_especialista"));
        registerSpecialistToAuditOutput.setNameStatusAnalysisKeys(rs.getString("estatus"));
        return registerSpecialistToAuditOutput;
    }

}
