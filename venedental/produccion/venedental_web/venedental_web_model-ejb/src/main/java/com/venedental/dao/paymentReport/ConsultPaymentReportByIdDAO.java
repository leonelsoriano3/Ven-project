/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;

import com.venedental.dto.paymentReport.PaymentReportOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class ConsultPaymentReportByIdDAO extends AbstractQueryDAO<Integer, PaymentReportOutput> {
    
    public ConsultPaymentReportByIdDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_001", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public PaymentReportOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PaymentReportOutput paymentReportOutput = new PaymentReportOutput();
        paymentReportOutput.setAmount(rs.getDouble("monto"));
        paymentReportOutput.setAmountTotal(rs.getDouble("monto_total"));
        paymentReportOutput.setAuditedAmount(rs.getDouble("monto_auditado"));
        
        paymentReportOutput.setBillDate(rs.getDate("fecha_factura"));
        paymentReportOutput.setBillNumber(rs.getString("numero_factura"));
        paymentReportOutput.setControlNumber(rs.getString("numero_control"));
        paymentReportOutput.setIdBank(rs.getInt("id_banco"));
        paymentReportOutput.setBankName(rs.getString("nombre_banco")); 
        paymentReportOutput.setBankAccountNumber(rs.getString("numero_cuenta_bancaria"));
        
        paymentReportOutput.setIdBankAccount(rs.getInt("id_cuenta_bancaria"));
        paymentReportOutput.setIdReport(rs.getInt("id_reporte"));
        paymentReportOutput.setNumberPaymentReport(rs.getString("numero_reporte"));
        paymentReportOutput.setReportDate(rs.getDate("fecha_reporte"));
        paymentReportOutput.setTransferDate(rs.getDate("fecha_transferencia"));
        paymentReportOutput.setTransferNumber(rs.getString("numero_transferencia"));  
        paymentReportOutput.setIdSpecialist(rs.getInt("id_especialista"));
        paymentReportOutput.setNameSpecialist(rs.getString("nombre_especialista"));
        paymentReportOutput.setNumberRetentionISLR(rs.getString("numero_retencion_islr"));
        return paymentReportOutput;
    } 
}
