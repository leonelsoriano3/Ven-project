package com.venedental.dto;

import java.io.Serializable;

/**
 * Created by Luiscarrasco1991@gmail.com on 06/05/16.
 */
public class InsertUserDataInput implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;

    private String login;

    private String password;

    private String email;


    public InsertUserDataInput(String userName,String login, String password,String email) {
        this.email = email;
        this.password = password;
        this.login = login;
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
