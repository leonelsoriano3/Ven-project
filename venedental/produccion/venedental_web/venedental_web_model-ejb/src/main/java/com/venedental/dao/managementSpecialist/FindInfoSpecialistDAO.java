package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindInfoSpecialistOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis.carrasco@arkiteck.biz on 02/08/16.
 */
public class FindInfoSpecialistDAO extends AbstractQueryDAO<Integer, FindInfoSpecialistOutput> {


    public FindInfoSpecialistDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_013", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public FindInfoSpecialistOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {



        FindInfoSpecialistOutput findInfoSpecialistOutput = new FindInfoSpecialistOutput();
        findInfoSpecialistOutput.setNumKey(rs.getString("numero_clave"));
        findInfoSpecialistOutput.setIdKey(rs.getInt("id_clave"));
        findInfoSpecialistOutput.setApplicationDate(rs.getDate("fecha_solicitud"));
        findInfoSpecialistOutput.setExpiredDate(rs.getDate("fecha_vencimiento"));
        findInfoSpecialistOutput.setIdSpecialist(rs.getInt("id_especialista"));
        findInfoSpecialistOutput.setNameSpecialist(rs.getString("nombre_especialista"));
        findInfoSpecialistOutput.setIdPlan(rs.getInt("id_plan"));
        findInfoSpecialistOutput.setNamePlan(rs.getString("nombre_plan"));
        findInfoSpecialistOutput.setIdSpeciality(rs.getInt("id_especialidad"));
        findInfoSpecialistOutput.setNameSpeciality(rs.getString("nombre_especialidad"));
        findInfoSpecialistOutput.setIdPatient(rs.getInt("id_paciente"));
        findInfoSpecialistOutput.setNamePatient(rs.getString("nombre_paciente"));
        findInfoSpecialistOutput.setBirthDate(rs.getDate("fecha_nacimiento_paciente"));

        return findInfoSpecialistOutput;

    }
}