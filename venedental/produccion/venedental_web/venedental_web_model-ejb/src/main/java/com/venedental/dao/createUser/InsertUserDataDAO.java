package com.venedental.dao.createUser;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.InsertUserDataInput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by akdesk10 on 06/05/16.
 */
public class InsertUserDataDAO extends AbstractQueryDAO<InsertUserDataInput,Integer> {


    public InsertUserDataDAO(DataSource dataSource){
        super(dataSource,"SP_T_USERS_I_001",5);
    }

    @Override
    public void prepareInput(InsertUserDataInput input, CallableStatement statement) throws SQLException {

        try
        {
            if(input.getUserName() == null)
            {
                statement.setNull(1, Types.NULL);
            }
            else
            {
                statement.setString(1,input.getUserName());
            }

            if(input.getLogin() == null)
            {
                statement.setNull(2, Types.NULL);
            }
            else
            {
                statement.setString(2,input.getLogin());
            }

            if(input.getPassword() == null)
            {
                statement.setNull(3, Types.NULL);
            }
            else
            {
                statement.setString(3,input.getPassword());
            }

            if(input.getEmail()== null)
            {
                statement.setNull(4, Types.NULL);
            }
            else
            {
                statement.setString(4,input.getEmail());
            }
            statement.registerOutParameter(5, java.sql.Types.INTEGER);

        }
        catch(SQLException e)
        {
            System.out.println(e);
        }

    }

    @Override
    public Integer prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Integer userId;
        userId = statement.getInt(5);
        return userId;
    }
}
