/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PaymentReport;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author AKDESKDEV90
 */
@Stateless
public class PaymentReportFacade extends AbstractFacade<PaymentReport> implements PaymentReportFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaymentReportFacade() {
        super(PaymentReport.class);
    }

    /*Init StoredProcedure*/
    /**
     * Service that calls the stored read by specialty was , which returns the
     * reports of payments of the specialty was in a particular state method and
     * date    *
     * @param specialistId
     * @param statusId
     * @param dateFrom
     * @param dateUntil
     * @return List PaymentReport
     */
    @Override
    public List<PaymentReport> filterBySpecialistReportStatusAndDateRange(Integer specialistId, Integer statusId, Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_REPORTES_PAGOS_ESPECIALISTA", PaymentReport.class);
        query.registerStoredProcedureParameter("PI_ID_ESPECIALISTA", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
        query.setParameter("PI_ID_ESPECIALISTA", specialistId);
        query.setParameter("PI_ID_ESTATUS", statusId);
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);
        return query.getResultList();
    }

    /**
     * Service that calls the stored procedure that creates the sequential
     * number of the payment reporting     *
     * @param date
     * @return
     */
    @Override
    public String generateNumber(Date date) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery("SP_NUMERO_REPORTE_PAGO");
            query.registerStoredProcedureParameter("PI_FECHA", Date.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PS_NUMERO", String.class, ParameterMode.OUT);
            query.setParameter("PI_FECHA", date);
            query.execute();
            return (String) query.getOutputParameterValue("PS_NUMERO");
        } catch (NoResultException exception) {
            return null;
        }
    }

    /**
     * Service that calls the stored procedure that allows you to check if you
     * have already created a report payment for specialist
     *
     * @param specialistId
     * @param statusId
     * @param dateFrom
     * @param dateUntil
     * @param reportId
     * @return boolean
     */
    @Override
    public boolean isCreatedPaymentReport(Integer specialistId, Integer statusId, Date dateFrom, Date dateUntil, Integer reportId) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_CAMBIO_ESTATUS_REPORTE_PAGO_FECHAS_ESPECIALISTA");
            query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_ID_REPORTE", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_ID_ESPECIALISTA", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_ID_ESTATUS", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PS_CAMBIO", Boolean.class, ParameterMode.OUT);
            query.setParameter("PI_FECHA_DESDE", dateFrom);
            query.setParameter("PI_FECHA_HASTA", dateUntil);
            query.setParameter("PI_ID_REPORTE", reportId);
            query.setParameter("PI_ID_ESPECIALISTA", specialistId);
            query.setParameter("PI_ID_ESTATUS", statusId);
            query.execute();
            return (boolean) query.getOutputParameterValue("PS_CAMBIO");
        } catch (NoResultException exception) {
            return false;
        }
    }

    /*End StoredProcedure */
}
