/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;
import com.venedental.dao.patients.FindPatientByIdDAO;
import com.venedental.model.Patients;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServicePatients {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
    
        /**
     * Servicio que permites buscar los datos de un paciente dado su id.
     * @param idPatient
     * @return
     */
    public Patients findPatientById(Integer idPatient) {
        FindPatientByIdDAO findPatientByIdDAO = new FindPatientByIdDAO(ds);
        findPatientByIdDAO.execute(idPatient);
        return findPatientByIdDAO.getResult();
    }
   
}
