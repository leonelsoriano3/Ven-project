package com.venedental.dto.createUser;

/**
 * Created by akdesk10 on 09/06/16.
 */
public class LoginUserInput {

    private String name;

    private String lastName;


    public LoginUserInput(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {return name; }

    public void setName(String name) {this.name = name; }

    public String getLastName() {return lastName;}

    public void setLastName(String lastName) {this.lastName = lastName; }
}
