/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class KeysByParametersOutput {

    private Date applicationDate;
    private Integer idKey;
    private String numberKey;
    private Integer idSpecialist;
    private Integer identityNumberSpecialist;
    private String nameSpecialist;
    private Integer idPatient;
    private Integer identityNumberPatient;
    private String namePatient;
    private Integer idStatus;
    private String nameStatus;

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdentityNumberSpecialist() {
        return identityNumberSpecialist;
    }

    public void setIdentityNumberSpecialist(Integer identityNumberSpecialist) {
        this.identityNumberSpecialist = identityNumberSpecialist;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdentityNumberPatient() {
        return identityNumberPatient;
    }

    public void setIdentityNumberPatient(Integer identityNumberPatient) {
        this.identityNumberPatient = identityNumberPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }




}