/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.baremo;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.baremo.ConfigBaremoGeneralnput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;
/**
 *
 * @author akdesk01
 */
public class ConfigBaremoGeneralDAO extends AbstractCommandDAO<ConfigBaremoGeneralnput> {

    public ConfigBaremoGeneralDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SCALETREATME_U_002", 5);
    }

    @Override
    public void prepareInput(ConfigBaremoGeneralnput input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input.getIdTreatment());
        statement.setDate(2,(Date)input.getEffectiveDate());
        statement.setDouble(3,input.getBaremo());
        if (input.getIdSpecialist() == 0) {

            statement.setNull(4, java.sql.Types.NULL);

        } else {
            statement.setInt(4, input.getIdSpecialist());

        }
        statement.setInt(5,input.getIdUser());


    }
}
