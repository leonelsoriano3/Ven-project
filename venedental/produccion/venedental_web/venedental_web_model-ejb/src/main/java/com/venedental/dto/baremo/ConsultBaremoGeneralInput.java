/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.sql.Date;

/**
 *
 * @author akdesk01
 * DTO
 */
public class ConsultBaremoGeneralInput {
    private Date fechaInicio;
    private Date fechaFin;
    private Integer idEspecialidad;

    public ConsultBaremoGeneralInput(){

    }

    public ConsultBaremoGeneralInput(Date fechaInicio, Date fechaFin, Integer especialidad){
        this.fechaInicio=fechaInicio;
        this.fechaFin=fechaFin;
        this.idEspecialidad=especialidad;

    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    

}
