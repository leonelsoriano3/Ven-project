package com.venedental.dto.managementSpecialist;


import java.util.Date;

/**
 * Created by luis.carrasco@arkiteck.biz on 02/08/16.
 */
public class FindInfoSpecialistOutput {

    private Integer idKey;
    private String numKey;
    private Date applicationDate;
    private Date expiredDate;
    private Integer idSpecialist;
    private String nameSpecialist;
    private Integer idPlan;
    private String namePlan;
    private Integer idSpeciality;
    private String nameSpeciality;
    private Integer idPatient;
    private String namePatient;
    private Date birthDate;

    private String expDate;
    private String appDate;
    private String bDate;

    public String getAppDate() {return appDate;}

    public void setAppDate(String appDate) {this.appDate = appDate; }

    public String getbDate() {  return bDate;}

    public void setbDate(String bDate) {this.bDate = bDate;}

    public String getExpDate() {return expDate;}

    public void setExpDate(String expDate) {this.expDate = expDate; }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNameSpeciality() {
        return nameSpeciality;
    }

    public void setNameSpeciality(String nameSpeciality) {
        this.nameSpeciality = nameSpeciality;
    }

    public Integer getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(Integer idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getNumKey() {
        return numKey;
    }

    public void setNumKey(String numKey) {
        this.numKey = numKey;
    }

}
