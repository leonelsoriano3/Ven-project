/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;


/**
 *
 * @author akdeskdev90
 */
public class HistoricalPatientInput {

    Integer idPatient;
    Integer idKeyPatient;
    Integer idSpecialist;

    public HistoricalPatientInput(Integer idPatient, Integer idKeyPatient, Integer idSpecialist) {
        this.idPatient = idPatient;
        this.idKeyPatient = idKeyPatient;
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }




}