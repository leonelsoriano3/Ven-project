/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 * DTO 
 */
public class ConfirmKeyDetailsInput {    
    
    private Integer idSpecialist;
    private Integer idStatus;
    private Integer idMonth;
    private Date dateReception;
    
    private Integer idPaymentReport;
    private Date dateFrom;
    private Date dateUtil;

    public ConfirmKeyDetailsInput() {
    }

    public ConfirmKeyDetailsInput(Integer idSpecialist, Integer idStatus, Integer idMonth, Date dateReception) {
        this.idSpecialist = idSpecialist;
        this.idStatus = idStatus;
        this.idMonth = idMonth;
        this.dateReception = dateReception;
    }

    public ConfirmKeyDetailsInput(Integer idPaymentReport, Integer idStatus, Date dateFrom, Date dateUtil) {
        this.idPaymentReport = idPaymentReport;
        this.idStatus = idStatus;
        this.dateFrom = dateFrom;
        this.dateUtil = dateUtil;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }
    
    public Integer getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Integer idMonth) {
        this.idMonth = idMonth;
    }
    
    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateUtil() {
        return dateUtil;
    }

    public void setDateUtil(Date dateUtil) {
        this.dateUtil = dateUtil;
    }

    public Integer getIdPaymentReport() {
        return idPaymentReport;
    }

    public void setIdPaymentReport(Integer idPaymentReport) {
        this.idPaymentReport = idPaymentReport;
    }
    
    
}
