/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Relationship;
import com.venedental.model.security.Users;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author CarlosDaniel
 */
@Local
public interface RelationshipFacadeLocal {

    Relationship create(Relationship relationship);

    Relationship edit(Relationship relationship);

    void remove(Relationship users);

    Relationship find(Object id);

    List<Relationship> findAll();

    List<Relationship> findRange(int[] range);

    int count();

    List<Relationship> findById(Integer id);

}
