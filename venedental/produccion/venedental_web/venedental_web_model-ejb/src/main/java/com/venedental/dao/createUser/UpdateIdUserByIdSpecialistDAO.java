package com.venedental.dao.createUser;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.createUser.UpdateIdUserByIdSpecialistInput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.SQLException;

/**
 * Created by luis.carrasco@arkiteck.biz on 13/05/16.
 */
public class UpdateIdUserByIdSpecialistDAO extends AbstractCommandDAO<UpdateIdUserByIdSpecialistInput> {
    public UpdateIdUserByIdSpecialistDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_U_001", 2);
    }

    @Override
    public void prepareInput(UpdateIdUserByIdSpecialistInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdEspecialist());
        statement.setInt(2, input.getIdUser());
    }
}
