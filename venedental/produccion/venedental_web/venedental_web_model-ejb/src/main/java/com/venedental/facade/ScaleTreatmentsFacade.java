/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Clinics;
import com.venedental.model.Scale;
import com.venedental.model.ScaleTreatments;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class ScaleTreatmentsFacade extends AbstractFacade<ScaleTreatments> implements ScaleTreatmentsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScaleTreatmentsFacade() {
        super(ScaleTreatments.class);
    }

    @Override
    public List<ScaleTreatments> findByTypeScale(Integer idScale) {
        TypedQuery<ScaleTreatments> query = em.createNamedQuery("ScaleTreatments.findByTypeScale", ScaleTreatments.class);
        query.setParameter("id_type_scale", idScale);
        return query.getResultList();
    }

    @Override
    public ScaleTreatments findByTreatmentsAndSpecialist(Integer PI_ID_TRATAMIENTO, Integer PI_ID_ESPECIALISTA) {
        try {


	    String sql = "SELECT DISTINCT *"
		+ " FROM scale_treatments str inner join scale_treatments_specialist stsp on stsp.id_scale_treatments = str.id inner join treatments tre on str.id_treatments = tre.id"
		+ " inner join (SELECT str1.id_treatments id_tratamiento, stsp1.id_specialist id_especialista, max(str1.date_scale) fecha_baremo"
		+ " FROM scale_treatments str1 inner join scale_treatments_specialist stsp1 on stsp1.id_scale_treatments = str1.id"
		+ " WHERE str1.id_type_scale = 2 and stsp1.id_specialist = " + PI_ID_ESPECIALISTA + " GROUP BY str1.id_treatments, stsp1.id_specialist) "
		+ " ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.id_especialista = stsp.id_specialist and ultimo_baremo.fecha_baremo = str.date_scale"
		+ " WHERE str.id_treatments = " + PI_ID_TRATAMIENTO + " AND stsp.id_specialist = " + PI_ID_ESPECIALISTA + " AND str.id_type_scale = 2 ORDER BY str.id DESC LIMIT 1";


            /*String sql = "SELECT DISTINCT *"
                    + " FROM scale_treatments str inner join scale_treatments_specialist stsp on stsp.id_scale_treatments = str.id inner join treatments tre on str.id_treatments = tre.id"
                    + " inner join (SELECT str1.id_treatments id_tratamiento, stsp1.id_specialist id_especialista, max(str1.date_scale) fecha_baremo"
                    + " FROM scale_treatments str1 inner join scale_treatments_specialist stsp1 on stsp1.id_scale_treatments = str1.id"
                    + " WHERE str1.id_type_scale = 2 and stsp1.id_specialist = " + PI_ID_ESPECIALISTA + " GROUP BY str1.id_treatments, stsp1.id_specialist) "
                    + " ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.id_especialista = stsp.id_specialist and ultimo_baremo.fecha_baremo = str.date_scale"
                    + " WHERE str.id_treatments = " + PI_ID_TRATAMIENTO + " AND stsp.id_specialist = " + PI_ID_ESPECIALISTA + " AND str.id_type_scale = 2";*/

            Query query = em.createNativeQuery(sql, ScaleTreatments.class);
            return (ScaleTreatments) query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public List<ScaleTreatments> findBySpecialist(Integer PI_ID_ESPECIALISTA) {
        try {
            
  
         
        String sql="SELECT DISTINCT *"
        +"FROM scale_treatments str inner join scale_treatments_specialist stsp on stsp.id_scale_treatments = str.idinner join treatments tre on str.id_treatments = tre.id"
        +"inner join (SELECT  str1.id_treatments id_tratamiento, stsp1.id_specialist id_especialista, max(str1.date_scale) fecha_baremo"
        +"FROM scale_treatments str1 inner join scale_treatments_specialist stsp1 on stsp1.id_scale_treatments = str1.id "
        +"WHERE str1.id_type_scale = 2 and stsp1.id_specialist = "+PI_ID_ESPECIALISTA+" GROUP BY str1.id_treatments, stsp1.id_specialist)"
        +"ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.id_especialista = stsp.id_specialist and ultimo_baremo.fecha_baremo = str.date_scale"
        +"WHERE (stsp.id_specialist = "+PI_ID_ESPECIALISTA+" AND str.id_type_scale = 2) ORDER BY tre.name";
            
        Query query = em.createNativeQuery(sql, ScaleTreatments.class);
       return query.getResultList();
       } catch (NoResultException exception) {
            return null;
        } 
    }   
    
    
   
    
    @Override
    public ScaleTreatments findByTreatments(Integer PI_ID_TRATAMIENTO) {
        try {

            String sql = "SELECT DISTINCT * FROM scale_treatments str"
                    + " inner join (SELECT str1.id_treatments id_tratamiento, max(str1.date_scale) fecha_baremo"
                    + " FROM scale_treatments str1 WHERE (str1.id_type_scale = 1 and datediff(current_date(),str1.date_scale)>=0)"
                    + " GROUP BY str1.id_treatments) ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.fecha_baremo = str.date_scale"
                    + " inner join treatments tre on str.id_treatments = tre.id WHERE str.id_treatments = " + PI_ID_TRATAMIENTO + " AND str.id_type_scale = 1"
                    + " and str.id = (select max(id) from scale_treatments where id_treatments = str.id_treatments and date_scale = str.date_scale and id_type_scale = 1)";

            Query query = em.createNativeQuery(sql, ScaleTreatments.class);
            return (ScaleTreatments) query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public List<ScaleTreatments> findByTypeSpeciality(int idTypeScale, int idTypeSpeciality) {
        String sql = "SELECT DISTINCT * FROM scale_treatments str "
                + " inner join (SELECT str1.id_treatments id_tratamiento, max(str1.date_scale) fecha_baremo FROM scale_treatments str1"
                + " WHERE (str1.id_type_scale = " + idTypeScale + ") GROUP BY str1.id_treatments) ultimo_baremo on ultimo_baremo.id_tratamiento = str.id_treatments and ultimo_baremo.fecha_baremo = str.date_scale "
                + " inner join treatments tre on str.id_treatments = tre.id and (str.id_type_scale = " + idTypeScale + ")"
                + " right join type_specialist tsp on tsp.id = tre.id_type_speciality WHERE (tre.id_type_speciality = " + idTypeSpeciality + ") ORDER BY tre.name";
        Query query = em.createNativeQuery(sql, ScaleTreatments.class);
        query.setParameter("idTypeScale", idTypeScale);
        query.setParameter("idTypeSpeciality", idTypeSpeciality);
        return query.getResultList();
    }

    @Override
    public ScaleTreatments findByTreatmentSpecialistAndDate(Integer idTreatment, Integer specialistId, Date dateTreatment) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_BAREMO_TRATAMIENTO_ESPECIALISTA_FECHA", ScaleTreatments.class);
            query.registerStoredProcedureParameter("PI_ID_TRATAMIENTO", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_ID_ESPECIALISTA", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_FECHA_TRATAMIENTO", Date.class, ParameterMode.IN);
            query.setParameter("PI_ID_TRATAMIENTO", idTreatment);
            query.setParameter("PI_ID_ESPECIALISTA", specialistId);
            query.setParameter("PI_FECHA_TRATAMIENTO", dateTreatment);
            return (ScaleTreatments) query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public ScaleTreatments findByTreatmentAndDate(Integer idTreatment, Date dateTreatment) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_BAREMO_TRATAMIENTO_GENERAL_FECHA", ScaleTreatments.class);
            query.registerStoredProcedureParameter("PI_ID_TRATAMIENTO", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_FECHA_TRATAMIENTO", Date.class, ParameterMode.IN);
            query.setParameter("PI_ID_TRATAMIENTO", idTreatment);
            query.setParameter("PI_FECHA_TRATAMIENTO", dateTreatment);
            return (ScaleTreatments) query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

}
