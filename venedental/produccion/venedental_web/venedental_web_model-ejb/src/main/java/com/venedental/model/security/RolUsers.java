/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Edgar
 */
@Entity
@Table(name = "rol_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolUsers.findAll", query = "SELECT r FROM RolUsers r"),
    @NamedQuery(name = "RolUsers.findByRolId", query = "SELECT r FROM RolUsers r WHERE r.rolUsersPK.rolId = :rolId"),
    @NamedQuery(name = "RolUsers.findByUserId", query = "SELECT r FROM RolUsers r WHERE r.rolUsersPK.userId = :userId and r.status = 1"),
    @NamedQuery(name = "RolUsers.findRolUserByUserRol", query = "SELECT r FROM RolUsers r WHERE r.rolUsersPK.userId = :userId and r.status = 1 and r.rol.id = :rolId"),
    @NamedQuery(name = "RolUsers.findByStatus", query = "SELECT r FROM RolUsers r WHERE r.status = :status")})
public class RolUsers implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected RolUsersPK rolUsersPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;
    
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;
    
    @JoinColumn(name = "rol_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Rol rol;

    public RolUsers() {
    }

    public RolUsers(RolUsersPK rolUsersPK) {
        this.rolUsersPK = rolUsersPK;
    }

    public RolUsers(RolUsersPK rolUsersPK, int status) {
        this.rolUsersPK = rolUsersPK;
        this.status = status;
    }

    public RolUsers(Integer rolId, Integer  userId) {
        this.rolUsersPK = new RolUsersPK(rolId, userId);
    }

    public RolUsersPK getRolUsersPK() {
        return rolUsersPK;
    }

    public void setRolUsersPK(RolUsersPK rolUsersPK) {
        this.rolUsersPK = rolUsersPK;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer  status) {
        this.status = status;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolUsersPK != null ? rolUsersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolUsers)) {
            return false;
        }
        RolUsers other = (RolUsers) object;
        if ((this.rolUsersPK == null && other.rolUsersPK != null) || (this.rolUsersPK != null && !this.rolUsersPK.equals(other.rolUsersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.RolUsers[ rolUsersPK=" + rolUsersPK + " ]";
    }

}
