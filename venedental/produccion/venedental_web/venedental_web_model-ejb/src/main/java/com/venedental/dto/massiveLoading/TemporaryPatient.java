/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class TemporaryPatient {

    private int identityNumber;
    private int identityNumberHolder;
    private int relationshipId;
    private String completeName;
    private String sex;
    private Date dateOfBirth;
    private int idInsurances;
    private int idPlan;
    private String company;

    public int getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }

    public int getIdentityNumberHolder() {
        return identityNumberHolder;
    }

    public void setIdentityNumberHolder(int identityNumberHolder) {
        this.identityNumberHolder = identityNumberHolder;
    }

    public int getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(int relationshipId) {
        this.relationshipId = relationshipId;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIdInsurances() {
        return idInsurances;
    }

    public void setIdInsurances(int idInsurances) {
        this.idInsurances = idInsurances;
    }

    public int getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(int idPlan) {
        this.idPlan = idPlan;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

}
