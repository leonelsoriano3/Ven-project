/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;

/**
 *
 * @author ARKDEV16
 */
public class ReadInsurancesReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer idInsurance;
    
    private String name;
    
    public Integer getIdInsurance() {
        return idInsurance;
    }
    
    public void setIdInsurance(Integer idInsurance) {
        this.idInsurance = idInsurance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}