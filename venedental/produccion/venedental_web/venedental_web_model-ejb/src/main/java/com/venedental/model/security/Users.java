/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.security;

import com.venedental.model.BinnacleKeys;
import com.venedental.model.BinnaclePlansTreatmentsKeys;
import com.venedental.model.BinnaclePropertiesTreatmentsKeys;
import com.venedental.model.Specialists;
import com.venedental.model.security.Employee;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Edgar
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
    @NamedQuery(name = "Users.findByName", query = "SELECT u FROM Users u WHERE u.name = :name"),
    @NamedQuery(name = "Users.findByLogin", query = "SELECT u FROM Users u WHERE u.login = :login"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password"),
    @NamedQuery(name = "Users.findByStatus", query = "SELECT u FROM Users u WHERE u.status = :status"),
    @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")})
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "login")
    private String login;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private Integer status;

    @Size(max = 100)
    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "idUsers")
    private List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList;

    @OneToMany(mappedBy = "idUsers")
    private List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList;

    @OneToMany(mappedBy = "idUsersKeys")
    private List<BinnacleKeys> binnacleKeysList;

    @OneToMany(mappedBy = "usersId")
    private List<Specialists> specialistsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private List<RolUsers> listRolUsers;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId")
    private List<Employee> listEmployee;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private List<InsurancesUsers> listInsurancesUsers;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private List<ClinicsUsers> listClinicsUsers;

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Users(Integer id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Employee> getListEmployee() {
        return listEmployee;
    }

    public void setListEmployee(List<Employee> listEmployee) {
        this.listEmployee = listEmployee;
    }

    public List<RolUsers> getListRolUsers() {
        return listRolUsers;
    }

    public void setListRolUsers(List<RolUsers> listRolUsers) {
        this.listRolUsers = listRolUsers;
    }

    public List<InsurancesUsers> getListInsurancesUsers() {
        return listInsurancesUsers;
    }

    public void setListInsurancesUsers(List<InsurancesUsers> listInsurancesUsers) {
        this.listInsurancesUsers = listInsurancesUsers;
    }

    public List<ClinicsUsers> getListClinicsUsers() {
        return listClinicsUsers;
    }

    public void setListClinicsUsers(List<ClinicsUsers> listClinicsUsers) {
        this.listClinicsUsers = listClinicsUsers;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.Users[ id=" + id + " ]";
    }

    @XmlTransient
    public List<Specialists> getSpecialistsList() {
        return specialistsList;
    }

    public void setSpecialistsList(List<Specialists> specialistsList) {
        this.specialistsList = specialistsList;
    }

    @XmlTransient
    public List<BinnacleKeys> getBinnacleKeysList() {
        return binnacleKeysList;
    }

    public void setBinnacleKeysList(List<BinnacleKeys> binnacleKeysList) {
        this.binnacleKeysList = binnacleKeysList;
    }

    @XmlTransient
    public List<BinnaclePlansTreatmentsKeys> getBinnaclePlansTreatmentsKeysList() {
        return binnaclePlansTreatmentsKeysList;
    }

    public void setBinnaclePlansTreatmentsKeysList(List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList) {
        this.binnaclePlansTreatmentsKeysList = binnaclePlansTreatmentsKeysList;
    }

    @XmlTransient
    public List<BinnaclePropertiesTreatmentsKeys> getBinnaclePropertiesTreatmentsKeysList() {
        return binnaclePropertiesTreatmentsKeysList;
    }

    public void setBinnaclePropertiesTreatmentsKeysList(List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList) {
        this.binnaclePropertiesTreatmentsKeysList = binnaclePropertiesTreatmentsKeysList;
    }
}
