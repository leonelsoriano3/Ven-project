/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConsultPaymentProcessInput;
import com.venedental.dto.analysisKeys.ConsultPaymentProcessOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
/**
 *
 * @author root
 */
public class ConsultPaymentProcessDAO extends AbstractQueryDAO<ConsultPaymentProcessInput, ConsultPaymentProcessOutput> {
    
    public ConsultPaymentProcessDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_002", 3);
    }

    @Override
    public void prepareInput(ConsultPaymentProcessInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdStatus());
        statement.setDate(2, (Date) input.getStartDate());
        statement.setDate(3, (Date) input.getEndDate());
    }

    @Override
    public ConsultPaymentProcessOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultPaymentProcessOutput paymentProcessOutput = new ConsultPaymentProcessOutput();
        paymentProcessOutput.setDoctorClinit(rs.getString("nombre_especialista"));
        paymentProcessOutput.setCelRif(rs.getString("cedula_rif_especialista"));
        
        if(!rs.getString("nombre_banco").equals("")){
           paymentProcessOutput.setBank(rs.getString("nombre_banco")); 
        }else{
           paymentProcessOutput.setBank("-");  
        }
        
        if(!rs.getString("numero_cuenta").equals("")){
           paymentProcessOutput.setAccountNumber(rs.getString("numero_cuenta"));
        }else{
           paymentProcessOutput.setAccountNumber("-");  
        }
        
        paymentProcessOutput.setAssociatedCelRif(rs.getString("cedula_rif_cuenta"));
        paymentProcessOutput.setNameStatus(rs.getString("nombre_estatus"));
        paymentProcessOutput.setIdStatus(rs.getInt("id_estatus"));
        paymentProcessOutput.setIdReport(rs.getInt("id_reporte"));
        paymentProcessOutput.setNumberReport(rs.getString("numero_reporte"));
        paymentProcessOutput.setNumberRetention(rs.getString("numero_retencion_islr"));
        paymentProcessOutput.setIsNatural(rs.getInt("es_persona_natural"));
        
        return paymentProcessOutput;
    }
    
}
