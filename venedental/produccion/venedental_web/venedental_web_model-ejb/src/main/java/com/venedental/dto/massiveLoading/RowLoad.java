/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import java.util.List;

/**
 *
 * @author akdesk01
 */
public class RowLoad {

    private final long numberRow;
    private final List<String> data;
    private final SheetLoad sheetLoad;
    private String messageError;

    public RowLoad(long numberRow, List<String> data, SheetLoad sheetLoad) {
        this.numberRow = numberRow;
        this.data = data;
        this.sheetLoad = sheetLoad;
        this.messageError = null;
    }

    public long getNumberRow() {
        return numberRow;
    }

    public List<String> getData() {
        return data;
    }

    public SheetLoad getSheetLoad() {
        return sheetLoad;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }
    
    public boolean hasError(){
        return messageError != null;
    }

    public String getFileSheet() {
        return String.format("%s\t%s", sheetLoad.getFileLoad().getFileName(), sheetLoad.getSheetName());
    }

}
