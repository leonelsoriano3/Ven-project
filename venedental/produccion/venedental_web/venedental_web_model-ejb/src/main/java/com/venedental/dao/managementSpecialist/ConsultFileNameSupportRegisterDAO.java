package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdesk01 on 21/12/16.
 */
public class ConsultFileNameSupportRegisterDAO extends AbstractQueryDAO<Integer, String> {
    public ConsultFileNameSupportRegisterDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEYSUP_G_001", 1);
    }
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public String prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        String nameFile;
        nameFile=(rs.getString("nombre_archivo_soporte"));

        return nameFile;
    }

}
