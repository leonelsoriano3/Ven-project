/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class HistoricalDetailsPatientInput {

    private Integer idPatient;
    private Integer idSpecialist;
    private Date dateFrom;
    private Date dateTo;


    public HistoricalDetailsPatientInput() {
    }

    public HistoricalDetailsPatientInput(Integer idPatient, Integer idSpecialist) {
        this.idPatient = idPatient;
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }


}