/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.consultKeys;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsInput {
    
    private String patient,dateStart, dateEnd,numberForKey,specialist;
    private Integer idstatusKey;

    public ConsultKeysPatientsInput() {
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getNumberForKey() {
        return numberForKey;
    }

    public void setNumberForKey(String numberForKey) {
        this.numberForKey = numberForKey;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public Integer getIdstatusKey() {
        return idstatusKey;
    }

    public void setIdstatusKey(Integer idstatusKey) {
        this.idstatusKey = idstatusKey;
    }
    
    
    
}
