/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planPatient;

/**
 *
 * @author akdeskdev90
 */
public class FilterPlanPatientInput {
    private String nameOrIdentiy;
    private Integer idSpeciality;
    private Integer idUser;

    public FilterPlanPatientInput(String nameOrIdentiy, Integer idSpeciality, Integer idUser) {
        this.nameOrIdentiy = nameOrIdentiy;
        this.idSpeciality = idSpeciality;
        this.idUser=idUser;
    }

    public String getNameOrIdentiy() {
        return nameOrIdentiy;
    }

    public void setNameOrIdentiy(String nameOrIdentiy) {
        this.nameOrIdentiy = nameOrIdentiy;
    }

    public Integer getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(Integer idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    
}