/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultDateDetailTreatmentOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
/**
 *
 * @author akdesk01
 */
public abstract class ConsultDatePatientsDetailsTreatmentDAO extends AbstractQueryDAO<Integer, ConsultDateDetailTreatmentOutput>{
    
     public ConsultDatePatientsDetailsTreatmentDAO (DataSource dataSource) {
        super(dataSource, "SP_M_ESP_ConsultarClaveDetalle", 3);
    }
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        statement.registerOutParameter(2, java.sql.Types.DATE);
        statement.registerOutParameter(3, java.sql.Types.DATE);
         
    }

    
    @Override
    public ConsultDateDetailTreatmentOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException{
    ConsultDateDetailTreatmentOutput consultDatePatientsDetailTreatmentOutput = new ConsultDateDetailTreatmentOutput();

   Date dateMinTreatment = statement.getDate(2);
       Date dateMiaxTreatment = statement.getDate(3);
    consultDatePatientsDetailTreatmentOutput.setDateMinTreatment(dateMinTreatment);
       consultDatePatientsDetailTreatmentOutput.setDateMiaxTreatment(dateMiaxTreatment);
    return consultDatePatientsDetailTreatmentOutput;
    
    }
}
