package com.venedental.dto.reportAuditConsultTrace;

/**
 * Created by luis.carrasco1991@arkiteck.biz on 15/07/16.
 */

/*


PROCEDURE `SP_T_KEYSPATIENTS_G_014`(IN PI_ID_USUARIO INT)

Parámetros de Ingreso:
1. PI_ID_USUARIO. ID del usuario especialista conectado. Es obligatorio.
Parámetros de Salida:
1. numero_reporte_tratamiento
2. id_reporte_pago
3. numero_reporte_pago
4. id_clave
5. numero_clave
6. nombre_tratamiento
7. numero_pieza
8. nombre_estatus
9. observacion


 */
public class AuditConsultTraceOutput {

    private Integer numReportTreatment;

    private Integer idPaymentReport;

    private String numPaymentReport;

    private Integer idKey;

    private String numKey;

    private String treatmentName;

    private String numTooth;

    private String statusName;

    private String observation;

    private String isVisible;


    public AuditConsultTraceOutput(Integer nrt,Integer ipr,String npr,Integer ik,String nk,String tn,Integer nt,String sn,String obs,String numT){

        this.numReportTreatment = nrt;
        this.idPaymentReport = ipr;
        this.numPaymentReport = npr;
        this.idKey = ik;
        this.numKey = nk;
        this.treatmentName = tn;
        this.statusName = sn;
        this.observation = obs;
        this.numTooth = numT;

        if(this.idPaymentReport == -1){

            this.isVisible = "false";
        }
        else{

            this.isVisible = "true";

        }

    }

    public AuditConsultTraceOutput(){}

    public String getVisible() {return isVisible;}

    public void setVisible(String visible) {isVisible = visible;}

    public Integer getNumReportTreatment() {return numReportTreatment;}

    public void setNumReportTreatment(Integer numReportTreatment) {this.numReportTreatment = numReportTreatment;}

    public Integer getIdPaymentReport() { return idPaymentReport; }

    public void setIdPaymentReport(Integer idPaymentReport) {this.idPaymentReport = idPaymentReport; }

    public String getNumPaymentReport() {return numPaymentReport;   }

    public void setNumPaymentReport(String numPaymentReport) {this.numPaymentReport = numPaymentReport;}

    public Integer getIdKey() { return idKey; }

    public void setIdKey(Integer idKey) { this.idKey = idKey;}

    public String getNumKey() { return numKey;}

    public void setNumKey(String numKey) {this.numKey = numKey;}

    public String getTreatmentName() { return treatmentName;}

    public void setTreatmentName(String treatmentName) { this.treatmentName = treatmentName;}

    public String getNumTooth() {return numTooth;}

    public void setNumTooth(String numTooth) {this.numTooth = numTooth;}

    public String getStatusName() {return statusName;}

    public void setStatusName(String statusName) { this.statusName = statusName;}

    public String getObservation() {return observation;}

    public void setObservation(String observation) {this.observation = observation;}


}
