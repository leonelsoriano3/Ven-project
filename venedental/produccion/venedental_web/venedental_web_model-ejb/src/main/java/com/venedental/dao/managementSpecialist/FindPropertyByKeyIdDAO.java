package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindPropertyByKeyIdIn;
import com.venedental.dto.managementSpecialist.FindPropertyByKeyIdOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 * modificated by luiscarrasco1991@gmail.com  on 17/10/16
 */
public class FindPropertyByKeyIdDAO extends AbstractQueryDAO<FindPropertyByKeyIdIn,
        FindPropertyByKeyIdOut> {


    public FindPropertyByKeyIdDAO(DataSource dataSource) {
        super(dataSource,"SP_T_PROPERTIES_G_004", 2);
    }


    @Override
    public void prepareInput(FindPropertyByKeyIdIn input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKey());
        statement.setInt(2, input.getIdTooth());
    }

    @Override
    public FindPropertyByKeyIdOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        FindPropertyByKeyIdOut findPropertyByKeyIdOut = new FindPropertyByKeyIdOut();

        findPropertyByKeyIdOut.setIdPlanTreatment(rs.getInt("id_plan_tratamiento"));
        findPropertyByKeyIdOut.setIdPieceTreatment(rs.getInt("id_pieza_tratamiento"));
        findPropertyByKeyIdOut.setIdBaremoTreatment(rs.getInt("id_baremo_tratamiento"));
        findPropertyByKeyIdOut.setIdBaremoPiece(rs.getInt("id_baremo_pieza"));
        findPropertyByKeyIdOut.setIdTreatment(rs.getInt("id_tratamiento"));
        findPropertyByKeyIdOut.setNameTreatment(rs.getString("nombre_tratamiento"));
        findPropertyByKeyIdOut.setIdPiece(rs.getInt("id_pieza"));
        findPropertyByKeyIdOut.setValuePiece(rs.getInt("valor_pieza"));
        findPropertyByKeyIdOut.setSelected(false);

        return findPropertyByKeyIdOut;
    }
}

