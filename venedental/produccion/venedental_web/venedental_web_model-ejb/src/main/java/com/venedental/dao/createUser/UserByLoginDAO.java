package com.venedental.dao.createUser;


import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.createUser.UserByLoginDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 11/05/16.
 */
public class UserByLoginDAO extends AbstractQueryDAO<String, UserByLoginDTO> {


    public UserByLoginDAO(DataSource dataSource){
        super(dataSource,"SP_T_USERS_G_002",1);
    }


    @Override
    public void prepareInput(String input, CallableStatement statement) throws SQLException {
        statement.setString(1, input);
    }

    @Override
    public UserByLoginDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        UserByLoginDTO userByLoginDTO = new UserByLoginDTO();
        userByLoginDTO.setId(rs.getInt("id"));
        userByLoginDTO.setName(rs.getString("name"));
        userByLoginDTO.setLogin(rs.getString("login"));
        userByLoginDTO.setPassword(rs.getString("password"));
        userByLoginDTO.setStatus(rs.getInt("status"));
        userByLoginDTO.setEmail(rs.getString("email"));
        return userByLoginDTO;
    }
}
