/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPlanTreatKeyByReportInvoicesTransferInput {
    
    int idReport;
    int idStatus;
    Date dateFrom;
    Date dateUntil;

    public FindPlanTreatKeyByReportInvoicesTransferInput(int idReport, int idStatus, Date dateFrom, Date dateUntil) {
        this.idReport = idReport;
        this.idStatus = idStatus;
        this.dateFrom = dateFrom;
        this.dateUntil = dateUntil;
    }

    public int getIdReport() {
        return idReport;
    }

    public void setIdReport(int idReport) {
        this.idReport = idReport;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(Date dateUtil) {
        this.dateUntil = dateUtil;
    }
 
}
