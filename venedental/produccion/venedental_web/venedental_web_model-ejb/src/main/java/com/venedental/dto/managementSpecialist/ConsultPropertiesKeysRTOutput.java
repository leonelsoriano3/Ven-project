/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesKeysRTOutput {
    private String nameTreatment,value_piece;
    private Integer id_treatment_key,id_piece_treatment_key,id_treatment,id_piece;
    private Date dateOrigin;
    private Integer reportePago;
    private List <ConsultNameFileOutput> listRecaudos;
    private String date;

    public ConsultPropertiesKeysRTOutput() {
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getValue_piece() {
        return value_piece;
    }

    public void setValue_piece(String value_piece) {
        this.value_piece = value_piece;
    }

    public Integer getId_treatment_key() {
        return id_treatment_key;
    }

    public void setId_treatment_key(Integer id_treatment_key) {
        this.id_treatment_key = id_treatment_key;
    }

    public Integer getId_piece_treatment_key() {
        return id_piece_treatment_key;
    }

    public void setId_piece_treatment_key(Integer id_piece_treatment_key) {
        this.id_piece_treatment_key = id_piece_treatment_key;
    }

    public Integer getId_treatment() {
        return id_treatment;
    }

    public void setId_treatment(Integer id_treatment) {
        this.id_treatment = id_treatment;
    }

    public Integer getId_piece() {
        return id_piece;
    }

    public void setId_piece(Integer id_piece) {
        this.id_piece = id_piece;
    }

    public Date getDateOrigin() {
        return dateOrigin;
    }

    public void setDateOrigin(Date dateOrigin) {
        this.dateOrigin = dateOrigin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    

    public Integer getReportePago() {
        return reportePago;
    }

    public void setReportePago(Integer reportePago) {
        this.reportePago = reportePago;
    }

    public List<ConsultNameFileOutput> getListRecaudos() {
        return listRecaudos;
    }

    public void setListRecaudos(List<ConsultNameFileOutput> listRecaudos) {
        this.listRecaudos = listRecaudos;
    }

    

    

   

    

    
    
    
    
    
    
}
