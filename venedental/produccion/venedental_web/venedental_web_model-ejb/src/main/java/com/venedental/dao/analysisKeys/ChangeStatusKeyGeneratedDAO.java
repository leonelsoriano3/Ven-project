package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.analysisKeys.ChangeStatusKeyGeneratedInput;

import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * @author akdeskdev90
 */
public class ChangeStatusKeyGeneratedDAO extends  AbstractCommandDAO<ChangeStatusKeyGeneratedInput>{

    public ChangeStatusKeyGeneratedDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_U_002", 2);
    }

    @Override
    public void prepareInput(ChangeStatusKeyGeneratedInput input, CallableStatement statement) throws SQLException {

        statement.setInt(1,input.getIdKey());
        statement.setDate(2, new java.sql.Date(input.getReciveDate().getTime()));

    }


}
