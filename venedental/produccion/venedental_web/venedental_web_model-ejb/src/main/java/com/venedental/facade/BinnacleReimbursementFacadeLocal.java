/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnacleReimbursement;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESK12
 */
@Local
public interface BinnacleReimbursementFacadeLocal {

    BinnacleReimbursement create(BinnacleReimbursement binnacleReimbursement);

    BinnacleReimbursement edit(BinnacleReimbursement binnacleReimbursement);

    void remove(BinnacleReimbursement binnacleReimbursement);

    BinnacleReimbursement find(Object id);

    List<BinnacleReimbursement> findAll();

    List<BinnacleReimbursement> findRange(int[] range);
    
    List<BinnacleReimbursement> findAllByReimbursement(int reimbursement_id);

    int count();
    
}
