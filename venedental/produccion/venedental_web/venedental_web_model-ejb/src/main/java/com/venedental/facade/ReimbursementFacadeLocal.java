/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Reimbursement;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESK12
 */
@Local
public interface ReimbursementFacadeLocal {

    Reimbursement create(Reimbursement reimbursement);

    Reimbursement edit(Reimbursement reimbursement);

    void remove(Reimbursement reimbursement);

    Reimbursement find(Object id);
    
    String generateRequestNumber(Date date);

    List<Reimbursement> findAll();
    
    List<Reimbursement> findByStatus(int status);
    
    List<Reimbursement> findByStatusTransfer(int status);
    
    List<Reimbursement> findRange(int[] range);
  
    List<Reimbursement> findByTwoStatus(int idStatus1, int idStatus2);
    
    List<Reimbursement> findByThreeStatus(int idStatus1, int idStatus2, int idStatus3);
    
    List<Reimbursement> findByNumberRequest(String numberRequest);
    
    List<Reimbursement> searchReimbursementReportsbyStatus(Integer insuranceId,int statusId,Date startDate,Date endDate);
    
    List<Reimbursement> findByPatientsId(Integer patientsId);
    
    boolean patientHasReimbursement (Integer idPatient, Integer idPlan);
    
    int count();
    
}
