/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ARKDEV16
 */
public class ReadPropertiesReportInput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer idInsurance;
    
    private Integer idHealthArea;

    private Integer idPlan;

    private Integer idTreatment;

    public Integer getIdInsurance() {
        return idInsurance;
    }

    public void setIdInsurance(Integer idInsurance) {
        this.idInsurance = idInsurance;
    }

    public Integer getIdHealthArea() {
        return idHealthArea;
    }

    public void setIdHealthArea(Integer idHealthArea) {
        this.idHealthArea = idHealthArea;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }
    
    
}
