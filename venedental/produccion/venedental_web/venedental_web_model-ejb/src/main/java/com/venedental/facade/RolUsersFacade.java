/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.RolUsers;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Edgar Anzola
 */
@Stateless
public class RolUsersFacade extends AbstractFacade< RolUsers> implements RolUsersFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolUsersFacade() {
        super(RolUsers.class);
    }

    @Override
    public RolUsers edit(RolUsers entity) {
        return super.edit(entity);
    }

    @Override
    public RolUsers create(RolUsers entity) {
        return super.create(entity);
    }

    @Override
    public List<RolUsers> findByUserId(int id) {
        TypedQuery<RolUsers> query = em.createNamedQuery("RolUsers.findByUserId", RolUsers.class);
        query.setParameter("userId", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public RolUsers findRolUserByUserRol(Integer idUser, Integer idRol) {

        try {
            TypedQuery<RolUsers> query = em.createNamedQuery("RolUsers.findRolUserByUserRol", RolUsers.class);
            query.setParameter("userId", idUser);
            query.setParameter("rolId", idRol);
            return query.getSingleResult(); //To change body of generated methods, choose Tools | Templates.
        } catch (NoResultException exception) {
            return null;
        }
    }
}
