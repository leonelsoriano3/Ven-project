/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class FindDataSpecialistOutput {
    
    private String nameSpecialist,addressSpecislist,specialities,phoneSpecialist,nameSpeciality;
    private String email_especialista,nameMedicalOffice,addressMedicalOffice;
    private Integer id_Specialist,identityCardSpecialist,idSpeciality,medicalOfficeVarious,idMedicalOficce,id_user;

    public FindDataSpecialistOutput(){
    }

    

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    

    public String getAddressSpecislist() {
        return addressSpecislist;
    }

    public void setAddressSpecislist(String addressSpecislist) {
        this.addressSpecislist = addressSpecislist;
    }

    public String getSpecialties() {
        return specialities;
    }

    public void setSpecialties(String specialties) {
        this.specialities = specialties;
    }

    public String getPhoneSpecialist() {
        return phoneSpecialist;
    }

    public void setPhoneSpecialist(String phoneSpecialist) {
        this.phoneSpecialist = phoneSpecialist;
    }

    public Integer getId_Specialist() {
        return id_Specialist;
    }

    public void setId_Specialist(Integer id_Specialist) {
        this.id_Specialist = id_Specialist;
    }

    public Integer getIdentityCardSpecialist() {
        return identityCardSpecialist;
    }

    public void setIdentityCardSpecialist(Integer identityCardSpecialist) {
        this.identityCardSpecialist = identityCardSpecialist;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public String getNameSpeciality() {
        return nameSpeciality;
    }

    public void setNameSpeciality(String nameSpeciality) {
        this.nameSpeciality = nameSpeciality;
    }

    public Integer getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(Integer idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public String getNameMedicalOffice() {
        return nameMedicalOffice;
    }

    public void setNameMedicalOffice(String nameMedicalOffice) {
        this.nameMedicalOffice = nameMedicalOffice;
    }

    public String getAddressMedicalOffice() {
        return addressMedicalOffice;
    }

    public void setAddressMedicalOffice(String addressMedicalOffice) {
        this.addressMedicalOffice = addressMedicalOffice;
    }

    public String getEmail_especialista() {
        return email_especialista;
    }

    public void setEmail_especialista(String email_especialista) {
        this.email_especialista = email_especialista;
    }

    public Integer getMedicalOfficeVarious() {
        return medicalOfficeVarious;
    }

    public void setMedicalOfficeVarious(Integer medicalOfficeVarious) {
        this.medicalOfficeVarious = medicalOfficeVarious;
    }

    public Integer getIdMedicalOficce() {
        return idMedicalOficce;
    }

    public void setIdMedicalOficce(Integer idMedicalOficce) {
        this.idMedicalOficce = idMedicalOficce;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }
    
    
    
}
