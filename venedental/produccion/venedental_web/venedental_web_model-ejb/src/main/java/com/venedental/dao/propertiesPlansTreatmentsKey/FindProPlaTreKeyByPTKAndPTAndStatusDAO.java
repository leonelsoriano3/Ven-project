/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.propertiesPlanTreatmentKey.FindProPlaTreKeyByPlanTreKeyAndProTreaInput;
import com.venedental.dto.propertiesPlanTreatmentKey.PropertiesPlansTreatmentsKeyOutput;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindProPlaTreKeyByPTKAndPTAndStatusDAO extends AbstractQueryDAO<FindProPlaTreKeyByPlanTreKeyAndProTreaInput, PropertiesPlansTreatmentsKeyOutput> {

    public FindProPlaTreKeyByPTKAndPTAndStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_G_004", 3);
    }

    @Override
    public void prepareInput(FindProPlaTreKeyByPlanTreKeyAndProTreaInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPlanTreatmentKey());
        statement.setInt(2, input.getIdPropertieTreatment());
        statement.setInt(3, input.getIdStatus());
    }

    @Override
    public PropertiesPlansTreatmentsKeyOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesPlansTreatmentsKeyOutput propertiesPlansTreatmentsKeyOutput = new PropertiesPlansTreatmentsKeyOutput();
        propertiesPlansTreatmentsKeyOutput.setId(rs.getInt("id"));
        propertiesPlansTreatmentsKeyOutput.setIdTreatments(rs.getInt("plans_treatments_keys"));
        propertiesPlansTreatmentsKeyOutput.setIdPropertiesTreatments(rs.getInt("properties_treatments_id"));
        propertiesPlansTreatmentsKeyOutput.setIdStatusProTreatments(rs.getInt("id_status"));
        return propertiesPlansTreatmentsKeyOutput;
    }

}
