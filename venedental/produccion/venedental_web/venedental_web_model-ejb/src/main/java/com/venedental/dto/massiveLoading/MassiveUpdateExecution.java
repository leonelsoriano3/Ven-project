/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class MassiveUpdateExecution {

    private Date dateFrom;
    private Date dateTo;
    private final Integer idInsurance;
    private final boolean completLoading;
    private final boolean include;
    private final boolean exclude;

    public MassiveUpdateExecution(Long currentYear, Integer idInsurance, boolean completLoading,boolean include, boolean exclude) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

        try {
            this.dateFrom = format.parse(String.format("%d-12-31", currentYear - 1));
            this.dateTo = format.parse(String.format("%d-12-31", currentYear));
        } catch (ParseException ex) {
        }
        this.idInsurance = idInsurance;
        this.completLoading = completLoading;
        this.include=include;
        this.exclude=exclude;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public Integer getIdInsurance() {
        return idInsurance;
    }

    public boolean getCompletLoading() {
        return completLoading;
    }

    public boolean getInclude() {
        return include;
    }

    public boolean getExclude() {
        return exclude;
    }

   

}
