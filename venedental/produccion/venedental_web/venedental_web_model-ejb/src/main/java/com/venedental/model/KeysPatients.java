/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Cacheable(true)
@Table(name = "keys_patients")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "KeysPatients.findAll", query = "SELECT k FROM KeysPatients k order by K.applicationDate DESC, K.number DESC"),
        @NamedQuery(name = "KeysPatients.findById", query = "SELECT k FROM KeysPatients k WHERE k.id = :id"),
        @NamedQuery(name = "KeysPatients.findByIdPatients", query = "SELECT k FROM KeysPatients k WHERE k.patientsId.id = :patients_id"),
        @NamedQuery(name = "KeysPatients.findByNumber", query = "SELECT k FROM KeysPatients k WHERE k.number = :number"),
        @NamedQuery(name = "KeysPatients.filterByNumber", query = "SELECT k FROM KeysPatients k WHERE k.number LIKE :number AND k.status = :status order by K.applicationDate DESC"),
        @NamedQuery(name = "KeysPatients.findByAttentionType", query = "SELECT k FROM KeysPatients k WHERE k.attentionType = :attentionType"),
        @NamedQuery(name = "KeysPatients.filterByApplicationDate", query = "SELECT k FROM KeysPatients k WHERE k.applicationDate = :applicationDate"),
        @NamedQuery(name = "KeysPatients.filterByApplicationDateAndSpecialist", query = "SELECT k FROM KeysPatients k WHERE k.applicationDate = :applicationDate AND k.specialistId.id = :specialistId"),
        @NamedQuery(name = "KeysPatients.findByApplicationDate", query = "SELECT k FROM KeysPatients k WHERE k.applicationDate BETWEEN :startDate AND :endDate AND k.status = :status ORDER BY k.applicationDate DESC "),
        @NamedQuery(name = "KeysPatients.findByKeysSpecialist", query = "SELECT k FROM KeysPatients k WHERE k.specialistId.id = :specialistId order by K.applicationDate DESC"),
        @NamedQuery(name = "KeysPatients.findByApplicationDateSpecialist", query = "SELECT k FROM KeysPatients k WHERE k.applicationDate BETWEEN :startDate AND :endDate AND k.specialistId.id = :specialistId ORDER BY k.applicationDate DESC "),
        @NamedQuery(name = "KeysPatients.findByApplicationDateSpeciality", query = "SELECT k FROM KeysPatients "
                + "k WHERE k.applicationDate BETWEEN :startDate AND :endDate AND k.specialistId.typeSpecialistid.id= :typeSpecialistId"),
        @NamedQuery(name = "KeysPatients.findByApplicationDateInsurances", query
                = "SELECT k FROM KeysPatients k "
                + "JOIN k.patientsId p "
                + "JOIN p.insurancesPatientsList i "
                + "WHERE k.applicationDate BETWEEN :startDate AND :endDate "
                + "AND i.insurances.id = :insurancesList"),
        @NamedQuery(name = "KeysPatients.findByStatus", query = "SELECT k FROM KeysPatients k WHERE k.status = :status order by k.applicationDate DESC, k.number DESC"),
        @NamedQuery(name = "KeysPatients.findByStatusAndSpecialist", query = "SELECT k FROM KeysPatients k WHERE k.status.id = :idStatus AND k.specialistId.id = :specialistId")})
public class KeysPatients implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "number")
    private String number;

    @Size(max = 45)
    @Column(name = "attention_type")
    private String attentionType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "application_date")
    private Date applicationDate;

    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne
    private StatusAnalysisKeys status;

    @JoinColumn(name = "specialist_id", referencedColumnName = "id")
    @ManyToOne
    private Specialists specialistId;

    @JoinColumn(name = "patients_id", referencedColumnName = "id")
    @ManyToOne
    private Patients patientsId;

    @OneToMany(mappedBy = "idKeys")
    private List<BinnacleKeys> binnacleKeysList;

    @OneToMany(mappedBy = "keysId", cascade = CascadeType.ALL)
    private List<PlansTreatmentsKeys> plansTreatmentsKeysList;

    @Transient
    private String applicationDateString;


    @Transient
    private String statusName;




    /*USADO EN LOS DTO*/
    @Transient
    private Integer idPatient;
    @Transient
    private String namePatient;
    @Transient
    private String identityNumberPatient;


    /**
     * variable que se encarga de validar si se puede deshacer recepción de clave
     * se apoya en dos servicios ServiceConsultKeyHaveTreatmet y ServiceConsultKeyInstatusGenered
     */
    @Transient
    private Boolean undoReceiveKey;


    public KeysPatients() {
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getIdentityNumberPatient() {
        return identityNumberPatient;
    }

    public void setIdentityNumberPatient(String identityNumberPatient) {
        this.identityNumberPatient = identityNumberPatient;
    }

    public KeysPatients(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Specialists getSpecialistId() {
        return specialistId;
    }

    public void setSpecialistId(Specialists specialistId) {
        this.specialistId = specialistId;
    }

    public Patients getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Patients patientsId) {
        this.patientsId = patientsId;
    }

    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysList() {
        return plansTreatmentsKeysList;
    }

    public void setPlansTreatmentsKeysList(List<PlansTreatmentsKeys> plansTreatmentsKeysList) {
        this.plansTreatmentsKeysList = plansTreatmentsKeysList;
    }

    public String getApplicationDateString() {
        return applicationDateString;
    }

    public void setApplicationDateString(String applicationDateString) {
        this.applicationDateString = applicationDateString;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KeysPatients)) {
            return false;
        }
        KeysPatients other = (KeysPatients) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.KeysPatients[ id=" + id + " ]";
    }

    public String getAttentionType() {
        return attentionType;
    }

    public void setAttentionType(String attentionType) {
        this.attentionType = attentionType;
    }

    @XmlTransient
    public List<BinnacleKeys> getBinnacleKeysList() {
        return binnacleKeysList;
    }

    public void setBinnacleKeysList(List<BinnacleKeys> binnacleKeysList) {
        this.binnacleKeysList = binnacleKeysList;
    }

    public StatusAnalysisKeys getStatus() {
        return status;
    }

    public void setStatus(StatusAnalysisKeys status) {
        this.status = status;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Boolean getUndoReceiveKey() {
        return undoReceiveKey;
    }

    public void setUndoReceiveKey(Boolean undoReceiveKey) {
        this.undoReceiveKey = undoReceiveKey;
    }

}
