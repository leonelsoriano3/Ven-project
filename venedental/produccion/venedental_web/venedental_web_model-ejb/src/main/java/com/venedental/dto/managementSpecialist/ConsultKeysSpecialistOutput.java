/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysSpecialistOutput {
    
    private String numberKey,namePatient,dateKeyTable,dateMinFilter, dataEndDateFilter,dateStartS, dateEndS;
     
    private Integer idPatient,identityNumberpatient,idKey,diagnostico,registerTreatment;
    
    private Date dateKey,dateStart, dateEnd;
    
    private Integer delete;

    public ConsultKeysSpecialistOutput() {
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdentityNumberpatient() {
        return identityNumberpatient;
    }

    public void setIdentityNumberpatient(Integer identityNumberpatient) {
        this.identityNumberpatient = identityNumberpatient;
    }

    public Date getDateKey() {
        return dateKey;
    }

    public void setDateKey(Date dateKey) {
        this.dateKey = dateKey;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Integer diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateKeyTable() {
        return dateKeyTable;
    }

    public void setDateKeyTable(String dateKeyTable) {
        this.dateKeyTable = dateKeyTable;
    }

    public String getDateMinFilter() {
        return dateMinFilter;
    }

    public void setDateMinFilter(String dateMinFilter) {
        this.dateMinFilter = dateMinFilter;
    }

    public String getDataEndDateFilter() {
        return dataEndDateFilter;
    }

    public void setDataEndDateFilter(String dataEndDateFilter) {
        this.dataEndDateFilter = dataEndDateFilter;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDateStartS() {
        return dateStartS;
    }

    public void setDateStartS(String dateStartS) {
        this.dateStartS = dateStartS;
    }

    public String getDateEndS() {
        return dateEndS;
    }

    public void setDateEndS(String dateEndS) {
        this.dateEndS = dateEndS;
    }

    public Integer getDelete() {
        return delete;
    }

    public void setDelete(Integer delete) {
        this.delete = delete;
    }

    public Integer getRegisterTreatment() {
        return registerTreatment;
    }

    public void setRegisterTreatment(Integer registerTreatment) {
        this.registerTreatment = registerTreatment;
    }
}
