/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.massiveLoading;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.massiveLoading.TemporaryPatient;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class InsertTemporaryPatientDAO extends AbstractCommandDAO<TemporaryPatient> {

    public InsertTemporaryPatientDAO(DataSource dataSource) {
        super(dataSource, "insert_pacientes", 9);
    }
    
    @Override
    public void prepareInput(TemporaryPatient input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdentityNumber());
        statement.setInt(2, input.getIdentityNumberHolder());
        statement.setInt(3, input.getRelationshipId());
        statement.setString(4, input.getCompleteName());
        statement.setString(5, input.getSex());
        statement.setDate(6, new Date(input.getDateOfBirth().getTime()));
        statement.setInt(7, input.getIdInsurances());
        statement.setInt(8, input.getIdPlan());
        statement.setString(9, input.getCompany());
    }
    
}
