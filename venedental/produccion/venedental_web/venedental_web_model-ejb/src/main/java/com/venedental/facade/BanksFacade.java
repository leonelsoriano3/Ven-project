/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Banks;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class BanksFacade extends AbstractFacade<Banks> implements BanksFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
     @Override
    public List<Banks> findAllOrderByName() {
        TypedQuery<Banks> query = em.createNamedQuery("Banks.findAll", Banks.class);
        return query.getResultList();
    }

    public BanksFacade() {
        super(Banks.class);
    }
    
}
