/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

/**
 *
 * @author AKDESKDEV90
 */
public class KeysBySpecialistAndStatusInput {
     Integer idSpecialist;
     Integer idStatus;
     Integer idStatus2;
     Integer idStatus3;
     Integer idUser;

     
    public KeysBySpecialistAndStatusInput(Integer idSpecialist, Integer idStatus, Integer idStatus2, Integer idStatus3, Integer idUser) {
        this.idSpecialist = idSpecialist;
        this.idStatus = idStatus;
        this.idStatus2 = idStatus2;
        this.idStatus3 = idStatus3;
        this.idUser=idUser;
    }

    public Integer getIdStatus3() {
        return idStatus3;
    }

    public void setIdStatus3(Integer idStatus3) {
        this.idStatus3 = idStatus3;
    }    

    public Integer getIdStatus2() {
        return idStatus2;
    }

    public void setIdStatus2(Integer idStatus2) {
        this.idStatus2 = idStatus2;
    }
    
    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }  

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
    
    
}
