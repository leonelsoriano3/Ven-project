/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade.store_procedure;

import com.venedental.dto.ReadInsurancesReportOutput;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class ReadInsurancesFacade
        extends AbstractStoreProcedureFacade<String, ReadInsurancesReportOutput>
        implements ReadInsurancesFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReadInsurancesFacade() {
        super("SP_LEER_ASEGURADORAS");
    }

    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
    }

    @Override
    protected void prepareInput(StoredProcedureQuery query, String input) {
    }

    @Override
    protected ReadInsurancesReportOutput prepareOutput(Object[] result) {
        ReadInsurancesReportOutput output = new ReadInsurancesReportOutput();
        output.setIdInsurance((Integer) result[0]);
        output.setName((String) result[1]);
        
        return output;
    }

 

 

 
}
