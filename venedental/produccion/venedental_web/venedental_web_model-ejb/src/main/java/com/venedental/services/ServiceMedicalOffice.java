/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.medicalOffice.FindMedicalOfficeByIdSpecialistDAO;
import com.venedental.dao.medicalOffice.FindMedicalOfficeSpecialistManagementDAO;
import com.venedental.dto.medicalOffice.FindMedicalOfficeBySepcialistOutput;
import com.venedental.model.MedicalOffice;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
@Stateless
public class ServiceMedicalOffice {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;


    /**
     * Buscar todos los consultorios de un especialista
     * @param idSpecialist
     * @return
     */
    public List<MedicalOffice> findByIdSpecialist(Integer idSpecialist) {
        FindMedicalOfficeByIdSpecialistDAO findMedicalOficceByIdSpecialistDAO = new FindMedicalOfficeByIdSpecialistDAO(ds);
        findMedicalOficceByIdSpecialistDAO.execute(idSpecialist);
        return findMedicalOficceByIdSpecialistDAO.getResultList();
    }

    
     /**
     * Buscar todos los consultorios de un especialista utilizando el output FindMedicalOfficeBySepcialistOutput
     * @param idSpecialist
     * @return
     */
    public FindMedicalOfficeBySepcialistOutput findMedicalOfficeByIdSpecialist(Integer idSpecialist) {
        FindMedicalOfficeSpecialistManagementDAO findMedicalOfficeSpecialistManagementDAO = new FindMedicalOfficeSpecialistManagementDAO(ds);
        findMedicalOfficeSpecialistManagementDAO.execute(idSpecialist);
        return findMedicalOfficeSpecialistManagementDAO.getResult();
    }

}