/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultTreatmentSelectOutput {
   private String nameTreatment,nameCategory;
           
   private Integer idCategory,idTreatment,treatmentAssigned;
   
   private boolean isCheck;
 

    public ConsultTreatmentSelectOutput() {
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getTreatmentAssigned() {
        return treatmentAssigned;
    }

    public void setTreatmentAssigned(Integer treatmentAssigned) {
        
        this.treatmentAssigned = treatmentAssigned;
    }

    public boolean isIsCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

    
    
}
