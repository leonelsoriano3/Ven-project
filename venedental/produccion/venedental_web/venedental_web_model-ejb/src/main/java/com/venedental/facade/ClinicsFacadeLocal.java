/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Clinics;
import com.venedental.model.Specialists;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface ClinicsFacadeLocal {

    Clinics create(Clinics clinics);

    Clinics edit(Clinics clinics);

    void remove(Clinics clinics);

    Clinics find(Object id);
    
    Clinics findByRif(String name, String rif, String address);

    List<Clinics> findAll();

    List<Clinics> findRange(int[] range);
    
    void deleteClinics(Integer id);

    int count();
    
    void  updateStatus (Integer id);
    
    List<Clinics> findByStatus(int status);
    
    
}
