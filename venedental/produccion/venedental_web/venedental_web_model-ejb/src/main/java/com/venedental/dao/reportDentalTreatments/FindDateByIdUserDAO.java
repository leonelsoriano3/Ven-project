package com.venedental.dao.reportDentalTreatments;

import com.venedental.dao.AbstractQueryDAO;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Created by akdesk10 on 15/07/16.
 */
public class FindDateByIdUserDAO extends AbstractQueryDAO<Integer, Date> {
    public FindDateByIdUserDAO(DataSource dataSource) {
        super(dataSource,"SP_M_ESP_MininaFechaReporteTratamiento", 2);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {

        statement.setInt(1,input);
        statement.registerOutParameter(2, Types.DATE);

    }

    @Override
    public Date prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Date date;
        date = statement.getDate(2);

        return date;
    }
}