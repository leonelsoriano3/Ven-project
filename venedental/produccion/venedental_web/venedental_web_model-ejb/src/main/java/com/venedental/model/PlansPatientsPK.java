/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Usuario
 */
@Embeddable
public class PlansPatientsPK implements Serializable {
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "plans_id", unique = false, nullable = false)
    private Integer plansId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "patients_id", unique = false, nullable = false)
    private Integer patientsId;

    public PlansPatientsPK() {
    }

    public PlansPatientsPK(Integer plansId, Integer patientsId) {
        super();
        this.plansId = plansId;
        this.patientsId = patientsId;
    }

    public int getPlansId() {
        return plansId;
    }

    public void setPlansId(Integer plansId) {
        this.plansId = plansId;
    }

    public int getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Integer patientsId) {
        this.patientsId = patientsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) plansId;
        hash += (int) patientsId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlansPatientsPK)) {
            return false;
        }
        PlansPatientsPK other = (PlansPatientsPK) object;
        if (this.plansId != other.plansId) {
            return false;
        }
        if (this.patientsId != other.patientsId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.PlansPatientsPK[ plansId=" + plansId + ", patientsId=" + patientsId + " ]";
    }
    
}
