package com.venedental.dto.createUser;

/**
 * Created by akdeskdev90 on 10/05/16.
 */
public class UpdatePasswordDTO {

    private String password;
    private Integer idUser;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

}
