/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsInput;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;


/**
 *
 * @author usuario
 */
public class ConsultPaymentReportDAO extends AbstractQueryDAO<ConfirmKeyDetailsInput, ConfirmKeyDetailsOutput> {

    public ConsultPaymentReportDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_ReportePago",3);
    }

    @Override
    public void prepareInput(ConfirmKeyDetailsInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdMonth());
        statement.setDate(3, new Date( input.getDateReception().getTime()));
    }

    @Override
    public ConfirmKeyDetailsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConfirmKeyDetailsOutput confirmKeyDetailsOutput = new ConfirmKeyDetailsOutput();
        confirmKeyDetailsOutput.setNumberKey(rs.getString("numero_clave"));
        confirmKeyDetailsOutput.setIdPlansTreatmentsKey(rs.getInt("id_tratamiento_clave"));
        confirmKeyDetailsOutput.setIdPropertiesPlansTreatmentsKey(rs.getInt("id_pieza_tratamiento_clave"));
        confirmKeyDetailsOutput.setNameTreatments(rs.getString("nombre_tratamiento"));
        confirmKeyDetailsOutput.setNameProperties(rs.getString("numero_pieza"));
        confirmKeyDetailsOutput.setObservation(rs.getString("observaciones"));
        confirmKeyDetailsOutput.setAmountPaid(rs.getDouble("monto_pagar"));
        confirmKeyDetailsOutput.setIdPatient(rs.getInt("id_paciente"));
        confirmKeyDetailsOutput.setNamePatient(rs.getString("nombre_paciente"));
        confirmKeyDetailsOutput.setIdStatus(rs.getInt("id_estatus"));
        confirmKeyDetailsOutput.setNameStatus(rs.getString("nombre_estatus"));
        confirmKeyDetailsOutput.setDateTreatment(rs.getDate("fecha"));
        return confirmKeyDetailsOutput;
    }

}
