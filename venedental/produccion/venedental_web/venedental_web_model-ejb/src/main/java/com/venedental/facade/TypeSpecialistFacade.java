/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.TypeSpecialist;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class TypeSpecialistFacade extends AbstractFacade<TypeSpecialist> implements TypeSpecialistFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TypeSpecialistFacade() {
        super(TypeSpecialist.class);
    }
    
}
