/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "clinics", schema = "")
@NamedQueries({
    @NamedQuery(name = "Clinics.findAll", query = "SELECT c FROM Clinics c"),
    @NamedQuery(name = "Clinics.findById", query = "SELECT c FROM Clinics c WHERE c.id = :id"),
    @NamedQuery(name = "Clinics.findByName", query = "SELECT c FROM Clinics c WHERE c.name = :name"),
    @NamedQuery(name = "Clinics.findByRif", query = "SELECT c FROM Clinics c WHERE c.name = :name AND c.rif = :rif AND c.addressId.address = :address"),
    @NamedQuery(name = "Clinics.findByStatus", query = "SELECT c FROM Clinics c WHERE c.status = :status"),
    @NamedQuery(name = "Clinics.updateStatus", query = "UPDATE Clinics c SET c.status = 0 WHERE c.id = :clinics_id"),
    @NamedQuery(name = "Clinics.deleteClinics", query = "DELETE FROM Clinics c WHERE c.id = :clinics_id")})

public class Clinics implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 20)
    @Column(name = "rif")
    private String rif;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @Size(max = 45)
    @Column(name = "email")
    private String email;

    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @OneToOne(cascade = CascadeType.ALL)
    private Addresses addressId;

    @Transient
    private String descStatus;

    @ElementCollection
    @CollectionTable(
            name = "phone_clinic",
            joinColumns = @JoinColumn(name = "clinic_id")
    )
    private List<PhoneClinic> phoneClinicList;

    @OneToMany(mappedBy = "clinicId")
    private List<MedicalOffice> medicalOfficeList;

    public Clinics() {
        this.modificarEstatus();
    }

    public Clinics(Integer id) {
        this.id = id;
        this.modificarEstatus();
    }

    public Clinics(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.modificarEstatus();
    }

    public Clinics(String name, String rif) {
        this.name = name;
        this.rif = rif;
        this.modificarEstatus();
    }

    public String getDescStatus() {
        return descStatus;
    }

    public void setDescStatus(String descStatus) {
        this.descStatus = descStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Addresses getAddressId() {
        return addressId;
    }

    public void setAddressId(Addresses addressId) {
        this.addressId = addressId;
    }

    @Transient
    private List<Specialists> specialistsList;

    public List<Specialists> getSpecialistsList() {
        return specialistsList;
    }

    public void setSpecialistsList(List<Specialists> specialistsList) {
        this.specialistsList = specialistsList;
    }

    public List<PhoneClinic> getPhoneClinicList() {
        return phoneClinicList;
    }

    public void setPhoneClinicList(List<PhoneClinic> phoneClinicList) {
        this.phoneClinicList = phoneClinicList;
    }

    private void modificarEstatus() {

        if (this.getStatus() != null) {
            if (this.status == 1) {
                setDescStatus("Habilitado");

            } else {

                if (this.status == 2) {
                    setDescStatus("Suspendido");
                } else {
                    setDescStatus("Inactivo");
                }
            }
        } else {
            setDescStatus("Inactivo");
        }

    }

    @XmlTransient
    public List<MedicalOffice> getMedicalOfficeList() {
        return medicalOfficeList;
    }

    public void setMedicalOfficeList(List<MedicalOffice> medicalOfficeList) {
        this.medicalOfficeList = medicalOfficeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clinics)) {
            return false;
        }
        Clinics other = (Clinics) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Clinics obj = (Clinics) super.clone();

        obj.addressId = obj.addressId == null ? null : (Addresses) obj.addressId.clone();

        if (obj.getPhoneClinicList() == null) {
            obj.phoneClinicList = null;
        } else {
            List<PhoneClinic> listaPhoneclinic = new ArrayList<>();
            for (PhoneClinic item : obj.getPhoneClinicList()) {
                listaPhoneclinic.add((PhoneClinic) item.clone());
            }
            obj.phoneClinicList = listaPhoneclinic;
        }
        return obj;
    }

    public Comparator compareToEstadoYCiudad() {
        return new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Clinics clinica1 = (Clinics) o1;
                Clinics clinica2 = (Clinics) o2;
                String e1;
                String e2;
                String c1;
                String c2;

                if (clinica1.addressId != null) {
                    e1 = clinica1.addressId.getCityId().getStateId().getNombre();
                    c1 = clinica1.addressId.getCityId().getName();
                } else {
                    e1 = "";
                    c1 = "";
                }

                if (clinica2.getAddressId() != null) {
                    e2 = clinica2.getAddressId().getCityId().getStateId().getNombre();
                    c2 = clinica2.getAddressId().getCityId().getName();
                } else {
                    e2 = "";
                    c2 = "";
                }

                int resultado = e1.compareTo(e2);
                if (resultado != 0) {
                    return resultado;
                }

                resultado = c1.compareTo(c2);
                return resultado;

            }
        };
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
