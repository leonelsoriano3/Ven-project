/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.RolUsers;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Anzola
 */
@Local
public interface RolUsersFacadeLocal {

    RolUsers create(RolUsers rolUser);

    RolUsers edit(RolUsers rolUser);

    void remove(RolUsers rolUser);

    RolUsers find(Object id);
    
    RolUsers findRolUserByUserRol(Integer idUser, Integer idRol);

    List<RolUsers> findAll();

    List<RolUsers> findByUserId(int userId);

    List<RolUsers> findRange(int[] range);

    int count();

}
