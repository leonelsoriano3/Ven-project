/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "bank_accounts", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankAccounts.findAll", query = "SELECT b FROM BankAccounts b"),
    @NamedQuery(name = "BankAccounts.findById", query = "SELECT b FROM BankAccounts b WHERE b.id = :id"),
    @NamedQuery(name = "BankAccounts.findByAccountNumber", query = "SELECT b FROM BankAccounts b WHERE b.accountNumber = :accountNumber")})
public class BankAccounts implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "account_number")
    private String accountNumber;
    
    @JoinColumn(name = "bank", referencedColumnName = "id")
    @ManyToOne
    private Banks bank;
    
    @JoinColumn(name = "id_owner", referencedColumnName = "id")
    @ManyToOne
    private Specialists idOwner;

    public BankAccounts() {
    }

    public BankAccounts(Integer id) {
        this.id = id;
    }

    public BankAccounts(Integer id, String accountNumber) {
        this.id = id;
        this.accountNumber = accountNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Banks getBank() {
        return bank;
    }

    public void setBank(Banks bank) {
        this.bank = bank;
    }

    public Specialists getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(Specialists idOwner) {
        this.idOwner = idOwner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankAccounts)) {
            return false;
        }
        BankAccounts other = (BankAccounts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.BankAccounts[ id=" + id + " ]";
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        BankAccounts obj=null;
        obj=(BankAccounts) super.clone();
        obj.bank = obj.bank==null?null:(Banks) obj.bank.clone();
        obj.idOwner = obj.idOwner==null?null:(Specialists) obj.idOwner.clone();
        return obj;
    }
    
}
