/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade.store_procedure;

import com.venedental.dto.AuditReportInput;
import com.venedental.dto.AuditReportOutput;
import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class AuditFacade
        extends AbstractStoreProcedureFacade<AuditReportInput, AuditReportOutput>
        implements AuditFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuditFacade() {
        super("SP_REPORTE_AUDITORIA");
    }

    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, Date.class, IN);
        query.registerStoredProcedureParameter(2, Date.class, IN);
        query.registerStoredProcedureParameter(3, Integer.class, IN);
        query.registerStoredProcedureParameter(4, Integer.class, IN);
        query.registerStoredProcedureParameter(5, Integer.class, IN);
        query.registerStoredProcedureParameter(6, Integer.class, IN);
        query.registerStoredProcedureParameter(7, Integer.class, IN);
        query.registerStoredProcedureParameter(8, Integer.class, IN);
        query.registerStoredProcedureParameter(9, Integer.class, IN);
    }

    @Override
    protected void prepareInput(StoredProcedureQuery query, AuditReportInput input) {
        query.setParameter(1, input.getStartDate());
        query.setParameter(2, input.getEndDate());
        query.setParameter(3, input.getIdTreatment());
        query.setParameter(4, input.getIdStatus());
        query.setParameter(5, input.getHealthArea());
        query.setParameter(6, input.getTypeBaremo());
        query.setParameter(7, input.getIdPlan());
        query.setParameter(8, input.getIdPieza());
        query.setParameter(9, input.getIdInsurance());
    }

    @Override
    protected AuditReportOutput prepareOutput(Object[] result) {
        AuditReportOutput output = new AuditReportOutput();
        output.setApplicationDate((Date) result[0]);
        output.setNumber((String) result[1]);
        output.setSpecialistIdentityNumber((String) result[2]);
        output.setSpecialistCompleteName((String) result[3]);
        output.setPatientIdentityNumber((String) result[4]);
        output.setPatientCompleteName((String) result[5]);
        output.setPlanName((String) result[6]);
        output.setTreatment((String) result[7]);
        output.setTooth((String) result[8]);
        output.setBaremoPrice( (Double) result[9]);
        output.setInsuranceName((String) result[10]);
        return output;
    }  
}
