/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "status_analysis_keys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StatusAnalysisKeys.findAll", query = "SELECT s FROM StatusAnalysisKeys s"),
    @NamedQuery(name = "StatusAnalysisKeys.findById", query = "SELECT s FROM StatusAnalysisKeys s WHERE s.id = :id"),
    @NamedQuery(name = "StatusAnalysisKeys.findByDescription", query = "SELECT s FROM StatusAnalysisKeys s WHERE s.description = :description")})
public class StatusAnalysisKeys implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 45)
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "status")
    private List<KeysPatients> keysPatientsList;

    @OneToMany(mappedBy = "idStatusKeys")
    private List<BinnacleKeys> binnacleKeysList;

    @OneToMany(mappedBy = "idStatusProperties")
    private List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList;

    @OneToMany(mappedBy = "idStatus" , cascade = CascadeType.ALL)
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList;

    @OneToMany(mappedBy = "idStatusPlans")
    private List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList;

    @OneToMany(mappedBy = "idStatus", cascade = CascadeType.ALL)
    private List<PlansTreatmentsKeys> plansTreatmentsKeysList;

    public StatusAnalysisKeys() {
    }

    public StatusAnalysisKeys(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatusAnalysisKeys)) {
            return false;
        }
        StatusAnalysisKeys other = (StatusAnalysisKeys) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.StatusAnalysisKeys[ id=" + id + " ]";
    }

    @XmlTransient
    public List<KeysPatients> getKeysPatientsList() {
        return keysPatientsList;
    }

    public void setKeysPatientsList(List<KeysPatients> keysPatientsList) {
        this.keysPatientsList = keysPatientsList;
    }

    @XmlTransient
    public List<BinnacleKeys> getBinnacleKeysList() {
        return binnacleKeysList;
    }

    public void setBinnacleKeysList(List<BinnacleKeys> binnacleKeysList) {
        this.binnacleKeysList = binnacleKeysList;
    }

    @XmlTransient
    public List<BinnaclePlansTreatmentsKeys> getBinnaclePlansTreatmentsKeysList() {
        return binnaclePlansTreatmentsKeysList;
    }

    public void setBinnaclePlansTreatmentsKeysList(List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList) {
        this.binnaclePlansTreatmentsKeysList = binnaclePlansTreatmentsKeysList;
    }

    @XmlTransient
    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysList() {
        return plansTreatmentsKeysList;
    }

    public void setPlansTreatmentsKeysList(List<PlansTreatmentsKeys> plansTreatmentsKeysList) {
        this.plansTreatmentsKeysList = plansTreatmentsKeysList;
    }

    @XmlTransient
    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeyList() {
        return propertiesPlansTreatmentsKeyList;
    }

    public void setPropertiesPlansTreatmentsKeyList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList) {
        this.propertiesPlansTreatmentsKeyList = propertiesPlansTreatmentsKeyList;
    }

    @XmlTransient
    public List<BinnaclePropertiesTreatmentsKeys> getBinnaclePropertiesTreatmentsKeysList() {
        return binnaclePropertiesTreatmentsKeysList;
    }

    public void setBinnaclePropertiesTreatmentsKeysList(List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList) {
        this.binnaclePropertiesTreatmentsKeysList = binnaclePropertiesTreatmentsKeysList;
    }

}
