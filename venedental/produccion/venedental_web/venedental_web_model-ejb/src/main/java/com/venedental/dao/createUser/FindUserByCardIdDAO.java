package com.venedental.dao.createUser;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.createUser.FindUserByCardIdOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Created by leonelsoriano3@gmail.com on 30/03/16.
 * clase encargada de conseguir el sepecialista por cedula
 * usado en crear especialista
 */
public class FindUserByCardIdDAO extends AbstractQueryDAO<Integer, FindUserByCardIdOutput> {


    public FindUserByCardIdDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_G_006", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }


    @Override
    public FindUserByCardIdOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        FindUserByCardIdOutput findUserByCardIdOutput = null;
        if(this.isReturnData) {
            findUserByCardIdOutput = new FindUserByCardIdOutput();
            findUserByCardIdOutput.setId(rs.getInt("id_especialista"));
            findUserByCardIdOutput.setFirstName(rs.getString("first_name"));
            findUserByCardIdOutput.setLastName(rs.getString("lastname"));
            findUserByCardIdOutput.setCompleteName(rs.getString("complete_name"));
            findUserByCardIdOutput.setEmail(rs.getString("email"));
            findUserByCardIdOutput.setUser_type(rs.getString("user_type"));
            findUserByCardIdOutput.setUsersId(rs.getInt("users_id"));
            findUserByCardIdOutput.setIdEmployee(rs.getInt("id_empleado"));

        }

        return findUserByCardIdOutput;

    }


}