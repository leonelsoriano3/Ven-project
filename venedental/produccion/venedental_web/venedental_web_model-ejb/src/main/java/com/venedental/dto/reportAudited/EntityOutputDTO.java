/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.reportAudited;

/**
 * Clase salida de las Entidades: Ejemplo Empresas u Organizaciones
 * @author akdesk10
 */
public class EntityOutputDTO {
    
    private Integer idEntity;
    private String nameEntity;

    public Integer getIdEntity() {
        return idEntity;
    }


    public void setIdEntity(Integer idEntity) {
        this.idEntity = idEntity;
    }

    public String getNameEntity() {
        return nameEntity;
    }

    public void setNameEntity(String nameEntity) {
        this.nameEntity = nameEntity;
    }
    
    
}