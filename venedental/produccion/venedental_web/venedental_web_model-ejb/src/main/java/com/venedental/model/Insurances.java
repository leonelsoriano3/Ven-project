/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "insurances")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Insurances.findAll", query = "SELECT i FROM Insurances i ORDER BY i.name ASC"),
    @NamedQuery(name = "Insurances.findById", query = "SELECT i FROM Insurances i WHERE i.id = :id"),
    @NamedQuery(name = "Insurances.findByName", query = "SELECT i FROM Insurances i WHERE i.name = :name"),
    @NamedQuery(name = "Insurances.findByRif", query = "SELECT i FROM Insurances i WHERE i.rif = :rif"),
    @NamedQuery(name = "Insurances.findByPhone", query = "SELECT i FROM Insurances i WHERE i.phone = :phone"),
    @NamedQuery(name = "Insurances.findByCellphone", query = "SELECT i FROM Insurances i WHERE i.cellphone = :cellphone"),
    @NamedQuery(name = "Insurances.findByEmail", query = "SELECT i FROM Insurances i WHERE i.email = :email")})
public class Insurances implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    @Size(max = 20)
    @Column(name = "rif")
    private String rif;

    @Size(max = 45)
    @Column(name = "phone")
    private String phone;

    @Size(max = 45)
    @Column(name = "cellphone")
    private String cellphone;

    @Size(max = 45)
    @Column(name = "email")
    private String email;

    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @ManyToOne
    private Addresses addressId;

    @OneToMany(mappedBy = "insuranceId")
    private List<Plans> plansList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insurances")
    private List<InsurancesPatients> insurancesPatientsList;

    @Size(max = 255)
    @Column(name = "folder")
    private String folder;

    public Insurances() {
    }

    public Insurances(Integer id) {
        this.id = id;
    }

    public Insurances(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Addresses getAddressId() {
        return addressId;
    }

    public void setAddressId(Addresses addressId) {
        this.addressId = addressId;
    }

    public List<Plans> getPlansList() {
        return plansList;
    }

    public void setPlansList(List<Plans> plansList) {
        this.plansList = plansList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insurances)) {
            return false;
        }
        Insurances other = (Insurances) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.Insurances[ id=" + id + " ]";
    }

    @XmlTransient
    public List<InsurancesPatients> getInsurancesPatientsList() {
        return insurancesPatientsList;
    }

    public void setInsurancesPatientsList(List<InsurancesPatients> insurancesPatientsList) {
        this.insurancesPatientsList = insurancesPatientsList;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

}
