/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.sql.Date;

/**
 *
 * @author akdesk01
 */
public class AddTreatmentForKeyInput {

    private Integer idKey;
    private Integer idPlantreatment;
    private Date date;
    private Integer idStatus;
    private Integer baremoTreatment;
    private String observation;
    private Integer idUser;
    private Date dateReception;
    private Integer creatediagnosis;
    private String fileSupport;

    public AddTreatmentForKeyInput(Integer idKey, Integer idPlantreatment, Date date, Integer idStatus, Integer baremoTreatment, String observation, Integer idUser, Date dateReception, Integer creatediagnosis) {
        this.idKey = idKey;
        this.idPlantreatment = idPlantreatment;
        this.date = date;
        this.idStatus = idStatus;
        this.baremoTreatment = baremoTreatment;
        this.observation = observation;
        this.idUser = idUser;
        this.dateReception = dateReception;
        this.creatediagnosis = creatediagnosis;
    }
    
    

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdPlantreatment() {
        return idPlantreatment;
    }

    public void setIdPlantreatment(Integer idPlantreatment) {
        this.idPlantreatment = idPlantreatment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getBaremoTreatment() {
        return baremoTreatment;
    }

    public void setBaremoTreatment(Integer baremoTreatment) {
        this.baremoTreatment = baremoTreatment;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Integer getCreatediagnosis() {
        return creatediagnosis;
    }

    public void setCreatediagnosis(Integer creatediagnosis) {
        this.creatediagnosis = creatediagnosis;
    }

    public String getFileSupport() {
        return fileSupport;
    }

    public void setFileSupport(String fileSupport) {
        this.fileSupport = fileSupport;
    }
    
    
    
}
