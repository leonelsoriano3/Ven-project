/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import java.sql.Date;

/**
 *
 * @author root
 */
public class ConsultRetentionDetailsOutput {

    private Date billDate;
    
    private String billNumber;
    
    private String controlNumber;
    
    private Double amountTotal;
    
    private Double baseRetention;
    
    private Double rateRetentionISLR;
            
    private Double amountISLR;
    
    public ConsultRetentionDetailsOutput() {
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public void setAmountTotal(double amountTotal) {
        this.amountTotal = amountTotal;
    }

    public void setBaseRetention(double baseRetention) {
        this.baseRetention = baseRetention;
    }

    public void setRateRetentionISLR(double rateRetentionISLR) {
        this.rateRetentionISLR = rateRetentionISLR;
    }

    public void setAmountISLR(double amountISLR) {
        this.amountISLR = amountISLR;
    }

    public Date getBillDate() {
        return billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public Double getAmountTotal() {
        return amountTotal;
    }

    public Double getBaseRetention() {
        return baseRetention;
    }

    public Double getRateRetentionISLR() {
        return rateRetentionISLR;
    }

    public Double getAmountISLR() {
        return amountISLR;
    }
}
