/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.facade.store_procedure;


import com.venedental.dto.ReadSpecialtiesReportOutput;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class ReadSpecialtiesFacade
extends AbstractStoreProcedureFacade<String, ReadSpecialtiesReportOutput>
implements ReadSpecialtiesFacadeLocal {
    
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public ReadSpecialtiesFacade() {
        super("SP_LEER_ESPECIALIDADES");
    }
    
    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
    }
    
    @Override
    protected void prepareInput(StoredProcedureQuery query, String input) {
    }
    
    @Override
    protected ReadSpecialtiesReportOutput prepareOutput(Object[] result) {
        ReadSpecialtiesReportOutput output = new ReadSpecialtiesReportOutput();
        output.setIdSpecialty((Integer) result[0]);
        output.setNameSpecialty((String) result[1]);
        
        return output;
        
    }



}
