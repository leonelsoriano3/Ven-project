/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.RegisterKeyInput;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.model.KeysPatients;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class RegisterKeyDAO extends AbstractQueryDAO<RegisterKeyInput, RegisterKeyOutput> {

    public RegisterKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_I_001", 8);
    }

    @Override
    public void prepareInput(RegisterKeyInput input, CallableStatement statement) throws SQLException {
        statement.setString(1, input.getNumber());
        statement.setString(2, input.getTypeAttention());
        statement.setInt(3, input.getIdPatient());
        statement.setInt(4, input.getIdSpecialist());
        statement.setInt(5, input.getIdUser());
        statement.setInt(6, input.getIdMedicalOffice());
        if (input.getIdCompany() == 0) {
                statement.setNull(7, java.sql.Types.NULL);
            } else {
                statement.setInt(7, input.getIdCompany());
            }
        
        statement.setInt(8, input.getIdPlan());
    }

    @Override
    public RegisterKeyOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        RegisterKeyOutput registerKeyOutput = new RegisterKeyOutput();
        registerKeyOutput.setIdKey(rs.getInt("id"));
        registerKeyOutput.setStatusKey(rs.getInt("status"));
        return registerKeyOutput;


    }
}