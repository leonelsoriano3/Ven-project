/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.propertiesPlanTreatmentKey;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class PropertiesPlanTreatmentKeyInput {
    
    Integer idPlanTreatmentKey;
    Integer idPropertieTreatment;
    Integer idScaleTreatments;
    Date dateApplication;
    Integer idStatus;
    String observation;
    Integer idUser;
    Date dateReception;   

    public PropertiesPlanTreatmentKeyInput(Integer idPlanTreatmentKey, Integer idPropertieTreatment, Integer idScaleTreatments, Date dateApplication, Integer idStatus, String observation, Integer idUser, Date dateReception) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.idPropertieTreatment = idPropertieTreatment;
        this.idScaleTreatments = idScaleTreatments;
        this.dateApplication = dateApplication;
        this.idStatus = idStatus;
        this.observation = observation;
        this.idUser = idUser;
        this.dateReception = dateReception;
    }

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdPropertieTreatment() {
        return idPropertieTreatment;
    }

    public void setIdPropertieTreatment(Integer idPropertieTreatment) {
        this.idPropertieTreatment = idPropertieTreatment;
    }
    
    public Integer getIdScaleTreatments() {
        return idScaleTreatments;
    }

    public void setIdScaleTreatments(Integer idScaleTreatments) {
        this.idScaleTreatments = idScaleTreatments;
    }

    public Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(Date dateApplication) {
        this.dateApplication = dateApplication;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

}
