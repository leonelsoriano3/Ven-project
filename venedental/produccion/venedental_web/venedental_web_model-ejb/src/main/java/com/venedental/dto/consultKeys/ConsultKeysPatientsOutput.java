/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.consultKeys;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsOutput {
    private Date date;
    private Integer idKey;
    private Integer idPatient,idSpecialist,idStatusKey;
    private String namePatient,numberForKey,nameSpecialist,nameStatusKey,observationKey;
    private Integer numberForIdentifySpecialist,numberForIdentifyPatient;
    

    public ConsultKeysPatientsOutput() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNumberForKey() {
        return numberForKey;
    }

    public void setNumberForKey(String numberForKey) {
        this.numberForKey = numberForKey;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getNumberForIdentifyPatient() {
        return numberForIdentifyPatient;
    }

    public void setNumberForIdentifyPatient(Integer numberForIdentifyPatient) {
        this.numberForIdentifyPatient = numberForIdentifyPatient;
    }

    public Integer getIdStatusKey() {
        return idStatusKey;
    }

    public void setIdStatusKey(Integer idStatusKey) {
        this.idStatusKey = idStatusKey;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public String getNameStatusKey() {
        return nameStatusKey;
    }

    public void setNameStatusKey(String nameStatusKey) {
        this.nameStatusKey = nameStatusKey;
    }

    public String getObservationKey() {
        return observationKey;
    }

    public void setObservationKey(String observationKey) {
        this.observationKey = observationKey;
    }

    public Integer getNumberForIdentifySpecialist() {
        return numberForIdentifySpecialist;
    }

    public void setNumberForIdentifySpecialist(Integer numberForIdentifySpecialist) {
        this.numberForIdentifySpecialist = numberForIdentifySpecialist;
    }

}
