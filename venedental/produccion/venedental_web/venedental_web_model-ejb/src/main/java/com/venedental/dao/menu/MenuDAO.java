package com.venedental.dao.menu;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.menu.MenuOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 18/07/16.
 */
public class MenuDAO extends AbstractQueryDAO<Integer, MenuOut> {



    public MenuDAO(DataSource dataSource) {
        super(dataSource, "SP_T_MENU_G_002", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public MenuOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        MenuOut menuOut = new MenuOut();

        menuOut.setIdMenu(rs.getInt("id_menu"));
        menuOut.setMenuName(rs.getString("nombre_menu"));
        menuOut.setIdFather(rs.getInt("id_padre"));
        menuOut.setPosition(rs.getInt("posicion"));
        menuOut.setNameFather(rs.getString("nombre_padre"));
        menuOut.setUrl(rs.getString("url"));

        return menuOut;
    }
}
