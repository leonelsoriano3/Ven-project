package com.venedental.dto.reportDentalTreatments;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by luis.carrasco@arkiteck.biz on 31/05/16.
 */
public class ReportDentalTreatmentsOutput implements Serializable {

    private Date dateKey;
    private Integer idPatient;
    private String namePatient;
    private Integer idKey;
    private String numberKey;
    private Integer idKeyTreatment;
    private String nameTreatemnent;
    private Integer idToothTreatment;
    private String  numTooth;
    private Integer numLine;
    private Date startDate;
    private Date endDate;
     private String dateKeyS;
     private String startDateS;
     private String endDateS;

    public Integer getIdToothTreatment() {
        return idToothTreatment;
    }

    public void setIdToothTreatment(Integer idToothTreatment) {
        this.idToothTreatment = idToothTreatment;
    }

    public Date getStartDate() {return startDate;}

    public void setStartDate(Date startDate) {this.startDate = startDate;}

    public Date getEndDate() {return endDate;}

    public void setEndDate(Date endDate) {this.endDate = endDate;}

    public Integer getNumLine() { return numLine; }

    public void setNumLine(Integer numLine) {   this.numLine = numLine;}

    public String getNumTooth() {return numTooth;}

    public void setNumTooth(String numberTooth) {this.numTooth = numberTooth;}

    public Integer getIdKeyTreatment() { return idKeyTreatment; }

    public void setIdKeyTreatment(Integer idKeyTreatment) {this.idKeyTreatment = idKeyTreatment;}

    public String getNumberKey() {return numberKey;}

    public void setNumberKey(String numberKey) {this.numberKey = numberKey;}

    public Integer getIdKey() {return idKey;}

    public void setIdKey(Integer idKey) {this.idKey = idKey;}

    public Integer getIdPatient() { return idPatient;}

    public void setIdPatient(Integer idPatient) { this.idPatient = idPatient; }

    public Date getDateKey() { return dateKey;}

    public void setDateKey(Date dateKey) {
        this.dateKey = dateKey;}

    public String getNamePatient() {return namePatient;}

    public void setNamePatient(String namePatient) {this.namePatient = namePatient;}

    public String getNameTreatemnent() {return nameTreatemnent;}

    public void setNameTreatemnent(String nameTreatemnent) {this.nameTreatemnent = nameTreatemnent;}

    public String getDateKeyS() {
        return dateKeyS;
    }

    public void setDateKeyS(String dateKeyS) {
        this.dateKeyS = dateKeyS;
    }

    public String getStartDateS() {
        return startDateS;
    }

    public void setStartDateS(String startDateS) {
        this.startDateS = startDateS;
    }

    public String getEndDateS() {
        return endDateS;
    }

    public void setEndDateS(String endDateS) {
        this.endDateS = endDateS;
    }

    

}
