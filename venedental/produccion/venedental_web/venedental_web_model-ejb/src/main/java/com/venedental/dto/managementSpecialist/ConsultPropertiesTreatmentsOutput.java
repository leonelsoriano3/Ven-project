/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesTreatmentsOutput {
    
    private String nameTreatment,pieceValue;
            
    private Integer idTreatment,idPiece;
    
    public ConsultPropertiesTreatmentsOutput() {
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getPieceValue() {
        return pieceValue;
    }

    public void setPieceValue(String pieceValue) {
        this.pieceValue = pieceValue;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(Integer idPiece) {
        this.idPiece = idPiece;
    }
    
    
    
}
