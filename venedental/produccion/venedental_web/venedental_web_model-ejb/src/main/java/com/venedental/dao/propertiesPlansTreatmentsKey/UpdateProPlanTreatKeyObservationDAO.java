/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.propertiesPlanTreatmentKey.UpdatePropertiesPlanTreatmentKeyObservationInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class UpdateProPlanTreatKeyObservationDAO extends AbstractCommandDAO<UpdatePropertiesPlanTreatmentKeyObservationInput> {

    public UpdateProPlanTreatKeyObservationDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_U_002", 2);
    }

    @Override
    public void prepareInput(UpdatePropertiesPlanTreatmentKeyObservationInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPropertiesPlanTreatmentKey());
        if (input.getObservation() != null) {
            statement.setString(2, input.getObservation());
        }else{
            statement.setNull(2, java.sql.Types.NULL);
        }
    }

}
