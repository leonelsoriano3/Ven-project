package com.venedental.dao.user;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.User.OutTokenDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class FindTokenByIdUserDAO extends AbstractQueryDAO<Integer,OutTokenDTO> {

    public FindTokenByIdUserDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_G_003", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public OutTokenDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        OutTokenDTO tokenOut = new OutTokenDTO();
        tokenOut.setToken(rs.getString("token"));
        tokenOut.setDateExpired(rs.getString("expired_date"));
        return tokenOut;
    }
}
