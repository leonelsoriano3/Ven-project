/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.propertiesPlanTreatmentKey.PropertiesPlanTreatmentKeyInput;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class RegisterPropertiesPlanTreatmentKeyDAO extends AbstractQueryDAO<PropertiesPlanTreatmentKeyInput, PropertiesPlansTreatmentsKey> {

    public RegisterPropertiesPlanTreatmentKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_I_001", 8);
    }

    @Override
    public void prepareInput(PropertiesPlanTreatmentKeyInput input, CallableStatement statement) {
        try {
            statement.setInt(1, input.getIdPlanTreatmentKey());
            statement.setInt(2, input.getIdPropertieTreatment());
            statement.setDate(3, new Date(input.getDateApplication().getTime()));
            statement.setInt(4, input.getIdStatus());
            statement.setInt(5, input.getIdScaleTreatments());
            statement.setString(6, input.getObservation());
            statement.setInt(7, input.getIdUser());
            if (input.getDateReception() != null) {
                statement.setDate(8, new Date(input.getDateReception().getTime()));
            } else {
                statement.setNull(8, java.sql.Types.NULL);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public PropertiesPlansTreatmentsKey prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        propertiesPlansTreatmentsKey.setId(rs.getInt("id"));
        propertiesPlansTreatmentsKey.setIdStatusProTreatments(rs.getInt("id_status"));
        return propertiesPlansTreatmentsKey;
    }

}
