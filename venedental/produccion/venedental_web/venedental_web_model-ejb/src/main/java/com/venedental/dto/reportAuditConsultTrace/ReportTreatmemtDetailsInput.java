package com.venedental.dto.reportAuditConsultTrace;

/**
 * Created by akdesk10 on 01/08/16.
 */
public class ReportTreatmemtDetailsInput {

    private  Integer userId;

    private Integer numCorrelative;

    public ReportTreatmemtDetailsInput(Integer userId, Integer numCorrelative) {
        this.userId = userId;
        this.numCorrelative = numCorrelative;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getNumCorrelative() {
        return numCorrelative;
    }

    public void setNumCorrelative(Integer numCorrelative) {
        this.numCorrelative = numCorrelative;
    }
}
