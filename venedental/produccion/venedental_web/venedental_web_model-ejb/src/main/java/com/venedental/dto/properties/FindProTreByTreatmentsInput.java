/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.properties;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProTreByTreatmentsInput {

    private Integer idTreatments;
    private Integer typePropertie;
    private Integer quadrant;
    private Integer idSpecialist;

    public FindProTreByTreatmentsInput(Integer idTreatments, Integer typePropertie, Integer quadrant, Integer idSpecialist) {
        this.idTreatments = idTreatments;
        this.typePropertie = typePropertie;
        this.quadrant = quadrant;
        this.idSpecialist = idSpecialist;
    }

    public Integer getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(Integer quadrant) {
        this.quadrant = quadrant;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }

    public Integer getTypePropertie() {
        return typePropertie;
    }

    public void setTypePropertie(Integer typePropertie) {
        this.typePropertie = typePropertie;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }


}