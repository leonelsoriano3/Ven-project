/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.KeysPatients;
import com.venedental.model.StatusAnalysisKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface KeysPatientsFacadeLocal {

    KeysPatients create(KeysPatients keys);

    KeysPatients edit(KeysPatients keys);

    //void remove(KeysPatients keys);
    
    List<KeysPatients> findByIdPatients(Integer id);

    List<KeysPatients> findAll();      
    
    List<KeysPatients> findByApplicationDate(Date startDate, Date endDate);
    
    List<KeysPatients> findByApplicationDateAndSpecialist(String date, int specialistId);
    
    List<KeysPatients> findByKeysSpecialist (int specialistId);
    
    List<KeysPatients> findByApplicationDateSpecialist(Date startDate, Date endDate, int specialistId);
    
    List<KeysPatients> findByApplicationDateSpeciality(Date startDate, Date endDate,Integer typeSpecialistId);
    
    List<KeysPatients> findByApplicationDateInsurances(Date startDate, Date endDate,Integer insurancesId);
    
    List<KeysPatients> findByPage(Integer startingAt, Integer maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterByNumber(String number, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys); 
    
    List<KeysPatients> filterByApplicationDate(String applicationDate, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterBySpecialistIdNumber(String identityNumber, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterBySpecialistName(String name, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterByPatientIdNumber(String identityNumber, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterByPatientName(String name, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> filterByApplicationDateRange(Date startDate, Date endDate, int startingAt, int maxPerPage, StatusAnalysisKeys statusAnalysisKeys);
    
    List<KeysPatients> findByStatusAndSpecialist(int idStatus, int idSpecialist);
    
    List<KeysPatients> findByStatusSpecialistAndTreatmentsStatus(Integer idStatus,Integer idStatus2, Integer specialistId, Integer idStatusTreatments1, Integer idStatusTreatments2, Integer idStatusTreatments3);
    
    List<KeysPatients> findByStatusAndTreatmentsStatus(Date dateFrom, Date dateUntil);
    
    int reciveCount ();
    
    int count();
    
    List<KeysPatients> readKeysRecived(Date dateFrom, Date dateUntil);
}