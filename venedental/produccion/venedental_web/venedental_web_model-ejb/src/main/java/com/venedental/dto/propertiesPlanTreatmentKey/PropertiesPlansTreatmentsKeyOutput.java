package com.venedental.dto.propertiesPlanTreatmentKey;

/**
 * Created by akdesk10 on 19/12/16.
 */

import java.sql.Date;
public class PropertiesPlansTreatmentsKeyOutput {

    private Integer id;
    private Integer idTreatments;
    private Integer idStatusProTreatments;
    private String observation;
    private String nameProperties;
    private String nameTreatments;
    private String fileRequirement1;
    private String fileRequirement2;
    private String fileRequirement3;
    private String fileRequirement4;
    private String fileRequirement5;
    private String nameStatusProTreatments;
    private Date dateTreatments;
    private Double price;
    private boolean hasGenerate;
    private Integer poseeRecaudos;
    private Integer idPropertiesTreatments;


    public PropertiesPlansTreatmentsKeyOutput() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }

    public Integer getIdStatusProTreatments() {
        return idStatusProTreatments;
    }

    public void setIdStatusProTreatments(Integer idStatusProTreatments) {
        this.idStatusProTreatments = idStatusProTreatments;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getNameProperties() {
        return nameProperties;
    }

    public void setNameProperties(String nameProperties) {
        this.nameProperties = nameProperties;
    }

    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }

    public String getNameStatusProTreatments() {
        return nameStatusProTreatments;
    }

    public void setNameStatusProTreatments(String nameStatusProTreatments) {
        this.nameStatusProTreatments = nameStatusProTreatments;
    }

    public String getFileRequirement2() {
        return fileRequirement2;
    }

    public void setFileRequirement2(String fileRequirement2) {
        this.fileRequirement2 = fileRequirement2;
    }

    public String getFileRequirement1() {
        return fileRequirement1;
    }

    public void setFileRequirement1(String fileRequirement1) {
        this.fileRequirement1 = fileRequirement1;
    }

    public String getFileRequirement3() {
        return fileRequirement3;
    }

    public void setFileRequirement3(String fileRequirement3) {
        this.fileRequirement3 = fileRequirement3;
    }

    public String getFileRequirement4() {
        return fileRequirement4;
    }

    public void setFileRequirement4(String fileRequirement4) {
        this.fileRequirement4 = fileRequirement4;
    }

    public String getFileRequirement5() {
        return fileRequirement5;
    }

    public void setFileRequirement5(String fileRequirement5) {
        this.fileRequirement5 = fileRequirement5;
    }

    public Date getDateTreatments() {
        return dateTreatments;
    }

    public void setDateTreatments(Date dateTreatments) {
        this.dateTreatments = dateTreatments;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isHasGenerate() {
        return hasGenerate;
    }

    public void setHasGenerate(boolean hasGenerate) {
        this.hasGenerate = hasGenerate;
    }

    public Integer getPoseeRecaudos() {
        return poseeRecaudos;
    }

    public void setPoseeRecaudos(Integer poseeRecaudos) {
        this.poseeRecaudos = poseeRecaudos;
    }

    public Integer getIdPropertiesTreatments() {
        return idPropertiesTreatments;
    }

    public void setIdPropertiesTreatments(Integer idPropertiesTreatments) {
        this.idPropertiesTreatments = idPropertiesTreatments;
    }
    
    
    
}
