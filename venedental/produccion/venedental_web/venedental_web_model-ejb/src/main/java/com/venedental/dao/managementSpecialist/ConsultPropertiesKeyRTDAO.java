/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultPropertiesKeysRTOutput;
import com.venedental.dto.managementSpecialist.ConsultPropertiesKeysRTInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesKeyRTDAO extends AbstractQueryDAO<ConsultPropertiesKeysRTInput, ConsultPropertiesKeysRTOutput>{

    public ConsultPropertiesKeyRTDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_009", 2);
    }
    
    
    @Override
    public void prepareInput(ConsultPropertiesKeysRTInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKey());
        statement.setInt(2, input.getIdPiece());
    }
    
    @Override
    public ConsultPropertiesKeysRTOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultPropertiesKeysRTOutput consultPropertiesKeysRTOutput = new ConsultPropertiesKeysRTOutput();
        consultPropertiesKeysRTOutput.setId_piece(rs.getInt("id_pieza"));
        consultPropertiesKeysRTOutput.setId_piece_treatment_key(rs.getInt("id_pieza_tratamiento_clave"));
        consultPropertiesKeysRTOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultPropertiesKeysRTOutput.setId_treatment(rs.getInt("id_tratamiento"));
        consultPropertiesKeysRTOutput.setId_treatment_key(rs.getInt("id_tratamiento_clave"));
        consultPropertiesKeysRTOutput.setValue_piece(rs.getString("valor_pieza"));
         consultPropertiesKeysRTOutput.setDateOrigin(rs.getDate("fecha_ejecucion_pieza"));
         consultPropertiesKeysRTOutput.setDate(rs.getString("fecha_ejecucion_pieza"));
         consultPropertiesKeysRTOutput.setReportePago(rs.getInt("reporte_pago_generado"));
        return consultPropertiesKeysRTOutput;
    }
    
    
    
    
    
    




    
}
