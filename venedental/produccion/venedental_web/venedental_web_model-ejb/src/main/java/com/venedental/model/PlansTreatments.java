/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "plans_treatments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlansTreatments.findAll", query = "SELECT p FROM PlansTreatments p"),
    @NamedQuery(name = "PlansTreatments.findByPlansId", query = "SELECT p FROM PlansTreatments p WHERE p.plansId.id = :plansId"),
    @NamedQuery(name = "PlansTreatments.findById", query = "SELECT p FROM PlansTreatments p WHERE p.id = :id")})
public class PlansTreatments implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "treatments_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Treatments treatmentsId;

    @JoinColumn(name = "plans_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Plans plansId;

    @OneToMany(mappedBy = "plansTreatmentsId")
    private List<PlansTreatmentsKeys> plansTreatmentsKeysList;
    
    @Transient
    private Integer idOutputTreatments;
    
    @Transient
    private Integer idTreatments;
    
    @Transient
    private String nameTreatments;

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }

    public PlansTreatments() {
    }

    public PlansTreatments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Treatments getTreatmentsId() {
        return treatmentsId;
    }

    public void setTreatmentsId(Treatments treatmentsId) {
        this.treatmentsId = treatmentsId;
    }

    public Plans getPlansId() {
        return plansId;
    }

    public void setPlansId(Plans plansId) {
        this.plansId = plansId;
    }
    
    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlansTreatments)) {
            return false;
        }
        PlansTreatments other = (PlansTreatments) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.venedental.model.PlansTreatments[ id=" + id + " ]";
    }

    @XmlTransient
    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysList() {
        return plansTreatmentsKeysList;
    }

    public void setPlansTreatmentsKeysList(List<PlansTreatmentsKeys> plansTreatmentsKeysList) {
        this.plansTreatmentsKeysList = plansTreatmentsKeysList;
    }

}
