/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class FindKeysToAuditBySpecialistAndDateInput {
    
    private Integer idSpecialist;
    private Integer idStatus;
    private Date dateFrom;
    private Date dateUntil; 

    public FindKeysToAuditBySpecialistAndDateInput(Integer idSpecialist, Integer idStatus, Date dateFrom, Date dateUntil) {
        this.idSpecialist = idSpecialist;
        this.idStatus = idStatus;
        this.dateFrom = dateFrom;
        this.dateUntil = dateUntil;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateUntil() {
        return dateUntil;
    }

    public void setDateUntil(Date dateUntil) {
        this.dateUntil = dateUntil;
    }
    
    
    
    
}
