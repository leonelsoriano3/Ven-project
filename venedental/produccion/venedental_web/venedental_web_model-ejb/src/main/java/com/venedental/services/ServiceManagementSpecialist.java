/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.consultKeys.ConsultDatePatientsDetailsTreatmentDAO;
import com.venedental.dao.consultKeys.ConsultKeysPatientsDetailsDAO;
import com.venedental.dao.consultKeys.ConsultKeysPatientsDetailsTreatmentDAO;
import com.venedental.dao.consultKeys.ReasonDeleteKeyDAO;
import com.venedental.dao.keysPatients.GenerateNumberKeyDAO;
import com.venedental.dao.keysPatients.KeysConsultDateMinDAO;
import com.venedental.dao.keysPatients.KeysDetailsDAO;
import com.venedental.dao.keysPatients.RegisterKeyDAO;
import com.venedental.dao.managementSpecialist.*;
import com.venedental.dto.consultKeys.ConsultDateDetailTreatmentOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailOutput;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;
import com.venedental.dto.consultKeys.ReasonDeleteKeyInput;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import com.venedental.dto.keyPatients.RegisterKeyInput;
import com.venedental.dto.keyPatients.RegisterKeyOutput;
import com.venedental.dto.managementSpecialist.*;
import com.venedental.dao.planTreatmentKey.RegisterPlanTreatKeyDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.RegisterPropertiesPlanTreatmentKeyDAO;
import com.venedental.dto.keyPatients.KeysConsultDateMinOutput;
import com.venedental.dto.planTreatmentKey.PlanTreatmentKeyInput;
import com.venedental.dto.propertiesPlanTreatmentKey.PropertiesPlanTreatmentKeyInput;
import com.venedental.model.PlansTreatmentsKeys;
import com.venedental.model.PropertiesPlansTreatmentsKey;

import java.sql.Date;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.util.List;

/**
 *
 * @author akdesk01
 */
@Stateless
public class ServiceManagementSpecialist {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    public DataKeyDTO getFindKeysToAuditByTooth(Integer key, Integer tooth) {
        DataKeyInput dataKeyInput = new DataKeyInput(key, tooth);
        FindDataKeyDAO findDataKeyDAO = new FindDataKeyDAO(ds);
        findDataKeyDAO.execute(dataKeyInput);
        return findDataKeyDAO.getResult();
    }

    public List<FindPieceByidKeyOut> findPiece(Integer key) {
        FindPieceByidKeyDAO findPieceByidKeyDAO = new FindPieceByidKeyDAO(ds);
        findPieceByidKeyDAO.execute(key);
        return findPieceByidKeyDAO.getResultList();
    }
 
    public List<ConsultKeysSpecialistOutput> findKeysSpecialist(Integer idSpecialist, Date dateStart, Date dateEnd, String numberForKey, String patient) {
        ConsultKeysSpecialistDAO consultKeysSpecialistDAO = new ConsultKeysSpecialistDAO(ds);
        ConsultKeysSpecialistInput input = new ConsultKeysSpecialistInput(idSpecialist, dateStart, dateEnd, numberForKey, patient);
        input.setIdSpecialist(idSpecialist);
        input.setDate(dateStart);
        input.setDateEnd(dateEnd);
        input.setNumberKey(numberForKey);
        input.setPatient(patient);
        consultKeysSpecialistDAO.execute(input);
        return consultKeysSpecialistDAO.getResultList();
    }

    public FindInfoSpecialistOutput findInfoSpecialist(Integer idKey) {

        FindInfoSpecialistOutput findInfoSpecialistOutput = null;
        FindInfoSpecialistDAO findInfoSpecialistDAO = new FindInfoSpecialistDAO(ds);
        findInfoSpecialistDAO.execute(idKey);
        findInfoSpecialistOutput = findInfoSpecialistDAO.getResult();

        return findInfoSpecialistOutput;

    }

    public List<FindPropertyByKeyIdOut> getTreatmentToTooth(Integer key, Integer tooth) {

        FindPropertyByKeyIdIn findPropertyByKeyIdIn;
        FindPropertyByKeyIdDAO findPropertyByKeyIdDAO;

        findPropertyByKeyIdIn = new FindPropertyByKeyIdIn(key, tooth);
        findPropertyByKeyIdDAO = new FindPropertyByKeyIdDAO(ds);
        findPropertyByKeyIdDAO.execute(findPropertyByKeyIdIn);
        return findPropertyByKeyIdDAO.getResultList();

    }

    public List<FindTreatmentWithoutPieceOut> getListTreatmentWithoutPiece(Integer input) {

        FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = null;

        FindTreatmentWithoutPieceDAO findTreatmentWithoutPieceDAO = new FindTreatmentWithoutPieceDAO(ds);
        findTreatmentWithoutPieceDAO.execute(input);
        return findTreatmentWithoutPieceDAO.getResultList();
    }

    public List<ConsultPropertiesTreatmentsOutput> FindPieceandTreatment(Integer input) {

        FindTreatmentWithoutPieceOut findTreatmentWithoutPieceOut = null;

        ConsultPropestiesTreatmentDAO consultPropestiesTreatmentDAO = new ConsultPropestiesTreatmentDAO(ds);
        consultPropestiesTreatmentDAO.execute(input);
        return consultPropestiesTreatmentDAO.getResultList();
    }

    public List<ConsultTreatmentSelectOutput> FindTreatmentSelect(Integer input) {

        ConsultTreatmentSelectOutput consultTreatmentSelectOutput = null;

        ConsultTreatmentSelectDAO consultTreatmentSelectDAO = new ConsultTreatmentSelectDAO(ds);
        consultTreatmentSelectDAO.execute(input);
        return consultTreatmentSelectDAO.getResultList();
    }

    public InsertPlantTreatmentKeyOut InsertPlantTreamentKey(InsertPlantTreatmentKeyIn insertPlantTreatmentKeyIn) {

        InsertPlantTreatmentKeyOut insertPlantTreatmentKeyOut = null;

        InsertPlantTreamentKeyDAO insertPlantTreamentKeyDAO = new InsertPlantTreamentKeyDAO(ds);
        insertPlantTreamentKeyDAO.execute(insertPlantTreatmentKeyIn);
        insertPlantTreatmentKeyOut = insertPlantTreamentKeyDAO.getResult();
        return insertPlantTreatmentKeyOut;
    }

    public InsertProplatreKeyOut InsertProplatreKeyService(InsertProplatreKeyIn input) {

        InsertProplatreKeyOut insertProplatreKeyOut = null;

        InsertProplatreKeyDAO insertProplatreKeyDAO = new InsertProplatreKeyDAO(ds);
        insertProplatreKeyDAO.execute(input);
        insertProplatreKeyOut = insertProplatreKeyDAO.getResult();
        return insertProplatreKeyOut;
    }

    /*Consultar las piezas ya agregadas a la clave en registrar diagnostico*/
    public List<ConsultPropertiesKeysRTOutput> ConsultPropertiesKeyRegisterT(Integer idKey, Integer idPiece) {
        ConsultPropertiesKeyRTDAO consultPropertiesKeyRTDAO = new ConsultPropertiesKeyRTDAO(ds);
        ConsultPropertiesKeysRTInput input=new ConsultPropertiesKeysRTInput();
        input.setIdKey(idKey);
        input.setIdPiece(idPiece);
        consultPropertiesKeyRTDAO.execute(input);
        return consultPropertiesKeyRTDAO.getResultList();
    }

    /*Consulta todos los tratamientos sin piezas asignado o no a la clave como diagnostico principal*/
    public ConsultTreatmentsKeyRTOutput ConsultTreatmentsKeyRegisterT(Integer input) {

        ConsultTreatmentsKeyRTOutput consultTreatmentsKeyRTOutput = null;

        ConsultTreatmentKeyRTDAO consultTreatmenteyRTDAO = new ConsultTreatmentKeyRTDAO(ds);
        consultTreatmenteyRTDAO.execute(input);
        return consultTreatmenteyRTDAO.getResult();
    }

    /*Consulta los tratamientos disponibles segun la pieza y el id de la clave*/
    public List<ConsultPropertiesTreatmentsRTOutput> ConsultPropertiesTreatmentsRegisterT(Integer idKey, Integer idPiece) {
        ConsultPropertiesTreatmentsDAO consultPropertiesTreatmentsDAO = new ConsultPropertiesTreatmentsDAO(ds);
        ConsultPropertiesTreatmentsRTInput input = new ConsultPropertiesTreatmentsRTInput();
        input.setId_Key(idKey);
        input.setId_piece(idPiece);
        consultPropertiesTreatmentsDAO.execute(input);
        return consultPropertiesTreatmentsDAO.getResultList();
    }

    /*Elimina los tratamientos en registrar tratamiento, tratamientos sin pieza*/
    public void DeleteTreatmentsKey(Integer idTreatmentKey) throws SQLException {
        RemoveAssignedTreatmentDAO removeAssignedTreatmentDAO = new RemoveAssignedTreatmentDAO(ds);
        RemoveAssignedtreatmentInput input = new RemoveAssignedtreatmentInput();
        input.setIdTreatmentKey(idTreatmentKey);
        removeAssignedTreatmentDAO.execute(input);

    }

    /*Elimina las piezas de la cave, agregadas en registar diagnóstico*/
    public void DeletePropertiesKey(Integer idPieceTreatment) throws SQLException {
        RemoveAssignedPropertiesDAO removeAssignedPropertiesDAO = new RemoveAssignedPropertiesDAO(ds);
        RemoveAssignedPropertiesInput input = new RemoveAssignedPropertiesInput();
        input.setIdPieceTreatment(idPieceTreatment);
        removeAssignedPropertiesDAO.execute(input);

    }

    

    /*Registrar tratamientos a la clave*/
    public InsertPlantTreatmentKeyOut InsertTreatmentForKey(Integer idKeys, Integer idPlanTreatment, java.util.Date dateApplication, Integer idStatus, Integer idScaleTreatments, String observation, Integer idUser, java.util.Date dateReception) {
        InsertPlantTreamentKeyDAO insertPlantTreamentKeyDAO = new InsertPlantTreamentKeyDAO(ds);
        InsertPlantTreatmentKeyIn input = new InsertPlantTreatmentKeyIn();
        input.setDateApplication(dateApplication);
        input.setDateReception(dateReception);
        input.setIdKey(idKeys);
        input.setIdPlanTreatment(idPlanTreatment);
        input.setIdScaleTreatments(idScaleTreatments);
        input.setIdStatus(idStatus);
        input.setIdUser(idUser);
        input.setObservation(observation);
        insertPlantTreamentKeyDAO.execute(input);
        return insertPlantTreamentKeyDAO.getResult();
    }
    
    /**
     * Servicio para registrar peizas dentales a un tratamiento de una clave
     *
     * @param idPlanTreatmentKey
     * @param idPropetieTreatment
     * @param idScaleTratment
     * @param dateApplication
     * @param idStatus
     * @param observation
     * @param idUser
     * @param dateReception
     * @return 
     * @throws SQLException
     */
    public PropertiesPlansTreatmentsKey InsertPropertiesForKey(Integer idPlanTreatmentKey, Integer idPropetieTreatment,Integer idScaleTratment, java.util.Date dateApplication, Integer idStatus, String observation, Integer idUser, java.util.Date dateReception) throws SQLException {
        RegisterPropertiesPlanTreatmentKeyDAO registrerPlanTreatmentKeyDAO = new RegisterPropertiesPlanTreatmentKeyDAO(ds);
        PropertiesPlanTreatmentKeyInput input = new PropertiesPlanTreatmentKeyInput(idPlanTreatmentKey, idPropetieTreatment,idScaleTratment,dateApplication,idStatus, observation, idUser, dateReception);
        registrerPlanTreatmentKeyDAO.execute(input);
        return registrerPlanTreatmentKeyDAO.getResult();
    }

   
    /*Consultar tratamientos generales asignados a la clave*/
    public List<ConsultTreatmentAssignedOutput> ConsultTreatmentAssignedforKey(Integer idKey) throws SQLException {

        ConsultTreatmentAssignedDAO consultTreatmentAssignedDAO = new ConsultTreatmentAssignedDAO(ds);
        consultTreatmentAssignedDAO.execute(idKey);
        return consultTreatmentAssignedDAO.getResultList();

    }

    /*Inserta los nombres de los archivos en base de datos*/
    public void InsertFileSupport(Integer idTreatmentKey, String nameFile) throws SQLException {
        InsertFileSupportDAO insertFileSupportDAO = new InsertFileSupportDAO(ds);
        InsertFileSupportInput input = new InsertFileSupportInput(idTreatmentKey,nameFile);
        insertFileSupportDAO.execute(input);
    }

    /*Consultar recaudos de tratamientos*/
    public List <ConsultNameFileOutput> ConsultnameFile(Integer idTreatmentKey) throws SQLException {

        ConsultFileNameSupportDAO consultFileNameSupportDAO = new ConsultFileNameSupportDAO(ds);
        consultFileNameSupportDAO.execute(idTreatmentKey);
        return consultFileNameSupportDAO.getResultList();

    }
    
    //para buscar la fechas minimas
     public Date getDateMin(Integer input){
         
        KeysConsultDateMinDAO keysConsultDateMinDAO = new KeysConsultDateMinDAO(ds);
        keysConsultDateMinDAO.execute(input);
        return (Date) keysConsultDateMinDAO.getResult().getDateMin();

     

    }

     //consultar nombre del archivo de los recaudos
   public List <ConsultNameFileOutput> ConsultNameFileRecaudos(Integer idTreatmentKey) throws SQLException {

        ConsultFileNameDAO consultFileNameDAO = new ConsultFileNameDAO(ds);
        consultFileNameDAO.execute(idTreatmentKey);
        return consultFileNameDAO.getResultList();

    }
     
       /**
     * Servicio que permite buscar las fechas para elc alendario del filtro en el historial
     *
     * @param idPatient
     * @param idSpecialist
     * @return
     */
      public List<ConsultHistorialOutput> findDateHistorialFilter(Integer idPatient, Integer idSpecialist) {
        ConsultHistorialDAO consultDateForHistorialDAO = new ConsultHistorialDAO(ds);
        ConsultHistorialInput consultDateForHistorialInput = new ConsultHistorialInput(idPatient,idSpecialist);
        consultDateForHistorialDAO.execute(consultDateForHistorialInput);
        return consultDateForHistorialDAO.getResultList();
    }


    public List<ConsultDateDetailTreatmentOutput> findDateMinYMaxDetalle(Integer idKey){
        ConsultDatePatientsDetailsTreatmentDAO consultDatePatientsDetailsTreatmentDAO = new ConsultDatePatientsDetailsTreatmentDAO(ds) {};
        consultDatePatientsDetailsTreatmentDAO.execute(idKey);
        return consultDatePatientsDetailsTreatmentDAO.getResultList();
    }

    public void reasonDeleteKey(Integer idKey, String observation, Integer idUser) throws SQLException{
        ReasonDeleteKeyDAO reasonDeleteKeyDAO=new ReasonDeleteKeyDAO(ds);
        ReasonDeleteKeyInput input= new ReasonDeleteKeyInput();
        input.setIdKey(idKey);
        input.setObservation(observation);
        input.setIdUser(idUser);
        reasonDeleteKeyDAO.execute(input);
    }


    public List<ConsultKeysPatientsDetailOutput> detailsConsultKeysPatients(Integer key) {
        ConsultKeysPatientsDetailsDAO consultKeysPatientsDetailsDAO = new ConsultKeysPatientsDetailsDAO(ds);
        consultKeysPatientsDetailsDAO.execute(key);
        return consultKeysPatientsDetailsDAO.getResultList();
    }

    public List<ConsultKeysPatientsDetailTreatmentOutput> detailsConsultKeysPatientsTreatments(Integer key) {
        ConsultKeysPatientsDetailsTreatmentDAO consultKeysPatientsDetailsTreatmentDAO = new ConsultKeysPatientsDetailsTreatmentDAO(ds);
        consultKeysPatientsDetailsTreatmentDAO.execute(key);
        return consultKeysPatientsDetailsTreatmentDAO.getResultList();
    }

    /**
     * Servicio que permite generar el numero de una clave
     * @param idPlan
     * @return
     */
    public String generateNumber (Integer idPlan){
        GenerateNumberKeyDAO generateNumberKeyDAO = new GenerateNumberKeyDAO(ds);
        generateNumberKeyDAO.execute(idPlan);
        return generateNumberKeyDAO.getResult();
    }




    public List<RegisterKeyOutput> registerKey (String number, String typeAttention, Integer idPatient, Integer idSpecialist,
                                                Integer idUser, Integer idMedicalOffice, Integer idCompany, Integer idPlan){
        RegisterKeyDAO registerKey = new RegisterKeyDAO(ds);
        RegisterKeyInput registerKeyInput = new RegisterKeyInput(number,typeAttention,idPatient,idSpecialist,idUser,
                idMedicalOffice, idCompany,idPlan);
        registerKey.execute(registerKeyInput);
        return registerKey.getResultList();
    }


    /**
     * Servicio que permite buscar el detalle de una clave dado su id
     * @param idKey
     * @return
     */
    public KeysDetailsOutput findDetails (Integer idKey){
        KeysDetailsDAO keysDetailsDAO = new KeysDetailsDAO(ds);
        keysDetailsDAO.execute(idKey);
        return keysDetailsDAO.getResult();
    }


}
