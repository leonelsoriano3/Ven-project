/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;

/**
 *
 * @author akdesk01
 */
public class HistoryBaremosOutput implements Serializable{
    private java.util.Date effective_date;
    private Integer idTreatment;
    private String nameTreatment;
    private Double baremo;
    private Integer idBaremo;
    private String processData;


    public HistoryBaremosOutput() {
    }

    public HistoryBaremosOutput(Integer idBaremo,Integer idTreatment, String nameTreatment, java.sql.Date effective_date, Double baremo, String processData ) {
        this.idBaremo=idBaremo;
        this.idTreatment=idTreatment;
        this.nameTreatment=nameTreatment;
        this.effective_date=effective_date;
        this.baremo=baremo;
        this.processData=processData;

    }

    public java.util.Date getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(java.util.Date effective_date) {
        this.effective_date = effective_date;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Double getBaremo() {
        return baremo;
    }

    public void setBaremo(Double baremo) {
        this.baremo = baremo;
    }

    public Integer getIdBaremo() {
        return idBaremo;
    }

    public void setIdBaremo(Integer idBaremo) {
        this.idBaremo = idBaremo;
    }

    public String getProcessData() {
        return processData;
    }

    public void setProcessData(String processData) {
        
        this.processData = processData;
    }

}
