/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.plans;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.plans.FindPlanByIdOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindPlansByIdDAO extends AbstractQueryDAO<Integer, FindPlanByIdOutput> {
    
    
      public FindPlansByIdDAO(DataSource dataSource) {
        super(dataSource, "SP_PLANES_ASEGURADORA", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
            
    }

    @Override
    public FindPlanByIdOutput prepareOutput(ResultSet rs , CallableStatement statement) throws SQLException {
        FindPlanByIdOutput findPlanByIdOutput = new FindPlanByIdOutput();
        findPlanByIdOutput.setIdPlan(rs.getInt("id"));
        return findPlanByIdOutput;
    }
}