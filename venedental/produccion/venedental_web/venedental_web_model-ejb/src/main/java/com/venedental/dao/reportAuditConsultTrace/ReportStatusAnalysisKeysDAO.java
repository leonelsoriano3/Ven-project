/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.reportAuditConsultTrace;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysInput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysOuput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author Salomé Gené
 */
public class ReportStatusAnalysisKeysDAO extends AbstractQueryDAO<ReportStatusAnalysisKeysInput, ReportStatusAnalysisKeysOuput> {

    public ReportStatusAnalysisKeysDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_016", 4);
    }

    @Override
    public void prepareInput(ReportStatusAnalysisKeysInput input, CallableStatement statement) throws SQLException {
        statement.setDate(1, input.getStartDate());
        statement.setDate(2, input.getEndDate());
        if (input.getIdKey().equalsIgnoreCase("")) {
            statement.setNull(3, java.sql.Types.NULL);
        } else {
            statement.setString(3, input.getIdKey());
        }
        if (input.getSpecialist().equalsIgnoreCase("")) {
            statement.setNull(4, java.sql.Types.NULL);
        } else {
            statement.setString(4, input.getSpecialist());
        }

    }

    @Override
    public ReportStatusAnalysisKeysOuput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ReportStatusAnalysisKeysOuput ouput = new ReportStatusAnalysisKeysOuput();

        ouput.setNumReportTreatment(rs.getInt("numero_reporte_tratamiento"));
        ouput.setIdPaymentReport(rs.getInt("id_reporte_pago"));
        ouput.setNumPaymentReport(rs.getString("numero_reporte_pago"));
        ouput.setIdKey(rs.getInt("id_clave"));
        ouput.setNumKey(rs.getString("numero_clave"));
        ouput.setTreatmentName(rs.getString("nombre_tratamiento"));
        ouput.setNumTooth(rs.getString("numero_pieza"));
        ouput.setStatusName(rs.getString("nombre_estatus"));
        ouput.setIdSpecialist(rs.getInt("id_especialista"));
        ouput.setSpecialist(rs.getString("nombre_especialista"));
       
        return ouput;

    }

}
