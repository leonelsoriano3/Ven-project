/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PropertiesPlansTreatmentsKeyFacadeLocal {

    PropertiesPlansTreatmentsKey create(PropertiesPlansTreatmentsKey properties);

    PropertiesPlansTreatmentsKey edit(PropertiesPlansTreatmentsKey properties);

    void remove(PropertiesPlansTreatmentsKey properties);

    PropertiesPlansTreatmentsKey find(Object id);

    List<PropertiesPlansTreatmentsKey> findAll();

    List<PropertiesPlansTreatmentsKey> findRange(int[] range);
    
    List<PropertiesPlansTreatmentsKey> findByTreatmentsId(Integer id);
    
    List<PropertiesPlansTreatmentsKey> findByPlansTreatmentsKeys(Integer id);
    
    List<PropertiesPlansTreatmentsKey> findByKeysAndStatus(Integer id,Integer idStatus);
    
    List<PropertiesPlansTreatmentsKey> findByKeys(Integer id);
    
    void deleteByPlansTreatmentsKeys(int id);
    
    String countProperties(Integer idPlansTreatmentsKeys,Integer status);
    
     
    int count();
      
}
