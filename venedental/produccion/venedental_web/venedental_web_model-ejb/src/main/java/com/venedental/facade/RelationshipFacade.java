/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import com.venedental.model.Relationship;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author CarlosDaniel
 */
@Stateless
public class RelationshipFacade extends AbstractFacade<Relationship> implements RelationshipFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RelationshipFacade() {
        super(Relationship.class);
    }

    @Override
    public List<Relationship> findById(Integer id) {
        TypedQuery<Relationship> query = em.createNamedQuery("Relationship.findById", Relationship.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

}
