/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.FindPlanstkStatusSPByKeyInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPlanTreatKeyWPByKeyAndStatusDAO extends AbstractQueryDAO<FindPlanstkStatusSPByKeyInput, PlansTreatmentsKeys> {

    public FindPlanTreatKeyWPByKeyAndStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_G_004", 3);
    }

    @Override
    public void prepareInput(FindPlanstkStatusSPByKeyInput input, CallableStatement statement) {
        try {
            statement.setInt(1, input.getIdKeyPatient());
            statement.setInt(2, input.getIdStatus());
            if (input.getReceptionDate() != null) {
                statement.setDate(3, new Date(input.getReceptionDate().getTime()));
            } else {
                statement.setNull(3, java.sql.Types.NULL);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public PlansTreatmentsKeys prepareOutput(ResultSet rs, CallableStatement statement) {
        try {
            PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
            plansTreatmentsKeys.setId(rs.getInt("id"));
            plansTreatmentsKeys.setObservation(rs.getString("observation"));
            plansTreatmentsKeys.setDatePlansTreatmentsKeys(rs.getDate("date_plans_treatments_keys"));
            plansTreatmentsKeys.setKeys_id(rs.getInt("keys_id"));
            plansTreatmentsKeys.setPlans_treatments_id(rs.getInt("plans_treatments_id"));
            plansTreatmentsKeys.setIdTreatments(rs.getInt("ID_TRATAMIENTO"));
            plansTreatmentsKeys.setNameTreatments(rs.getString("NOMBRE_TRATAMIENTO"));
            plansTreatmentsKeys.setStatusId(rs.getInt("ID_ESTATUS"));
            plansTreatmentsKeys.setIdDescriptionStatusTreatments(rs.getString("NOMBRE_ESTATUS"));
            plansTreatmentsKeys.setHasGenerated(rs.getBoolean("FUE_GENERADO"));
          //  plansTreatmentsKeys.setPrice(rs.getDouble("PRECIO"));
            return plansTreatmentsKeys;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

}
