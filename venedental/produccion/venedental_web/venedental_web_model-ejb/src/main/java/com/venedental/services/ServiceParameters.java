/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.parameters.FindParametersDAO;
import com.venedental.dto.parameters.FindParametersOutput;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServiceParameters {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
    
        /**
     * Servicio que permite buscar el valor del parámetro dado el nombre del parámetro
     *
     * @param codigo
     * 
     * @return
     */
    public List<FindParametersOutput> FindParameters(String codigo) {
        FindParametersDAO findParametersDAO = new FindParametersDAO(ds);
        findParametersDAO.execute(codigo);
        return findParametersDAO.getResultList();
    }
}
