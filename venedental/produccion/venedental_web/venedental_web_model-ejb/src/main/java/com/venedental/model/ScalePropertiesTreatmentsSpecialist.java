/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Desarrollo
 */
@Entity
@Table(name = "scale_properties_treatments_specialist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScalePropertiesTreatmentsSpecialist.findAll", query = "SELECT s FROM ScalePropertiesTreatmentsSpecialist s"),
    @NamedQuery(name = "ScalePropertiesTreatmentsSpecialist.findById", query = "SELECT s FROM ScalePropertiesTreatmentsSpecialist s WHERE s.id = :id"),
    @NamedQuery(name = "ScalePropertiesTreatmentsSpecialist.findByIdScalePropertiesTreatments", query = "SELECT s FROM ScalePropertiesTreatmentsSpecialist s WHERE s.idScalePropertiesTreatments = :idScalePropertiesTreatments"),
    @NamedQuery(name = "ScalePropertiesTreatmentsSpecialist.findByIdScaleTreatmentsSpecialist", query = "SELECT s FROM ScalePropertiesTreatmentsSpecialist s WHERE s.idScaleTreatmentsSpecialist = :idScaleTreatmentsSpecialist"),
    @NamedQuery(name = "ScalePropertiesTreatmentsSpecialist.findByStatus", query = "SELECT s FROM ScalePropertiesTreatmentsSpecialist s WHERE s.status = :status")})
public class ScalePropertiesTreatmentsSpecialist implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "status")
    private Integer status;

    @JoinColumn(name = "id_scale_treatments_specialist", referencedColumnName = "id")
    @ManyToOne
    private ScaleTreatmentsSpecialist idScaleTreatmentsSpecialist;
    
    @JoinColumn(name = "id_scale_properties_treatments", referencedColumnName = "id")
    @ManyToOne
    private ScalePropertiesTreatments idScalePropertiesTreatments;

    public ScalePropertiesTreatmentsSpecialist() {
    }

    public ScalePropertiesTreatmentsSpecialist(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScalePropertiesTreatmentsSpecialist)) {
            return false;
        }
        ScalePropertiesTreatmentsSpecialist other = (ScalePropertiesTreatmentsSpecialist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.ScalePropertiesTreatmentsSpecialist[ id=" + id + " ]";
    }

    public ScaleTreatmentsSpecialist getIdScaleTreatmentsSpecialist() {
        return idScaleTreatmentsSpecialist;
    }

    public void setIdScaleTreatmentsSpecialist(ScaleTreatmentsSpecialist idScaleTreatmentsSpecialist) {
        this.idScaleTreatmentsSpecialist = idScaleTreatmentsSpecialist;
    }

    public ScalePropertiesTreatments getIdScalePropertiesTreatments() {
        return idScalePropertiesTreatments;
    }

    public void setIdScalePropertiesTreatments(ScalePropertiesTreatments idScalePropertiesTreatments) {
        this.idScalePropertiesTreatments = idScalePropertiesTreatments;
    }

}
