/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.sql.Date;


/**
 *
 * @author akdesk01
 */
public class ConsultKeysSpecialistInput {

    private String namePatient,numberKey, patient;
    
    private Integer idSpecialist;
    
    private Date date, dateEnd,dateEndF;
    
    public ConsultKeysSpecialistInput( Integer idSpecialist,Date date, Date dateEnd,String numberKey, String patient) {
        this.patient=patient;
        this.numberKey=numberKey;
        this.idSpecialist=idSpecialist;
        this.date=date;
        this.dateEnd=dateEnd;
        
        
    }


    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public Date getDateEndF() {
        return dateEndF;
    }

    public void setDateEndF(Date dateEndF) {
        this.dateEndF = dateEndF;
    }
    
    
    
    
    
}
