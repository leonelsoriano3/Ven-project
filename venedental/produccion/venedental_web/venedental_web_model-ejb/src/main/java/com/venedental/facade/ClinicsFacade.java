/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Clinics;
import com.venedental.model.Specialists;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class ClinicsFacade extends AbstractFacade<Clinics> implements ClinicsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClinicsFacade() {
        super(Clinics.class);
    }

    @Override
    public Clinics edit(Clinics entity) {
        return super.edit(entity);
    }

    @Override
    public Clinics create(Clinics entity) {
        return super.create(entity);
    }

    @Override
    public void remove(Clinics entity) {
        super.remove(entity);
    }

    @Override
    public void deleteClinics(Integer id) {
        TypedQuery<Clinics> query = em.createNamedQuery("Clinics.deleteClinics", Clinics.class);
        query.setParameter("clinics_id", id);
        query.executeUpdate();
        em.flush();
        em.clear();
    }

    @Override
    public List<Clinics> findByStatus(int status) {
        TypedQuery<Clinics> query = em.createNamedQuery("Clinics.findByStatus", Clinics.class);
        query.setParameter("status", status);
        return query.getResultList();
    }

    @Override
    public Clinics findByRif(String name, String rif, String address) {
        try {
            TypedQuery<Clinics> query = em.createNamedQuery("Clinics.findByRif", Clinics.class);
            query.setParameter("name", name);
            query.setParameter("rif", rif);
            query.setParameter("address", address);
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public void updateStatus(Integer id) {
        TypedQuery<Clinics> query = em.createNamedQuery("Clinics.updateStatus", Clinics.class);
        query.setParameter("clinics_id", id);
        query.executeUpdate();
        em.flush();
        em.clear();
    }

}
