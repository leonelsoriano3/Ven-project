package com.venedental.dao.user;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.User.UserDTO;
import com.venedental.model.Specialists;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class FindUserByIdDAO extends AbstractQueryDAO<Integer,UserDTO> {

    public FindUserByIdDAO(DataSource dataSource) {
        super(dataSource, "SP_T_USERS_G_001", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public UserDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        UserDTO user = new UserDTO();
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        user.setStatus(rs.getInt("status"));
        user.setEmail(rs.getString("email"));
        return user;
    }


}
