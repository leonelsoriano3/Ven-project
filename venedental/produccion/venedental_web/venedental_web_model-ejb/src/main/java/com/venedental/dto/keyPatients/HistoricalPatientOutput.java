/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class HistoricalPatientOutput {

    Integer idPatient;
    String namePatient;
    Integer idKeyPatient;
    Integer idPlanTreatmentKey;
    Integer idTreatment;
    String nameTratment;
    Integer idPropertiePlantreatmentKey;
    String numberPropertie;
    Integer idStatus;
    String observation;
    String nameStatus;
    Date dateApplication;
    String numberKey;
    String company;
    String specialist;
    String relationShip;
    String insurance;
    String plan;
    Integer idSpecialist;
    String dateForHistorial;
    private String fechaPrueba;
    private  String nameSpecialist;
    private Double price;
    private String nameHealthArea;

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNameHealthArea() {
        return nameHealthArea;
    }

    public void setNameHealthArea(String nameHealthArea) {
        this.nameHealthArea = nameHealthArea;
    }

    public String getFechaPrueba() {
        return fechaPrueba;
    }

    public void setFechaPrueba(String fechaPrueba) {
        this.fechaPrueba = fechaPrueba;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTratment() {
        return nameTratment;
    }

    public void setNameTratment(String nameTratment) {
        this.nameTratment = nameTratment;
    }

    public Integer getIdPropertiePlantreatmentKey() {
        return idPropertiePlantreatmentKey;
    }

    public void setIdPropertiePlantreatmentKey(Integer idPropertiePlantreatmentKey) {
        this.idPropertiePlantreatmentKey = idPropertiePlantreatmentKey;
    }

    public String getNumberPropertie() {
        return numberPropertie;
    }

    public void setNumberPropertie(String numberPropertie) {
        this.numberPropertie = numberPropertie;
        if(this.numberPropertie.equals("")){
            this.numberPropertie="-";
        }else{
            this.numberPropertie = numberPropertie;
        }
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(Date dateApplication) {
        this.dateApplication = dateApplication;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getRelationShip() {
        return relationShip;
    }

    public void setRelationShip(String relationShip) {
        this.relationShip = relationShip;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getDateForHistorial() {
        return dateForHistorial;
    }

    public void setDateForHistorial(String dateForHistorial) {
        this.dateForHistorial = dateForHistorial;
    }















}