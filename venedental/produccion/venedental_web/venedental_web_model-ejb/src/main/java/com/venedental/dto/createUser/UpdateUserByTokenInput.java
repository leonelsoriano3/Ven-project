package com.venedental.dto.createUser;

/**
 * Created by akdesk10 on 22/06/16.
 */
public class UpdateUserByTokenInput {

    private String token;

    public UpdateUserByTokenInput(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
