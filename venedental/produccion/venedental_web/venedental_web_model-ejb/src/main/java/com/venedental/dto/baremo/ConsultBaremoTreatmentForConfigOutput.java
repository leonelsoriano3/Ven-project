/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 *
 * @author akdesk01
 */
public class ConsultBaremoTreatmentForConfigOutput {

    private Date dateBaremo,dateUtil;
    private Integer idTreatment;
    private String nameTreatment, dateBaremoString;
    private Double baremo;
    private Integer idBaremo;
    

    public ConsultBaremoTreatmentForConfigOutput() {
    }

    public ConsultBaremoTreatmentForConfigOutput(Date dateBaremo, Integer idTreatment, String nameTreatment, Double baremo, Integer idBaremo) {
        this.dateBaremo = dateBaremo;
        this.idTreatment = idTreatment;
        this.nameTreatment = nameTreatment;
        this.baremo = baremo;
        this.idBaremo=idBaremo;
    }

    public Date getDateBaremo() {
        return dateBaremo;
    }

    public void setDateBaremo(Date dateBaremo) {
        this.dateBaremo = dateBaremo;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Double getBaremo() {
        return baremo;
    }

    public void setBaremo(Double baremo) {
        this.baremo = baremo;
    }

   
    public Integer getIdBaremo() {
        return idBaremo;
    }

    public void setIdBaremo(Integer idBaremo) {
        this.idBaremo = idBaremo;
    }

    public String getDateBaremoString() {
        return dateBaremoString;
    }

    public void setDateBaremoString(String dateBaremoString) {
        
        this.dateBaremoString = dateBaremoString;
    }

    public Date getDateUtil() {
        return dateUtil;
    }

    public void setDateUtil(Date dateUtil) {
        this.dateUtil = dateUtil;
    }

    
}
