/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade.store_procedure;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.AuditReportInput;
import com.venedental.dto.AuditReportOutput;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

/**
 *
 * @author ARKDEV16
 */
public class AuditDAO
        extends AbstractQueryDAO<AuditReportInput, AuditReportOutput>{
    
        public AuditDAO(DataSource dataSource) {
        super(dataSource, "SP_REPORTE_AUDITORIA", 10);
    }


    @Override
    public void prepareInput(AuditReportInput input, CallableStatement statement) throws SQLException {
            if(input.getStartDate()==null){
                statement.setNull(1, java.sql.Types.NULL);
             }
             else{
                statement.setDate(1, input.getStartDate());
             }
            if(input.getEndDate()==null){
                statement.setNull(2, java.sql.Types.NULL);
             }
             else{
                statement.setDate(2, input.getEndDate());
             }
             if(input.getIdInsurance() == null || input.getIdInsurance()==0){
                statement.setNull(3, java.sql.Types.NULL);
             }
             else{
                statement.setInt(3, input.getIdInsurance());
             }
             
             if(input.getHealthArea() == null || input.getHealthArea()==0){
                statement.setNull(4, java.sql.Types.NULL);
             }
             else{
                statement.setInt(4, input.getHealthArea());
             }                    
             if(input.getIdPlan() == null || input.getIdPlan()==0){
                statement.setNull(5, java.sql.Types.NULL);
             }
             else{
                statement.setInt(5, input.getIdPlan());
             }   
             
             if(input.getIdTreatment() == null || input.getIdTreatment()==0){
                statement.setNull(6, java.sql.Types.NULL);
             }
             else{
                statement.setInt(6, input.getIdTreatment());
             }
             
             if(input.getIdPieza() == null || input.getIdPieza()==0){
                statement.setNull(7, java.sql.Types.NULL);
             }
             else{
                 statement.setInt(7, input.getIdPieza());
             }

            if(input.getIdEntity() == null || input.getIdEntity()==0){
                statement.setNull(8, java.sql.Types.NULL);
             }
             else{
                 statement.setInt(8, input.getIdEntity());
             }
            
            if(input.getIdState() == null || input.getIdState()==0){
                statement.setNull(9, java.sql.Types.NULL);
             }
             else{
                 statement.setInt(9, input.getIdState());
             }
            
            if(input.getIdResult() == null){
                statement.setInt(10, 0);
            }
            else{
                statement.setInt(10,input.getIdResult());
            }
    }

    @Override
    public AuditReportOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
                AuditReportOutput auditReportOutput = new AuditReportOutput();
                auditReportOutput.setApplicationDate(rs.getDate("FECHA"));
                auditReportOutput.setNumber(rs.getString("NUMERO_CLAVE"));
                auditReportOutput.setSpecialistCompleteName(rs.getString("NOMBRE_ESPECIALISTA"));
                auditReportOutput.setSpecialistIdentityNumber(rs.getString("CEDULA_ESPECIALISTA"));
                auditReportOutput.setPatientCompleteName(rs.getString("NOMBRE_PACIENTE"));
                auditReportOutput.setPatientIdentityNumber(rs.getString("CEDULA_PACIENTE"));
                auditReportOutput.setEntity(rs.getString("ENTE"));
                auditReportOutput.setTreatment(rs.getString("TRATAMIENTO"));
                auditReportOutput.setTooth(rs.getString("PIEZA"));
                auditReportOutput.setBaremoPrice(rs.getDouble("BAREMO"));
                auditReportOutput.setStateName(rs.getString("ESTADO"));
                auditReportOutput.setSpecialityName(rs.getString("ESPECIALIDAD_ESPECIALISTA"));
                auditReportOutput.setPlanName(rs.getString("NOMBRE_PLAN"));
                auditReportOutput.setInsuranceName(rs.getString("NOMBRE_ASEGURADORA"));    
                 return auditReportOutput;
    }
}
