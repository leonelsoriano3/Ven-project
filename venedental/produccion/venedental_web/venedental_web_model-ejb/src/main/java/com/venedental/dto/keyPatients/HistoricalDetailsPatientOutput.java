/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;
import java.util.List;

/**
 *
 * @author akdeskdev90
 */
public class HistoricalDetailsPatientOutput {


    Integer idKeyPatient;
    String numberKey;
    Integer idPlanTreatmentKey;
    Integer idTreatment;
    String nameTratment;
    List<String> numberPropertie;
    Date dateApplication;

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTratment() {
        return nameTratment;
    }

    public void setNameTratment(String nameTratment) {
        this.nameTratment = nameTratment;
    }

    public List<String> getNumberPropertie() {
        return numberPropertie;
    }

    public void setNumberPropertie(List<String> numberPropertie) {
        this.numberPropertie = numberPropertie;
    }

    public Date getDateApplication() {
        return dateApplication;
    }

    public void setDateApplication(Date dateApplication) {
        this.dateApplication = dateApplication;
    }

}