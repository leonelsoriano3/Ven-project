/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.propertiesPlanTreatmentKey.FindProPlaTreKeyByPlanTreKeyInput;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindProPlaTreKeyByPlanTreKeyDAO extends AbstractQueryDAO<FindProPlaTreKeyByPlanTreKeyInput, PropertiesPlansTreatmentsKey> {
    
    
       public FindProPlaTreKeyByPlanTreKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_G_003", 2);
    }

    @Override
    public void prepareInput(FindProPlaTreKeyByPlanTreKeyInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdPlanTreatmentKey()); 
        statement.setInt(2, input.getIdStatus()); 
    }

     @Override
    public PropertiesPlansTreatmentsKey prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new  PropertiesPlansTreatmentsKey();
        propertiesPlansTreatmentsKey.setId(rs.getInt("id"));
        propertiesPlansTreatmentsKey.setIdTreatments(rs.getInt("plans_treatments_keys"));
        propertiesPlansTreatmentsKey.setIdPropertiesTreatments(rs.getInt("properties_treatments_id"));
        propertiesPlansTreatmentsKey.setIdStatusProTreatments(rs.getInt("id_estatus"));
        return propertiesPlansTreatmentsKey;
    }
    
}
