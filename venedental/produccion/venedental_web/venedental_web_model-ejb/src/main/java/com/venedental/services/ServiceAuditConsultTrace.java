
package com.venedental.services;

import com.venedental.dao.reportAuditConsultTrace.AuditConsultTraceDAO;
import com.venedental.dao.reportAuditConsultTrace.DateReportStatusDAO;
import com.venedental.dao.reportAuditConsultTrace.ReportPaymentDetailsDAO;
import com.venedental.dao.reportAuditConsultTrace.ReportStatusAnalysisKeysDAO;
import com.venedental.dao.reportAuditConsultTrace.ReportTreatmentDetailsDAO;
import com.venedental.dto.reportAuditConsultTrace.AuditConsultTraceOutput;
import com.venedental.dto.reportAuditConsultTrace.DateReportStatusOuput;
import com.venedental.dto.reportAuditConsultTrace.ReportPaymentDetailsOutput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysInput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysOuput;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmemtDetailsInput;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmentDetailsOutput;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by akdesk10 on 15/07/16.
 */

@Stateless
public class ServiceAuditConsultTrace {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;


    public List<AuditConsultTraceOutput> getAuditConsultTrace(Integer input){

        AuditConsultTraceDAO auditConsultTraceDAO = new AuditConsultTraceDAO(ds);
        auditConsultTraceDAO.execute(input);

        return auditConsultTraceDAO.getResultList();

    }

   public List<ReportTreatmentDetailsOutput> getInfoTreatmentReport(Integer userId, Integer numRepTreat){

       ReportTreatmemtDetailsInput input;
       input = new ReportTreatmemtDetailsInput(userId,numRepTreat);
       ReportTreatmentDetailsDAO reportTreatmentDetailsDAO = new ReportTreatmentDetailsDAO(ds);
       reportTreatmentDetailsDAO.execute(input);
       return reportTreatmentDetailsDAO.getResultList();

   }

    public List<ReportPaymentDetailsOutput> getInfoTreatmentsForPaymentReport(Integer idPaymentReport){

        ReportPaymentDetailsDAO reportPaymentDetailsDAO = new ReportPaymentDetailsDAO(ds);
        reportPaymentDetailsDAO.execute(idPaymentReport);
        return reportPaymentDetailsDAO.getResultList();

    }
    
    public List<ReportStatusAnalysisKeysOuput> getAuditConsultTracebyFilter
        (ReportStatusAnalysisKeysInput input){
            ReportStatusAnalysisKeysDAO reportStatusAnalysisKeysDAO = new ReportStatusAnalysisKeysDAO(ds);
            reportStatusAnalysisKeysDAO.execute(input);
        return reportStatusAnalysisKeysDAO.getResultList();
    }

        
        public DateReportStatusOuput getMinDateReportStatus
        (ReportStatusAnalysisKeysInput input){
            DateReportStatusDAO dateReportStatusDAO = new DateReportStatusDAO(ds);
            dateReportStatusDAO.execute(input);
        return dateReportStatusDAO.getResult();
    }

}