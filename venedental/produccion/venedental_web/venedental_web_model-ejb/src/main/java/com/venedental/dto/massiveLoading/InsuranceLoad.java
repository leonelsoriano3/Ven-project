/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import com.venedental.model.Insurances;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author akdesk01
 */
public class InsuranceLoad {

    private final Insurances insurance;
    private final String folderName;
    private final String folderPath;
    private final String folderProcessedPath;
    private String folderCurrentLoadPath;
    private long currentYear;
    private boolean parcialLoading;
    private List<FileLoad> files;

    public InsuranceLoad(Insurances insurance, String path) {
        this.insurance = insurance;
        this.folderName = insurance.getFolder();
        this.folderPath = path + folderName;
        this.folderProcessedPath = this.folderPath + "/Procesados";
        this.parcialLoading = true;
        this.files = new ArrayList<>();
    }

    public Insurances getInsurance() {
        return insurance;
    }

    public String getFolderName() {
        return folderName;
    }

    public File getFolder() {
        return new File(getFolderPath());
    }

    public String getFolderPath() {
        return folderPath;
    }

    public File getFolderProcessed() {
        return new File(getFolderProcessedPath());
    }

    public String getFolderProcessedPath() {
        return folderProcessedPath;
    }

    public File getFolderCurrentLoad() {
        return new File(getFolderCurrentLoadPath());
    }

    public String getFolderCurrentLoadPath() {
        return folderCurrentLoadPath;
    }

    public long getCurrentYear() {
        return currentYear;
    }

    public void configureFolderCurrentLoadPath(String folderCurrentLoadPath) {
        this.currentYear = Long.parseLong(folderCurrentLoadPath.substring(0, 4));
        this.folderCurrentLoadPath = this.folderProcessedPath + "/" + folderCurrentLoadPath;
    }

    public boolean isParcialLoading() {
        return parcialLoading;
    }

    public void setParcialLoading(boolean parcialLoading) {
        this.parcialLoading = parcialLoading;
    }

    public List<FileLoad> getFiles() {
        return files;
    }

    public void setFiles(List<FileLoad> files) {
        this.files = files;
    }

    public boolean addFileLoad(FileLoad fileLoad) {
        fileLoad.setInsuranceLoad(this);
        return files.add(fileLoad);
    }

}
