/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.sql.Date;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyDetailsStatusPaidInput {

    private Integer specialistId;
    private Integer numberMonth;
    private Date dateReception;

    public ConsultKeyDetailsStatusPaidInput(Integer specialistId,Integer numberMonth,Date dateReception){
        this.specialistId = specialistId;
        this.numberMonth = numberMonth;
        this.dateReception = dateReception;
    }


    public Integer getSpecialistId() {
        return specialistId;
    }

    public void setSpecialistId(Integer specialistId) {
        this.specialistId = specialistId;
    }

    public Integer getNumberMonth() {
        return numberMonth;
    }

    public void setNumberMonth(Integer numberMonth) {
        this.numberMonth = numberMonth;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

}
