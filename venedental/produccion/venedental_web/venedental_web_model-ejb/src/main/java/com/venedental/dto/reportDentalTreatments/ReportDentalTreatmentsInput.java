package com.venedental.dto.reportDentalTreatments;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by luiscarrasco1991@gmail.com on 31/05/16.
 */
public class ReportDentalTreatmentsInput implements Serializable {

    Date startDate;
    Date endDate;
    Integer idUser;
    Integer keyNumber;
    String findParameter;
    Integer isGeneral;

    public ReportDentalTreatmentsInput(Date startDate, Date endDate, Integer idUser, Integer keyNumber,
                                       String findParameter, Integer isGeneral) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.idUser = idUser;
        this.keyNumber = keyNumber;
        this.findParameter = findParameter;
        this.isGeneral = isGeneral;
    }

    public Date getStartDate() {return startDate; }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getFindParameter() {
        return findParameter;
    }

    public void setFindParameter(String findParameter) {
        this.findParameter = findParameter;
    }

    public Integer getKeyNumber() {
        return keyNumber;
    }

    public void setKeyNumber(Integer keyNumber) {
        this.keyNumber = keyNumber;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getIsGeneral() {return isGeneral; }

    public void setIsGeneral(Integer isGeneral) {this.isGeneral = isGeneral; }
}
