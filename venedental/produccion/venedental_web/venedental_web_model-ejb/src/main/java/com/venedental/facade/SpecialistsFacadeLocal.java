/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Specialists;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface SpecialistsFacadeLocal {

    Specialists create(Specialists specialists);
    
    Specialists findByIdentityNumber(int identityNumber);
    
    Specialists findById(Integer id);

    Specialists edit(Specialists specialists);

    void remove(Specialists specialists);

    Specialists find(Object id);
    
    Specialists findByUser (int usersId);
    
     void  updateStatus (Integer id);

    List<Specialists> findAll();

    List<Specialists> findRange(int[] range);

    int count();
    
    List<Specialists> findByStatus(int status);
    
    void deleteSpecialist(Integer id);
    
    List<Specialists> findByIdentityNumberOrName(String filter);
    
    
    List<Specialists> filterByTratmentStatusAndDateRange(Date dateFrom, Date dateUntil);
    
    List<Specialists> findByPaymentReportStatusAndDateRange(Integer idStatus, Date dateFrom, Date dateUntil);
    
}
