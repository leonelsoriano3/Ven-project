/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultNameFileSupportOutput {
    
    private String nameFileSupport;

    public ConsultNameFileSupportOutput() {
    }

    public String getNameFileSupport() {
        return nameFileSupport;
    }

    public void setNameFileSupport(String nameFileSupport) {
        this.nameFileSupport = nameFileSupport;
    }
    
    
    
}
