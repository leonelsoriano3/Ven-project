/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Edgar
 */
@Embeddable
public class RolUsersPK implements Serializable {
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "rol_id")
    private Integer rolId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Integer userId;

    public RolUsersPK() {
    }

    public RolUsersPK(Integer  rolId, Integer  userId) {
        this.rolId = rolId;
        this.userId = userId;
    }

    public int getRolId() {
        return rolId;
    }

    public void setRolId(Integer rolId) {
        this.rolId = rolId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) rolId;
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolUsersPK)) {
            return false;
        }
        RolUsersPK other = (RolUsersPK) object;
        if (this.rolId != other.rolId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.RolUsersPK[ rolId=" + rolId + ", userId=" + userId + " ]";
    }

}
