package com.venedental.dao.employee;


import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.emplooyes.EmployeeDTO;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akdesk10 on 15/06/16.
 */
public class FindEmployeeByIdUserDAO extends AbstractQueryDAO<Integer, EmployeeDTO> {

    public FindEmployeeByIdUserDAO(DataSource dataSource){
        super(dataSource, "SP_T_EMPLOYEE_G_001",1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input);
    }

    @Override
    public EmployeeDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        EmployeeDTO employees = new EmployeeDTO();
        employees.setIdEmployee(rs.getInt("id"));
        employees.setCompleteName(rs.getString("complete_name"));
        employees.setIdentityNumber(rs.getInt("identity_number"));
        return employees;
    }
}
