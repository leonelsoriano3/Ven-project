/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author root
 */
public class ConsultPaymentProcessInput {
    private int idStatus;
    private Date startDate;
    private Date endDate;
    private int idStatus2;

    public ConsultPaymentProcessInput() {
    }

    public ConsultPaymentProcessInput(int idStatus, Date startDate, Date endDate) {
        this.idStatus = idStatus;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ConsultPaymentProcessInput(int idStatus, int idStatus2, Date startDate, Date endDate) {
        this.idStatus = idStatus;
        this.idStatus2 = idStatus2;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getIdStatus2() {
        return idStatus2;
    }

    public void setIdStatus2(int idStatus2) {
        this.idStatus2 = idStatus2;
    }

    
}
