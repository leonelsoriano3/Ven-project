/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.plansPatients.FindPlanPatientByIdPatientDAO;
import com.venedental.dao.plansPatients.FindPlanPatientByIdentityOrNameDAO;
import com.venedental.dto.planPatient.FilterPlanPatientInput;
import com.venedental.dto.planPatient.FilterPlanPatientOutput;
import com.venedental.model.PlansPatients;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServicePlansPatients {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite buscar los planes de una paciente
     *
     * @param idPatient
     * @return
     */
    public List<PlansPatients> findPlanPatientByIdPatient(Integer idPatient) {
        FindPlanPatientByIdPatientDAO FindPlanPatientByIdPatientDAO = new FindPlanPatientByIdPatientDAO(ds);
        FindPlanPatientByIdPatientDAO.execute(idPatient);
        return FindPlanPatientByIdPatientDAO.getResultList();
    }

    /**
     * Servicio que permite buscar un paciente con sus palnes y aseguradora por nombre o cedula
     *
     * @param parameter
     * @param idSpeciality
     * @return
     */
    public List<FilterPlanPatientOutput> findByIdentityOrName(String parameter, Integer idSpeciality, Integer idUser) {
        FindPlanPatientByIdentityOrNameDAO findPlanPatientByIdentityOrNameDAO = new FindPlanPatientByIdentityOrNameDAO(ds);
        FilterPlanPatientInput filterPlanPatientInput = new FilterPlanPatientInput(parameter, idSpeciality, idUser) ;
        findPlanPatientByIdentityOrNameDAO.execute(filterPlanPatientInput);
        return findPlanPatientByIdentityOrNameDAO.getResultList();
    }
    
    /**
     * Servicio que permite buscar un paciente con sus planes y aseguradora por nombre o cedula(se utiliza en RestFull)
     *
     * @param parameter
     * @param idSpeciality
     * @return
     */
    public List <FilterPlanPatientOutput> findByPatient(String parameter, Integer idSpeciality, Integer idUser) {
        FindPlanPatientByIdentityOrNameDAO findPlanPatientByIdentityOrNameDAO = new FindPlanPatientByIdentityOrNameDAO(ds);
        FilterPlanPatientInput filterPlanPatientInput = new FilterPlanPatientInput(parameter, idSpeciality, idUser) ;
        findPlanPatientByIdentityOrNameDAO.execute(filterPlanPatientInput);
        return findPlanPatientByIdentityOrNameDAO.getResultList();
    }
}