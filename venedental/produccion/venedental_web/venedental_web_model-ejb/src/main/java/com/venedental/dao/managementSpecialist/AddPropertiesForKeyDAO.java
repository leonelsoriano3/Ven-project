/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.AddPropertiesForKeyInput;
import com.venedental.dto.managementSpecialist.AddPropertiesForKeyOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class AddPropertiesForKeyDAO extends AbstractQueryDAO<AddPropertiesForKeyInput, AddPropertiesForKeyOutput>{
    
     public AddPropertiesForKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_I_002", 9);
    }
    
     @Override
    public void prepareInput(AddPropertiesForKeyInput input, CallableStatement statement) throws SQLException {
        
         if(input.getIdTreatmentKey()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getIdTreatmentKey()); 
        }
         
         if(input.getIdTreatmentPiece()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setInt(2,input.getIdTreatmentPiece()); 
        }
        
         
         if(input.getDate()==null){
            statement.setNull(3,java.sql.Types.NULL);
        }else{
            statement.setDate(3,input.getDate()); 
        }
          
         if(input.getIdStatus()==null){
            statement.setNull(4,java.sql.Types.NULL);
        }else{
            statement.setInt(4,input.getIdStatus()); 
        }
         
          if(input.getBaremoTreatmentPiece()==null){
            statement.setNull(5,java.sql.Types.NULL);
        }else{
            statement.setInt(5,input.getBaremoTreatmentPiece()); 
        }
          
           if(input.getObservation()==null){
            statement.setNull(6,java.sql.Types.NULL);
        }else{
            statement.setString(6,input.getObservation()); 
        }
           
            if(input.getIdUsuario()==null){
            statement.setNull(7,java.sql.Types.NULL);
        }else{
            statement.setInt(7,input.getIdUsuario()); 
        }
            
             if(input.getDateReception()==null){
            statement.setNull(8,java.sql.Types.NULL);
        }else{
            statement.setDate(8,input.getDateReception()); 
        }
         
             
             if(input.getIdTreatmentKeyDiagnosis()==null){
            statement.setNull(9,java.sql.Types.NULL);
        }else{
            statement.setInt(9,input.getIdTreatmentKeyDiagnosis()); 
        }
             
            
    }

    @Override
    public AddPropertiesForKeyOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
       
       AddPropertiesForKeyOutput addPropertiesForKeyOutput = new AddPropertiesForKeyOutput();
        addPropertiesForKeyOutput.setIdPieceTreatmentDiagnosis(rs.getInt(" id_pieza_tratamiento_clave_diagnostico"));
        addPropertiesForKeyOutput.setIdPieceTreatmentKey(rs.getInt("id_pieza_tratamiento_clave"));
        
        return addPropertiesForKeyOutput;
    }
    
    
    
}
