/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

/**
 *
 * @author root
 */
public class CreatePaymentReportTreatmentsInput {
    
    Integer plansTreatmentsKeysId;
    Integer paymentReportId;
   

    public CreatePaymentReportTreatmentsInput() {
    }

    public CreatePaymentReportTreatmentsInput(Integer plansTreatmentsKeysId,Integer paymentReportId) {

        this.plansTreatmentsKeysId = plansTreatmentsKeysId;
        this.paymentReportId = paymentReportId;
       
    }

    public Integer getPaymentReportId() {
        return paymentReportId;
    }

    public void setPaymentReportId(Integer paymentReportId) {
        this.paymentReportId = paymentReportId;
    }

    public Integer getPlansTreatmentsKeysId() {
        return plansTreatmentsKeysId;
    }

    public void setPlansTreatmentsKeysId(Integer plansTreatmentsKeysId) {
        this.plansTreatmentsKeysId = plansTreatmentsKeysId;
    }

   
    
    
    
    
    

    
    
}
