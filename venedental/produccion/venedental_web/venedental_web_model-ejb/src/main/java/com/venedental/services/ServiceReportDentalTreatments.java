package com.venedental.services;

import com.venedental.dao.managementSpecialist.ConsultFileNameSupportDAO;
import com.venedental.dao.paymentReport.FindSpecialistByIdSpecialistDAO;
import com.venedental.dao.reportDentalTreatments.*;
import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalInfoOutput;
import com.venedental.dto.reportDentalTreatments.ReportDentalTreatmentsOutput;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import com.venedental.dto.reportDentalTreatments.UpdateKeysStatusInput;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by luis.carrasco@arkiteck.biz on 28/06/16.
 */

@Stateless
public class ServiceReportDentalTreatments {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;



    public List<ReportDentalTreatmentsOutput> getReportDentalTreatements(Integer input){

        ReportDentalTreatmentsDAO reportDentalTreatmentsDAO = new ReportDentalTreatmentsDAO(ds);
        reportDentalTreatmentsDAO.execute(input);

        return reportDentalTreatmentsDAO.getResultList();


    }

    public SpecialistOutput getSpecialistToReport(Integer idUSer){

        FindSpecialistByUserIdDAO findSpecialistByUserIdDAO = new FindSpecialistByUserIdDAO(ds);
        findSpecialistByUserIdDAO.execute(idUSer);

        return findSpecialistByUserIdDAO.getResult();

    }

    public SpecialistOutput getSpecialistToReportById(Integer idSpecialist){
        FindSpecialistByIdSpecialistDAO findSpecialistByIdSpecialistDAO = new FindSpecialistByIdSpecialistDAO(ds);
        findSpecialistByIdSpecialistDAO.execute(idSpecialist);
        return findSpecialistByIdSpecialistDAO.getResult();
    }

    public ReportDentalInfoOutput getCorrelativeNumber(Integer userId){

        Integer correlativeNumber;
        CorrelativeNumberDAO correlativeNumberDAO = new CorrelativeNumberDAO(ds);
        correlativeNumberDAO.execute(userId);
        return correlativeNumberDAO.getResult();
    }

    public Boolean updateKeysStatus(Long idKey,Integer idKeyTreatment,Integer idToothTreatment, Integer numReport,Integer idSpecialist){

        UpdateKeysStatusInput updateKeysStatusInput;
        updateKeysStatusInput = new UpdateKeysStatusInput(idKey,idKeyTreatment,idToothTreatment,numReport,idSpecialist);
        UpdateKeysStatusDAO updateKeysStatusDAO = new UpdateKeysStatusDAO(ds);

        try {

            updateKeysStatusDAO.execute(updateKeysStatusInput);
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        }

    }

    public Date getDateFirstTreatmentGenerate(Integer input){

        FindDateByIdUserDAO findDateByIdUserDAO = new FindDateByIdUserDAO(ds);
        findDateByIdUserDAO.execute(input);
        return  findDateByIdUserDAO.getResult();

    }

    public List <ConsultNameFileOutput> ConsultnameFile(Integer idTreatmentKey) throws SQLException {

        ConsultFileNameSupportDAO consultFileNameSupportDAO = new ConsultFileNameSupportDAO(ds);
        consultFileNameSupportDAO.execute(idTreatmentKey);
        return consultFileNameSupportDAO.getResultList();

    }
}
