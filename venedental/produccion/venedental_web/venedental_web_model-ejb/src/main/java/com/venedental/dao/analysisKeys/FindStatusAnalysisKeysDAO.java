/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.StatusAnalysisKeys;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindStatusAnalysisKeysDAO extends AbstractQueryDAO<Integer, StatusAnalysisKeys> {

    public FindStatusAnalysisKeysDAO(DataSource dataSource) {
        super(dataSource, "SP_T_STATANALKEYS_G_001", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) {
        try {
            if (input == 0) {
                statement.setNull(1, java.sql.Types.NULL);
            } else {
                statement.setInt(1, input);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    @Override
    public StatusAnalysisKeys prepareOutput(ResultSet rs, CallableStatement statement) {
        try {
            StatusAnalysisKeys statusAnalysisKeys = new StatusAnalysisKeys();
            statusAnalysisKeys.setId(rs.getInt("ID_ESTATUS"));
            statusAnalysisKeys.setDescription(rs.getString("NOMBRE_ESTATUS"));
            return statusAnalysisKeys;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

}
