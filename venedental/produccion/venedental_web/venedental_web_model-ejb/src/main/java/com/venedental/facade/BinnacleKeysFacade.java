/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnacleKeys;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author AKDESKDEV90
 */
@Stateless
public class BinnacleKeysFacade extends AbstractFacade<BinnacleKeys> implements BinnacleKeysFacadeLocal{
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BinnacleKeysFacade() {
        super(BinnacleKeys.class);
    }
}
