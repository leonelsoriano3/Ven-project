package com.venedental.dao.reportDentalTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportDentalTreatments.ReportDentalTreatmentsOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luiscarrasco.arkiteck.biz on 31/05/16.
 *
 *
 */
public class ReportDentalTreatmentsDAO extends AbstractQueryDAO<Integer,ReportDentalTreatmentsOutput> {

    public ReportDentalTreatmentsDAO(DataSource dataSource) {
        super(dataSource,"SP_T_KEYSPATIENTS_G_012",3);
    }


    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {

        statement.setInt(1,input);
        statement.registerOutParameter(2,java.sql.Types.DATE);
        statement.registerOutParameter(3,java.sql.Types.DATE);
        }



    @Override
    public ReportDentalTreatmentsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ReportDentalTreatmentsOutput reportDentalTreatmentsOutput = new ReportDentalTreatmentsOutput();
        reportDentalTreatmentsOutput.setDateKey(rs.getDate("fecha_clave"));
        reportDentalTreatmentsOutput.setIdPatient(rs.getInt("id_paciente"));
        reportDentalTreatmentsOutput.setNamePatient(rs.getString("nombre_paciente"));
        reportDentalTreatmentsOutput.setIdKey(rs.getInt("id_clave"));
        reportDentalTreatmentsOutput.setNumberKey(rs.getString("numero_clave"));
        reportDentalTreatmentsOutput.setIdKeyTreatment(rs.getInt("id_clave_tratamiento"));
        reportDentalTreatmentsOutput.setNameTreatemnent(rs.getString("nombre_tratamiento"));
        reportDentalTreatmentsOutput.setIdToothTreatment(rs.getInt("id_pieza_tratamiento"));
        reportDentalTreatmentsOutput.setNumTooth(rs.getString("numero_diente"));
        reportDentalTreatmentsOutput.setNumLine(rs.getInt("id_registro"));
        Date dateMin = statement.getDate(2);
        Date dateMax = statement.getDate(3);
        reportDentalTreatmentsOutput.setStartDate(dateMin);
        reportDentalTreatmentsOutput.setEndDate(dateMax);

        return reportDentalTreatmentsOutput;
    }

}
