/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.consultKeys;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class LoadKeyStatesOutput {
    
    private Integer idstatusKey;
    private String nameStatusKeys;
    

    public LoadKeyStatesOutput() {
    }

    public LoadKeyStatesOutput(Integer idstatusKey, String nameStatusKeys) {
        this.idstatusKey = idstatusKey;
        this.nameStatusKeys = nameStatusKeys;
    }
    
    public Integer getIdstatusKey() {
        return idstatusKey;
    }

    public void setIdstatusKey(Integer idstatusKey) {
        this.idstatusKey = idstatusKey;
    }

    public String getNameStatusKeys() {
        return nameStatusKeys;
    }

    public void setNameStatusKeys(String nameStatusKeys) {
        this.nameStatusKeys = nameStatusKeys;
    }

}
