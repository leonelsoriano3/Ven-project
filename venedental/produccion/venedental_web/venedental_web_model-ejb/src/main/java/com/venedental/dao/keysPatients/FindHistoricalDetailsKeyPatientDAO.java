/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.HistoricalDetailsPatientInput;
import com.venedental.dto.keyPatients.HistoricalDetailsPatientOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindHistoricalDetailsKeyPatientDAO  extends AbstractQueryDAO<HistoricalDetailsPatientInput, HistoricalDetailsPatientOutput> {

    public FindHistoricalDetailsKeyPatientDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_009", 4);
    }

    @Override
    public void prepareInput(HistoricalDetailsPatientInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdPatient());
        if (input.getDateFrom() != null) {
            statement.setDate(3, (Date) input.getDateFrom());
        } else {
            statement.setNull(3, java.sql.Types.NULL);
        }
        if (input.getDateTo() != null) {
            statement.setDate(4, (Date) input.getDateTo());
        } else {
            statement.setNull(4, java.sql.Types.NULL);
        }

    }

    @Override
    public HistoricalDetailsPatientOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        HistoricalDetailsPatientOutput historicalDetailsPatientOutput = new HistoricalDetailsPatientOutput();
        historicalDetailsPatientOutput.setIdKeyPatient(rs.getInt("ID_CLAVE"));
        historicalDetailsPatientOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
        historicalDetailsPatientOutput.setDateApplication(rs.getDate("FECHA_CLAVE"));
        historicalDetailsPatientOutput.setIdPlanTreatmentKey(rs.getInt("ID_PLAN_TRATAMIENTO_CLAVE"));
        historicalDetailsPatientOutput.setIdPlanTreatmentKey(rs.getInt("ID_PLAN_TRATAMIENTO_CLAVE"));
        historicalDetailsPatientOutput.setIdTreatment(rs.getInt("ID_TRATAMIENTO"));
        historicalDetailsPatientOutput.setNameTratment(rs.getString("NOMBRE_TRATAMIENTO"));
        historicalDetailsPatientOutput.setNumberPropertie((List<String>) rs.getArray("NOMBRE_PIEZA"));
        return historicalDetailsPatientOutput;
    }

}