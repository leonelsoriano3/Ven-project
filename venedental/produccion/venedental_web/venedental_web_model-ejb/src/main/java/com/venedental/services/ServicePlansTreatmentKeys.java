/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;


import com.venedental.dao.managementSpecialist.ConsultFileNameSupportDAO;
import com.venedental.dao.managementSpecialist.ConsultFileNameSupportRegisterDAO;
import com.venedental.dao.managementSpecialist.InsertFileSupportDAO;
import com.venedental.dao.planTreatmentKey.FindPlanTreatKeyByKeyAndPlanTreatDAO;
import com.venedental.dao.planTreatmentKey.FindPlanTreatKeyByKeyAndStatusDAO;
import com.venedental.dao.planTreatmentKey.FindPlanTreatKeyByReportInvoicesTransferDAO;
import com.venedental.dao.planTreatmentKey.FindPlanTreatKeyWPByKeyAndStatusDAO;
import com.venedental.dao.planTreatmentKey.RegisterPlanTreatKeyDAO;
import com.venedental.dao.planTreatmentKey.UpdatePlanTreatKeyObservationDAO;
import com.venedental.dao.planTreatmentKey.UpdatePlanTreatKeyStatusDAO;
import com.venedental.dao.planTreatmentKey.UpdatePlanTreatKeyStatusScaleDAO;
import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;
import com.venedental.dto.managementSpecialist.InsertFileSupportInput;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByKeyAndPlanTreatInput;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByReportInvoicesTransferInput;
import com.venedental.dto.planTreatmentKey.FindPlanstkStatusSPByKeyInput;
import com.venedental.dto.planTreatmentKey.FindPtkByKeyAndStatusInput;
import com.venedental.dto.planTreatmentKey.PlanTreatmentKeyInput;
import com.venedental.dto.planTreatmentKey.UpdatePlanTreatmentKeyObservationInput;
import com.venedental.dto.planTreatmentKey.UpdatePlanTreatmentKeyStatusInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
@Stateless
public class ServicePlansTreatmentKeys {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite buscar plan treatment key por id clave y id tratamiento
     *
     * @param idKey
     * @param idPlanTreatment
     * @return integer idPlanTreatKey
     */
    public PlansTreatmentsKeys findByKeyAndPlaTreat(Integer idKey, Integer idPlanTreatment) {
        FindPlanTreatKeyByKeyAndPlanTreatDAO findPlanTreatKeyByKeyAndPlanTreatDAO = new FindPlanTreatKeyByKeyAndPlanTreatDAO(ds);
        FindPlanTreatKeyByKeyAndPlanTreatInput input = new FindPlanTreatKeyByKeyAndPlanTreatInput(idKey, idPlanTreatment);
        findPlanTreatKeyByKeyAndPlanTreatDAO.execute(input);
        return findPlanTreatKeyByKeyAndPlanTreatDAO.getResult();
    }


    /**
     * Servicio que permite actualizar el estado de los tratamientos de una clave y su bitacora
     *
     * @param idPlanTreatmentKey
     * @param idStatus
     * @param idUser
     * @param dateReception
     * @throws SQLException
     */
    public void updateStatus(Integer idPlanTreatmentKey, Integer idStatus, Integer idUser, Date dateReception) throws SQLException {
        UpdatePlanTreatKeyStatusDAO updatePlanTreatKeyStatusDAO = new UpdatePlanTreatKeyStatusDAO(ds);
        UpdatePlanTreatmentKeyStatusInput input = new UpdatePlanTreatmentKeyStatusInput(idPlanTreatmentKey,
                idStatus, idUser, dateReception);
        updatePlanTreatKeyStatusDAO.execute(input);
    }

    /**
     * Servicio que permite actualizar el estado de los tratamientos de una clave y su bitacora, actualizando la fecha
     * de ejecuación y el scale
     *
     * @param idPlanTreatmentKey
     * @param idStatus
     * @param idUser
     * @param dateReception
     * @param dateEjecution
     * @param idScale
     * @throws SQLException
     */
    public void updateStatusAndScale(Integer idPlanTreatmentKey, Integer idStatus, Integer idUser,
                                     Date dateReception, Date dateEjecution, Integer idScale) throws SQLException {
        UpdatePlanTreatKeyStatusScaleDAO updatePlanTreatKeyStatusDAO = new UpdatePlanTreatKeyStatusScaleDAO(ds);
        UpdatePlanTreatmentKeyStatusInput input = new UpdatePlanTreatmentKeyStatusInput(idPlanTreatmentKey,
                idStatus, idUser, dateReception, dateEjecution, idScale);
        updatePlanTreatKeyStatusDAO.execute(input);
    }

    /**
     * Servicio que permite actualizar la observación de los tratamientos de una clave y su bitacora
     *
     * @param idPlanTreatmentKey
     * @param observation
     * @throws SQLException
     */
    public void updateObservation(int idPlanTreatmentKey, String observation) throws SQLException {
        UpdatePlanTreatKeyObservationDAO updatePlanTreatmentKeyObservationDAO = new UpdatePlanTreatKeyObservationDAO(ds);
        UpdatePlanTreatmentKeyObservationInput input = new UpdatePlanTreatmentKeyObservationInput(idPlanTreatmentKey, observation);
        updatePlanTreatmentKeyObservationDAO.execute(input);
    }

    /**
     * Servicio que permite buscar los tratamientos de una clave por dos estados
     *
     * @param idKeyPatient
     * @param idStatusOne
     * @param idStatusTwo
     * @return
     */
    public List<PlansTreatmentsKeys> findPlanTreKeyByKeyAndStatus(Integer idKeyPatient, Integer idStatusOne, Integer idStatusTwo) {
        FindPlanTreatKeyByKeyAndStatusDAO findPtkByKeyAndStatusDAO = new FindPlanTreatKeyByKeyAndStatusDAO(ds);
        FindPtkByKeyAndStatusInput input = new FindPtkByKeyAndStatusInput(idKeyPatient, idStatusOne, idStatusTwo);
        findPtkByKeyAndStatusDAO.execute(input);
        return findPtkByKeyAndStatusDAO.getResultList();
    }

    /**
     * Servicio para buscar los tratamientos sin piezas que pertenecen a la clave dado el id de la clave y el id del
     * estado a consultar
     *
     * @param idKeys
     * @param idStatus
     * @param receptionDate
     * @return
     */
    public List<PlansTreatmentsKeys> findPlanTreatKeySPByKeyAndStatus(Integer idKeys, Integer idStatus, Date receptionDate) {
        FindPlanTreatKeyWPByKeyAndStatusDAO findPlanstkStatusSPByKeyDAO = new FindPlanTreatKeyWPByKeyAndStatusDAO(ds);
        FindPlanstkStatusSPByKeyInput input = new FindPlanstkStatusSPByKeyInput(idKeys, idStatus, receptionDate);
        findPlanstkStatusSPByKeyDAO.execute(input);
        return findPlanstkStatusSPByKeyDAO.getResultList();
    }

    /**
     * Servicio para resgistrar tratamientos a una clave
     *
     * @param idKeys
     * @param idPlanTreatment
     * @param dateApplication
     * @param idStatus
     * @param idScaleTreatments
     * @param observation
     * @param idUser
     * @param dateReception
     * @return
     */
    public PlansTreatmentsKeys registrerPlansTreatmentsKeys(Integer idKeys, Integer idPlanTreatment, Date dateApplication, Integer idStatus, Integer idScaleTreatments, String observation, Integer idUser, Date dateReception) {
        RegisterPlanTreatKeyDAO registrerPlanTreatmentKeyDAO = new RegisterPlanTreatKeyDAO(ds);
        PlanTreatmentKeyInput input = new PlanTreatmentKeyInput(idKeys, idPlanTreatment, dateApplication, idStatus, idScaleTreatments, observation, idUser, dateReception);
        registrerPlanTreatmentKeyDAO.execute(input);
        return registrerPlanTreatmentKeyDAO.getResult();
    }

    /**
     * Servicio que permite buscar los tratamientos de una clave por reporte de pago
     *
     * @param idReport
     * @param idStatus
     * @param dateFrom
     * @param dateUntil
     * @return
     */
    public List<PlansTreatmentsKeys> findProPlaTreKeyByReportInvoicesTransfer(Integer idReport, Integer idStatus, Date dateFrom, Date dateUntil) {
        FindPlanTreatKeyByReportInvoicesTransferDAO findPlanTreatKeyByReportInvoicesTransferDAO = new FindPlanTreatKeyByReportInvoicesTransferDAO(ds);
        FindPlanTreatKeyByReportInvoicesTransferInput input = new FindPlanTreatKeyByReportInvoicesTransferInput(idReport, idStatus, dateFrom, dateUntil);
        findPlanTreatKeyByReportInvoicesTransferDAO.execute(input);
        return findPlanTreatKeyByReportInvoicesTransferDAO.getResultList();
    }

    public void InsertFileSupportRegister(Integer idTreatmentKey, String nameFile) throws SQLException {
        InsertFileSupportDAO insertFileSupportDAO = new InsertFileSupportDAO(ds);
        InsertFileSupportInput input = new InsertFileSupportInput(idTreatmentKey,nameFile);
        insertFileSupportDAO.execute(input);
    }

    public String ConsultnameFileRegister(Integer idTreatmentKey) throws SQLException {

        ConsultFileNameSupportRegisterDAO consultFileNameSupportRegisterDAO = new ConsultFileNameSupportRegisterDAO(ds);
        consultFileNameSupportRegisterDAO.execute(idTreatmentKey);
        return consultFileNameSupportRegisterDAO.getResult();

    }
}
