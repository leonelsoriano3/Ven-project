package com.venedental.dao.reportDentalTreatments;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.reportDentalTreatments.UpdateKeysStatusInput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.SQLException;

/**
 * Created by akdesk10 on 14/07/16.
 */
public class UpdateKeysStatusDAO extends AbstractCommandDAO<UpdateKeysStatusInput> {

    public UpdateKeysStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_U_003",5);
    }

    @Override
    public void prepareInput(UpdateKeysStatusInput input, CallableStatement statement) throws SQLException {

        statement.setLong(1,input.getIdKey());
        statement.setInt(2,input.getIdKeyTreatment());
        statement.setInt(3,input.getIdToothTreatment());
        statement.setInt(4,input.getNumReport());
        statement.setInt(5,input.getIdSpecialist());

    }
}
