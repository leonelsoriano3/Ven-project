/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesPlansTreatmentsKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.propertiesPlanTreatmentKey.FindProPlanstkStatusByKeyAndStatusInput;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProPlaTreKeyByKeyAndStatusDAO extends AbstractQueryDAO<FindProPlanstkStatusByKeyAndStatusInput, PropertiesPlansTreatmentsKey> {

    public FindProPlaTreKeyByKeyAndStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_G_002", 3);
    }

    @Override
    public void prepareInput(FindProPlanstkStatusByKeyAndStatusInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKeyPatient());
        statement.setInt(2, input.getIdStatus());
        if (input.getReceptionDate() != null) {
            statement.setDate(3, new Date(input.getReceptionDate().getTime()));
        } else {
            statement.setNull(3, java.sql.Types.NULL);
        }
    }

    @Override
    public PropertiesPlansTreatmentsKey prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey = new PropertiesPlansTreatmentsKey();
        propertiesPlansTreatmentsKey.setId(rs.getInt("id"));
        propertiesPlansTreatmentsKey.setObservation(rs.getString("observation"));
        propertiesPlansTreatmentsKey.setNameProperties(rs.getString("numero_pieza"));
        propertiesPlansTreatmentsKey.setIdTreatments(rs.getInt("id_tratamiento"));
        propertiesPlansTreatmentsKey.setNameTreatments(rs.getString("nombre_tratamiento"));
        propertiesPlansTreatmentsKey.setDateTreatments(rs.getDate("fecha_tratamiento"));
        propertiesPlansTreatmentsKey.setIdStatusProTreatments(rs.getInt("id_estatus"));
        propertiesPlansTreatmentsKey.setNameStatusProTreatments(rs.getString("nombre_estatus"));
        propertiesPlansTreatmentsKey.setHasGenerate(rs.getBoolean("FUE_GENERADO"));
    //    propertiesPlansTreatmentsKey.setPrice(rs.getDouble("PRECIO"));
        return propertiesPlansTreatmentsKey;
    }

}
