/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.typeAttention;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.TypeAttention;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindTypeAttentionDAO extends AbstractQueryDAO<Integer, TypeAttention> {

    public FindTypeAttentionDAO(DataSource dataSource) {
        super(dataSource, "SP_T_TYPEATTENTIO_G_001", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        if (input != null) {
            statement.setInt(1, input);
        } else {
            statement.setNull(1, java.sql.Types.NULL);
        }
    }

    @Override
    public TypeAttention prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        TypeAttention typeAttention = new TypeAttention();
        typeAttention.setId(rs.getInt("ID_TIPO_ATENCION"));
        typeAttention.setName(rs.getString("NOMBRE_TIPO_ATENCION"));
        return typeAttention;
    }

}