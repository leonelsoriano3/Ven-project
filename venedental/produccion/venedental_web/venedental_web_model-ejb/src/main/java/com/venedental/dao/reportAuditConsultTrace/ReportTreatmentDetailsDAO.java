package com.venedental.dao.reportAuditConsultTrace;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmemtDetailsInput;
import com.venedental.dto.reportAuditConsultTrace.ReportTreatmentDetailsOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis.carrasco@arkiteck.biz on 21/07/16.
 */
public class ReportTreatmentDetailsDAO extends AbstractQueryDAO<ReportTreatmemtDetailsInput,ReportTreatmentDetailsOutput> {


    public ReportTreatmentDetailsDAO(DataSource dataSource){
        super(dataSource,"SP_M_ESP_LeerTratamientosReporteTratamiento",2);
    }


    @Override
    public void prepareInput(ReportTreatmemtDetailsInput input, CallableStatement statement) throws SQLException {       
        statement.setInt(1,input.getUserId());
        statement.setInt(2,input.getNumCorrelative());

    }

    @Override
    public ReportTreatmentDetailsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ReportTreatmentDetailsOutput reportTreatmentDetailsOutput = new ReportTreatmentDetailsOutput();

        reportTreatmentDetailsOutput.setIdKey(rs.getInt("id_clave"));
        reportTreatmentDetailsOutput.setNumberKey(rs.getString("numero_clave"));
        reportTreatmentDetailsOutput.setDate(rs.getString("fecha"));
        reportTreatmentDetailsOutput.setIdKeyTreatment(rs.getInt("id_clave_tratamiento"));
        reportTreatmentDetailsOutput.setIdPatient(rs.getInt("id_paciente"));
        reportTreatmentDetailsOutput.setCarId(rs.getString("cedula_paciente"));
        reportTreatmentDetailsOutput.setNamePatient(rs.getString("nombre_paciente"));
        reportTreatmentDetailsOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        reportTreatmentDetailsOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        reportTreatmentDetailsOutput.setIdToothKeyTreatment(rs.getInt("id_pieza_clave_tratamiento"));
        reportTreatmentDetailsOutput.setIdTooth(rs.getInt("id_pieza"));
        reportTreatmentDetailsOutput.setNameTooth(rs.getString("nombre_pieza"));
        reportTreatmentDetailsOutput.setDateReport(rs.getDate("fecha_creacion"));
        

        return  reportTreatmentDetailsOutput;
    }
}
