/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class AddPropertiesForKeyOutput {
    
    private Integer idPieceTreatmentKey;
    private Integer idPieceTreatmentDiagnosis;

    public AddPropertiesForKeyOutput() {
    }

    public Integer getIdPieceTreatmentKey() {
        return idPieceTreatmentKey;
    }

    public void setIdPieceTreatmentKey(Integer idPieceTreatmentKey) {
        this.idPieceTreatmentKey = idPieceTreatmentKey;
    }

    public Integer getIdPieceTreatmentDiagnosis() {
        return idPieceTreatmentDiagnosis;
    }

    public void setIdPieceTreatmentDiagnosis(Integer idPieceTreatmentDiagnosis) {
        this.idPieceTreatmentDiagnosis = idPieceTreatmentDiagnosis;
    }
    
    
    
}
