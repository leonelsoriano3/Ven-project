package com.venedental.facade;

import com.venedental.model.Requirement;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class RequirementFacade extends AbstractFacade<Requirement> implements RequirementFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RequirementFacade() {
        super(Requirement.class);
    }

    @Override
    public List<Requirement> findAllByReimbursement(int reimbursement_id) {
       
        TypedQuery<Requirement> query = em.createNamedQuery("Requirement.findAllByReimbursement", Requirement.class);
        query.setParameter("reimbursementId", reimbursement_id);
        return query.getResultList();
    }

}
