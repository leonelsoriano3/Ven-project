/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.sql.Date;

/**
 *
 * @author akdesk01
 */
public class HistoryBaremoInput {

    Integer id_treatment, id_speciality;
    Date start_date, end_date;


    public HistoryBaremoInput() {

    }

    public HistoryBaremoInput(Integer id_treatment, Integer id_speciality, Date start_date, Date end_date) {
        this.id_treatment=id_treatment;
        this.id_speciality=id_speciality;
        this.start_date=start_date;
        this.end_date=end_date;

    }

    public Integer getId_treatment() {
        return id_treatment;
    }

    public void setId_treatment(Integer id_treatment) {
        this.id_treatment = id_treatment;
    }

    public Integer getId_speciality() {
        return id_speciality;
    }

    public void setId_speciality(Integer id_speciality) {
        this.id_speciality = id_speciality;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }


}
