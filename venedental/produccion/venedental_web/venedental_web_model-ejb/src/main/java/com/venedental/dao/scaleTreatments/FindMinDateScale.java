/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.scaleTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.scaleTreatments.FindScaleRegisterInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindMinDateScale extends AbstractQueryDAO<FindScaleRegisterInput, Date> {

    public FindMinDateScale(DataSource dataSource) {
       super(dataSource, "SP_T_SCALETREATME_G_003", 3);       
    }

     @Override
    public void prepareInput(FindScaleRegisterInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdTreatments());    
        statement.registerOutParameter(3, java.sql.Types.DATE);
    }

    @Override
    public Date prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {        
        return statement.getDate(3);
    }
}
