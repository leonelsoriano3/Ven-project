/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Usuario
 */
@Embeddable
public class PhoneClinic implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "phone")
    private String phone;
    
    public PhoneClinic() {
    }

    public PhoneClinic(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (phone != null ? phone.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PhoneClinic)) {
            return false;
        }
        PhoneClinic other = (PhoneClinic) object;
        return this.phone.equals(other.phone);
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        PhoneClinic obj = null;
        obj = (PhoneClinic) super.clone();
        return obj;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.PhoneClinic[ id=" +this.getPhone()+ " ]";
    }
}
