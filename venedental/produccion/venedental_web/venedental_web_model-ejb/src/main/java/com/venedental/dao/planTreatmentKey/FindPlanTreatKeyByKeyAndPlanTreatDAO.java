/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByKeyAndPlanTreatInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindPlanTreatKeyByKeyAndPlanTreatDAO extends AbstractQueryDAO<FindPlanTreatKeyByKeyAndPlanTreatInput, PlansTreatmentsKeys> {
    
    
    public FindPlanTreatKeyByKeyAndPlanTreatDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_G_005", 2);
    }

    @Override
    public void prepareInput(FindPlanTreatKeyByKeyAndPlanTreatInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKey());
        statement.setInt(2, input.getIdPlanTreatment());      
    }

    @Override
    public PlansTreatmentsKeys prepareOutput(ResultSet rs , CallableStatement statement) throws SQLException {
        PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
        plansTreatmentsKeys.setId(rs.getInt("id"));
        plansTreatmentsKeys.setStatusId(rs.getInt("id_status"));
        plansTreatmentsKeys.setIdTreatments(rs.getInt("id_tratamiento"));
        return plansTreatmentsKeys;
    }
}