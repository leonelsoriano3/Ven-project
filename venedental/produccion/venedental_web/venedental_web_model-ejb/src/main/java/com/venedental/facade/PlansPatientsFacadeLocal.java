/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansPatients;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PlansPatientsFacadeLocal {

    PlansPatients create(PlansPatients patients);

    PlansPatients edit(PlansPatients patients);

    void remove(PlansPatients patients);

    PlansPatients find(Object id);

    List<PlansPatients> findAll();

    List<PlansPatients> findRange(int[] range);
      
    List<PlansPatients> findByPatientsId (Integer id);
    
    List<PlansPatients> findByPatientsIdAndTypeSpecialist (Integer id, Integer idTypeSpecialist);

    List<PlansPatients> findByPatientsAndPlans(Integer patientsId, Integer plansId);
    
    List<PlansPatients> findByIdentityHolderAndPlan(Integer identityHolder, Integer specialistId);
        
    int count();
    
    
}
