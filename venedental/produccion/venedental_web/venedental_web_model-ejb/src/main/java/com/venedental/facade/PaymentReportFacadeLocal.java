/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PaymentReport;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESKDEV90
 */
@Local
public interface PaymentReportFacadeLocal {

    PaymentReport create(PaymentReport paymentReport);

    PaymentReport edit(PaymentReport paymentReport);

    void remove(PaymentReport paymentReport);

    PaymentReport find(Object id);
    
    String generateNumber(Date date);
    
    List<PaymentReport> filterBySpecialistReportStatusAndDateRange(Integer specialistId, Integer statusId, Date dateFrom, Date dateUntil);
    
    boolean isCreatedPaymentReport(Integer specialistId, Integer statusId, Date dateFrom, Date dateUntil, Integer reportId);

}
