/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade.store_procedure;

import com.venedental.model.Insurances;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class CleanStatusFacade
        extends AbstractStoreProcedureFacade<Insurances, Long>
        implements CleanStatusFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CleanStatusFacade() {
        super("SP_LIMPIAR_ESTATUS_PACIENTES");
    }

    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, Integer.class, IN);
    }

    @Override
    protected void prepareInput(StoredProcedureQuery query, Insurances input) {
        query.setParameter(1, input.getId());
    }

    @Override
    protected Long prepareOutput(Object[] result) {
        return (Long) result[0];
    }

}
