/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
*/

package com.venedental.facade;

import com.venedental.model.security.MenuRolOperation;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Anzola
 */
@Local
public interface MenuRolOperationFacadeLocal {

    MenuRolOperation create(MenuRolOperation menuRolOperation);

    MenuRolOperation edit(MenuRolOperation menuRolOperation);

    void remove(MenuRolOperation menuRolOperation);

    MenuRolOperation find(Object id);

    List<MenuRolOperation> findAll();

    List<MenuRolOperation> findRange(int[] range);
    
    List<MenuRolOperation> findByMenuRolId (int menuRolId);

    int count();

}