/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.interfaces;

import java.sql.Statement;

/**
 *
 * @author usuario
 * @param <InputType>
 * @param <StatementType>
 */
public interface ICommandDAO<InputType, StatementType extends Statement> extends IGenericDAO<InputType, StatementType> {

}
