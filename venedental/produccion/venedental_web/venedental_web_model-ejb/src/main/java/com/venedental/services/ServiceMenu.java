package com.venedental.services;

import com.venedental.dao.menu.MenuDAO;
import com.venedental.dto.menu.MenuOut;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by leonelsoriano3@gmail.com on 18/07/16.
 */
@Stateless
public class ServiceMenu {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;


    public List<MenuOut> findMenuByUserId(Integer idUser){

        MenuDAO menuDAO = new MenuDAO(ds);
        menuDAO.execute(idUser);

        return  menuDAO.getResultList();
    }

}
