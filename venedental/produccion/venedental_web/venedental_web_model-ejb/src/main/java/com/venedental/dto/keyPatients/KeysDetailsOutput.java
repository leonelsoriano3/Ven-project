/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailTreatmentOutput;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 *
 * @author akdeskdev90
 */
public class KeysDetailsOutput {

    private Integer id;
    private String number;
    private Date applicationDate;
    private Date expirationDate;
    private Integer idSpecialist;
    private String nameSpecialist;
    private String specialitys;
    private Integer idPatient;
    private String namePatient;
    private Date dateOfBirthPatient;
    private Integer idPlan;
    private String namePlan;
    private Integer idEnte;
    private String ente;
    private Integer idInsurances;
    private String nameInsurances;
    private String typeAttencion;
    private Integer idRelationShip;
    private String nameRelationShip;
    private Integer idMedicalOffice;
    private String nameMedicalOffice;
    private String identityNumberForPatient;
    private String treatmentsForPlan;
    private String dateKey;
    private Integer emailSpecialist;
    private String dateKeyDetail;
    private String dateExpirationDetail;
    private String dateBrithayDetail;
    private String aplicationDateDetails;

    public String getAplicationDateDetails() {
        return aplicationDateDetails;
    }

    public void setAplicationDateDetails(String aplicationDateDetails) {
        this.aplicationDateDetails = aplicationDateDetails;
    }

    private List<ConsultKeysPatientsDetailTreatmentOutput> ListConsultKeysPatientsDetailTreatmentOutput;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ConsultKeysPatientsDetailTreatmentOutput> getListConsultKeysPatientsDetailTreatmentOutput() {
        return ListConsultKeysPatientsDetailTreatmentOutput;
    }

    public void setListConsultKeysPatientsDetailTreatmentOutput(List<ConsultKeysPatientsDetailTreatmentOutput> listConsultKeysPatientsDetailTreatmentOutput) {
        ListConsultKeysPatientsDetailTreatmentOutput = listConsultKeysPatientsDetailTreatmentOutput;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public String getSpecialitys() {
        return specialitys;
    }

    public void setSpecialitys(String specialitys) {
        this.specialitys = specialitys;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Date getDateOfBirthPatient() {
        return dateOfBirthPatient;
    }

    public void setDateOfBirthPatient(Date dateOfBirthPatient) {
        this.dateOfBirthPatient = dateOfBirthPatient;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    }

    public Integer getIdEnte() {
        return idEnte;
    }

    public void setIdEnte(Integer idEnte) {
        this.idEnte = idEnte;
    }

    public String getEnte() {
        return ente;
    }

    public void setEnte(String ente) {
        if(ente.isEmpty()){
            ente="-";
        }else{
            ente=ente;
        }
        this.ente = ente;
    }

    public Integer getIdInsurances() {
        return idInsurances;
    }

    public void setIdInsurances(Integer idInsurances) {
        this.idInsurances = idInsurances;
    }

    public String getNameInsurances() {
        return nameInsurances;
    }

    public void setNameInsurances(String nameInsurances) {
        this.nameInsurances = nameInsurances;
    }

    public String getTypeAttencion() {
        return typeAttencion;
    }

    public void setTypeAttencion(String typeAttencion) {
        this.typeAttencion = typeAttencion;
    }

    public Integer getIdRelationShip() {
        return idRelationShip;
    }

    public void setIdRelationShip(Integer idRelationShip) {
        this.idRelationShip = idRelationShip;
    }

    public String getNameRelationShip() {
        return nameRelationShip;
    }

    public void setNameRelationShip(String nameRelationShip) {
        this.nameRelationShip = nameRelationShip;
    }

    public Integer getIdMedicalOffice() {
        return idMedicalOffice;
    }

    public void setIdMedicalOffice(Integer idMedicalOffice) {
        this.idMedicalOffice = idMedicalOffice;
    }

    public String getNameMedicalOffice() {
        return nameMedicalOffice;
    }

    public void setNameMedicalOffice(String nameMedicalOffice) {
        this.nameMedicalOffice = nameMedicalOffice;
    }

    public String getIdentityNumberForPatient() {
        return identityNumberForPatient;
    }

    public void setIdentityNumberForPatient(String identityNumberForPatient) {
        this.identityNumberForPatient = identityNumberForPatient;
    }

    public String getTreatmentsForPlan() {
        return treatmentsForPlan;
    }

    public void setTreatmentsForPlan(String treatmentsForPlan) {
        this.treatmentsForPlan = treatmentsForPlan;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        String cut = dateKey.substring(0,19);
        dateKey = cut;
        this.dateKey = dateKey;
    }

    public Integer getEmailSpecialist() {
        return emailSpecialist;
    }

    public void setEmailSpecialist(Integer emailSpecialist) {
        this.emailSpecialist = emailSpecialist;
    }

    public String getDateKeyDetail() {
        return dateKeyDetail;
    }

    public void setDateKeyDetail(String dateKeyDetail) {
        this.dateKeyDetail = dateKeyDetail;
    }

    public String getDateExpirationDetail() {
        return dateExpirationDetail;
    }

    public void setDateExpirationDetail(String dateExpirationDetail) {
        this.dateExpirationDetail = dateExpirationDetail;
    }

    public String getDateBrithayDetail() {
        return dateBrithayDetail;
    }

    public void setDateBrithayDetail(String dateBrithayDetail) {
        this.dateBrithayDetail = dateBrithayDetail;
    }



}