/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.Scale;
import com.venedental.model.ScalePropertiesTreatmentsSpecialist;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class ScalePropertiesTreatmentsSpecialitsFacade extends AbstractFacade<ScalePropertiesTreatmentsSpecialist> implements ScalePropertiesTreatmentsSpecialistFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScalePropertiesTreatmentsSpecialitsFacade() {
        super(ScalePropertiesTreatmentsSpecialist.class);
    }
    
}
