/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnaclePropertiesTreatmentsKeys;
import java.util.List;
import javax.ejb.Local;
/**
 *
 * @author Usuario
 */
@Local
public interface BinnaclePropertiesTreatmentsKeysFacadeLocal {

    BinnaclePropertiesTreatmentsKeys create(BinnaclePropertiesTreatmentsKeys addresses);

    BinnaclePropertiesTreatmentsKeys edit(BinnaclePropertiesTreatmentsKeys addresses);

    void remove(BinnaclePropertiesTreatmentsKeys addresses);

    BinnaclePropertiesTreatmentsKeys find(Object id);

    List<BinnaclePropertiesTreatmentsKeys> findAll();

    List<BinnaclePropertiesTreatmentsKeys> findRange(int[] range);

    int count();
    
}
