/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.propertiesPlanTreatmentKey;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class UpdatePropertiesPlanTreatmentKeyStatusInput {
    
    Integer idPropertiesPlanTreatmentKey;
    Integer idStatus;
    Integer idUser;
    Date dateReception;

    public UpdatePropertiesPlanTreatmentKeyStatusInput(Integer idPropertiesPlanTreatmentKey, Integer idStatus, Integer idUser, Date dateReception) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
        this.idStatus = idStatus;
        this.idUser = idUser;
        this.dateReception = dateReception;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Integer getIdPropertiesPlanTreatmentKey() {
        return idPropertiesPlanTreatmentKey;
    }

    public void setIdPropertiesPlanTreatmentKey(Integer idPropertiesPlanTreatmentKey) {
        this.idPropertiesPlanTreatmentKey = idPropertiesPlanTreatmentKey;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }    

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
}
