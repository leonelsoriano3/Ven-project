/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Treatments;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface TreatmentsFacadeLocal {

    Treatments create(Treatments treatments);
    
    List<Treatments> findAll(); 
    
    List<Treatments> findById(Integer id);
    
    List<Treatments> findByName(Integer id);
    
  List<Treatments> findByTypeSpecialistanSpecialialist(Integer idSpecialist,Integer idTypeSpecialist);
    
}
