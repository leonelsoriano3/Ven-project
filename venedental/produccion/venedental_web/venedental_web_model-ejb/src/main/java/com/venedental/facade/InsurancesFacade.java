/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Clinics;
import com.venedental.model.Insurances;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public  class InsurancesFacade extends AbstractFacade<Insurances> implements InsurancesFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InsurancesFacade() {
        super(Insurances.class);
    }

    @Override
    public List<Insurances> findById(Integer id) {
        TypedQuery<Insurances> query = em.createNamedQuery("Insurances.findById", Insurances.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Override
    public List<Insurances> findByName(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<Insurances> findAllOrderByName() {
        TypedQuery<Insurances> query = em.createNamedQuery("Insurances.findAll", Insurances.class);
        return query.getResultList();
    }


}