/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.scaleTreatments;

import com.venedental.dao.AbstractCommandDAO;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class RegisterScalePropertieTreatment extends AbstractCommandDAO<Integer> {

    public RegisterScalePropertieTreatment(DataSource dataSource) {
        super(dataSource, "SP_M_CLA_ActualizarBaremosPiezas", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        
    }
    
}
