/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.planTreatment.FindPlanTreatByKeyAndPlanDAO;
import com.venedental.dao.planTreatment.FindTreatmentsAvailableEditDAO;
import com.venedental.dao.planTreatment.FindTreatmentsAvailableRegisterDAO;
import com.venedental.dto.planTreatments.TreatmentsAvailableInput;
import com.venedental.dto.planTreatments.TreatmentsAvailableOutput;
import com.venedental.dto.planTreatments.FindPtByKeyAndPlanInput;
import com.venedental.model.PlansTreatments;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServicePlansTreatments {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite buscar los tratamientos disponibles para ser registrados a la clave
     *
     * @param idPlan
     * @param idKeyPatient
     * @return
     */
    public List<PlansTreatments> findPlanTreaByKeyAndPlan(Integer idPlan, Integer idKeyPatient) {
        FindPlanTreatByKeyAndPlanDAO findPtByKeyAndPlanDAO = new FindPlanTreatByKeyAndPlanDAO(ds);
        FindPtByKeyAndPlanInput input = new FindPtByKeyAndPlanInput(idPlan,idKeyPatient);
        findPtByKeyAndPlanDAO.execute(input);
        return findPtByKeyAndPlanDAO.getResultList();
    }

    /**
     * Servicio que permite buscar los tratamientos dispnibles para un paciente al crear clave
     *
     * @param idPatient
     * @param idPlan
     * @param idSpecialist
     * @param typeAttention
     * @return
     */
    public List<TreatmentsAvailableOutput> findTreatmentsAvailableRegister(Integer idPatient, Integer idPlan,
                                                                           Integer idSpecialist, Integer typeAttention) {
        FindTreatmentsAvailableRegisterDAO findTreatmentsAvailableByPatientDAO = new FindTreatmentsAvailableRegisterDAO(ds);
        TreatmentsAvailableInput input = new TreatmentsAvailableInput(idPatient, idPlan, idSpecialist, typeAttention);
        findTreatmentsAvailableByPatientDAO.execute(input);
        return findTreatmentsAvailableByPatientDAO.getResultList();
    }

    /**
     * Servicio que permite buscar los tratamientos disponibles por clave en la edición
     *
     * @param idKeyPatient
     * @param idPlan
     * @return
     */
    public List<TreatmentsAvailableOutput> findTreatmentsAvailableEdit(Integer idKeyPatient, Integer idPlan) {
        FindTreatmentsAvailableEditDAO findTreatmentsAvailableByKeyPatientDAO = new FindTreatmentsAvailableEditDAO(ds);
        FindPtByKeyAndPlanInput input = new FindPtByKeyAndPlanInput(idKeyPatient, idPlan);
        findTreatmentsAvailableByKeyPatientDAO.execute(input);
        return findTreatmentsAvailableByKeyPatientDAO.getResultList();
    }

}