/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;
import java.sql.Date;



/**
 *
 * @author ARKDEV16
 */
public class AuditReportInput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Date startDate, endDate;
    
    private Integer idTreatment;
    
    private Integer idStatus;
    
    private Integer idState;
    
    private Integer idEntity;
    
    private Integer healthArea;
    
    private Integer typeBaremo;
    
    private Integer idPlan;
    
    private Integer idPieza;
    
    private Integer idInsurance;
    
    private Integer idResult;

    public Integer getIdResult() {
        return idResult;
    }

    public void setIdResult(Integer idResult) {
        this.idResult = idResult;
    }
    
    
    public Date getStartDate() {
        return startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public Date getEndDate() {
        return endDate;
    }
    
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public Integer getIdTreatment() {
        return idTreatment;
    }
    
    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }
    
    public Integer getIdStatus() {
        return idStatus;
    }
    
    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }
    
    public Integer getHealthArea() {
        return healthArea;
    }
    
    public void setHealthArea(Integer healthArea) {
        this.healthArea = healthArea;
    }
    
    public Integer getTypeBaremo() {
        return typeBaremo;
    }
    
    public void setTypeBaremo(Integer typeBaremo) {
        this.typeBaremo = typeBaremo;
    }
    
    public Integer getIdPlan() {
        return idPlan;
    }
    
    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }
    
    public Integer getIdPieza() {
        return idPieza;
    }
    
    public void setIdPieza(Integer idPieza) {
        this.idPieza = idPieza;
    }
    
    public Integer getIdInsurance() {
        return idInsurance;
    }
    
    public void setIdInsurance(Integer idInsurance) {
        this.idInsurance = idInsurance;
    }

    public Integer getIdState() {
        return idState;
    }

    public void setIdState(Integer idState) {
        this.idState = idState;
    }

    public Integer getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(Integer idEntity) {
        this.idEntity = idEntity;
    }

   
}
