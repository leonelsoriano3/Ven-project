/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.propertiesTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.properties.FindProTreByTreatmentsInput;
import com.venedental.model.PropertiesTreatments;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProTreByTreatmentsDAO extends AbstractQueryDAO<FindProTreByTreatmentsInput, PropertiesTreatments> {

    public FindProTreByTreatmentsDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_003", 4);
    }

    @Override
    public void prepareInput(FindProTreByTreatmentsInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdTreatments());
        if (input.getTypePropertie() != null) {
            statement.setInt(2, input.getTypePropertie());
        } else {
            statement.setNull(2, java.sql.Types.NULL);
        }
        if (input.getQuadrant() != null) {
            statement.setInt(3, input.getQuadrant());
        } else {
            statement.setNull(3, java.sql.Types.NULL);
        }
        statement.setInt(4, input.getIdSpecialist());
    }

    @Override
    public PropertiesTreatments prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        PropertiesTreatments propertiesTreatments = new PropertiesTreatments();
        propertiesTreatments.setId(rs.getInt("ID_PIEZA_TRATAMIENTO"));
        propertiesTreatments.setIdPropertie(rs.getInt("ID_PIEZA"));
        propertiesTreatments.setPropertieQuadrant(rs.getInt("CUADRANTE"));
        propertiesTreatments.setPropertieStringValue(rs.getString("NOMBRE_PIEZA"));
        propertiesTreatments.setIdScale(rs.getInt("ID_BAREMO"));
        propertiesTreatments.setPrice(rs.getDouble("PRECIO_BAREMO"));


        return propertiesTreatments;
    }

}