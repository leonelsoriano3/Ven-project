/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class InsertFileSupportInput {
    
    private Integer idTreatmentKey;
    private String nameFileSupport;

    public InsertFileSupportInput(Integer idTreatmentKey, String nameFileSupport) {
        this.idTreatmentKey=idTreatmentKey;
        this.nameFileSupport=nameFileSupport;
    }

    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public String getNameFileSupport() {
        return nameFileSupport;
    }

    public void setNameFileSupport(String nameFileSupport) {
        this.nameFileSupport = nameFileSupport;
    }
    
    
    
    
    
}
