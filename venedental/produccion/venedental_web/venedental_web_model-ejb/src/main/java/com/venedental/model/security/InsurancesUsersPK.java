/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.security;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Edgar
 */
@Embeddable
public class InsurancesUsersPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "insurances_id")
    private Integer insurancesId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "users_id")
    private Integer usersId;

    public InsurancesUsersPK() {
    }

    public InsurancesUsersPK(Integer insurancesId, Integer usersId) {
        this.insurancesId = insurancesId;
        this.usersId = usersId;
    }

    public int getInsurancesId() {
        return insurancesId;
    }

    public void setInsurancesId(Integer insurancesId) {
        this.insurancesId = insurancesId;
    }

    public int getUsersId() {
        return usersId;
    }

    public void setUsersId(int usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) insurancesId;
        hash += (int) usersId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsurancesUsersPK)) {
            return false;
        }
        InsurancesUsersPK other = (InsurancesUsersPK) object;
        if (this.insurancesId != other.insurancesId) {
            return false;
        }
        if (this.usersId != other.usersId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.InsurancesUsersPK[ insurancesId=" + insurancesId + ", usersId=" + usersId + " ]";
    }

}
