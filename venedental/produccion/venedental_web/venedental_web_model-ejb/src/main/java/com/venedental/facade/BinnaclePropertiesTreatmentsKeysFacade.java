/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnaclePropertiesTreatmentsKeys;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Usuario
 */
@Stateless
public class BinnaclePropertiesTreatmentsKeysFacade extends AbstractFacade<BinnaclePropertiesTreatmentsKeys> implements BinnaclePropertiesTreatmentsKeysFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BinnaclePropertiesTreatmentsKeysFacade() {
        super(BinnaclePropertiesTreatmentsKeys.class);
    }
    
    
}
