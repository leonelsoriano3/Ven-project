/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "treatments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Treatments.findAll", query = "SELECT t FROM Treatments t"),
    @NamedQuery(name = "Treatments.findById", query = "SELECT t FROM Treatments t WHERE t.id = :id"),
    @NamedQuery(name = "Treatments.findByName", query = "SELECT t FROM Treatments t WHERE t.name = :name"),
    @NamedQuery(name = "Treatments.findByTypeSpecialist", query = "SELECT t FROM Treatments t WHERE t.idTypeSpeciality = :idTypeSpeciality")})
public class Treatments implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    @JoinColumn(name = "id_type_speciality", referencedColumnName = "id")
    @ManyToOne
    private TypeSpecialist idTypeSpeciality;

    @ManyToMany(mappedBy = "treatmentsList")
    private List<Plans> plansList;

    @ManyToMany(mappedBy = "treatmentsList")
    private List<TypeAttention> typeAttentionList;

    @OneToMany(mappedBy = "treatmentsId", cascade = CascadeType.ALL)
    private List<PropertiesTreatments> propertiesTreatmentsList;
    
    @OneToMany(mappedBy = "idTreatments")
    private List<ScaleTreatments> scaleTreatmentsList;
    
    @Transient
    private List<ScaleTreatments> scaleTreatmentsListListFilter;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTreatment")
    private List<RuleTreatments> ruleTreatmentsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "treatmentsId")
    private List<PlansTreatments> plansTreatmentsList;

    @Transient
    private Double price;
    
   

     @Transient
    private Double priceNew;
    
    @Transient
    private String priceString;

    public Treatments() {
        this.scaleTreatmentsListListFilter = new ArrayList<>();
    }

    public Treatments(Integer id) {
        this.id = id;
    }

    public Treatments(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Plans> getPlansList() {
        return plansList;
    }

    public void setPlansList(List<Plans> plansList) {
        this.plansList = plansList;
    }

    public TypeSpecialist getIdTypeSpeciality() {
        return idTypeSpeciality;
    }

    public void setIdTypeSpeciality(TypeSpecialist idTypeSpeciality) {
        this.idTypeSpeciality = idTypeSpeciality;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsList() {
        return propertiesTreatmentsList;
    }

    public void setPropertiesTreatmentsList(List<PropertiesTreatments> propertiesTreatmentsList) {
        this.propertiesTreatmentsList = propertiesTreatmentsList;
    }

    public List<TypeAttention> getTypeAttentionList() {
        return typeAttentionList;
    }

    public void setTypeAttentionList(List<TypeAttention> typeAttentionList) {
        this.typeAttentionList = typeAttentionList;
    }
    
    @XmlTransient
    public List<ScaleTreatments> getScaleTreatmentsList() {
        return scaleTreatmentsList;
    }

    public void setScaleTreatmentsList(List<ScaleTreatments> scaleTreatmentsList) {
        this.scaleTreatmentsList = scaleTreatmentsList;
    }

    public List<ScaleTreatments> getScaleTreatmentsListListFilter() {
         if (this.scaleTreatmentsListListFilter == null) {
            this.scaleTreatmentsListListFilter = new ArrayList<>();
        }
        return scaleTreatmentsListListFilter;
    }

    public void setScaleTreatmentsListListFilter(List<ScaleTreatments> scaleTreatmentsListListFilter) {
        this.scaleTreatmentsListListFilter = scaleTreatmentsListListFilter;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPriceString() {
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

    public Double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(Double priceNew) {
        this.priceNew = priceNew;
    }
    
    
    
    


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Treatments)) {
            return false;
        }
        Treatments other = (Treatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.Treatments[ id=" + id + " ]";
    }

    @XmlTransient
    public List<RuleTreatments> getRuleTreatmentsList() {
        return ruleTreatmentsList;
}

    public void setRuleTreatmentsList(List<RuleTreatments> ruleTreatmentsList) {
        this.ruleTreatmentsList = ruleTreatmentsList;
    }

    @XmlTransient
    public List<PlansTreatments> getPlansTreatmentsList() {
        return plansTreatmentsList;
    }

    public void setPlansTreatmentsList(List<PlansTreatments> plansTreatmentsList) {
        this.plansTreatmentsList = plansTreatmentsList;
    }

}
