/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultNameFileOutput;
import com.venedental.dto.managementSpecialist.ConsultNameFileSupportOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultFileNameDAO extends AbstractQueryDAO<Integer, ConsultNameFileOutput> {
public ConsultFileNameDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEYSUP_G_001", 1);
    }
  @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public ConsultNameFileOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
       ConsultNameFileOutput consultNameFileOutput = new ConsultNameFileOutput();
        consultNameFileOutput.setNameFile(rs.getString("nombre_archivo_soporte"));
       
        return consultNameFileOutput;
    }

}
