/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.patients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.Patients;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class FindPatientByIdDAO  extends AbstractQueryDAO<Integer, Patients> {

    public FindPatientByIdDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PATIENTS_G_001", 1);
    }
    
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public Patients prepareOutput(ResultSet rs, CallableStatement statement) {
        try {
            Patients patient = new Patients();
            patient.setId(rs.getInt("id"));
            patient.setIdentityNumber(rs.getInt("identity_number"));
            patient.setIdentityNumberHolder(rs.getInt("identity_number_holder"));
            patient.setCompleteName(rs.getString("complete_name"));
            patient.setPhone(rs.getString("phone"));
            patient.setCellphone(rs.getString("cellphone"));
            patient.setSex(rs.getString("sex"));
            patient.setCivilStatus(rs.getString("civil_status"));
            patient.setEmail(rs.getString("email"));
            patient.setFirstName(rs.getString("first_name"));
            patient.setSecondName(rs.getString("second_name"));
            patient.setLastname(rs.getString("lastname"));
            patient.setSecondLastname(rs.getString("second_lastname"));
            patient.setIdRelationship(rs.getInt("relationship_id"));
            patient.setInsuredType(rs.getInt("insured_type"));
            patient.setDateOfBirth((rs.getDate("date_of_birth")));
            patient.setStatus(rs.getInt("status"));            
            return patient;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
