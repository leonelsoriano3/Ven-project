package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 * Created by akdeskdev90 on 03/02/16.
 */
public class ChangeStatusKeyGeneratedInput {

    private Integer idKey;
    private Date reciveDate;

    public ChangeStatusKeyGeneratedInput(Integer idKey, Date reciveDate) {
        this.idKey = idKey;
        this.reciveDate = reciveDate;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Date getReciveDate() {
        return reciveDate;
    }

    public void setReciveDate(Date reciveDate) {
        this.reciveDate = reciveDate;
    }
}
