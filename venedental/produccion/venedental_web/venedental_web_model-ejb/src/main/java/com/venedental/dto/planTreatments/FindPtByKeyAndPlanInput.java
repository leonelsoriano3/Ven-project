/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatments;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPtByKeyAndPlanInput {
    private Integer idKeyPatient;
    private Integer idPlan;


    public FindPtByKeyAndPlanInput(Integer idPlan, Integer idKeyPatient) {        
        this.idPlan = idPlan;
        this.idKeyPatient = idKeyPatient;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }
    
    


       
}
