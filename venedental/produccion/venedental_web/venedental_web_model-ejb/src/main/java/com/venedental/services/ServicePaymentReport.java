/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.paymentReport.ConsultPaymentReportByIdDAO;
import com.venedental.dao.paymentReport.ConsultPaymentReportDAO;
import com.venedental.dao.paymentReport.ConsultPaymentReportKeysAnalisysByCriterialDAO;
import com.venedental.dao.paymentReport.ConsultPaymentReportRetentionDAO;
import com.venedental.dao.paymentReport.ConsultRetentionDetailsDAO;
import com.venedental.dao.paymentReport.ConsultTotalBodyRetentionDAO;
import com.venedental.dao.paymentReport.FindValidationNumberControlInvoicesKeysDAO;
import com.venedental.dao.paymentReport.GeneratePaymentReportRetentionDAO;
import com.venedental.dao.paymentReport.UpdatePaymentReportInvoicesDAO;
import com.venedental.dao.paymentReport.UpdatePaymentReportTransferDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsInput;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.paymentReport.ConsultPaymentReportRetentionOutput;
import com.venedental.dto.paymentReport.ConsultRetentionDetailsOutput;
import com.venedental.dto.paymentReport.ConsultTotalBodyRetentionOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import com.venedental.dto.paymentReport.PaymentReportOutput;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialInput;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialOuput;
import com.venedental.dto.paymentReport.UpdatePaymentReportInput;
import java.sql.SQLException;
import java.util.Date;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServicePaymentReport {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite actualizar los datos de la factura del reporte de pago.
     *
     * @param idPaymentReport
     * @param billNumber
     * @param date
     * @param controlNumber
     * @param idUser
     * @throws SQLException
     */
    public void updatePaymentReportInvoices(int idPaymentReport, java.sql.Date date, String billNumber,
            String controlNumber, int idUser) throws SQLException {
        UpdatePaymentReportInvoicesDAO updatePaymentReportDAO = new UpdatePaymentReportInvoicesDAO(ds);
        UpdatePaymentReportInput input = new UpdatePaymentReportInput(idPaymentReport, date, billNumber,
                controlNumber, idUser);
        updatePaymentReportDAO.execute(input);
    }

    /**
     * Servicio que permite actualizar los datos de la transferencia del reporte de pago.
     *
     * @param numberPaymentReport
     * @param date
     * @param transferNumber
     * @param idAccountNumber
     * @throws SQLException
     */
    public void updatePaymentReportTransfer(int numberPaymentReport, java.sql.Date date, String transferNumber,
            int idAccountNumber) throws SQLException {
        UpdatePaymentReportTransferDAO updatePaymentReportTransferDAO = new UpdatePaymentReportTransferDAO(ds);
        UpdatePaymentReportInput input = new UpdatePaymentReportInput(numberPaymentReport, date, transferNumber,
                idAccountNumber);
        updatePaymentReportTransferDAO.execute(input);
    }

    /**
     * Servicio que permite consultar los datos del reporte de pago por id de reporte.
     *
     * @param idPaymentReport
     * @return
     * @throws SQLException
     */
    public PaymentReportOutput consultPaymentReportById(Integer idPaymentReport) throws SQLException {
        ConsultPaymentReportByIdDAO consultPaymentReportByIdDAO = new ConsultPaymentReportByIdDAO(ds);
        consultPaymentReportByIdDAO.execute(idPaymentReport);
        return consultPaymentReportByIdDAO.getResult();
    }

    /**
     * Servicio que generar el reporte de retención del impuesto sobre la renta.
     *
     * @param idSpecialist
     * @param idPaymentReport
     * @return String
     * @throws SQLException
     */
    public String[] generatePaymentReportRetention(Integer idSpecialist, Integer idPaymentReport) throws SQLException {
        GeneratePaymentReportRetentionDAO generatePaymentReportRetentionDAO = new GeneratePaymentReportRetentionDAO(ds);
        PaymentReportInput input = new PaymentReportInput(idSpecialist, idPaymentReport);
        generatePaymentReportRetentionDAO.execute(input);
        return generatePaymentReportRetentionDAO.getResult();
    }

    /**
     * Servicio que permite consultar los datos del reporte de retención del impuesto sobre la renta.
     *
     * @param idSpecialist
     * @param numberRetentionISLR
     * @return
     * @throws SQLException
     */
    public List<ConsultPaymentReportRetentionOutput> consultPaymentReportRetention(int idSpecialist,
            String numberRetentionISLR) throws SQLException {
        ConsultPaymentReportRetentionDAO consultPaymentReportRetentionDAO = new ConsultPaymentReportRetentionDAO(ds);
        PaymentReportInput paymentReportInput = new PaymentReportInput(idSpecialist, numberRetentionISLR);
        consultPaymentReportRetentionDAO.execute(paymentReportInput);
        return consultPaymentReportRetentionDAO.getResultList();
    }

    /**
     * Servicio que permite validar el campo número de control para registrar facturas por estado
     * (PENDIENTE_POR_FACTURAR).
     *
     * @param idSpecialist
     * @return String
     * @throws SQLException
     */
    public Boolean findValidationNumberControlInvoicesKeys(Integer idSpecialist) throws SQLException {
        FindValidationNumberControlInvoicesKeysDAO findValidationNumberControlInvoicesKeysDAO
                = new FindValidationNumberControlInvoicesKeysDAO(ds);
        findValidationNumberControlInvoicesKeysDAO.execute(idSpecialist);
        return findValidationNumberControlInvoicesKeysDAO.getResult();
    }

    /**
     * Servicio que permite consultar el cuerpo del pdf de comprobante de retención.
     *
     * @param idSpecialist
     * @param numberRetentionISLR
     * @return
     * @throws SQLException
     */
    public List<ConsultTotalBodyRetentionOutput> consultTotalBodyRetention(int idSpecialist, String numberRetentionISLR)
            throws SQLException {
        ConsultTotalBodyRetentionDAO consultTotalBodyRetentionDAO = new ConsultTotalBodyRetentionDAO(ds);
        PaymentReportInput paymentReportInput = new PaymentReportInput(idSpecialist, numberRetentionISLR);
        consultTotalBodyRetentionDAO.execute(paymentReportInput);
        return consultTotalBodyRetentionDAO.getResultList();
    }

    /**
     * Servicio que permite consultar los datos de la lista detalle contenida en el pdf de comprobante de atención.
     *
     * @param idSpecialist
     * @param numberRetentionISLR
     * @return
     * @throws SQLException
     */
    public List<ConsultRetentionDetailsOutput> consultRetentionDetails(int idSpecialist, String numberRetentionISLR)
            throws SQLException {
        ConsultRetentionDetailsDAO consultRetentionDetailsDAO = new ConsultRetentionDetailsDAO(ds);
        PaymentReportInput paymentReportInput = new PaymentReportInput(idSpecialist, numberRetentionISLR);
        consultRetentionDetailsDAO.execute(paymentReportInput);
        return consultRetentionDetailsDAO.getResultList();
    }

    /**
     * Servicio que permite consultar los datos para el reporte contenido en un excel para el desarrollo del
     * CUS_013_Reporte_Pago
     *
     * @param idSpecialist
     * @param startDate
     * @param endDate
     * @param numberInvoices
     * @param statusKeysAnalysis
     * @return
     * @throws SQLException
     */
    public List<PaymentReportkeysAnalisysByCriterialOuput>
            consultKeysAnalisysPaymentReportByCriterial(Integer idSpecialist, Date startDate, Date endDate,
                    String numberInvoices, Integer statusKeysAnalysis) throws SQLException {
        ConsultPaymentReportKeysAnalisysByCriterialDAO consultPaymentReport
                = new ConsultPaymentReportKeysAnalisysByCriterialDAO(ds);
        PaymentReportkeysAnalisysByCriterialInput paymentReportkeysAnalisysByCriterialInput
                = new PaymentReportkeysAnalisysByCriterialInput(idSpecialist, startDate, endDate, numberInvoices,
                        statusKeysAnalysis);
        consultPaymentReport.execute(paymentReportkeysAnalisysByCriterialInput);
        return consultPaymentReport.getResultList();
    }

    /**
     * Servicio que permite consultar los tratamientos consultados y rechazados para crear el reporte de pago
     *
     * @param idSpecialist
     * @param month
     * @param dateReception
     * @return ConfirmKeyDetailsOutput
     */
    public List<ConfirmKeyDetailsOutput> consultPaymentReport(Integer idSpecialist,Integer month, Date dateReception ) {
        ConsultPaymentReportDAO consultPaymentReportDAO = new ConsultPaymentReportDAO(ds);
        ConfirmKeyDetailsInput input = new ConfirmKeyDetailsInput();
        input.setIdSpecialist(idSpecialist);
        input.setIdMonth(month);
        input.setDateReception(dateReception);
        consultPaymentReportDAO.execute(input);
        return consultPaymentReportDAO.getResultList();
    }

}
