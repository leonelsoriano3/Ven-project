/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Anthony Torres 
 */
@Entity
@Table(name = "plans_patients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlansPatients.findAll", query = "SELECT p FROM PlansPatients p"),
    @NamedQuery(name = "PlansPatients.findByPlansId", query = "SELECT p FROM PlansPatients p WHERE p.plansPatientsPK.plansId = :plansId"),
    @NamedQuery(name = "PlansPatients.findByPatientsId", query = "SELECT p FROM PlansPatients p WHERE  p.plansPatientsPK.patientsId = :patientsId and p.status = 1"),
    @NamedQuery(name = "PlansPatients.findByPatientsAndPlans", query = "SELECT p FROM PlansPatients p WHERE  p.plansPatientsPK.patientsId = :patientsId and p.plansPatientsPK.plansId = :plansId"),
    @NamedQuery(name = "PlansPatients.findByPatientsIdAndTypeSpecialist", query = "SELECT p FROM PlansPatients p WHERE  p.plansPatientsPK.patientsId = :patientsId and p.status = 1 and p.plansId.typeSpecialistId.id = :typeSpecialistId"),
    @NamedQuery(name = "PlansPatients.findByDueDate", query = "SELECT p FROM PlansPatients p WHERE p.dueDate = :dueDate")})
public class PlansPatients implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected PlansPatientsPK plansPatientsPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @Basic(optional = false)
    @NotNull
    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "effective_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    
    // Asociación bi-direccional many-to-one con Plans
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name = "plans_id", referencedColumnName = "id", nullable=false, insertable = false, updatable = false)
    })
    private Plans plansId;
    
    // Asociación bi-direccional many-to-one con Patients
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name = "patients_id", referencedColumnName = "id", nullable=false, insertable = false, updatable = false)
    })
    private Patients patientsId;
    
    // Asociación bi-direccional many-to-one con Reimbursement
    @OneToMany(cascade = CascadeType.ALL,mappedBy="plansPatientsId")
    private List<Reimbursement> reimbursementList;
    
    @Transient
    private String dueDateString;
    
    @Transient
    private Integer patientIdDto;
    
    @Transient
    private Integer plantIdDto;
    
    @Transient
    private Integer planTypeSpecialistId;
    
    @Transient
    private String planName;

    public PlansPatients() {
    }

    public PlansPatients(PlansPatientsPK plansPatientsPK) {
        this.plansPatientsPK = plansPatientsPK;
    }

    public PlansPatients(PlansPatientsPK plansPatientsPK, Date dueDate) {
        this.plansPatientsPK = plansPatientsPK;
        this.dueDate = dueDate;
    }

    public PlansPatients(Integer plansId, Integer patientsId) {
        this.plansPatientsPK = new PlansPatientsPK(plansId, patientsId);
    }

    public PlansPatientsPK getPlansPatientsPK() {
        return plansPatientsPK;
    }

    public void setPlansPatientsPK(PlansPatientsPK plansPatientsPK) {
        this.plansPatientsPK = plansPatientsPK;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Plans getPlansId() {
        return plansId;
    }

    public void setPlansId(Plans plansId) {
        this.plansId = plansId;
    }

    public Patients getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Patients patientsId) {
        this.patientsId = patientsId;
    }

    public String getDueDateString() {
        return dueDateString;
    }

    public void setDueDateString(String dueDateString) {
        this.dueDateString = dueDateString;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plansPatientsPK != null ? plansPatientsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlansPatients)) {
            return false;
        }
        PlansPatients other = (PlansPatients) object;
        return !((this.plansPatientsPK == null && other.plansPatientsPK != null) || (this.plansPatientsPK != null && !this.plansPatientsPK.equals(other.plansPatientsPK)));
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.PlansPatients[ plansPatientsPK=" + plansPatientsPK + " ]";
    }

    public Integer getPatientIdDto() {
        return patientIdDto;
    }

    public void setPatientIdDto(Integer patientIdDto) {
        this.patientIdDto = patientIdDto;
    }

    public Integer getPlantIdDto() {
        return plantIdDto;
    }

    public void setPlantIdDto(Integer plantIdDto) {
        this.plantIdDto = plantIdDto;
    }

    public Integer getPlanTypeSpecialistId() {
        return planTypeSpecialistId;
    }

    public void setPlanTypeSpecialistId(Integer planTypeSpecialistId) {
        this.planTypeSpecialistId = planTypeSpecialistId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

}
