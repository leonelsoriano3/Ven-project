package com.venedental.dto.managementSpecialist;

import java.util.Date;

/**
 * Created by akdeskdev90 on 15/07/16.
 */
public class FindTreatmentWithoutPieceOut {

    private Integer idTreament;
    private String nameTreatment;
    private Integer idCategory;
    private String nameCategory;
    private Integer idPlanTreatment;
    private Integer idBaremo;
    private Integer treatmentAssigned;
    private Integer idTreatmentKey;
    private Date dateTreatment;
    private boolean isTreatmentAssigned;


    public FindTreatmentWithoutPieceOut(Integer idTreament, String nameTreatment, Integer idCategory, String nameCategory,
                                        Integer idPlanTreatment, Integer idBaremo, Integer treatmentAssigned) {
        this.idTreament = idTreament;
        this.nameTreatment = nameTreatment;
        this.idCategory = idCategory;
        this.nameCategory = nameCategory;
        this.idPlanTreatment = idPlanTreatment;
        this.idBaremo = idBaremo;
        this.treatmentAssigned=treatmentAssigned;
    }

    public FindTreatmentWithoutPieceOut() {
    }

    public Integer getIdTreament() {
        return idTreament;
    }

    public void setIdTreament(Integer idTreament) {
        this.idTreament = idTreament;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdBaremo() {
        return idBaremo;
    }

    public void setIdBaremo(Integer idBaremo) {
        this.idBaremo = idBaremo;
    }

    public Integer getTreatmentAssigned() {
        return treatmentAssigned;
    }

    public void setTreatmentAssigned(Integer treatmentAssigned) {
        this.treatmentAssigned = treatmentAssigned;
    }

    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public Date getDateTreatment() {
        return dateTreatment;
    }

    public void setDateTreatment(Date dateTreatment) {
        this.dateTreatment = dateTreatment;
    }

    public boolean isIsTreatmentAssigned() {
        return isTreatmentAssigned;
    }

    public void setIsTreatmentAssigned(boolean isTreatmentAssigned) {
        this.isTreatmentAssigned = isTreatmentAssigned;
    }
    
    
    
}
