/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.KeysDetailsOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class KeysDetailsDAO extends AbstractQueryDAO<Integer, KeysDetailsOutput> {

    public KeysDetailsDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_008", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public KeysDetailsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        KeysDetailsOutput keysDetailsOutput = new KeysDetailsOutput();
        keysDetailsOutput.setId(rs.getInt("ID_CLAVE"));
        keysDetailsOutput.setNumber(rs.getString("NUMERO_CLAVE"));
        keysDetailsOutput.setApplicationDate(rs.getDate("FECHA_SOLICITUD"));
        keysDetailsOutput.setExpirationDate(rs.getDate("FECHA_VENCIMIENTO"));
        keysDetailsOutput.setIdSpecialist(rs.getInt("ID_ESPECIALISTA"));
        keysDetailsOutput.setNameSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
        keysDetailsOutput.setSpecialitys(rs.getString("ESPECIALIDADES"));
        keysDetailsOutput.setIdPatient(rs.getInt("ID_PACIENTE"));
        keysDetailsOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
        keysDetailsOutput.setDateOfBirthPatient(rs.getDate("FECHA_NACIMIENTO"));
        keysDetailsOutput.setIdPlan(rs.getInt("ID_PLAN"));
        keysDetailsOutput.setNamePlan(rs.getString("NOMBRE_PLAN"));
        keysDetailsOutput.setIdEnte(rs.getInt("ID_ENTE"));
        keysDetailsOutput.setEnte(rs.getString("NOMBRE_ENTE"));
        keysDetailsOutput.setIdInsurances(rs.getInt("ID_ASEGURADORA"));
        keysDetailsOutput.setNameInsurances(rs.getString("NOMBRE_ASEGURADORA"));
        keysDetailsOutput.setIdRelationShip(rs.getInt("ID_PARENTESCO"));
        keysDetailsOutput.setNameRelationShip(rs.getString("NOMBRE_PARENTESCO"));
        keysDetailsOutput.setIdMedicalOffice(rs.getInt("ID_CONSULTORIO"));
        keysDetailsOutput.setNameMedicalOffice(rs.getString("NOMBRE_CONSULTORIO"));
        keysDetailsOutput.setTreatmentsForPlan(rs.getString("TRATAMIENTOS_POR_PLAN"));
        keysDetailsOutput.setIdentityNumberForPatient(rs.getString("CEDULA_PACIENTE"));
        keysDetailsOutput.setDateKey(rs.getString("FECHA_SOLICITUD"));
        return keysDetailsOutput;
    }


}