/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.reportAuditConsultTrace;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAuditConsultTrace.DateReportStatusOuput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysInput;
import com.venedental.dto.reportAuditConsultTrace.ReportStatusAnalysisKeysOuput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

public class DateReportStatusDAO extends AbstractQueryDAO<ReportStatusAnalysisKeysInput, DateReportStatusOuput> {

    public DateReportStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_LEER_FECHA_MINIMA_EJECUCION_TRATAMIENTOS", 3);
    }

    @Override
    public void prepareInput(ReportStatusAnalysisKeysInput input, CallableStatement statement) throws SQLException {
        statement.setDate(1, input.getStartDate());
        statement.setDate(2, input.getEndDate());
        statement.registerOutParameter(3,java.sql.Types.DATE);

    }

    @Override
    public DateReportStatusOuput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        DateReportStatusOuput ouput = new DateReportStatusOuput();

        Date dateMax = statement.getDate(3);
        ouput.setDateMin(dateMax);

        return ouput;

    }

}
