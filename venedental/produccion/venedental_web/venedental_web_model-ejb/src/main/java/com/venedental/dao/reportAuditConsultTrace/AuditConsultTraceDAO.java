package com.venedental.dao.reportAuditConsultTrace;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAuditConsultTrace.AuditConsultTraceOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis.carrasco@arkiteck.biz on 15/07/16.
 */

public class AuditConsultTraceDAO extends AbstractQueryDAO<Integer,AuditConsultTraceOutput> {


    public AuditConsultTraceDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_014",1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {

        statement.setInt(1,input);

    }

    @Override
    public AuditConsultTraceOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        AuditConsultTraceOutput auditConsultTraceOutput = new AuditConsultTraceOutput();

        auditConsultTraceOutput.setNumReportTreatment(rs.getInt("numero_reporte_tratamiento"));
        auditConsultTraceOutput.setIdPaymentReport(rs.getInt("id_reporte_pago"));
        auditConsultTraceOutput.setNumPaymentReport(rs.getString("numero_reporte_pago"));
        auditConsultTraceOutput.setIdKey(rs.getInt("id_clave"));
        auditConsultTraceOutput.setNumKey(rs.getString("numero_clave"));
        auditConsultTraceOutput.setTreatmentName(rs.getString("nombre_tratamiento"));
        auditConsultTraceOutput.setNumTooth(rs.getString("numero_pieza"));
        auditConsultTraceOutput.setStatusName(rs.getString("nombre_estatus"));
        auditConsultTraceOutput.setObservation(rs.getString("observacion"));

        return auditConsultTraceOutput;


    }

}