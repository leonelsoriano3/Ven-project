/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsInput;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;


/**
 *
 * @author usuario
 */
public class GenerateNumberKeyDAO extends AbstractQueryDAO<Integer, String> {

    public GenerateNumberKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_GenerarNumeroClave", 2);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        statement.registerOutParameter(2, java.sql.Types.VARCHAR);
    }

    @Override
    public String prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        return statement.getString(2);
    }

}