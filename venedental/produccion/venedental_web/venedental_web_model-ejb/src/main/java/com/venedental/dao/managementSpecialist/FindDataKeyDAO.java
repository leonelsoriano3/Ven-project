package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.DataKeyDTO;
import com.venedental.dto.managementSpecialist.DataKeyInput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 12/07/16.
 * dato el id de  la clave conseguir si informacion
 */
public class FindDataKeyDAO extends AbstractQueryDAO<DataKeyInput, DataKeyDTO> {


    public FindDataKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_004", 2);
    }

    @Override
    public void prepareInput(DataKeyInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKey());
        statement.setInt(2,input.getNumTooth());
    }

    @Override
    public DataKeyDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        DataKeyDTO dataKeyDTO = new DataKeyDTO();

        dataKeyDTO.setIdKey(rs.getInt("id_clave"));

        dataKeyDTO.setNumberKey(rs.getString("numero_clave"));

        dataKeyDTO.setDataRequest(rs.getDate("fecha_solicitud"));

        dataKeyDTO.setExpirationDate(rs.getDate("fecha_vencimiento"));

        dataKeyDTO.setIdSpecialist(rs.getInt("id_especialista"));

        dataKeyDTO.setSpecialtName(rs.getString("nombre_especialista"));

        dataKeyDTO.setIdPlan(rs.getInt("id_plan"));

        dataKeyDTO.setNamePlan(rs.getString("nombre_plan"));

        dataKeyDTO.setIdSpecialty(rs.getInt("id_especialidad"));

        dataKeyDTO.setNameSpeciality(rs.getString("nombre_especialidad"));

        dataKeyDTO.setIdPatient(rs.getInt("id_pacinete"));

        dataKeyDTO.setNamePAtien(rs.getString("nombre_paciente"));

        dataKeyDTO.setBirthdatePatien(rs.getDate("fecha_nacimiento_pacinete"));

            /*
            * 1. id_plan_tratamiento. Te va a servir al momento de guardar.
    2. id_pieza_tratamiento. Te va a servir al momento de grabar.
    3. id_baremo_tratamiento. Te va a servir al momento de grabar.
    4. id_baremo_pieza. Te va a servir al momento de grabar.
    5. id_tratamiento.
    6. nombre_tratamiento.
    7. id_pieza
    8. valor_pieza
        * */

        return dataKeyDTO;
    }
}
