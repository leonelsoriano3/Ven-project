package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindPieceByidKeyOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 * llamado para dibujar los componentes de los dientes en angularjs
 */
public class FindPieceByidKeyDAO extends AbstractQueryDAO<Integer, FindPieceByidKeyOut> {


    public FindPieceByidKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_008", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public FindPieceByidKeyOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        FindPieceByidKeyOut findPieceByidKeyOut = new FindPieceByidKeyOut();

        findPieceByidKeyOut.setIdPiece(rs.getInt("id_pieza"));
        findPieceByidKeyOut.setNamePiece(rs.getString("nombre_pieza"));
        findPieceByidKeyOut.setQuadrant(rs.getInt("cuadrante"));
        findPieceByidKeyOut.setLine(rs.getInt("linea"));
        findPieceByidKeyOut.setExodoncia(rs.getInt("posee_exodoncia"));
        findPieceByidKeyOut.setTreatments(rs.getInt("posee_tratamientos"));

        return findPieceByidKeyOut;
    }
}
