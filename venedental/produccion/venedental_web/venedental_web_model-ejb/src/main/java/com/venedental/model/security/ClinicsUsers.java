/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.security;

import com.venedental.model.Clinics;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Edgar
 */
@Entity
@Table(name = "clinics_users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClinicsUsers.findAll", query = "SELECT c FROM ClinicsUsers c"),
    @NamedQuery(name = "ClinicsUsers.findByClinicsId", query = "SELECT c FROM ClinicsUsers c WHERE c.clinicsUsersPK.clinicsId = :clinicsId"),
    @NamedQuery(name = "ClinicsUsers.findByUsersId", query = "SELECT c FROM ClinicsUsers c WHERE c.clinicsUsersPK.usersId = :usersId"),
    @NamedQuery(name = "ClinicsUsers.findByStatus", query = "SELECT c FROM ClinicsUsers c WHERE c.status = :status")})
public class ClinicsUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected ClinicsUsersPK clinicsUsersPK;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @JoinColumn(name = "users_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    @JoinColumn(name = "clinics_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Clinics clinics;

    public ClinicsUsers() {
    }

    public ClinicsUsers(ClinicsUsersPK clinicsUsersPK) {
        this.clinicsUsersPK = clinicsUsersPK;
    }

    public ClinicsUsers(ClinicsUsersPK clinicsUsersPK, Integer status) {
        this.clinicsUsersPK = clinicsUsersPK;
        this.status = status;
    }

    public ClinicsUsers(Integer clinicsId, Integer usersId) {
        this.clinicsUsersPK = new ClinicsUsersPK(clinicsId, usersId);
    }

    public ClinicsUsersPK getClinicsUsersPK() {
        return clinicsUsersPK;
    }

    public void setClinicsUsersPK(ClinicsUsersPK clinicsUsersPK) {
        this.clinicsUsersPK = clinicsUsersPK;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Clinics getClinics() {
        return clinics;
    }

    public void setClinics(Clinics clinics) {
        this.clinics = clinics;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clinicsUsersPK != null ? clinicsUsersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClinicsUsers)) {
            return false;
        }
        ClinicsUsers other = (ClinicsUsers) object;
        if ((this.clinicsUsersPK == null && other.clinicsUsersPK != null) || (this.clinicsUsersPK != null && !this.clinicsUsersPK.equals(other.clinicsUsersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.security.ClinicsUsers[ clinicsUsersPK=" + clinicsUsersPK + " ]";
    }

}
