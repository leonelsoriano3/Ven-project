/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ARIANA
 */
@Entity
@Table(name = "plans_treatments_keys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlansTreatmentsKeys.findAll", query = "SELECT p FROM PlansTreatmentsKeys p"),
    @NamedQuery(name = "PlansTreatmentsKeys.findById", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.id = :id"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysId", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByPatientId", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keysId.patientsId.id = :patientId"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByTwoStatusAndSpecialist", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keysId.specialistId.id = :specialistId AND (p.idStatus.id = :idStatus1 OR p.idStatus.id = :idStatus2) ORDER BY p.keysId.number DESC"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByStatusAndSpecialist", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keysId.specialistId.id = :specialistId AND p.idStatus.id = :idStatus ORDER BY p.keysId.number DESC"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByPlansTreatmentsId", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.plansTreatmentsId = :plansTreatmentsId"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysAndStatus", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId AND p.idStatus.id = :idStatus"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysAndExcludeStatus", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId AND p.idStatus.id != :idStatus"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysAndTwoStatus", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId AND (p.idStatus.id = :idStatus1 OR p.idStatus.id = :idStatus2)"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysAndThreeStatus", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId AND (p.idStatus.id = :idStatus1 OR p.idStatus.id = :idStatus2 or p.idStatus.id = :idStatus3)"),
    @NamedQuery(name = "PlansTreatmentsKeys.findByKeysAndPlansTreatment", query = "SELECT p FROM PlansTreatmentsKeys p WHERE p.keys_id = :keysId AND p.plansTreatmentsId.id = :plansTreatmentId")})
public class PlansTreatmentsKeys implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(insertable = false, updatable = false, name = "plans_treatments_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PlansTreatments plansTreatmentsId;

    @JoinColumn(insertable = false, updatable = false, name = "keys_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private KeysPatients keysId;

    @JoinColumn(name = "plans_treatments_id", referencedColumnName = "id")
    private int plans_treatments_id;

    @JoinColumn(name = "keys_id", referencedColumnName = "id")
    private int keys_id;

    @Column(name = "date_plans_treatments_keys")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePlansTreatmentsKeys;

    @Size(max = 255)
    @Column(name = "observation")
    private String observation;

    @JoinColumn(name = "id_scale_treatments", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ScaleTreatments scaleTreatmentsKeys;

    @JoinColumn(insertable = false, updatable = true,name = "id_status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusAnalysisKeys idStatus;

//    @JoinColumn(name = "id_status", referencedColumnName = "id")
//    private Integer id_status;

    @OneToMany(mappedBy = "idPlansTreatmentsKeys")
    private List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList;

    @OneToMany(mappedBy = "plansTreatmentsKeys", cascade = CascadeType.ALL)
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList;

    @ManyToMany(mappedBy = "plansTreatmentsKeysList")
    private List<PaymentReport> paymentReportList;

    /*It is used for payment report*/
    @Transient
    private String scalePrice;

    @Transient
    private boolean hasGenerated;

    @Transient
    private Integer columnsNumberPermanent;

    @Transient
    private Integer columnsNumberTemporary;

    @Transient
    private List<PropertiesTreatments> propertiesPermanent;

    @Transient
    private List<PropertiesTreatments> propertiesTemporary;

    @Transient
    private List<PropertiesTreatments> selectedPropertiesPermanent;

    @Transient
    private List<PropertiesTreatments> selectedPropertiesTemporary;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListString;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringII;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringIII;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringIV;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringV;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringVI;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringVII;

    @Transient
    private List<String> propertiesPlansTreatmentsKeyListStringVIII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNew;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewIII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewIV;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewV;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVI;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVIII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilter;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterIII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterIV;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterV;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVI;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVII;

    @Transient
    private List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVIII;

    @Transient
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyAnalysis;

    @Transient
    private Boolean componentTooth;

    @Transient
    private Boolean disableEdit;

    @Transient
    private Boolean disableConsult;

    @Transient
    private Boolean disableAcceptAnalysis;

    @Transient
    private Boolean disableRefuseAnalysis;

    @Transient
    private Boolean disableEvaluationAnalysis;

    @Transient
    private Boolean disableDeleteAnalysis;

    @Transient
    private Integer statusId;

    @Transient
    private String nameTreatments;

    @Transient
    private Integer idTreatments;

    @Transient
    private String idDescriptionStatusTreatments;

    @Transient
    private Integer idScaleTreatment;

    @Transient
    private Integer idUser;
    
    @Transient
    private Double price;

    public PlansTreatmentsKeys() {
        this.setComponentTooth(false);
        this.setDisableEdit(false);
        this.setDisableConsult(false);
        this.setColumnsNumberPermanent(1);
        this.setColumnsNumberTemporary(1);
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdScaleTreatment() {
        return idScaleTreatment;
    }

    public void setIdScaleTreatment(Integer idScaleTreatment) {
        this.idScaleTreatment = idScaleTreatment;
    }

    public boolean isHasGenerated() {
        return hasGenerated;
    }

    public void setHasGenerated(boolean hasGenerated) {
        this.hasGenerated = hasGenerated;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public StatusAnalysisKeys getidStatus() {
        return idStatus;
    }

    public void setidStatus(StatusAnalysisKeys statusId) {
        this.idStatus = statusId;
    }
    

    public PlansTreatmentsKeys(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PlansTreatments getPlansTreatmentsId() {
        return plansTreatmentsId;
    }

    public void setPlansTreatmentsId(PlansTreatments plansTreatmentsId) {
        this.plansTreatmentsId = plansTreatmentsId;
    }

    public KeysPatients getKeysId() {
        return keysId;
    }

    public void setKeysId(KeysPatients keysId) {
        this.keysId = keysId;
    }

    public int getPlans_treatments_id() {
        return plans_treatments_id;
    }

    public void setPlans_treatments_id(int plans_treatments_id) {
        this.plans_treatments_id = plans_treatments_id;
    }

    public int getKeys_id() {
        return keys_id;
    }

    public void setKeys_id(int keys_id) {
        this.keys_id = keys_id;
    }

    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }    

    public String getIdDescriptionStatusTreatments() {
        return idDescriptionStatusTreatments;
    }

    public void setIdDescriptionStatusTreatments(String idDescriptionStatusTreatments) {
        this.idDescriptionStatusTreatments = idDescriptionStatusTreatments;
    }

    @XmlTransient
    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeyList() {
        if (this.propertiesPlansTreatmentsKeyList == null) {
            this.propertiesPlansTreatmentsKeyList = new ArrayList<>();
        }
        return this.propertiesPlansTreatmentsKeyList;
    }

    public void setPropertiesPlansTreatmentsKeyList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyList) {
        this.propertiesPlansTreatmentsKeyList = propertiesPlansTreatmentsKeyList;
    }

    public Boolean getComponentTooth() {
        return componentTooth;
    }

    public void setComponentTooth(Boolean componentTooth) {
        this.componentTooth = componentTooth;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListString() {
        if (this.propertiesPlansTreatmentsKeyListString == null) {
            this.propertiesPlansTreatmentsKeyListString = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListString;
    }

    public void setPropertiesPlansTreatmentsKeyListString(List<String> propertiesPlansTreatmentsKeyListString) {
        this.propertiesPlansTreatmentsKeyListString = propertiesPlansTreatmentsKeyListString;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringII() {
        if (this.propertiesPlansTreatmentsKeyListStringII == null) {
            this.propertiesPlansTreatmentsKeyListStringII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringII;
    }

    public void setPropertiesPlansTreatmentsKeyListStringII(List<String> propertiesPlansTreatmentsKeyListStringII) {
        this.propertiesPlansTreatmentsKeyListStringII = propertiesPlansTreatmentsKeyListStringII;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringIII() {
        if (this.propertiesPlansTreatmentsKeyListStringIII == null) {
            this.propertiesPlansTreatmentsKeyListStringIII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringIII;
    }

    public void setPropertiesPlansTreatmentsKeyListStringIII(List<String> propertiesPlansTreatmentsKeyListStringIII) {
        this.propertiesPlansTreatmentsKeyListStringIII = propertiesPlansTreatmentsKeyListStringIII;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringIV() {
        if (this.propertiesPlansTreatmentsKeyListStringIV == null) {
            this.propertiesPlansTreatmentsKeyListStringIV = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringIV;
    }

    public void setPropertiesPlansTreatmentsKeyListStringIV(List<String> propertiesPlansTreatmentsKeyListStringIV) {
        this.propertiesPlansTreatmentsKeyListStringIV = propertiesPlansTreatmentsKeyListStringIV;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringV() {
        if (this.propertiesPlansTreatmentsKeyListStringV == null) {
            this.propertiesPlansTreatmentsKeyListStringV = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringV;
    }

    public void setPropertiesPlansTreatmentsKeyListStringV(List<String> propertiesPlansTreatmentsKeyListStringV) {
        this.propertiesPlansTreatmentsKeyListStringV = propertiesPlansTreatmentsKeyListStringV;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringVI() {
        if (this.propertiesPlansTreatmentsKeyListStringVI == null) {
            this.propertiesPlansTreatmentsKeyListStringVI = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringVI;
    }

    public void setPropertiesPlansTreatmentsKeyListStringVI(List<String> propertiesPlansTreatmentsKeyListStringVI) {
        this.propertiesPlansTreatmentsKeyListStringVI = propertiesPlansTreatmentsKeyListStringVI;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringVII() {
        if (this.propertiesPlansTreatmentsKeyListStringVII == null) {
            this.propertiesPlansTreatmentsKeyListStringVII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringVII;
    }

    public void setPropertiesPlansTreatmentsKeyListStringVII(List<String> propertiesPlansTreatmentsKeyListStringVII) {
        this.propertiesPlansTreatmentsKeyListStringVII = propertiesPlansTreatmentsKeyListStringVII;
    }

    public List<String> getPropertiesPlansTreatmentsKeyListStringVIII() {
        if (this.propertiesPlansTreatmentsKeyListStringVIII == null) {
            this.propertiesPlansTreatmentsKeyListStringVIII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListStringVIII;
    }

    public void setPropertiesPlansTreatmentsKeyListStringVIII(List<String> propertiesPlansTreatmentsKeyListStringVIII) {
        this.propertiesPlansTreatmentsKeyListStringVIII = propertiesPlansTreatmentsKeyListStringVIII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNew() {
        if (this.propertiesPlansTreatmentsKeyListNew == null) {
            this.propertiesPlansTreatmentsKeyListNew = new ArrayList<>();
        }
        Collections.sort(propertiesPlansTreatmentsKeyListNew, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListNew;
    }

    public void setPropertiesPlansTreatmentsKeyListNew(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNew) {
        this.propertiesPlansTreatmentsKeyListNew = propertiesPlansTreatmentsKeyListNew;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewII() {
        if (this.propertiesPlansTreatmentsKeyListNewII == null) {
            this.propertiesPlansTreatmentsKeyListNewII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListNewII;
    }

    public void setPropertiesPlansTreatmentsKeyListNewII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewII) {
        this.propertiesPlansTreatmentsKeyListNewII = propertiesPlansTreatmentsKeyListNewII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewIII() {
        if (this.propertiesPlansTreatmentsKeyListNewIII == null) {
            this.propertiesPlansTreatmentsKeyListNewIII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListNewIII;
    }

    public void setPropertiesPlansTreatmentsKeyListNewIII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewIII) {
        this.propertiesPlansTreatmentsKeyListNewIII = propertiesPlansTreatmentsKeyListNewIII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewIV() {
        if (this.propertiesPlansTreatmentsKeyListNewIV == null) {
            this.propertiesPlansTreatmentsKeyListNewIV = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListNewIV;
    }

    public void setPropertiesPlansTreatmentsKeyListNewIV(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewIV) {
        this.propertiesPlansTreatmentsKeyListNewIV = propertiesPlansTreatmentsKeyListNewIV;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewV() {
        if (this.propertiesPlansTreatmentsKeyListNewV == null) {
            this.propertiesPlansTreatmentsKeyListNewV = new ArrayList<>();
        }

        return propertiesPlansTreatmentsKeyListNewV;
    }

    public void setPropertiesPlansTreatmentsKeyListNewV(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewV) {
        this.propertiesPlansTreatmentsKeyListNewV = propertiesPlansTreatmentsKeyListNewV;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewVI() {
        if (this.propertiesPlansTreatmentsKeyListNewVI == null) {
            this.propertiesPlansTreatmentsKeyListNewVI = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListNewVI;
    }

    public void setPropertiesPlansTreatmentsKeyListNewVI(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVI) {
        this.propertiesPlansTreatmentsKeyListNewVI = propertiesPlansTreatmentsKeyListNewVI;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewVII() {
        if (this.propertiesPlansTreatmentsKeyListNewVII == null) {
            this.propertiesPlansTreatmentsKeyListNewVII = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyListNewVII;
    }

    public void setPropertiesPlansTreatmentsKeyListNewVII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVII) {
        this.propertiesPlansTreatmentsKeyListNewVII = propertiesPlansTreatmentsKeyListNewVII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListNewVIII() {
        if (this.propertiesPlansTreatmentsKeyListNewVIII == null) {
            this.propertiesPlansTreatmentsKeyListNewVIII = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListNewVIII, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListNewVIII;
    }

    public void setPropertiesPlansTreatmentsKeyListNewVIII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListNewVIII) {
        this.propertiesPlansTreatmentsKeyListNewVIII = propertiesPlansTreatmentsKeyListNewVIII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilter() {
        if (this.propertiesPlansTreatmentsKeyListFilter == null) {
            this.propertiesPlansTreatmentsKeyListFilter = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilter, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilter;
    }

    public void setPropertiesPlansTreatmentsKeyListFilter(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilter) {
        this.propertiesPlansTreatmentsKeyListFilter = propertiesPlansTreatmentsKeyListFilter;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterII() {
        if (this.propertiesPlansTreatmentsKeyListFilterII == null) {
            this.propertiesPlansTreatmentsKeyListFilterII = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterII, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterII;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterII) {
        this.propertiesPlansTreatmentsKeyListFilterII = propertiesPlansTreatmentsKeyListFilterII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterIII() {
        if (this.propertiesPlansTreatmentsKeyListFilterIII == null) {
            this.propertiesPlansTreatmentsKeyListFilterIII = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterIII, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterIII;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterIII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterIII) {
        this.propertiesPlansTreatmentsKeyListFilterIII = propertiesPlansTreatmentsKeyListFilterIII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterIV() {
        if (this.propertiesPlansTreatmentsKeyListFilterIV == null) {
            this.propertiesPlansTreatmentsKeyListFilterIV = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterIV, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterIV;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterIV(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterIV) {
        this.propertiesPlansTreatmentsKeyListFilterIV = propertiesPlansTreatmentsKeyListFilterIV;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterV() {
        if (this.propertiesPlansTreatmentsKeyListFilterV == null) {
            this.propertiesPlansTreatmentsKeyListFilterV = new ArrayList<>();

        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterV, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterV;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterV(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterV) {
        this.propertiesPlansTreatmentsKeyListFilterV = propertiesPlansTreatmentsKeyListFilterV;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterVI() {
        if (this.propertiesPlansTreatmentsKeyListFilterVI == null) {
            this.propertiesPlansTreatmentsKeyListFilterVI = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterVI, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments2.getPropertiesId().getStringValue().compareTo(propertiesTreatments1.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterVI;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterVI(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVI) {
        this.propertiesPlansTreatmentsKeyListFilterVI = propertiesPlansTreatmentsKeyListFilterVI;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterVII() {
        if (this.propertiesPlansTreatmentsKeyListFilterVII == null) {
            this.propertiesPlansTreatmentsKeyListFilterVII = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterVII, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

            }
        });
        return propertiesPlansTreatmentsKeyListFilterVII;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterVII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVII) {
        this.propertiesPlansTreatmentsKeyListFilterVII = propertiesPlansTreatmentsKeyListFilterVII;
    }

    public List<PropertiesTreatments> getPropertiesPlansTreatmentsKeyListFilterVIII() {
        if (this.propertiesPlansTreatmentsKeyListFilterVIII == null) {
            this.propertiesPlansTreatmentsKeyListFilterVIII = new ArrayList<>();
        }
        Collections.sort(this.propertiesPlansTreatmentsKeyListFilterVIII, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                PropertiesTreatments propertiesTreatments1 = (PropertiesTreatments) o1;
                PropertiesTreatments propertiesTreatments2 = (PropertiesTreatments) o2;
                return propertiesTreatments1.getPropertiesId().getStringValue().compareTo(propertiesTreatments2.getPropertiesId().getStringValue());

            }
        });

        return propertiesPlansTreatmentsKeyListFilterVIII;
    }

    public void setPropertiesPlansTreatmentsKeyListFilterVIII(List<PropertiesTreatments> propertiesPlansTreatmentsKeyListFilterVIII) {
        this.propertiesPlansTreatmentsKeyListFilterVIII = propertiesPlansTreatmentsKeyListFilterVIII;
    }

    public Boolean isDisableConsult() {
        return disableConsult;
    }

    public Boolean getDisableConsult() {
        return disableConsult;
    }

    public void setDisableConsult(Boolean disableConsult) {
        this.disableConsult = disableConsult;
    }

    public Boolean getDisableEdit() {
        return disableEdit;
    }

    public void setDisableEdit(Boolean disableEdit) {
        this.disableEdit = disableEdit;
    }

    public ScaleTreatments getScaleTreatmentsKeys() {
        return scaleTreatmentsKeys;
    }

    public void setScaleTreatmentsKeys(ScaleTreatments scaleTreatmentsKeys) {
        this.scaleTreatmentsKeys = scaleTreatmentsKeys;
    }

//    public int getId_scale_treatments() {
//        return id_scale_treatments;
//    }
//
//    public void setId_scale_treatments(int id_scale_treatments) {
//        this.id_scale_treatments = id_scale_treatments;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlansTreatmentsKeys)) {
            return false;
        }
        PlansTreatmentsKeys other = (PlansTreatmentsKeys) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.venedental.model.PlansTreatmentsKeys[ id=" + id + " ]";
    }

    public Date getDatePlansTreatmentsKeys() {
        return datePlansTreatmentsKeys;
    }

    public void setDatePlansTreatmentsKeys(Date datePlansTreatmentsKeys) {
        this.datePlansTreatmentsKeys = datePlansTreatmentsKeys;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @XmlTransient
    public List<BinnaclePlansTreatmentsKeys> getBinnaclePlansTreatmentsKeysList() {
        return binnaclePlansTreatmentsKeysList;
    }

    public void setBinnaclePlansTreatmentsKeysList(List<BinnaclePlansTreatmentsKeys> binnaclePlansTreatmentsKeysList) {
        this.binnaclePlansTreatmentsKeysList = binnaclePlansTreatmentsKeysList;
    }

    public StatusAnalysisKeys getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(StatusAnalysisKeys idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getColumnsNumberPermanent() {
        return columnsNumberPermanent;
    }

    public void setColumnsNumberPermanent(Integer columnsNumberPermanent) {
        this.columnsNumberPermanent = columnsNumberPermanent;
    }

    public Integer getColumnsNumberTemporary() {
        return columnsNumberTemporary;
    }

    public void setColumnsNumberTemporary(Integer columnsNumberTemporary) {
        this.columnsNumberTemporary = columnsNumberTemporary;
    }

    public List<PropertiesTreatments> getPropertiesPermanent() {
        if (this.propertiesPermanent == null) {
            this.propertiesPermanent = new ArrayList<>();
        }
        return propertiesPermanent;
    }

    public void setPropertiesPermanent(List<PropertiesTreatments> propertiesPermanent) {
        this.propertiesPermanent = propertiesPermanent;
    }

    public List<PropertiesTreatments> getPropertiesTemporary() {
        if (this.propertiesTemporary == null) {
            this.propertiesTemporary = new ArrayList<>();
        }
        return propertiesTemporary;
    }

    public void setPropertiesTemporary(List<PropertiesTreatments> propertiesTemporary) {
        this.propertiesTemporary = propertiesTemporary;
    }

    public List<PropertiesTreatments> getSelectedPropertiesPermanent() {
        if (this.selectedPropertiesPermanent == null) {
            this.selectedPropertiesPermanent = new ArrayList<>();
        }
        return selectedPropertiesPermanent;
    }

    public void setSelectedPropertiesPermanent(List<PropertiesTreatments> selectedPropertiesPermanent) {
        this.selectedPropertiesPermanent = selectedPropertiesPermanent;
    }

    public List<PropertiesTreatments> getSelectedPropertiesTemporary() {
        if (this.selectedPropertiesTemporary == null) {
            this.selectedPropertiesTemporary = new ArrayList<>();
        }
        return selectedPropertiesTemporary;
    }

    public void setSelectedPropertiesTemporary(List<PropertiesTreatments> selectedPropertiesTemporary) {
        this.selectedPropertiesTemporary = selectedPropertiesTemporary;
    }

    @XmlTransient
    public List<PaymentReport> getPaymentReportList() {
        if (this.paymentReportList == null) {
            this.paymentReportList = new ArrayList<>();
        }
        return paymentReportList;
    }

    public void setPaymentReportList(List<PaymentReport> paymentReportList) {
        this.paymentReportList = paymentReportList;
    }

    public String getScalePrice() {
        return scalePrice;
    }

    public void setScalePrice(String scalePrice) {
        this.scalePrice = scalePrice;
    }

    public Boolean getDisableAcceptAnalysis() {
        return disableAcceptAnalysis;
    }

    public void setDisableAcceptAnalysis(Boolean disableAcceptAnalysis) {
        this.disableAcceptAnalysis = disableAcceptAnalysis;
    }

    public Boolean getDisableRefuseAnalysis() {
        return disableRefuseAnalysis;
    }

    public void setDisableRefuseAnalysis(Boolean disableRefuseAnalysis) {
        this.disableRefuseAnalysis = disableRefuseAnalysis;
    }

    public Boolean getDisableEvaluationAnalysis() {
        return disableEvaluationAnalysis;
    }

    public void setDisableEvaluationAnalysis(Boolean disableEvaluationAnalysis) {
        this.disableEvaluationAnalysis = disableEvaluationAnalysis;
    }

    public Boolean getDisableDeleteAnalysis() {
        return disableDeleteAnalysis;
    }

    public void setDisableDeleteAnalysis(Boolean disableDeleteAnalysis) {
        this.disableDeleteAnalysis = disableDeleteAnalysis;
    }
    
    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeyAnalysis() {
        if (this.propertiesPlansTreatmentsKeyList == null) {
            this.propertiesPlansTreatmentsKeyList = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeyAnalysis;
    }

    public void setPropertiesPlansTreatmentsKeyAnalysis(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeyAnalysis) {
        this.propertiesPlansTreatmentsKeyAnalysis = propertiesPlansTreatmentsKeyAnalysis;
    }
    
    
    
}
