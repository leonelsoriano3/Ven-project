package com.venedental.dto.createUser;

/**
 * Created by akdesk10 on 09/06/16.
 */
public class UpdateIdUserByEmployeeInput {

    private Integer id_user;

    private Integer id_employee;


    public UpdateIdUserByEmployeeInput(Integer id_employee,Integer id_user) {
        this.id_user = id_user;
        this.id_employee = id_employee;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {this.id_user = id_user;}

    public Integer getId_employee() {return id_employee;}

    public void setId_employee(Integer id_employee) {this.id_employee = id_employee;}

}
