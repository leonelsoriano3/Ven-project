/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.consultKeys;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultKeysPatientsDetailsDAO extends AbstractQueryDAO<Integer, ConsultKeysPatientsDetailOutput>{
    
     public ConsultKeysPatientsDetailsDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_ConsultarClaveCuerpo", 1);
    }
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
         
    }

    @Override
    public ConsultKeysPatientsDetailOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultKeysPatientsDetailOutput consultKeysPatientsDetailOutput = new ConsultKeysPatientsDetailOutput();
        consultKeysPatientsDetailOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
        consultKeysPatientsDetailOutput.setDateKey(rs.getDate("FECHA_CLAVE"));
        consultKeysPatientsDetailOutput.setExpiratioKey(rs.getDate("FECHA_VENCIMIENTO_CLAVE"));
        consultKeysPatientsDetailOutput.setNameSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
        consultKeysPatientsDetailOutput.setSpecialitySpecialist(rs.getString("ESPECIALIDADES_ESPECIALISTA"));
        consultKeysPatientsDetailOutput.setNamePatient(rs.getString("NOMBRE_PACIENTE"));
        consultKeysPatientsDetailOutput.setBrithayPatient(rs.getDate("FECHA_NACIMIENTO_PACIENTE"));
        consultKeysPatientsDetailOutput.setNamePlan(rs.getString("NOMBRE_PLAN"));
         consultKeysPatientsDetailOutput.setNameEnte(rs.getString("NOMBRE_ENTE"));
         consultKeysPatientsDetailOutput.setNameConsultorio(rs.getString("NOMBRE_CONSULTORIO"));
         consultKeysPatientsDetailOutput.setNameAseguradora(rs.getString("NOMBRE_ASEGURADORA"));
         consultKeysPatientsDetailOutput.setParentesco(rs.getString("NOMBRE_PARENTESCO"));
         consultKeysPatientsDetailOutput.setNumberForIdentityPatient(rs.getString("CEDULA_PACIENTE"));
         return consultKeysPatientsDetailOutput;
    }
    
}
