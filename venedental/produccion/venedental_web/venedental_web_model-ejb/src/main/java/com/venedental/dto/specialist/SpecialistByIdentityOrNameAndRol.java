/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.specialist;

/**
 *
 * @author akdesk01
 */
public class SpecialistByIdentityOrNameAndRol {

    String inputData;
    Integer idUser;

    public SpecialistByIdentityOrNameAndRol( String inputData, Integer idUSer) {
        this.inputData=inputData;
        this.idUser=idUSer;

    }

    public String getInputData() {
        return inputData;
    }

    public void setInputData(String inputData) {
        this.inputData = inputData;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }



}