/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.specialist;

/**
 *
 * @author AKDESKDEV90
 */
public class SpecialistByIdentityOrNameInput {
    
    Integer identityNumber;
    String completeName;

    public SpecialistByIdentityOrNameInput(Integer identityNumber, String completeName) {
        this.identityNumber = identityNumber;
        this.completeName = completeName;
    }

    public Integer getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }
    
}
