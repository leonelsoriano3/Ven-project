/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PropertiesPlansTreatmentsKeyFacade extends AbstractFacade<PropertiesPlansTreatmentsKey> implements  PropertiesPlansTreatmentsKeyFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PropertiesPlansTreatmentsKeyFacade() {
        super(PropertiesPlansTreatmentsKey.class);
    }


    @Override
    public List<PropertiesPlansTreatmentsKey> findByTreatmentsId(Integer id) {
        TypedQuery<PropertiesPlansTreatmentsKey> query = em.createNamedQuery("PropertiesPlansTreatmentsKey.findByTreatmenstId", PropertiesPlansTreatmentsKey.class);  
        query.setParameter("treatmentsId", id);
       return  query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PropertiesPlansTreatmentsKey> findByPlansTreatmentsKeys(Integer id) {
        TypedQuery<PropertiesPlansTreatmentsKey> query = em.createNamedQuery("PropertiesPlansTreatmentsKey.findByPlansTreatmentsKeys", PropertiesPlansTreatmentsKey.class);  
        query.setParameter("plans_treatments_keys", id);
       return  query.getResultList();
    }
    
    @Override
    public List<PropertiesPlansTreatmentsKey> findByKeysAndStatus(Integer id, Integer idStatus) {
        TypedQuery<PropertiesPlansTreatmentsKey> query = em.createNamedQuery("PropertiesPlansTreatmentsKey.findByKeysAndStatus", PropertiesPlansTreatmentsKey.class);  
        query.setParameter("keysId", id);
        query.setParameter("idStatus", idStatus);
       return  query.getResultList();
    }
    
     @Override
    public List<PropertiesPlansTreatmentsKey> findByKeys(Integer id) {
        TypedQuery<PropertiesPlansTreatmentsKey> query = em.createNamedQuery("PropertiesPlansTreatmentsKey.findByKeys", PropertiesPlansTreatmentsKey.class);  
        query.setParameter("keysId", id);
       return  query.getResultList();
    }

    @Override
    public void deleteByPlansTreatmentsKeys(int id) {
   
        TypedQuery<PropertiesPlansTreatmentsKey> query = em.createNamedQuery("PropertiesPlansTreatmentsKey.deleteByPlansTreatmentsKeys", PropertiesPlansTreatmentsKey.class);
        query.setParameter("plans_treatments_keys", id);        
        query.executeUpdate();  
        em.flush();
        em.clear();
    }
    
    @Override
    public String countProperties(Integer idPlansTreatmentsKeys, Integer status){
        
       Query query = em.createNativeQuery("select count(*) from properties_plans_treatments_key where properties_plans_treatments_key.id_status !=" + status + " and properties_plans_treatments_key.plans_treatments_keys =" + idPlansTreatmentsKeys);  
       return  query.getSingleResult().toString();
    }

}
