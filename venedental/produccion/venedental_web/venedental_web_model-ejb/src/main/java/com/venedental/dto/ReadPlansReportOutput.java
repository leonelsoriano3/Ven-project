/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;

/**
 *
 * @author ARKDEV16
 */
public class ReadPlansReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer idPlan;
    
    private String namePlan;

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    } 
}