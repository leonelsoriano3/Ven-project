package com.venedental.dto.analysisKeys;

/**
 * Created by akdeskdev90 on 17/03/16.
 */
public class ConsultPaymentReportOutPut {

    private Integer idReport;
    private String numberReport;

    public ConsultPaymentReportOutPut() {
    }

    public ConsultPaymentReportOutPut(Integer idReport, String numberReport) {
        this.idReport = idReport;
        this.numberReport = numberReport;
    }

    public Integer getIdReport() {
        return idReport;
    }

    public void setIdReport(Integer idReport) {
        this.idReport = idReport;
    }

    public String getNumberReport() {
        return numberReport;
    }

    public void setNumberReport(String numberReport) {
        this.numberReport = numberReport;
    }
}
