/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

import java.sql.Date;

/**
 *
 * @author akdesk01
 */
public class AddPropertiesForKeyInput {
    
     private Integer idTreatmentKey;
    private Integer idTreatmentPiece;
    private Date date;
    private Integer idStatus;
    private Integer baremoTreatmentPiece;
    private String observation;
    private Integer IdUsuario;
    private Date dateReception;
    private Integer idTreatmentKeyDiagnosis;
    private String fileSupport;

    public AddPropertiesForKeyInput(Integer idTreatmentKey, Integer idTreatmentPiece, Date date, Integer idStatus, Integer baremoTreatmentPiece, String observation, Integer IdUsuario, Date dateReception, Integer idTreatmentKeyDiagnosis) {
        this.idTreatmentKey = idTreatmentKey;
        this.idTreatmentPiece = idTreatmentPiece;
        this.date = date;
        this.idStatus = idStatus;
        this.baremoTreatmentPiece = baremoTreatmentPiece;
        this.observation = observation;
        this.IdUsuario = IdUsuario;
        this.dateReception = dateReception;
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }

    

    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public Integer getIdTreatmentPiece() {
        return idTreatmentPiece;
    }

    public void setIdTreatmentPiece(Integer idTreatmentPiece) {
        this.idTreatmentPiece = idTreatmentPiece;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getBaremoTreatmentPiece() {
        return baremoTreatmentPiece;
    }

    public void setBaremoTreatmentPiece(Integer baremoTreatmentPiece) {
        this.baremoTreatmentPiece = baremoTreatmentPiece;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(Integer IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Integer getIdTreatmentKeyDiagnosis() {
        return idTreatmentKeyDiagnosis;
    }

    public void setIdTreatmentKeyDiagnosis(Integer idTreatmentKeyDiagnosis) {
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }

    public String getFileSupport() {
        return fileSupport;
    }

    public void setFileSupport(String fileSupport) {
        this.fileSupport = fileSupport;
    }

    
    
    
}
