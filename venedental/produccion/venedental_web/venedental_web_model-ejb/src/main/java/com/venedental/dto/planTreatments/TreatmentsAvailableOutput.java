/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatments;

import com.venedental.model.PropertiesTreatments;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author akdeskdev90
 */
public class TreatmentsAvailableOutput {

    private Integer idPlanTratment;
    private Integer idTreatment;
    private String descriptionTreatment;
    private Integer idScaleTreatment;
    private Double price;
    private Double priceAcumulate;
    private String priceString;
    private List<PropertiesTreatments> propertiesTreatmentsPermanent;
    private List<PropertiesTreatments> propertiesTreatmentsTemporary;
    private List<PropertiesTreatments> propertiesTreatmentsPermanentSelected;
    private List<PropertiesTreatments> propertiesTreatmentsTemporarySelected;
    private boolean visiblePanelPermanent = false;
    private boolean visiblePanelTemporary = false;
    private Integer numberColumnPermanent = 1;
    private Integer numberColumnTemporary = 1;

    public TreatmentsAvailableOutput() {
        this.setNumberColumnPermanent(1);
        this.setNumberColumnTemporary(1);
    }

    public TreatmentsAvailableOutput(Integer idPlanTratment, Integer idTreatment, String descriptionTreatment, Integer idScaleTreatment, Double price, String priceString) {
        this.idPlanTratment = idPlanTratment;
        this.idTreatment = idTreatment;
        this.descriptionTreatment = descriptionTreatment;
        this.idScaleTreatment = idScaleTreatment;
        this.price = price;
        this.priceString = priceString;
    }

    public Double getPriceAcumulate() {
        return priceAcumulate;
    }

    public void setPriceAcumulate(Double priceAcumulate) {
        this.priceAcumulate = priceAcumulate;
    }

    public Integer getNumberColumnPermanent() {
        return numberColumnPermanent;
    }

    public void setNumberColumnPermanent(Integer numberColumnPermanent) {
        this.numberColumnPermanent = numberColumnPermanent;
    }

    public Integer getNumberColumnTemporary() {
        return numberColumnTemporary;
    }

    public void setNumberColumnTemporary(Integer numberColumnTemporary) {
        this.numberColumnTemporary = numberColumnTemporary;
    }

    public boolean isVisiblePanelPermanent() {
        return visiblePanelPermanent;
    }

    public void setVisiblePanelPermanent(boolean visiblePanelPermanent) {
        this.visiblePanelPermanent = visiblePanelPermanent;
    }

    public boolean isVisiblePanelTemporary() {
        return visiblePanelTemporary;
    }

    public void setVisiblePanelTemporary(boolean visiblePanelTemporary) {
        this.visiblePanelTemporary = visiblePanelTemporary;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsPermanent() {
        if (this.propertiesTreatmentsPermanent == null) {
            this.propertiesTreatmentsPermanent = new ArrayList<>();
        }
        return propertiesTreatmentsPermanent;
    }

    public void setPropertiesTreatmentsPermanent(List<PropertiesTreatments> propertiesTreatmentsPermanent) {
        this.propertiesTreatmentsPermanent = propertiesTreatmentsPermanent;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsTemporary() {
        if (this.propertiesTreatmentsTemporary == null) {
            this.propertiesTreatmentsTemporary = new ArrayList<>();
        }
        return propertiesTreatmentsTemporary;
    }

    public void setPropertiesTreatmentsTemporary(List<PropertiesTreatments> propertiesTreatmentsTemporary) {
        this.propertiesTreatmentsTemporary = propertiesTreatmentsTemporary;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsPermanentSelected() {
        if (this.propertiesTreatmentsPermanentSelected == null) {
            this.propertiesTreatmentsPermanentSelected = new ArrayList<>();
        }
        return propertiesTreatmentsPermanentSelected;
    }

    public void setPropertiesTreatmentsPermanentSelected(List<PropertiesTreatments> propertiesTreatmentsPermanentSelected) {
        this.propertiesTreatmentsPermanentSelected = propertiesTreatmentsPermanentSelected;
    }

    public List<PropertiesTreatments> getPropertiesTreatmentsTemporarySelected() {
        if (this.propertiesTreatmentsTemporarySelected == null) {
            this.propertiesTreatmentsTemporarySelected = new ArrayList<>();
        }
        return propertiesTreatmentsTemporarySelected;
    }

    public void setPropertiesTreatmentsTemporarySelected(List<PropertiesTreatments> propertiesTreatmentsTemporarySelected) {
        this.propertiesTreatmentsTemporarySelected = propertiesTreatmentsTemporarySelected;
    }

    public Integer getIdPlanTratment() {
        return idPlanTratment;
    }

    public void setIdPlanTratment(Integer idPlanTratment) {
        this.idPlanTratment = idPlanTratment;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getDescriptionTreatment() {
        return descriptionTreatment;
    }

    public void setDescriptionTreatment(String descriptionTreatment) {
        this.descriptionTreatment = descriptionTreatment;
    }

    public Integer getIdScaleTreatment() {
        return idScaleTreatment;
    }

    public void setIdScaleTreatment(Integer idScaleTreatment) {
        this.idScaleTreatment = idScaleTreatment;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPriceString() {
        this.priceString=this.priceString.replace(".", ",");
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

}