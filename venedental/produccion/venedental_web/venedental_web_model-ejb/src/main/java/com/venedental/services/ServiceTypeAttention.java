/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.typeAttention.FindTypeAttentionDAO;
import com.venedental.model.TypeAttention;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
@Stateless
public class ServiceTypeAttention {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Buscar todos los tipos de atencion
     *
     * @return
     */
    public List<TypeAttention> findAll() {
        FindTypeAttentionDAO findTypeAttentionDAO = new FindTypeAttentionDAO(ds);
        findTypeAttentionDAO.execute(null);
        return findTypeAttentionDAO.getResultList();
    }

    /**
     * Buscar todos los consultorios de un especialista
     *
     * @param idTypeAttention
     * @return
     */
    public TypeAttention findById(Integer idTypeAttention) {
        FindTypeAttentionDAO findTypeAttentionDAO = new FindTypeAttentionDAO(ds);
        findTypeAttentionDAO.execute(idTypeAttention);
        return findTypeAttentionDAO.getResult();
    }

}