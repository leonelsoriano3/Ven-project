/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.properties;

/**
 *
 * @author AKDESKDEV90
 */
public class FindProtByKeyAndTreatmenstInput {
    private Integer idKeyPatient;
    private Integer idTreatments;
   

    public FindProtByKeyAndTreatmenstInput(Integer idTreatments, Integer idKeyPatient) {
        this.idTreatments = idTreatments;
        this.idKeyPatient = idKeyPatient;       
    
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }
    
}
