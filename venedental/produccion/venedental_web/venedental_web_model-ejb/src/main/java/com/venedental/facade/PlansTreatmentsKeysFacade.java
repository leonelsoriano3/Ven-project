/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansTreatmentsKeys;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PlansTreatmentsKeysFacade extends AbstractFacade<PlansTreatmentsKeys> implements PlansTreatmentsKeysFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlansTreatmentsKeysFacade() {
        super(PlansTreatmentsKeys.class);
    }
    
    /**
     * Service that lets you search for treatments plans keys by id´plans
     * @param id
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByPlansId(Integer id) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByPlansId", PlansTreatmentsKeys.class);
        query.setParameter("plansId", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Service that lets you search for treatments plans keys by  key id
     * @param id
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByKeysId(Integer id) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysId", PlansTreatmentsKeys.class);
        query.setParameter("keysId", id);
        return query.getResultList();
    }
    
    /**
     * Service that lets you search for treatments plans key by patient id
     * @param patientId
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByPatientId(Integer patientId) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByPatientId", PlansTreatmentsKeys.class);
        query.setParameter("patientId", patientId);
        return query.getResultList();
    }
    
    
    /**
     * Service that lets you search for treatments plans keys by two status and specialistId
     * @param idStatus1
     * @param idStatus2
     * @param specialistId
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByTwoStatusAndSpecialist(int idStatus1, int idStatus2, int specialistId) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByTwoStatusAndSpecialist", PlansTreatmentsKeys.class);
        query.setParameter("idStatus1", idStatus1);
        query.setParameter("idStatus2", idStatus2);
        query.setParameter("specialistId", specialistId);
        return query.getResultList();
    }
    
    /**
     * Service that lets you search for treatments plans keys by one status and specialistId
     * @param idStatus
     * @param specialistId
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByStatusAndSpecialist(int idStatus, int specialistId) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByStatusAndSpecialist", PlansTreatmentsKeys.class);
        query.setParameter("idStatus", idStatus);
        query.setParameter("specialistId", specialistId);
        return query.getResultList();
    }
    
    /**
     * Service that lets you search for treatments plans keys by keysId and one statusId
     * @param keysId
     * @param idStatus
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByKeysAndStatus(int keysId, int idStatus) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysAndStatus", PlansTreatmentsKeys.class);
        query.setParameter("idStatus", idStatus);
        query.setParameter("keysId", keysId);
        return query.getResultList();
    }
    
    /**
     * Service that lets you search for treatments plans keys by keysId and one statusId
     * @param keysId
     * @param idStatus
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByKeysAndExcludeStatus(int keysId, int idStatus) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysAndExcludeStatus", PlansTreatmentsKeys.class);
        query.setParameter("idStatus", idStatus);
        query.setParameter("keysId", keysId);
        return query.getResultList();
    }
    /**
     * Service that lets you search for treatments plans keys by keysId and two statusId
     * @param keysId
     * @param idStatus1
     * @param idStatus2
     * @return List PlansTreatmentsKeys
     */
    @Override
    public List<PlansTreatmentsKeys> findByKeysAndTwoStatus(int keysId, int idStatus1, int idStatus2) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysAndTwoStatus", PlansTreatmentsKeys.class);
        query.setParameter("idStatus1", idStatus1);
        query.setParameter("idStatus2", idStatus2);
        query.setParameter("keysId", keysId);
        return query.getResultList();
    }

    /**
     * Service that lets you search for one treatments plans keys by keysId and plansTreatmentId
     * @param keysId
     * @param plansTreatmentId
     * @return PlansTreatmentsKeys
     */
    @Override
    public PlansTreatmentsKeys findByKeysAndPlansTreatment(int keysId, int plansTreatmentId) {
        try {
            TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysAndPlansTreatment", PlansTreatmentsKeys.class);
            query.setParameter("keysId", keysId);
            query.setParameter("plansTreatmentId", plansTreatmentId);
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }
    
    /**
     * Call service that allows the stored procedure that searches a list of keys treatments plans that have been accepted or rejected
     * @param specialistId
     * @param dateFrom
     * @param dateUntil
     * @return 
     */

    @Override
    public List<PlansTreatmentsKeys> filterBySpecialistTratmentStatusAndDateRange(Integer specialistId, Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_PLANES_TRATAMIENTOS_ESPECIALISTA_ACEPTADOS_RECHAZADOS", PlansTreatmentsKeys.class);
        query.registerStoredProcedureParameter("PI_ID_ESPECIALISTA", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
        query.setParameter("PI_ID_ESPECIALISTA", specialistId);
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);
        return query.getResultList();
    }

    @Override
    public List<PlansTreatmentsKeys> findByKeysAndThreeStatus(int keysId, int idStatus1, int idStatus2, int idStatus3) {
        TypedQuery<PlansTreatmentsKeys> query = em.createNamedQuery("PlansTreatmentsKeys.findByKeysAndThreeStatus", PlansTreatmentsKeys.class);
        query.setParameter("idStatus1", idStatus1);
        query.setParameter("idStatus2", idStatus2);
        query.setParameter("idStatus3", idStatus3);
        query.setParameter("keysId", keysId);
        return query.getResultList();
    }
    
}
