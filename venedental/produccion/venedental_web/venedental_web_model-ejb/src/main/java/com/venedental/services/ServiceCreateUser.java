package com.venedental.services;

import com.venedental.dao.createUser.*;
import com.venedental.dao.createUser.UpdateIdUserByIdSpecialistDAO;
import com.venedental.dto.InsertUserDataInput;
import com.venedental.dto.createUser.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.SQLException;


/**
 * Created by leonelsoriano3@gmail.com on 31/03/16.
 */
@Stateless
public class ServiceCreateUser {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * @param cardId cedula del especialista
     */
    public FindUserByCardIdOutput findSpecialistByCardId(Integer cardId){
        FindUserByCardIdDAO findUserByCardIdDAO = new FindUserByCardIdDAO(ds);
        findUserByCardIdDAO.execute(cardId);
        return findUserByCardIdDAO.getResult();
    }

    /**
     * @param name nombre del especialista
     *     @param lastName apellido del especialista
     */
    public String getUserNameByIdSpecialist(String name, String lastName)
    {
        LoginUserInput loginUserInput;
        loginUserInput = new LoginUserInput(name,lastName);
        LoginUserDAO loginUserDAO =new LoginUserDAO(ds);
        loginUserDAO.execute(loginUserInput);
        return loginUserDAO.getResult();
    }
    public boolean updateIdUserByIdEmployee(Integer idEmployee, Integer idUser){
        UpdateIdUserByEmployeeInput updateIdUserByEmployeeInput;
        updateIdUserByEmployeeInput = new UpdateIdUserByEmployeeInput(idEmployee,idUser);
        UpdateidUserbyEmployeeDAO updateidUserbyEmployeeDAO;
        updateidUserbyEmployeeDAO = new UpdateidUserbyEmployeeDAO(ds);

        try {
            updateidUserbyEmployeeDAO.execute(updateIdUserByEmployeeInput);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    public Integer getUserIdIsertData(String userName,String login, String password,String email)
    {
        InsertUserDataInput insertUserDataInput;
        insertUserDataInput = new InsertUserDataInput(userName,login,password,email);
        InsertUserDataDAO insertUserDataDAO = new InsertUserDataDAO(ds);
        insertUserDataDAO.execute(insertUserDataInput);
        return insertUserDataDAO.getResult();
    }

    /**
     *
     * @param idSpecialist
     * @param idUser
     * @throws SQLException
     */
    public void updateIdUserByIdSpecialist(Integer idSpecialist, Integer idUser) throws SQLException {
        UpdateIdUserByIdSpecialistInput updateIdUserByIdSpecialistInput;
        updateIdUserByIdSpecialistInput = new UpdateIdUserByIdSpecialistInput(idSpecialist,idUser);
        UpdateIdUserByIdSpecialistDAO updateIdUserByIdSpecialistDAO = new UpdateIdUserByIdSpecialistDAO(ds);
        updateIdUserByIdSpecialistDAO.execute(updateIdUserByIdSpecialistInput);
    }

    /**
     * cambia la contraseña pasandole el ide del usuario
     * @param idUser
     * @param newPassword
     * @return
     */
    public boolean updatePassword(Integer idUser,String newPassword){

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPasswordEncripter = passwordEncoder.encode(newPassword);

        UpdatePasswordDTO updatePasswordDTO = new UpdatePasswordDTO();
        updatePasswordDTO.setIdUser(idUser);
        updatePasswordDTO.setPassword(newPasswordEncripter);
        UpdatePAsswordDao updatePAsswordDao = new UpdatePAsswordDao(ds);
        try {
            updatePAsswordDao.execute(updatePasswordDTO);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;

        }
    }

    public UserByLoginDTO findUserByLogin(String login){
         UserByLoginDAO userByLoginDAO = new UserByLoginDAO(ds);
        userByLoginDAO.execute(login);
        return  userByLoginDAO.getResult();
    }




}

