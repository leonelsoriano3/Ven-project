package com.venedental.dto.reportAuditConsultTrace;

import java.sql.Date;

/**
 * Created by luis.carrasco@arkiteck.biz on 10/11/16.
 */
public class ReportPaymentDetailsOutput {

    private Integer idKey;
    private String numKey;
    private String date;
    private Integer idKeyTreatment;
    private Integer idPatient;
    private String cardIdPatient;
    private String namePatient;
    private Integer idTreatment;
    private String nameTreatment;
    private Integer idToothKeyTreatment;
    private Integer idTooth;
    private String nameTooth;
    private String statusTooth;
    private String observation;
    private String baremo;

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public String getBaremo() {
        return baremo;
    }

    public void setBaremo(String baremo) {
        this.baremo = baremo;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getStatusTooth() {
        return statusTooth;
    }

    public void setStatusTooth(String statusTooth) {
        this.statusTooth = statusTooth;
    }

    public String getNameTooth() {
        return nameTooth;
    }

    public void setNameTooth(String nameTooth) {
        this.nameTooth = nameTooth;
    }

    public Integer getIdTooth() {
        return idTooth;
    }

    public void setIdTooth(Integer idTooth) {
        this.idTooth = idTooth;
    }

    public Integer getIdToothKeyTreatment() {
        return idToothKeyTreatment;
    }

    public void setIdToothKeyTreatment(Integer idToothKeyTreatment) {
        this.idToothKeyTreatment = idToothKeyTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getCardIdPatient() {
        return cardIdPatient;
    }

    public void setCardIdPatient(String cardIdPatient) {
        this.cardIdPatient = cardIdPatient;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdKeyTreatment() {
        return idKeyTreatment;
    }

    public void setIdKeyTreatment(Integer idKeyTreatment) {
        this.idKeyTreatment = idKeyTreatment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumKey() {
        return numKey;
    }

    public void setNumKey(String numKey) {
        this.numKey = numKey;
    }
}
