package com.venedental.dto.reportAuditConsultTrace;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class ReportTreatmentsDataSource implements JRDataSource {

    private int index = -1;
    private final List<ReportTreatmentDetailsOutput> listRep;

    public ReportTreatmentsDataSource(List<ReportTreatmentDetailsOutput> listRep) {
        this.listRep = listRep;
    }

    @Override
    public boolean next() throws JRException {
        System.out.println("\t\t" + (index + 1) + "\t");
        return ++index < listRep.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        ReportTreatmentDetailsOutput to = listRep.get(index);
        System.out.print("\t\t" +jrf.getName());
        switch (jrf.getName()){
            case "dateKey":
                return stringValido(to.getDate());
            case "namePatient":
                return stringValido(to.getNamePatient());
            case "numberKey":
                return stringValido(to.getNumberKey());
            case "numTooth":
                return stringValido(to.getNameTooth());
            case "nameTreatment":
                return stringValido(to.getNameTreatment());
        }
        return null;
    }

    private String stringValido(String str){
        if (str != null){
            return str.trim();
        }
        return "";
    }
}
