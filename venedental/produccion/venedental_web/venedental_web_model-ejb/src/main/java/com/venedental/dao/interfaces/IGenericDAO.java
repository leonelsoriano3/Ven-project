/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.interfaces;

import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author usuario
 * @param <InputType>
 * @param <StatementType>
 */
public interface IGenericDAO<InputType, StatementType extends Statement> {

    void prepareInput(InputType input, StatementType statement) throws SQLException;

    void execute(InputType input) throws SQLException;

}
