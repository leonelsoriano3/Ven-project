/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.TypeAttention;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class TypeAttentionFacade extends AbstractFacade<TypeAttention> implements TypeAttentionFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TypeAttentionFacade() {
        super(TypeAttention.class);
    }

    @Override
    public List<TypeAttention> findByStatus(int status) {
        TypedQuery<TypeAttention> query = em.createNamedQuery("TypeAttention.findByStatus", TypeAttention.class);
        query.setParameter("status", status);
        return query.getResultList();
    }

}