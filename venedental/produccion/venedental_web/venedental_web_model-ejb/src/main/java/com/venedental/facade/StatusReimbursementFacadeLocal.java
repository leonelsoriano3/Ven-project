/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.StatusReimbursement;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESK12
 */
@Local
public interface StatusReimbursementFacadeLocal {

    StatusReimbursement create(StatusReimbursement statusReimbursement);

    StatusReimbursement edit(StatusReimbursement statusReimbursement);

    void remove(StatusReimbursement statusReimbursement);

    StatusReimbursement find(Object id);
    
    StatusReimbursement findById(int id);

    List<StatusReimbursement> findAll();
    
    List<StatusReimbursement> findAllRangeStatus(int range1, int range2);

    List<StatusReimbursement> findRange(int[] range);
    
    

    int count();
    
}
