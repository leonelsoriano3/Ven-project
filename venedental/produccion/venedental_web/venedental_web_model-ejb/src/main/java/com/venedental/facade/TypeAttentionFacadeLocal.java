/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.TypeAttention;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface TypeAttentionFacadeLocal {

    TypeAttention create(TypeAttention typeAttention);

    TypeAttention edit(TypeAttention typeAttention);

    TypeAttention find(Object id);

    List<TypeAttention> findAll();

    List<TypeAttention> findRange(int[] range);

    int count();

    List<TypeAttention> findByStatus(int status);

}
