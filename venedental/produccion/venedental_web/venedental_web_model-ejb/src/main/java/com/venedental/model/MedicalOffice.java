/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "medical_office", schema = "")
@XmlRootElement

public class MedicalOffice  implements Serializable   {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_clinics", referencedColumnName = "id")
    private Clinics clinicId;

    @Size(max = 20)
    @Column(name = "rif")
    private String rif;

    @Basic(optional = true)
    @Size(max = 255)
    @Column(name = "location")
    private String location;

    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "medicalOfficeId")
    private List<SpecialistJob> specialistJobList;

    @JoinColumn(name = "id_address", referencedColumnName = "id")
    @OneToOne(cascade = CascadeType.ALL)
    private Addresses addressId;

    /*Used DTO*/
    @Transient
    private Integer idAddress;

    @Transient
    private String descriptionAddress;


    public MedicalOffice() {
    }

    public Integer getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(Integer idAddress) {
        this.idAddress = idAddress;
    }

    public String getDescriptionAddress() {
        return descriptionAddress;
    }

    public void setDescriptionAddress(String descriptionAddress) {
        this.descriptionAddress = descriptionAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @XmlTransient
    public List<SpecialistJob> getSpecialistJobList() {
        return specialistJobList;
    }

    public void setSpecialistJobList(List<SpecialistJob> specialistJobList) {
        this.specialistJobList = specialistJobList;
    }

    public Addresses getAddressId() {
        return addressId;
    }

    public void setAddressId(Addresses addressId) {
        this.addressId = addressId;
    }

    public Clinics getClinicId() {
        return clinicId;
    }

    public void setClinicId(Clinics clinicId) {
        this.clinicId = clinicId;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        MedicalOffice obj = (MedicalOffice) super.clone();

        obj.addressId = obj.addressId == null ? null : (Addresses) obj.addressId.clone();

        return obj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalOffice)) {
            return false;
        }
        MedicalOffice other = (MedicalOffice) object;
        return (this.getId() != null || other.getId() == null) && (this.getId() == null || this.getId().equals(other.getId()));
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.MedicalOffice[ id=" +this.getId()+ " ]";
    }

}