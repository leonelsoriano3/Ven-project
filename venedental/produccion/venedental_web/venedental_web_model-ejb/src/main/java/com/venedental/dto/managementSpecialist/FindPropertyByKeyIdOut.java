package com.venedental.dto.managementSpecialist;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 */
public class FindPropertyByKeyIdOut {

    private  Integer idPlanTreatment;
    private Integer idPieceTreatment;
    private Integer idBaremoTreatment;
    private Integer idBaremoPiece;
    private Integer idTreatment;
    private String nameTreatment;
    private Integer idPiece;
    private Integer valuePiece;
    private Boolean isSelected;


    public FindPropertyByKeyIdOut() {
    }

    public FindPropertyByKeyIdOut(Integer idPlanTreatment, Integer idPieceTreatment, Integer idBaremoTreatment,
                                  Integer idBaremoPiece, Integer idTreatment, String nameTreatment, Integer idPiece,
                                  Integer valuePiece,Boolean isSelected) {

        this.idPlanTreatment = idPlanTreatment;
        this.idPieceTreatment = idPieceTreatment;
        this.idBaremoTreatment = idBaremoTreatment;
        this.idBaremoPiece = idBaremoPiece;
        this.idTreatment = idTreatment;
        this.nameTreatment = nameTreatment;
        this.idPiece = idPiece;
        this.valuePiece = valuePiece;
        this.isSelected = isSelected;
    }

    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdPieceTreatment() {
        return idPieceTreatment;
    }

    public void setIdPieceTreatment(Integer idPieceTreatment) {
        this.idPieceTreatment = idPieceTreatment;
    }

    public Integer getIdBaremoTreatment() {
        return idBaremoTreatment;
    }

    public void setIdBaremoTreatment(Integer idBaremoTreatment) {
        this.idBaremoTreatment = idBaremoTreatment;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Integer getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(Integer idPiece) {
        this.idPiece = idPiece;
    }

    public Integer getValuePiece() {
        return valuePiece;
    }

    public void setValuePiece(Integer valuePiece) {
        this.valuePiece = valuePiece;
    }

    public Integer getIdBaremoPiece() {
        return idBaremoPiece;
    }

    public void setIdBaremoPiece(Integer idBaremoPiece) {
        this.idBaremoPiece = idBaremoPiece;
    }

    public Boolean getSelected() {return isSelected; }

    public void setSelected(Boolean selected) {isSelected = selected; }

}