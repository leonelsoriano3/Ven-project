/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BankAccounts;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface BankAccountsFacadeLocal {

    BankAccounts create(BankAccounts bankAccounts);

    BankAccounts edit(BankAccounts bankAccounts);

    void remove(BankAccounts bankAccounts);

    BankAccounts find(Object id);

    List<BankAccounts> findAll();

    List<BankAccounts> findRange(int[] range);

    int count();
    
}
