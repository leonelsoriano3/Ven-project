/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.massiveLoading;

import java.io.File;
import java.util.List;

/**
 *
 * @author akdesk01
 */
public class FileLoad {

    private final String fileName;
    private final List<Integer> fields;
    private InsuranceLoad insuranceLoad;

    public FileLoad(String fileName, List<Integer> fields) {
        this.fields = fields;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public File getFileExcel() {
        return new File(getFilepath());
    }

    public String getFilepath() {
        return insuranceLoad.getFolderPath() + "/" + getFileName();
    }

    public List<Integer> getFields() {
        return fields;
    }

    public InsuranceLoad getInsuranceLoad() {
        return insuranceLoad;
    }

    public void setInsuranceLoad(InsuranceLoad insuranceLoad) {
        this.insuranceLoad = insuranceLoad;
    }
    
}
