/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.baremo;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.baremo.ConsultBaremoTreatmentForConfigOutput;
import java.sql.CallableStatement;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultBaremoTreatmentForConfigDAO extends AbstractQueryDAO<Integer, ConsultBaremoTreatmentForConfigOutput> {

    public ConsultBaremoTreatmentForConfigDAO(DataSource dataSource) {
        super(dataSource, " SP_T_SCALETREATME_G_006", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        try {
            if (input == 0) {
                statement.setNull(1, java.sql.Types.NULL);
            } else {
                statement.setInt(1, input);
            }
            } catch (SQLException e) {
            System.out.println(e);
        }

    }

    @Override
    public ConsultBaremoTreatmentForConfigOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultBaremoTreatmentForConfigOutput consultBaremoTreatmentForConfigOutput = new ConsultBaremoTreatmentForConfigOutput();
        consultBaremoTreatmentForConfigOutput.setDateBaremo(rs.getDate("fecha_baremo"));
        consultBaremoTreatmentForConfigOutput.setDateBaremoString(rs.getString("fecha_baremo"));
        consultBaremoTreatmentForConfigOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultBaremoTreatmentForConfigOutput.setBaremo(rs.getDouble("baremo"));
        consultBaremoTreatmentForConfigOutput.setIdBaremo(rs.getInt("id_baremo"));
        consultBaremoTreatmentForConfigOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        return consultBaremoTreatmentForConfigOutput;
    }

}
