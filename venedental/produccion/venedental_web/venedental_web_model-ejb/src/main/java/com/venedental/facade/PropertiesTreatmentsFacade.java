/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PropertiesTreatments;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PropertiesTreatmentsFacade extends AbstractFacade<PropertiesTreatments> implements  PropertiesTreatmentsFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PropertiesTreatmentsFacade() {
        super(PropertiesTreatments.class);
    }


    @Override
    public  PropertiesTreatments edit(PropertiesTreatments entity) {
        return super.edit(entity);
    }

    @Override
    public  PropertiesTreatments create(PropertiesTreatments entity) {
        return super.create(entity);
    }

    @Override
    public List<PropertiesTreatments> findByTreatmentsId(Integer id) {
        TypedQuery<PropertiesTreatments> query = em.createNamedQuery("PropertiesTreatments.findByTreatmenstId", PropertiesTreatments.class);  
        query.setParameter("treatmentsId", id);
       return  query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
