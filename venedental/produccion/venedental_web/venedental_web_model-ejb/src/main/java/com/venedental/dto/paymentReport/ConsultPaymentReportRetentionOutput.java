/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import java.util.Date;

/**
 *
 * @author root
 */
public class ConsultPaymentReportRetentionOutput {
    String numberISLR;
    Date dateISLR;
    Date billDate;
    String billNumber;
    String numberControl;
    Double amountTotal;
    Double baseISLR;
    Double rateISLR;
    Double amountISLR;

    public ConsultPaymentReportRetentionOutput() {
    }

    public ConsultPaymentReportRetentionOutput(String numberISLR, Date dateISLR, Date billDate, String billNumber, String numberControl, Double amountTotal, Double baseISLR, Double rateISLR, Double amountISLR) {
        this.numberISLR = numberISLR;
        this.dateISLR = dateISLR;
        this.billDate = billDate;
        this.billNumber = billNumber;
        this.numberControl = numberControl;
        this.amountTotal = amountTotal;
        this.baseISLR = baseISLR;
        this.rateISLR = rateISLR;
        this.amountISLR = amountISLR;
    }

    public String getNumberISLR() {
        return numberISLR;
    }

    public void setNumberISLR(String numberISLR) {
        this.numberISLR = numberISLR;
    }

    public Date getDateISLR() {
        return dateISLR;
    }

    public void setDateISLR(Date dateISLR) {
        this.dateISLR = dateISLR;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getNumberControl() {
        return numberControl;
    }

    public void setNumberControl(String numberControl) {
        this.numberControl = numberControl;
    }

    public Double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        this.amountTotal = amountTotal;
    }

    public Double getBaseISLR() {
        return baseISLR;
    }

    public void setBaseISLR(Double baseISLR) {
        this.baseISLR = baseISLR;
    }

    public Double getRateISLR() {
        return rateISLR;
    }

    public void setRateISLR(Double rateISLR) {
        this.rateISLR = rateISLR;
    }

    public Double getAmountISLR() {
        return amountISLR;
    }

    public void setAmountISLR(Double amountISLR) {
        this.amountISLR = amountISLR;
    }
    
    
    
}
