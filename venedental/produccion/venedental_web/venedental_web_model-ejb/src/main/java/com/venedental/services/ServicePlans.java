/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.plans.FindPlansByIdDAO;
import com.venedental.dto.plans.FindPlanByIdOutput;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
@Stateless
public class ServicePlans {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds; 
    
    public List<FindPlanByIdOutput> findPlans(Integer idPlan)  {
        
        FindPlansByIdDAO findPlansByIdDAO = new FindPlansByIdDAO(ds);
        findPlansByIdDAO.execute(idPlan);
        return findPlansByIdDAO.getResultList();
        //return null;
    }
    
}
