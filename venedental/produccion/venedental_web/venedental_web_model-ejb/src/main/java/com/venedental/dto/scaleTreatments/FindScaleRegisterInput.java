/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.scaleTreatments;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class FindScaleRegisterInput {
    private Integer idSpecialist;
    private Integer idTreatments;
    private Date date;
    private Integer idProTratment;

    public FindScaleRegisterInput(Integer idSpecialist, Integer idTreatments, Date date, Integer idProTratment) {
        this.idSpecialist = idSpecialist;
        this.idTreatments = idTreatments;
        this.date = date;
        this.idProTratment = idProTratment;
    }    

    public FindScaleRegisterInput(Integer idSpecialist, Integer idTreatments, Date date) {
        this.idSpecialist = idSpecialist;
        this.idTreatments = idTreatments;
        this.date = date;
    }

    public Integer getIdProTratment() {
        return idProTratment;
    }

    public void setIdProTratment(Integer idProTratment) {
        this.idProTratment = idProTratment;
    }    

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
       
}
