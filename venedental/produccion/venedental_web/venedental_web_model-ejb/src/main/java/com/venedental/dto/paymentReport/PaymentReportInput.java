/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import java.util.Date;

/**
 *
 * @author root
 */
public class PaymentReportInput {
    int idSpecialist;
    int idPaymentReport;
    
    String numberPaymentReport;
    String billNumber;
    Date date;
    String controlNumber;
    String transferNumber;
    String accountNumber;
    double amount;
    double auditedAmount;
    
    String numberISLR;

    public PaymentReportInput(int idSpecialist, String numberISLR) {
        this.idSpecialist = idSpecialist;
        this.numberISLR = numberISLR;
    }
    

    public PaymentReportInput(int idSpecialist, int idPaymentReport) {
        this.idSpecialist = idSpecialist;
        this.idPaymentReport = idPaymentReport;
    }

    public PaymentReportInput(String billNumber, Date date, String controlNumber) {
        this.billNumber = billNumber;
        this.date = date;
        this.controlNumber = controlNumber;
    }


    public PaymentReportInput() {
    }

    public int getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(int idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public int getIdPaymentReport() {
        return idPaymentReport;
    }

    public void setIdPaymentReport(int idPaymentReport) {
        this.idPaymentReport = idPaymentReport;
    }


    public String getNumberPaymentReport() {
        return numberPaymentReport;
    }

    public void setNumberPaymentReport(String numberPaymentReport) {
        this.numberPaymentReport = numberPaymentReport;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public String getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAuditedAmount() {
        return auditedAmount;
    }

    public void setAuditedAmount(double auditedAmount) {
        this.auditedAmount = auditedAmount;
    }

    public String getNumberISLR() {
        return numberISLR;
    }

    public void setNumberISLR(String numberISLR) {
        this.numberISLR = numberISLR;
    }

}
