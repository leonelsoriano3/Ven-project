/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import com.venedental.dto.paymentReport.PaymentReportOutput;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyDetailsStatusPaidOutPut {

    private Integer patientId;
    private String patientName;
    private Integer treatmentKeyId;
    private Integer treatmentKeyPieceId;
    private String keyNumber;
    private String treatmentName;
    private String numberPiece;
    private String observation;
    private Integer statusId;
    private String nameStatus;
    private Double mountPaid;
    private Date date;


    public ConsultKeyDetailsStatusPaidOutPut(Integer patientId, String patientName, Integer treatmentKeyId,
                                             Integer treatmentKeyPieceId, String keyNumber, String treatmentName,
                                             String numberPiece, String observation, Integer statusId, String nameStatus,
                                             Double mountPaid, Date date) {
        this.patientId = patientId;
        this.patientName = patientName;
        this.treatmentKeyId = treatmentKeyId;
        this.treatmentKeyPieceId = treatmentKeyPieceId;
        this.keyNumber = keyNumber;
        this.treatmentName = treatmentName;
        this.numberPiece = numberPiece;
        this.observation = observation;
        this.statusId = statusId;
        this.nameStatus = nameStatus;
        this.mountPaid = mountPaid;
        this.date = date;

    }

    public ConsultKeyDetailsStatusPaidOutPut() {
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public Integer getTreatmentKeyId() {
        return treatmentKeyId;
    }

    public void setTreatmentKeyId(Integer treatmentKeyId) {
        this.treatmentKeyId = treatmentKeyId;
    }

    public Integer getTreatmentKeyPieceId() {
        return treatmentKeyPieceId;
    }

    public void setTreatmentKeyPieceId(Integer treatmentKeyPieceId) {
        this.treatmentKeyPieceId = treatmentKeyPieceId;
    }

    public String getKeyNumber() {
        return keyNumber;
    }

    public void setKeyNumber(String keyNumber) {
        this.keyNumber = keyNumber;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    public String getNumberPiece() {
        return numberPiece;
    }

    public void setNumberPiece(String numberPiece) {
        this.numberPiece = numberPiece;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public Double getMountPaid() {
        return mountPaid;
    }

    public void setMountPaid(Double mountPaid) {
        this.mountPaid = mountPaid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ConfirmKeyDetailsOutput convertToConfirmKeyDetailsOutput() {

        ConfirmKeyDetailsOutput confirmKeyDetailsOutput = new ConfirmKeyDetailsOutput();

        confirmKeyDetailsOutput.setAmountPaid(mountPaid);
        confirmKeyDetailsOutput.setIdPatient(patientId);
        confirmKeyDetailsOutput.setIdPlansTreatmentsKey(treatmentKeyId);
        confirmKeyDetailsOutput.setIdStatus(statusId);
        confirmKeyDetailsOutput.setIdPropertiesPlansTreatmentsKey(treatmentKeyPieceId);
        confirmKeyDetailsOutput.setNamePatient(patientName);
        confirmKeyDetailsOutput.setNameProperties(numberPiece);
        confirmKeyDetailsOutput.setNameStatus(nameStatus);
        confirmKeyDetailsOutput.setNameTreatments(treatmentName);
        confirmKeyDetailsOutput.setNumberKey(keyNumber);
        confirmKeyDetailsOutput.setObservation(observation);
        confirmKeyDetailsOutput.setDateTreatment(date);
        
        return confirmKeyDetailsOutput;

    }







}
