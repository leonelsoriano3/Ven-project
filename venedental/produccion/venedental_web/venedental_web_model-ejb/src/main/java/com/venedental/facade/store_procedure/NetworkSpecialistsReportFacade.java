/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.facade.store_procedure;

import com.venedental.dto.AuditReportInput;
import com.venedental.dto.AuditReportOutput;
import com.venedental.dto.NetworkSpecialistsReportOutput;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class NetworkSpecialistsReportFacade
extends AbstractStoreProcedureFacade<String, NetworkSpecialistsReportOutput>
implements NetworkSpecialistsReportFacadeLocal {
    
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public NetworkSpecialistsReportFacade() {
        super("SP_REPORTE_ESPECIALISTAS");
    }
    
    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
    }
    
    @Override
    protected void prepareInput(StoredProcedureQuery query, String input) {
        
    }
    
    @Override
    protected NetworkSpecialistsReportOutput prepareOutput(Object[] result) {
        NetworkSpecialistsReportOutput output = new NetworkSpecialistsReportOutput();
        output.setIdentityNumber((Integer) result[0]);
        output.setName((String) result[1]);
        output.setHealthArea((String) result[2]);
        output.setSpeciality((String) result[3]);
        output.setClinicName((String) result[4]);
        output.setPhone((String) result[5]);
        output.setAddress((String) result[6]);
        output.setCity((String) result[7]);
        output.setState((String) result[8]);
        output.setSchedule((String) result[9]);
        return output;
    }
}

