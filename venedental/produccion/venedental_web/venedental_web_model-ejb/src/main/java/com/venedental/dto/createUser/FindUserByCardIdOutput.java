package com.venedental.dto.createUser;

/**
 * Created by akdeskdev90 on 30/03/16.
 */
public class FindUserByCardIdOutput {

    private Integer id;
    private String firstName;
    private String lastName;
    private String completeName;
    private String email;
    private String login;
    private Integer usersId;
    private String user_type;
    private Integer idEmployee;


    public FindUserByCardIdOutput() {

    }

    public FindUserByCardIdOutput(Integer id, String firstName,String lastName,String completeName,String email,
                                  String login, Integer usersId,Integer idEmployee) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.completeName = completeName;
        this.email = email;
        this.login = login;


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {return login;}

    public void setLogin(String login) {this.login = login;}

    public Integer getUsersId() {return usersId; }

    public void setUsersId(Integer usersId) {this.usersId = usersId;}

    public String getUser_type() {return user_type;}

    public void setUser_type(String user_type) {this.user_type = user_type;}


    public Integer getIdEmployee() { return idEmployee;}

    public void setIdEmployee(Integer idEmployee) {this.idEmployee = idEmployee;}
}
