
package com.venedental.dto.paymentReport;

import java.io.Serializable;
import java.util.Date;



/**
 * Clase principal para los parametros a utilizar en el procedimiento almacenado 
 * SP_T_PAYMENTREPOR_G_007
 * 
 * @author David Rivero
 */
public class PaymentReportkeysAnalisysByCriterialInput implements Serializable {

    /* Attributes */
    private Integer IndentityNumberSpecialistsClinic;
    private Date startDate;
    private Date endDate;
    private String numberInvoices;
    private Integer statusKeysAnalysis;
    
    
    /* Constructs */
    public PaymentReportkeysAnalisysByCriterialInput(Integer IndentityNumberSpecialistsClinic, Date startDate, Date endDate, String numberInvoices, Integer statusKeysAnalysis) {
        this.IndentityNumberSpecialistsClinic = IndentityNumberSpecialistsClinic;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numberInvoices = numberInvoices;
        this.statusKeysAnalysis = statusKeysAnalysis;
    }

    /* Getter and Setter */
    
    /**
     * obtener el valor del id del especialista o clinica asociado al criterio.
     *
     * @return 
     */
    public Integer getIndentityNumberSpecialistsClinic() {
        return IndentityNumberSpecialistsClinic;
    }
    
    /**
     * almacenar el valor del id del especialista o clinica.
     * 
     * @param IndentityNumberSpecialistsClinic 
     */
    public void setIndentityNumberSpecialistsClinic(Integer IndentityNumberSpecialistsClinic) {
        this.IndentityNumberSpecialistsClinic = IndentityNumberSpecialistsClinic;
    }
    
    /**
     *  obtener el valor del numero de factura asociado al criterio de busqueda.
     * 
     * @return 
     */
    public String getNumberInvoices() {
        return numberInvoices;
    }
    
    /**
     * almacenar el valor del numero de factura asociado al criterio de busqueda.
     * 
     * @param numberInvoices 
     */
    public void setNumberInvoices(String numberInvoices) {
        this.numberInvoices = numberInvoices;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStatusKeysAnalysis() {
        return statusKeysAnalysis;
    }

    public void setStatusKeysAnalysis(Integer statusKeysAnalysis) {
        this.statusKeysAnalysis = statusKeysAnalysis;
    }
    
    
    
}
