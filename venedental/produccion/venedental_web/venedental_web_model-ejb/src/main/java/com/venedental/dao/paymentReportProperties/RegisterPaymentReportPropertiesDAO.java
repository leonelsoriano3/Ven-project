/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReportProperties;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.analysisKeys.CreatePaymentReportPropertiesInput;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class RegisterPaymentReportPropertiesDAO extends AbstractCommandDAO<CreatePaymentReportPropertiesInput> {

    public RegisterPaymentReportPropertiesDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYREPPROP_I_001", 2);
    }

    @Override
    public void prepareInput(CreatePaymentReportPropertiesInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getPaymentReportId());
        statement.setInt(2, input.getPropertiesPlansTreatmentsKeysId());
       
    }

}
