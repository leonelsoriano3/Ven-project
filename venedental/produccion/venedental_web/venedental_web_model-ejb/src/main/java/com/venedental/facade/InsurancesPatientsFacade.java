/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import com.venedental.model.InsurancesPatients;
import com.venedental.model.Patients;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class InsurancesPatientsFacade extends AbstractFacade<InsurancesPatients> implements InsurancesPatientsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InsurancesPatientsFacade() {
        super(InsurancesPatients.class);
    }

    @Override
    public InsurancesPatients edit(InsurancesPatients entity) {
        return super.edit(entity);
    }

    @Override
    public InsurancesPatients create(InsurancesPatients entity) {
        return super.create(entity);
    }

    @Override
    public List<InsurancesPatients> findByPatientsId(Integer id) {
        TypedQuery<InsurancesPatients> query = em.createNamedQuery("InsurancesPatients.findByPatientsId", InsurancesPatients.class);
        query.setParameter("patientsId", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<InsurancesPatients> findByInsuranceId(Insurances insurance) {
        TypedQuery<InsurancesPatients> query = em.createNamedQuery("InsurancesPatients.findByInsurancesId", InsurancesPatients.class);
        query.setParameter("insurancesId", insurance.getId());
        return query.getResultList();
    }

    @Override
    public List<InsurancesPatients> findByInsurancesPatientsId(Patients patient, Insurances insurance) {
        TypedQuery<InsurancesPatients> query = em.createNamedQuery("InsurancesPatients.findByInsurancesPatientsId", InsurancesPatients.class);
        query.setParameter("patientsId", patient.getId());
        query.setParameter("insurancesId", insurance.getId());
        return query.getResultList();
    }

}
