/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.FindDataSpecialistOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class FindDataSpecislitDAO extends AbstractQueryDAO<Integer, FindDataSpecialistOutput>{
    
    public FindDataSpecislitDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_G_007", 1);
    }
    
     @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public FindDataSpecialistOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        FindDataSpecialistOutput findDataSpecialistOutput = new FindDataSpecialistOutput();
        findDataSpecialistOutput.setId_Specialist(rs.getInt("id_especialista"));
        findDataSpecialistOutput.setNameSpecialist(rs.getString("nombre_especialista"));
        findDataSpecialistOutput.setIdentityCardSpecialist(rs.getInt("cedula_especialista"));
        findDataSpecialistOutput.setAddressSpecislist(rs.getString("direccion_especialista"));
        findDataSpecialistOutput.setPhoneSpecialist(rs.getString("telefono_especialista"));
        findDataSpecialistOutput.setSpecialties(rs.getString("especialidades"));
        findDataSpecialistOutput.setIdSpeciality(rs.getInt("id_especialidad"));
        findDataSpecialistOutput.setNameSpeciality(rs.getString("nombre_especialidad"));
        findDataSpecialistOutput.setNameMedicalOffice(rs.getString("nombre_consultorio"));
        findDataSpecialistOutput.setEmail_especialista(rs.getString("email_especialista"));
        findDataSpecialistOutput.setIdMedicalOficce(rs.getInt("id_consultorio"));
        return findDataSpecialistOutput;
    }

    
}
