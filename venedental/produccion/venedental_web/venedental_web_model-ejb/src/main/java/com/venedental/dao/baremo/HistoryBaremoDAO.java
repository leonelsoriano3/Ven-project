/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.baremo;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.baremo.HistoryBaremoInput;
import com.venedental.dto.baremo.HistoryBaremosOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class HistoryBaremoDAO extends AbstractQueryDAO<HistoryBaremoInput, HistoryBaremosOutput>{

    public HistoryBaremoDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SCALETREATME_G_004", 4);
    }

    @Override
    public void prepareInput(HistoryBaremoInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getId_treatment());
        statement.setDate(2, (Date) input.getStart_date());
        statement.setDate(3, (Date) input.getEnd_date());
        statement.setInt(4, input.getId_speciality());

//        if (input.getIdEspecialidad() == 0) {
//
//            statement.setNull(3, java.sql.Types.NULL);
//
//        } else {
//            statement.setInt(3, input.getIdEspecialidad());
//
//        }
    }

    @Override
    public HistoryBaremosOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        HistoryBaremosOutput historyBaremosOutput = new HistoryBaremosOutput();
        historyBaremosOutput.setIdBaremo(rs.getInt("id_baremo"));
        historyBaremosOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        historyBaremosOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        historyBaremosOutput.setEffective_date(rs.getDate("fecha_vigencia"));
        historyBaremosOutput.setBaremo(rs.getDouble("baremo"));
        historyBaremosOutput.setProcessData(rs.getString("datos_proceso"));
        return historyBaremosOutput;
    }


}
