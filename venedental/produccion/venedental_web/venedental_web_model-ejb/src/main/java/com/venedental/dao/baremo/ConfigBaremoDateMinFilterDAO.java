/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.baremo;

import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.baremo.ConfigBaremoDateMinFilterOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConfigBaremoDateMinFilterDAO extends AbstractQueryDAO<Integer, Date> {
    
    public ConfigBaremoDateMinFilterDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SCALETREATME_G_007", 2);
    }
    
    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        try {
            if (input == 0) {
                statement.setNull(1, java.sql.Types.NULL);
            } else {
                statement.setInt(1, input);
            }
            } catch (SQLException e) {
            System.out.println(e);
        }
         statement.registerOutParameter(2, java.sql.Types.BOOLEAN);

    }
    
   @Override
    public Date prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Date dateMinFilter;
        
        dateMinFilter = statement.getDate(2);
        
        return dateMinFilter;
    }
    
}
