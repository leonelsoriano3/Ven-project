/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PropertiesTreatments;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PropertiesTreatmentsFacadeLocal {

    PropertiesTreatments create(PropertiesTreatments properties);

    PropertiesTreatments edit(PropertiesTreatments properties);

    void remove(PropertiesTreatments properties);

    PropertiesTreatments find(Object id);

    List<PropertiesTreatments> findAll();

    List<PropertiesTreatments> findRange(int[] range);
    
    List<PropertiesTreatments> findByTreatmentsId(Integer id);

    int count();
    
    
}
