/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

import java.util.Date;

/**
 *
 * @author akdesk01
 */
public class HeadboardEmailOutput {
    
    private Integer idKey;
    private Integer idSpecialist;
    private Integer idPatient;
    private Integer idPlan;
    private Integer idInsurance;
    private Integer idEntity;
    private Integer idConsultingRoom;
    private Integer idRelationship;
    private Integer typePlan;
    private Date requestdate;
    private Date expirationDate;
    private Date birthdate;
    private String numberKey;
    private String namePatient;
    private String nameSpecialist;
    private String identityNumberPatient;
    private String namePlan;
    private String nameInsurance;
    private String nameEntity;
    private String specialties;
    private String nameRelationShip;
    private String nameConsultingRoom;

    public HeadboardEmailOutput() {
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

    public Integer getIdInsurance() {
        return idInsurance;
    }

    public void setIdInsurance(Integer idInsurance) {
        this.idInsurance = idInsurance;
    }

    public Integer getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(Integer idEntity) {
        this.idEntity = idEntity;
    }

    public Integer getIdConsultingRoom() {
        return idConsultingRoom;
    }

    public void setIdConsultingRoom(Integer idConsultingRoom) {
        this.idConsultingRoom = idConsultingRoom;
    }

    public Integer getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(Integer idRelationship) {
        this.idRelationship = idRelationship;
    }

    public Date getRequestdate() {
        return requestdate;
    }

    public void setRequestdate(Date requestdate) {
        this.requestdate = requestdate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public String getIdentityNumberPatient() {
        return identityNumberPatient;
    }

    public void setIdentityNumberPatient(String identityNumberPatient) {
        this.identityNumberPatient = identityNumberPatient;
    }

    public String getNamePlan() {
        return namePlan;
    }

    public void setNamePlan(String namePlan) {
        this.namePlan = namePlan;
    }

    public String getNameInsurance() {
        return nameInsurance;
    }

    public void setNameInsurance(String nameInsurance) {
        this.nameInsurance = nameInsurance;
    }

    public String getNameEntity() {
        return nameEntity;
    }

    public void setNameEntity(String nameEntity) {
        if(nameEntity.equals("")){
            nameEntity="-";
        }
        this.nameEntity = nameEntity;
    }

    public String getSpecialties() {
        return specialties;
    }

    public void setSpecialties(String specialties) {
        this.specialties = specialties;
    }

    public String getNameRelationShip() {
        return nameRelationShip;
    }

    public void setNameRelationShip(String nameRelationShip) {
        this.nameRelationShip = nameRelationShip;
    }

    public String getNameConsultingRoom() {
        return nameConsultingRoom;
    }

    public void setNameConsultingRoom(String nameConsultingRoom) {
        this.nameConsultingRoom = nameConsultingRoom;
    }

    public Integer getTypePlan() {
        return typePlan;
    }

    public void setTypePlan(Integer typePlan) {
        this.typePlan = typePlan;
    }
    
    
    
    
}
