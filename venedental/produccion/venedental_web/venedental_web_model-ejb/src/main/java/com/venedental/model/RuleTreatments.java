/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "rule_treatments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RuleTreatments.findAll", query = "SELECT r FROM RuleTreatments r"),
    @NamedQuery(name = "RuleTreatments.findById", query = "SELECT r FROM RuleTreatments r WHERE r.id = :id"),
    @NamedQuery(name = "RuleTreatments.findByMinValue", query = "SELECT r FROM RuleTreatments r WHERE r.minValue = :minValue"),
    @NamedQuery(name = "RuleTreatments.findByMaxValue", query = "SELECT r FROM RuleTreatments r WHERE r.maxValue = :maxValue"),
    @NamedQuery(name = "RuleTreatments.findByIsGeneral", query = "SELECT r FROM RuleTreatments r WHERE r.isGeneral = :isGeneral")})
public class RuleTreatments implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "min_value")
    private Double minValue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "max_value")
    private Double maxValue;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_general")
    private Boolean isGeneral;

    @JoinColumn(name = "id_treatment", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Treatments idTreatment;

    @JoinColumn(name = "id_frequency_keys", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private FrequencyKeys idFrequencyKeys;

    @ManyToMany(mappedBy = "ruleTreatmentsList")
    private List<PropertiesTreatments> propertiesTreatmentsList;

    public RuleTreatments() {
    }

    public RuleTreatments(Integer id) {
        this.id = id;
    }

    public RuleTreatments(Integer id, Double minValue, Double maxValue, Boolean isGeneral) {
        this.id = id;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.isGeneral = isGeneral;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Boolean getIsGeneral() {
        return isGeneral;
    }

    public void setIsGeneral(Boolean isGeneral) {
        this.isGeneral = isGeneral;
    }

    public Treatments getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Treatments idTreatment) {
        this.idTreatment = idTreatment;
    }

    public FrequencyKeys getIdFrequencyKeys() {
        return idFrequencyKeys;
    }

    public void setIdFrequencyKeys(FrequencyKeys idFrequencyKeys) {
        this.idFrequencyKeys = idFrequencyKeys;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RuleTreatments)) {
            return false;
        }
        RuleTreatments other = (RuleTreatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.RuleTreatments[ id=" + id + " ]";
    }

    @XmlTransient
    public List<PropertiesTreatments> getPropertiesTreatmentsList() {
        return propertiesTreatmentsList;
    }

    public void setPropertiesTreatmentsList(List<PropertiesTreatments> propertiesTreatmentsList) {
        this.propertiesTreatmentsList = propertiesTreatmentsList;
    }

}
