/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Edgar
 */
@Entity
@Table(name = "insurances_patients")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsurancesPatients.findAll", query = "SELECT i FROM InsurancesPatients i"),
    @NamedQuery(name = "InsurancesPatients.findByInsurancesId", query = "SELECT i FROM InsurancesPatients i WHERE i.insurancesPatientsPK.insurancesId = :insurancesId"),
    @NamedQuery(name = "InsurancesPatients.findByInsurancesPatientsId", query = "SELECT i FROM InsurancesPatients i WHERE i.insurancesPatientsPK.patientsId = :patientsId AND i.insurancesPatientsPK.insurancesId = :insurancesId"),
    @NamedQuery(name = "InsurancesPatients.findByPatientsId", query = "SELECT i FROM InsurancesPatients i WHERE i.insurancesPatientsPK.patientsId = :patientsId"),
    @NamedQuery(name = "InsurancesPatients.findByStatus", query = "SELECT i FROM InsurancesPatients i WHERE i.status = :status")})
public class InsurancesPatients implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    protected InsurancesPatientsPK insurancesPatientsPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Integer status;

    @JoinColumn(name = "patients_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Patients patients;

    @JoinColumn(name = "insurances_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Insurances insurances;

    public InsurancesPatients() {
    }

    public InsurancesPatients(InsurancesPatientsPK insurancesPatientsPK) {
        this.insurancesPatientsPK = insurancesPatientsPK;
    }

    public InsurancesPatients(InsurancesPatientsPK insurancesPatientsPK, Integer status) {
        this.insurancesPatientsPK = insurancesPatientsPK;
        this.status = status;
    }

    public InsurancesPatients(Integer insurancesId, Integer patientsId) {
        this.insurancesPatientsPK = new InsurancesPatientsPK(insurancesId, patientsId);
    }

    public InsurancesPatientsPK getInsurancesPatientsPK() {
        return insurancesPatientsPK;
    }

    public void setInsurancesPatientsPK(InsurancesPatientsPK insurancesPatientsPK) {
        this.insurancesPatientsPK = insurancesPatientsPK;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public Insurances getInsurances() {
        return insurances;
    }

    public void setInsurances(Insurances insurances) {
        this.insurances = insurances;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insurancesPatientsPK != null ? insurancesPatientsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsurancesPatients)) {
            return false;
        }
        InsurancesPatients other = (InsurancesPatients) object;
        if ((this.insurancesPatientsPK == null && other.insurancesPatientsPK != null) || (this.insurancesPatientsPK != null && !this.insurancesPatientsPK.equals(other.insurancesPatientsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.InsurancesPatients[ insurancesPatientsPK=" + insurancesPatientsPK + " ]";
    }

}
