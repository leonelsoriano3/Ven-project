/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BinnacleKeys;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESKDEV90
 */
@Local
public interface BinnacleKeysFacadeLocal {
    BinnacleKeys create(BinnacleKeys binnacleKeys);

    BinnacleKeys edit(BinnacleKeys binnacleKeys);

    void remove(BinnacleKeys binnacleKeys);

    BinnacleKeys find(Object id);

    List<BinnacleKeys> findAll();

    List<BinnacleKeys> findRange(int[] range);

    int count();
    
}
