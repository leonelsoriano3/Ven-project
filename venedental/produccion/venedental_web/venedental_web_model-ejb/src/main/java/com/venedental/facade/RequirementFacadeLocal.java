/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Requirement;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author AKDESK12
 */
@Local
public interface RequirementFacadeLocal {

    Requirement create(Requirement requirement);

    Requirement edit(Requirement requirement);

    void remove(Requirement requirement);

    Requirement find(Object id);
    
    List<Requirement> findAll();
    
    List<Requirement> findAllByReimbursement(int reimbursement_id);    

    List<Requirement> findRange(int[] range);

    int count();
    
}
