/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.planTreatmentKey;

import java.util.Date;

/**
 *
 * @author AKDESKDEV90
 */
public class UpdatePlanTreatmentKeyStatusInput {
    
    Integer idPlanTreatmentKey;
    Integer idStatus;
    Integer idUser;
    Date dateReception;
    Date dateEjecution;
    Integer idScale;

    public UpdatePlanTreatmentKeyStatusInput(Integer idPlanTreatmentKey, Integer idStatus, Integer idUser, Date dateReception) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.idStatus = idStatus;
        this.idUser = idUser;
        this.dateReception = dateReception;
    } 

    public UpdatePlanTreatmentKeyStatusInput(Integer idPlanTreatmentKey, Integer idStatus, Integer idUser, Date dateReception, Date dateEjecution, Integer idScale) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
        this.idStatus = idStatus;
        this.idUser = idUser;
        this.dateReception = dateReception;
        this.dateEjecution = dateEjecution;
        this.idScale = idScale;
    }
    
    public Date getDateEjecution() {
        return dateEjecution;
    }

    public void setDateEjecution(Date dateEjecution) {
        this.dateEjecution = dateEjecution;
    }

    public Integer getIdScale() {
        return idScale;
    }

    public void setIdScale(Integer idScale) {
        this.idScale = idScale;
    }
    
    public Integer getIdPlanTreatmentKey() {
        return idPlanTreatmentKey;
    }

    public void setIdPlanTreatmentKey(Integer idPlanTreatmentKey) {
        this.idPlanTreatmentKey = idPlanTreatmentKey;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }    

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }    
}
