package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "binnacle_reimbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BinnacleReimbursement.findAll", query = "SELECT r FROM BinnacleReimbursement r"),
    @NamedQuery(name = "BinnacleReimbursement.findAllByReimbursement", query = "SELECT r FROM BinnacleReimbursement r WHERE r.reimbursementId.id = :reimbursementId"),
    @NamedQuery(name = "BinnacleReimbursement.findById", query = "SELECT r FROM BinnacleReimbursement r WHERE r.id = :id")
})
public class BinnacleReimbursement implements Serializable {
    
    private static final long serialVersionUID = 1L;
     
    /* Attributes */
    
    // -> ID Binnacle Reimbursement
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    // -> Reimbursement ID - Identificador del Reembolso
    @JoinColumn(name = "reimbursement_id", referencedColumnName = "id")
    @ManyToOne(optional=false)
    private Reimbursement reimbursementId;
    
    // ->StatusReimbursement ID - Identificador de Status de reembolso
    @JoinColumn(name="status_reimbursement_id", referencedColumnName="id")
    @ManyToOne(optional=false)
    private StatusReimbursement statusReimbursementId;
    
    // --> Date - Fecha de Accion
    @Basic(optional=false)
    @NotNull
    @Column(name="date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    // --> User ID - Usuario Emisor
    @Column(name = "users_id")
    private Integer userId;
    
    // --> Transient    
    @Transient
    private String issuerUser;
    

    /* Constructs */

    public BinnacleReimbursement() {
    }

    public BinnacleReimbursement(Integer id) {
        this.id = id;
    }
    
    /* Getter and Setter */
    
    public Integer getId() {    
        return id;
    }
    
    public void setId(Integer id) {    
        this.id = id;
    }

    public StatusReimbursement getStatusReimbursementId() {
        return statusReimbursementId;
    }

    public void setStatusReimbursementId(StatusReimbursement statusReimbursementId) {
        this.statusReimbursementId = statusReimbursementId;
    }

    public Reimbursement getReimbursementId() {
        return reimbursementId;
    }

    public void setReimbursementId(Reimbursement reimbursementId) {
        this.reimbursementId = reimbursementId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getIssuerUser() {
        return issuerUser;
    }

    public void setIssuerUser(String issuerUser) {
        this.issuerUser = issuerUser;
    }
        
    /* Methods */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BinnacleReimbursement)) {
            return false;
        }
        BinnacleReimbursement other = (BinnacleReimbursement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.BinnacleReimbursement[ id=" + id + " ]";
    }
}