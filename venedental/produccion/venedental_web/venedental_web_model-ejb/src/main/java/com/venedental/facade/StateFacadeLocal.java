/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.State;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Mosquera
 */
@Local
public interface StateFacadeLocal {

    State create(State state);

    State edit(State state);

    void remove(State state);

    State find(Object id);

    List<State> findAll();

    List<State> findRange(int[] range);

    int count();
    
}
