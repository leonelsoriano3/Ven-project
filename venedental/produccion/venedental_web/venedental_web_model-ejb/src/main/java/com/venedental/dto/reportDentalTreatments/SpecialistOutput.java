package com.venedental.dto.reportDentalTreatments;

/**
 * Created by akdesk10 on 29/06/16.
 */
public class SpecialistOutput {


    private Integer id;
    private String name;
    private String cadrId;
    private String address;
    private String phone;
    private String specialties;
    private String consultingRoom;
    private Integer idSpeciality;
    private String specialityName;
    private String rif;

    public SpecialistOutput() {
        this.id = 0;
        this.name = "";
        this.cadrId = "";
        this.address = "";
        this.phone = "";
        this.specialties = "";
        this.consultingRoom = "";
        this.idSpeciality = 0;
        this.specialityName = "";

    }

    public Integer getId() {return id;}

    public void setId(Integer id) {this.id = id; }

    public String getSpecialties() {return specialties;  }

    public void setSpecialties(String specialties) {this.specialties = specialties; }

    public String getCadrId() {return cadrId;}

    public void setCadrId(String cadrId) {this.cadrId = cadrId;}

    public String getPhone() {return phone;}

    public void setPhone(String phone) {this.phone = phone;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}

    public String getConsultingRoom() {return consultingRoom;}

    public void setConsultingRoom(String consultingRoom) {this.consultingRoom = consultingRoom;}

    public Integer getIdSpeciality() {return idSpeciality; }

    public void setIdSpeciality(Integer idSpeciality) {this.idSpeciality = idSpeciality;}

    public String getSpecialityName() {return specialityName;}

    public void setSpecialityName(String specialityName) { this.specialityName = specialityName; }

    public String getRif() {return rif;}

    public void setRif(String rif) {this.rif = rif;}
}
