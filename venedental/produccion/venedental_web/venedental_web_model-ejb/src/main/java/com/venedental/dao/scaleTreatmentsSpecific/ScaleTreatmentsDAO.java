/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.scaleTreatmentsSpecific;


import com.venedental.dao.AbstractCommandDAO;
import com.venedental.dto.scaleTreatmentsSpecific.ScaleTreatmentsInput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author usuario
 */
public class ScaleTreatmentsDAO extends AbstractCommandDAO<ScaleTreatmentsInput> {

    public ScaleTreatmentsDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SCALETREATME_I_002", 5);
    }

    @Override
    public void prepareInput(ScaleTreatmentsInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdTreatment());
        statement.setDouble(2, input.getBaremo());
        statement.setString(3,input.getReceptionDate());
        statement.setInt(4, input.getIdSpecialist());
        statement.setInt(5, input.getIdUser());
    }

}
