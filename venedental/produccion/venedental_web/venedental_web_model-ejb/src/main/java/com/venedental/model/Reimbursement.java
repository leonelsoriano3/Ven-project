package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "reimbursement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reimbursement.findAll", query = "SELECT r FROM Reimbursement r"),
    @NamedQuery(name = "Reimbursement.findAllByStatus", query = "SELECT r FROM Reimbursement r WHERE r.statusReimbursmentId.id = :statusReimbursmentId ORDER BY R.date_request DESC"),
    @NamedQuery(name = "Reimbursement.findById", query = "SELECT r FROM Reimbursement r WHERE r.id = :id"),
    @NamedQuery(name = "Reimbursement.findByTwoStatus", query = "SELECT r FROM Reimbursement r WHERE r.statusReimbursmentId.id = :statusReimbursmentId OR r.statusReimbursmentId.id = :statusReimbursmentIdTwo ORDER BY R.date_request DESC"),
    @NamedQuery(name = "Reimbursement.findByStatusTransfer", query = "SELECT r FROM Reimbursement r WHERE r.statusReimbursmentId.id = :statusReimbursmentId  ORDER BY R.date_transfer DESC"),
    @NamedQuery(name = "Reimbursement.findByThreeStatus", query = "SELECT r FROM Reimbursement r WHERE r.statusReimbursmentId.id = :statusReimbursmentId OR r.statusReimbursmentId.id = :statusReimbursmentIdTwo OR r.statusReimbursmentId.id = :statusReimbursmentIdThree"),
    @NamedQuery(name = "Reimbursement.findByPatientsId", query = "SELECT r FROM Reimbursement r WHERE r.patientsId = :patientsId and r.statusReimbursmentId not in (2,3)"),
    @NamedQuery(name = "Reimbursement.findByNumberRequest", query = "SELECT r FROM Reimbursement r WHERE r.numberRequest = :numberRequest")
})
public class Reimbursement implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Attributes
     */
    // Column ID
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    // Column  NUMBER - REQUEST 
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "number_request")
    private String numberRequest;

    // Column DATE - REQUEST
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_request")
    @Temporal(TemporalType.DATE)
    private Date date_request;

    // Column Number - Transfer
    @Size(max = 50)
    @Column(name = "number_transfer")
    private String number_transfer;

    // Column Date Transfer
    @Column(name = "date_transfer")
    @Temporal(TemporalType.DATE)
    private Date date_transfer;

    // Column Plans ID - Referenciada a Plans Patients
    // Column Banks Patients
    @JoinColumn(name = "banks_patients_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BanksPatients banksPatientsId;

    // Columna Company - Empresa donde Labora el Paciente
    @Size(max = 100)
    @Column(name = "company")
    private String company;

    // Columna Concept - Observaciones Generales en la orden
    @Size(max = 100)
    @Column(name = "concept")
    private String concept;

    // Invoice Number - Numero de Factura
    @Size(max = 50)
    @Column(name = "invoice_number")
    private String invoice_number;

    // Invoice Amount - Monto de la Factura  
    @Column(name = "invoice_amount")
    private double amount;

    @Column(name = "monto_remb_pagar")
    private Double monto_remb_pagar;

    @Column(name = "obser_monto_pagar")
    private String obser_monto_pagar;

    // Invoice Date - Fecha de Emision Factura
    @Column(name = "invoice_date")
    @Temporal(TemporalType.DATE)
    private Date invoice_date;

    // Response Email - Correo de Respuesta
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "response_email")
    private String response_email;

    // Response Phone - Telefono de Respuesta
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "response_phone")
    private String response_phone;

    // Response Cellphone - Celular de Respuesta
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "response_cellphone")
    private String response_cellphone;

    // Status Reimbursement - Estado de la Solicitud
    @JoinColumn(name = "status_reimbursement_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusReimbursement statusReimbursmentId;

    // --> Lists
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reimbursementId")
    private List<Requirement> requirementList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reimbursementId")
    private List<BinnacleReimbursement> binnacleReimbursementList;

    //Bi-direccional many-to-one association to PlansPatients
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumns({
        @JoinColumn(name = "plans_id", referencedColumnName = "plans_id", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "patients_id", referencedColumnName = "patients_id", nullable = false, insertable = false, updatable = false)})
    private PlansPatients plansPatientsId;

    @JoinColumn(name = "plans_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Plans plansId;

    @JoinColumn(name = "patients_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Patients patientsId;

    /* Atributes Transient */
    @Transient
    private String completeNameTitular;

    @Transient
    private Integer indentityNumberTitular;

    @Transient
    private String completeNameBeneficiary;

    @Transient
    private String accountNumberSubTring;

    /* Constructs */
    public Reimbursement() {
    }

    public Reimbursement(Integer id) {
        this.id = id;
    }

    public Reimbursement(Integer id, String numberRequest, Date date_request, String number_transfer, Date date_transfer,
            BanksPatients banksPatientsId, String company, String concept, String invoice_number, double amount,
            Date invoice_date, String response_email, String response_phone, String response_cellphone,
            StatusReimbursement statusReimbursmentId, List<Requirement> requirementList,
            List<BinnacleReimbursement> binnacleReimbursementList, PlansPatients plansPatientsId, Plans plansId, Patients patientsId,
            String completeNameTitular, Integer indentityNumberTitular, String completeNameBeneficiary,
            String accountNumberSubTring, double monto_remb_pagar, String obser_monto_pagar) {

        this.id = id;
        this.numberRequest = numberRequest;
        this.date_request = date_request;
        this.number_transfer = number_transfer;
        this.date_transfer = date_transfer;
        this.banksPatientsId = banksPatientsId;
        this.company = company;
        this.concept = concept;
        this.invoice_number = invoice_number;
        this.amount = amount;
        this.invoice_date = invoice_date;
        this.response_email = response_email;
        this.response_phone = response_phone;
        this.response_cellphone = response_cellphone;
        this.statusReimbursmentId = statusReimbursmentId;
        this.requirementList = requirementList;
        this.binnacleReimbursementList = binnacleReimbursementList;
        this.plansPatientsId = plansPatientsId;
        this.plansId = plansId;
        this.patientsId = patientsId;
        this.completeNameTitular = completeNameTitular;
        this.indentityNumberTitular = indentityNumberTitular;
        this.completeNameBeneficiary = completeNameBeneficiary;
        this.accountNumberSubTring = accountNumberSubTring;
        this.monto_remb_pagar = monto_remb_pagar;
        this.obser_monto_pagar = obser_monto_pagar;
    }

    /* Getter and Setter */
    public Double getMonto_remb_pagar() {
        return monto_remb_pagar;
    }

    public void setMonto_remb_pagar(Double monto_remb_pagar) {
        this.monto_remb_pagar = monto_remb_pagar;
    }

    public String getObser_monto_pagar() {
        return obser_monto_pagar;
    }

    public void setObser_monto_pagar(String obser_monto_pagar) {
        this.obser_monto_pagar = obser_monto_pagar;
    }

    @XmlTransient
    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public void setRequirementList(List<Requirement> requirementList) {
        this.requirementList = requirementList;
    }

    public Plans getPlansId() {
        return plansId;
    }

    public void setPlansId(Plans plansId) {
        this.plansId = plansId;
    }

    public Patients getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(Patients patientsId) {
        this.patientsId = patientsId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PlansPatients getPlansPatientsId() {
        return plansPatientsId;
    }

    public List<BinnacleReimbursement> getBinnacleReimbursementList() {
        return binnacleReimbursementList;
    }

    public void setBinnacleReimbursementList(List<BinnacleReimbursement> binnacleReimbursementList) {
        this.binnacleReimbursementList = binnacleReimbursementList;
    }

    public String getResponse_cellphone() {
        return response_cellphone;
    }

    public void setResponse_cellphone(String response_cellphone) {
        this.response_cellphone = response_cellphone;
    }

    public void setPlansPatientsId(PlansPatients plansPatientsId) {
        this.plansPatientsId = plansPatientsId;
    }

    public String getNumberRequest() {
        return numberRequest;
    }

    public void setNumberRequest(String numberRequest) {
        this.numberRequest = numberRequest;
    }

    public Date getDate_request() {
        return date_request;
    }

    public void setDate_request(Date date_request) {
        this.date_request = date_request;
    }

    public String getNumber_transfer() {
        return number_transfer;
    }

    public void setNumber_transfer(String number_transfer) {
        this.number_transfer = number_transfer;
    }

    public Date getDate_transfer() {
        return date_transfer;
    }

    public void setDate_transfer(Date date_transfer) {
        this.date_transfer = date_transfer;
    }

    public BanksPatients getBanksPatientsId() {
        return banksPatientsId;
    }

    public void setBanks_patients_id(BanksPatients banksPatientsId) {
        this.banksPatientsId = banksPatientsId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(Date invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getResponse_email() {
        return response_email;
    }

    public void setResponse_email(String response_email) {
        this.response_email = response_email;
    }

    public String getResponse_phone() {
        return response_phone;
    }

    public void setResponse_phone(String response_phone) {
        this.response_phone = response_phone;
    }

    public StatusReimbursement getStatusReimbursmentId() {
        return statusReimbursmentId;
    }

    public void setStatusReimbursmentId(StatusReimbursement statusReimbursmentId) {
        this.statusReimbursmentId = statusReimbursmentId;
    }

    public String getCompleteNameTitular() {
        return completeNameTitular;
    }

    public void setCompleteNameTitular(String completeNameTitular) {
        this.completeNameTitular = completeNameTitular;
    }

    public Integer getIndentityNumberTitular() {
        return indentityNumberTitular;
    }

    public void setIndentityNumberTitular(Integer indentityNumberTitular) {
        this.indentityNumberTitular = indentityNumberTitular;
    }

    public String getCompleteNameBeneficiary() {
        return completeNameBeneficiary;
    }

    public void setCompleteNameBeneficiary(String completeNameBeneficiary) {
        this.completeNameBeneficiary = completeNameBeneficiary;
    }

    public String getAccountNumberSubTring() {
        return accountNumberSubTring;
    }

    public void setAccountNumberSubTring(String accountNumberSubTring) {
        this.accountNumberSubTring = accountNumberSubTring;
    }

    /* Methods */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reimbursement)) {
            return false;
        }
        Reimbursement other = (Reimbursement) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.venedental.model.Reimbursement[ id=" + id + " ]";
    }
}
