/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.facade.store_procedure;

import com.venedental.dto.ReadPlansReportInput;
import com.venedental.dto.ReadPlansReportOutput;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ARKDEV16
 */
@Local
public interface ReadPlansFacadeLocal {
    
    public List<ReadPlansReportOutput> execute(ReadPlansReportInput input);
    
}
