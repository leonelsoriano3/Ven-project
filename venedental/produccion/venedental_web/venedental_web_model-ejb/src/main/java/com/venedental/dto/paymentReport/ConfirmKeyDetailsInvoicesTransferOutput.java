/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

/**
 *
 * @author root
 */
public class ConfirmKeyDetailsInvoicesTransferOutput {
    private Integer idMonth;
    private String nameMonth;
    private Integer idSpecialist;
    private String nameSpecialist;
    private Integer idKeyPatient;
    private String nameKeyPatient;
    private Integer idTreatment;
    private String nameTreatment;
    private Integer idPropertie;
    private String namePropertie;
    private Double amountTotal;
    private String observations;
     private Double totalAmountPaid;

    public ConfirmKeyDetailsInvoicesTransferOutput() {
    }

    public Integer getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Integer idMonth) {
        this.idMonth = idMonth;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public Integer getIdKeyPatient() {
        return idKeyPatient;
    }

    public void setIdKeyPatient(Integer idKeyPatient) {
        this.idKeyPatient = idKeyPatient;
    }

    public String getNameKeyPatient() {
        return nameKeyPatient;
    }

    public void setNameKeyPatient(String nameKeyPatient) {
        this.nameKeyPatient = nameKeyPatient;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public Integer getIdPropertie() {
        return idPropertie;
    }

    public void setIdPropertie(Integer idPropertie) {
        this.idPropertie = idPropertie;
    }

    public String getNamePropertie() {
        return namePropertie;
    }

    public void setNamePropertie(String namePropertie) {
        this.namePropertie = namePropertie;
    }

    public Double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Double getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(Double totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }
    
    
}
