/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConfirmKeyDetailsOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class FindValidationNumberControlInvoicesKeysDAO extends AbstractQueryDAO<Integer, Boolean> {

    public FindValidationNumberControlInvoicesKeysDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_G_003", 2);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        statement.registerOutParameter(2, java.sql.Types.BOOLEAN);
    }

    @Override
    public Boolean prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Boolean retieneISLR = false;
        
        retieneISLR = statement.getBoolean(2);
        
        return retieneISLR;
    }
}
