package com.venedental.dto.analysisKeys;

import java.sql.Date;

/**
 * Created by akdeskdev90 on 17/03/16.
 */
public class ConsultPaymentReportInput {

    private Integer idSpecialist;
    private Integer idStatus;
    private Integer idMonth;
    private Date dateReception;

    public ConsultPaymentReportInput() {
    }

    public ConsultPaymentReportInput(Integer idSpecialist, Integer idStatus, Integer idMonth, Date dateReception) {
        this.idSpecialist = idSpecialist;
        this.idStatus = idStatus;
        this.idMonth = idMonth;
        this.dateReception = dateReception;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdMonth() {
        return idMonth;
    }

    public void setIdMonth(Integer idMonth) {
        this.idMonth = idMonth;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

}
