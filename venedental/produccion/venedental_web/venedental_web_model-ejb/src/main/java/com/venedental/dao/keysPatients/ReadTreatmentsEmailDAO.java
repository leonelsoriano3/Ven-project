/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.ReadTreatmentsEmailOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ReadTreatmentsEmailDAO extends AbstractQueryDAO<Integer, ReadTreatmentsEmailOutput> {

    public ReadTreatmentsEmailDAO(DataSource dataSource) {
        super(dataSource, "SP_M_GCL_LeerTratamientosCorreo", 1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }

    @Override
    public ReadTreatmentsEmailOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ReadTreatmentsEmailOutput readTreatmentsEmailOutput = new ReadTreatmentsEmailOutput();
        readTreatmentsEmailOutput.setIdColumn(rs.getInt("NUMERO_COLUMNA"));
        readTreatmentsEmailOutput.setIdLine(rs.getInt("ID_LINEA"));
        readTreatmentsEmailOutput.setLineTreatments(rs.getString("TRATAMIENTO_LINEA"));
        return readTreatmentsEmailOutput;
    }


}