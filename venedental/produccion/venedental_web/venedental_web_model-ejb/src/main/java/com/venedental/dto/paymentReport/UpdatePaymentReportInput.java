/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import java.sql.Date;

/**
 *
 * @author root
 */
public class UpdatePaymentReportInput {
    int idPaymentReport;
    String numberPaymentReport;
    String billNumber;
    java.sql.Date date;
    String controlNumber;
    String transferNumber;
    int idAccountNumber;
    String accountNumber;
    double amount;
    double auditedAmount;
    int idUser;
    
    public UpdatePaymentReportInput (int idPaymentReport, String numberPaymentReport, String billNumber, Date date, String controlNumber,
            int idUser, String transferNumber, double amount, double auditedAmount){
        this.idPaymentReport = idPaymentReport;
        this.date = date;
        this.numberPaymentReport = numberPaymentReport;
        this.billNumber = billNumber;
        this.controlNumber = controlNumber;        
        this.transferNumber = transferNumber;        
        this.amount = amount;
        this.auditedAmount = auditedAmount;
        this.idUser = idUser;
    }

    public UpdatePaymentReportInput(int idPaymentReport, java.sql.Date date, String billNumber, String controlNumber,int idUser) {
        this.idPaymentReport = idPaymentReport;
        this.date = date;
        this.billNumber = billNumber;
        this.controlNumber = controlNumber;
        this.idUser = idUser;
    }
    
    public UpdatePaymentReportInput(int idUser, String numberPaymentReport, Date date, String transferNumber, String accountNumber) {
        this.idUser = idUser;
        this.numberPaymentReport = numberPaymentReport;
        this.date = date;
        this.transferNumber = transferNumber;
        this.accountNumber = accountNumber;
    }
    
    /* Construct and parameters update data register transfer */
    
    public UpdatePaymentReportInput(int idPaymentReport,Date date, String transferNumber,int idAccountNumber) {
       this.idPaymentReport = idPaymentReport;
       this.date = date;
       this.transferNumber = transferNumber;
       this.idAccountNumber = idAccountNumber;
    }

    public String getNumberPaymentReport() {
        return numberPaymentReport;
    }

    public void setNumberPaymentReport(String numberPaymentReport) {
        this.numberPaymentReport = numberPaymentReport;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public String getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAuditedAmount() {
        return auditedAmount;
    }

    public void setAuditedAmount(double auditedAmount) {
        this.auditedAmount = auditedAmount;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getIdPaymentReport() {
        return idPaymentReport;
    }

    public void setIdPaymentReport(int idPaymentReport) {
        this.idPaymentReport = idPaymentReport;
    }

    public int getIdAccountNumber() {
        return idAccountNumber;
    }

    public void setIdAccountNumber(int idAccountNumber) {
        this.idAccountNumber = idAccountNumber;
    }
    
    
}
