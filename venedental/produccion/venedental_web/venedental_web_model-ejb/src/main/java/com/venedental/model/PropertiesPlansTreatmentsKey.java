/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ARIANA
 */
@Entity
@Table(name = "properties_plans_treatments_key")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.findAll", query = "SELECT p FROM PropertiesPlansTreatmentsKey p"),
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.findById", query = "SELECT p FROM PropertiesPlansTreatmentsKey p WHERE p.id = :id"),
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.deleteByPlansTreatmentsKeys", query = "DELETE  FROM PropertiesPlansTreatmentsKey p WHERE p.plansTreatmentsKeys.id = :plans_treatments_keys"),
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.findByPlansTreatmentsKeys", query = "SELECT p FROM PropertiesPlansTreatmentsKey p WHERE p.plansTreatmentsKeys.id = :plans_treatments_keys"),
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.findByKeysAndStatus", query = "SELECT p FROM PropertiesPlansTreatmentsKey p WHERE p.plansTreatmentsKeys.keysId.id = :keysId AND p.idStatus.id != :idStatus"),
    @NamedQuery(name = "PropertiesPlansTreatmentsKey.findByKeys", query = "SELECT p FROM PropertiesPlansTreatmentsKey p WHERE p.plansTreatmentsKeys.keysId.id = :keysId")})
public class PropertiesPlansTreatmentsKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "observation")
    private String observation;
    
   
    @Column(name = "date_properties_plans_treatments_keys")
     @Temporal(TemporalType.TIMESTAMP)
    private Date date_properties;
    

    @JoinColumn(name = "id_status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StatusAnalysisKeys idStatus;
    
   /* @JoinColumn(name = "id_status", referencedColumnName = "id")
    private Integer id_status;*/

    @JoinColumn(name = "id_scale_properties_treatments", referencedColumnName = "id")
    @ManyToOne
    private ScalePropertiesTreatments idScalePropertiesTreatments;

    @JoinColumn(name = "properties_treatments_id", referencedColumnName = "id")
    @ManyToOne
    private PropertiesTreatments propertiesTreatments;

    @JoinColumn(name = "plans_treatments_keys", referencedColumnName = "id")
    @ManyToOne
    private PlansTreatmentsKeys plansTreatmentsKeys;

    @OneToMany(mappedBy = "idPropertiesPlansTreatmentsKey")
    private List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList;
    
    @ManyToMany(mappedBy = "propertiesPlansTreatmentsKeysList")
    private List<PaymentReport> paymentReportList;
    
    

    @Transient
    private Integer idTreatments;

    @Transient
    private Integer idPlansTreatmentsKey;

    @Transient
    private Integer idPropertiesTreatments;

    @Transient
    private Integer idScaleTretments;

    @Transient
    private Integer idUser;

    @Transient
    private String nameTreatments;

    @Transient
    private Integer idStatusProTreatments;

    @Transient
    private String nameStatusProTreatments;

    @Transient
    private Date dateTreatments;

    @Transient
    private String nameProperties;
    
    @Transient
    private Integer idScalePropertiesTreatment;
    
    @Transient
    private boolean hasGenerate;

    @Transient
    private Double price;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    
    public PropertiesPlansTreatmentsKey() {
    }

    public boolean isHasGenerate() {
        return hasGenerate;
    }

    public void setHasGenerate(boolean hasGenerate) {
        this.hasGenerate = hasGenerate;
    }   
    
    public PropertiesPlansTreatmentsKey(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PropertiesTreatments getPropertiesTreatments() {
        return propertiesTreatments;
    }

    public void setPropertiesTreatments(PropertiesTreatments propertiesTreatments) {
        this.propertiesTreatments = propertiesTreatments;
    }

    public PlansTreatmentsKeys getPlansTreatmentsKeys() {
        return plansTreatmentsKeys;
    }

    public void setPlansTreatmentsKeys(PlansTreatmentsKeys plansTreatmentsKeys) {
        this.plansTreatmentsKeys = plansTreatmentsKeys;
    }

    public Integer getIdTreatments() {
        return idTreatments;
    }

    public void setIdTreatments(Integer idTreatments) {
        this.idTreatments = idTreatments;
    }

    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }

    public Integer getIdStatusProTreatments() {
        return idStatusProTreatments;
    }

    public void setIdStatusProTreatments(Integer idStatusProTreatments) {
        this.idStatusProTreatments = idStatusProTreatments;
    }

    public String getNameStatusProTreatments() {
        return nameStatusProTreatments;
    }

    public void setNameStatusProTreatments(String nameStatusProTreatments) {
        this.nameStatusProTreatments = nameStatusProTreatments;
    }

    public Date getDateTreatments() {
        return dateTreatments;
    }

    public void setDateTreatments(Date dateTreatments) {
        this.dateTreatments = dateTreatments;
    }

    public String getNameProperties() {
        return nameProperties;
    }

    public void setNameProperties(String nameProperties) {
        this.nameProperties = nameProperties;
    }

    public Integer getIdPlansTreatmentsKey() {
        return idPlansTreatmentsKey;
    }

    public void setIdPlansTreatmentsKey(Integer idPlansTreatmentsKey) {
        this.idPlansTreatmentsKey = idPlansTreatmentsKey;
    }

    public Integer getIdPropertiesTreatments() {
        return idPropertiesTreatments;
    }

    public void setIdPropertiesTreatments(Integer idPropertiesTreatments) {
        this.idPropertiesTreatments = idPropertiesTreatments;
    }

    public Integer getIdScaleTretments() {
        return idScaleTretments;
    }

    public void setIdScaleTretments(Integer idScaleTretments) {
        this.idScaleTretments = idScaleTretments;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public StatusAnalysisKeys getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(StatusAnalysisKeys idStatus) {
        this.idStatus = idStatus;
    }
    public ScalePropertiesTreatments getIdScalePropertiesTreatments() {
        return idScalePropertiesTreatments;
    }

    public void setIdScalePropertiesTreatments(ScalePropertiesTreatments idScalePropertiesTreatments) {
        this.idScalePropertiesTreatments = idScalePropertiesTreatments;
    }

    public Integer getIdScalePropertiesTreatment() {
        return idScalePropertiesTreatment;
    }

    public void setIdScalePropertiesTreatment(Integer idScalePropertiesTreatment) {
        this.idScalePropertiesTreatment = idScalePropertiesTreatment;
    }

    public Date getDate_properties() {
        return date_properties;
    }

    public void setDate_properties(Date date_properties) {
        this.date_properties = date_properties;
    }

    @XmlTransient
    public List<BinnaclePropertiesTreatmentsKeys> getBinnaclePropertiesTreatmentsKeysList() {
        return binnaclePropertiesTreatmentsKeysList;
    }

    public void setBinnaclePropertiesTreatmentsKeysList(List<BinnaclePropertiesTreatmentsKeys> binnaclePropertiesTreatmentsKeysList) {
        this.binnaclePropertiesTreatmentsKeysList = binnaclePropertiesTreatmentsKeysList;
    }
    
    @XmlTransient
    public List<PaymentReport> getPaymentReportList() {
        return paymentReportList;
    }

    public void setPaymentReportList(List<PaymentReport> paymentReportList) {
        this.paymentReportList = paymentReportList;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropertiesPlansTreatmentsKey)) {
            return false;
        }
        PropertiesPlansTreatmentsKey other = (PropertiesPlansTreatmentsKey) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "com.venedental.model.PropertiesTreatmentsKeys[ id=" + id + " ]";
    }

}
