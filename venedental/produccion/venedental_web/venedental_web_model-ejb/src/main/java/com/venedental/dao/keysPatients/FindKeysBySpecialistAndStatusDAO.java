/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.keysPatients;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.keyPatients.KeysBySpecialistAndStatusInput;
import com.venedental.model.KeysPatients;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindKeysBySpecialistAndStatusDAO extends AbstractQueryDAO<KeysBySpecialistAndStatusInput, KeysPatients> {
    
     public FindKeysBySpecialistAndStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_KEYSPATIENTS_G_004", 5);
    }

    @Override
    public void prepareInput(KeysBySpecialistAndStatusInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setInt(2, input.getIdStatus());
        statement.setInt(3, input.getIdStatus2());
        statement.setInt(4, input.getIdStatus3());
        statement.setInt(5, input.getIdUser());
    }

    @Override
    public KeysPatients prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        KeysPatients keysPatients = new KeysPatients();
        keysPatients.setId(rs.getInt("id"));
        keysPatients.setNumber(rs.getString("number"));
        keysPatients.setApplicationDate(rs.getDate("application_date"));
        keysPatients.setIdPatient(rs.getInt("patients_id"));
        keysPatients.setIdentityNumberPatient(rs.getString("cedula_paciente"));
        keysPatients.setNamePatient(rs.getString("nombre_paciente"));
        return keysPatients;
    }
    
    
    
}
