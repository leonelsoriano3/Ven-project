/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ReportAuditedAnyStateInput;
import com.venedental.dto.analysisKeys.ReportAuditedAnyStateOutPut;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 *
 */
public class ReportAuditedAnyStateDAO extends AbstractQueryDAO<ReportAuditedAnyStateInput, ReportAuditedAnyStateOutPut> {

    public ReportAuditedAnyStateDAO(DataSource dataSource) {
            super(dataSource, "SP_T_PAYMENTREPOR_G_008", 3);
    }

    @Override
    public void prepareInput(ReportAuditedAnyStateInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getStatus());
        statement.setDate(2, input.getStartdate());
        statement.setDate(3, input.getEndDate());
    }


    @Override
    public ReportAuditedAnyStateOutPut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ReportAuditedAnyStateOutPut output = new ReportAuditedAnyStateOutPut();

        output.setPaidmentReportId(rs.getInt("id_reporte"));
        output.setReportNumber(rs.getString("numero_reporte"));
        output.setDateReport(rs.getString("fecha_reporte"));
        output.setSpecialistName(rs.getString("nombre_especialista"));
        output.setIdCardSpecialist(rs.getString("cedula_rif_especialista"));
        output.setBankName(rs.getString("nombre_banco"));
        output.setNumberAccount(rs.getString("numero_cuenta"));
        output.setIdCardAccunt(rs.getString("cedula_rif_cuenta"));
        output.setNumberInvoice(rs.getString("numero_factura"));
        output.setNumberControl(rs.getString("numero_control"));
        output.setDateInvoice(rs.getDate("fecha_factura"));
        output.setAmountInvoice(rs.getDouble("monto_factura"));
        output.setPercentRetention(rs.getDouble("porcentaje_retencion"));
        output.setAmountRetentionISLR(rs.getDouble("monto_retencion_islr"));
        output.setAmountNet(rs.getDouble("monto_neto"));
        output.setDateTransfer(rs.getDate("fecha_transferencia"));
        output.setNumberTransfer(rs.getString("numero_transferencia"));

        return output;
    }

}
