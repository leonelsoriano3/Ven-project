/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author AKDESKDEV90
 */
@Entity
@Table(name = "payment_report")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentReport.findAll", query = "SELECT p FROM PaymentReport p"),
    @NamedQuery(name = "PaymentReport.findById", query = "SELECT p FROM PaymentReport p WHERE p.id = :id"),
    @NamedQuery(name = "PaymentReport.findByNumber", query = "SELECT p FROM PaymentReport p WHERE p.number = :number"),
    @NamedQuery(name = "PaymentReport.findByDate", query = "SELECT p FROM PaymentReport p WHERE p.date = :date"),
    @NamedQuery(name = "PaymentReport.findByBillNumber", query = "SELECT p FROM PaymentReport p WHERE p.billNumber = :billNumber"),
    @NamedQuery(name = "PaymentReport.findByControlNumber", query = "SELECT p FROM PaymentReport p WHERE p.controlNumber = :controlNumber"),
    @NamedQuery(name = "PaymentReport.findByTransferNumber", query = "SELECT p FROM PaymentReport p WHERE p.transferNumber = :transferNumber"),
    @NamedQuery(name = "PaymentReport.findByAmount", query = "SELECT p FROM PaymentReport p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentReport.findByAuditedAmount", query = "SELECT p FROM PaymentReport p WHERE p.auditedAmount = :auditedAmount")})
public class PaymentReport implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "number")
    private String number;
    
    
    @JoinTable(name = "payment_report_properties", joinColumns = {
        @JoinColumn(name = "payment_report_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "properties_plans_treatments_key_id", referencedColumnName = "id")})
    
    @ManyToMany
    private List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeysList; 
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    @Size(max = 30)
    @Column(name = "bill_number")
    private String billNumber;
    
    @Size(max = 30)
    @Column(name = "control_number")
    private String controlNumber;
    
    @Size(max = 30)
    @Column(name = "transfer_number")
    private String transferNumber;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "audited_amount")
    private double auditedAmount;
    
    @JoinTable(name = "payment_report_treatments", joinColumns = {
        @JoinColumn(name = "payment_report_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "plans_treatments_keys_id", referencedColumnName = "id")})
    
    @ManyToMany
    private List<PlansTreatmentsKeys> plansTreatmentsKeysList;    
    
    public PaymentReport() {
    }

    public PaymentReport(Integer id) {
        this.id = id;
    }

    public PaymentReport(Integer id, String number, Date date, double amount, double auditedAmount) {
        this.id = id;
        this.number = number;
        this.date = date;
        this.amount = amount;
        this.auditedAmount = auditedAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public String getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAuditedAmount() {
        return auditedAmount;
    }

    public void setAuditedAmount(double auditedAmount) {
        this.auditedAmount = auditedAmount;
    }

    @XmlTransient
    public List<PlansTreatmentsKeys> getPlansTreatmentsKeysList() {
        if (this.plansTreatmentsKeysList == null) {
            this.plansTreatmentsKeysList = new ArrayList<>();
        }
        return plansTreatmentsKeysList;
    }

    public void setPlansTreatmentsKeysList(List<PlansTreatmentsKeys> plansTreatmentsKeysList) {
        this.plansTreatmentsKeysList = plansTreatmentsKeysList;
    }
    
    
    @XmlTransient
    public List<PropertiesPlansTreatmentsKey> getPropertiesPlansTreatmentsKeysList() {
        if (this.propertiesPlansTreatmentsKeysList == null) {
            this.propertiesPlansTreatmentsKeysList = new ArrayList<>();
        }
        return propertiesPlansTreatmentsKeysList;
    }

    public void setPropertiesPlansTreatmentsKeysList(List<PropertiesPlansTreatmentsKey> propertiesPlansTreatmentsKeysList) {
        this.propertiesPlansTreatmentsKeysList = propertiesPlansTreatmentsKeysList;
    }
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentReport)) {
            return false;
        }
        PaymentReport other = (PaymentReport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.PaymentReport[ id=" + id + " ]";
    }


    
}
