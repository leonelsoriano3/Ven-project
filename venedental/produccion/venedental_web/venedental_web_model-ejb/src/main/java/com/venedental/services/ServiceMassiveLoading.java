/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.massiveLoading.InsertTemporaryPatientDAO;
import com.venedental.dao.massiveLoading.MassiveUpdateDAO;
import com.venedental.dto.massiveLoading.MassiveUpdateExecution;
import com.venedental.dto.massiveLoading.TemporaryPatient;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServiceMassiveLoading {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    public void registerTemporaryPatient(TemporaryPatient temporaryPatient) throws SQLException {
        InsertTemporaryPatientDAO insertTemporaryPatientDAO = new InsertTemporaryPatientDAO(ds);
        insertTemporaryPatientDAO.execute(temporaryPatient);
        System.out.println("SERVICIO - REGISTER TEMPORARY PATIENT");
    }

    public void executeMassiveUpdate(MassiveUpdateExecution massiveUpdateExecution, Boolean disassociate, Boolean include, Boolean exclude) throws SQLException {
        MassiveUpdateDAO massiveUpdateDAO = new MassiveUpdateDAO(ds);

        massiveUpdateDAO.execute(massiveUpdateExecution);
        System.out.println("SERVICIO - EXECUTE MASSIVE UPDATE");
    }

}
