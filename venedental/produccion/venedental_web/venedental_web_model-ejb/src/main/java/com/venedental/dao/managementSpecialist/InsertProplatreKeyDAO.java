package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.InsertProplatreKeyIn;
import com.venedental.dto.managementSpecialist.InsertProplatreKeyOut;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 */
public class InsertProplatreKeyDAO extends AbstractQueryDAO<InsertProplatreKeyIn,InsertProplatreKeyOut> {


    public InsertProplatreKeyDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPLATREKEY_I_002", 9);
    }

    @Override
    public void prepareInput(InsertProplatreKeyIn input, CallableStatement statement) throws SQLException {

        statement.setInt("PI_ID_TRATAMIENTO_CLAVE",input.getIdTreatment());
        statement.setInt("PI_ID_TRATAMIENTO_PIEZA",input.getIdTreatmentPiece());

        if(input.getDate() == null){

            statement.setNull("PI_FECHA", java.sql.Types.NULL);
        }
        else{
            statement.setDate("PI_FECHA",input.getDate());
        }
        statement.setInt("PI_ID_ESTATUS",input.getIdStatus());
        statement.setInt("PI_BAREMO_TRATAMIENTO_PIEZA",input.getBaremoTreatmentPiece());
        statement.setString("PI_OBSERVACION",input.getObservation());
        statement.setInt("PI_ID_USUARIO",input.getIdUsuario());

        if(input.getDateReception() == null){
            statement.setNull("PI_FECHA_RECEPCION", Types.NULL);
        }
        else{
            statement.setDate("PI_FECHA_RECEPCION",input.getDateReception());

        }
            statement.setInt("PI_ID_TRATAMIENTO_CLAVE_DIAGNOSTICO",input.getIdTreatmentKeyDiagnosis());

    }

    @Override
    public InsertProplatreKeyOut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        InsertProplatreKeyOut insertProplatreKeyOut = new InsertProplatreKeyOut();
        insertProplatreKeyOut.setIdPieceTreatmentKey(rs.getInt("id_pieza_tratamiento_clave"));
        insertProplatreKeyOut.setIdPieceTreatmentKey(rs.getInt("id_pieza_tratamiento_clave_diagnostico"));

        return insertProplatreKeyOut;
    }
}

