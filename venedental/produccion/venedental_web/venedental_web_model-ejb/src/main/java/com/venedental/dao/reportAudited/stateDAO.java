/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.reportAudited;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAudited.StateOutputDTO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk10
 */
public class stateDAO extends AbstractQueryDAO<Integer, StateOutputDTO>{
    
    public stateDAO(DataSource dataSource) {
       
        super(dataSource,"SP_T_STATE_G_001",1);
        
      }
    
     @Override
     public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        if(input==null){
            statement.setNull(1, java.sql.Types.NULL);
        }else{
            statement.setInt(1, input);
            
        }      
       }
     
    @Override
    public StateOutputDTO prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        StateOutputDTO state = new StateOutputDTO();
        state.setIdState(rs.getInt("id_estado"));
        state.setNameState(rs.getString("nombre_estado"));
        return state;
    }
}
