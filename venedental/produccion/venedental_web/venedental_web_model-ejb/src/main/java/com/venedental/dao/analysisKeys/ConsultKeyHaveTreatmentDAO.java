package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConsultKeyHaveTreatmentOutPut;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyHaveTreatmentDAO extends AbstractQueryDAO<Integer, ConsultKeyHaveTreatmentOutPut> {

    public ConsultKeyHaveTreatmentDAO(DataSource dataSource) {
        super(dataSource, "SP_T_BUSCAR_CLAVE_PLANTTREAMKEY", 2);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
        statement.registerOutParameter(2, java.sql.Types.BOOLEAN);
    }

    @Override
    public ConsultKeyHaveTreatmentOutPut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ConsultKeyHaveTreatmentOutPut consultKeyKeyInStatusGenetaredOutPut
                = new ConsultKeyHaveTreatmentOutPut();
        consultKeyKeyInStatusGenetaredOutPut.SetContainsTreatment(statement.getBoolean(2));
        return consultKeyKeyInStatusGenetaredOutPut;


    }

}
