/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.model;

import com.venedental.model.security.Users;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
@Entity
@Table(name = "binnacle_plans_treatments_keys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BinnaclePlansTreatmentsKeys.findAll", query = "SELECT b FROM BinnaclePlansTreatmentsKeys b"),
    @NamedQuery(name = "BinnaclePlansTreatmentsKeys.findById", query = "SELECT b FROM BinnaclePlansTreatmentsKeys b WHERE b.id = :id"),
    @NamedQuery(name = "BinnaclePlansTreatmentsKeys.findByDateBinnacleInitiation", query = "SELECT b FROM BinnaclePlansTreatmentsKeys b WHERE b.dateBinnacleInitiation = :dateBinnacleInitiation"),
    @NamedQuery(name = "BinnaclePlansTreatmentsKeys.findByDateBinnacleEnd", query = "SELECT b FROM BinnaclePlansTreatmentsKeys b WHERE b.dateBinnacleEnd = :dateBinnacleEnd")})
public class BinnaclePlansTreatmentsKeys implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "date_binnacle_initiation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBinnacleInitiation;
    
    @Column(name = "date_binnacle_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBinnacleEnd;
    
    @JoinColumn(name = "id_status_plans", referencedColumnName = "id")
    @ManyToOne
    private StatusAnalysisKeys idStatusPlans;
    
    @JoinColumn(name = "id_plans_treatments_keys", referencedColumnName = "id")
    @ManyToOne
    private PlansTreatmentsKeys idPlansTreatmentsKeys;
    
    @JoinColumn(name = "id_users", referencedColumnName = "id")
    @ManyToOne
    private Users idUsers;

    public BinnaclePlansTreatmentsKeys() {
    }

    public BinnaclePlansTreatmentsKeys(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateBinnacleInitiation() {
        return dateBinnacleInitiation;
    }

    public void setDateBinnacleInitiation(Date dateBinnacleInitiation) {
        this.dateBinnacleInitiation = dateBinnacleInitiation;
    }

    public Date getDateBinnacleEnd() {
        return dateBinnacleEnd;
    }

    public void setDateBinnacleEnd(Date dateBinnacleEnd) {
        this.dateBinnacleEnd = dateBinnacleEnd;
    }

    public StatusAnalysisKeys getIdStatusPlans() {
        return idStatusPlans;
    }

    public void setIdStatusPlans(StatusAnalysisKeys idStatusPlans) {
        this.idStatusPlans = idStatusPlans;
    }

    public PlansTreatmentsKeys getIdPlansTreatmentsKeys() {
        return idPlansTreatmentsKeys;
    }

    public void setIdPlansTreatmentsKeys(PlansTreatmentsKeys idPlansTreatmentsKeys) {
        this.idPlansTreatmentsKeys = idPlansTreatmentsKeys;
    }

    public Users getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Users idUsers) {
        this.idUsers = idUsers;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BinnaclePlansTreatmentsKeys)) {
            return false;
        }
        BinnaclePlansTreatmentsKeys other = (BinnaclePlansTreatmentsKeys) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.BinnaclePlansTreatmentsKeys[ id=" + id + " ]";
    }

}
