/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.sql.Date;

/**
 *
 * @author akdesk01
 * DTO
 */
public class ConfigBaremoGeneralnput {
    private Integer idTreatment,idSpecialist,idUser;
    private Date effectiveDate;
    private Double baremo;




    public ConfigBaremoGeneralnput() {
    }

    public ConfigBaremoGeneralnput(Integer idTreatment,Integer idSpecialist,Integer idUser, Double  baremo, Date effectiveDate) {
        this.idTreatment=idTreatment;
        this.idSpecialist=idSpecialist;
        this.idUser=idUser;
        this.baremo=baremo;
        this.effectiveDate=effectiveDate;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    
    public Double getBaremo() {
        return baremo;
    }

    public void setBaremo(Double baremo) {
        this.baremo = baremo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    
}


