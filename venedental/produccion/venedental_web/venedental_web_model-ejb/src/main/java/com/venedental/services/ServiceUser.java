package com.venedental.services;

import com.venedental.dao.user.*;

import com.venedental.dto.User.InTokenDTO;
import com.venedental.dto.User.OutTokenDTO;
import com.venedental.dto.User.UserDTO;
import com.venedental.dto.createUser.UpdateUserByTokenInput;
import com.venedental.dto.createUser.UpdateUserByTokenOutput;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * Created by leonelsoriano3@gmail.com on 18/05/16.
 */
@Stateless
public class ServiceUser {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;


    /**
     * consigue el usuario dado su ide
     * @param id
     * @return
     */
    public UserDTO findUserById(Integer id){
        FindUserByIdDAO userDAO = new FindUserByIdDAO(ds);
        userDAO.execute(id);
        return  userDAO.getResult();
    }

    /**
     * actualiza el token y fecha maxima dado su id
     * @param id
     * @param token
     * @param expiredDate
     * @throws SQLException
     */
    public void updateToken(Integer id,String token,String expiredDate) throws SQLException {
        InTokenDTO input = new InTokenDTO();
        input.setUserId(id);
        input.setToken(token);
        input.setExpiredDate(expiredDate);
        UpdateTokenDAO updateTokenDAO = new UpdateTokenDAO(ds);
        updateTokenDAO.execute(input);
    }


    /**
     * buscar un token dado su id
     * @param id
     * @return
     */
    public OutTokenDTO findtoken(Integer id){
        FindTokenByIdUserDAO findTokenByIdUserDAO = new FindTokenByIdUserDAO(ds);
        findTokenByIdUserDAO.execute(id);
        return findTokenByIdUserDAO.getResult();
    }


    public UserDTO findUserByToken(String token){
        FindUserByTokenDAO findUserByIdDAO = new FindUserByTokenDAO(ds);
        findUserByIdDAO.execute(token);
        return findUserByIdDAO.getResult();
    }

    public UpdateUserByTokenOutput consumeTokenClicking(String token) throws SQLException {

        UpdateUserByTokenInput updateUserByTokenInput = new UpdateUserByTokenInput(token);
        ConsumeTokenClickingDAO consumeTokenClickingDAO = new ConsumeTokenClickingDAO(ds);
        consumeTokenClickingDAO.execute(updateUserByTokenInput);
        return consumeTokenClickingDAO.getResult();

    }
}
