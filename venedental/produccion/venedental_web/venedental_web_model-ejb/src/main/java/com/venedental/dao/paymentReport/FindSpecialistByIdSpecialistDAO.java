package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportDentalTreatments.SpecialistOutput;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FindSpecialistByIdSpecialistDAO extends AbstractQueryDAO<Integer,SpecialistOutput> {
    public FindSpecialistByIdSpecialistDAO(DataSource dataSource){
        super(dataSource,"SP_T_SPECIALISTS_G_011",1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input);
    }

    @Override
    public SpecialistOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        SpecialistOutput specialistOutput = new SpecialistOutput();
        specialistOutput.setId(rs.getInt("id_especialista"));
        specialistOutput.setCadrId(rs.getString("cedula_especialista"));
        specialistOutput.setAddress(rs.getString("direccion_especialista"));
        specialistOutput.setPhone(rs.getString("telefono_especialista"));
        specialistOutput.setSpecialties(rs.getString("especialidades"));
        specialistOutput.setConsultingRoom(rs.getString("nombre_consultorio"));
        specialistOutput.setIdSpeciality(rs.getInt("id_especialidad"));
        specialistOutput.setSpecialityName(rs.getString("nombre_especialidad"));
        specialistOutput.setName(rs.getString("nombre_especialista"));
        specialistOutput.setRif(rs.getString("rif_especialista"));

        return specialistOutput;

    }
}
