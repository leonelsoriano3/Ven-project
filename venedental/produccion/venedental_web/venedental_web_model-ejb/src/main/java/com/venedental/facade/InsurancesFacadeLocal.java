/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface InsurancesFacadeLocal {

    Insurances create(Insurances insurances);
    
    Insurances find(Object id);
    
    List<Insurances> findAll(); 
    
    List<Insurances> findById(Integer id);
    
    List<Insurances> findByName(Integer id);
    
    List<Insurances> findAllOrderByName();
    
}
