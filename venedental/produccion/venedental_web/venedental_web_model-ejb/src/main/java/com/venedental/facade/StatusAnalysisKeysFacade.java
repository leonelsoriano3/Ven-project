/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;


import com.venedental.model.StatusAnalysisKeys;
import com.venedental.model.security.Menu;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Edgar Mosquera
 */
@Stateless
public class StatusAnalysisKeysFacade extends AbstractFacade<StatusAnalysisKeys> implements StatusAnalysisKeysFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatusAnalysisKeysFacade() {
        super(StatusAnalysisKeys.class);
    }
    
    @Override
    public StatusAnalysisKeys findById(Integer id) {
        TypedQuery<StatusAnalysisKeys> query = em.createNamedQuery("StatusAnalysisKeys.findById", StatusAnalysisKeys.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
}
