/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.paymentReport;

import com.venedental.dto.analysisKeys.ConsultKeyDetailsStatusPaidOutPut;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author root
 */
public class PaymentReportOutput {
    private int idReport;    
    private String numberPaymentReport;
    private String billNumber;
    private Date billDate;
    private Date reportDate;
    private String controlNumber;
    private String transferNumber;
    private Date transferDate;
    private int idBankAccount;
    private String bankAccountNumber;
    private String bankName;
    private int idBank;
    private double amount;
    private double auditedAmount;
    private double amountTotal;
    private int idUser;
    private Integer idSpecialist;
    private String nameSpecialist;
    private String numberRetentionISLR;

    public PaymentReportOutput() {
    }

        
    public String getNumberPaymentReport() {
        return numberPaymentReport;
    }

    public void setNumberPaymentReport(String numberPaymentReport) {
        this.numberPaymentReport = numberPaymentReport;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public String getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAuditedAmount() {
        return auditedAmount;
    }

    public void setAuditedAmount(double auditedAmount) {
        this.auditedAmount = auditedAmount;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdReport() {
        return idReport;
    }

    public void setIdReport(int idReport) {
        this.idReport = idReport;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public int getIdBankAccount() {
        return idBankAccount;
    }

    public void setIdBankAccount(int idBankAccount) {
        this.idBankAccount = idBankAccount;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getIdBank() {
        return idBank;
    }

    public void setIdBank(int idBank) {
        this.idBank = idBank;
    }

    public double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(double amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    
    public String getNameSpecialist() {
        return nameSpecialist;
    }

    public void setNameSpecialist(String nameSpecialist) {
        this.nameSpecialist = nameSpecialist;
    }

    public String getNumberRetentionISLR() {
        return numberRetentionISLR;
    }

    public void setNumberRetentionISLR(String numberRetentionISLR) {
        this.numberRetentionISLR = numberRetentionISLR;
    }

////    confirmKeyOutput.setDateExecution(rs.getDate("fecha_ejecucion"));
////    confirmKeyOutput.setIdMonthExecution(rs.getInt("id_mes_ejecucion"));
////    confirmKeyOutput.setNameMonthExecution(rs.getString("nombre_mes_ejecucion"));
////    confirmKeyOutput.setStatus(rs.getInt("id_estatus"));
////    confirmKeyOutput.setNameStatus(rs.getString("nombre_estatus_ver"));
//    public List<ConsultKeyDetailsStatusPaidOutPut> conterPaymentKeyDetailsStatusPaidOutPuts(
//            List<PaymentReportOutput> paymentReportOutputs){
//        List<ConsultKeyDetailsStatusPaidOutPut>  consultKeyDetailsStatusPaidOutPuts = new ArrayList<>();
//
//        for(PaymentReportOutput iterPaymentReportOutput : paymentReportOutputs){
//            ConsultKeyDetailsStatusPaidOutPut consultKeyDetailsStatusPaidOutPut = new ConsultKeyDetailsStatusPaidOutPut();
//            consultKeyDetailsStatusPaidOutPut.setDate(iterPaymentReportOutput.getda);
//
//        }
//
//
//        return null;
//    }



}
