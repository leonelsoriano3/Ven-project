package com.venedental.dto.User;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class OutTokenDTO {

    private String token;
    private String dateExpired;

    public OutTokenDTO() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDateExpired() {
        return dateExpired;
    }

    public void setDateExpired(String dateExpired) {
        this.dateExpired = dateExpired;
    }
}
