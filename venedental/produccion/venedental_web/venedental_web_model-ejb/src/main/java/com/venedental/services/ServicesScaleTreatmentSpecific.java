/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;
import com.venedental.dao.scaleTreatmentsSpecific.ScaleTreatmentsDAO;
import com.venedental.dto.scaleTreatmentsSpecific.ScaleTreatmentsInput;
import java.sql.SQLException;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
@Stateless
public class ServicesScaleTreatmentSpecific {
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
    
      /**
     * Servicio para configurar baremo general y especifico a los especialistas.
     *
     * @param idTreatment
     * @param baremo
     * @param receptionDate
     * @param idSpecialist
     * @param idUser
     * @throws java.sql.SQLException
     */
    public void registerScaleTreatment(Integer idTreatment,Double baremo,String receptionDate,Integer idSpecialist ,Integer idUser) throws SQLException {
        ScaleTreatmentsDAO scaleTreatmentsDAO = new ScaleTreatmentsDAO(ds);
        ScaleTreatmentsInput input= new ScaleTreatmentsInput(idTreatment,baremo,receptionDate,idSpecialist,idUser);
        scaleTreatmentsDAO.execute(input);
    }
//   
   
}

