/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author akdesk01
 */
public class ConsultBaremoGeneralOutput {

    private Date fechaBaremo;
    private Integer idTratamiento;
    private String nameTratamiento;
    private Double baremo;
    private Integer idEspecialidad;
    private String nameEspecialidad;
    private String dateForReport;
    private String baremoForReport;

    public ConsultBaremoGeneralOutput() {
    }

    public ConsultBaremoGeneralOutput(Date fechaBaremo, Integer idTratamiento, String nameTratamiento, Double baremo) {
        this.fechaBaremo = fechaBaremo;
        this.idTratamiento = idTratamiento;
        this.nameTratamiento = nameTratamiento;
        this.baremo = baremo;
    }

    public String getNameEspecialidad() {
        return nameEspecialidad;
    }

    public void setNameEspecialidad(String nameEspecialidad) {
        this.nameEspecialidad = nameEspecialidad;
    }

    public String getNameTratamiento() {
        return nameTratamiento;
    }

    public void setNameTratamiento(String nameTratamiento) {
        this.nameTratamiento = nameTratamiento;
    }

    public Date getFechaBaremo() {
        return fechaBaremo;
    }

    public void setFechaBaremo(Date fechaBaremo) {
        this.fechaBaremo = fechaBaremo;
    }

    public Double getBaremo() {

        return baremo;
    }

    public void setBaremo(Double baremo) {
       
        this.baremo = baremo;
    }

    public String getDateForReport() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String fechaStr = formatter.format(fechaBaremo);
        return fechaStr;
    }

    public void setDateForReport(String dateForReport) {
        this.dateForReport = dateForReport;
    }

    public Integer getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Integer idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public String getBaremoForReport() {
        return baremoForReport;
    }

    public void setBaremoForReport(String baremoForReport) {
        baremoForReport = baremoForReport.replace(".", ",");
        this.baremoForReport = baremoForReport;
    }

   
    
    

}
