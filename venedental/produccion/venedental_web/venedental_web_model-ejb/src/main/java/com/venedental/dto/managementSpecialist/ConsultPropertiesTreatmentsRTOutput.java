/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultPropertiesTreatmentsRTOutput {
    
    private String nameTreatment, ValuePiece;
    
    private Integer idPlanTreatment,idPieceTreatment,idPriceTreatment,idPricePiece,idTreatment,idPiece;

    public ConsultPropertiesTreatmentsRTOutput() {
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getValuePiece() {
        return ValuePiece;
    }

    public void setValuePiece(String ValuePiece) {
        this.ValuePiece = ValuePiece;
    }

    public Integer getIdPlanTreatment() {
        return idPlanTreatment;
    }

    public void setIdPlanTreatment(Integer idPlanTreatment) {
        this.idPlanTreatment = idPlanTreatment;
    }

    public Integer getIdPieceTreatment() {
        return idPieceTreatment;
    }

    public void setIdPieceTreatment(Integer idPieceTreatment) {
        this.idPieceTreatment = idPieceTreatment;
    }

    public Integer getIdPriceTreatment() {
        return idPriceTreatment;
    }

    public void setIdPriceTreatment(Integer idPriceTreatment) {
        this.idPriceTreatment = idPriceTreatment;
    }

    public Integer getIdPricePiece() {
        return idPricePiece;
    }

    public void setIdPricePiece(Integer idPricePiece) {
        this.idPricePiece = idPricePiece;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdPiece() {
        return idPiece;
    }

    public void setIdPiece(Integer idPiece) {
        this.idPiece = idPiece;
    }
    
    
    
    
}
