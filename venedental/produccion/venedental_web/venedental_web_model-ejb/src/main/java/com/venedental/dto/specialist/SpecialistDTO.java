package com.venedental.dto.specialist;

/**
 * Created by akdeskdev90 on 19/05/16.
 */
public class SpecialistDTO {

    private Integer id;
    private String address;
    private String firsName;
    private String SecondName;
    private  String completeName;
    private Integer typeSpecialist;
    private String phone;
    private String cellPhone;
    private String rif;
    private Integer idScale;
    private Integer identityNumber;
    private String msds;
    private String medicalAsociation;
    private Integer islr;
    private Integer userId;
    private String sex;
    private String civilStatus;
    private String schedule;
    private String status;
    private String email;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public Integer getTypeSpecialist() {
        return typeSpecialist;
    }

    public void setTypeSpecialist(Integer typeSpecialist) {
        this.typeSpecialist = typeSpecialist;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public Integer getIdScale() {
        return idScale;
    }

    public void setIdScale(Integer idScale) {
        this.idScale = idScale;
    }

    public Integer getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getMsds() {
        return msds;
    }

    public void setMsds(String msds) {
        this.msds = msds;
    }

    public String getMedicalAsociation() {
        return medicalAsociation;
    }

    public void setMedicalAsociation(String medicalAsociation) {
        this.medicalAsociation = medicalAsociation;
    }

    public Integer getIslr() {
        return islr;
    }

    public void setIslr(Integer islr) {
        this.islr = islr;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
