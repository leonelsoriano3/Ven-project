/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "speciality", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Speciality.findAll", query = "SELECT s FROM Speciality s"),
    @NamedQuery(name = "Speciality.findById", query = "SELECT s FROM Speciality s WHERE s.id = :id"),
    @NamedQuery(name = "Speciality.findByName", query = "SELECT s FROM Speciality s WHERE s.name = :name")})
public class Speciality implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    @JoinColumn(name = "type_specialist", referencedColumnName = "id")
    @ManyToOne
    private TypeSpecialist typeSpecialist;

    @ManyToMany(mappedBy = "specialityList")
    private List<Specialists> specialistsList;

    public Speciality() {
    }

    public Speciality(Integer id) {
        this.id = id;
    }

    public Speciality(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Specialists> getSpecialistsList() {
        return specialistsList;
    }

    public void setSpecialistsList(List<Specialists> specialistsList) {
        this.specialistsList = specialistsList;
    }

    public TypeSpecialist getTypeSpecialist() {
        return typeSpecialist;
    }

    public void setTypeSpecialist(TypeSpecialist typeSpecialist) {
        this.typeSpecialist = typeSpecialist;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Speciality)) {
            return false;
        }
        Speciality other = (Speciality) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Speciality obj = null;
        obj = (Speciality) super.clone();
        obj.typeSpecialist = obj.typeSpecialist == null ? null : (TypeSpecialist) obj.typeSpecialist.clone();

        return obj;
    }

}
