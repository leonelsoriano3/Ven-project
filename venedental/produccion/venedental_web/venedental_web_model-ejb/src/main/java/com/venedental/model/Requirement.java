/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Anthony Torres
 */
@Entity
@Table(name = "requirement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requirement.findAll", query = "SELECT r FROM Requirement r"),
    @NamedQuery(name = "Requirement.findById", query = "SELECT r FROM Requirement r WHERE r.id = :id"),
    @NamedQuery(name = "Requirement.findByName", query = "SELECT r FROM Requirement r WHERE r.name = :name"),
    @NamedQuery(name = "Requirement.findByDateOfReception", query = "SELECT r FROM Requirement r WHERE r.dateOfReception = :dateOfReception"),
    @NamedQuery(name = "Requirement.findByValidated", query = "SELECT r FROM Requirement r WHERE r.validated = :validated"),
    @NamedQuery(name = "Requirement.findByNumberDocument", query = "SELECT r FROM Requirement r WHERE r.numberDocument = :numberDocument"),
    @NamedQuery(name = "Requirement.findByFileName", query = "SELECT r FROM Requirement r WHERE r.fileName = :fileName"),
    @NamedQuery(name = "Requirement.findByFileType", query = "SELECT r FROM Requirement r WHERE r.fileType = :fileType"),
    @NamedQuery(name = "Requirement.findAllByReimbursement", query = "SELECT r FROM Requirement r WHERE r.reimbursementId.id = :reimbursementId"),
    @NamedQuery(name = "Requirement.findByFileSystemPath", query = "SELECT r FROM Requirement r WHERE r.fileSystemPath = :fileSystemPath"),
    @NamedQuery(name = "Requirement.findByFileSize", query = "SELECT r FROM Requirement r WHERE r.fileSize = :fileSize") 
})
public class Requirement implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_of_reception")
    @Temporal(TemporalType.DATE)
    private Date dateOfReception;
    
    @Size(max = 120)
    @Column(name = "name")
    private String name;
    
    @Column(name = "validated")
    private boolean validated;
    
    @Size(max = 100)
    @Column(name = "number_document")
    private String numberDocument;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "file_name")
    private String fileName;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "file_system_path")
    private String fileSystemPath;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "file_type")
    private String fileType;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "file_size")
    private String fileSize;
    
    @JoinColumn(name = "reimbursement_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Reimbursement reimbursementId;

    public Requirement() {
    }

    public Requirement(Integer id) {
        this.id = id;
    }

    public Requirement(Integer id, Date dateOfReception, boolean validated, String fileName, String fileSystemPath, String fileType, String fileSize, Reimbursement reimbursementId) {
        this.id = id;
        this.dateOfReception = dateOfReception;
        this.validated = validated;
        this.fileName = fileName;
        this.fileSystemPath = fileSystemPath;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.reimbursementId= reimbursementId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfReception() {
        return dateOfReception;
    }

    public void setDateOfReception(Date dateOfReception) {
        this.dateOfReception = dateOfReception;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public String getNumberDocument() {
        return numberDocument;
    }

    public void setNumberDocument(String numberDocument) {
        this.numberDocument = numberDocument;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSystemPath() {
        return fileSystemPath;
    }

    public void setFileSystemPath(String fileSystemPath) {
        this.fileSystemPath = fileSystemPath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public Reimbursement getReimbursementId() {
        return reimbursementId;
    }

    public void setReimbursementId(Reimbursement reimbursementId) {
        this.reimbursementId = reimbursementId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requirement)) {
            return false;
        }
        Requirement other = (Requirement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.Requirement[ id=" + id + " ]";
    }

}
