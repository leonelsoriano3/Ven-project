/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;

import com.venedental.dto.paymentReport.ConsultPaymentReportRetentionOutput;
import com.venedental.dto.paymentReport.PaymentReportInput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
public class ConsultPaymentReportRetentionDAO extends AbstractQueryDAO<PaymentReportInput, ConsultPaymentReportRetentionOutput> {
    
    public ConsultPaymentReportRetentionDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_ConsultarISLR", 2);
    }

    @Override
    public void prepareInput(PaymentReportInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdSpecialist());
        statement.setString(2, input.getNumberISLR());
    }

    @Override
    public ConsultPaymentReportRetentionOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultPaymentReportRetentionOutput consultPaymentReportRetentionOutput = new ConsultPaymentReportRetentionOutput();
        consultPaymentReportRetentionOutput.setAmountISLR(rs.getDouble("monto_retencion_islr"));
        consultPaymentReportRetentionOutput.setAmountTotal(rs.getDouble("monto_total"));
        consultPaymentReportRetentionOutput.setBaseISLR(rs.getDouble("base_retencion"));
        consultPaymentReportRetentionOutput.setBillDate(rs.getDate("fecha_factura"));
        consultPaymentReportRetentionOutput.setBillNumber(rs.getString("numero_factura"));
        consultPaymentReportRetentionOutput.setDateISLR(rs.getDate("fecha_retencion_islr"));
        consultPaymentReportRetentionOutput.setNumberISLR(rs.getString("numero_retencion_islr"));
        consultPaymentReportRetentionOutput.setRateISLR(rs.getDouble("tasa_retencion_islr"));
        consultPaymentReportRetentionOutput.setNumberControl(rs.getString("numero_control"));
        return consultPaymentReportRetentionOutput;
    } 
}
