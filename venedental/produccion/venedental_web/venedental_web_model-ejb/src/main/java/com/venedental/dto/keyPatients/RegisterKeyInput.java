/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.keyPatients;

/**
 *
 * @author akdeskdev90
 */
public class RegisterKeyInput {

    private String number;
    private String typeAttention;
    private Integer idPatient;
    private Integer idSpecialist;
    private Integer idUser;
    private Integer idMedicalOffice;
    private Integer idCompany;
    private Integer idPlan;

    public RegisterKeyInput(String number, String typeAttention, Integer idPatient, Integer idSpecialist,
                            Integer idUser, Integer idMedicalOffice, Integer idCompany,Integer idPlan) {
        this.number = number;
        this.typeAttention = typeAttention;
        this.idPatient = idPatient;
        this.idSpecialist = idSpecialist;
        this.idUser = idUser;
        this.idMedicalOffice = idMedicalOffice;
        this.idCompany = idCompany;
        this.idPlan=idPlan;
    }

    public Integer getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Integer idCompany) {
        this.idCompany = idCompany;
    }

    public Integer getIdSpecialist() {
        return idSpecialist;
    }

    public void setIdSpecialist(Integer idSpecialist) {
        this.idSpecialist = idSpecialist;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypeAttention() {
        return typeAttention;
    }

    public void setTypeAttention(String typeAttention) {
        this.typeAttention = typeAttention;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdMedicalOffice() {
        return idMedicalOffice;
    }

    public void setIdMedicalOffice(Integer idMedicalOffice) {
        this.idMedicalOffice = idMedicalOffice;
    }

    public Integer getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(Integer idPlan) {
        this.idPlan = idPlan;
    }

}