/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;

/**
 *
 * @author ARKDEV16
 */
public class NetworkSpecialistsReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer identityNumber;
    
    private String name;
    
    private String healthArea;
    
    private String speciality;
    
    private String clinicName;
    
    private String phone;
    
    private String address;
    
    private String city;
    
    private String state;
    
    private String schedule;
    
    
    public Integer getIdentityNumber() {
        return identityNumber;
    }
    
    public void setIdentityNumber(Integer identityNumber) {
        this.identityNumber = identityNumber;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getHealthArea() {
        return healthArea;
    }
    
    public void setHealthArea(String healthArea) {
        this.healthArea = healthArea;
    }
    
    public String getSpeciality() {
        return speciality;
    }
    
    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
    
    public String getClinicName() {
        return clinicName;
    }
    
    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }
    
    public String getPhone() {
        return phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getState() {
        return state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public String getSchedule() {
        return schedule;
    }
    
    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }
}
