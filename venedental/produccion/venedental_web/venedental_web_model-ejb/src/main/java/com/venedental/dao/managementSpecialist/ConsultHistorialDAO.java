/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultHistorialInput;
import com.venedental.dto.managementSpecialist.ConsultHistorialOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultHistorialDAO extends AbstractQueryDAO<ConsultHistorialInput, ConsultHistorialOutput> {
    
    
    public ConsultHistorialDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ESP_HistoricoClavesPacienteEspecialista", 4);
    }
    
     @Override
    public void prepareInput(ConsultHistorialInput input, CallableStatement statement) throws SQLException {
        if(input.getIdSpecialist()==null){
            statement.setNull(1,java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getIdPatient()); 
        }
        
         if(input.getIdPatient()==null){
            statement.setNull(2,java.sql.Types.NULL);
        }else{
            statement.setInt(2,input.getIdSpecialist()); 
        }
           
        
        statement.registerOutParameter(3, java.sql.Types.DATE); 
        statement.registerOutParameter(4, java.sql.Types.DATE);
         
    }

    @Override
    public ConsultHistorialOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Date dateMinFilter;
        Date dateEndDateFilter;
        dateMinFilter = statement.getDate(3);
        dateEndDateFilter = statement.getDate(4);
        
       ConsultHistorialOutput consultDateForHistorialOutput = new ConsultHistorialOutput();
       consultDateForHistorialOutput.setDateApplication(rs.getDate("FECHA_GENERACION_CLAVE"));
        consultDateForHistorialOutput.setIdKeyPatient(rs.getInt("ID_CLAVE"));
        consultDateForHistorialOutput.setNumberKey(rs.getString("NUMERO_CLAVE"));
        consultDateForHistorialOutput.setIdTreatment(rs.getInt("ID_TRATAMIENTO"));
        consultDateForHistorialOutput.setNameTratment(rs.getString("NOMBRE_TRATAMIENTO"));
        consultDateForHistorialOutput.setIdPropertiePlantreatmentKey(rs.getInt("ID_PIEZA_TRATAMIENTO_CLAVE"));
        consultDateForHistorialOutput.setNumberPropertie(rs.getString("NUMERO_PIEZA"));
        consultDateForHistorialOutput.setIdSpecialist(rs.getInt("ID_ESPECIALISTA"));
        consultDateForHistorialOutput.setSpecialist(rs.getString("NOMBRE_ESPECIALISTA"));
        consultDateForHistorialOutput.setRelationShip(rs.getString("NOMBRE_PARENTESCO"));
        consultDateForHistorialOutput.setInsurance(rs.getString("NOMBRE_ASEGURADORA"));
        consultDateForHistorialOutput.setPlan(rs.getString("NOMBRE_PLAN"));
        consultDateForHistorialOutput.setCompany(rs.getString("NOMBRE_COMPAÑIA"));
       
        
        consultDateForHistorialOutput.setDateMaxFilter(dateEndDateFilter);
        consultDateForHistorialOutput.setDateMinFilter(dateMinFilter);
        
        return consultDateForHistorialOutput;
    }
    
    
}
