/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.consultKeys.ConsultKeysPatientsDetailsDAO;
import com.venedental.dao.specialist.FindSpecialistByIdDAO;
import com.venedental.dao.managementSpecialist.FindRolUserDAO;
import com.venedental.dao.managementSpecialist.FindDataSpecislitDAO;
import com.venedental.dao.specialist.FindSpecialistByIdUser;
import com.venedental.dao.specialist.FindSpecialistByIdentityOrNameActiveDAO;
import com.venedental.dao.specialist.FindSpecialistByIdentityOrNameDAO;
import com.venedental.dao.specialist.FindSpecialistByUserIdDAO;
import com.venedental.dto.consultKeys.ConsultKeysPatientsDetailOutput;
import com.venedental.dto.specialist.SpecialistDTO;
import com.venedental.dto.managementSpecialist.FindRolUserOutput;
import com.venedental.dto.managementSpecialist.FindDataSpecialistOutput;
import com.venedental.dto.specialist.SpecialistByIdentityOrNameAndRol;
import com.venedental.model.Specialists;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServiceSpecialists {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite busca run especialista por id
     * @param idSpecialist
     * @return
     */
    public Specialists findSpecialistById(Integer idSpecialist) {
        FindSpecialistByIdDAO findSpecialistByIdDAO = new FindSpecialistByIdDAO(ds);
        findSpecialistByIdDAO.execute(idSpecialist);
        return findSpecialistByIdDAO.getResult();
    }

    /**
     * Servicio que permite buscar un especialista por nombre o cedula
     *
     * @param parameter
     * @return
     */
    public List<Specialists> findByIdentityOrName(String parameter) {
        FindSpecialistByIdentityOrNameDAO findSpecialistByIdentityOrNameDAO = new FindSpecialistByIdentityOrNameDAO(ds);
        findSpecialistByIdentityOrNameDAO.execute(parameter);
        return findSpecialistByIdentityOrNameDAO.getResultList();
    }


    public SpecialistDTO findSpecialistByUserid(Integer userId){

        FindSpecialistByUserIdDAO findSpecialistByUserIdDAO = new FindSpecialistByUserIdDAO(ds);
        findSpecialistByUserIdDAO.execute(userId);
        return findSpecialistByUserIdDAO.getResult();

    }

    
    /**
     * Servicio que permite obtener el id y rol de un usuario logeado(se utiliza para verificar si tiene rol especialista asignado)
     *
     * @param idUser
     * @return
     */
    public List<FindRolUserOutput> findRolByUser(Integer idUser) {
        FindRolUserDAO findRolUserDAO = new FindRolUserDAO(ds);
        findRolUserDAO.execute(idUser);
        return findRolUserDAO.getResultList();
    }
    
    /**
     * Servicio que permite obtener la información de un especialista logeado dado su id para la creación de una clave
     *
     * @param idUser
     * @return
     */
    public List <FindDataSpecialistOutput> findDataSpecialist(Integer idUser) {
       FindDataSpecislitDAO findDataSpecislitDAO = new FindDataSpecislitDAO(ds);
        findDataSpecislitDAO.execute(idUser);
        return findDataSpecislitDAO.getResultList();
    }
    
    

    /**
     * Servicio que permite buscar un especialista por nombre o cedula activos
     * @param inputDate
     * @param idUser
     * @return
     */
    public List<Specialists> findByIdentityOrNameActive(String inputDate, Integer idUser) {
        FindSpecialistByIdentityOrNameActiveDAO findSpecialist = new FindSpecialistByIdentityOrNameActiveDAO(ds);
        SpecialistByIdentityOrNameAndRol input= new SpecialistByIdentityOrNameAndRol(inputDate,idUser);
        findSpecialist.execute(input);
        return findSpecialist.getResultList();
    }


    /**
     * Servicio que permite buscar un especialista id de usuario
     *
     * @param idUser
     * @return
     */
    public Specialists findByIdUser(Integer idUser) {
        FindSpecialistByIdUser findSpecialistByIdUser = new FindSpecialistByIdUser(ds);
        findSpecialistByIdUser.execute(idUser);
        return findSpecialistByIdUser.getResult();
    }
    /**
     * Servicio que permite buscar el detalle de una clave
     *
     * @param key
     * @return
     */
    public ConsultKeysPatientsDetailOutput detailsConsultKeysSpecialist(Integer key) {
        ConsultKeysPatientsDetailsDAO consultKeysPatientsDetailsDAO = new ConsultKeysPatientsDetailsDAO(ds);
        consultKeysPatientsDetailsDAO.execute(key);
        return consultKeysPatientsDetailsDAO.getResult();
    }
    
    
}




