/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.baremo;

import java.sql.Date;


/**
 *
 * @author akdesk01
 */
public class ConfigBaremoDateMinFilterOutput {
    
    private Date dateMinFilter;
    
    public ConfigBaremoDateMinFilterOutput() {
       
    }

    public Date getDateMinFilter() {
        return dateMinFilter;
    }

    public void setDateMinFilter(Date dateMinFilter) {
        this.dateMinFilter = dateMinFilter;
    }
    
    
    
}
