package com.venedental.dao.reportAuditConsultTrace;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportAuditConsultTrace.ReportPaymentDetailsOutput;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luiscarrasco1991@arkiteck.biz on 10/11/16.
 */
public class ReportPaymentDetailsDAO extends AbstractQueryDAO<Integer,ReportPaymentDetailsOutput> {

    public ReportPaymentDetailsDAO(DataSource dataSource){
        super(dataSource,"SP_M_ESP_LeerTratamientosReportePago",1);
    }

    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {

        statement.setInt(1,input);
    }

    @Override
    public ReportPaymentDetailsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ReportPaymentDetailsOutput reportTreatmentDetailsOutput = new ReportPaymentDetailsOutput();

        reportTreatmentDetailsOutput.setIdKey(rs.getInt("id_clave"));
        reportTreatmentDetailsOutput.setNumKey(rs.getString("numero_clave"));
        reportTreatmentDetailsOutput.setDate(rs.getString("fecha"));
        reportTreatmentDetailsOutput.setIdKeyTreatment(rs.getInt("id_clave_tratamiento"));
        reportTreatmentDetailsOutput.setIdPatient(rs.getInt("id_paciente"));
        reportTreatmentDetailsOutput.setCardIdPatient(rs.getString("cedula_paciente"));
        reportTreatmentDetailsOutput.setNamePatient(rs.getString("nombre_paciente"));
        reportTreatmentDetailsOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        reportTreatmentDetailsOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        reportTreatmentDetailsOutput.setIdToothKeyTreatment(rs.getInt("id_pieza_clave_tratamiento"));
        reportTreatmentDetailsOutput.setIdTooth(rs.getInt("id_pieza"));
        reportTreatmentDetailsOutput.setNameTooth(rs.getString("nombre_pieza"));
        reportTreatmentDetailsOutput.setStatusTooth(rs.getString("estatus_pieza"));
        reportTreatmentDetailsOutput.setObservation(rs.getString("observacion"));
        reportTreatmentDetailsOutput.setBaremo(rs.getString("baremo"));

        return  reportTreatmentDetailsOutput;

    }

}
