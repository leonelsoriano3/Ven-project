package com.venedental.dto.reportAuditConsultTrace;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by akdesk10 on 21/07/16.
 */
public class ReportTreatmentDetailsOutput {


    private Integer idKey;
    private String carId;
    private String numberKey;
    private Integer idKeyTreatment;
    private String  date;
    private Integer idPatient;
    private Integer idTreatment;
    private String namePatient;
    private String nameTreatment;
    private Integer idToothKeyTreatment;
    private Integer idTooth;
    private String nameTooth;
    private Date dateReport;

    public ReportTreatmentDetailsOutput() {}

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public String getNameTooth() {
        return nameTooth;
    }

    public void setNameTooth(String nameTooth) {
        this.nameTooth = nameTooth;
    }

    public Integer getIdTooth() {
        return idTooth;
    }

    public void setIdTooth(Integer idTooth) {
        this.idTooth = idTooth;
    }

    public Integer getIdToothKeyTreatment() {
        return idToothKeyTreatment;
    }

    public void setIdToothKeyTreatment(Integer idToothKeyTreatment) {
        this.idToothKeyTreatment = idToothKeyTreatment;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String dateKey) {

        DateTimeFormatter formatterFrom = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateKey, formatterFrom);
        DateTimeFormatter formatterTo = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.date = localDate.format(formatterTo);

    }

    public Integer getIdKeyTreatment() {
        return idKeyTreatment;
    }

    public void setIdKeyTreatment(Integer idKeyTreatment) {
        this.idKeyTreatment = idKeyTreatment;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public Date getDateReport() {
        return dateReport;
    }

    public void setDateReport(Date dateReport) {
        this.dateReport = dateReport;
    }
    
    
}
