/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.managementSpecialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.managementSpecialist.ConsultPropertiesTreatmentsOutput;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultPropestiesTreatmentDAO extends AbstractQueryDAO<Integer, ConsultPropertiesTreatmentsOutput> {
public ConsultPropestiesTreatmentDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PROPERTIES_G_005", 1);
    }
  @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input);
    }
    
    @Override
    public ConsultPropertiesTreatmentsOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultPropertiesTreatmentsOutput consultPropertiesTreatmentsOutput = new ConsultPropertiesTreatmentsOutput();
        consultPropertiesTreatmentsOutput.setIdPiece(rs.getInt("id_pieza"));
        consultPropertiesTreatmentsOutput.setIdTreatment(rs.getInt("id_tratamiento"));
        consultPropertiesTreatmentsOutput.setNameTreatment(rs.getString("nombre_tratamiento"));
        consultPropertiesTreatmentsOutput.setPieceValue(rs.getString("valor_pieza"));
        return consultPropertiesTreatmentsOutput;
    }

}
