/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Cities;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Edgar Mosquera
 */
@Local
public interface CitiesFacadeLocal {

    Cities create(Cities cities);

    Cities edit(Cities cities);

    void remove(Cities cities);

    Cities find(Object id);

    List<Cities> findAll();

    List<Cities> findRange(int[] range);

    int count();
    
}
