/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.venedental.facade;

import com.venedental.model.Scale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author CarlosDaniel
 */
@Stateless
public class ScaleFacade extends AbstractFacade<Scale> implements ScaleFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScaleFacade() {
        super(Scale.class);
    }
    
}
