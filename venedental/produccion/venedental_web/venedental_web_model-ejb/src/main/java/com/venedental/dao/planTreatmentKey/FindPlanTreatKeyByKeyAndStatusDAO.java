/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.FindPtkByKeyAndStatusInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindPlanTreatKeyByKeyAndStatusDAO extends AbstractQueryDAO<FindPtkByKeyAndStatusInput, PlansTreatmentsKeys> {

    public FindPlanTreatKeyByKeyAndStatusDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PLANTREAKEYS_G_002", 3);
    }

    @Override
    public void prepareInput(FindPtkByKeyAndStatusInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdKeyPatient());
        statement.setInt(2, input.getIdStatusOne());
        if (input.getIdStatusTwo() == 0) {
             statement.setNull(3, java.sql.Types.NULL);
        } else {           
            statement.setInt(3, input.getIdStatusTwo());
        }
    }

    @Override
    public PlansTreatmentsKeys prepareOutput(ResultSet rs , CallableStatement statement) throws SQLException {
        PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
        plansTreatmentsKeys.setId(rs.getInt("id"));
        plansTreatmentsKeys.setStatusId(rs.getInt("id_status"));
        return plansTreatmentsKeys;
    }

}
