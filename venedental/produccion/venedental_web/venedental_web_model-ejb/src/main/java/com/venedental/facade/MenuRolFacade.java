/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.security.MenuRol;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Edgar Anzola
 */
@Stateless
public class MenuRolFacade extends AbstractFacade<MenuRol> implements MenuRolFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuRolFacade() {
        super(MenuRol.class);
    }

    @Override
    public MenuRol edit(MenuRol entity) {
        return super.edit(entity);
    }

    @Override
    public MenuRol create(MenuRol entity) {
        return super.create(entity);
    }

    @Override
    public List<MenuRol> findByRolId(int id) {
        TypedQuery<MenuRol> query = em.createNamedQuery("MenuRol.findByRolId", MenuRol.class);
        query.setParameter("rol_id", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

}
