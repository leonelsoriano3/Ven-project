/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.PlansPatients;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class PlansPatientsFacade extends AbstractFacade<PlansPatients> implements PlansPatientsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlansPatientsFacade() {
        super(PlansPatients.class);
    }

    @Override
    public PlansPatients edit(PlansPatients entity) {
        return super.edit(entity);
    }

    @Override
    public PlansPatients create(PlansPatients entity) {
        return super.create(entity);
    }

    @Override
    public List<PlansPatients> findByPatientsId(Integer id) {
        TypedQuery<PlansPatients> query = em.createNamedQuery("PlansPatients.findByPatientsId", PlansPatients.class);
        query.setParameter("patientsId", id);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<PlansPatients> findByPatientsAndPlans(Integer patientsId, Integer plansId){
        TypedQuery<PlansPatients> query = em.createNamedQuery("PlansPatients.findByPatientsAndPlans", PlansPatients.class);
        query.setParameter("patientsId", patientsId);
        query.setParameter("plansId", plansId);
        return query.getResultList();
    }
    
    @Override
    public List<PlansPatients> findByIdentityHolderAndPlan(Integer identityHolder, Integer specialistId){
        try{
            StoredProcedureQuery query = em.createStoredProcedureQuery("SP_T_PLANSPATIENT_G_002");
            query.registerStoredProcedureParameter("PI_CEDULA_PACIENTE", Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("PI_AREA_SALUD", Integer.class, ParameterMode.IN);
            query.setParameter("PI_CEDULA_PACIENTE", identityHolder);
            query.setParameter("PI_AREA_SALUD", specialistId);
            return query.getResultList();
        }catch(NoResultException exception){
            return null;
        }
    }

     @Override
    public List<PlansPatients>  findByPatientsIdAndTypeSpecialist(Integer id, Integer idTypeSpecialist) {

        TypedQuery<PlansPatients> query = em.createNamedQuery("PlansPatients.findByPatientsIdAndTypeSpecialist", PlansPatients.class);
        query.setParameter("patientsId", id);
        query.setParameter("typeSpecialistId", idTypeSpecialist);
        return query.getResultList();
    }

}
