/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class AddTreatmentForKeyOutput {
    private Integer idTreatmentKey;
    private Integer idTreatmentKeyDiagnosis;

    public AddTreatmentForKeyOutput() {
    }
    
    
    public Integer getIdTreatmentKey() {
        return idTreatmentKey;
    }

    public void setIdTreatmentKey(Integer idTreatmentKey) {
        this.idTreatmentKey = idTreatmentKey;
    }

    public Integer getIdTreatmentKeyDiagnosis() {
        return idTreatmentKeyDiagnosis;
    }

    public void setIdTreatmentKeyDiagnosis(Integer idTreatmentKeyDiagnosis) {
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }
    
    
}
