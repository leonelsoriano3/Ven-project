/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Specialists;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

/**
 *
 * @author Usuario
 */
@Stateless
public class SpecialistsFacade extends AbstractFacade<Specialists> implements SpecialistsFacadeLocal {

    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SpecialistsFacade() {
        super(Specialists.class);
    }

    @Override
    public List<Specialists> findByStatus(int status) {
        TypedQuery<Specialists> query = em.createNamedQuery("Specialists.findByStatus", Specialists.class);
        query.setParameter("status", status);
        return query.getResultList();
    }
    
    @Override
    public Specialists findById(Integer id) {
        try {
            TypedQuery<Specialists> query = em.createNamedQuery("Specialists.findById", Specialists.class);
            query.setParameter("id", id);
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public Specialists findByUser(int usersId) {
        try {
            TypedQuery<Specialists> query = em.createNamedQuery("Specialists.findByUserId", Specialists.class);
            query.setParameter("users_id", usersId);
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public void deleteSpecialist(Integer id) {
        TypedQuery<Specialists> query = em.createNamedQuery("Specialists.deleteSpecialist", Specialists.class);
        query.setParameter("specialist_id", id);
        query.executeUpdate();
        em.flush();
        em.clear();
    }

    @Override
    public void updateStatus(Integer id) {
        TypedQuery<Specialists> query = em.createNamedQuery("Specialists.updateStatus", Specialists.class);
        query.setParameter("specialist_id", id);
        query.executeUpdate();
        em.flush();
        em.clear();
    }

    @Override
    public Specialists findByIdentityNumber(int identity_number) {
        try {
            TypedQuery<Specialists> query = em.createNamedQuery("Specialists.findByIdentityNumber", Specialists.class);
            query.setParameter("identity_number", identity_number);
            return query.getSingleResult();

        } catch (NoResultException exception) {
            return null;
        }

    }
    
    
    @Override
    public List<Specialists> findByIdentityNumberOrName(String filter) {
        String NewFilter = "'%" + filter + "%'";
        String sql = "SELECT * FROM specialists where status = 1 and (identity_number LIKE " + NewFilter +
                      " OR complete_name LIKE " + NewFilter +")";

        Query query = em.createNativeQuery(sql, Specialists.class);
        return query.getResultList();
    }
        
    /*Init StoredProcedure */
    @Override
    public List<Specialists> filterByTratmentStatusAndDateRange(Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_ESPECIALISTAS_PLANES_TRATAMIENTOS_ACEPTADOS_RECHAZADOS", Specialists.class);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);    
        return query.getResultList();
    }
    
    
    @Override
    public List<Specialists> findByPaymentReportStatusAndDateRange(Integer idStatus, Date dateFrom, Date dateUntil) {
        StoredProcedureQuery query = em.createStoredProcedureQuery("SP_LEER_ESPECIALISTAS_REPORTE_PAGO", Specialists.class);
        query.registerStoredProcedureParameter("PI_ID_ESTATUS", Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_DESDE", Date.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("PI_FECHA_HASTA", Date.class, ParameterMode.IN);
        query.setParameter("PI_ID_ESTATUS", idStatus);         
        query.setParameter("PI_FECHA_DESDE", dateFrom);
        query.setParameter("PI_FECHA_HASTA", dateUntil);    
        return query.getResultList();
    }
    
     /*End StoredProcedure */

}
