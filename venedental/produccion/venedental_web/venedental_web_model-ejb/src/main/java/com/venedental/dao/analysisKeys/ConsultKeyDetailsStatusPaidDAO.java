/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.analysisKeys;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.analysisKeys.ConsultKeyDetailsStatusPaidInput;
import com.venedental.dto.analysisKeys.ConsultKeyDetailsStatusPaidOutPut;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class ConsultKeyDetailsStatusPaidDAO extends AbstractQueryDAO<ConsultKeyDetailsStatusPaidInput, ConsultKeyDetailsStatusPaidOutPut> {

    //SP_M_ACL_ReportePagado

    public ConsultKeyDetailsStatusPaidDAO(DataSource dataSource) {
        super(dataSource, "SP_M_ACL_ReportePorFacturar", 3);
    }

    @Override
    public void prepareInput(ConsultKeyDetailsStatusPaidInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getSpecialistId());
        statement.setInt(2, input.getNumberMonth());
        statement.setDate(3, input.getDateReception());
    }


    @Override
    public ConsultKeyDetailsStatusPaidOutPut prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {

        ConsultKeyDetailsStatusPaidOutPut consultKeyDetailsStatusPaidOutPut = new ConsultKeyDetailsStatusPaidOutPut();

        consultKeyDetailsStatusPaidOutPut.setPatientId(rs.getInt("id_paciente"));
        consultKeyDetailsStatusPaidOutPut.setPatientName(rs.getString("nombre_paciente"));
        consultKeyDetailsStatusPaidOutPut.setTreatmentKeyId(rs.getInt("id_tratamiento_clave"));
        consultKeyDetailsStatusPaidOutPut.setTreatmentKeyPieceId(rs.getInt("id_pieza_tratamiento_clave"));
        consultKeyDetailsStatusPaidOutPut.setKeyNumber(rs.getString("numero_clave"));
        consultKeyDetailsStatusPaidOutPut.setTreatmentName(rs.getString("nombre_tratamiento"));
        consultKeyDetailsStatusPaidOutPut.setNumberPiece(rs.getString("numero_pieza"));
        consultKeyDetailsStatusPaidOutPut.setObservation(rs.getString("observaciones"));
        consultKeyDetailsStatusPaidOutPut.setStatusId(rs.getInt("id_estatus"));
        consultKeyDetailsStatusPaidOutPut.setNameStatus(rs.getString("nombre_estatus"));
        consultKeyDetailsStatusPaidOutPut.setMountPaid(rs.getDouble("monto_pagar"));
        consultKeyDetailsStatusPaidOutPut.setDate(rs.getDate("fecha"));

        return consultKeyDetailsStatusPaidOutPut;
    }

}
