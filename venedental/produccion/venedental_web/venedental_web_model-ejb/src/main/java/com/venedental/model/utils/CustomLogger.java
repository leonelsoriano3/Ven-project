/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akdesarrollo
 */
public class CustomLogger {

    private static Logger generalLogger;

    public static Logger getGeneralLogger(String nombre) {
        generalLogger = Logger.getLogger(nombre);
        if (PropertiesConfiguration.get("logger").compareTo("off") == 0) {
            generalLogger.setLevel(Level.OFF);
        } else {
            generalLogger.setLevel(Level.ALL);
        }
        return generalLogger;
    }

    public static void setGeneralLogger(Logger generalLogger) {
        CustomLogger.generalLogger = generalLogger;
    }

    public static Logger getLogger(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
