/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.propertiesPlansTreatmentsKey.CreatePropertiePlanTreatKeyDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.FindProPlaTreKeyByKeyAndStatusDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.FindProPlaTreKeyByPTKAndPTAndStatusDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.FindProPlaTreKeyByPlanTreKeyDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.FindProPlaTreKeyByReportInvoicesTransferDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.RegisterPropertiesPlanTreatmentKeyDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.UpdateProPlanTreaKeyStatusScaleDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.UpdateProPlanTreatKeyObservationDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.UpdateProPlanTreatKeyStatusDAO;
import com.venedental.dao.propertiesPlansTreatmentsKey.FindProPlaTreKeyByKeyAndStatusFilesDAO;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByReportInvoicesTransferInput;
import com.venedental.dto.planTreatmentKey.UpdatePlanTreatmentKeyStatusInput;
import com.venedental.dto.propertiesPlanTreatmentKey.*;
import com.venedental.model.PropertiesPlansTreatmentsKey;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
@Stateless
public class ServicePropertiesPlansTreatmentKey {

    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;

    /**
     * Servicio que permite crear plan treatment key
     *
     * @param propertiesPlansTreatmentsKey
     * @throws java.sql.SQLException
     */
    public void create(PropertiesPlansTreatmentsKey propertiesPlansTreatmentsKey) throws SQLException {
        CreatePropertiePlanTreatKeyDAO createPropertiePlanTreatKeyDAO = new CreatePropertiePlanTreatKeyDAO(ds);
        createPropertiePlanTreatKeyDAO.execute(propertiesPlansTreatmentsKey);
    }

    /**
     * Servicio que permite actualizar el estado de las piezas de una clave y su
     * bitacora
     *
     * @param idPropertiePlanTreatmentKey
     * @param idStatus
     * @param idUser
     * @param dateReception
     * @throws SQLException
     */
    public void updateStatus(Integer idPropertiePlanTreatmentKey, Integer idStatus, Integer idUser, Date dateReception) throws SQLException {
        UpdateProPlanTreatKeyStatusDAO updatePropertiePlanTreatmentKeyStatusDAO = new UpdateProPlanTreatKeyStatusDAO(ds);
        UpdatePlanTreatmentKeyStatusInput input = new UpdatePlanTreatmentKeyStatusInput(idPropertiePlanTreatmentKey, idStatus, idUser, dateReception);
        updatePropertiePlanTreatmentKeyStatusDAO.execute(input);
    }
    
    /**
     * Servicio que permite actualizar el estado de las piezas de una clave y su
     * bitacora
     *
     * @param idPropertiePlanTreatmentKey
     * @param idStatus
     * @param idUser
     * @param dateReception
     * @param dateEjecution
     * @param idScale
     * @throws SQLException
     */
    public void updateStatusAndScale(Integer idPropertiePlanTreatmentKey, Integer idStatus, Integer idUser, 
            Date dateReception, Date dateEjecution, Integer idScale) throws SQLException {
        UpdateProPlanTreaKeyStatusScaleDAO updateProPlanTreaKeyStatusScaleDAO = new UpdateProPlanTreaKeyStatusScaleDAO(ds);
        UpdatePlanTreatmentKeyStatusInput input = new UpdatePlanTreatmentKeyStatusInput(idPropertiePlanTreatmentKey, idStatus, 
                idUser, dateReception, dateEjecution, idScale);
        updateProPlanTreaKeyStatusScaleDAO.execute(input);
    }

    /**
     * Servicio que permite actualizar la observación de las piezas de una clave
     * y su bitacora
     *
     * @param idPropertiePlanTreatmentKey
     * @param observation
     * @throws SQLException
     */
    public void updateObservation(int idPropertiePlanTreatmentKey, String observation) throws SQLException {
        UpdateProPlanTreatKeyObservationDAO updatePropertiesPlanTreatmentKeyObservationDAO = new UpdateProPlanTreatKeyObservationDAO(ds);
        UpdatePropertiesPlanTreatmentKeyObservationInput input = new UpdatePropertiesPlanTreatmentKeyObservationInput(idPropertiePlanTreatmentKey, observation);
        updatePropertiesPlanTreatmentKeyObservationDAO.execute(input);
    }

    /**
     * Servicio para buscar los tratamientos con piezas que pertenecen a la
     * clave dado el id de la clave y el id del estado a consultar
     *
     * @param idKeys
     * @param idStatus
     * @param receptionDate
     * @return
     */
    public List<PropertiesPlansTreatmentsKey> findProPlanstkStatusByKeyAndStatus(Integer idKeys, Integer idStatus, Date receptionDate) {
        FindProPlaTreKeyByKeyAndStatusDAO findProPlanstkStatusByKeyAndStatusDAO = new FindProPlaTreKeyByKeyAndStatusDAO(ds);
        FindProPlanstkStatusByKeyAndStatusInput input = new FindProPlanstkStatusByKeyAndStatusInput(idKeys, idStatus, receptionDate);
        findProPlanstkStatusByKeyAndStatusDAO.execute(input);
        return findProPlanstkStatusByKeyAndStatusDAO.getResultList();
    }

    /**
     * Servicio para buscar los tratamientos con piezas que pertenecen a la
     * clave dado el id de la clave y el id del estado a consultar, fue agregado para utilizar los recaudos en analisis--registrar
     *
     * @param idKeys
     * @param idStatus
     * @param receptionDate
     * @return
     */
    public List<PropertiesPlansTreatmentsKeyOutput> findProPlanstkStatusByKeyAndStatusFiles(Integer idKeys, Integer idStatus, Date receptionDate) {
        FindProPlaTreKeyByKeyAndStatusFilesDAO findProPlaTreKeyByKeyAndStatusFilesDAO = new FindProPlaTreKeyByKeyAndStatusFilesDAO(ds);
        FindProPlanstkStatusByKeyAndStatusInput input = new FindProPlanstkStatusByKeyAndStatusInput(idKeys, idStatus, receptionDate);
        findProPlaTreKeyByKeyAndStatusFilesDAO.execute(input);
        return findProPlaTreKeyByKeyAndStatusFilesDAO.getResultList();
    }

    /**
     * Servicio para registrar peizas dentales a un tratamiento de una clave
     *
     * @param idPlanTreatmentKey
     * @param idPropetieTreatment
     * @param idScaleTratment
     * @param dateApplication
     * @param idStatus
     * @param observation
     * @param idUser
     * @param dateReception
     * @return 
     * @throws SQLException
     */
    public PropertiesPlansTreatmentsKey registerPropertiesPlanTreatmentKey(Integer idPlanTreatmentKey, Integer idPropetieTreatment,Integer idScaleTratment, Date dateApplication, Integer idStatus, String observation, Integer idUser, Date dateReception) throws SQLException {
        RegisterPropertiesPlanTreatmentKeyDAO registrerPlanTreatmentKeyDAO = new RegisterPropertiesPlanTreatmentKeyDAO(ds);
        PropertiesPlanTreatmentKeyInput input = new PropertiesPlanTreatmentKeyInput(idPlanTreatmentKey, idPropetieTreatment,idScaleTratment,dateApplication,idStatus, observation, idUser, dateReception);
        registrerPlanTreatmentKeyDAO.execute(input);
        return registrerPlanTreatmentKeyDAO.getResult();
    }

    /**
     * Servicio para buscar piezas dentales de un tratamiento de una clave por
     * estado
     *
     * @param idPlanTreatmentKey
     * @param idStatus
     * @return List PropertiesPlansTreatmentsKey
     */
    public List<PropertiesPlansTreatmentsKey> findByPlanTreatmentKeyAndStatus(Integer idPlanTreatmentKey, Integer idStatus) {
        FindProPlaTreKeyByPlanTreKeyDAO findProPlaTreKeyByPlanTreKeyDAO = new FindProPlaTreKeyByPlanTreKeyDAO(ds);
        FindProPlaTreKeyByPlanTreKeyInput input = new FindProPlaTreKeyByPlanTreKeyInput(idPlanTreatmentKey, idStatus);
        findProPlaTreKeyByPlanTreKeyDAO.execute(input);
        return findProPlaTreKeyByPlanTreKeyDAO.getResultList();
    }
    
    
        /**
     * Servicio para buscar piezas dentales de un tratamiento de una clave por
     * estado
     *
     * @param idPlanTreatmentKey
     * @param idPropertieTreatment
     * @param idStatus
     * @return List PropertiesPlansTreatmentsKeyOutput
     */
    public PropertiesPlansTreatmentsKeyOutput findByPlanTreatKeyStatusAndProTreat(Integer idPlanTreatmentKey, Integer idPropertieTreatment, Integer idStatus) {
        FindProPlaTreKeyByPTKAndPTAndStatusDAO findProPlaTreKeyByPlanTreKeyAndProTreaDAO = new FindProPlaTreKeyByPTKAndPTAndStatusDAO(ds);
        FindProPlaTreKeyByPlanTreKeyAndProTreaInput input = new FindProPlaTreKeyByPlanTreKeyAndProTreaInput(idPlanTreatmentKey,idPropertieTreatment, idStatus);
        findProPlaTreKeyByPlanTreKeyAndProTreaDAO.execute(input);
        return findProPlaTreKeyByPlanTreKeyAndProTreaDAO.getResult();
    }
    
                /**
     * Servicio que permite buscar piezas dentales de un tratamiento de una clave por reporte de pago
     *
     * @param idReport
     * @param idStatus
     * @param dateFrom
     * @param dateUntil
     * @return
     */
    public List<PropertiesPlansTreatmentsKey> findProPlaTreKeyByReportInvoicesTransfer(Integer idReport, Integer idStatus, Date dateFrom, Date dateUntil) {
        FindProPlaTreKeyByReportInvoicesTransferDAO findProPlaTreKeyByReportInvoicesTransferDAO = new FindProPlaTreKeyByReportInvoicesTransferDAO(ds);
        FindPlanTreatKeyByReportInvoicesTransferInput input = new FindPlanTreatKeyByReportInvoicesTransferInput(idReport, idStatus, dateFrom, dateUntil);
        findProPlaTreKeyByReportInvoicesTransferDAO.execute(input);
        return findProPlaTreKeyByReportInvoicesTransferDAO.getResultList();
    }
}
