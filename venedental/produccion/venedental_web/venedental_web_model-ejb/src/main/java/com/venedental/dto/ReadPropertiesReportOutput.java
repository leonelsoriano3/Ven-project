/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.venedental.dto;

import java.io.Serializable;

/**
 *
 * @author ARKDEV16
 */
public class ReadPropertiesReportOutput implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer idProperties;
    
    private String nameProperties;

    public Integer getIdProperties() {
        return idProperties;
    }

    public void setIdProperties(Integer idProperties) {
        this.idProperties = idProperties;
    }

    public String getNameProperties() {
        return nameProperties;
    }

    public void setNameProperties(String nameProperties) {
        this.nameProperties = nameProperties;
    } 
}