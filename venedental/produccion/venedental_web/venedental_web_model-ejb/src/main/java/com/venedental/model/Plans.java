/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "plans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plans.findAll", query = "SELECT p FROM Plans p"),
    @NamedQuery(name = "Plans.findById", query = "SELECT p FROM Plans p WHERE p.id = :id"),
    @NamedQuery(name = "Plans.findByName", query = "SELECT p FROM Plans p WHERE p.name = :name"),
    @NamedQuery(name = "Plans.findByInsurance", query = "SELECT p FROM Plans p WHERE p.insuranceId = :insurance"),
    @NamedQuery(name = "Plans.findByDescription", query = "SELECT p FROM Plans p WHERE p.description = :description")})
public class Plans implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Size(max = 11)
    @Column(name = "identifier")
    private Integer identifier;

    @JoinColumn(name = "type_specialist_id", referencedColumnName = "id")
    @ManyToOne
    private TypeSpecialist typeSpecialistId;

    @JoinColumn(name = "insurance_id", referencedColumnName = "id")
    @ManyToOne
    private Insurances insuranceId;

    @Column(name = "reimbursement_amount")
    private Double reimbursement_amount;

    @JoinTable(name = "plans_treatments", joinColumns = {
        @JoinColumn(name = "plans_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "treatments_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Treatments> treatmentsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plansId")
    private List<PlansTreatments> plansTreatmentsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plansId")
    private List<PlansPatients> plansPatientsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plansId")
    private List<Reimbursement> reimbursementList;

    public Double getReimbursement_amount() {
        return reimbursement_amount;
    }

    public void setReimbursement_amount(Double reimbursement_amount) {
        this.reimbursement_amount = reimbursement_amount;
    }

    public Plans() {
    }

    public Plans(Integer id) {
        this.id = id;
    }

    public Plans(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    @XmlTransient
    public List<Treatments> getTreatmentsList() {
        return treatmentsList;
    }

    public void setTreatmentsList(List<Treatments> treatmentsList) {
        this.treatmentsList = treatmentsList;
    }

    public TypeSpecialist getTypeSpecialistId() {
        return typeSpecialistId;
    }

    public void setTypeSpecialistId(TypeSpecialist typeSpecialistId) {
        this.typeSpecialistId = typeSpecialistId;
    }

    public Insurances getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(Insurances insuranceId) {
        this.insuranceId = insuranceId;
    }

    @XmlTransient
    public List<PlansPatients> getPlansPatientsList() {
        return plansPatientsList;
    }

    public void setPlansPatientsList(List<PlansPatients> plansPatientsList) {
        this.plansPatientsList = plansPatientsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plans)) {
            return false;
        }
        Plans other = (Plans) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.backend.model.Plans[ id=" + id + " ]";
    }

    @XmlTransient
    public List<PlansTreatments> getPlansTreatmentsList() {
        return plansTreatmentsList;
    }

    public void setPlansTreatmentsList(List<PlansTreatments> plansTreatmentsList) {
        this.plansTreatmentsList = plansTreatmentsList;
    }

}
