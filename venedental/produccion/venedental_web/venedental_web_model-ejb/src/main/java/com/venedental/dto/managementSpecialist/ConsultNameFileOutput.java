/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.managementSpecialist;

/**
 *
 * @author akdesk01
 */
public class ConsultNameFileOutput {
    private Integer idFile;
    private String nameFile;

    public ConsultNameFileOutput() {
    }
    
    

    public Integer getIdFile() {
        return idFile;
    }

    public void setIdFile(Integer idFile) {
        this.idFile = idFile;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }
    
    
}
