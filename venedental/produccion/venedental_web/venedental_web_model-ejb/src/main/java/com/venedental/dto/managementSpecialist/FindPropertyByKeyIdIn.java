package com.venedental.dto.managementSpecialist;

/**
 * Created by akdeskdev90 on 15/07/16.
 */
public class FindPropertyByKeyIdIn {

    private Integer idKey;
    private Integer idTooth;

    public FindPropertyByKeyIdIn() {
    }

    public FindPropertyByKeyIdIn(Integer idKey, Integer idTooth) {
        this.idKey = idKey;
        this.idTooth = idTooth;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idKey) {
        this.idKey = idKey;
    }

    public Integer getIdTooth() {
        return idTooth;
    }

    public void setIdTooth(Integer idTooth) {
        this.idTooth = idTooth;
    }
}
