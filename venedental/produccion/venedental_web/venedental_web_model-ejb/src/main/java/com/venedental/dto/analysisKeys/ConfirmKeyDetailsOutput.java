/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author akdeskdev90
 */
public class ConfirmKeyDetailsOutput {

    private String numberKey;
    private Integer idPlansTreatmentsKey;
    private Integer idPropertiesPlansTreatmentsKey;
    private String nameTreatments;
    private String nameProperties;
    private String observation;
    private Double amountPaid;
    private String amountPaidString;
    private Double totalAmountPaid;
    private Integer idPatient;
    private String namePatient;
    private Integer idStatus;
    private String nameStatus;
    private Date dateTreatment;
    private Date dateMinFilter;
    private Date MaxFilter;


    public ConfirmKeyDetailsOutput() {
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public Integer getIdPlansTreatmentsKey() {
        return idPlansTreatmentsKey;
    }

    public void setIdPlansTreatmentsKey(Integer idPlansTreatmentsKey) {
        this.idPlansTreatmentsKey = idPlansTreatmentsKey;
    }

    public Integer getIdPropertiesPlansTreatmentsKey() {
        return idPropertiesPlansTreatmentsKey;
    }

    public void setIdPropertiesPlansTreatmentsKey(Integer idPropertiesPlansTreatmentsKey) {
        this.idPropertiesPlansTreatmentsKey = idPropertiesPlansTreatmentsKey;
    }

    public String getNameTreatments() {
        return nameTreatments;
    }

    public void setNameTreatments(String nameTreatments) {
        this.nameTreatments = nameTreatments;
    }

    public String getNameProperties() {
        return nameProperties;
    }

    public void setNameProperties(String nameProperties) {
        this.nameProperties = nameProperties;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Double getAmountPaid() {
        Double i=0.00;
        i=(double)((int)(amountPaid*100)/100.0); 
       amountPaid = i;
       return amountPaid;
    }

    public void setAmountPaid(Double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Double getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(Double totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String getAmountPaidString() {
        return amountPaidString;
    }

    public void setAmountPaidString(String amountPaidString) {
        this.amountPaidString = amountPaidString;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public Date getDateTreatment() {
        return dateTreatment;
    }

    public String getDateTreatmentStr(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return  formatter.format(dateTreatment);
    }

    public void setDateTreatment(Date dateTreatment) {
        this.dateTreatment = dateTreatment;
    }

    public Date getDateMinFilter() {
        return dateMinFilter;
    }

    public void setDateMinFilter(Date dateMinFilter) {
        this.dateMinFilter = dateMinFilter;
    }

    public Date getMaxFilter() {
        return MaxFilter;
    }

    public void setMaxFilter(Date MaxFilter) {
        this.MaxFilter = MaxFilter;
    }

    
    
}
