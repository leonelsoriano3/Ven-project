/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.paymentReport;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialInput;
import com.venedental.dto.paymentReport.PaymentReportkeysAnalisysByCriterialOuput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk58
 */
public class ConsultPaymentReportKeysAnalisysByCriterialDAO extends AbstractQueryDAO<PaymentReportkeysAnalisysByCriterialInput, PaymentReportkeysAnalisysByCriterialOuput> {
    
    public ConsultPaymentReportKeysAnalisysByCriterialDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_007", 5);
    }


    @Override
    public void prepareInput(PaymentReportkeysAnalisysByCriterialInput input, CallableStatement statement) throws SQLException {
        
        // validamos parametros null en ced / rif 
        if (input.getIndentityNumberSpecialistsClinic() == 0) {
            statement.setNull(1, java.sql.Types.NULL);
        }else{
            statement.setInt(1,input.getIndentityNumberSpecialistsClinic());
        }
        
        // validamos parametros null en rango de fechas.
        if(input.getStartDate()!=null&&input.getEndDate()!=null){
           statement.setDate(2, new Date(input.getStartDate().getTime()));
           statement.setDate(3, new Date(input.getEndDate().getTime()));
        }else{
           statement.setNull(2, java.sql.Types.NULL);
           statement.setNull(3, java.sql.Types.NULL);           
        }
                
        if(input.getNumberInvoices()!=""){
            statement.setString(4,input.getNumberInvoices());
        }else{
           statement.setNull(4, java.sql.Types.NULL);
        }
        
        if(input.getStatusKeysAnalysis()==0){
            statement.setNull(5, java.sql.Types.NULL);
        }else{
            statement.setInt(5,input.getStatusKeysAnalysis());
          
        }
    }

    @Override
    public PaymentReportkeysAnalisysByCriterialOuput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
       PaymentReportkeysAnalisysByCriterialOuput 
       PaymentReportkeysAnalisys;
       PaymentReportkeysAnalisys = new PaymentReportkeysAnalisysByCriterialOuput();
       
       PaymentReportkeysAnalisys.setPaymentReporId(rs.getInt("ID_REPORTE_PAGO"));
       PaymentReportkeysAnalisys.setPaymentReportRefundNumber(rs.getString("NUMERO_REPORTE_PAGO"));
       PaymentReportkeysAnalisys.setPaymentReportRefundDate(rs.getDate("FECHA_REPORTE_PAGO"));
       PaymentReportkeysAnalisys.setSpecialistId(rs.getInt("ID_ESPECIALISTA"));
       PaymentReportkeysAnalisys.setCompleteNameSpecialists(rs.getString("NOMBRE_ESPECIALISTA"));
       PaymentReportkeysAnalisys.setIdentityNumberRIFSpecialistsClinic(rs.getString("CEDULA_RIF_ESPECIALISTA"));
       PaymentReportkeysAnalisys.setNumberInvoices(rs.getString("NUMERO_FACTURA"));
       PaymentReportkeysAnalisys.setControlNumber(rs.getString("NUMERO_CONTROL"));
       PaymentReportkeysAnalisys.setInvoicesDate(rs.getDate("FECHA_FACTURA"));
       PaymentReportkeysAnalisys.setTransferDate(rs.getDate("FECHA_TRANSFERENCIA"));
       PaymentReportkeysAnalisys.setNumberTransfer(rs.getString("NUMERO_TRANSFERENCIA"));
       PaymentReportkeysAnalisys.setIslr_rate(rs.getDouble("PORCENTAJE_RETENCION"));
       PaymentReportkeysAnalisys.setAmountRetention(rs.getDouble("MONTO_RETENCION_ISLR"));
       PaymentReportkeysAnalisys.setAmountInvoices(rs.getDouble("MONTO_FACTURA"));
       PaymentReportkeysAnalisys.setNetAmountInvoices(rs.getDouble("MONTO_NETO"));
       
       return PaymentReportkeysAnalisys;
    }

    
}
