/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.BanksPatients;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author AKDESK12
 */
@Stateless
public class BanksPatientsFacade extends AbstractFacade<BanksPatients> implements BanksPatientsFacadeLocal {
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BanksPatientsFacade() {
        super(BanksPatients.class);
    }
    
    @Override
    public List<BanksPatients> findByAccountNumber(String accountNumber) {
        TypedQuery<BanksPatients> query = em.createNamedQuery("BanksPatients.findByAccountNumber", BanksPatients.class);
        query.setParameter("accountNumber", accountNumber);
        return query.getResultList(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
