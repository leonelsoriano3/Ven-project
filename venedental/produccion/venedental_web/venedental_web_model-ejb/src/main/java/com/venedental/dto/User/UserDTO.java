package com.venedental.dto.User;

/**
 * Created by akdeskdev90 on 18/05/16.
 */
public class UserDTO {

    private Integer id;
    private String name;
    private String login;
    private String password;
    private Integer status;
    private String email;
    private String token;
    private String expiredDate;
    private String idCard;
    private boolean tokenConsumed;

    //transiend
    private String completeNameSpecialist;
    private Integer carIdSpecialist;


    public UserDTO(){
        this.id = 0;
        this.name = "";
        this.login = "";
        this.password = "";
        this.status = 0;
        this.email = "";
        this.token = "";
        this.expiredDate = "";
        this.idCard = "";
    }


    public UserDTO(String email, Integer status, String password, String login, String name, Integer id) {
        this.email = email;
        this.status = status;
        this.password = password;
        this.login = login;
        this.name = name;
        this.id = id;
    }

    public UserDTO(Integer id, String name, String login, String password, Integer status, String email,
                   String token, String expiredDate) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.status = status;
        this.email = email;
        this.token = token;
        this.expiredDate = expiredDate;
    }



    public String getCompleteNameSpecialist() {
        return completeNameSpecialist;
    }

    public void setCompleteNameSpecialist(String completeNameSpecialist) {
        this.completeNameSpecialist = completeNameSpecialist;
    }

    public Integer getCarIdSpecialist() {
        return carIdSpecialist;
    }

    public void setCarIdSpecialist(Integer carIdSpecialist) {
        this.carIdSpecialist = carIdSpecialist;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isTokenConsumed() {return tokenConsumed;}

    public void setTokenConsumed(boolean tokenConsumed) {this.tokenConsumed = tokenConsumed; }
}
