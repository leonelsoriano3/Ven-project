package com.venedental.dto.reportDentalTreatments;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by akdesk10 on 04/11/16.
 */
public class TreatmentOutput {

    private String dateKey;
    private String namePatient;
    private String numberKey;
    private String numTooth;
    private String nameTreatment;

    public TreatmentOutput() {
    }

    public TreatmentOutput(String dateKey, String namePatient, String numberKey, String numTooth) {
        this.dateKey = dateKey;
        this.namePatient = namePatient;
        this.numberKey = numberKey;
        this.numTooth = numTooth;
    }

    public String getNameTreatment() {
        return nameTreatment;
    }

    public void setNameTreatment(String nameTreatment) {
        this.nameTreatment = nameTreatment;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {


//        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-DD");
//        Date dateKeyD = null;
//        try {
//            dateKeyD = formatter.parse(dateKey);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//       if(dateKeyD == null){
//           this.dateKey = dateKey;
//       }
//        else{
//           String dateKeyS = new SimpleDateFormat("dd/MM/yyyy").format(dateKeyD);
//           this.dateKey = dateKeyS;
//       }
//        SimpleDateFormat fromUser = new SimpleDateFormat("YYYY/MM/DD");
//        SimpleDateFormat myFormat = new SimpleDateFormat("DD/MM/YYYY");
//
//        try {
//
//            String reformattedStr = myFormat.format(fromUser.parse(dateKey));
//            this.dateKey = reformattedStr;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        this.dateKey = dateKey;

    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getNumberKey() {
        return numberKey;
    }

    public void setNumberKey(String numberKey) {
        this.numberKey = numberKey;
    }

    public String getNumTooth() {
        return numTooth;
    }

    public void setNumTooth(String numTooth) {
        this.numTooth = numTooth;
    }
}
