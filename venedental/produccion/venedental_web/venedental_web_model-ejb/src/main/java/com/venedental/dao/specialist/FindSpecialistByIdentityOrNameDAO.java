/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.specialist;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.model.Specialists;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author AKDESKDEV90
 */
public class FindSpecialistByIdentityOrNameDAO extends AbstractQueryDAO<String, Specialists> {

    public FindSpecialistByIdentityOrNameDAO(DataSource dataSource) {
        super(dataSource, "SP_T_SPECIALISTS_G_002", 1);
    }

    @Override
    public void prepareInput(String input, CallableStatement statement) throws SQLException {
        statement.setString(1, input);
    }

    @Override
    public Specialists prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        Specialists specialists = new Specialists();
        specialists.setId(rs.getInt("id"));
        specialists.setCompleteName(rs.getString("complete_name"));
        specialists.setIdentityNumber(rs.getInt("identity_number"));
        return specialists;
    }
}
