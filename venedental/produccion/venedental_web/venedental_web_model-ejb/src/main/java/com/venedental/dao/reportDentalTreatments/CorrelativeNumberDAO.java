package com.venedental.dao.reportDentalTreatments;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.reportDentalTreatments.ReportDentalInfoOutput;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by akdesk10 on 29/06/16.
 */
public class CorrelativeNumberDAO extends AbstractQueryDAO<Integer,ReportDentalInfoOutput> {
    public CorrelativeNumberDAO(DataSource dataSource){
        super(dataSource, "SP_T_SPECIALISTS_G_008",3);
    }


    @Override
    public void prepareInput(Integer input, CallableStatement statement) throws SQLException {
        statement.setInt(1,input);
        statement.registerOutParameter(2, java.sql.Types.INTEGER);
        statement.registerOutParameter(3, java.sql.Types.DATE);
    }

    @Override
    public ReportDentalInfoOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        
        ReportDentalInfoOutput reportDentalInfoOutput=new ReportDentalInfoOutput();
        reportDentalInfoOutput.setNumCorrelativo(statement.getInt(2));
        reportDentalInfoOutput.setDateForReport(statement.getDate(3));

      
        return reportDentalInfoOutput;
    }
}