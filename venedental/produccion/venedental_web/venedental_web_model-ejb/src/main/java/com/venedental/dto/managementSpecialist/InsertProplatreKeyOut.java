package com.venedental.dto.managementSpecialist;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 */
public class InsertProplatreKeyOut {



    private Integer idPieceTreatmentKey;
    private Integer idPieceTreamentKeyDiagnosis;

    public InsertProplatreKeyOut() {
    }

    public Integer getIdPieceTreatmentKey() {
        return idPieceTreatmentKey;
    }

    public void setIdPieceTreatmentKey(Integer idPieceTreatmentKey) {
        this.idPieceTreatmentKey = idPieceTreatmentKey;
    }

    public Integer getIdPieceTreamentKeyDiagnosis() {
        return idPieceTreamentKeyDiagnosis;
    }

    public void setIdPieceTreamentKeyDiagnosis(Integer idPieceTreamentKeyDiagnosis) {
        this.idPieceTreamentKeyDiagnosis = idPieceTreamentKeyDiagnosis;
    }
}
