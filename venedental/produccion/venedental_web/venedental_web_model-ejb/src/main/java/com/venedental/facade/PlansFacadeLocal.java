/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.facade;

import com.venedental.model.Insurances;
import com.venedental.model.Plans;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Usuario
 */
@Local
public interface PlansFacadeLocal {

    Plans create(Plans patients);

    Plans edit(Plans patients);

    void remove(Plans patients);

    Plans find(Object id);

    List<Plans> findAll();

    List<Plans> findRange(int[] range);

    List<Plans> findByPatientsId(Integer id);

    List<Plans> findByInsuranceId(Insurances insurance);

    List<Plans> findById(Integer id);

    List<Plans> findPlansAvailableByPatient(Integer idPatient, Integer idTypeSpeciality, Integer isHolder);

    List<Plans> finPlansPatientsByHealthArea(Integer identityHolder, Integer healtArea);

    int count();

}
