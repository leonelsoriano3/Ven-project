/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "banks", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banks.findAll", query = "SELECT b FROM Banks b ORDER BY b.name ASC"),
    @NamedQuery(name = "Banks.findById", query = "SELECT b FROM Banks b WHERE b.id = :id"),
    @NamedQuery(name = "Banks.findByName", query = "SELECT b FROM Banks b WHERE b.name = :name")})
public class Banks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    @Basic(optional=false)
    @NotNull
    @Size(min=1, max=4)
    @Column(name="code")
    private String code;

    @OneToMany(mappedBy = "bank")
    private List<BankAccounts> bankAccountsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bankId")
    private List<BanksPatients> banksPatientsList;

    public Banks() {
    }

    public Banks(Integer id) {
        this.id = id;
    }

    public Banks(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public List<BankAccounts> getBankAccountsList() {
        return bankAccountsList;
    }

    public void setBankAccountsList(List<BankAccounts> bankAccountsList) {
        this.bankAccountsList = bankAccountsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banks)) {
            return false;
        }
        Banks other = (Banks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.venedental.model.Banks[ id=" + id + " ]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Banks obj = null;
        obj = (Banks) super.clone();
        if (obj.getBankAccountsList() == null) {
            obj.bankAccountsList = null;
        } else {
            List<BankAccounts> listaBankAccounts = new ArrayList<>();
            for (BankAccounts item : obj.getBankAccountsList()) {
                listaBankAccounts.add((BankAccounts) item.clone());
            }
            obj.bankAccountsList = listaBankAccounts;
        }
        return obj;
    }

    @XmlTransient
    public List<BanksPatients> getBanksPatientsList() {
        return banksPatientsList;
    }

    public void setBanksPatientsList(List<BanksPatients> banksPatientsList) {
        this.banksPatientsList = banksPatientsList;
    }

}
