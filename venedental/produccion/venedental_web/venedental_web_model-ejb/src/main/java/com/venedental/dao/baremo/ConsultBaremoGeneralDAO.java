/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.baremo;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.baremo.ConsultBaremoGeneralInput;
import com.venedental.dto.baremo.ConsultBaremoGeneralOutput;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdesk01
 */
public class ConsultBaremoGeneralDAO extends AbstractQueryDAO<ConsultBaremoGeneralInput, ConsultBaremoGeneralOutput> {

    public ConsultBaremoGeneralDAO(DataSource dataSource) {
        super(dataSource, "SP_M_BAR_LeerBaremosGenerales", 3);
    }

    @Override
    public void prepareInput(ConsultBaremoGeneralInput input, CallableStatement statement) throws SQLException {
        statement.setDate(1, (Date) input.getFechaInicio());
        statement.setDate(2, (Date) input.getFechaFin());

        if (input.getIdEspecialidad() == 0) {

            statement.setNull(3, java.sql.Types.NULL);

        } else {
            statement.setInt(3, input.getIdEspecialidad());

        }
    }

    @Override
    public ConsultBaremoGeneralOutput prepareOutput(ResultSet rs, CallableStatement statement) throws SQLException {
        ConsultBaremoGeneralOutput consultBaremoGeneralOutput = new ConsultBaremoGeneralOutput();
        consultBaremoGeneralOutput.setFechaBaremo(rs.getDate("fecha_baremo"));
        consultBaremoGeneralOutput.setNameTratamiento(rs.getString("nombre_tratamiento"));
        consultBaremoGeneralOutput.setBaremo(rs.getDouble("baremo"));
        consultBaremoGeneralOutput.setBaremoForReport(rs.getString("baremo"));
        consultBaremoGeneralOutput.setNameEspecialidad(rs.getString("nombre_especialidad"));
        consultBaremoGeneralOutput.setIdEspecialidad(rs.getInt("id_especialidad"));
        return consultBaremoGeneralOutput;
    }

}
