package com.venedental.dto.managementSpecialist;


import java.sql.Date;

/**
 * Created by leonelsoriano3@gmail.com on 15/07/16.
 */





public class InsertProplatreKeyIn {

    private Integer idTreatment;
    private Integer idTreatmentPiece;
    private Date date;
    private Integer idStatus;
    private Integer baremoTreatmentPiece;
    private String observation;
    private Integer IdUsuario;
    private Date dateReception;
    private Integer idTreatmentKeyDiagnosis;

    public InsertProplatreKeyIn() {}

    public InsertProplatreKeyIn(Integer idTreatment, Integer idTreatmentPiece, Date date, Integer idStatus,
                                Integer baremoTreatmentPiece, String observation, Date dateReception,
                                Integer idUsuario, Integer idTreatmentKeyDiagnosis) {

        this.idTreatment = idTreatment;
        this.idTreatmentPiece = idTreatmentPiece;
        this.date = date;
        this.idStatus = idStatus;
        this.baremoTreatmentPiece = baremoTreatmentPiece;
        this.observation = observation;
        this.dateReception = dateReception;
        IdUsuario = idUsuario;
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdTreatmentPiece() {
        return idTreatmentPiece;
    }

    public void setIdTreatmentPiece(Integer idTreatmentPiece) {
        this.idTreatmentPiece = idTreatmentPiece;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getBaremoTreatmentPiece() {
        return baremoTreatmentPiece;
    }

    public void setBaremoTreatmentPiece(Integer baremoTreatmentPiece) {
        this.baremoTreatmentPiece = baremoTreatmentPiece;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        IdUsuario = idUsuario;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Integer getIdTreatmentKeyDiagnosis() {
        return idTreatmentKeyDiagnosis;
    }

    public void setIdTreatmentKeyDiagnosis(Integer idTreatmentKeyDiagnosis) {
        this.idTreatmentKeyDiagnosis = idTreatmentKeyDiagnosis;
    }
}
