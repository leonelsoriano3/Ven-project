/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.services;

import com.venedental.dao.scaleTreatments.FindMinDateScale;
import com.venedental.dao.scaleTreatments.FindScaleRegisterDAO;
import com.venedental.dao.scaleTreatments.RegisterScalePropertieTreatment;
import com.venedental.dto.scaleTreatments.FindScaleRegisterInput;
import com.venedental.dto.scaleTreatments.FindScaleRegisterOutput;
import java.sql.SQLException;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author root
 */
@Stateless
public class ServiceScaleTreatments {
    
    @Resource(lookup = "jdbc/venedental")
    private DataSource ds;
    
        /**
     * Servicio para buscar el precio de un tratamiento si el especialista tiene
     * baremo especifico devuelve el precio especifico mas cercano a la fecha
     * pasada por parametro, sino devuelve el precio general mas cercano a la
     * fecha pasada por parametros
     *
     * @param idSpecialist
     * @param idTreatments
     * @param date
     * @param idProTreat
     * @return
     */
    public FindScaleRegisterOutput findScaleTratmentRegister(Integer idSpecialist, Integer idTreatments, Date date, Integer idProTreat) {
        FindScaleRegisterDAO scaleTreat = new FindScaleRegisterDAO(ds);
        FindScaleRegisterInput input = new FindScaleRegisterInput(idSpecialist, idTreatments, date, idProTreat);
        scaleTreat.execute(input);
        return scaleTreat.getResult();
    }
    
    /**
     * Servicio que retorna la fecha minima de registro de un scale 
     * verificando si general o especifico para el especialista
     * @param idSpecialist
     * @param idTreatments
     * @return 
     */
    public Date findMinDateScale(Integer idSpecialist, Integer idTreatments) {
        FindMinDateScale findMinDateScale = new FindMinDateScale(ds);
        FindScaleRegisterInput input = new FindScaleRegisterInput(idSpecialist, idTreatments, null);
        findMinDateScale.execute(input);
        return findMinDateScale.getResult();
    }
    
    
    /**
     * Servicio que permite registrar el scale a las piezas dado el del
     * sacleTratment
     * @param idScaleTreat    
     */
    public void registerScaleProTre(Integer idScaleTreat) throws SQLException {
        RegisterScalePropertieTreatment registerScalePropertieTreatment = new RegisterScalePropertieTreatment(ds);
        registerScalePropertieTreatment.execute(idScaleTreat);
    }
}
