/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dto.analysisKeys;

import java.util.Date;

/**
 *
 * @author root
 */
public class ConsultPaymentProcessOutput {

    private String doctorClinit;
    private String celRif;
    private String bank;
    private String accountNumber;
    private String associatedCelRif;
    private String nameStatus;
    private Integer idStatus;
    private Integer idReport;
    private String numberReport;
    public String numberRetention;
    private Integer isNatural;

    public ConsultPaymentProcessOutput() {
    }

    public ConsultPaymentProcessOutput(String doctorClinit, String celRif, String bank, String accountNumber, String associatedCelRif, String nameStatus, Integer idStatus, Integer idReport, String numberReport, String numberRetention, Integer isNatural) {
        this.doctorClinit = doctorClinit;
        this.celRif = celRif;
        this.bank = bank;
        this.accountNumber = accountNumber;
        this.associatedCelRif = associatedCelRif;
        this.nameStatus = nameStatus;
        this.idStatus = idStatus;
        this.idReport = idReport;
        this.numberReport = numberReport;
        this.numberRetention = numberRetention;
        this.isNatural = isNatural;
    }

    public Integer getIsNatural() {
        return isNatural;
    }

    public void setIsNatural(Integer esNatural) {
        this.isNatural = esNatural;
    }

    public String getNumberReport() {
        return numberReport;
    }

    public void setNumberReport(String numberReport) {
        this.numberReport = numberReport;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getDoctorClinit() {
        return doctorClinit;
    }

    public void setDoctorClinit(String doctorClinit) {
        this.doctorClinit = doctorClinit;
    }

    public String getCelRif() {
        return celRif;
    }

    public void setCelRif(String celRif) {
        this.celRif = celRif;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAssociatedCelRif() {
        return associatedCelRif;
    }

    public void setAssociatedCelRif(String associatedCelRif) {
        this.associatedCelRif = associatedCelRif;
    }

    public Integer getIdReport() {
        return idReport;
    }

    public void setIdReport(Integer idReport) {
        this.idReport = idReport;
    }

    public String getNumberRetention() {
        return numberRetention;
    }

    public void setNumberRetention(String numberRetention) {
        this.numberRetention = numberRetention;
    }

}
