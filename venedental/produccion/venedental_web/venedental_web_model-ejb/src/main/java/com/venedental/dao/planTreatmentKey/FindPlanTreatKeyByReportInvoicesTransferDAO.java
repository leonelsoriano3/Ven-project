/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venedental.dao.planTreatmentKey;

import com.venedental.dao.AbstractQueryDAO;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByKeyAndPlanTreatInput;
import com.venedental.dto.planTreatmentKey.FindPlanTreatKeyByReportInvoicesTransferInput;
import com.venedental.model.PlansTreatmentsKeys;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author akdeskdev90
 */
public class FindPlanTreatKeyByReportInvoicesTransferDAO extends AbstractQueryDAO<FindPlanTreatKeyByReportInvoicesTransferInput, PlansTreatmentsKeys> {
    
    
    public FindPlanTreatKeyByReportInvoicesTransferDAO(DataSource dataSource) {
        super(dataSource, "SP_T_PAYMENTREPOR_G_005", 4);
    }

    @Override
    public void prepareInput(FindPlanTreatKeyByReportInvoicesTransferInput input, CallableStatement statement) throws SQLException {
        statement.setInt(1, input.getIdReport());
        statement.setInt(2, input.getIdStatus());
        statement.setDate(3, (Date) input.getDateFrom()); 
        statement.setDate(4, (Date) input.getDateUntil()); 
    }

    @Override
    public PlansTreatmentsKeys prepareOutput(ResultSet rs , CallableStatement statement) throws SQLException {
        PlansTreatmentsKeys plansTreatmentsKeys = new PlansTreatmentsKeys();
        plansTreatmentsKeys.setId(rs.getInt("id"));
        plansTreatmentsKeys.setKeys_id(rs.getInt("keys_id"));
        plansTreatmentsKeys.setPlans_treatments_id(rs.getInt("plans_treatments_id"));
        plansTreatmentsKeys.setDatePlansTreatmentsKeys(rs.getDate("date_plans_treatments_keys"));
        plansTreatmentsKeys.setStatusId(rs.getInt("id_status"));
        plansTreatmentsKeys.setIdScaleTreatment(rs.getInt("id_scale_treatments"));
        plansTreatmentsKeys.setObservation(rs.getString("observation"));
        return plansTreatmentsKeys;
    }
}