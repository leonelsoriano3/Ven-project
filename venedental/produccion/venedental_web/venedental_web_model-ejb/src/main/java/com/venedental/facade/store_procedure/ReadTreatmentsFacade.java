/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.venedental.facade.store_procedure;



import com.venedental.dto.ReadTreatmentsReportInput;
import com.venedental.dto.ReadTreatmentsReportOutput;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ARKDEV16
 */
@Stateless
public class ReadTreatmentsFacade
extends AbstractStoreProcedureFacade<ReadTreatmentsReportInput, ReadTreatmentsReportOutput>
implements ReadTreatmentsFacadeLocal {
    
    @PersistenceContext(unitName = "venedentalPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public ReadTreatmentsFacade() {
        super("SP_LEER_TRATAMIENTOS");
    }
    
    @Override
    protected void prepareParameter(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, Integer.class, IN);
        query.registerStoredProcedureParameter(2, Integer.class, IN);
        query.registerStoredProcedureParameter(3, Integer.class, IN);
    }
    
    @Override
    protected void prepareInput(StoredProcedureQuery query, ReadTreatmentsReportInput input) {
        query.setParameter(1, input.getIdInsurance());
        query.setParameter(2, input.getIdHealthArea());
        query.setParameter(3, input.getIdPlan());
    }
    
    @Override
    protected ReadTreatmentsReportOutput prepareOutput(Object[] result) {
        ReadTreatmentsReportOutput output = new ReadTreatmentsReportOutput();
        output.setIdTreatment((Integer) result[0]);
        output.setNameTreatment((String) result[1]);
        return output;
    }


    




 
}
