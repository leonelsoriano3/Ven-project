/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;

/**
 *
 * @author akdesarrollo
 */
public class ListaLinks implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String titulo;
    private String url;
    private String rutaImagen;

    public ListaLinks() {
    }

    public ListaLinks(String titulo, String url, String rutaImagen) {
        this.titulo = titulo;
        this.url = url;
        this.rutaImagen = rutaImagen;
    }

    
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }
    
    
}
