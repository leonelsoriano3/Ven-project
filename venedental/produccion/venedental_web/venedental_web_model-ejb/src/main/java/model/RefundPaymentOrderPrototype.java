package model;

import java.io.Serializable;

public class RefundPaymentOrderPrototype implements Serializable{

    /*
    
    Atributes 
    
    */
    
    // Reimbursement    
    private String reimbursement_id;
    private String fecha_reimbursement;
    private String nombre_titular;
    private String apellido_titular;
    private String cedula_titular;
    private String direccion;
    private String status_reimbursement;
    
    // Order 
    private String number_order;
    private String date;
    private String date_tranfer;
    private String number_transfer;
    private String concept;
    private String order_status;
    
    // Requirement
    private String nro_factura;
    private String cedula_beneficiario;
    private String nombre_beneficiario;
    private String apellido_beneficiario;
    private String monto_factura;
    private String tratamiento;
    private String document;
    private String bank;
    private String account_bank;
    
    // PaymeReport
    private String documentReport;
    
    
    // Contructs
    public RefundPaymentOrderPrototype() {
    }

    public RefundPaymentOrderPrototype(String reimbursement_id, String fecha_reimbursement, String nombre_titular, String apellido_titular, String cedula_titular, String direccion, String status_reimbursement, String number_order, String date, String date_tranfer, String number_transfer, String concept, String order_status, String nro_factura, String cedula_beneficiario, String nombre_beneficiario, String apellido_beneficiario, String monto_factura, String tratamiento, String document, String bank, String account_bank, String documentReport) {
        this.reimbursement_id = reimbursement_id;
        this.fecha_reimbursement = fecha_reimbursement;
        this.nombre_titular = nombre_titular;
        this.apellido_titular = apellido_titular;
        this.cedula_titular = cedula_titular;
        this.direccion = direccion;
        this.status_reimbursement = status_reimbursement;
        this.number_order = number_order;
        this.date = date;
        this.date_tranfer = date_tranfer;
        this.number_transfer = number_transfer;
        this.concept = concept;
        this.order_status = order_status;
        this.nro_factura = nro_factura;
        this.cedula_beneficiario = cedula_beneficiario;
        this.nombre_beneficiario = nombre_beneficiario;
        this.apellido_beneficiario = apellido_beneficiario;
        this.monto_factura = monto_factura;
        this.tratamiento = tratamiento;
        this.document = document;
        this.bank = bank;
        this.account_bank = account_bank;
        this.documentReport = documentReport;
    }

   

    
    
      
    // Getter and Setter

    public String getNumber_order() {
        return number_order;
    }

    public void setNumber_order(String number_order) {
        this.number_order = number_order;
    }

    public String getReimbursement_id() {
        return reimbursement_id;
    }

    public void setReimbursement_id(String reimbursement_id) {
        this.reimbursement_id = reimbursement_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumber_transfer() {
        return number_transfer;
    }

    public void setNumber_transfer(String number_transfer) {
        this.number_transfer = number_transfer;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getFecha_reimbursement() {
        return fecha_reimbursement;
    }

    public void setFecha_reimbursement(String fecha_reimbursement) {
        this.fecha_reimbursement = fecha_reimbursement;
    }

    public String getNombre_titular() {
        return nombre_titular;
    }

    public void setNombre_titular(String nombre_titular) {
        this.nombre_titular = nombre_titular;
    }

    public String getApellido_titular() {
        return apellido_titular;
    }

    public void setApellido_titular(String apellido_titular) {
        this.apellido_titular = apellido_titular;
    }

    public String getCedula_titular() {
        return cedula_titular;
    }

    public void setCedula_titular(String cedula_titular) {
        this.cedula_titular = cedula_titular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getStatus_reimbursement() {
        return status_reimbursement;
    }

    public void setStatus_reimbursement(String status_reimbursement) {
        this.status_reimbursement = status_reimbursement;
    }

    public String getNro_factura() {
        return nro_factura;
    }

    public void setNro_factura(String nro_factura) {
        this.nro_factura = nro_factura;
    }

    public String getCedula_beneficiario() {
        return cedula_beneficiario;
    }

    public void setCedula_beneficiario(String cedula_beneficiario) {
        this.cedula_beneficiario = cedula_beneficiario;
    }

    public String getNombre_beneficiario() {
        return nombre_beneficiario;
    }

    public void setNombre_beneficiario(String nombre_beneficiario) {
        this.nombre_beneficiario = nombre_beneficiario;
    }

    public String getApellido_beneficiario() {
        return apellido_beneficiario;
    }

    public void setApellido_beneficiario(String apellido_beneficiario) {
        this.apellido_beneficiario = apellido_beneficiario;
    }

    public String getMonto_factura() {
        return monto_factura;
    }

    public void setMonto_factura(String monto_factura) {
        this.monto_factura = monto_factura;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccount_bank() {
        return account_bank;
    }

    public void setAccount_bank(String account_bank) {
        this.account_bank = account_bank;
    }

    public String getDate_tranfer() {
        return date_tranfer;
    }

    public void setDate_tranfer(String date_tranfer) {
        this.date_tranfer = date_tranfer;
    }

    public String getDocumentReport() {
        return documentReport;
    }

    public void setDocumentReport(String documentReport) {
        this.documentReport = documentReport;
    }
  
    
}
