/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author AKDESK12
 */
public class ReimbursementReport implements Serializable{
    
      private static final long serialVersionUID = 1L;
      
      private Date applicationDate;
      private String fecha;
      private String nroOrden;
      private String cedula;
      private String nombre;
      private String monto;
      
      public ReimbursementReport(){}
      
      public ReimbursementReport(String fecha,String nroOrden,String cedula,String nombre,String monto){
          this.fecha=fecha;
          this.nroOrden=nroOrden;
          this.cedula=cedula;
          this.nombre=nombre;
          this.monto=monto;
      }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(String nroOrden) {
        this.nroOrden = nroOrden;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
      
}
