package model;

public class RequirementPrototype {
    
    private String id;
    private String bank;
    private String account_bank;
    private String number_fact;
    private String amount;
    private String reimbursement_id;
    private String document;

    
    public RequirementPrototype(String id, String bank, String account_bank, String number_fact, String amount, String reimbursement_id, String document) {
        this.id = id;
        this.bank = bank;
        this.account_bank = account_bank;
        this.number_fact = number_fact;
        this.amount = amount;
        this.reimbursement_id = reimbursement_id;
        this.document = document;
    }
    public RequirementPrototype() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccount_bank() {
        return account_bank;
    }

    public void setAccount_bank(String account_bank) {
        this.account_bank = account_bank;
    }

    public String getNumber_fact() {
        return number_fact;
    }

    public void setNumber_fact(String number_fact) {
        this.number_fact = number_fact;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReimbursement_id() {
        return reimbursement_id;
    }

    public void setReimbursement_id(String reimbursement_id) {
        this.reimbursement_id = reimbursement_id;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    
    
}
