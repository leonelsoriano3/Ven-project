package model;

import java.io.Serializable;

public class BinnacleReimbursementPrototype implements Serializable {
    
    private String reimbursement_id;
    private String date_binnacle;
    private String action;
    private String date_process;
    private String status_reimbursment;

    public BinnacleReimbursementPrototype(String reimbursement_id, String date_binnacle, String action, String date_process, String status_reimbursment) {
        this.reimbursement_id = reimbursement_id;
        this.date_binnacle = date_binnacle;
        this.action = action;
        this.date_process = date_process;
        this.status_reimbursment = status_reimbursment;
    }

    public BinnacleReimbursementPrototype() {
    }

    public String getReimbursement_id() {
        return reimbursement_id;
    }

    public void setReimbursement_id(String reimbursement_id) {
        this.reimbursement_id = reimbursement_id;
    }

    public String getDate_binnacle() {
        return date_binnacle;
    }

    public void setDate_binnacle(String date_binnacle) {
        this.date_binnacle = date_binnacle;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDate_process() {
        return date_process;
    }

    public void setDate_process(String date_process) {
        this.date_process = date_process;
    }

    public String getStatus_reimbursment() {
        return status_reimbursment;
    }

    public void setStatus_reimbursment(String status_reimbursment) {
        this.status_reimbursment = status_reimbursment;
    }
    
    
}
