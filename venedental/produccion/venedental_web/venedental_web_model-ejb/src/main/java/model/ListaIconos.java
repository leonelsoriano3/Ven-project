/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author akdesarrollo
 */
public class ListaIconos implements Serializable {
    
    private static final long serialVersionUID = 1L;

    private String titulo;
    private String url;
    private String rutaImagen;
    private Integer x;
    private Integer y;

    public ListaIconos() {
    }

    public ListaIconos(String titulo, String url, String rutaImagen, Integer x, Integer y) {
        this.titulo = titulo;
        this.url = url;
        this.rutaImagen = rutaImagen;
        this.x = x;
        this.y = y;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

}
