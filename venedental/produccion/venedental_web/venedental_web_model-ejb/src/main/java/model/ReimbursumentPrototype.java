
package model;


public class ReimbursumentPrototype {
 
     private static final long serialVersionUID = 1L;
     
     // Reembolso
     private String status;
     private String nro_orden;
     private String fecha;
     private String titular;
     private String cedula_titular;
     private String beneficiario;
     private String cedula_beneficiario;
     private String contratante;
     private String plan;
             
     // requirement
     private String banco;
     private String nro_cuenta;
     private String nro_factura;
     private String monto;
     private String documento;
     private String documento2;

    
     
     
     // Contruct

     public ReimbursumentPrototype() {
    }

    public ReimbursumentPrototype(String status, String nro_orden, String fecha, String titular, String cedula_titular, String beneficiario, String cedula_beneficiario, String contratante, String plan, String banco, String nro_cuenta, String nro_factura, String monto, String documento, String documento2) {
        this.status = status;
        this.nro_orden = nro_orden;
        this.fecha = fecha;
        this.titular = titular;
        this.cedula_titular = cedula_titular;
        this.beneficiario = beneficiario;
        this.cedula_beneficiario = cedula_beneficiario;
        this.contratante = contratante;
        this.plan = plan;
        this.banco = banco;
        this.nro_cuenta = nro_cuenta;
        this.nro_factura = nro_factura;
        this.monto = monto;
        this.documento = documento;
        this.documento2 = documento2;
    }

   
     
     

   

    // Potting
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNro_orden() {
        return nro_orden;
    }

    public void setNro_orden(String nro_orden) {
        this.nro_orden = nro_orden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }    
  
    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNro_cuenta() {
        return nro_cuenta;
    }

    public void setNro_cuenta(String nro_cuenta) {
        this.nro_cuenta = nro_cuenta;
    }

    public String getNro_factura() {
        return nro_factura;
    }

    public void setNro_factura(String nro_factura) {
        this.nro_factura = nro_factura;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getCedula_titular() {
        return cedula_titular;
    }

    public void setCedula_titular(String cedula_titular) {
        this.cedula_titular = cedula_titular;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getCedula_beneficiario() {
        return cedula_beneficiario;
    }

    public void setCedula_beneficiario(String cedula_beneficiario) {
        this.cedula_beneficiario = cedula_beneficiario;
    }

    public String getContratante() {
        return contratante;
    }

    public void setContratante(String contratante) {
        this.contratante = contratante;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
    
    public String getDocumento2() {
        return documento2;
    }

    public void setDocumento2(String documento2) {
        this.documento2 = documento2;
    }
     
     
}
