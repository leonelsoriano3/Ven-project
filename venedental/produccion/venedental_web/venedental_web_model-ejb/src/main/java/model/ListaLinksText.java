/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author akdesarrollo
 */
public class ListaLinksText extends ListaLinks implements Serializable {

    private static final long serialVersionUID = 1L;

    private String texto;

    public ListaLinksText() {
    }

    public ListaLinksText(String titulo, String texto, String url, String rutaImagen) {
        super(titulo, url, rutaImagen);
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

}
